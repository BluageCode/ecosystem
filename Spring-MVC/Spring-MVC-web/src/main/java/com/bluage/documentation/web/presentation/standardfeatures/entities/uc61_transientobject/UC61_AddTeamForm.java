/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.entities.uc61_transientobject;


import java.io.Serializable;
import java.util.List;

import com.bluage.documentation.business.standardfeatures.entities.uc61_transientobject.bos.Team61BO;
import com.netfective.bluage.core.web.AbstractForm;
import com.netfective.bluage.core.web.IForm;

/**
* Class : UC61_AddTeamForm
*/
public class UC61_AddTeamForm extends AbstractForm implements Serializable, IForm {
	
	/** Constant : serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	//CONSTANT : teams
	private static final String TEAMS = "teams";
	//CONSTANT : teamToAdd
	private static final String TEAM_TO_ADD = "teamToAdd";
	/**
	 * 	Property: teams 
	 */
	private List<Team61BO> teams;
	/**
	 * 	Property: teamToAdd 
	 */
	private Team61BO teamToAdd;
/**
	 * Default constructor : UC61_AddTeamForm
	 */
	public UC61_AddTeamForm() {
		super();
		// Initialize : teams
		this.teams = new java.util.ArrayList<com.bluage.documentation.business.standardfeatures.entities.uc61_transientobject.bos.Team61BO>();
		// Initialize : teamToAdd
		this.teamToAdd = new Team61BO();
					this.setAlwaysCallPreControllers(true);
	}
	
	/**
	 * 	Getter : teams 
	 * 	@return : Return the teams instance.
	 */
	public List<Team61BO> getTeams(){
		return teams; // For UC61_AddTeamForm
	}
	
	/**
	 * 	Setter : teams 
	 *  @param teamsinstance : The instance to set.
	 */
	public void setTeams(final List<Team61BO> teamsinstance){
		this.teams = teamsinstance;// For UC61_AddTeamForm
	}
	/**
	 * 	Getter : teamToAdd 
	 * 	@return : Return the teamToAdd instance.
	 */
	public Team61BO getTeamToAdd(){
		return teamToAdd; // For UC61_AddTeamForm
	}
	
	/**
	 * 	Setter : teamToAdd 
	 *  @param teamToAddinstance : The instance to set.
	 */
	public void setTeamToAdd(final Team61BO teamToAddinstance){
		this.teamToAdd = teamToAddinstance;// For UC61_AddTeamForm
	}
/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UC61_AddTeamForm [ "+
TEAMS +" = " + teams +TEAM_TO_ADD +" = " + teamToAdd + "]";
	}
	
	/**
	 * Method setInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public void setInstanceByInstanceName(final String instanceName, final Object instance) {
		// Set the instance Teams.
		if(TEAMS.equals(instanceName)){// For UC61_AddTeamForm
			this.setTeams((List<Team61BO>)instance); // For UC61_AddTeamForm
		}
				// Set the instance TeamToAdd.
		if(TEAM_TO_ADD.equals(instanceName)){// For UC61_AddTeamForm
			this.setTeamToAdd((Team61BO)instance); // For UC61_AddTeamForm
		}
			}
	
	/**
	 * Method getInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public Object getInstanceByInstanceName(final String instanceName){
		// Temporary object for tmpUC61_AddTeamForm.
		Object tmpUC61_AddTeamForm = null;
		
			
		// Get the instance Teams for UC61_AddTeamForm.
		if(TEAMS.equals(instanceName)){ // For UC61_AddTeamForm
			tmpUC61_AddTeamForm = this.getTeams(); // For UC61_AddTeamForm
		}
			
		// Get the instance TeamToAdd for UC61_AddTeamForm.
		if(TEAM_TO_ADD.equals(instanceName)){ // For UC61_AddTeamForm
			tmpUC61_AddTeamForm = this.getTeamToAdd(); // For UC61_AddTeamForm
		}
		return tmpUC61_AddTeamForm;// For UC61_AddTeamForm
	}
	
			}
