--<ScriptOptions statementTerminator=";"/>

ALTER TABLE player04 DROP CONSTRAINT player04_position_fkc;

ALTER TABLE player04 DROP CONSTRAINT player04_pkey;

ALTER TABLE position04 DROP CONSTRAINT position04_pkey;

DROP TABLE player04;

DROP TABLE position04;

CREATE TABLE player04 (
		id INT8 NOT NULL,
		version INT4 NOT NULL,
		first_name VARCHAR(30) NOT NULL,
		last_name VARCHAR(30) NOT NULL,
		date_of_birth DATE,
		estimated_value FLOAT4,
		weight INT4,
		height INT4,
		rookie BOOL,
		position_fk VARCHAR(2) NOT NULL
	);

CREATE TABLE position04 (
		code VARCHAR(2) NOT NULL,
		version INT4 NOT NULL,
		name VARCHAR(20) NOT NULL
	);



INSERT INTO position04 VALUES ('PG', 0, 'Point Guard');
INSERT INTO position04 VALUES ('SG', 0, 'Shooting Guard');
INSERT INTO position04 VALUES ('SF', 0, 'Small Forward');
INSERT INTO position04 VALUES ('PF', 0, 'Power Forward');
INSERT INTO position04 VALUES ('C', 0, 'Center');	

INSERT INTO player04 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, position_fk) VALUES (202, 0, 'Bernard', 'Laplace', '1980-07-14', 37.32, 95, 191, false, 'PF');
INSERT INTO player04 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, position_fk) VALUES (201, 1, 'Julien', 'Sorel', '1975-01-20', 23.549999, 87, 182, false, 'C');

ALTER TABLE player04 ADD CONSTRAINT player04_pkey PRIMARY KEY (id);

ALTER TABLE position04 ADD CONSTRAINT position04_pkey PRIMARY KEY (code);

ALTER TABLE player04 ADD CONSTRAINT player04_position_fkc FOREIGN KEY (position_fk)
	REFERENCES position04 (code);
	