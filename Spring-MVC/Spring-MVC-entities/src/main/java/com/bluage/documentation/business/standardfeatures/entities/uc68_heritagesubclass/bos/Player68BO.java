/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.business.standardfeatures.entities.uc68_heritagesubclass.bos;

import java.util.Date;

import javax.inject.Inject;
import javax.inject.Named;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import com.bluage.documentation.business.AbstractBusinessObject;
import com.bluage.documentation.business.standardfeatures.entities.uc68_heritagesubclass.entities.daofinder.Player68DAOFinderImpl;

/**
 * Player68BO
 *
* @see com.netfective.bluage.core.business.IBusinessObject
 */
@XmlRootElement

public class Player68BO
 extends AbstractBusinessObject {
	
	/**
	 * Default SUID for Player68BO.
	 */
	private static final long serialVersionUID = 1L;
	

	@Inject
	@Named ("com.bluage.documentation.business.standardfeatures.entities.uc68_heritagesubclass.entities.daofinder.Player68Finder") 
	private Player68DAOFinderImpl finder;
	/**
	 * Hibernate optimistic lock for class Player68BO.
	 */
	private int version;

						/**
	 * Entity property: dateOfBirth
	 *
	 */
private Date dateOfBirth;

					/**
	 * Entity property: estimatedValue
	 *
	 */
private Float estimatedValue;

					/**
	 * Entity property: weight
	 *
	 */
private Integer weight;

					/**
	 * Entity property: height
	 *
	 */
private Integer height;

					/**
	 * Entity property: id
	 *
	 */
private Long id;

					/**
	 * Entity property: firstName
	 *
	 */
private String firstName;

					/**
	 * Entity property: lastName
	 *
	 */
private String lastName;



   /**
	* public default constructor for class Player68BO
	*/
	public Player68BO(){
		//NOP Player68BO
	}

	
	/**
	 * Getter for version for class Player68BO
	 * @return int
	 */
	public int getVersion() {
		return this.version;
	}
	
	/**
	 * Setter for version for class Player68BO
	 * @param int version
	 */
	public void setVersion(final int version) {
		this.version = version;
	}

						/**
	 * Getter for dateOfBirth of class Player68BO.
	 *
	 * @return {@link java.util.Date}
	 * @generated
	 */ 
public  Date getDateOfBirth() {
	return this.dateOfBirth;
	}


	/**
	 * Setter for dateOfBirth of class Player68BO.
	 * @param value {@link java.util.Date}
	 * @generated
	 */

public  void setDateOfBirth(final Date dateOfBirth) {
	this.dateOfBirth = dateOfBirth;
}

					/**
	 * Getter for estimatedValue of class Player68BO.
	 *
	 * @return {@link java.lang.Float}
	 * @generated
	 */ 
public  Float getEstimatedValue() {
	return this.estimatedValue;
	}


	/**
	 * Setter for estimatedValue of class Player68BO.
	 * @param value {@link java.lang.Float}
	 * @generated
	 */

public  void setEstimatedValue(final Float estimatedValue) {
	this.estimatedValue = estimatedValue;
}

					/**
	 * Getter for weight of class Player68BO.
	 *
	 * @return {@link java.lang.Integer}
	 * @generated
	 */ 
public  Integer getWeight() {
	return this.weight;
	}


	/**
	 * Setter for weight of class Player68BO.
	 * @param value {@link java.lang.Integer}
	 * @generated
	 */

public  void setWeight(final Integer weight) {
	this.weight = weight;
}

					/**
	 * Getter for height of class Player68BO.
	 *
	 * @return {@link java.lang.Integer}
	 * @generated
	 */ 
public  Integer getHeight() {
	return this.height;
	}


	/**
	 * Setter for height of class Player68BO.
	 * @param value {@link java.lang.Integer}
	 * @generated
	 */

public  void setHeight(final Integer height) {
	this.height = height;
}

					/**
	 * Getter for id of class Player68BO.
	 *
	 * @return {@link java.lang.Long}
	 * @generated
	 */ 
public  Long getId() {
	return this.id;
	}


	/**
	 * Setter for id of class Player68BO.
	 * @param value {@link java.lang.Long}
	 * @generated
	 */

public  void setId(final Long id) {
	this.isEmpty = (id == null);
	this.id = id;
}

					/**
	 * Getter for firstName of class Player68BO.
	 *
	 * @return {@link java.lang.String}
	 * @generated
	 */ 
public  String getFirstName() {
	return this.firstName;
	}


	/**
	 * Setter for firstName of class Player68BO.
	 * @param value {@link java.lang.String}
	 * @generated
	 */

public  void setFirstName(final String firstName) {
	this.firstName = firstName;
}

					/**
	 * Getter for lastName of class Player68BO.
	 *
	 * @return {@link java.lang.String}
	 * @generated
	 */ 
public  String getLastName() {
	return this.lastName;
	}


	/**
	 * Setter for lastName of class Player68BO.
	 * @param value {@link java.lang.String}
	 * @generated
	 */

public  void setLastName(final String lastName) {
	this.lastName = lastName;
}


/**
	 * Returns <code>true</code> if the argument is an Player68 instance and all identifiers for this entity
     * equal the identifiers of the argument entity. Returns <code>false</code> otherwise.
	 * @param object  object to compare with current instance      
     * @return boolean true if equals false if not
     */
    public boolean equals(final Object object )
    {
		if ( this == object )
        {
            return true;
        }
        if ( !( object instanceof Player68BO ) )
        {
            return false;
        }
        final Player68BO that = (Player68BO)object;
    
    	            if (        				        			this.getId() == null
        	        ) {
        	return false;
        }
		return new EqualsBuilder()
			.append(this.getId(), that.getId())
			 	.appendSuper(super.equals(object))
		 	.isEquals();
	}
	/**
     * Returns a hash code based on this entity's identifiers.     * @return int
     */    
    public int hashCode() {
        return new HashCodeBuilder()
             .append(getId())
		            .toHashCode();
    }
}

