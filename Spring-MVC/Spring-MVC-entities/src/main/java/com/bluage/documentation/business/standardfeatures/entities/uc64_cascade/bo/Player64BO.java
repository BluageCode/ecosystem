/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.business.standardfeatures.entities.uc64_cascade.bo;

import javax.inject.Inject;
import javax.inject.Named;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import com.bluage.documentation.business.common.bos.PlayerBO;
import com.bluage.documentation.business.standardfeatures.entities.uc64_cascade.entities.daofinder.Player64DAOFinderImpl;

/**
 * Player64BO
 *
* @see com.netfective.bluage.core.business.IBusinessObject
* * @see PlayerBO
 */
@XmlRootElement

public class Player64BO
 extends PlayerBO {
	
	/**
	 * Default SUID for Player64BO.
	 */
	private static final long serialVersionUID = 1L;
	

	@Inject
	@Named ("com.bluage.documentation.business.standardfeatures.entities.uc64_cascade.entities.daofinder.Player64Finder") 
	private Player64DAOFinderImpl finder;

												/**
	 * Entity property: team
	 *
	 */
private Team64BO team;



   /**
	* public default constructor for class Player64BO
	*/
	public Player64BO(){
		//NOP Player64BO
	}


												/**
	 * Getter for team of class Player64BO.
	 *
	 * @return {@link com.bluage.documentation.business.standardfeatures.entities.uc64_cascade.bo.Team64BO}
	 * @generated
	 */ 
public  Team64BO getTeam() {
		Team64BO team0 = this.team;
		if(this.team == null) {
			if(DAO_MODE.equals(this.getObjMode( ) ) ) {
				team0 = null;
			} else {
				this.team = new Team64BO();
				this.team.setObjMode( this.getObjMode( ) );
				team0 = this.team;
			}
		} else {
			this.team.setObjMode( this.getObjMode( ) );
			if( HELPER_MODE.equals(this.getObjMode( ) ) ) {
				team0 = this.team;
			}
			if( DAO_MODE.equals(this.getObjMode()) && this.team.isEmpty ) {
				team0 = null;
			}
		}
		return team0;
	}


	/**
	 * Setter for team of class Player64BO.
	 * @param value {@link com.bluage.documentation.business.standardfeatures.entities.uc64_cascade.bo.Team64BO}
	 * @generated
	 */

public  void setTeam(final Team64BO team) {
	if(team != null) {
		team.isEmpty = false;
		team.setObjMode( this.getObjMode( ) );
	}
	this.isEmpty = false;
	this.team = team;
}


/**
	 * Returns <code>true</code> if the argument is an Player64 instance and all identifiers for this entity
     * equal the identifiers of the argument entity. The <code>equals</code> method of the parent entity
     * will also need to return <code>true</code>. Returns <code>false</code> otherwise.
     *
     * @see PlayerBO#hashCode()
	 * @param object  object to compare with current instance      
     * @return boolean true if equals false if not
     */
    public boolean equals(final Object object )
    {
		if ( this == object )
        {
            return true;
        }
        if ( !( object instanceof Player64BO ) )
        {
            return false;
        }
        final Player64BO that = (Player64BO)object;
    
    	            if (        				        			this.getId() == null
        	        ) {
        	return false;
        }
		return new EqualsBuilder()
			.append(this.getId(), that.getId())
			 	.isEquals();
	}
	/**

	 * Returns a hash code based on this entity's identifiers and the hash code of the parent entity.
     *
     * @see PlayerBO#hashCode()
     * @return int
     */    
    public int hashCode() {
        return new HashCodeBuilder()
             .append(getId())
				 	.appendSuper(super.hashCode())
            .toHashCode();
    }
}

