/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package   com.bluage.documentation.business.exceptions;

import org.springframework.dao.DataAccessException;


/**
 * This class represents the implementation of the generic search methods class.
 *
 */
public class ConstraintViolationException 
		extends DataAccessException {

	/**
	 * SUID
	 */
	private static final long serialVersionUID = -4438953601462284080L;


	/**
	 * 
	 * 
	 * @param msg	A message
	 */
	public ConstraintViolationException( String msg ) {
		super( msg );
	}

	/**
	 * 
	 * 
	 * @param msg	A message
	 * @param ex	An exception
	 */
	public ConstraintViolationException( String msg, Throwable ex ) {
		super( msg, ex );
	}
} 
