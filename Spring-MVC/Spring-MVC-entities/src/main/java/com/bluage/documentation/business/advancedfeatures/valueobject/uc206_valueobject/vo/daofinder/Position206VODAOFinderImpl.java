/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.business.advancedfeatures.valueobject.uc206_valueobject.vo.daofinder;


import javax.inject.Named;

import com.bluage.documentation.business.AbstractVODaoFinderImpl;
import com.bluage.documentation.business.advancedfeatures.valueobject.uc206_valueobject.bos.Position206BO;
import com.bluage.documentation.business.advancedfeatures.valueobject.uc206_valueobject.vo.Position206VO;
/**
 * This class represents the implementation of the search methods for the vo reflecting the entity Position206.
 * 
 * @see AbstractVODaoFinderImpl
 */

@Named ("com.bluage.documentation.business.advancedfeatures.valueobject.uc206_valueobject.vo.daofinder.Position206VODAOFinderImpl") 
public class Position206VODAOFinderImpl extends AbstractVODaoFinderImpl<Position206VO, Position206BO> {

	/**
	 * The associated BO
	 */
	private Class<Position206BO> refBo = Position206BO.class;
	
	/**
	 * The corresponding VO
	 */
	private Class<Position206VO> refVo = Position206VO.class;


	/**
	 * Returns the associated BO
	 * 
	 * @return Class<Position206BO>
	 */
	@Override
	public final Class<Position206BO> getRefBo() {
		return this.refBo;
	}

	/**
	* Default order
	*/

	private String defaultOrder =" order by ";
			/**
	 * Returns the default order by instruction
	 * 
	 * @return String
	 */
	@Override
	public final String getDefaultOrder() {
		return this.defaultOrder;
	}

	
	/**
	 * Returns the associated VO
	 * 
	 * @return Class
	*/
	@Override
	public Class getRefVo() {
		return refVo;
	}
}
