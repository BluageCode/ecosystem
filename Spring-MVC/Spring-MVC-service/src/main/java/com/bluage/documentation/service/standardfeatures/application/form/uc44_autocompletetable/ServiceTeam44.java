/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.service.standardfeatures.application.form.uc44_autocompletetable;


import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import com.bluage.documentation.business.standardfeatures.application.form.uc44_autocompletetable.bos.Team44BO;
import com.bluage.documentation.business.standardfeatures.application.form.uc44_autocompletetable.entities.daofinder.Team44DAOFinderImpl;
import com.bluage.documentation.service.AbstractService;
import com.bluage.documentation.service.standardfeatures.application.form.uc44_autocompletetable.daofinder.ServiceTeam44DAOMethodFinderImpl;
import com.netfective.bluage.core.exception.ApplicationException;

/**
 * Defines  application services for ServiceTeam44
  */
@Named("com.bluage.documentation.service.standardfeatures.application.form.uc44_autocompletetable.ServiceTeam44")
public class ServiceTeam44  extends AbstractService{


						@Inject
		@Named ("com.bluage.documentation.business.standardfeatures.application.form.uc44_autocompletetable.entities.daofinder.Team44DAOFinderImpl")
		private Team44DAOFinderImpl team44BOFinder;
		@Inject
		@Named ("com.bluage.documentation.service.standardfeatures.application.form.uc44_autocompletetable.daofinder.ServiceTeam44DAOMethodFinder")
		private ServiceTeam44DAOMethodFinderImpl serviceTeam44Dao;



	/**
	 *  Default constructor 
	 */
	public ServiceTeam44() {
		super(); // Call super for default constructor : ServiceTeam44
	}
	


			
				
	/**    
	 * Operation Crud
	 * @param bo {@link Team44BO}
		 * @return {@link Team44BO}
	 */
	public Team44BO team44FindByID(Team44BO bo )
 throws ApplicationException {

					return (Team44BO)team44BOFinder.findByPrimaryKey(getBoId(bo));
	}
	/**
	 *  Return the primary key from business object.
	 */
	private Object getBoId(Team44BO bo) {
				return bo.getId();
		}

		
								
	/**    
	 * Operation Crud
	 * @return {@link List}
	 */
	public List team44FindAll( )
 throws ApplicationException {

					return team44BOFinder.findAll();
	}

		
			
	/**    
	 * Process operation.
	 * @return {@link Team44BO}
	 */
	public Team44BO initTeam44( )
 throws ApplicationException {

				return privateInitTeam44();
	}
	

	/**    
	 * Process operation.
	 * @return {@link Team44BO}
	 */
	private Team44BO privateInitTeam44( )
 throws ApplicationException {
				// Return Type declaration.
				Team44BO team = new Team44BO();
					// Central buffer node declaration for operation initTeam44
			
						return team;
		}

		
			
	/**    
	 * Operation HQL.
	 * @param name {@link String}
		 * @return {@link Team44BO}
	 */
	public Team44BO searchTeam44(String name )
 throws ApplicationException {
	
	return serviceTeam44Dao.searchTeam44(name);
	}

		}
