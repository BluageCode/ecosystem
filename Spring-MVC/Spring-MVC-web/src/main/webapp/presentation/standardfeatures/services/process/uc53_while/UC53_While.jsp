<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file="/include.jsp"  %>
<html >
<head >
<title  > 
<fmt:message key="cUC53_While_BLU_AGE_DOCUMENTATION"/>
</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"  > 
</meta>
<link href="<c:url value= "/presentation/_style/_style.css"/>" rel="stylesheet" type="text/css"  /> 
<link href="<c:url value= "/presentation/_include/jquery/rd_lib.css"/>" rel="stylesheet" type="text/css"  /> 
<script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/jquery.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/rd_setup.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/rd_lib.js"/>" space="preserve"  ></script><link rel="shortcut icon" href="<c:url value= "/presentation/_style/favicon.ico"/>"  /> 

</head>

<body class=" " > 
<div id="rd_ghost"  > 
<small  > 
<fmt:message key="cUC53_While___CSS_DEACTIVATED"/>
</small>
</div>
<div id="rd_jghost"  > 
<small  > 
<fmt:message key="cUC53_While___JAVASCRIPT_DEACTIVATED"/>
</small>
</div>
<div id="shape"  > 
<div class="ui-layout-center"  > 
<div class="east-north"  > 
	<form:form id="uC53_While" commandName="uC53_WhileForm"  enctype="application/x-www-form-urlencoded" method="post">
<h2 class="rd_first"  > 
<fmt:message key="cUC53_While_STANDARD_FEATURES__SERVICES_"/>
</h2>
<h4  > 
<fmt:message key="cUC53_While_PLAYER_TO_TRAIN"/>
</h4>
<a id="lnk_showPlayer"onclick="javascript:btn_click_cExecuteUC53_While(this.href);return false;"				href="<c:url value="/presentation/standardfeatures/services/process/uc53_while/UC53_While/lnk_showPlayer.html"/>"
							 shape="rect"  >									 
<fmt:message key="cUC53_While_SHOW_PLAYER"/>
</a>
<br clear="none"  /> 
<br clear="none"  /> 
<table class="rd_tabledata"  >
<tr  > 
<th rowspan="1" colspan="1"  > 
<fmt:message key="cUC53_While_LAST_NAME"/>
</th>
<th rowspan="1" colspan="1"  > 
<fmt:message key="cUC53_While_FIRST_NAME"/>
</th>
<th rowspan="1" colspan="1"  > 
<fmt:message key="cUC53_While_ROOKIE"/>
</th>
<th rowspan="1" colspan="1"  > 
<fmt:message key="cUC53_While_NUMBER_OF_MATCHES_PLAYED"/>
</th>
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
		<span id="txt_lastname1">
	<c:out value="${uC53_WhileForm.player.lastName}" />
		</span>
	</td> 
<td rowspan="1" colspan="1"  >
		<span id="txt_firstname1">
	<c:out value="${uC53_WhileForm.player.firstName}" />
		</span>
	</td> 
<td rowspan="1" colspan="1"  >
<c:if test="${uC53_WhileForm.player.rookie!=null}"> 
<form:checkbox id="chk_rookie" path="player.rookie"  name="isRookie" disabled="true"  
		/>
</c:if>
</td> 
<td rowspan="1" colspan="1"  >
		<span id="txt_matches1">
	<c:out value="${uC53_WhileForm.player.matches}" />
		</span>
	</td> 
</tr>
</table> 
<br clear="none"  /> 
<br clear="none"  /> 
<a id="lnk_trainPlayer"onclick="javascript:btn_click_cExecuteUC53_While(this.href);return false;"				href="<c:url value="/presentation/standardfeatures/services/process/uc53_while/UC53_While/lnk_trainPlayer.html"/>"
							 shape="rect"  >									 
<fmt:message key="cUC53_While_TRAIN_PLAYER"/>
</a>
	</form:form>
	<script language="javascript">
		function btn_click_cExecuteUC53_While(url) {
			UC53_Whilevar = document.getElementById('uC53_While');
			UC53_Whilevar.setAttribute('action', url);
			UC53_Whilevar.submit();
		}
		function btn_click_cSetActionUC53_While(url) {
			UC53_Whilevar = document.getElementById('uC53_While');
			UC53_Whilevar.setAttribute('action', url);
		}
	</script>
</div>
<div class="east-center"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/services/process/uc53_while/UC53_Whilecmp_tabs" />
</div>
</div>
<div class="ui-layout-north"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/services/process/uc53_while/UC53_Whilecmp_header" />
</div>
<div class="ui-layout-south"  > 
<fmt:message key="cUC53_While________"/>
<span id="rd_jversion"  > 
<fmt:message key="cUC53_While_BLU_AGE_DOCUMENTATION_214"/>
</span>
</div>
<div class="ui-layout-west"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/services/process/uc53_while/UC53_Whilecmp_treeview" />
</div>
</div>
</body>
</html>
