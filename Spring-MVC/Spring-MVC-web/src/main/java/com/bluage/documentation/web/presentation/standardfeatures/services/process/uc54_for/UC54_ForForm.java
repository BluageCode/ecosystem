/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.services.process.uc54_for;


import java.io.Serializable;
import java.util.List;

import com.bluage.documentation.business.standardfeatures.services.process.uc54_for.bos.Player54BO;
import com.netfective.bluage.core.web.AbstractForm;
import com.netfective.bluage.core.web.IForm;

/**
* Class : UC54_ForForm
*/
public class UC54_ForForm extends AbstractForm implements Serializable, IForm {
	
	/** Constant : serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	//CONSTANT : teamPlayers
	private static final String TEAM_PLAYERS = "teamPlayers";
	//CONSTANT : players
	private static final String PLAYERS = "players";
	/**
	 * 	Property: teamPlayers 
	 */
	private List<Player54BO> teamPlayers;
	/**
	 * 	Property: players 
	 */
	private List<Player54BO> players;
/**
	 * Default constructor : UC54_ForForm
	 */
	public UC54_ForForm() {
		super();
		// Initialize : teamPlayers
		this.teamPlayers = new java.util.ArrayList<com.bluage.documentation.business.standardfeatures.services.process.uc54_for.bos.Player54BO>();
		// Initialize : players
		this.players = new java.util.ArrayList<com.bluage.documentation.business.standardfeatures.services.process.uc54_for.bos.Player54BO>();
							this.setAlwaysCallPreControllers(true);
	}
	
	/**
	 * 	Getter : teamPlayers 
	 * 	@return : Return the teamPlayers instance.
	 */
	public List<Player54BO> getTeamPlayers(){
		return teamPlayers; // For UC54_ForForm
	}
	
	/**
	 * 	Setter : teamPlayers 
	 *  @param teamPlayersinstance : The instance to set.
	 */
	public void setTeamPlayers(final List<Player54BO> teamPlayersinstance){
		this.teamPlayers = teamPlayersinstance;// For UC54_ForForm
	}
	/**
	 * 	Getter : players 
	 * 	@return : Return the players instance.
	 */
	public List<Player54BO> getPlayers(){
		return players; // For UC54_ForForm
	}
	
	/**
	 * 	Setter : players 
	 *  @param playersinstance : The instance to set.
	 */
	public void setPlayers(final List<Player54BO> playersinstance){
		this.players = playersinstance;// For UC54_ForForm
	}
/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UC54_ForForm [ "+
TEAM_PLAYERS +" = " + teamPlayers +PLAYERS +" = " + players + "]";
	}
	
	/**
	 * Method setInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public void setInstanceByInstanceName(final String instanceName, final Object instance) {
		// Set the instance TeamPlayers.
		if(TEAM_PLAYERS.equals(instanceName)){// For UC54_ForForm
			this.setTeamPlayers((List<Player54BO>)instance); // For UC54_ForForm
		}
				// Set the instance Players.
		if(PLAYERS.equals(instanceName)){// For UC54_ForForm
			this.setPlayers((List<Player54BO>)instance); // For UC54_ForForm
		}
			}
	
	/**
	 * Method getInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public Object getInstanceByInstanceName(final String instanceName){
		// Temporary object for tmpUC54_ForForm.
		Object tmpUC54_ForForm = null;
		
			
		// Get the instance TeamPlayers for UC54_ForForm.
		if(TEAM_PLAYERS.equals(instanceName)){ // For UC54_ForForm
			tmpUC54_ForForm = this.getTeamPlayers(); // For UC54_ForForm
		}
			
		// Get the instance Players for UC54_ForForm.
		if(PLAYERS.equals(instanceName)){ // For UC54_ForForm
			tmpUC54_ForForm = this.getPlayers(); // For UC54_ForForm
		}
		return tmpUC54_ForForm;// For UC54_ForForm
	}
	
					}
