/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.services.ajax.uc50_ajaxcall ;


import org.apache.log4j.Logger;
import org.springframework.validation.Errors;

import com.netfective.bluage.core.validator.AbstractValidator;
import com.netfective.bluage.core.validator.ValidatorsUtil;
/**
* Class :UC50_AjaxCallValidator
*/
public class UC50_AjaxCallValidator extends AbstractValidator{
	
	// LOGGER for the class UC50_AjaxCallValidator
	private static final Logger LOGGER = Logger.getLogger( UC50_AjaxCallValidator.class);
	
	
	/**
	* Operation validate for UC50_AjaxCallForm
	* @param obj : the current form (UC50_AjaxCallForm)
	* @param errors : The spring errors to return for the form UC50_AjaxCallForm
	*/
	public void validate(final Object obj,final Errors errors){
		LOGGER.info("Beginning method : validate"); // UC50_AjaxCallValidator
		UC50_AjaxCallForm cUC50_AjaxCallForm = (UC50_AjaxCallForm)obj; // UC50_AjaxCallValidator
		
		if(LOGGER.isDebugEnabled()){
			LOGGER.debug("validation of the 'minimumValue.estimatedValue' field with the FormatValidator validator");
		}
		
		if(ValidatorsUtil.formatValidator(cUC50_AjaxCallForm.getMinimumValue().getEstimatedValue(), "", "")== 0 ){
			errors.rejectValue("minimumValue.estimatedValue", "","Please enter a correct integer value");
		} 
				
		LOGGER.info("Ending method : validate the form "+ cUC50_AjaxCallForm.getClass().getName()); // UC50_AjaxCallValidator
	}

	/**
	* Method to implements to use spring validators (UC50_AjaxCallForm)
	* @param aClass : Class for the form UC50_AjaxCallForm
	* @return boolean
	*/
	public boolean supports(final Class aClass){
		return (new UC50_AjaxCallForm()).getClass().equals(aClass); // UC50_AjaxCallValidator
	}
}
