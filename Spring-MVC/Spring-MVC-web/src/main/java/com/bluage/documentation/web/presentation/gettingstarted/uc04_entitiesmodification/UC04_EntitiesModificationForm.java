/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.gettingstarted.uc04_entitiesmodification;


import java.io.Serializable;
import java.util.List;

import com.bluage.documentation.business.gettingstarted.uc04_entitiesmodification.bos.Player04BO;
import com.netfective.bluage.core.web.AbstractForm;
import com.netfective.bluage.core.web.IForm;

/**
* Class : UC04_EntitiesModificationForm
*/
public class UC04_EntitiesModificationForm extends AbstractForm implements Serializable, IForm {
	
	/** Constant : serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	//CONSTANT : allPlayers
	private static final String ALL_PLAYERS = "allPlayers";
	//CONSTANT : playerToUpdate
	private static final String PLAYER_TO_UPDATE = "playerToUpdate";
	/**
	 * 	Property: allPlayers 
	 */
	private List<Player04BO> allPlayers;
	/**
	 * 	Property: playerToUpdate 
	 */
	private Player04BO playerToUpdate;
/**
	 * Default constructor : UC04_EntitiesModificationForm
	 */
	public UC04_EntitiesModificationForm() {
		super();
		// Initialize : allPlayers
		this.allPlayers = new java.util.ArrayList<com.bluage.documentation.business.gettingstarted.uc04_entitiesmodification.bos.Player04BO>();
		// Initialize : playerToUpdate
		this.playerToUpdate = null;
					this.setAlwaysCallPreControllers(true);
	}
	
	/**
	 * 	Getter : allPlayers 
	 * 	@return : Return the allPlayers instance.
	 */
	public List<Player04BO> getAllPlayers(){
		return allPlayers; // For UC04_EntitiesModificationForm
	}
	
	/**
	 * 	Setter : allPlayers 
	 *  @param allPlayersinstance : The instance to set.
	 */
	public void setAllPlayers(final List<Player04BO> allPlayersinstance){
		this.allPlayers = allPlayersinstance;// For UC04_EntitiesModificationForm
	}
	/**
	 * 	Getter : playerToUpdate 
	 * 	@return : Return the playerToUpdate instance.
	 */
	public Player04BO getPlayerToUpdate(){
		return playerToUpdate; // For UC04_EntitiesModificationForm
	}
	
	/**
	 * 	Setter : playerToUpdate 
	 *  @param playerToUpdateinstance : The instance to set.
	 */
	public void setPlayerToUpdate(final Player04BO playerToUpdateinstance){
		this.playerToUpdate = playerToUpdateinstance;// For UC04_EntitiesModificationForm
	}
/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UC04_EntitiesModificationForm [ "+
ALL_PLAYERS +" = " + allPlayers +PLAYER_TO_UPDATE +" = " + playerToUpdate + "]";
	}
	
	/**
	 * Method setInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public void setInstanceByInstanceName(final String instanceName, final Object instance) {
		// Set the instance AllPlayers.
		if(ALL_PLAYERS.equals(instanceName)){// For UC04_EntitiesModificationForm
			this.setAllPlayers((List<Player04BO>)instance); // For UC04_EntitiesModificationForm
		}
				// Set the instance PlayerToUpdate.
		if(PLAYER_TO_UPDATE.equals(instanceName)){// For UC04_EntitiesModificationForm
			this.setPlayerToUpdate((Player04BO)instance); // For UC04_EntitiesModificationForm
		}
			}
	
	/**
	 * Method getInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public Object getInstanceByInstanceName(final String instanceName){
		// Temporary object for tmpUC04_EntitiesModificationForm.
		Object tmpUC04_EntitiesModificationForm = null;
		
			
		// Get the instance AllPlayers for UC04_EntitiesModificationForm.
		if(ALL_PLAYERS.equals(instanceName)){ // For UC04_EntitiesModificationForm
			tmpUC04_EntitiesModificationForm = this.getAllPlayers(); // For UC04_EntitiesModificationForm
		}
			
		// Get the instance PlayerToUpdate for UC04_EntitiesModificationForm.
		if(PLAYER_TO_UPDATE.equals(instanceName)){ // For UC04_EntitiesModificationForm
			tmpUC04_EntitiesModificationForm = this.getPlayerToUpdate(); // For UC04_EntitiesModificationForm
		}
		return tmpUC04_EntitiesModificationForm;// For UC04_EntitiesModificationForm
	}
	
			}
