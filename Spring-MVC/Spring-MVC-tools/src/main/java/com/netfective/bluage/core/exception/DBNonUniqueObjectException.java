/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.netfective.bluage.core.exception;
/**
 * @author NETFECTIVE TECHNOLOGY
 */
public class DBNonUniqueObjectException extends TechnicalException {

	/**
	 * serial UID
	 */
	private static final long serialVersionUID = -5194511226181721502L;
	
	/**
	 * DBNonUniqueObjectException constructor with Exception parameter
	 * @param ex Exception
	 */
	public DBNonUniqueObjectException(final Exception ex){
		super(ex);
	}
	
	
	/**
	 * DBNonUniqueObjectException constructor with Message parameter
	 * @param msg String
	 */
	public DBNonUniqueObjectException(final String msg){
		super(msg);
	}
	
	
	/**
	 * DBNonUniqueObjectException constructor with Message and Throwable
	 * @param String msg
	 * @param ex Throwable
	 */
	public DBNonUniqueObjectException(final String msg, final Throwable ex) {
		super(msg, ex);
	}
}
