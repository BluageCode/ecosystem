/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.netfective.bluage.core.exception;

/**
 * @author NETFECTIVE TECHNOLOGY
 */
public class ValidationException extends Exception {
	
	/**
	 * serial UID 
	 */
	private static final long serialVersionUID = -7389243631109735283L;
	/**
	 * className field String
	 */
	private final String className;
	
	/**
	 * methodName field String
	 */
	private final String methodName;
	
	/**
	 * innerException field Exception
	 */
	private final Exception innerException;
	
	/**
	 * Constructeur par defaut de ValidationException.
	 */
	public ValidationException() {
		super();
		className = "";
		methodName = "";
		innerException = null;
	}
	
	//	Les constructeurs qui ne prennent pas comme parametres le nom de
	// la classe et de la methode ou l exception s est produite.
	
	/**
	 * Constructeur de ValidationException avec message d exception
	 * @param mess Le message de l exception.
	 */
	public ValidationException(final String mess) {
		super(mess);
		className = "";
		methodName = "";
		innerException = null;
	}
	
	/**
	 * Constructeur de ValidationException avec exception imbriquee
	 * @param innerException L exception imbriquee.
	 */
	public ValidationException(final Exception innerException) {
		super();
		this.innerException = innerException;
		className = "";
		methodName = "";
	}
	
	/**
	 * Constructeur de ValidationException avec message d exception et exception imbriquee..
	 * @param mess Le message de l exception.
	 * @param innerException  L exception imbriquee.
	 */
	public ValidationException(final String mess, final Exception innerException) {
		super(mess);
		this.innerException = innerException;
		className = "";
		methodName = "";
	}
	
	//	Les constructeurs qui prennent comme parametres le nom de
	// la classe et de la methode ou l exception s est produite.
	// ils sont utilises dans le cas ou on utilise une api qui n a pas d exception dans la signature 
	// et qui remonte un RuntimeException, dans ce cas on n est pas sur
	// d avoir la classe et la methode dans la pile. 
	
	/**
	 * Constructeur de ValidationException sans message d exception.
	 * @param className le nom de la classe ou l exception s est produite.
	 * @param methodName le nom de la methode ou l exception s est produite.
	 */
	public ValidationException(final String className, final String methodName) {
		super();
		this.className = className;
		this.methodName = methodName;
		innerException = null;
	}
	
	/**
	 * Constructeur de ValidationException avec message d exception
	 * @param mess Le message de l exception.
	 * @param className le nom de la classe ou l exception s est produite.
	 * @param methodName le nom de la methode ou l exception s est produite.
	 */
	public ValidationException(final String mess,final String className, final String methodName) {
		super(mess);
		this.className = className;
		this.methodName = methodName;
		innerException = null;
	}
	
	/**
	 * Constructeur de ValidationException sans message d exception.
	 * @param className le nom de la classe ou l exception s est produite.
	 * @param methodName le nom de la methode ou l exception s est produite.
	 * @param innerException L exception imbriquee.
	 */
	public ValidationException(final String className, final String methodName, final Exception innerException) {
		super();
		this.className = className;
		this.methodName = methodName;
		this.innerException = innerException;
	}
	
	/**
	 * Constructeur de ValidationException avec message d exception et exception imbriquee..
	 * @param mess Le message de l exception
	 * @param className le nom de la classe ou l exception s est produite.
	 * @param methodName le nom de la methode ou l exception s est produite.
	 * @param innerException L exception imbriquee.
	 */
	public ValidationException(final String mess,final String className, final String methodName, final Exception innerException) {
		super(mess);
		this.className = className;
		this.methodName = methodName;
		this.innerException = innerException;
	}

	/**
	 * className getter for ValidationException
	 * @return className
	 */
	public String getClassName() {
		return className;
	}

	/**
	 * innerException getter for ValidationException
	 * @return innerException
	 */
	public Exception getInnerException() {
		return innerException;
	}

	/**
	 * methodName getter for ValidationException
	 * @return methodName
	 */
	public String getMethodName() {
		return methodName;
	}
}
