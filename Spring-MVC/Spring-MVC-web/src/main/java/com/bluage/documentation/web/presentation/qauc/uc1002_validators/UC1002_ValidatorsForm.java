/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.qauc.uc1002_validators;


import java.io.Serializable;

import com.bluage.documentation.business.qauc.uc1002_validators.bos.Player1002BO;
import com.netfective.bluage.core.web.AbstractForm;
import com.netfective.bluage.core.web.IForm;

/**
* Class : UC1002_ValidatorsForm
*/
public class UC1002_ValidatorsForm extends AbstractForm implements Serializable, IForm {
	
	/** Constant : serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	//CONSTANT : playerToCreate
	private static final String PLAYER_TO_CREATE = "playerToCreate";
	/**
	 * 	Property: playerToCreate 
	 */
	private Player1002BO playerToCreate;
/**
	 * Default constructor : UC1002_ValidatorsForm
	 */
	public UC1002_ValidatorsForm() {
		super();
		// Initialize : playerToCreate
		this.playerToCreate = new Player1002BO();
			this.setAlwaysCallPreControllers(true);
	}
	
	/**
	 * 	Getter : playerToCreate 
	 * 	@return : Return the playerToCreate instance.
	 */
	public Player1002BO getPlayerToCreate(){
		return playerToCreate; // For UC1002_ValidatorsForm
	}
	
	/**
	 * 	Setter : playerToCreate 
	 *  @param playerToCreateinstance : The instance to set.
	 */
	public void setPlayerToCreate(final Player1002BO playerToCreateinstance){
		this.playerToCreate = playerToCreateinstance;// For UC1002_ValidatorsForm
	}
/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UC1002_ValidatorsForm [ "+
PLAYER_TO_CREATE +" = " + playerToCreate + "]";
	}
	
	/**
	 * Method setInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public void setInstanceByInstanceName(final String instanceName, final Object instance) {
		// Set the instance PlayerToCreate.
		if(PLAYER_TO_CREATE.equals(instanceName)){// For UC1002_ValidatorsForm
			this.setPlayerToCreate((Player1002BO)instance); // For UC1002_ValidatorsForm
		}
			}
	
	/**
	 * Method getInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public Object getInstanceByInstanceName(final String instanceName){
		// Temporary object for tmpUC1002_ValidatorsForm.
		Object tmpUC1002_ValidatorsForm = null;
		
			
		// Get the instance PlayerToCreate for UC1002_ValidatorsForm.
		if(PLAYER_TO_CREATE.equals(instanceName)){ // For UC1002_ValidatorsForm
			tmpUC1002_ValidatorsForm = this.getPlayerToCreate(); // For UC1002_ValidatorsForm
		}
		return tmpUC1002_ValidatorsForm;// For UC1002_ValidatorsForm
	}
	
	}
