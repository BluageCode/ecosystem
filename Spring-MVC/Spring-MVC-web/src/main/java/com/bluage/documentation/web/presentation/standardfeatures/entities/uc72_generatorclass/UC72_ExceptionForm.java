/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.entities.uc72_generatorclass;


import java.io.Serializable;
import java.util.List;

import com.netfective.bluage.core.web.AbstractForm;
import com.netfective.bluage.core.web.IForm;

/**
* Class : UC72_ExceptionForm
*/
public class UC72_ExceptionForm extends AbstractForm implements Serializable, IForm {
	
	/** Constant : serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	//CONSTANT : listPlayers
	private static final String LIST_PLAYERS = "listPlayers";
	/**
	 * 	Property: listPlayers 
	 */
	private List listPlayers;
/**
	 * Default constructor : UC72_ExceptionForm
	 */
	public UC72_ExceptionForm() {
		super();
		// Initialize : listPlayers
		this.listPlayers = null;
			this.setAlwaysCallPreControllers(true);
	}
	
	/**
	 * 	Getter : listPlayers 
	 * 	@return : Return the listPlayers instance.
	 */
	public List getListPlayers(){
		return listPlayers; // For UC72_ExceptionForm
	}
	
	/**
	 * 	Setter : listPlayers 
	 *  @param listPlayersinstance : The instance to set.
	 */
	public void setListPlayers(final List listPlayersinstance){
		this.listPlayers = listPlayersinstance;// For UC72_ExceptionForm
	}
/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UC72_ExceptionForm [ "+
LIST_PLAYERS +" = " + listPlayers + "]";
	}
	
	/**
	 * Method setInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public void setInstanceByInstanceName(final String instanceName, final Object instance) {
		// Set the instance ListPlayers.
		if(LIST_PLAYERS.equals(instanceName)){// For UC72_ExceptionForm
			this.setListPlayers((List)instance); // For UC72_ExceptionForm
		}
			}
	
	/**
	 * Method getInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public Object getInstanceByInstanceName(final String instanceName){
		// Temporary object for tmpUC72_ExceptionForm.
		Object tmpUC72_ExceptionForm = null;
		
			
		// Get the instance ListPlayers for UC72_ExceptionForm.
		if(LIST_PLAYERS.equals(instanceName)){ // For UC72_ExceptionForm
			tmpUC72_ExceptionForm = this.getListPlayers(); // For UC72_ExceptionForm
		}
		return tmpUC72_ExceptionForm;// For UC72_ExceptionForm
	}
	
	}
