/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.application.navigation.uc28_packagescope.package_two ;


import org.apache.log4j.Logger;
import org.springframework.validation.Errors;

import com.netfective.bluage.core.validator.AbstractValidator;
/**
* Class :UC28_StatesValidator
*/
public class UC28_StatesValidator extends AbstractValidator{
	
	// LOGGER for the class UC28_StatesValidator
	private static final Logger LOGGER = Logger.getLogger( UC28_StatesValidator.class);
	
	
	/**
	* Operation validate for UC28_StatesForm
	* @param obj : the current form (UC28_StatesForm)
	* @param errors : The spring errors to return for the form UC28_StatesForm
	*/
	public void validate(final Object obj,final Errors errors){
		LOGGER.info("Beginning method : validate"); // UC28_StatesValidator
		UC28_StatesForm cUC28_StatesForm = (UC28_StatesForm)obj; // UC28_StatesValidator
		LOGGER.info("Ending method : validate the form "+ cUC28_StatesForm.getClass().getName()); // UC28_StatesValidator
	}

	/**
	* Method to implements to use spring validators (UC28_StatesForm)
	* @param aClass : Class for the form UC28_StatesForm
	* @return boolean
	*/
	public boolean supports(final Class aClass){
		return (new UC28_StatesForm()).getClass().equals(aClass); // UC28_StatesValidator
	}
}
