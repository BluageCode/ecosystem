/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.business.standardfeatures.services.hql.uc57_hqloperation.bos;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.inject.Inject;
import javax.inject.Named;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.LazyInitializationException;

import com.bluage.documentation.business.common.bos.TeamBO;
import com.bluage.documentation.business.standardfeatures.services.hql.uc57_hqloperation.entities.daofinder.Team57DAOFinderImpl;
import com.netfective.bluage.core.exception.ApplicationException;

/**
 * Team57BO
 *
* @see com.netfective.bluage.core.business.IBusinessObject
* * @see TeamBO
 */
@XmlRootElement

public class Team57BO
 extends TeamBO {
	
	/**
	 * Default SUID for Team57BO.
	 */
	private static final long serialVersionUID = 1L;
	

	@Inject
	@Named ("com.bluage.documentation.business.standardfeatures.services.hql.uc57_hqloperation.entities.daofinder.Team57Finder") 
	private Team57DAOFinderImpl finder;

						/**
	 * Entity property: budget
	 *
	 */
private Integer budget;

											/**
	 * Entity property: state
	 *
	 */
private State57BO state;

																		/**
	 * Entity property: players
	 *
	 */
private Set<Player57BO> players = new HashSet<Player57BO>();



   /**
	* public default constructor for class Team57BO
	*/
	public Team57BO(){
		//NOP Team57BO
	}


						/**
	 * Getter for budget of class Team57BO.
	 *
	 * @return {@link java.lang.Integer}
	 * @generated
	 */ 
public  Integer getBudget() {
	return this.budget;
	}


	/**
	 * Setter for budget of class Team57BO.
	 * @param value {@link java.lang.Integer}
	 * @generated
	 */

public  void setBudget(final Integer budget) {
	this.budget = budget;
}

											/**
	 * Getter for state of class Team57BO.
	 *
	 * @return {@link com.bluage.documentation.business.standardfeatures.services.hql.uc57_hqloperation.bos.State57BO}
	 * @generated
	 */ 
public  State57BO getState() {
		State57BO state0 = this.state;
		if(this.state == null) {
			if(DAO_MODE.equals(this.getObjMode( ) ) ) {
				state0 = null;
			} else {
				this.state = new State57BO();
				this.state.setObjMode( this.getObjMode( ) );
				state0 = this.state;
			}
		} else {
			this.state.setObjMode( this.getObjMode( ) );
			if( HELPER_MODE.equals(this.getObjMode( ) ) ) {
				state0 = this.state;
			}
			if( DAO_MODE.equals(this.getObjMode()) && this.state.isEmpty ) {
				state0 = null;
			}
		}
		return state0;
	}


	/**
	 * Setter for state of class Team57BO.
	 * @param value {@link com.bluage.documentation.business.standardfeatures.services.hql.uc57_hqloperation.bos.State57BO}
	 * @generated
	 */

public  void setState(final State57BO state) {
	if(state != null) {
		state.isEmpty = false;
		state.setObjMode( this.getObjMode( ) );
	}
	this.isEmpty = false;
	this.state = state;
}

																		/**
	 * Getter for players of class Team57BO.
	 *
	 * @return {@link java.util.Set<com.bluage.documentation.business.standardfeatures.services.hql.uc57_hqloperation.bos.Player57BO>}
	 * @generated
	 */ 
public  Set<Player57BO> getPlayers() {
	return this.players;
			}


	/**
	 * Setter for players of class Team57BO.
	 * @param value {@link java.util.Set<com.bluage.documentation.business.standardfeatures.services.hql.uc57_hqloperation.bos.Player57BO>}
	 * @generated
	 */

public  void setPlayers(final Set<Player57BO> players) {
	this.players = players;
}

	/**
	 * Remove an entity from the com.bluage.documentation.business.standardfeatures.services.hql.uc57_hqloperation.bos.Player57BO collection.
	 *
	 * @param entity
	 *		: {@link Set<Player57BO>} the entity
	 * @throws ApplicationException
     */
public void removeFromPlayers( Player57BO entity ) throws ApplicationException {
	try {
		if( getPlayers() != null && getPlayers().contains(entity) ) {
			getPlayers().remove( entity );
		}
	} catch ( LazyInitializationException e ) {
				Team57BO bo = ( Team57BO ) finder.findByPrimaryKey( getId() );
			finder.getHibernateTemplate().initialize( bo.getPlayers() );
			setPlayers( bo.getPlayers() );
			// evict object from session to avoid NonUniqueObjectException
			finder.getHibernateTemplate().evict( bo );
		
			if( getPlayers() != null && getPlayers().contains( entity ) ) {
				getPlayers().remove( entity );
			}
		}
	}
	
	/**
	 * Add entity to the com.bluage.documentation.business.standardfeatures.services.hql.uc57_hqloperation.bos.Player57BO collection.
	 *
	 * @param entity
	 *		{@link Player57BO} the entity
	 * @throws ApplicationException
     */
public void addToPlayers( Player57BO entity ) throws ApplicationException
	{
		try {
			if( getPlayers() != null && !getPlayers().contains( entity ) ) {
				getPlayers().add( entity );
			}
		} catch ( LazyInitializationException e ) {
							Team57BO bo = ( Team57BO ) finder.findByPrimaryKey( getId() );
		finder.getHibernateTemplate().initialize( bo.getPlayers() );
		setPlayers( bo.getPlayers() );
		// evict object from session to avoid NonUniqueObjectException
		finder.getHibernateTemplate().evict( bo );
		if( getPlayers() != null && !getPlayers().contains( entity ) ) {
			getPlayers().add( entity );
		}
	}
	}
	
	/**
	 * Add entitys to the com.bluage.documentation.business.standardfeatures.services.hql.uc57_hqloperation.bos.Player57BO collection
	 *
	 * @param entities			
	 *		: {@link java.util.List} A list of entities
	 * @throws ApplicationException 
     */	
public void addAllToPlayers( Collection<Player57BO> entities ) throws ApplicationException {
	for( Player57BO entity : entities) {
		if( getPlayers()!=null ) {
			addToPlayers( entity );
		}
	}
}
	
	/**
	 * Remove entities form the com.bluage.documentation.business.standardfeatures.services.hql.uc57_hqloperation.bos.Player57BO collection
	 *
	 * @param entities				
	 *		: {@link java.util.List} a list of entities
	 * @throws ApplicationException
     */	
public void removeAllFromPlayers( Collection<Player57BO> entities ) throws ApplicationException {
	for( Player57BO entity : entities) {
		if( getPlayers()!=null && getPlayers().contains( entity ) )	{		
			removeFromPlayers( entity );
		}
	}
}
	
	/**
	 * Remove all items form the collection com.bluage.documentation.business.standardfeatures.services.hql.uc57_hqloperation.bos.Player57BO
	 *
	 * @throws ApplicationException
     */	
public void removeAllFromPlayers() throws ApplicationException {
	Set<Player57BO> entities = this.getPlayers();
	if ( entities == null ) {
		return;
	}
	entities.clear();
}

/**
	 * Returns <code>true</code> if the argument is an Team57 instance and all identifiers for this entity
     * equal the identifiers of the argument entity. The <code>equals</code> method of the parent entity
     * will also need to return <code>true</code>. Returns <code>false</code> otherwise.
     *
     * @see TeamBO#hashCode()
	 * @param object  object to compare with current instance      
     * @return boolean true if equals false if not
     */
    public boolean equals(final Object object )
    {
		if ( this == object )
        {
            return true;
        }
        if ( !( object instanceof Team57BO ) )
        {
            return false;
        }
        final Team57BO that = (Team57BO)object;
    
    	            if (        				        			this.getId() == null
        	        ) {
        	return false;
        }
		return new EqualsBuilder()
			.append(this.getId(), that.getId())
			 	.isEquals();
	}
	/**

	 * Returns a hash code based on this entity's identifiers and the hash code of the parent entity.
     *
     * @see TeamBO#hashCode()
     * @return int
     */    
    public int hashCode() {
        return new HashCodeBuilder()
             .append(getId())
				 	.appendSuper(super.hashCode())
            .toHashCode();
    }
}

