/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.entities.uc61_transientobject ;

// Import declaration.
// Java imports.
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;

/**
 * Class : UC61_ExceptionController
 */
@Controller
@Scope("prototype")
@SessionAttributes("uC61_ExceptionForm")
@RequestMapping(value= "/presentation/standardfeatures/entities/uc61_transientobject")
public class UC61_ExceptionController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC61_ExceptionController.class);
	
	//Current form name
	private static final String U_C61__EXCEPTION_FORM = "uC61_ExceptionForm";

	
	/**
	 * Property:customDateEditorsUC61_ExceptionController
	 */
	final private Map<String,CustomDateEditor> customDateEditorsUC61_ExceptionController = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:uC61_ExceptionValidator
	 */
	final private UC61_ExceptionValidator uC61_ExceptionValidator = new UC61_ExceptionValidator();
	
	
	// Initialise all the instances for UC61_ExceptionController

	/**
	* This method initialise the form : UC61_ExceptionForm 
	* @return UC61_ExceptionForm
	*/
	@ModelAttribute("UC61_ExceptionFormInit")
	public void initUC61_ExceptionForm(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, U_C61__EXCEPTION_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method UC61_ExceptionForm."); //for lnk_addteam 
		}
		UC61_ExceptionForm uC61_ExceptionForm;
	
		if(request.getSession().getAttribute(U_C61__EXCEPTION_FORM) != null){
			uC61_ExceptionForm = (UC61_ExceptionForm)request.getSession().getAttribute(U_C61__EXCEPTION_FORM);
		} else {
			uC61_ExceptionForm = new UC61_ExceptionForm();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form UC61_ExceptionForm.");
		}
		uC61_ExceptionForm = (UC61_ExceptionForm)populateDestinationForm(request.getSession(), uC61_ExceptionForm);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  UC61_ExceptionForm.");
		}
		model.addAttribute(U_C61__EXCEPTION_FORM, uC61_ExceptionForm);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param UC61_ExceptionForm : The sceen form.
	 */
	@RequestMapping(value = "/UC61_Exception.html" ,method = RequestMethod.GET)
	public void prepareUC61_Exception(final HttpServletRequest request,final  Model model,final  @ModelAttribute("UC61_ExceptionFormInit") UC61_ExceptionForm uC61_ExceptionForm){
		
		UC61_ExceptionForm currentUC61_ExceptionForm = uC61_ExceptionForm;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(U_C61__EXCEPTION_FORM) == null){
			if(currentUC61_ExceptionForm!=null){
				request.getSession().setAttribute(U_C61__EXCEPTION_FORM, currentUC61_ExceptionForm);
			}else {
				currentUC61_ExceptionForm = new UC61_ExceptionForm();
				request.getSession().setAttribute(U_C61__EXCEPTION_FORM, currentUC61_ExceptionForm);	
			}
		} else {
			currentUC61_ExceptionForm = (UC61_ExceptionForm) request.getSession().getAttribute(U_C61__EXCEPTION_FORM);
		}

		currentUC61_ExceptionForm = (UC61_ExceptionForm)populateDestinationForm(request.getSession(), currentUC61_ExceptionForm);
		// Call all the Precontroller.
	currentUC61_ExceptionForm.setAlwaysCallPreControllers(true);
		model.addAttribute(U_C61__EXCEPTION_FORM, currentUC61_ExceptionForm);
		request.getSession().setAttribute(U_C61__EXCEPTION_FORM, currentUC61_ExceptionForm);
		
	}
	
	
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("UC61_ExceptionController [ ");
		// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(uC61_ExceptionValidator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsUC61_ExceptionController!=null); 
		return strBToS.toString();
	}
}
