<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file="/include.jsp"  %>
<html >
<head >
<title  > 
<fmt:message key="cUC206_HomePage_BLU_AGE_DOCUMENTATION"/>
</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"  > 
</meta>
<link href="<c:url value= "/presentation/_style/_style.css"/>" rel="stylesheet" type="text/css"  /> 
<link href="<c:url value= "/presentation/_include/jquery/rd_lib.css"/>" rel="stylesheet" type="text/css"  /> 
<script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/jquery.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/rd_setup.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/rd_lib.js"/>" space="preserve"  ></script><link rel="shortcut icon" href="<c:url value= "/presentation/_style/favicon.ico"/>"  /> 

</head>

<body class=" " > 
<div id="rd_ghost"  > 
<small  > 
<fmt:message key="cUC206_HomePage___CSS_DEACTIVATED"/>
</small>
</div>
<div id="rd_jghost"  > 
<small  > 
<fmt:message key="cUC206_HomePage___JAVASCRIPT_DEACTIVATED"/>
</small>
</div>
<div id="shape"  > 
<div class="ui-layout-center"  > 
<div class="east-north"  > 
	<form:form id="uC206_HomePage" commandName="uC206_HomePageForm"  enctype="application/x-www-form-urlencoded" method="post">
<h2 class="rd_first"  > 
<fmt:message key="cUC206_HomePage_ADVANCED_FEATURES__VALUE_OBJE"/>
</h2>
<table class="rd_tableform"  >
<tr  > 
<td rowspan="1" colspan="1"  >
<a id="lnk_createPlayer"onclick="javascript:btn_click_cExecuteUC206_HomePage(this.href);return false;"				href="<c:url value="/presentation/advancedfeatures/valueobject/uc206_valueobject/UC206_HomePage/lnk_createPlayer.html"/>"
							 shape="rect"  >									 
<fmt:message key="cUC206_HomePage_CREATE_A_PLAYER"/>
</a>
</td> 
<td rowspan="1" colspan="1"  >
<a id="lnk_searchPlayer"onclick="javascript:btn_click_cExecuteUC206_HomePage(this.href);return false;"				href="<c:url value="/presentation/advancedfeatures/valueobject/uc206_valueobject/UC206_HomePage/lnk_searchPlayer.html"/>"
							 shape="rect"  >									 
<fmt:message key="cUC206_HomePage_SEARCH_A_PLAYER"/>
</a>
</td> 
<td rowspan="1" colspan="1"  >
<a id="lnk_listPlayer"onclick="javascript:btn_click_cExecuteUC206_HomePage(this.href);return false;"				href="<c:url value="/presentation/advancedfeatures/valueobject/uc206_valueobject/UC206_HomePage/lnk_listPlayer.html"/>"
							 shape="rect"  >									 
<fmt:message key="cUC206_HomePage_LIST_PLAYERS"/>
</a>
</td> 
</tr>
</table> 
	</form:form>
	<script language="javascript">
		function btn_click_cExecuteUC206_HomePage(url) {
			UC206_HomePagevar = document.getElementById('uC206_HomePage');
			UC206_HomePagevar.setAttribute('action', url);
			UC206_HomePagevar.submit();
		}
		function btn_click_cSetActionUC206_HomePage(url) {
			UC206_HomePagevar = document.getElementById('uC206_HomePage');
			UC206_HomePagevar.setAttribute('action', url);
		}
	</script>
</div>
<div class="east-center"  > 
<tiles:insertDefinition	name="presentation/advancedfeatures/valueobject/uc206_valueobject/UC206_HomePagecmp_tabs" />
</div>
</div>
<div class="ui-layout-north"  > 
<tiles:insertDefinition	name="presentation/advancedfeatures/valueobject/uc206_valueobject/UC206_HomePagecmp_header" />
</div>
<div class="ui-layout-south"  > 
<fmt:message key="cUC206_HomePage_______"/>
<span id="rd_jversion"  > 
<fmt:message key="cUC206_HomePage_BLU_AGE_DOCUMENTATION_15"/>
</span>
</div>
<div class="ui-layout-west"  > 
<tiles:insertDefinition	name="presentation/advancedfeatures/valueobject/uc206_valueobject/UC206_HomePagecmp_treeview" />
</div>
</div>
</body>
</html>
