/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.netfective.bluage.springmvc.ajax.util;


import java.io.UnsupportedEncodingException;
import java.util.Set;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.netfective.bluage.springmvc.ajax.BufferResponseWrapper;
import com.netfective.bluage.springmvc.ajax.tag.TagUtils;


/**
 * @author NETFECTIVE TECHNOLOGY
 * @version 1.0
 * @since 23 oct. 07
 */
public final class AjaxUtils {

	/**
	* Default builder
	*/
	private AjaxUtils(){};
	
	/**
	 * Verify if the current request is an ajax request
	 * @param {@link ServletRequest} :
	 *					request
	 * @return boolean
	 */
	public static boolean isAjaxRequest(final ServletRequest request) {
		final HttpServletRequest httpServletRequest = (HttpServletRequest) request;
		return "true".equals(httpServletRequest
				.getParameter(Constants.AJAX_URL_PARAMETER));
	}
	
	/**
	 * Split <code>commaSeparatedZones</code> and add zones to refresh
	 * to the request
	 * @param {@link ServletRequest} :
	 * 					request
	 * @param {@link String} : 
	 * 					commaSeparatedZones
	 */
	public static void addZonesToRefresh(final ServletRequest request, final String commaSeparatedZones) {
		final Set res = getZonesToRefresh(request);
		if (res == null){
			return;
		}
		final String[] zones = commaSeparatedZones.split(",");
		for (int i = 0; i < zones.length; i++) {
			res.add(zones[i]);
		}
	}	

	/**
	 * Get the zone to refresh
	 * @param {@link ServletRequest} :
	 *					request
	 * @return <b>Set</b> of zones to refresh
	 */
	
	public static Set getZonesToRefresh(final ServletRequest request) {
		return (Set) request.getAttribute(Constants.REFRESH_ZONES_KEY);
	}	

	/**
	 * Add the Set of <b>zones</b> to refresh to th <b>request</b>
	 * @param {@link ServletRequest} :
	 *					request
	 * @param {@link Set} :
	 *					zones
	 */
	public static void setZonesToRefresh(final ServletRequest request, final Set zones) {
		request.setAttribute(Constants.REFRESH_ZONES_KEY, zones);
	}

	/**
	 * Extract the zones to refresh from the <b>request</b>
	 * @param {@link ServletRequest} : 
	 *					request
	 */
	public static void getRefreshZonesFromURL(final ServletRequest request) {
		final String[] zones = request.getParameterValues(Constants.ZONES_URL_KEY);
		if (zones != null) {
			for (int i1 = 0; i1 < zones.length; i1++) {
				final String zone1 = zones[i1];
				addZonesToRefresh(request, zone1);
			}
		}
	}

	/**
	 * Extract the <b>zone</b> html content from response <b>bufferResponseWrapper</b>
	 * @param {@link String} :
	 * 				zone
	 * @param {@link BufferResponseWrapper} :
	 *				bufferResponseWrapper
	 * @return {@link String}
	 */
	public static String getZoneContent(final String zone,final BufferResponseWrapper bufferResponseWrapper) {
		return bufferResponseWrapper.findSubstring(TagUtils.getZoneStartDelimiter(zone), TagUtils.getZoneEndDelimiter(zone));
	}

	/**
	 * Add ajax capability to the <b>request</b> and <b>response</b>
	 * @param {@link HttpServletResponse} :
	 *				request
	 * @param {@link HttpServletResponse} :
	 *				response
	 * @throws {@link UnsupportedEncodingException}
	 */
	public static void addAjaxCapabilities(final HttpServletRequest request,
			final HttpServletResponse response) throws UnsupportedEncodingException {
		request.setCharacterEncoding("UTF-8");
		response.setContentType("text/xml;charset=UTF-8");
		response.setHeader("Cache-Control", "no-cache");
		response.setDateHeader("Expires", 0);
		response.setHeader("Pragma", "no-cache");
	}
	
	/**
	 * Is Blank
	 * @param str
	 * @return
	 */
    public static boolean isBlank(final String str) {
		boolean isBlank = true;
		if (str == null || str.length() == 0) {
			isBlank = true;
		} else {
			final int strLen = str.length();
			for (int i = 0; i < strLen; i++) {
				if (!Character.isWhitespace(str.charAt(i))) {
					isBlank = false;
				}
			}
		}
		return isBlank;
    }


    /**
     * Is Not Blank.
     * @param str
     * @return
     */
    public static boolean isNotBlank(final String str) {
    	return !isBlank(str);
    }
    
    /**
     * Delete whitespace.
     * @param str
     * @return
     */
    public static String deleteWhitespace(final String str) {
		String stringToReturn = null;
		if (isEmpty(str)) {
			stringToReturn = str;
		}else{
			final int sz = str.length();
			char[] chs = new char[sz];
			int count = 0;
			for (int i = 0; i < sz; i++) {
				if (!Character.isWhitespace(str.charAt(i))) {
					chs[count++] = str.charAt(i);
				}
			}
			if (count == sz) {
				stringToReturn = str;
			} else {
				stringToReturn = new String(chs, 0, count);
			}
		}
		return stringToReturn;
	}
    
    /**
     * isEmpty?
     * @param str
     * @return
     */
    public static boolean isEmpty(final String str) {
    	return str == null || str.length() == 0;
    }
}
