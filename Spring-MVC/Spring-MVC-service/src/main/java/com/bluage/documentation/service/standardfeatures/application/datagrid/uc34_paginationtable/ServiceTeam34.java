/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.service.standardfeatures.application.datagrid.uc34_paginationtable;


import javax.inject.Inject;
import javax.inject.Named;

import com.bluage.documentation.business.standardfeatures.application.datagrid.uc34_paginationtable.bos.Team34BO;
import com.bluage.documentation.business.standardfeatures.application.datagrid.uc34_paginationtable.entities.daofinder.Team34DAOFinderImpl;
import com.bluage.documentation.service.AbstractService;
import com.netfective.bluage.core.exception.ApplicationException;

/**
 * Defines  application services for ServiceTeam34
  */
@Named("com.bluage.documentation.service.standardfeatures.application.datagrid.uc34_paginationtable.ServiceTeam34")
public class ServiceTeam34  extends AbstractService{


			@Inject
		@Named ("com.bluage.documentation.business.standardfeatures.application.datagrid.uc34_paginationtable.entities.daofinder.Team34DAOFinderImpl")
		private Team34DAOFinderImpl team34BOFinder;
	


	/**
	 *  Default constructor 
	 */
	public ServiceTeam34() {
		super(); // Call super for default constructor : ServiceTeam34
	}
	


			
				
	/**    
	 * Operation Crud
	 * @param bo {@link Team34BO}
		 * @return {@link Team34BO}
	 */
	public Team34BO teamFindByID(Team34BO bo )
 throws ApplicationException {

					return (Team34BO)team34BOFinder.findByPrimaryKey(getBoId(bo));
	}
	/**
	 *  Return the primary key from business object.
	 */
	private Object getBoId(Team34BO bo) {
				return bo.getId();
		}

		}
