/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.application.form.uc44_autocompletetable ;

// Import declaration.
// Java imports.
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.bluage.documentation.business.standardfeatures.application.form.uc44_autocompletetable.bos.Player44BO;
import com.bluage.documentation.business.standardfeatures.application.form.uc44_autocompletetable.bos.Team44BO;
import com.bluage.documentation.business.standardfeatures.application.form.uc44_autocompletetable.entities.daofinder.Team44DAOFinderImpl;
import com.bluage.documentation.service.standardfeatures.application.form.uc44_autocompletetable.ServicePlayer44;
import com.netfective.bluage.core.exception.ApplicationException;
import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;
import com.netfective.bluage.core.web.IForm;

/**
 * Class : UC44_AutocompleteTableController
 */
@Controller
@Scope("prototype")
@SessionAttributes("uC44_AutocompleteTableForm")
@RequestMapping(value= "/presentation/standardfeatures/application/form/uc44_autocompletetable")
public class UC44_AutocompleteTableController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC44_AutocompleteTableController.class);
	
	//Current form name
	private static final String U_C44__AUTOCOMPLETE_TABLE_FORM = "uC44_AutocompleteTableForm";

		//CONSTANT: instance_teams
	private static final String INSTANCE_TEAMS = "instance_teams";
				//CONSTANT: allPlayers table or repeater.
	private static final String ALL_PLAYERS = "allPlayers";
				//CONSTANT: selectedPlayer
	private static final String SELECTED_PLAYER = "selectedPlayer";
	//CONSTANT: selectedTeam
	private static final String SELECTED_TEAM = "selectedTeam";
	
	/**
	 * Property:customDateEditorsUC44_AutocompleteTableController
	 */
	final private Map<String,CustomDateEditor> customDateEditorsUC44_AutocompleteTableController = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:uC44_AutocompleteTableValidator
	 */
	final private UC44_AutocompleteTableValidator uC44_AutocompleteTableValidator = new UC44_AutocompleteTableValidator();
	
	/**
	 * Service declaration : servicePlayer44.
	 */
	@Autowired
	private ServicePlayer44 servicePlayer44;
	
	/**
	 * Generic Finder : team44DAOFinderImpl.
	 */
	@Autowired
	private Team44DAOFinderImpl team44DAOFinderImpl;
	
	
	// Initialise all the instances for UC44_AutocompleteTableController
			// Initialize the instance instance_teams for the state lnk_display
		private List instance_teams; // Initialize the instance instance_teams for UC44_AutocompleteTableController
						// Initialize the instance allPlayers for the state lnk_display
		private List allPlayers; // Initialize the instance allPlayers for UC44_AutocompleteTableController
						// Declare the instance selectedPlayer
		private Player44BO selectedPlayer;
		// Declare the instance selectedTeam
		private Team44BO selectedTeam;
			/**
	 * Operation : lnk_update
 	 * @param model : 
 	 * @param uC44_AutocompleteTable : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC44_AutocompleteTable/lnk_update.html",method = RequestMethod.POST)
	public String lnk_update(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC44_AutocompleteTableForm") UC44_AutocompleteTableForm  uC44_AutocompleteTableForm, final BindingResult bindingResult ,final @RequestParam(ID_REQUEST_PARAM) Integer index){
		
		
		
		LOGGER.info("Begin method : lnk_update"); 
		infoLogger(uC44_AutocompleteTableForm); 
		uC44_AutocompleteTableForm.setAlwaysCallPreControllers(false); 
		// initialization for lnk_update
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state lnk_update if the validation fail, you will be forward to the screen : "/presentation/standardfeatures/application/form/uc44_autocompletetable/UC44_AutocompleteTable"
		if(errorsMessages(request,response,uC44_AutocompleteTableForm,bindingResult,"selectedPlayer", uC44_AutocompleteTableValidator, customDateEditorsUC44_AutocompleteTableController)){ 
			return "/presentation/standardfeatures/application/form/uc44_autocompletetable/UC44_AutocompleteTable"; 
		}
		// Updating selected row for the state lnk_update 
		uC44_AutocompleteTableForm.setSelectedRow(-1); //Set selected row for lnk_update 
		// Updating selected table for the state lnk_update 
		uC44_AutocompleteTableForm.setSelectedTab(null); //reset selected row for lnk_update 

														destinationPath =  callUpdatePlayer(request,uC44_AutocompleteTableForm , index, model);
					if(destinationPath != null){
						return destinationPath;
					}
						// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.application.form.uc44_autocompletetable.UC44_AutocompleteTableForm or not from lnk_update
			// Populate the destination form with all the instances returned from lnk_update.
			final IForm uC44_AutocompleteTableForm2 = populateDestinationForm(request.getSession(), uC44_AutocompleteTableForm); 
					
			// Add the form to the model.
			model.addAttribute("uC44_AutocompleteTableForm", uC44_AutocompleteTableForm2); 
			
			request.getSession().setAttribute("uC44_AutocompleteTableForm", uC44_AutocompleteTableForm2);
			
			// "OK" CASE => destination screen path from lnk_update
			LOGGER.info("Go to the screen 'UC44_AutocompleteTable'.");
			// Redirect (PRG) from lnk_update
			destinationPath =  "redirect:/presentation/standardfeatures/application/form/uc44_autocompletetable/UC44_AutocompleteTable.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_update 
	}
	
		/**
	 * Operation : lnk_edit
 	 * @param model : The model
 	 * @param uC44_AutocompleteTable : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC44_AutocompleteTable/lnk_edit.html")
	public String lnk_edit(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC44_AutocompleteTableForm") UC44_AutocompleteTableForm  uC44_AutocompleteTableForm, BindingResult bindingResult, final @RequestParam(ID_REQUEST_PARAM) Integer index){
		uC44_AutocompleteTableForm.setSelectedRow(index);	
		String tableId = request.getParameter(TAB_REQUEST_PARAM);
		uC44_AutocompleteTableForm.setSelectedTab(tableId);
		
		Player44BO selectedPlayer = (Player44BO) uC44_AutocompleteTableForm.getAllPlayers().get(index);
		uC44_AutocompleteTableForm.setSelectedPlayer(selectedPlayer);
		return "/presentation/standardfeatures/application/form/uc44_autocompletetable/UC44_AutocompleteTable";
	}

	/**
	* This method initialise the form : UC44_AutocompleteTableForm 
	* @return UC44_AutocompleteTableForm
	*/
	@ModelAttribute("UC44_AutocompleteTableFormInit")
	public void initUC44_AutocompleteTableForm(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, U_C44__AUTOCOMPLETE_TABLE_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method UC44_AutocompleteTableForm."); //for lnk_edit 
		}
		UC44_AutocompleteTableForm uC44_AutocompleteTableForm;
	
		if(request.getSession().getAttribute(U_C44__AUTOCOMPLETE_TABLE_FORM) != null){
			uC44_AutocompleteTableForm = (UC44_AutocompleteTableForm)request.getSession().getAttribute(U_C44__AUTOCOMPLETE_TABLE_FORM);
		} else {
			uC44_AutocompleteTableForm = new UC44_AutocompleteTableForm();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form UC44_AutocompleteTableForm.");
		}
		uC44_AutocompleteTableForm = (UC44_AutocompleteTableForm)populateDestinationForm(request.getSession(), uC44_AutocompleteTableForm);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  UC44_AutocompleteTableForm.");
		}
		model.addAttribute(U_C44__AUTOCOMPLETE_TABLE_FORM, uC44_AutocompleteTableForm);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param UC44_AutocompleteTableForm : The sceen form.
	 */
	@RequestMapping(value = "/UC44_AutocompleteTable.html" ,method = RequestMethod.GET)
	public void prepareUC44_AutocompleteTable(final HttpServletRequest request,final  Model model,final  @ModelAttribute("UC44_AutocompleteTableFormInit") UC44_AutocompleteTableForm uC44_AutocompleteTableForm){
		
		UC44_AutocompleteTableForm currentUC44_AutocompleteTableForm = uC44_AutocompleteTableForm;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(U_C44__AUTOCOMPLETE_TABLE_FORM) == null){
			if(currentUC44_AutocompleteTableForm!=null){
				request.getSession().setAttribute(U_C44__AUTOCOMPLETE_TABLE_FORM, currentUC44_AutocompleteTableForm);
			}else {
				currentUC44_AutocompleteTableForm = new UC44_AutocompleteTableForm();
				request.getSession().setAttribute(U_C44__AUTOCOMPLETE_TABLE_FORM, currentUC44_AutocompleteTableForm);	
			}
		} else {
			currentUC44_AutocompleteTableForm = (UC44_AutocompleteTableForm) request.getSession().getAttribute(U_C44__AUTOCOMPLETE_TABLE_FORM);
		}

		try {
			List instance_teams = team44DAOFinderImpl.findAll();
			put(request.getSession(), INSTANCE_TEAMS, instance_teams);
		} catch (ApplicationException e) {
			errorLogger("An error while calling the generic finder for the instance : instance_teams.",e);
		}
				currentUC44_AutocompleteTableForm = (UC44_AutocompleteTableForm)populateDestinationForm(request.getSession(), currentUC44_AutocompleteTableForm);
		// Call all the Precontroller.
	currentUC44_AutocompleteTableForm.setAlwaysCallPreControllers(true);
		model.addAttribute(U_C44__AUTOCOMPLETE_TABLE_FORM, currentUC44_AutocompleteTableForm);
		request.getSession().setAttribute(U_C44__AUTOCOMPLETE_TABLE_FORM, currentUC44_AutocompleteTableForm);
		
	}
	
			/**
	 * Operation : instance_teamsautocomplete
	 * 
	 * @param model
	 *            : The Spring MVC Model
	 * @param UC44_AutocompleteTableForm
	 *            : The form
	 * @return
	 */
	@RequestMapping(value = "/instance_teamsautocomplete.html", method = RequestMethod.GET)
	public @ResponseBody String instance_teamsautocomplete(final @ModelAttribute("uC44_AutocompleteTableForm") UC44_AutocompleteTableForm uC44_AutocompleteTableForm,final  @RequestParam String query) {

		StringBuffer buffer = new StringBuffer();
		List<Team44BO> bos = uC44_AutocompleteTableForm.getInstance_teams();
		for (Team44BO bo : bos) {
			String name = bo.getName();
			if (name != null && name.toLowerCase().startsWith(query.toLowerCase())) {	
				buffer.append(name).append(",");
			}
		}
		
		return buffer.toString();
	}
														/**
	 * method callUpdatePlayer
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 * @param index Integer the current index 
	 * @param model Model the model
	 * @return String result NOK 
	 */
	private String callUpdatePlayer(HttpServletRequest request,IForm form,Integer index, final Model model) {
		String str = null;
					if(index!=null){  
			 selectedPlayer = (Player44BO)((List)get(request.getSession(),form, "allPlayers")).get(index);  
		} else {
			 selectedPlayer= null; 
		}
				 
					selectedTeam = (Team44BO)get(request.getSession(),form, "selectedTeam");  
										 
					try {
				// executing UpdatePlayer in lnk_edit
	servicePlayer44.updatePlayer(
			selectedPlayer
		,			selectedTeam
			);  

								// processing variables UpdatePlayer in lnk_edit
				put(request.getSession(), SELECTED_PLAYER,selectedPlayer); 
							put(request.getSession(), SELECTED_TEAM,selectedTeam); 
			
			} catch (ApplicationException e) { 
				// error handling for operation updatePlayer called UpdatePlayer
				errorLogger("An error occured during the execution of the operation : updatePlayer",e); 
		
				// "NOK" DEFAULT CASE
				UC44_ExceptionForm uC44_ExceptionForm = new UC44_ExceptionForm();
				final IForm uC44_ExceptionForm2 = populateDestinationForm(request.getSession(), uC44_ExceptionForm);
				model.addAttribute("uC44_ExceptionForm", uC44_ExceptionForm2);
				request.getSession().setAttribute("uC44_ExceptionForm", uC44_ExceptionForm2);
				request.getSession().setAttribute("messages", e.getMessage());
				return "redirect:/presentation/standardfeatures/application/form/uc44_autocompletetable/UC44_Exception.html";
						}
		return str;
	}
		/**
	* This method Binder.
	* @param binder : 
	*		 WebDataBinder
	*/
	@InitBinder
	protected void initBinder(final WebDataBinder binder){
			customDateEditorsUC44_AutocompleteTableController.put("allPlayers.dateOfBirth", new CustomDateEditor(new SimpleDateFormat("MM/dd/yyyy"), true, "Invalid format."));
			binder.registerCustomEditor(Date.class, "allPlayers.dateOfBirth", customDateEditorsUC44_AutocompleteTableController.get("allPlayers.dateOfBirth"));
	}
	
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("UC44_AutocompleteTableController [ ");
				strBToS.append(INSTANCE_TEAMS);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(instance_teams);
			strBToS.append(END_TO_STRING_DELIMITER);
						strBToS.append(ALL_PLAYERS);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(allPlayers);
			strBToS.append(END_TO_STRING_DELIMITER);
						strBToS.append(SELECTED_PLAYER);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(selectedPlayer);
			strBToS.append(END_TO_STRING_DELIMITER);
			strBToS.append(SELECTED_TEAM);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(selectedTeam);
			strBToS.append(END_TO_STRING_DELIMITER);
		// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(uC44_AutocompleteTableValidator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsUC44_AutocompleteTableController!=null); 
		return strBToS.toString();
	}
}
