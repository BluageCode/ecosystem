<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file="/include.jsp"  %>
<html >
<head >
<title  > 
<fmt:message key="cUC58_CopyDetailsTeam58_BLU_AGE_DOCUMENTATION"/>
</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"  > 
</meta>
<link href="<c:url value= "/presentation/_style/_style.css"/>" rel="stylesheet" type="text/css"  /> 
<link href="<c:url value= "/presentation/_include/jquery/rd_lib.css"/>" rel="stylesheet" type="text/css"  /> 
<script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/jquery.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/rd_setup.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/rd_lib.js"/>" space="preserve"  ></script><link rel="shortcut icon" href="<c:url value= "/presentation/_style/favicon.ico"/>"  /> 

</head>

<body class=" " > 
<div id="rd_ghost"  > 
<small  > 
<fmt:message key="cUC58_CopyDetailsTeam58___CSS_DEACTIVATED"/>
</small>
</div>
<div id="rd_jghost"  > 
<small  > 
<fmt:message key="cUC58_CopyDetailsTeam58___JAVASCRIPT_DEACTIVATED"/>
</small>
</div>
<div id="shape"  > 
<div class="ui-layout-center"  > 
<div class="east-north"  > 
	<form:form id="uC58_CopyDetailsTeam58" commandName="uC58_CopyDetailsTeam58Form"  enctype="application/x-www-form-urlencoded" method="post">
<h2 class="rd_first"  > 
<fmt:message key="cUC58_CopyDetailsTeam58_STANDARD_FEATURES__SERVICES_"/>
</h2>
<h4  > 
<fmt:message key="cUC58_CopyDetailsTeam58_COPY_OF_TEAM"/>
</h4>
<table class="rd_tableform"  >
<tr  > 
<td width="285" rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC58_CopyDetailsTeam58_CITY"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
		<span id="txt_city1">
	<c:out value="${uC58_CopyDetailsTeam58Form.copyTeam.city}" />
		</span>
	</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC58_CopyDetailsTeam58_NAME"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
		<span id="txt_name1">
	<c:out value="${uC58_CopyDetailsTeam58Form.copyTeam.name}" />
		</span>
	</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC58_CopyDetailsTeam58_STATE"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
		<span id="select_state1">
	<c:out value="${uC58_CopyDetailsTeam58Form.copyTeam.state.name}" />
		</span>
	</td> 
</tr>
</table> 
<h4  > 
<fmt:message key="cUC58_CopyDetailsTeam58_TEAM"/>
</h4>
<table class="rd_tableform"  >
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC58_CopyDetailsTeam58_CITY_225"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
<form:input id="txt_city" path="teamDetails.city"  
		/>
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC58_CopyDetailsTeam58_NAME_226"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
<form:input id="txt_name" path="teamDetails.name"  
		/>
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC58_CopyDetailsTeam58_STATE_227"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
<form:select id="select_state" path="teamDetails.state.code"   
		>
		<form:options items="${uC58_CopyDetailsTeam58Form.allStates1}" itemValue="code" itemLabel="name" />
</form:select>
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
</td> 
<td rowspan="1" colspan="1"  >
<a id="lnk_updateTeam"onclick="javascript:btn_click_cExecuteUC58_CopyDetailsTeam58(this.href);return false;"				href="<c:url value="/presentation/standardfeatures/services/standard/uc58_servicecopy/UC58_CopyDetailsTeam58/lnk_updateTeam.html"/>"
							 shape="rect"  >									 
<fmt:message key="cUC58_CopyDetailsTeam58_UPDATE"/>
</a>
</td> 
</tr>
</table> 
<table class="rd_tableform"  >
<tr  > 
<td width="285" rowspan="1" colspan="1"  >
</td> 
<td rowspan="1" colspan="1"  >
<a id="lnk_back"				href="<c:url value="/presentation/standardfeatures/services/standard/uc58_servicecopy/UC58_DisplayTeams58.html"/>"
													 shape="rect"  >									 
<fmt:message key="cUC58_CopyDetailsTeam58_BACK"/>
</a>
</td> 
</tr>
</table> 
	</form:form>
	<script language="javascript">
		function btn_click_cExecuteUC58_CopyDetailsTeam58(url) {
			UC58_CopyDetailsTeam58var = document.getElementById('uC58_CopyDetailsTeam58');
			UC58_CopyDetailsTeam58var.setAttribute('action', url);
			UC58_CopyDetailsTeam58var.submit();
		}
		function btn_click_cSetActionUC58_CopyDetailsTeam58(url) {
			UC58_CopyDetailsTeam58var = document.getElementById('uC58_CopyDetailsTeam58');
			UC58_CopyDetailsTeam58var.setAttribute('action', url);
		}
	</script>
</div>
<div class="east-center"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/services/standard/uc58_servicecopy/UC58_CopyDetailsTeam58cmp_tabs" />
</div>
</div>
<div class="ui-layout-north"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/services/standard/uc58_servicecopy/UC58_CopyDetailsTeam58cmp_header" />
</div>
<div class="ui-layout-south"  > 
<fmt:message key="cUC58_CopyDetailsTeam58___"/>
<span id="rd_jversion"  > 
<fmt:message key="cUC58_CopyDetailsTeam58_BLU_AGE_CORPORATION"/>
</span>
</div>
<div class="ui-layout-west"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/services/standard/uc58_servicecopy/UC58_CopyDetailsTeam58cmp_treeview" />
</div>
</div>
</body>
</html>
