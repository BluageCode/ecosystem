/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.netfective.bluage.core.presentation.screen.paginator;

import java.text.MessageFormat;
import java.util.List;

import org.springframework.context.ApplicationContext;

import com.netfective.bluage.core.exception.ApplicationException;
import com.netfective.bluage.spring.AppContext;
/**		 
 * AbstractPageCommand
 * Command class used for pagination
 */
public abstract class AbstractPageCommand implements IPageCommand{
	
	public static final int DEFAULT_START_PAGE = 0;
	private static ApplicationContext appContext;
		
	private int rewindFastForwardBy;
	private int itemsPerPage;
	private int currentPage;
	private int numberOfPages;
	private int maxPage;
	private int numberOfCount;
	private int numberOfItems;
	
	/**
	 * @see com.netfective.bluage.core.presentation.screen.paginator.IPageCommand#forward(int)
	 */
	public final void forward(final int aRewindFastForwardBy) {
		final int newPageNumber = getCurrentPage() + aRewindFastForwardBy;
		if (newPageNumber > getNumberOfPages()) {
			setCurrentPage(getNumberOfPages());
		} else {
			setCurrentPage(newPageNumber);
		}

	}
	
	/**
	 * @see com.netfective.bluage.core.presentation.screen.paginator.IPageCommand#getItemsPerPage()
	 */
	public final int getItemsPerPage() {
		return itemsPerPage;
	}

	/**
	 * @see com.netfective.bluage.core.presentation.screen.paginator.IPageCommand#getNumberOfPages()
	 */
	public final int getNumberOfPages() {
		return numberOfPages;
	}

	/**
	 * @see com.netfective.bluage.core.presentation.screen.paginator.IPageCommand#getRecordStatus()
	 */
	public final String getRecordStatus() {
		return  MessageFormat.format(FORMAT_0_OF_1,
				new Object[] {
				Integer.valueOf(this.getCurrentPage()),
						Integer.valueOf(this.getNumberOfPages()) });
	}

	/**
	 * @see com.netfective.bluage.core.presentation.screen.paginator.IPageCommand#getRewindFastForwardBy()
	 */
	public final int getRewindFastForwardBy() {
		return rewindFastForwardBy;
	}

	/**
	 * @see com.netfective.bluage.core.presentation.screen.paginator.IPageCommand#getSortLinkNatural()
	 */
	public final int getSortLinkNatural() {
		return 2;
	}

	/**
	 * @see com.netfective.bluage.core.presentation.screen.paginator.IPageCommand#next()
	 */
	public final String next() {
		this.forward(1);
		return NEXT;
	}
	
/**
	 * @see com.netfective.bluage.core.presentation.screen.paginator.IPageCommand#returnResults()
	 */
	public final List returnResults() throws ApplicationException {
		return null;
	}
	
	/**
	 * @see com.netfective.bluage.core.presentation.screen.paginator.IPageCommand#rewind(int)
	 */	
	public final void rewind(final int aRewindFastForwardBy) {
		final int newPageNumber = getCurrentPage() - aRewindFastForwardBy;
		if (newPageNumber < 0) {
			setCurrentPage(0);
		} else {
			setCurrentPage(newPageNumber);
		}
	}

	/**
	 * @see com.netfective.bluage.core.presentation.screen.paginator.IPageCommand#rewind()
	 */
	public final String rewind() {
		setRewindFastForwardBy(rewindFastForwardBy);
		this.rewind(getRewindFastForwardBy());
		return REWIND;
	}

	/**
	 * @see com.netfective.bluage.core.presentation.screen.paginator.IPageCommand#setItemsPerPage(int)
	 */
	public final void setItemsPerPage(final int itemsPerPage) {
		this.itemsPerPage = itemsPerPage;
	}

	/**
	 * @see com.netfective.bluage.core.presentation.screen.paginator.IPageCommand#setNumberOfPages(int)
	 */
	public final void setNumberOfPages(final int numberOfPages) {
		this.numberOfPages = numberOfPages;
	}

	/**
	 * @see com.netfective.bluage.core.presentation.screen.paginator.IPageCommand#setRewindFastForwardBy(int)
	 */
	public final void setRewindFastForwardBy(final int rewindFastForwardBy) {
		this.rewindFastForwardBy = rewindFastForwardBy;
	}

    /**
    * sets the SORT_LINK_NATURAL
    */
	public final void setSortLinkNatural(final int sortLinkNatural) {
	}

	/**
	 * @see com.netfective.bluage.core.presentation.screen.paginator.IPageCommand#fastForward()
	 */
	public final String fastForward() {
		setRewindFastForwardBy(rewindFastForwardBy);
		this.forward(getRewindFastForwardBy());
		return FAST_FORWARD;
	}

	/**
	 * @see com.netfective.bluage.core.presentation.screen.paginator.IPageCommand#previous()
	 */
	public final String previous() {
		this.rewind(1);
		return PREVIOUS;
	}
	
	/**
	 * @see com.netfective.bluage.core.presentation.screen.paginator.IPageCommand#renderPaginator(int)
	 */
	public final void renderPaginator(final int pageNumber) {
		setCurrentPage(pageNumber);
	}
	
	/**
	 * @see com.netfective.bluage.core.presentation.screen.paginator.IPageCommand#getMaxPage()
	 */
	public final int getMaxPage() {
		return maxPage;
	}
	
	/**
	 * @see com.netfective.bluage.core.presentation.screen.paginator.IPageCommand#setMaxPage(int)
	 */
	public final void setMaxPage(final int maxPage) {
		this.maxPage = maxPage;
	}
	
	/**
	 * @see com.netfective.bluage.core.presentation.screen.paginator.IPageCommand#getNumberOfCount()
	 */
	public int getNumberOfCount() {
		return numberOfCount;
	}
	
	/**
	 * @see com.netfective.bluage.core.presentation.screen.paginator.IPageCommand#setNumberOfCount(int)
	 */
	public final void setNumberOfCount(final int ofCount) {
		this.numberOfCount = ofCount;
	}


	/**
	 * Method setCurrentPage.
	 * @param currentPage int
	 * @see com.netfective.bluage.core.presentation.screen.paginator.IPageCommand#setCurrentPage(int)
	 */
	public void setCurrentPage(final int currentPage) {
		this.currentPage = currentPage;
	}
	/**
	 * Method getNumberOfItems.
	 * @return int
	 * @see com.netfective.bluage.core.presentation.screen.paginator.IPageCommand#getNumberOfItems()
	 */
	public int getNumberOfItems() {
		return numberOfItems;
	}

	/**
	 * Method setNumberOfItems.
	 * @param numberOfItems int
	 * @see com.netfective.bluage.core.presentation.screen.paginator.IPageCommand#setNumberOfItems(int)
	 */
	public void setNumberOfItems(final int numberOfItems) {
		this.numberOfItems = numberOfItems;
	}


	/**
	 * Method getCurrentPage.
	 * @return int
	 * @see com.netfective.bluage.core.presentation.screen.paginator.IPageCommand#getCurrentPage()
	 */
	public int getCurrentPage() {
		return currentPage;
	}
	
	/**
	 * Return the corresponding bean from the spring context
	 * 
	 * @param value
	 *            the bean identifier
	 */
	public static final Object getBean(final String value) {
		if (appContext == null) {
			appContext = AppContext.getApplicationContext();
		}
		return appContext.getBean(value);
	}
}
