/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.business.standardfeatures.entities.uc63_association.bos;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.inject.Inject;
import javax.inject.Named;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.LazyInitializationException;

import com.bluage.documentation.business.common.bos.TeamBO;
import com.bluage.documentation.business.standardfeatures.entities.uc63_association.entities.daofinder.Team63DAOFinderImpl;
import com.netfective.bluage.core.exception.ApplicationException;

/**
 * Team63BO
 *
* @see com.netfective.bluage.core.business.IBusinessObject
* * @see TeamBO
 */
@XmlRootElement

public class Team63BO
 extends TeamBO {
	
	/**
	 * Default SUID for Team63BO.
	 */
	private static final long serialVersionUID = 1L;
	

	@Inject
	@Named ("com.bluage.documentation.business.standardfeatures.entities.uc63_association.entities.daofinder.Team63Finder") 
	private Team63DAOFinderImpl finder;

												/**
	 * Entity property: state
	 *
	 */
private State63BO state;

											/**
	 * Entity property: coach
	 *
	 */
private Coach63BO coach;

																		/**
	 * Entity property: player63s
	 *
	 */
private Set<Player63BO> player63s = new HashSet<Player63BO>();



   /**
	* public default constructor for class Team63BO
	*/
	public Team63BO(){
		//NOP Team63BO
	}


												/**
	 * Getter for state of class Team63BO.
	 *
	 * @return {@link com.bluage.documentation.business.standardfeatures.entities.uc63_association.bos.State63BO}
	 * @generated
	 */ 
public  State63BO getState() {
		State63BO state0 = this.state;
		if(this.state == null) {
			if(DAO_MODE.equals(this.getObjMode( ) ) ) {
				state0 = null;
			} else {
				this.state = new State63BO();
				this.state.setObjMode( this.getObjMode( ) );
				state0 = this.state;
			}
		} else {
			this.state.setObjMode( this.getObjMode( ) );
			if( HELPER_MODE.equals(this.getObjMode( ) ) ) {
				state0 = this.state;
			}
			if( DAO_MODE.equals(this.getObjMode()) && this.state.isEmpty ) {
				state0 = null;
			}
		}
		return state0;
	}


	/**
	 * Setter for state of class Team63BO.
	 * @param value {@link com.bluage.documentation.business.standardfeatures.entities.uc63_association.bos.State63BO}
	 * @generated
	 */

public  void setState(final State63BO state) {
	if(state != null) {
		state.isEmpty = false;
		state.setObjMode( this.getObjMode( ) );
	}
	this.isEmpty = false;
	this.state = state;
}

											/**
	 * Getter for coach of class Team63BO.
	 *
	 * @return {@link com.bluage.documentation.business.standardfeatures.entities.uc63_association.bos.Coach63BO}
	 * @generated
	 */ 
public  Coach63BO getCoach() {
		Coach63BO coach0 = this.coach;
		if(this.coach == null) {
			if(DAO_MODE.equals(this.getObjMode( ) ) ) {
				coach0 = null;
			} else {
				this.coach = new Coach63BO();
				this.coach.setObjMode( this.getObjMode( ) );
				coach0 = this.coach;
			}
		} else {
			this.coach.setObjMode( this.getObjMode( ) );
			if( HELPER_MODE.equals(this.getObjMode( ) ) ) {
				coach0 = this.coach;
			}
			if( DAO_MODE.equals(this.getObjMode()) && this.coach.isEmpty ) {
				coach0 = null;
			}
		}
		return coach0;
	}


	/**
	 * Setter for coach of class Team63BO.
	 * @param value {@link com.bluage.documentation.business.standardfeatures.entities.uc63_association.bos.Coach63BO}
	 * @generated
	 */

public  void setCoach(final Coach63BO coach) {
	if(coach != null) {
		coach.isEmpty = false;
		coach.setObjMode( this.getObjMode( ) );
	}
	this.isEmpty = false;
	this.coach = coach;
}

																		/**
	 * Getter for player63s of class Team63BO.
	 *
	 * @return {@link java.util.Set<com.bluage.documentation.business.standardfeatures.entities.uc63_association.bos.Player63BO>}
	 * @generated
	 */ 
public  Set<Player63BO> getPlayer63s() {
	return this.player63s;
			}


	/**
	 * Setter for player63s of class Team63BO.
	 * @param value {@link java.util.Set<com.bluage.documentation.business.standardfeatures.entities.uc63_association.bos.Player63BO>}
	 * @generated
	 */

public  void setPlayer63s(final Set<Player63BO> player63s) {
	this.player63s = player63s;
}

	/**
	 * Remove an entity from the com.bluage.documentation.business.standardfeatures.entities.uc63_association.bos.Player63BO collection.
	 *
	 * @param entity
	 *		: {@link Set<Player63BO>} the entity
	 * @throws ApplicationException
     */
public void removeFromPlayer63s( Player63BO entity ) throws ApplicationException {
	try {
		if( getPlayer63s() != null && getPlayer63s().contains(entity) ) {
			getPlayer63s().remove( entity );
		}
	} catch ( LazyInitializationException e ) {
				Team63BO bo = ( Team63BO ) finder.findByPrimaryKey( getId() );
			finder.getHibernateTemplate().initialize( bo.getPlayer63s() );
			setPlayer63s( bo.getPlayer63s() );
			// evict object from session to avoid NonUniqueObjectException
			finder.getHibernateTemplate().evict( bo );
		
			if( getPlayer63s() != null && getPlayer63s().contains( entity ) ) {
				getPlayer63s().remove( entity );
			}
		}
	}
	
	/**
	 * Add entity to the com.bluage.documentation.business.standardfeatures.entities.uc63_association.bos.Player63BO collection.
	 *
	 * @param entity
	 *		{@link Player63BO} the entity
	 * @throws ApplicationException
     */
public void addToPlayer63s( Player63BO entity ) throws ApplicationException
	{
		try {
			if( getPlayer63s() != null && !getPlayer63s().contains( entity ) ) {
				getPlayer63s().add( entity );
			}
		} catch ( LazyInitializationException e ) {
							Team63BO bo = ( Team63BO ) finder.findByPrimaryKey( getId() );
		finder.getHibernateTemplate().initialize( bo.getPlayer63s() );
		setPlayer63s( bo.getPlayer63s() );
		// evict object from session to avoid NonUniqueObjectException
		finder.getHibernateTemplate().evict( bo );
		if( getPlayer63s() != null && !getPlayer63s().contains( entity ) ) {
			getPlayer63s().add( entity );
		}
	}
	}
	
	/**
	 * Add entitys to the com.bluage.documentation.business.standardfeatures.entities.uc63_association.bos.Player63BO collection
	 *
	 * @param entities			
	 *		: {@link java.util.List} A list of entities
	 * @throws ApplicationException 
     */	
public void addAllToPlayer63s( Collection<Player63BO> entities ) throws ApplicationException {
	for( Player63BO entity : entities) {
		if( getPlayer63s()!=null ) {
			addToPlayer63s( entity );
		}
	}
}
	
	/**
	 * Remove entities form the com.bluage.documentation.business.standardfeatures.entities.uc63_association.bos.Player63BO collection
	 *
	 * @param entities				
	 *		: {@link java.util.List} a list of entities
	 * @throws ApplicationException
     */	
public void removeAllFromPlayer63s( Collection<Player63BO> entities ) throws ApplicationException {
	for( Player63BO entity : entities) {
		if( getPlayer63s()!=null && getPlayer63s().contains( entity ) )	{		
			removeFromPlayer63s( entity );
		}
	}
}
	
	/**
	 * Remove all items form the collection com.bluage.documentation.business.standardfeatures.entities.uc63_association.bos.Player63BO
	 *
	 * @throws ApplicationException
     */	
public void removeAllFromPlayer63s() throws ApplicationException {
	Set<Player63BO> entities = this.getPlayer63s();
	if ( entities == null ) {
		return;
	}
	entities.clear();
}

/**
	 * Returns <code>true</code> if the argument is an Team63 instance and all identifiers for this entity
     * equal the identifiers of the argument entity. The <code>equals</code> method of the parent entity
     * will also need to return <code>true</code>. Returns <code>false</code> otherwise.
     *
     * @see TeamBO#hashCode()
	 * @param object  object to compare with current instance      
     * @return boolean true if equals false if not
     */
    public boolean equals(final Object object )
    {
		if ( this == object )
        {
            return true;
        }
        if ( !( object instanceof Team63BO ) )
        {
            return false;
        }
        final Team63BO that = (Team63BO)object;
    
    	            if (        				        			this.getId() == null
        	        ) {
        	return false;
        }
		return new EqualsBuilder()
			.append(this.getId(), that.getId())
			 	.isEquals();
	}
	/**

	 * Returns a hash code based on this entity's identifiers and the hash code of the parent entity.
     *
     * @see TeamBO#hashCode()
     * @return int
     */    
    public int hashCode() {
        return new HashCodeBuilder()
             .append(getId())
				 	.appendSuper(super.hashCode())
            .toHashCode();
    }
}

