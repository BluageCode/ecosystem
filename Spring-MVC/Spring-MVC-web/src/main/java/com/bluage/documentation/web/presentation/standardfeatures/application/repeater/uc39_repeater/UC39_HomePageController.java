/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.application.repeater.uc39_repeater ;

// Import declaration.
// Java imports.
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.bluage.documentation.service.standardfeatures.application.repeater.uc39_repeater.ServiceTeam39;
import com.netfective.bluage.core.exception.ApplicationException;
import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;
import com.netfective.bluage.core.web.IForm;

/**
 * Class : UC39_HomePageController
 */
@Controller
@Scope("prototype")
@SessionAttributes("uC39_HomePageForm")
@RequestMapping(value= "/presentation/standardfeatures/application/repeater/uc39_repeater")
public class UC39_HomePageController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC39_HomePageController.class);
	
	//Current form name
	private static final String U_C39__HOME_PAGE_FORM = "uC39_HomePageForm";

	//CONSTANT: teams1
	private static final String TEAMS1 = "teams1";
	//CONSTANT: teams2
	private static final String TEAMS2 = "teams2";
	//CONSTANT: teams3
	private static final String TEAMS3 = "teams3";
	
	/**
	 * Property:customDateEditorsUC39_HomePageController
	 */
	final private Map<String,CustomDateEditor> customDateEditorsUC39_HomePageController = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:uC39_HomePageValidator
	 */
	final private UC39_HomePageValidator uC39_HomePageValidator = new UC39_HomePageValidator();
	
	/**
	 * Service declaration : serviceTeam39.
	 */
	@Autowired
	private ServiceTeam39 serviceTeam39;
	
	
	// Initialise all the instances for UC39_HomePageController
		// Declare the instance teams1
		private List teams1;
		// Declare the instance teams2
		private List teams2;
		// Declare the instance teams3
		private List teams3;
			/**
	 * Operation : lnk_loadTeams
 	 * @param model : 
 	 * @param uC39_HomePage : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC39_HomePage/lnk_loadTeams.html",method = RequestMethod.POST)
	public String lnk_loadTeams(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC39_HomePageForm") UC39_HomePageForm  uC39_HomePageForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : lnk_loadTeams"); 
		infoLogger(uC39_HomePageForm); 
		uC39_HomePageForm.setAlwaysCallPreControllers(false); 
		// initialization for lnk_loadTeams
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state lnk_loadTeams if the validation fail, you will be forward to the screen : "/presentation/standardfeatures/application/repeater/uc39_repeater/UC39_HomePage"
		if(errorsMessages(request,response,uC39_HomePageForm,bindingResult,"", uC39_HomePageValidator, customDateEditorsUC39_HomePageController)){ 
			return "/presentation/standardfeatures/application/repeater/uc39_repeater/UC39_HomePage"; 
		}

	 callLoadTeam1(request,uC39_HomePageForm );
			
 callLoadTeam2(request,uC39_HomePageForm );
			
 callLoadTeam3(request,uC39_HomePageForm );
											// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.application.repeater.uc39_repeater.UC39_RepeaterForm or not from lnk_loadTeams
			final UC39_RepeaterForm uC39_RepeaterForm =  new UC39_RepeaterForm();
			// Populate the destination form with all the instances returned from lnk_loadTeams.
			final IForm uC39_RepeaterForm2 = populateDestinationForm(request.getSession(), uC39_RepeaterForm); 
					
			// Add the form to the model.
			model.addAttribute("uC39_RepeaterForm", uC39_RepeaterForm2); 
			
			request.getSession().setAttribute("uC39_RepeaterForm", uC39_RepeaterForm2);
			
			// "OK" CASE => destination screen path from lnk_loadTeams
			LOGGER.info("Go to the screen 'UC39_Repeater'.");
			// Redirect (PRG) from lnk_loadTeams
			destinationPath =  "redirect:/presentation/standardfeatures/application/repeater/uc39_repeater/UC39_Repeater.html";
						
						
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_loadTeams 
	}
	
	
	/**
	* This method initialise the form : UC39_HomePageForm 
	* @return UC39_HomePageForm
	*/
	@ModelAttribute("UC39_HomePageFormInit")
	public void initUC39_HomePageForm(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, U_C39__HOME_PAGE_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method UC39_HomePageForm."); //for lnk_loadTeams 
		}
		UC39_HomePageForm uC39_HomePageForm;
	
		if(request.getSession().getAttribute(U_C39__HOME_PAGE_FORM) != null){
			uC39_HomePageForm = (UC39_HomePageForm)request.getSession().getAttribute(U_C39__HOME_PAGE_FORM);
		} else {
			uC39_HomePageForm = new UC39_HomePageForm();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form UC39_HomePageForm.");
		}
		uC39_HomePageForm = (UC39_HomePageForm)populateDestinationForm(request.getSession(), uC39_HomePageForm);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  UC39_HomePageForm.");
		}
		model.addAttribute(U_C39__HOME_PAGE_FORM, uC39_HomePageForm);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param UC39_HomePageForm : The sceen form.
	 */
	@RequestMapping(value = "/UC39_HomePage.html" ,method = RequestMethod.GET)
	public void prepareUC39_HomePage(final HttpServletRequest request,final  Model model,final  @ModelAttribute("UC39_HomePageFormInit") UC39_HomePageForm uC39_HomePageForm){
		
		UC39_HomePageForm currentUC39_HomePageForm = uC39_HomePageForm;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(U_C39__HOME_PAGE_FORM) == null){
			if(currentUC39_HomePageForm!=null){
				request.getSession().setAttribute(U_C39__HOME_PAGE_FORM, currentUC39_HomePageForm);
			}else {
				currentUC39_HomePageForm = new UC39_HomePageForm();
				request.getSession().setAttribute(U_C39__HOME_PAGE_FORM, currentUC39_HomePageForm);	
			}
		} else {
			currentUC39_HomePageForm = (UC39_HomePageForm) request.getSession().getAttribute(U_C39__HOME_PAGE_FORM);
		}

		currentUC39_HomePageForm = (UC39_HomePageForm)populateDestinationForm(request.getSession(), currentUC39_HomePageForm);
		// Call all the Precontroller.
	currentUC39_HomePageForm.setAlwaysCallPreControllers(true);
		model.addAttribute(U_C39__HOME_PAGE_FORM, currentUC39_HomePageForm);
		request.getSession().setAttribute(U_C39__HOME_PAGE_FORM, currentUC39_HomePageForm);
		
	}
	
	/**
	 * method callLoadTeam1
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callLoadTeam1(HttpServletRequest request,IForm form) {
		teams1 = (List)get(request.getSession(),form, "teams1");  
										 
					try {
				// executing LoadTeam1 in lnk_loadTeams
				teams1 = 	serviceTeam39.findAllTeam39(
	);  
 
				put(request.getSession(), TEAMS1,teams1);
								// processing variables LoadTeam1 in lnk_loadTeams

			} catch (ApplicationException e) { 
				// error handling for operation findAllTeam39 called LoadTeam1
				errorLogger("An error occured during the execution of the operation : findAllTeam39",e); 
		
			}
	}
		/**
	 * method callLoadTeam2
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callLoadTeam2(HttpServletRequest request,IForm form) {
		teams2 = (List)get(request.getSession(),form, "teams2");  
										 
					try {
				// executing LoadTeam2 in lnk_loadTeams
				teams2 = 	serviceTeam39.findAllTeam39(
	);  
 
				put(request.getSession(), TEAMS2,teams2);
								// processing variables LoadTeam2 in lnk_loadTeams

			} catch (ApplicationException e) { 
				// error handling for operation findAllTeam39 called LoadTeam2
				errorLogger("An error occured during the execution of the operation : findAllTeam39",e); 
		
			}
	}
		/**
	 * method callLoadTeam3
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callLoadTeam3(HttpServletRequest request,IForm form) {
		teams3 = (List)get(request.getSession(),form, "teams3");  
										 
					try {
				// executing LoadTeam3 in lnk_loadTeams
				teams3 = 	serviceTeam39.findAllTeam39(
	);  
 
				put(request.getSession(), TEAMS3,teams3);
								// processing variables LoadTeam3 in lnk_loadTeams

			} catch (ApplicationException e) { 
				// error handling for operation findAllTeam39 called LoadTeam3
				errorLogger("An error occured during the execution of the operation : findAllTeam39",e); 
		
			}
	}
		
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("UC39_HomePageController [ ");
			strBToS.append(TEAMS1);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(teams1);
			strBToS.append(END_TO_STRING_DELIMITER);
			strBToS.append(TEAMS2);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(teams2);
			strBToS.append(END_TO_STRING_DELIMITER);
			strBToS.append(TEAMS3);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(teams3);
			strBToS.append(END_TO_STRING_DELIMITER);
		// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(uC39_HomePageValidator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsUC39_HomePageController!=null); 
		return strBToS.toString();
	}
}
