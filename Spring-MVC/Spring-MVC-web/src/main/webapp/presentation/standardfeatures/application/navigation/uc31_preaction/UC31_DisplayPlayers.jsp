<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file="/include.jsp"  %>
<html >
<head >
<title  > 
<fmt:message key="cUC31_DisplayPlayers_BLU_AGE_DOCUMENTATION"/>
</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"  > 
</meta>
<link href="<c:url value= "/presentation/_style/_style.css"/>" rel="stylesheet" type="text/css"  /> 
<link href="<c:url value= "/presentation/_include/jquery/rd_lib.css"/>" rel="stylesheet" type="text/css"  /> 
<script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/jquery.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/rd_setup.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/rd_lib.js"/>" space="preserve"  ></script><link rel="shortcut icon" href="<c:url value= "/presentation/_style/favicon.ico"/>"  /> 

</head>

<body class=" " > 
<div id="rd_ghost"  > 
<small  > 
<fmt:message key="cUC31_DisplayPlayers___CSS_DEACTIVATED"/>
</small>
</div>
<div id="rd_jghost"  > 
<small  > 
<fmt:message key="cUC31_DisplayPlayers___JAVASCRIPT_DEACTIVATED"/>
</small>
</div>
<div id="shape"  > 
<div class="ui-layout-center"  > 
<div class="east-north"  > 
	<form:form id="uC31_DisplayPlayers" commandName="uC31_DisplayPlayersForm"  enctype="application/x-www-form-urlencoded" method="post">
<h2 class="rd_first"  > 
<fmt:message key="cUC31_DisplayPlayers_STANDARD_FEATURES__APPLICATIO"/>
</h2>
<display:table uid="tab_searchResult" pagesize="0"  sort="list" requestURI="" name="sessionScope.uC31_DisplayPlayersForm.playersList" class="rd_tabledata"  >
	<c:set   var="rownum">
		<%=(tab_searchResult !=	null) ? new Integer(tab_searchResult_rowNum.intValue() - 1) : new Integer(0) %>
	</c:set>
	<c:set   var="idRow">
		<%=(tab_searchResult != null ) ? tab_searchResult_rowNum.intValue() - 1 : -1 %>
	</c:set>
	<c:set   var="idTab">
		<%=(tab_searchResult != null ) ? "tab_searchResult" : null %>
	</c:set>
	<c:set var="tableHashId">
		<%= new org.displaytag.util.ParamEncoder("tab_searchResult").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE) %>
	</c:set>
	<c:set var="tableHashId2">
		<%=request.getParameter(new org.displaytag.util.ParamEncoder("tab_searchResult").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE)) %>
	</c:set>	
	
<display:column format=""  titleKey="cUC31_DisplayPlayers_LAST_NAME">
		<span id="txt_lastname1">
	<c:out value="${tab_searchResult.lastName}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="cUC31_DisplayPlayers_FIRST_NAME">
		<span id="txt_firstname1">
	<c:out value="${tab_searchResult.firstName}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="cUC31_DisplayPlayers_POSITION">
		<span id="txt_position">
	<c:out value="${tab_searchResult.position.name}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="cUC31_DisplayPlayers_HEIGHT_CM">
		<span id="txt_height">
	<c:out value="${tab_searchResult.height}" />
		</span>
	</display:column>
</display:table>
	</form:form>
	<script language="javascript">
		function btn_click_cExecuteUC31_DisplayPlayers(url) {
			UC31_DisplayPlayersvar = document.getElementById('uC31_DisplayPlayers');
			UC31_DisplayPlayersvar.setAttribute('action', url);
			UC31_DisplayPlayersvar.submit();
		}
		function btn_click_cSetActionUC31_DisplayPlayers(url) {
			UC31_DisplayPlayersvar = document.getElementById('uC31_DisplayPlayers');
			UC31_DisplayPlayersvar.setAttribute('action', url);
		}
	</script>
</div>
<div class="east-center"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/application/navigation/uc31_preaction/UC31_DisplayPlayerscmp_tabs" />
</div>
</div>
<div class="ui-layout-north"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/application/navigation/uc31_preaction/UC31_DisplayPlayerscmp_header" />
</div>
<div class="ui-layout-south"  > 
<fmt:message key="cUC31_DisplayPlayers___"/>
<span id="rd_jversion"  > 
<fmt:message key="cUC31_DisplayPlayers_BLU_AGE_DOCUMENTATION_126"/>
</span>
</div>
<div class="ui-layout-west"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/application/navigation/uc31_preaction/UC31_DisplayPlayerscmp_treeview" />
</div>
</div>
</body>
</html>
