/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.business.standardfeatures.application.component.uc47_contents.bos;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.inject.Inject;
import javax.inject.Named;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.LazyInitializationException;

import com.bluage.documentation.business.common.bos.TeamBO;
import com.bluage.documentation.business.standardfeatures.application.component.uc47_contents.entities.daofinder.Team47DAOFinderImpl;
import com.netfective.bluage.core.exception.ApplicationException;

/**
 * Team47BO
 *
* @see com.netfective.bluage.core.business.IBusinessObject
* * @see TeamBO
 */
@XmlRootElement

public class Team47BO
 extends TeamBO {
	
	/**
	 * Default SUID for Team47BO.
	 */
	private static final long serialVersionUID = 1L;
	

	@Inject
	@Named ("com.bluage.documentation.business.standardfeatures.application.component.uc47_contents.entities.daofinder.Team47Finder") 
	private Team47DAOFinderImpl finder;

																			/**
	 * Entity property: playerTeam
	 *
	 */
private Set<PlayerTeam47BO> playerTeam = new HashSet<PlayerTeam47BO>();



   /**
	* public default constructor for class Team47BO
	*/
	public Team47BO(){
		//NOP Team47BO
	}


																			/**
	 * Getter for playerTeam of class Team47BO.
	 *
	 * @return {@link java.util.Set<com.bluage.documentation.business.standardfeatures.application.component.uc47_contents.bos.PlayerTeam47BO>}
	 * @generated
	 */ 
public  Set<PlayerTeam47BO> getPlayerTeam() {
	return this.playerTeam;
			}


	/**
	 * Setter for playerTeam of class Team47BO.
	 * @param value {@link java.util.Set<com.bluage.documentation.business.standardfeatures.application.component.uc47_contents.bos.PlayerTeam47BO>}
	 * @generated
	 */

public  void setPlayerTeam(final Set<PlayerTeam47BO> playerTeam) {
	this.playerTeam = playerTeam;
}

	/**
	 * Remove an entity from the com.bluage.documentation.business.standardfeatures.application.component.uc47_contents.bos.PlayerTeam47BO collection.
	 *
	 * @param entity
	 *		: {@link Set<PlayerTeam47BO>} the entity
	 * @throws ApplicationException
     */
public void removeFromPlayerTeam( PlayerTeam47BO entity ) throws ApplicationException {
	try {
		if( getPlayerTeam() != null && getPlayerTeam().contains(entity) ) {
			getPlayerTeam().remove( entity );
		entity.setTeam( null );
		}
		} catch ( LazyInitializationException e ) {
				Team47BO bo = ( Team47BO ) finder.findByPrimaryKey( getId() );
			finder.getHibernateTemplate().initialize( bo.getPlayerTeam() );
			setPlayerTeam( bo.getPlayerTeam() );
			// evict object from session to avoid NonUniqueObjectException
			finder.getHibernateTemplate().evict( bo );
		
			if( getPlayerTeam() != null && getPlayerTeam().contains( entity ) ) {
				getPlayerTeam().remove( entity );
				entity.setTeam( null );
			}
				}
	}
	
	/**
	 * Add entity to the com.bluage.documentation.business.standardfeatures.application.component.uc47_contents.bos.PlayerTeam47BO collection.
	 *
	 * @param entity
	 *		{@link PlayerTeam47BO} the entity
	 * @throws ApplicationException
     */
public void addToPlayerTeam( PlayerTeam47BO entity ) throws ApplicationException
	{
		try {
			if( getPlayerTeam() != null && !getPlayerTeam().contains( entity ) ) {
				getPlayerTeam().add( entity );
				entity.setTeam( this );
			}
			} catch ( LazyInitializationException e ) {
			Team47BO bo = ( Team47BO ) finder.findByPrimaryKey( getId() );
		finder.getHibernateTemplate().initialize( bo.getPlayerTeam() );
		setPlayerTeam( bo.getPlayerTeam() );
		// evict object from session to avoid NonUniqueObjectException
		finder.getHibernateTemplate().evict( bo );
		if( getPlayerTeam() != null && !getPlayerTeam().contains( entity ) ) {
			getPlayerTeam().add( entity );
			entity.setTeam( this );
		}
			}
	}
	
	/**
	 * Add entitys to the com.bluage.documentation.business.standardfeatures.application.component.uc47_contents.bos.PlayerTeam47BO collection
	 *
	 * @param entities			
	 *		: {@link java.util.List} A list of entities
	 * @throws ApplicationException 
     */	
public void addAllToPlayerTeam( Collection<PlayerTeam47BO> entities ) throws ApplicationException {
	for( PlayerTeam47BO entity : entities) {
		if( getPlayerTeam()!=null ) {
			addToPlayerTeam( entity );
		}
	}
}
	
	/**
	 * Remove entities form the com.bluage.documentation.business.standardfeatures.application.component.uc47_contents.bos.PlayerTeam47BO collection
	 *
	 * @param entities				
	 *		: {@link java.util.List} a list of entities
	 * @throws ApplicationException
     */	
public void removeAllFromPlayerTeam( Collection<PlayerTeam47BO> entities ) throws ApplicationException {
	for( PlayerTeam47BO entity : entities) {
		if( getPlayerTeam()!=null && getPlayerTeam().contains( entity ) )	{		
			removeFromPlayerTeam( entity );
		}
	}
}
	
	/**
	 * Remove all items form the collection com.bluage.documentation.business.standardfeatures.application.component.uc47_contents.bos.PlayerTeam47BO
	 *
	 * @throws ApplicationException
     */	
public void removeAllFromPlayerTeam() throws ApplicationException {
	Set<PlayerTeam47BO> entities = this.getPlayerTeam();
	if ( entities == null ) {
		return;
	}
	Iterator<PlayerTeam47BO> it = entities.iterator();
	while ( it.hasNext() ) {
    	PlayerTeam47BO entity = it.next(); 
		entity.setTeam( null ); 
	}
	entities.clear();
}

/**
	 * Returns <code>true</code> if the argument is an Team47 instance and all identifiers for this entity
     * equal the identifiers of the argument entity. The <code>equals</code> method of the parent entity
     * will also need to return <code>true</code>. Returns <code>false</code> otherwise.
     *
     * @see TeamBO#hashCode()
	 * @param object  object to compare with current instance      
     * @return boolean true if equals false if not
     */
    public boolean equals(final Object object )
    {
		if ( this == object )
        {
            return true;
        }
        if ( !( object instanceof Team47BO ) )
        {
            return false;
        }
        final Team47BO that = (Team47BO)object;
    
    	            if (        				        			this.getId() == null
        	        ) {
        	return false;
        }
		return new EqualsBuilder()
			.append(this.getId(), that.getId())
			 	.isEquals();
	}
	/**

	 * Returns a hash code based on this entity's identifiers and the hash code of the parent entity.
     *
     * @see TeamBO#hashCode()
     * @return int
     */    
    public int hashCode() {
        return new HashCodeBuilder()
             .append(getId())
				 	.appendSuper(super.hashCode())
            .toHashCode();
    }
}

