/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.service.standardfeatures.services.process.uc53_while;


import javax.inject.Inject;
import javax.inject.Named;

import com.bluage.documentation.business.standardfeatures.services.process.uc53_while.bos.Player53BO;
import com.bluage.documentation.service.AbstractService;
import com.netfective.bluage.core.exception.ApplicationException;

/**
 * Defines  application services for ServicePlayer53
  */
@Named("com.bluage.documentation.service.standardfeatures.services.process.uc53_while.ServicePlayer53")
public class ServicePlayer53  extends AbstractService{


					@Inject
		@Named ("com.bluage.documentation.service.standardfeatures.services.process.uc53_while.ServicePlayer53FindByID")
		private ServicePlayer53FindByID  instanceServicePlayer53FindByID;
									


	/**
	 *  Default constructor 
	 */
	public ServicePlayer53() {
		super(); // Call super for default constructor : ServicePlayer53
	}
	



			
	/**    
	 * Process operation.
	 * @param player {@link Player53BO}
		 * @return {@link void}
	 */
	public void trainPlayer(Player53BO player )
 throws ApplicationException {

			privateTrainPlayer(player);
	}
	

	/**    
	 * Process operation.
	 * @param player {@link Player53BO}
		 * @return {@link void}
	 */
	private void privateTrainPlayer(Player53BO player )
 throws ApplicationException {
		// Central buffer node declaration for operation trainPlayer
			Integer	 matches =  Integer.valueOf(0);
					/* Executing : playerNeedTraining = true*/
boolean playerNeedTraining = true;
while (playerNeedTraining) {
// Calling an operation : 
matches = player.getMatches();
if (matches.intValue() >= 50) {
// Calling an operation : 
player.setRookie(Boolean.FALSE);
/* Executing : playerNeedTraining = false*/
playerNeedTraining = false;
} else {
/* Executing : matches = matches + 1*/
matches = new Integer(matches.intValue() + 1);
// Calling an operation : 
player.setMatches(matches);
}
}

		}

		
			
	/**    
	 * Process operation.
	 * @return {@link Player53BO}
	 */
	public Player53BO findFirstPlayer( )
 throws ApplicationException {

				return privateFindFirstPlayer();
	}
	

	/**    
	 * Process operation.
	 * @return {@link Player53BO}
	 */
	private Player53BO privateFindFirstPlayer( )
 throws ApplicationException {
				// Return Type declaration.
				Player53BO player = new Player53BO();
					// Central buffer node declaration for operation findFirstPlayer
				// Calling an operation : 
player.setId(new Long(1));
// Calling an operation : 
player = instanceServicePlayer53FindByID.player53FindByID(player);

						return player;
		}

		}
