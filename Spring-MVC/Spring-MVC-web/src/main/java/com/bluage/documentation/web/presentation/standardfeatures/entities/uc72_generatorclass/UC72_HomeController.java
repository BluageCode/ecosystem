/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.entities.uc72_generatorclass ;

// Import declaration.
// Java imports.
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.bluage.documentation.service.standardfeatures.entities.uc72_generatorclass.ServicePlayer72;
import com.bluage.documentation.service.standardfeatures.entities.uc72_generatorclass.ServiceTeam72;
import com.netfective.bluage.core.exception.ApplicationException;
import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;
import com.netfective.bluage.core.web.IForm;

/**
 * Class : UC72_HomeController
 */
@Controller
@Scope("prototype")
@SessionAttributes("uC72_HomeForm")
@RequestMapping(value= "/presentation/standardfeatures/entities/uc72_generatorclass")
public class UC72_HomeController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC72_HomeController.class);
	
	//Current form name
	private static final String U_C72__HOME_FORM = "uC72_HomeForm";

	//CONSTANT: listPlayers
	private static final String LIST_PLAYERS = "listPlayers";
	//CONSTANT: listTeams
	private static final String LIST_TEAMS = "listTeams";
	
	/**
	 * Property:customDateEditorsUC72_HomeController
	 */
	final private Map<String,CustomDateEditor> customDateEditorsUC72_HomeController = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:uC72_HomeValidator
	 */
	final private UC72_HomeValidator uC72_HomeValidator = new UC72_HomeValidator();
	
	/**
	 * Service declaration : servicePlayer72.
	 */
	@Autowired
	private ServicePlayer72 servicePlayer72;
	
	/**
	 * Service declaration : serviceTeam72.
	 */
	@Autowired
	private ServiceTeam72 serviceTeam72;
	
	
	// Initialise all the instances for UC72_HomeController
		// Declare the instance listPlayers
		private List listPlayers;
		// Declare the instance listTeams
		private List listTeams;
					/**
	 * Operation : lnk_addPlayer
 	 * @param model : 
 	 * @param uC72_Home : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC72_Home/lnk_addPlayer.html",method = RequestMethod.GET)
	public String lnk_addPlayer(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC72_HomeForm") UC72_HomeForm  uC72_HomeForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : lnk_addPlayer"); 
		infoLogger(uC72_HomeForm); 
		uC72_HomeForm.setAlwaysCallPreControllers(false); 
		// initialization for lnk_addPlayer
		init();
		
		String destinationPath = null; 
		

	 callFindallplayers(request,uC72_HomeForm );
											// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.entities.uc72_generatorclass.UC72_AddPlayerForm or not from lnk_addPlayer
			final UC72_AddPlayerForm uC72_AddPlayerForm =  new UC72_AddPlayerForm();
			// Populate the destination form with all the instances returned from lnk_addPlayer.
			final IForm uC72_AddPlayerForm2 = populateDestinationForm(request.getSession(), uC72_AddPlayerForm); 
					
			// Add the form to the model.
			model.addAttribute("uC72_AddPlayerForm", uC72_AddPlayerForm2); 
			
			request.getSession().setAttribute("uC72_AddPlayerForm", uC72_AddPlayerForm2);
			
			// "OK" CASE => destination screen path from lnk_addPlayer
			LOGGER.info("Go to the screen 'UC72_AddPlayer'.");
			// Redirect (PRG) from lnk_addPlayer
			destinationPath =  "redirect:/presentation/standardfeatures/entities/uc72_generatorclass/UC72_AddPlayer.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_addPlayer 
	}
	
						/**
	 * Operation : lnk_addTeam
 	 * @param model : 
 	 * @param uC72_Home : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC72_Home/lnk_addTeam.html",method = RequestMethod.GET)
	public String lnk_addTeam(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC72_HomeForm") UC72_HomeForm  uC72_HomeForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : lnk_addTeam"); 
		infoLogger(uC72_HomeForm); 
		uC72_HomeForm.setAlwaysCallPreControllers(false); 
		// initialization for lnk_addTeam
		init();
		
		String destinationPath = null; 
		

	 callFindallteams(request,uC72_HomeForm );
											// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.entities.uc72_generatorclass.UC72_AddTeamForm or not from lnk_addTeam
			final UC72_AddTeamForm uC72_AddTeamForm =  new UC72_AddTeamForm();
			// Populate the destination form with all the instances returned from lnk_addTeam.
			final IForm uC72_AddTeamForm2 = populateDestinationForm(request.getSession(), uC72_AddTeamForm); 
					
			// Add the form to the model.
			model.addAttribute("uC72_AddTeamForm", uC72_AddTeamForm2); 
			
			request.getSession().setAttribute("uC72_AddTeamForm", uC72_AddTeamForm2);
			
			// "OK" CASE => destination screen path from lnk_addTeam
			LOGGER.info("Go to the screen 'UC72_AddTeam'.");
			// Redirect (PRG) from lnk_addTeam
			destinationPath =  "redirect:/presentation/standardfeatures/entities/uc72_generatorclass/UC72_AddTeam.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_addTeam 
	}
	
	
	/**
	* This method initialise the form : UC72_HomeForm 
	* @return UC72_HomeForm
	*/
	@ModelAttribute("UC72_HomeFormInit")
	public void initUC72_HomeForm(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, U_C72__HOME_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method UC72_HomeForm."); //for lnk_addTeam 
		}
		UC72_HomeForm uC72_HomeForm;
	
		if(request.getSession().getAttribute(U_C72__HOME_FORM) != null){
			uC72_HomeForm = (UC72_HomeForm)request.getSession().getAttribute(U_C72__HOME_FORM);
		} else {
			uC72_HomeForm = new UC72_HomeForm();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form UC72_HomeForm.");
		}
		uC72_HomeForm = (UC72_HomeForm)populateDestinationForm(request.getSession(), uC72_HomeForm);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  UC72_HomeForm.");
		}
		model.addAttribute(U_C72__HOME_FORM, uC72_HomeForm);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param UC72_HomeForm : The sceen form.
	 */
	@RequestMapping(value = "/UC72_Home.html" ,method = RequestMethod.GET)
	public void prepareUC72_Home(final HttpServletRequest request,final  Model model,final  @ModelAttribute("UC72_HomeFormInit") UC72_HomeForm uC72_HomeForm){
		
		UC72_HomeForm currentUC72_HomeForm = uC72_HomeForm;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(U_C72__HOME_FORM) == null){
			if(currentUC72_HomeForm!=null){
				request.getSession().setAttribute(U_C72__HOME_FORM, currentUC72_HomeForm);
			}else {
				currentUC72_HomeForm = new UC72_HomeForm();
				request.getSession().setAttribute(U_C72__HOME_FORM, currentUC72_HomeForm);	
			}
		} else {
			currentUC72_HomeForm = (UC72_HomeForm) request.getSession().getAttribute(U_C72__HOME_FORM);
		}

		currentUC72_HomeForm = (UC72_HomeForm)populateDestinationForm(request.getSession(), currentUC72_HomeForm);
		// Call all the Precontroller.
	currentUC72_HomeForm.setAlwaysCallPreControllers(true);
		model.addAttribute(U_C72__HOME_FORM, currentUC72_HomeForm);
		request.getSession().setAttribute(U_C72__HOME_FORM, currentUC72_HomeForm);
		
	}
	
	/**
	 * method callFindallplayers
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callFindallplayers(HttpServletRequest request,IForm form) {
		listPlayers = (List)get(request.getSession(),form, "listPlayers");  
										 
					try {
				// executing Findallplayers in lnk_addTeam
				listPlayers = 	servicePlayer72.player72FindAll(
	);  
 
				put(request.getSession(), LIST_PLAYERS,listPlayers);
								// processing variables Findallplayers in lnk_addTeam

			} catch (ApplicationException e) { 
				// error handling for operation player72FindAll called Findallplayers
				errorLogger("An error occured during the execution of the operation : player72FindAll",e); 
		
			}
	}
		/**
	 * method callFindallteams
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callFindallteams(HttpServletRequest request,IForm form) {
		listTeams = (List)get(request.getSession(),form, "listTeams");  
										 
					try {
				// executing Findallteams in lnk_addTeam
				listTeams = 	serviceTeam72.team72FindAll(
	);  
 
				put(request.getSession(), LIST_TEAMS,listTeams);
								// processing variables Findallteams in lnk_addTeam

			} catch (ApplicationException e) { 
				// error handling for operation team72FindAll called Findallteams
				errorLogger("An error occured during the execution of the operation : team72FindAll",e); 
		
			}
	}
		
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("UC72_HomeController [ ");
			strBToS.append(LIST_PLAYERS);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(listPlayers);
			strBToS.append(END_TO_STRING_DELIMITER);
			strBToS.append(LIST_TEAMS);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(listTeams);
			strBToS.append(END_TO_STRING_DELIMITER);
		// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(uC72_HomeValidator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsUC72_HomeController!=null); 
		return strBToS.toString();
	}
}
