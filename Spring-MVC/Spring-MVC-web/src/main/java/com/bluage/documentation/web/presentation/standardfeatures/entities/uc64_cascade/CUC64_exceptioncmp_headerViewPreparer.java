/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.web.presentation.standardfeatures.entities.uc64_cascade;

import org.apache.log4j.Logger;
import org.apache.tiles.AttributeContext;
import org.apache.tiles.context.TilesRequestContext;
import org.apache.tiles.preparer.ViewPreparer;

import com.bluage.documentation.web.common.components.cmp_headerForm;
import com.netfective.bluage.core.common.IStringConstants;
/**
 * Class : CUC64_exceptioncmp_headerViewPreparer
 * Prepare the component cmp_header for the screen UC64_exception.
 * 
 */
public class CUC64_exceptioncmp_headerViewPreparer implements ViewPreparer , IStringConstants{

	private static final String UC64_EXCEPTIONCMP_HEADER = "cmp_headerComponentBean";
	
	private static final String COMPONENT_FORM_NAME = "com.bluage.documentation.web.common.components.cmp_headerForm";
	
	private static final String COMPONENT_SHORT_FORM_NAME = "cmp_headerForm";

	/**
	 * Property:LOGGER
	 */
	private static final Logger LOGGER = Logger.getLogger( CUC64_exceptioncmp_headerViewPreparer.class);

	/**
	 * Method execute for CUC64_exceptioncmp_headerViewPreparer
	 * @param tilesContext : The tiles context of the component  CUC64_exceptioncmp_headerViewPreparer
	 * @param attributeContext : The attribute context of the component  CUC64_exceptioncmp_headerViewPreparer
	 * @see org.apache.tiles.preparer.ViewPreparer#execute(org.apache.tiles.context.TilesRequestContext, org.apache.tiles.AttributeContext)
	 */
	public void execute(TilesRequestContext tilesContext, AttributeContext attributeContext) {
		
		LOGGER.info("Starting the component initialisation for CUC64_exceptioncmp_headerViewPreparer "+ UC64_EXCEPTIONCMP_HEADER +" Component Form name : "+COMPONENT_FORM_NAME);
		

		// Init the component cmp_headerForm
		final cmp_headerForm componentScreen = new cmp_headerForm(); // For the form cmp_headerForm
		
		tilesContext.getSessionScope().put(COMPONENT_SHORT_FORM_NAME, componentScreen); // For the component CUC64_exceptioncmp_headerViewPreparer
		
		LOGGER.info("The component is initialized (CUC64_exceptioncmp_headerViewPreparer).");
	}
}
