/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.business.standardfeatures.application.navigation.uc30_navigation.bos;

import javax.inject.Inject;
import javax.inject.Named;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import com.bluage.documentation.business.AbstractBusinessObject;
import com.bluage.documentation.business.standardfeatures.application.navigation.uc30_navigation.entities.daofinder.Page30DAOFinderImpl;

/**
 * Page30BO
 *
* @see com.netfective.bluage.core.business.IBusinessObject
 */
@XmlRootElement

public class Page30BO
 extends AbstractBusinessObject {
	
	/**
	 * Default SUID for Page30BO.
	 */
	private static final long serialVersionUID = 1L;
	

	@Inject
	@Named ("com.bluage.documentation.business.standardfeatures.application.navigation.uc30_navigation.entities.daofinder.Page30Finder") 
	private Page30DAOFinderImpl finder;
	/**
	 * Hibernate optimistic lock for class Page30BO.
	 */
	private int version;

						/**
	 * Entity property: id
	 *
	 */
private Integer id;

					/**
	 * Entity property: title
	 *
	 */
private String title;



   /**
	* public default constructor for class Page30BO
	*/
	public Page30BO(){
		//NOP Page30BO
	}

	
	/**
	 * Getter for version for class Page30BO
	 * @return int
	 */
	public int getVersion() {
		return this.version;
	}
	
	/**
	 * Setter for version for class Page30BO
	 * @param int version
	 */
	public void setVersion(final int version) {
		this.version = version;
	}

						/**
	 * Getter for id of class Page30BO.
	 *
	 * @return {@link java.lang.Integer}
	 * @generated
	 */ 
public  Integer getId() {
	return this.id;
	}


	/**
	 * Setter for id of class Page30BO.
	 * @param value {@link java.lang.Integer}
	 * @generated
	 */

public  void setId(final Integer id) {
	this.isEmpty = (id == null);
	this.id = id;
}

					/**
	 * Getter for title of class Page30BO.
	 *
	 * @return {@link java.lang.String}
	 * @generated
	 */ 
public  String getTitle() {
	return this.title;
	}


	/**
	 * Setter for title of class Page30BO.
	 * @param value {@link java.lang.String}
	 * @generated
	 */

public  void setTitle(final String title) {
	this.title = title;
}


/**
	 * Returns <code>true</code> if the argument is an Page30 instance and all identifiers for this entity
     * equal the identifiers of the argument entity. Returns <code>false</code> otherwise.
	 * @param object  object to compare with current instance      
     * @return boolean true if equals false if not
     */
    public boolean equals(final Object object )
    {
		if ( this == object )
        {
            return true;
        }
        if ( !( object instanceof Page30BO ) )
        {
            return false;
        }
        final Page30BO that = (Page30BO)object;
    
    	            if (        				        			this.getId() == null
        	        ) {
        	return false;
        }
		return new EqualsBuilder()
			.append(this.getId(), that.getId())
			 	.appendSuper(super.equals(object))
		 	.isEquals();
	}
	/**
     * Returns a hash code based on this entity's identifiers.     * @return int
     */    
    public int hashCode() {
        return new HashCodeBuilder()
             .append(getId())
		            .toHashCode();
    }
}

