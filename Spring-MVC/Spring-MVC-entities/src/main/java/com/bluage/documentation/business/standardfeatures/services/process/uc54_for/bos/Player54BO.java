/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.business.standardfeatures.services.process.uc54_for.bos;

import javax.inject.Inject;
import javax.inject.Named;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import com.bluage.documentation.business.common.bos.PersonBO;
import com.bluage.documentation.business.standardfeatures.services.process.uc54_for.entities.daofinder.Player54DAOFinderImpl;

/**
 * Player54BO
 *
* @see com.netfective.bluage.core.business.IBusinessObject
* * @see PersonBO
 */
@XmlRootElement

public class Player54BO
 extends PersonBO {
	
	/**
	 * Default SUID for Player54BO.
	 */
	private static final long serialVersionUID = 1L;
	

	@Inject
	@Named ("com.bluage.documentation.business.standardfeatures.services.process.uc54_for.entities.daofinder.Player54Finder") 
	private Player54DAOFinderImpl finder;

						/**
	 * Entity property: rookie
	 *
	 */
private Boolean rookie;

					/**
	 * Entity property: value
	 *
	 */
private Float value;


	private Boolean isInTeam;

   /**
	* public default constructor for class Player54BO
	*/
	public Player54BO(){
		//NOP Player54BO
	}


						/**
	 * Getter for rookie of class Player54BO.
	 *
	 * @return {@link java.lang.Boolean}
	 * @generated
	 */ 
public  Boolean getRookie() {
	return this.rookie;
	}


	/**
	 * Setter for rookie of class Player54BO.
	 * @param value {@link java.lang.Boolean}
	 * @generated
	 */

public  void setRookie(final Boolean rookie) {
	this.rookie = rookie;
}

					/**
	 * Getter for value of class Player54BO.
	 *
	 * @return {@link java.lang.Float}
	 * @generated
	 */ 
public  Float getValue() {
	return this.value;
	}


	/**
	 * Setter for value of class Player54BO.
	 * @param value {@link java.lang.Float}
	 * @generated
	 */

public  void setValue(final Float value) {
	this.value = value;
}


	
	/**
	 * Getter for isInTeam of class Player54BO.
	 * @return {@link Boolean}
	 */
	public Boolean getIsInTeam()
	{
		return this.isInTeam;
	}
	
	
	/**
	 * Setter for isInTeam of class Player54BO.
	 * @param value: {@link Boolean}
	 */
	public void setIsInTeam(Boolean isInTeam) 
	{
		this.isInTeam = isInTeam;
	}
/**
	 * Returns <code>true</code> if the argument is an Player54 instance and all identifiers for this entity
     * equal the identifiers of the argument entity. The <code>equals</code> method of the parent entity
     * will also need to return <code>true</code>. Returns <code>false</code> otherwise.
     *
     * @see PersonBO#hashCode()
	 * @param object  object to compare with current instance      
     * @return boolean true if equals false if not
     */
    public boolean equals(final Object object )
    {
		if ( this == object )
        {
            return true;
        }
        if ( !( object instanceof Player54BO ) )
        {
            return false;
        }
        final Player54BO that = (Player54BO)object;
    
    	            if (        				        			this.getId() == null
        	        ) {
        	return false;
        }
		return new EqualsBuilder()
			.append(this.getId(), that.getId())
			 	.isEquals();
	}
	/**

	 * Returns a hash code based on this entity's identifiers and the hash code of the parent entity.
     *
     * @see PersonBO#hashCode()
     * @return int
     */    
    public int hashCode() {
        return new HashCodeBuilder()
             .append(getId())
				 	.appendSuper(super.hashCode())
            .toHashCode();
    }
}

