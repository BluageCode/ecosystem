/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.common.links ;

// Import declaration.
// Java imports.
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.bluage.documentation.service.common.utility.ServiceUtility;
import com.bluage.documentation.web.presentation.common.home.homeForm;
import com.netfective.bluage.core.exception.ApplicationException;
import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;
import com.netfective.bluage.core.web.IForm;

/**
 * Class : GLO_GlobalLinksController
 */
@Controller
@Scope("prototype")
@SessionAttributes("gLO_GlobalLinksForm")
@RequestMapping(value= "/common/links")
public class GLO_GlobalLinksController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(GLO_GlobalLinksController.class);
	
	//Current form name
	private static final String G_L_O__GLOBAL_LINKS_FORM = "gLO_GlobalLinksForm";

	
	/**
	 * Property:customDateEditorsGLO_GlobalLinksController
	 */
	final private Map<String,CustomDateEditor> customDateEditorsGLO_GlobalLinksController = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:gLO_GlobalLinksValidator
	 */
	final private GLO_GlobalLinksValidator gLO_GlobalLinksValidator = new GLO_GlobalLinksValidator();
	
	/**
	 * Service declaration : serviceUtility.
	 */
	@Autowired
	private ServiceUtility serviceUtility;
	
	
	// Initialise all the instances for GLO_GlobalLinksController
	/**
	 * Operation : lnk_home
 	 * @param model : 
 	 * @param gLO_GlobalLinks : The form
 	 * @return
	 */
	@RequestMapping(value = "/GLO_GlobalLinks/lnk_home.html",method = RequestMethod.POST)
	public String lnk_home(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("gLO_GlobalLinksForm") GLO_GlobalLinksForm  gLO_GlobalLinksForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : lnk_home"); 
		infoLogger(gLO_GlobalLinksForm); 
		gLO_GlobalLinksForm.setAlwaysCallPreControllers(false); 
		// initialization for lnk_home
		init();
		
		String destinationPath = null; 
		

	 callGoToHome(request,gLO_GlobalLinksForm );
											// Init the destination form com.bluage.documentation.web.presentation.common.home.homeForm or not from lnk_home
			final homeForm homeForm =  new homeForm();
			// Populate the destination form with all the instances returned from lnk_home.
			final IForm homeForm2 = populateDestinationForm(request.getSession(), homeForm); 
					
			// Add the form to the model.
			model.addAttribute("homeForm", homeForm2); 
			
			request.getSession().setAttribute("homeForm", homeForm2);
			
			// "OK" CASE => destination screen path from lnk_home
			LOGGER.info("Go to the screen 'home'.");
			// Redirect (PRG) from lnk_home
			destinationPath =  "redirect:/presentation/common/home/home.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_home 
	}
	
	
	/**
	* This method initialise the form : GLO_GlobalLinksForm 
	* @return GLO_GlobalLinksForm
	*/
	@ModelAttribute("GLO_GlobalLinksFormInit")
	public void initGLO_GlobalLinksForm(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, G_L_O__GLOBAL_LINKS_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method GLO_GlobalLinksForm."); //for lnk_home 
		}
		GLO_GlobalLinksForm gLO_GlobalLinksForm;
	
		if(request.getSession().getAttribute(G_L_O__GLOBAL_LINKS_FORM) != null){
			gLO_GlobalLinksForm = (GLO_GlobalLinksForm)request.getSession().getAttribute(G_L_O__GLOBAL_LINKS_FORM);
		} else {
			gLO_GlobalLinksForm = new GLO_GlobalLinksForm();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form GLO_GlobalLinksForm.");
		}
		gLO_GlobalLinksForm = (GLO_GlobalLinksForm)populateDestinationForm(request.getSession(), gLO_GlobalLinksForm);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  GLO_GlobalLinksForm.");
		}
		model.addAttribute(G_L_O__GLOBAL_LINKS_FORM, gLO_GlobalLinksForm);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param GLO_GlobalLinksForm : The sceen form.
	 */
	@RequestMapping(value = "/GLO_GlobalLinks.html" ,method = RequestMethod.GET)
	public void prepareGLO_GlobalLinks(final HttpServletRequest request,final  Model model,final  @ModelAttribute("GLO_GlobalLinksFormInit") GLO_GlobalLinksForm gLO_GlobalLinksForm){
		
		GLO_GlobalLinksForm currentGLO_GlobalLinksForm = gLO_GlobalLinksForm;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(G_L_O__GLOBAL_LINKS_FORM) == null){
			if(currentGLO_GlobalLinksForm!=null){
				request.getSession().setAttribute(G_L_O__GLOBAL_LINKS_FORM, currentGLO_GlobalLinksForm);
			}else {
				currentGLO_GlobalLinksForm = new GLO_GlobalLinksForm();
				request.getSession().setAttribute(G_L_O__GLOBAL_LINKS_FORM, currentGLO_GlobalLinksForm);	
			}
		} else {
			currentGLO_GlobalLinksForm = (GLO_GlobalLinksForm) request.getSession().getAttribute(G_L_O__GLOBAL_LINKS_FORM);
		}

		currentGLO_GlobalLinksForm = (GLO_GlobalLinksForm)populateDestinationForm(request.getSession(), currentGLO_GlobalLinksForm);
		// Call all the Precontroller.
	currentGLO_GlobalLinksForm.setAlwaysCallPreControllers(true);
		model.addAttribute(G_L_O__GLOBAL_LINKS_FORM, currentGLO_GlobalLinksForm);
		request.getSession().setAttribute(G_L_O__GLOBAL_LINKS_FORM, currentGLO_GlobalLinksForm);
		
	}
	
	/**
	 * method callGoToHome
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callGoToHome(HttpServletRequest request,IForm form) {
					try {
				// executing GoToHome in lnk_home
	serviceUtility.doNothing(
	);  

								// processing variables GoToHome in lnk_home

			} catch (ApplicationException e) { 
				// error handling for operation doNothing called GoToHome
				errorLogger("An error occured during the execution of the operation : doNothing",e); 
		
			}
	}
		
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("GLO_GlobalLinksController [ ");
		// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(gLO_GlobalLinksValidator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsGLO_GlobalLinksController!=null); 
		return strBToS.toString();
	}
}
