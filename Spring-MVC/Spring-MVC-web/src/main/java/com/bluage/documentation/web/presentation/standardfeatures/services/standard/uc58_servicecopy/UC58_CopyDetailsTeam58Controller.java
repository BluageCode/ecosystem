/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.services.standard.uc58_servicecopy ;

// Import declaration.
// Java imports.
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.bluage.documentation.business.standardfeatures.services.standard.uc58_servicecopy.bos.Team58BO;
import com.bluage.documentation.business.standardfeatures.services.standard.uc58_servicecopy.entities.daofinder.State58DAOFinderImpl;
import com.bluage.documentation.service.standardfeatures.services.standard.uc58_servicecopy.ServiceTeam58Update;
import com.netfective.bluage.core.exception.ApplicationException;
import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;
import com.netfective.bluage.core.web.IForm;

/**
 * Class : UC58_CopyDetailsTeam58Controller
 */
@Controller
@Scope("prototype")
@SessionAttributes("uC58_CopyDetailsTeam58Form")
@RequestMapping(value= "/presentation/standardfeatures/services/standard/uc58_servicecopy")
public class UC58_CopyDetailsTeam58Controller extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC58_CopyDetailsTeam58Controller.class);
	
	//Current form name
	private static final String U_C58__COPY_DETAILS_TEAM58_FORM = "uC58_CopyDetailsTeam58Form";

	//CONSTANT: copyTeam
	private static final String COPY_TEAM = "copyTeam";
			//CONSTANT: allStates1
	private static final String ALL_STATES1 = "allStates1";
				//CONSTANT: teamDetails
	private static final String TEAM_DETAILS = "teamDetails";
	
	/**
	 * Property:customDateEditorsUC58_CopyDetailsTeam58Controller
	 */
	final private Map<String,CustomDateEditor> customDateEditorsUC58_CopyDetailsTeam58Controller = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:uC58_CopyDetailsTeam58Validator
	 */
	final private UC58_CopyDetailsTeam58Validator uC58_CopyDetailsTeam58Validator = new UC58_CopyDetailsTeam58Validator();
	
	/**
	 * Service declaration : serviceTeam58Update.
	 */
	@Autowired
	private ServiceTeam58Update serviceTeam58Update;
	
	/**
	 * Generic Finder : state58DAOFinderImpl.
	 */
	@Autowired
	private State58DAOFinderImpl state58DAOFinderImpl;
	
	
	// Initialise all the instances for UC58_CopyDetailsTeam58Controller
		// Initialize the instance copyTeam of type com.bluage.documentation.business.standardfeatures.services.standard.uc58_servicecopy.bos.Team58BO with the value : null for UC58_CopyDetailsTeam58Controller
		private Team58BO copyTeam;
				// Initialize the instance allStates1 for the state lnk_view
		private List allStates1; // Initialize the instance allStates1 for UC58_CopyDetailsTeam58Controller
						// Declare the instance teamDetails
		private Team58BO teamDetails;
			/**
	 * Operation : lnk_updateTeam
 	 * @param model : 
 	 * @param uC58_CopyDetailsTeam58 : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC58_CopyDetailsTeam58/lnk_updateTeam.html",method = RequestMethod.POST)
	public String lnk_updateTeam(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC58_CopyDetailsTeam58Form") UC58_CopyDetailsTeam58Form  uC58_CopyDetailsTeam58Form, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : lnk_updateTeam"); 
		infoLogger(uC58_CopyDetailsTeam58Form); 
		uC58_CopyDetailsTeam58Form.setAlwaysCallPreControllers(false); 
		// initialization for lnk_updateTeam
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state lnk_updateTeam if the validation fail, you will be forward to the screen : "/presentation/standardfeatures/services/standard/uc58_servicecopy/UC58_CopyDetailsTeam58"
		if(errorsMessages(request,response,uC58_CopyDetailsTeam58Form,bindingResult,"", uC58_CopyDetailsTeam58Validator, customDateEditorsUC58_CopyDetailsTeam58Controller)){ 
			return "/presentation/standardfeatures/services/standard/uc58_servicecopy/UC58_CopyDetailsTeam58"; 
		}

					 callTeam58Update(request,uC58_CopyDetailsTeam58Form );
											// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.services.standard.uc58_servicecopy.UC58_DisplayTeams58Form or not from lnk_updateTeam
			final UC58_DisplayTeams58Form uC58_DisplayTeams58Form =  new UC58_DisplayTeams58Form();
			// Populate the destination form with all the instances returned from lnk_updateTeam.
			final IForm uC58_DisplayTeams58Form2 = populateDestinationForm(request.getSession(), uC58_DisplayTeams58Form); 
					
			// Add the form to the model.
			model.addAttribute("uC58_DisplayTeams58Form", uC58_DisplayTeams58Form2); 
			
			request.getSession().setAttribute("uC58_DisplayTeams58Form", uC58_DisplayTeams58Form2);
			
			// "OK" CASE => destination screen path from lnk_updateTeam
			LOGGER.info("Go to the screen 'UC58_DisplayTeams58'.");
			// Redirect (PRG) from lnk_updateTeam
			destinationPath =  "redirect:/presentation/standardfeatures/services/standard/uc58_servicecopy/UC58_DisplayTeams58.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_updateTeam 
	}
	
	
	/**
	* This method initialise the form : UC58_CopyDetailsTeam58Form 
	* @return UC58_CopyDetailsTeam58Form
	*/
	@ModelAttribute("UC58_CopyDetailsTeam58FormInit")
	public void initUC58_CopyDetailsTeam58Form(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, U_C58__COPY_DETAILS_TEAM58_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method UC58_CopyDetailsTeam58Form."); //for lnk_updateTeam 
		}
		UC58_CopyDetailsTeam58Form uC58_CopyDetailsTeam58Form;
	
		if(request.getSession().getAttribute(U_C58__COPY_DETAILS_TEAM58_FORM) != null){
			uC58_CopyDetailsTeam58Form = (UC58_CopyDetailsTeam58Form)request.getSession().getAttribute(U_C58__COPY_DETAILS_TEAM58_FORM);
		} else {
			uC58_CopyDetailsTeam58Form = new UC58_CopyDetailsTeam58Form();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form UC58_CopyDetailsTeam58Form.");
		}
		uC58_CopyDetailsTeam58Form = (UC58_CopyDetailsTeam58Form)populateDestinationForm(request.getSession(), uC58_CopyDetailsTeam58Form);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  UC58_CopyDetailsTeam58Form.");
		}
		model.addAttribute(U_C58__COPY_DETAILS_TEAM58_FORM, uC58_CopyDetailsTeam58Form);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param UC58_CopyDetailsTeam58Form : The sceen form.
	 */
	@RequestMapping(value = "/UC58_CopyDetailsTeam58.html" ,method = RequestMethod.GET)
	public void prepareUC58_CopyDetailsTeam58(final HttpServletRequest request,final  Model model,final  @ModelAttribute("UC58_CopyDetailsTeam58FormInit") UC58_CopyDetailsTeam58Form uC58_CopyDetailsTeam58Form){
		
		UC58_CopyDetailsTeam58Form currentUC58_CopyDetailsTeam58Form = uC58_CopyDetailsTeam58Form;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(U_C58__COPY_DETAILS_TEAM58_FORM) == null){
			if(currentUC58_CopyDetailsTeam58Form!=null){
				request.getSession().setAttribute(U_C58__COPY_DETAILS_TEAM58_FORM, currentUC58_CopyDetailsTeam58Form);
			}else {
				currentUC58_CopyDetailsTeam58Form = new UC58_CopyDetailsTeam58Form();
				request.getSession().setAttribute(U_C58__COPY_DETAILS_TEAM58_FORM, currentUC58_CopyDetailsTeam58Form);	
			}
		} else {
			currentUC58_CopyDetailsTeam58Form = (UC58_CopyDetailsTeam58Form) request.getSession().getAttribute(U_C58__COPY_DETAILS_TEAM58_FORM);
		}

		try {
			List allStates1 = state58DAOFinderImpl.findAll();
			put(request.getSession(), ALL_STATES1, allStates1);
		} catch (ApplicationException e) {
			errorLogger("An error while calling the generic finder for the instance : allStates1.",e);
		}
					currentUC58_CopyDetailsTeam58Form = (UC58_CopyDetailsTeam58Form)populateDestinationForm(request.getSession(), currentUC58_CopyDetailsTeam58Form);
		// Call all the Precontroller.
	currentUC58_CopyDetailsTeam58Form.setAlwaysCallPreControllers(true);
		model.addAttribute(U_C58__COPY_DETAILS_TEAM58_FORM, currentUC58_CopyDetailsTeam58Form);
		request.getSession().setAttribute(U_C58__COPY_DETAILS_TEAM58_FORM, currentUC58_CopyDetailsTeam58Form);
		
	}
	
							/**
	 * method callTeam58Update
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callTeam58Update(HttpServletRequest request,IForm form) {
					teamDetails = (Team58BO)get(request.getSession(),form, "teamDetails");  
										 
					try {
				// executing Team58Update in lnk_updateTeam
	serviceTeam58Update.team58Update(
			teamDetails
			);  

													// processing variables Team58Update in lnk_updateTeam
				put(request.getSession(), TEAM_DETAILS,teamDetails); 
			
			} catch (ApplicationException e) { 
				// error handling for operation team58Update called Team58Update
				errorLogger("An error occured during the execution of the operation : team58Update",e); 
		
			}
	}
		
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("UC58_CopyDetailsTeam58Controller [ ");
			strBToS.append(COPY_TEAM);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(copyTeam);
			strBToS.append(END_TO_STRING_DELIMITER);
					strBToS.append(ALL_STATES1);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(allStates1);
			strBToS.append(END_TO_STRING_DELIMITER);
						strBToS.append(TEAM_DETAILS);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(teamDetails);
			strBToS.append(END_TO_STRING_DELIMITER);
		// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(uC58_CopyDetailsTeam58Validator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsUC58_CopyDetailsTeam58Controller!=null); 
		return strBToS.toString();
	}
}
