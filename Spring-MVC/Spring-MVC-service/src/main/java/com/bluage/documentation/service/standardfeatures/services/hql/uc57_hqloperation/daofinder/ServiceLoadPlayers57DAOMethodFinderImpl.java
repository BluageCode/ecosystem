/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.service.standardfeatures.services.hql.uc57_hqloperation.daofinder;


import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateTemplate;

import com.bluage.documentation.business.standardfeatures.services.hql.uc57_hqloperation.bos.Team57BO;
import com.netfective.bluage.core.exception.ApplicationException;


/**
 * This class represents query invocation methods implementation
 * for ServiceLoadPlayers57 service.
 *
 * @see com.bluage.documentation.service.standardfeatures.services.hql.uc57_hqloperation.ServiceLoadPlayers57
 */
 
@Named ("com.bluage.documentation.service.standardfeatures.services.hql.uc57_hqloperation.daofinder.ServiceLoadPlayers57DAOMethodFinderImpl")
public class ServiceLoadPlayers57DAOMethodFinderImpl {

	// LOGGER for ServiceLoadPlayers57DAOMethodFinderImpl
	private static final Logger LOGGER = Logger.getLogger(ServiceLoadPlayers57DAOMethodFinderImpl.class);
	
	/**
	* Hibernate Template (injected)
	*/
	@Inject
	private HibernateTemplate hibernateTemplate;

	
	/**
	 * setter for HibernateTemplate
	 * @param value : {@link HibernateTemplate}
	 */
	public void setHibernateTemplate( HibernateTemplate value )
	{
		hibernateTemplate = value;
	}
	

	/**    
	 * HQL operation.
	 * @param team {@link Team57BO}
		 * @return {@link Team57BO}
	 */
	public Team57BO loadPlayers(Team57BO team )
 throws ApplicationException {
Session session = hibernateTemplate.getSessionFactory().getCurrentSession();
			return 
				privateLoadPlayers(team, session);
	}

	
	/**    
	 * private delegation for HQL operation.
	 * @param team {@link Team57BO}
		 * @return {@link Team57BO}
	 */
	private Team57BO privateLoadPlayers(Team57BO team , Session session)
 throws ApplicationException {
			Query query = session.createQuery(" SELECT t FROM Team57BO t JOIN FETCH t.players s WHERE t = :team ");

			query.setParameter( "team", team);
									Team57BO returnValue = null;
		try {
			Object result = query.uniqueResult();
			if( result != null ) {
				returnValue = ( com.bluage.documentation.business.standardfeatures.services.hql.uc57_hqloperation.bos.Team57BO )result;
			}				
		} catch(HibernateException ex) {
			LOGGER.error("An error occured while excecuting the HQL operation : com.bluage.documentation.service.standardfeatures.services.hql.uc57_hqloperation.loadPlayers");
			throw new ApplicationException(ex);
		}
		return returnValue;
	}

	




}
