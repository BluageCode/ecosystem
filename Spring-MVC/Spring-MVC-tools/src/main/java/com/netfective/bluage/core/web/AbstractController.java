/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.netfective.bluage.core.web ;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;

import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.validator.AbstractValidator;


/**
 * @author NETFECTIVE TECHNOLOGY
 */
public abstract class AbstractController {
	
	/**
	 *  ID request parameter to get the selected instance.
	 */
	public static final String ID_REQUEST_PARAM = "id";
	
	/**
	 *  To string delimiter start string.
	 */
	public static final String START_TO_STRING_DELIMITER = " : [";
	
	/**
	 *  To string delimiter start string.
	 */
	public static final char END_TO_STRING_DELIMITER = ']';
	
	/**
	 *  Log validator is initialised.
	 */
	public static final String LOG_VALIDATOR_INITIALISED = " The validator is initialised : ";

	/**
	 *  Log custom date editor is initialised.
	 */
	public static final String LOG_CUSTOM_DATE_INITIALISED = " The custom validator is initialised : ";
	
	
	/**
	 * File extension for captcha
	 */
	public static final String CAPTCHA_IMAGE_FORMAT = "jpeg";
	
	/**
	 *  TAB request parameter to get the selected instance.
	 */
	public static final String TAB_REQUEST_PARAM = "tab";
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(AbstractController.class);
	
	/**
	 * static field callBackStack
	 */
	 private static final String CALL_BACK_STACK = "callBackStack";
	
	/**
	 * SESSION_VARIABLES static attribute
	 */
	private static final List<String> SESSION_VARIABLES = new ArrayList<String>();
	/**
	 * tempVariables attribute
	 */
	private Map<String, Object> tempVariables = new HashMap<String, Object>();
	
	static {
	};


	/**
	 * getter for sessionVariable
	 * @return List<String>
	 */
	public static List<String> getSessionVariables() {
		return SESSION_VARIABLES;
	}


	/**
	 * 
	 * @param currentSession
	 * @param attributeName
	 * @param attributeValue
	 */
	public void put(final HttpSession currentSession, final String attributeName, final Object attributeValue){
		tempVariables.put(attributeName, attributeValue);
		putSession(currentSession, attributeName,attributeValue);
	}
	

	/**
	 * 
	 * @param currentSession
	 * @param attributeName
	 * @param attributeValue
	 */
	public void putSession(final HttpSession currentSession, final String attributeName, final Object attributeValue){
		if(getSessionVariables().contains(attributeName) || CALL_BACK_STACK.equals(attributeName)){
			currentSession.setAttribute(attributeName, attributeValue);
		}
	}
	
	/**
	 * @param session HttpSession
	 * @param form IForm
	 * @param attributeName String
	 * @return Object
	 */
	public Object get(final HttpSession session, final IForm form, final String attributeName){
		Object objToReturn = get(session, attributeName);
		if(objToReturn == null){
			objToReturn = form.getInstanceByInstanceName(attributeName);
		}
		return objToReturn;
	}
	
	/**
	 * 
	 * @param currentSession
	 * @param attributeName
	 * @return
	 */
	public Object get(final HttpSession currentSession, final String attributeName){
		if(currentSession.getAttribute(attributeName)!=null){
			return currentSession.getAttribute(attributeName);
		} else {
			return tempVariables.get(attributeName);
		}
	}
	
	/**
	 * Populate all the instances into the form in parameter.
	 * @param currentForm : The IForm.
	 * @param : currentSession : The HttpSession
	 * @return : The updated IForm.
	 */
	public IForm populateDestinationForm(final HttpSession currentSession, final IForm currentForm){
		final Set<String> allTheInstancesToSet = tempVariables.keySet();
		// For all the instance returned from the executed service.
		for (String instanceName : allTheInstancesToSet) {
			currentForm.setInstanceByInstanceName(instanceName, tempVariables.get(instanceName));
		}
		
		//For all the instances in session
		for (String sessionInstanceName : SESSION_VARIABLES) {
			currentForm.setInstanceByInstanceName(sessionInstanceName, currentSession.getAttribute(sessionInstanceName));
		}
		return currentForm;
	}
	
	/**
	 * Processes instances redispatchint while using component in ajax
	 * @param session the HttpSession
	 * @param form the form
	 * @param screenName
	 * @param defaultPath
	 * @param model
	 * @return the redirect String
	 */
	public String processAjaxComponent(final HttpSession session, final IForm form, final String screenName, final String defaultPath, final Model model){
			final ComponentsBean compBean = (ComponentsBean)session.getAttribute(screenName.concat("ComponentBean"));
			// Ajax and components
			if (compBean != null) {
				final IForm ishelper = (IForm) session.getAttribute(compBean.getParentHelperName());

				final Set<String> componentInstances = compBean.getAttributs().keySet();
				for (String sourceBeanName : componentInstances) {
					final String destinationBean = compBean.getAttributs().get(sourceBeanName);
					ishelper.setInstanceByInstanceName(destinationBean, form.getInstanceByInstanceName(sourceBeanName));
				}

				session.setAttribute(compBean.getParentHelperName(), ishelper);
				model.addAttribute(compBean.getParentHelperName(), ishelper);
				return compBean.getParentPathScreenName();
			}
			return defaultPath;
	}
	
	/**
	 * Info LOGGER
	 * @param currentForm : The current form.
	 */
	 public void infoLogger(final IForm currentForm){
		LOGGER.info("Diagnostic of Source Form " + currentForm);
	 }
	
	/**
	 * Error LOGGER
	 * @param errorMessage : The error message
	 * @param ex : The Exception
	 */
	 public void errorLogger(final String errorMessage, final Exception ex){
		LOGGER.error(errorMessage, ex);
	 }
	/**
	* This method start the validation for the current form.
	* @param request :  HttpServletRequest
	* @param response : HttpServletResponse
	* @param IForm : The form
	* @param bindingResult : BindingResult
	* @return Boolean
	*/
	public Boolean errorsMessages(final HttpServletRequest request, final HttpServletResponse response, final IForm currentForm, final BindingResult bindingResult, 
		final String tableElement, final AbstractValidator validator , final Map<String,CustomDateEditor> customDateEditors
	){
		gatherValidationErrorsFromEditors(bindingResult, customDateEditors);
		final String index = request.getParameter(ID_REQUEST_PARAM);
		validator.setRequestIndex(index);
		validator.setTableElement(tableElement);
		validator.validate(currentForm,bindingResult);
		final int count = bindingResult.getErrorCount();
		return count > 0;
	}
	
	/**
	* This method Validation Errors From Editors.
	* @param bindingResult : BindingResult
	*/
	private void gatherValidationErrorsFromEditors(final BindingResult bindingResult, final Map<String,CustomDateEditor> customDateEditors){
		for(final Iterator<String> fields = customDateEditors.keySet().iterator();fields.hasNext();){
			final String field = fields.next();
			final CustomDateEditor mcde = customDateEditors.get(field);
			if(mcde.getValidationError() != null){
				bindingResult.rejectValue(field,"",mcde.getValidationError());
			}
		}
	}

	/**
	 * Clean HTTP session of forms different of current one. Current form is a
	 * bean stored in HTTP Session and named <i>x</i>, <i>x</i> is equal to the
	 * property value named <code>currentFormName</code> of parameter named
	 * <code>currentFormName</code>.
	 * 
	 * @param HttpServletRequest
	 *            : request : current HTTP request
	 * @param currentFormName
	 *            : The current form name.
	 */
	public static void cleanHttpSession(HttpServletRequest request, String currentFormName) {
		HttpSession session = request.getSession();
		Enumeration names = session.getAttributeNames();

		while (names.hasMoreElements()) {
			String name = (String) names.nextElement();
			Object objectInSession = session.getAttribute(name);
			LOGGER.info("The following Form is :" + name + ":" + objectInSession.getClass().getName() + "?");
			if ((objectInSession instanceof IForm) && !currentFormName.equals(name)) {
					LOGGER.info("The following Form :" + name + ":" + objectInSession.getClass().getName() + " is deleted from HTTP Session");
					session.removeAttribute(name);
			}
		} // end while
	}
	
	/**
	 * utility method to handle callbacks
	 * @param currentSession HttpSession
	 * @param redirect String
	 */
	public void pushCallback(final HttpSession currentSession,String redirect){
		Deque<String> callBackStack = null;
		if(currentSession.getAttribute(CALL_BACK_STACK) != null){
			callBackStack = (Deque<String>) currentSession.getAttribute(CALL_BACK_STACK);
		}else{
			callBackStack = new ArrayDeque<String>();
		}
		callBackStack.add(redirect);
		putSession(currentSession, CALL_BACK_STACK, callBackStack);
	}
	
	/**
	 * utility method to handle callbacks
	 * @param currentSession HttpSession
	 * @return String redirect
	 */
	public String popCallback(final HttpSession currentSession){
		Deque<String> callBackStack = null;
		if(currentSession.getAttribute(CALL_BACK_STACK) != null){
			callBackStack = (Deque<String>) currentSession.getAttribute(CALL_BACK_STACK);
		}else{
			callBackStack = new ArrayDeque<String>();
		}
		String callBackBean = callBackStack.pollLast();
		putSession(currentSession, CALL_BACK_STACK, callBackStack);
		return callBackBean;
	}

	/**
	 * initializes temporary variables map
	 */
	public void init(){
		tempVariables.clear();
	}

}
