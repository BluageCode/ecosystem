ALTER TABLE player205 DROP CONSTRAINT player205_position205_fkc;

ALTER TABLE player205 DROP CONSTRAINT player205_team205_fkc;


ALTER TABLE team205 DROP CONSTRAINT team205_pkey;

ALTER TABLE player205 DROP CONSTRAINT player205_pkey;


DROP TABLE team205;

DROP TABLE player205;

DROP TABLE position205;



CREATE TABLE team205 (
		id INT8 NOT NULL,
		version INT4 NOT NULL,
		city VARCHAR(1024),
		name VARCHAR(1024)
	);
	


CREATE TABLE position205 (
    code character varying(2) NOT NULL,
    version integer NOT NULL,
    name character varying(20) NOT NULL
);



ALTER TABLE ONLY position205
    ADD CONSTRAINT position205_pkey PRIMARY KEY (code);
	
CREATE TABLE player205 (
		id INT8 NOT NULL,
		version INT4 NOT NULL,
		first_name VARCHAR(1024),
		last_name VARCHAR(1024),
		date_of_birth DATE,
		estimated_value real,
		weight integer, 
		height integer, 
		rookie boolean, 
		team205_fk INT8,
		position205_fk character varying(2) NOT NULL
	);
	


INSERT INTO position205 VALUES ('PG', 0, 'Point Guard');
INSERT INTO position205 VALUES ('SG', 0, 'Shooting Guard');
INSERT INTO position205 VALUES ('SF', 0, 'Small Forward');
INSERT INTO position205 VALUES ('PF', 0, 'Power Forward');
INSERT INTO position205 VALUES ('C', 0, 'Center');	
	
INSERT INTO team205 (id, version, city, name) VALUES (1, 0, 'Charlotte', 'Tigers');
INSERT INTO team205 (id, version, city, name) VALUES (2, 0, 'Kansas City', 'Bears');
INSERT INTO team205 (id, version, city, name) VALUES (3, 0, 'Detroit', 'Chargers');
INSERT INTO team205 (id, version, city, name) VALUES (4, 0, 'Detroit', 'Hawks');
INSERT INTO team205 (id, version, city, name) VALUES (5, 0, 'Omaha', 'Braves');
INSERT INTO team205 (id, version, city, name) VALUES (6, 0, 'Raleigh', 'Eagles');
INSERT INTO team205 (id, version, city, name) VALUES (7, 0, 'Oakland', 'Cardinals');
INSERT INTO team205 (id, version, city, name) VALUES (8, 0, 'Kansas City', 'Red Devils');
INSERT INTO team205 (id, version, city, name) VALUES (9, 0, 'Fresno', 'Mustangs');

INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (201, 0, 'Oliver', 'Frank', '1968-10-31', 0.11, 116, 228, false, null, 'PG');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (202, 0, 'Elbert', 'Cole', '1965-04-07', 3.95, 100, 171, false, null, 'SG');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (203, 0, 'Abel', 'Dennis', '1980-04-16', 30.51, 105, 208, false, null, 'SF');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (204, 0, 'Ricky', 'Dawson', '1990-03-03', 29.299999, 115, 168, true, null, 'C');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (205, 0, 'Earl', 'Russell', '1981-12-15', 44.73, 60, 234, false, null, 'PF');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (206, 0, 'Nick', 'Garner', '1992-07-27', 47.84, 87, 221, true, null, 'PG');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (207, 0, 'Howard', 'Ramos', '1970-08-25', 10.76, 107, 199, false, 1, 'SG');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (208, 0, 'Allan', 'Meyer', '1969-02-14', 33.669998, 114, 229, false, 1, 'SF');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (209, 0, 'Ruben', 'Pope', '1985-05-01', 44.66, 86, 201, false, 1, 'C');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (210, 0, 'Lester', 'Cross', '1976-02-21', 30.98, 69, 214, false, 1, 'PF');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (218, 0, 'Roderick', 'Robbins', '1975-07-29', 12.04, 115, 164, false, 1, 'SF');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (219, 0, 'Christian', 'Chavez', '1983-05-17', 37.880001, 97, 209, false, 2, 'C');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (220, 0, 'Cecil', 'Conner', '1978-06-29', 39.709999, 160, 170, false, 2, 'PF');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (221, 0, 'Stephen', 'Lambert', '1977-03-14', 18.9, 107, 153, false, 2, 'PG');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (222, 0, 'Gustavo', 'Romero', '1978-02-10', 5.0300002, 130, 167, false, 2, 'SG');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (223, 0, 'Salvatore', 'Robertson', '1979-12-22', 36.169998, 156, 156, false, 2, 'SF');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (224, 0, 'Guadalupe', 'Banks', '1986-07-03', 30.209999, 111, 167, false, null, 'C');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (225, 0, 'Todd', 'Gilbert', '1976-03-13', 0.41999999, 115, 211, false, null, 'PF');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (226, 0, 'Ralph', 'Castillo', '1975-07-29', 36.130001, 115, 229, false, null, 'PG');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (227, 0, 'Gerardo', 'George', '1982-03-17', 11.32, 151, 159, false, null, 'SG');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (228, 0, 'Vernon', 'Lawson', '1974-09-13', 22.48, 146, 208, false, null, 'SF');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (229, 0, 'Marshall', 'Soto', '1977-10-04', 35.639999, 82, 172, false, null, 'C');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (230, 0, 'Jim', 'Riley', '1971-10-20', 43.98, 104, 171, false, null, 'PF');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (231, 0, 'Matthew', 'Fitzgerald', '1982-08-09', 33.610001, 145, 230, false, 3, 'PG');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (232, 0, 'Stuart', 'Ford', '1983-09-22', 2.96, 86, 174, false, 3, 'SG');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (233, 0, 'Timmy', 'Miller', '1972-08-07', 35.169998, 89, 222, false, 3, 'SF');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (234, 0, 'Antonio', 'Steele', '1984-03-05', 41.650002, 62, 233, false, 3, 'C');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (236, 0, 'Jordan', 'Hayes', '1989-07-11', 44.419998, 113, 203, false, 3, 'PF');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (237, 0, 'Milton', 'White', '1970-12-20', 25.4, 132, 159, false, 3, 'PG');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (239, 0, 'Ralph', 'Crawford', '1974-10-06', 38.43, 106, 173, false, 3, 'SG');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (241, 0, 'Timmy', 'Carson', '1966-09-13', 13.62, 142, 176, false, 4, 'SF');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (242, 0, 'Alfonso', 'Harrison', '1981-07-28', 47.43, 147, 200, false, 4, 'C');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (243, 0, 'Salvador', 'Watson', '1984-10-02', 49.900002, 79, 173, false, 4, 'PF');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (244, 0, 'Nelson', 'Cook', '1966-03-01', 34.950001, 129, 186, false, 4, 'PG');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (245, 0, 'Vernon', 'Davis', '1971-10-19', 2.24, 108, 206, false, 4, 'PG');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (246, 0, 'Earnest', 'Maxwell', '1986-01-20', 42.16, 135, 238, false, 4, 'SG');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (247, 0, 'Dwayne', 'Stevenson', '1968-10-26', 43.02, 101, 226, false, 4, 'SF');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (248, 0, 'Archie', 'Roberson', '1974-08-11', 7.1500001, 101, 152, false, 4, 'C');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (249, 0, 'Darryl', 'Norris', '1965-01-22', 47.700001, 121, 202, false, null, 'PF');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (250, 0, 'Dean', 'Stokes', '1974-10-15', 49.09, 150, 166, false, null, 'PG');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (251, 0, 'Allen', 'Freeman', '1969-09-03', 33.279999, 99, 201, false, null, 'SG');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (252, 0, 'Brett', 'Weaver', '1971-10-04', 14.83, 75, 234, false, null, 'SF');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (253, 0, 'Jerome', 'Griffin', '1987-12-19', 9.1499996, 146, 229, false, null, 'C');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (254, 0, 'John', 'Lynch', '1980-09-06', 28.35, 123, 201, false, null, 'PF');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (256, 0, 'Jody', 'Elliott', '1972-05-07', 45.290001, 66, 220, false, null, 'PG');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (257, 0, 'Willis', 'Francis', '1977-06-21', 23.91, 77, 223, false, 4, 'SG');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (259, 0, 'Stanley', 'Wolfe', '1985-12-14', 0.50999999, 129, 211, false, 4, 'SF');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (260, 0, 'Don', 'Griffith', '1975-10-31', 24.25, 101, 212, false, null, 'C');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (262, 0, 'Edwin', 'Reese', '1989-06-20', 10.7, 103, 184, false, null, 'PF');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (263, 0, 'Salvador', 'Bryant', '1965-08-24', 35.830002, 106, 216, false, null, 'PG');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (265, 0, 'Pablo', 'Rhodes', '1982-06-17', 31.6, 108, 201, false, null, 'SG');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (266, 0, 'Dana', 'Hamilton', '1983-05-30', 33.599998, 97, 175, false, null, 'SF');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (267, 0, 'Cecil', 'Wells', '1989-12-26', 38.610001, 122, 189, false, null, 'C');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (268, 0, 'Earl', 'Barrett', '1982-09-15', 45.34, 138, 208, false, null, 'PF');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (269, 0, 'Paul', 'Page', '1969-12-13', 25.809999, 125, 193, false, null, 'PG');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (270, 0, 'Freddie', 'Williamson', '1976-10-10', 3.8, 105, 218, false, null, 'SG');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (271, 0, 'Gilbert', 'Johnston', '1981-12-30', 40.259998, 61, 178, false, null, 'SF');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (272, 0, 'Darnell', 'Williamson', '1978-06-15', 45.080002, 157, 190, false, 1, 'C');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (273, 0, 'Joshua', 'Hardy', '1975-04-09', 9.6300001, 150, 172, false, 1, 'PF');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (274, 0, 'Dale', 'Colon', '1984-09-28', 9.25, 68, 160, false, null, 'PG');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (275, 0, 'Clarence', 'Johnson', '1972-05-25', 2.02, 128, 187, false, 1, 'SG');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (276, 0, 'Rolando', 'Waters', '1966-02-26', 7.0999999, 67, 201, false, null, 'SF');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (277, 0, 'Dominic', 'Martinez', '1967-03-04', 1.34, 127, 203, false, 1, 'C');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (278, 0, 'Ricardo', 'Oliver', '1968-05-02', 20.5, 147, 152, false, 5, 'PF');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (279, 0, 'Edward', 'Hogan', '1986-02-02', 23.24, 75, 158, false, 5, 'PG');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (280, 0, 'Gerald', 'Sanders', '1976-03-03', 21.68, 127, 207, false, 5, 'SG');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (281, 0, 'Noel', 'Oliver', '1984-11-24', 45.18, 112, 160, false, 5, 'SF');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (282, 0, 'Preston', 'Medina', '1974-02-15', 0.33000001, 95, 213, false, 5, 'C');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (283, 0, 'Abraham', 'Drake', '1985-11-27', 21.77, 94, 207, false, 5, 'PF');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (284, 0, 'Julian', 'Gibbs', '1985-07-01', 23.34, 80, 163, false, 5, 'PG');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (285, 0, 'Juan', 'Dawson', '1987-01-14', 9.9799995, 75, 204, false, 5, 'SG');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (286, 0, 'Marvin', 'Mitchell', '1981-05-27', 7.9400001, 91, 210, false, 5, 'SF');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (287, 0, 'Owen', 'Little', '1981-10-27', 45.869999, 135, 183, false, 5, 'C');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (288, 0, 'Irvin', 'Lee', '1988-02-25', 10.61, 88, 157, false, 5, 'PF');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (289, 0, 'Wayne', 'Rose', '1981-10-28', 20.41, 115, 238, false, 5, 'PG');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (290, 0, 'Lucas', 'Williamson', '1983-06-29', 38.360001, 73, 151, false, 5, 'SG');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (291, 0, 'Walter', 'Reyes', '1984-12-09', 46.150002, 104, 193, false, 6, 'SF');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (292, 0, 'Kirk', 'Cox', '1973-05-10', 26.58, 60, 179, false, 6, 'C');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (293, 0, 'Arthur', 'Payne', '1981-11-20', 30.860001, 66, 190, false, 6, 'PF');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (294, 0, 'Leroy', 'Moore', '1987-07-26', 45.799999, 89, 183, false, 6, 'PG');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (295, 0, 'Milton', 'Johnson', '1969-07-02', 22.219999, 68, 205, false, 6, 'SG');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (296, 0, 'Sammy', 'Mendez', '1986-07-01', 2.27, 96, 234, false, 6, 'SF');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (297, 0, 'Rafael', 'Steele', '1968-11-25', 15.48, 66, 153, false, 6, 'C');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (298, 0, 'Bob', 'Jacobs', '1988-05-21', 13.23, 72, 172, false, 6, 'PF');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (299, 0, 'Jared', 'Stevenson', '1974-05-03', 12.11, 125, 187, false, null, 'PG');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (300, 0, 'Shannon', 'Barton', '1980-03-10', 23.809999, 77, 197, false, null, 'SG');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (301, 0, 'Erick', 'Mccarthy', '1970-12-07', 3.76, 75, 150, false, null, 'SF');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (302, 0, 'Justin', 'Johnson', '1988-02-27', 47.950001, 137, 161, false, null, 'C');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (303, 0, 'Elmer', 'Hansen', '1984-07-12', 12.86, 88, 213, false, null, 'PF');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (304, 0, 'Everett', 'Sherman', '1978-11-03', 23.280001, 118, 213, false, null, 'PG');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (305, 0, 'Rafael', 'Ward', '1967-02-08', 38.77, 91, 178, false, null, 'SG');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (306, 0, 'Kenny', 'Rogers', '1965-02-02', 5.4400001, 68, 153, false, null, 'SF');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (308, 0, 'Lonnie', 'Brady', '1987-12-11', 11.83, 77, 219, false, null, 'C');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (309, 0, 'Sergio', 'Taylor', '1984-08-29', 25.290001, 133, 225, false, null, 'PF');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (310, 0, 'Byron', 'Morton', '1981-03-28', 21.4, 154, 196, false, null, 'PG');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (311, 0, 'Bob', 'Estrada', '1975-10-20', 39.59, 98, 197, false, null, 'SG');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (312, 0, 'Frank', 'Strickland', '1970-06-27', 21.879999, 116, 234, false, null, 'SF');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (313, 0, 'Johnathan', 'Norman', '1978-08-20', 17.620001, 117, 214, false, 7, 'C');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (314, 0, 'Leonard', 'Stephens', '1965-12-02', 13.12, 130, 168, false, 7, 'PF');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (315, 0, 'Tim', 'Taylor', '1985-01-26', 8.1599998, 146, 167, false, 7, 'PG');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (316, 0, 'Bobby', 'Weaver', '1979-11-25', 3.1099999, 99, 227, false, 7, 'SG');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (318, 0, 'Bernard', 'Wise', '1970-02-01', 48.48, 66, 219, false, 7, 'SF');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (319, 0, 'Rodolfo', 'Wong', '1968-08-10', 26.969999, 84, 191, false, null, 'C');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (320, 0, 'Wayne', 'Clarke', '1965-07-24', 24.9, 114, 154, false, null, 'PF');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (321, 0, 'Saul', 'Zimmerman', '1988-12-28', 45.82, 149, 201, false, null, 'PG');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (322, 0, 'Floyd', 'Hampton', '1988-03-03', 21.67, 115, 178, false, null, 'SG');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (323, 0, 'Timmy', 'Walker', '1969-04-14', 45.91, 149, 190, false, null, 'SF');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (324, 0, 'Jon', 'Hines', '1980-12-27', 24.879999, 68, 155, false, null, 'C');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (325, 0, 'Orlando', 'Burns', '1973-09-02', 19.139999, 159, 194, false, 7, 'PF');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (326, 0, 'Randal', 'Mendez', '1974-03-04', 21.200001, 141, 175, false, 7, 'PG');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (327, 0, 'Jeremy', 'Holmes', '1983-12-08', 38.299999, 118, 234, false, 7, 'SG');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (330, 0, 'Shannon', 'Watkins', '1979-02-07', 25.66, 69, 168, false, 7, 'SF');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (331, 0, 'Ralph', 'Burton', '1969-11-13', 7.52, 85, 175, false, 7, 'C');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (332, 0, 'Bobby', 'Jefferson', '1985-10-06', 17.4, 128, 240, false, null, 'PF');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (333, 0, 'Travis', 'Williamson', '1974-08-23', 20.299999, 115, 154, false, null, 'PG');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (334, 0, 'Matthew', 'Haynes', '1982-07-16', 18.91, 102, 218, false, null, 'SG');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (335, 0, 'Clayton', 'Doyle', '1976-12-10', 36.240002, 127, 177, false, null, 'SF');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (337, 0, 'Bradford', 'Porter', '1977-06-02', 16.190001, 111, 234, false, 8, 'C');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (339, 0, 'Bernard', 'Roy', '1987-05-07', 25.15, 65, 172, false, 8, 'PF');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (340, 0, 'Abraham', 'Page', '1978-03-20', 19.24, 125, 213, false, 8, 'PG');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (341, 0, 'Nathan', 'Banks', '1978-10-10', 7.1500001, 121, 161, false, 8, 'SG');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (343, 0, 'Marvin', 'Davis', '1980-06-13', 11, 123, 205, false, 8, 'SF');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (344, 0, 'Alberto', 'Dunn', '1969-11-23', 15.04, 127, 186, false, 8, 'C');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (345, 0, 'Kim', 'Hammond', '1979-07-31', 0.22, 130, 155, false, null, 'PF');
INSERT INTO player205 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team205_fk, position205_fk) VALUES (346, 0, 'Sylvester', 'Rowe', '1970-12-16', 34.459999, 94, 164, false, 8, 'PG');


ALTER TABLE team205 ADD CONSTRAINT team205_pkey PRIMARY KEY (id);

ALTER TABLE player205 ADD CONSTRAINT player205_pkey PRIMARY KEY (id);

ALTER TABLE ONLY player205
    ADD CONSTRAINT player205_position205_fkc FOREIGN KEY (position205_fk) REFERENCES position205(code);
	
ALTER TABLE ONLY player205
    ADD CONSTRAINT player205_team205_fkc FOREIGN KEY (team205_fk) REFERENCES team205(id);
