/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.entities.uc64_cascade;


import java.io.Serializable;
import java.util.List;

import com.bluage.documentation.business.standardfeatures.entities.uc64_cascade.bo.Audience64BO;
import com.bluage.documentation.business.standardfeatures.entities.uc64_cascade.bo.Coach64BO;
import com.bluage.documentation.business.standardfeatures.entities.uc64_cascade.bo.Player64BO;
import com.bluage.documentation.business.standardfeatures.entities.uc64_cascade.bo.State64BO;
import com.bluage.documentation.business.standardfeatures.entities.uc64_cascade.bo.Team64BO;
import com.netfective.bluage.core.web.AbstractForm;
import com.netfective.bluage.core.web.IForm;

/**
* Class : UC64_HomePageForm
*/
public class UC64_HomePageForm extends AbstractForm implements Serializable, IForm {
	
	/** Constant : serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	//CONSTANT : allcoachs
	private static final String ALLCOACHS = "allcoachs";
	//CONSTANT : teamToUpdate
	private static final String TEAM_TO_UPDATE = "teamToUpdate";
	//CONSTANT : allaudiences
	private static final String ALLAUDIENCES = "allaudiences";
	//CONSTANT : allstates
	private static final String ALLSTATES = "allstates";
	//CONSTANT : listTeams
	private static final String LIST_TEAMS = "listTeams";
	//CONSTANT : allPlayers
	private static final String ALL_PLAYERS = "allPlayers";
	//CONSTANT : selectedTeam
	private static final String SELECTED_TEAM = "selectedTeam";
	//CONSTANT : selectedState
	private static final String SELECTED_STATE = "selectedState";
	/**
	 * 	Property: allcoachs 
	 */
	private List<Coach64BO> allcoachs;
	/**
	 * 	Property: teamToUpdate 
	 */
	private Team64BO teamToUpdate;
	/**
	 * 	Property: allaudiences 
	 */
	private List<Audience64BO> allaudiences;
	/**
	 * 	Property: allstates 
	 */
	private List<State64BO> allstates;
	/**
	 * 	Property: listTeams 
	 */
	private List<Team64BO> listTeams;
	/**
	 * 	Property: allPlayers 
	 */
	private List<Player64BO> allPlayers;
	/**
	 * 	Property: selectedTeam 
	 */
	private Team64BO selectedTeam;
	/**
	 * 	Property: selectedState 
	 */
	private State64BO selectedState;
/**
	 * Default constructor : UC64_HomePageForm
	 */
	public UC64_HomePageForm() {
		super();
		// Initialize : allcoachs
		this.allcoachs = new java.util.ArrayList<com.bluage.documentation.business.standardfeatures.entities.uc64_cascade.bo.Coach64BO>();
		// Initialize : teamToUpdate
		this.teamToUpdate = null;
		// Initialize : allaudiences
		this.allaudiences = new java.util.ArrayList<com.bluage.documentation.business.standardfeatures.entities.uc64_cascade.bo.Audience64BO>();
		// Initialize : allstates
		this.allstates = new java.util.ArrayList<com.bluage.documentation.business.standardfeatures.entities.uc64_cascade.bo.State64BO>();
		// Initialize : listTeams
		this.listTeams = new java.util.ArrayList<com.bluage.documentation.business.standardfeatures.entities.uc64_cascade.bo.Team64BO>();
		// Initialize : allPlayers
		this.allPlayers = new java.util.ArrayList<com.bluage.documentation.business.standardfeatures.entities.uc64_cascade.bo.Player64BO>();
		// Initialize : selectedTeam
		this.selectedTeam = null;
		// Initialize : selectedState
		this.selectedState = null;
													this.setAlwaysCallPreControllers(true);
	}
	
	/**
	 * 	Getter : allcoachs 
	 * 	@return : Return the allcoachs instance.
	 */
	public List<Coach64BO> getAllcoachs(){
		return allcoachs; // For UC64_HomePageForm
	}
	
	/**
	 * 	Setter : allcoachs 
	 *  @param allcoachsinstance : The instance to set.
	 */
	public void setAllcoachs(final List<Coach64BO> allcoachsinstance){
		this.allcoachs = allcoachsinstance;// For UC64_HomePageForm
	}
	/**
	 * 	Getter : teamToUpdate 
	 * 	@return : Return the teamToUpdate instance.
	 */
	public Team64BO getTeamToUpdate(){
		return teamToUpdate; // For UC64_HomePageForm
	}
	
	/**
	 * 	Setter : teamToUpdate 
	 *  @param teamToUpdateinstance : The instance to set.
	 */
	public void setTeamToUpdate(final Team64BO teamToUpdateinstance){
		this.teamToUpdate = teamToUpdateinstance;// For UC64_HomePageForm
	}
	/**
	 * 	Getter : allaudiences 
	 * 	@return : Return the allaudiences instance.
	 */
	public List<Audience64BO> getAllaudiences(){
		return allaudiences; // For UC64_HomePageForm
	}
	
	/**
	 * 	Setter : allaudiences 
	 *  @param allaudiencesinstance : The instance to set.
	 */
	public void setAllaudiences(final List<Audience64BO> allaudiencesinstance){
		this.allaudiences = allaudiencesinstance;// For UC64_HomePageForm
	}
	/**
	 * 	Getter : allstates 
	 * 	@return : Return the allstates instance.
	 */
	public List<State64BO> getAllstates(){
		return allstates; // For UC64_HomePageForm
	}
	
	/**
	 * 	Setter : allstates 
	 *  @param allstatesinstance : The instance to set.
	 */
	public void setAllstates(final List<State64BO> allstatesinstance){
		this.allstates = allstatesinstance;// For UC64_HomePageForm
	}
	/**
	 * 	Getter : listTeams 
	 * 	@return : Return the listTeams instance.
	 */
	public List<Team64BO> getListTeams(){
		return listTeams; // For UC64_HomePageForm
	}
	
	/**
	 * 	Setter : listTeams 
	 *  @param listTeamsinstance : The instance to set.
	 */
	public void setListTeams(final List<Team64BO> listTeamsinstance){
		this.listTeams = listTeamsinstance;// For UC64_HomePageForm
	}
	/**
	 * 	Getter : allPlayers 
	 * 	@return : Return the allPlayers instance.
	 */
	public List<Player64BO> getAllPlayers(){
		return allPlayers; // For UC64_HomePageForm
	}
	
	/**
	 * 	Setter : allPlayers 
	 *  @param allPlayersinstance : The instance to set.
	 */
	public void setAllPlayers(final List<Player64BO> allPlayersinstance){
		this.allPlayers = allPlayersinstance;// For UC64_HomePageForm
	}
	/**
	 * 	Getter : selectedTeam 
	 * 	@return : Return the selectedTeam instance.
	 */
	public Team64BO getSelectedTeam(){
		return selectedTeam; // For UC64_HomePageForm
	}
	
	/**
	 * 	Setter : selectedTeam 
	 *  @param selectedTeaminstance : The instance to set.
	 */
	public void setSelectedTeam(final Team64BO selectedTeaminstance){
		this.selectedTeam = selectedTeaminstance;// For UC64_HomePageForm
	}
	/**
	 * 	Getter : selectedState 
	 * 	@return : Return the selectedState instance.
	 */
	public State64BO getSelectedState(){
		return selectedState; // For UC64_HomePageForm
	}
	
	/**
	 * 	Setter : selectedState 
	 *  @param selectedStateinstance : The instance to set.
	 */
	public void setSelectedState(final State64BO selectedStateinstance){
		this.selectedState = selectedStateinstance;// For UC64_HomePageForm
	}
/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UC64_HomePageForm [ "+
ALLCOACHS +" = " + allcoachs +TEAM_TO_UPDATE +" = " + teamToUpdate +ALLAUDIENCES +" = " + allaudiences +ALLSTATES +" = " + allstates +LIST_TEAMS +" = " + listTeams +ALL_PLAYERS +" = " + allPlayers +SELECTED_TEAM +" = " + selectedTeam +SELECTED_STATE +" = " + selectedState + "]";
	}
	
	/**
	 * Method setInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public void setInstanceByInstanceName(final String instanceName, final Object instance) {
		// Set the instance Allcoachs.
		if(ALLCOACHS.equals(instanceName)){// For UC64_HomePageForm
			this.setAllcoachs((List<Coach64BO>)instance); // For UC64_HomePageForm
		}
				// Set the instance TeamToUpdate.
		if(TEAM_TO_UPDATE.equals(instanceName)){// For UC64_HomePageForm
			this.setTeamToUpdate((Team64BO)instance); // For UC64_HomePageForm
		}
				// Set the instance Allaudiences.
		if(ALLAUDIENCES.equals(instanceName)){// For UC64_HomePageForm
			this.setAllaudiences((List<Audience64BO>)instance); // For UC64_HomePageForm
		}
				// Set the instance Allstates.
		if(ALLSTATES.equals(instanceName)){// For UC64_HomePageForm
			this.setAllstates((List<State64BO>)instance); // For UC64_HomePageForm
		}
				// Set the instance ListTeams.
		if(LIST_TEAMS.equals(instanceName)){// For UC64_HomePageForm
			this.setListTeams((List<Team64BO>)instance); // For UC64_HomePageForm
		}
				// Set the instance AllPlayers.
		if(ALL_PLAYERS.equals(instanceName)){// For UC64_HomePageForm
			this.setAllPlayers((List<Player64BO>)instance); // For UC64_HomePageForm
		}
				// Set the instance SelectedTeam.
		if(SELECTED_TEAM.equals(instanceName)){// For UC64_HomePageForm
			this.setSelectedTeam((Team64BO)instance); // For UC64_HomePageForm
		}
				// Set the instance SelectedState.
		if(SELECTED_STATE.equals(instanceName)){// For UC64_HomePageForm
			this.setSelectedState((State64BO)instance); // For UC64_HomePageForm
		}
			}
	
	/**
	 * Method getInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public Object getInstanceByInstanceName(final String instanceName){
		// Temporary object for tmpUC64_HomePageForm.
		Object tmpUC64_HomePageForm = null;
		
			
		// Get the instance Allcoachs for UC64_HomePageForm.
		if(ALLCOACHS.equals(instanceName)){ // For UC64_HomePageForm
			tmpUC64_HomePageForm = this.getAllcoachs(); // For UC64_HomePageForm
		}
			
		// Get the instance TeamToUpdate for UC64_HomePageForm.
		if(TEAM_TO_UPDATE.equals(instanceName)){ // For UC64_HomePageForm
			tmpUC64_HomePageForm = this.getTeamToUpdate(); // For UC64_HomePageForm
		}
			
		// Get the instance Allaudiences for UC64_HomePageForm.
		if(ALLAUDIENCES.equals(instanceName)){ // For UC64_HomePageForm
			tmpUC64_HomePageForm = this.getAllaudiences(); // For UC64_HomePageForm
		}
			
		// Get the instance Allstates for UC64_HomePageForm.
		if(ALLSTATES.equals(instanceName)){ // For UC64_HomePageForm
			tmpUC64_HomePageForm = this.getAllstates(); // For UC64_HomePageForm
		}
			
		// Get the instance ListTeams for UC64_HomePageForm.
		if(LIST_TEAMS.equals(instanceName)){ // For UC64_HomePageForm
			tmpUC64_HomePageForm = this.getListTeams(); // For UC64_HomePageForm
		}
			
		// Get the instance AllPlayers for UC64_HomePageForm.
		if(ALL_PLAYERS.equals(instanceName)){ // For UC64_HomePageForm
			tmpUC64_HomePageForm = this.getAllPlayers(); // For UC64_HomePageForm
		}
			
		// Get the instance SelectedTeam for UC64_HomePageForm.
		if(SELECTED_TEAM.equals(instanceName)){ // For UC64_HomePageForm
			tmpUC64_HomePageForm = this.getSelectedTeam(); // For UC64_HomePageForm
		}
			
		// Get the instance SelectedState for UC64_HomePageForm.
		if(SELECTED_STATE.equals(instanceName)){ // For UC64_HomePageForm
			tmpUC64_HomePageForm = this.getSelectedState(); // For UC64_HomePageForm
		}
		return tmpUC64_HomePageForm;// For UC64_HomePageForm
	}
	
											}
