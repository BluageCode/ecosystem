/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.application.datagrid.uc38_selectedlist ;


import org.apache.log4j.Logger;
import org.springframework.validation.Errors;

import com.netfective.bluage.core.validator.AbstractValidator;
/**
* Class :UC38_SelectedListValidator
*/
public class UC38_SelectedListValidator extends AbstractValidator{
	
	// LOGGER for the class UC38_SelectedListValidator
	private static final Logger LOGGER = Logger.getLogger( UC38_SelectedListValidator.class);
	
	
	/**
	* Operation validate for UC38_SelectedListForm
	* @param obj : the current form (UC38_SelectedListForm)
	* @param errors : The spring errors to return for the form UC38_SelectedListForm
	*/
	public void validate(final Object obj,final Errors errors){
		LOGGER.info("Beginning method : validate"); // UC38_SelectedListValidator
		UC38_SelectedListForm cUC38_SelectedListForm = (UC38_SelectedListForm)obj; // UC38_SelectedListValidator
		LOGGER.info("Ending method : validate the form "+ cUC38_SelectedListForm.getClass().getName()); // UC38_SelectedListValidator
	}

	/**
	* Method to implements to use spring validators (UC38_SelectedListForm)
	* @param aClass : Class for the form UC38_SelectedListForm
	* @return boolean
	*/
	public boolean supports(final Class aClass){
		return (new UC38_SelectedListForm()).getClass().equals(aClass); // UC38_SelectedListValidator
	}
}
