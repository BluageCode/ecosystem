/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.advancedfeatures.advancedtagsuse.uc205_popup ;

// Import declaration.
// Java imports.
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;

/**
 * Class : UC205_HomeController
 */
@Controller
@Scope("prototype")
@SessionAttributes("uC205_HomeForm")
@RequestMapping(value= "/presentation/advancedfeatures/advancedtagsuse/uc205_popup")
public class UC205_HomeController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC205_HomeController.class);
	
	//Current form name
	private static final String U_C205__HOME_FORM = "uC205_HomeForm";

	
	/**
	 * Property:customDateEditorsUC205_HomeController
	 */
	final private Map<String,CustomDateEditor> customDateEditorsUC205_HomeController = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:uC205_HomeValidator
	 */
	final private UC205_HomeValidator uC205_HomeValidator = new UC205_HomeValidator();
	
	
	// Initialise all the instances for UC205_HomeController

	/**
	* This method initialise the form : UC205_HomeForm 
	* @return UC205_HomeForm
	*/
	@ModelAttribute("UC205_HomeFormInit")
	public void initUC205_HomeForm(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, U_C205__HOME_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method UC205_HomeForm."); //for lnk_clear 
		}
		UC205_HomeForm uC205_HomeForm;
	
		if(request.getSession().getAttribute(U_C205__HOME_FORM) != null){
			uC205_HomeForm = (UC205_HomeForm)request.getSession().getAttribute(U_C205__HOME_FORM);
		} else {
			uC205_HomeForm = new UC205_HomeForm();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form UC205_HomeForm.");
		}
		uC205_HomeForm = (UC205_HomeForm)populateDestinationForm(request.getSession(), uC205_HomeForm);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  UC205_HomeForm.");
		}
		model.addAttribute(U_C205__HOME_FORM, uC205_HomeForm);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param UC205_HomeForm : The sceen form.
	 */
	@RequestMapping(value = "/UC205_Home.html" ,method = RequestMethod.GET)
	public void prepareUC205_Home(final HttpServletRequest request,final  Model model,final  @ModelAttribute("UC205_HomeFormInit") UC205_HomeForm uC205_HomeForm){
		
		UC205_HomeForm currentUC205_HomeForm = uC205_HomeForm;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(U_C205__HOME_FORM) == null){
			if(currentUC205_HomeForm!=null){
				request.getSession().setAttribute(U_C205__HOME_FORM, currentUC205_HomeForm);
			}else {
				currentUC205_HomeForm = new UC205_HomeForm();
				request.getSession().setAttribute(U_C205__HOME_FORM, currentUC205_HomeForm);	
			}
		} else {
			currentUC205_HomeForm = (UC205_HomeForm) request.getSession().getAttribute(U_C205__HOME_FORM);
		}

		if("cp_traitement".equals(request.getParameter("popupId"))){
			put(request.getSession(), "cp_traitementShowPopUp", true);
		}else{
			put(request.getSession(), "cp_traitementShowPopUp", false);
		}
		currentUC205_HomeForm = (UC205_HomeForm)populateDestinationForm(request.getSession(), currentUC205_HomeForm);
		// Call all the Precontroller.
	currentUC205_HomeForm.setAlwaysCallPreControllers(true);
		model.addAttribute(U_C205__HOME_FORM, currentUC205_HomeForm);
		request.getSession().setAttribute(U_C205__HOME_FORM, currentUC205_HomeForm);
		
	}
	
	
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("UC205_HomeController [ ");
		// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(uC205_HomeValidator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsUC205_HomeController!=null); 
		return strBToS.toString();
	}
}
