--<ScriptOptions statementTerminator=";"/>

ALTER TABLE player09 DROP CONSTRAINT player09_year_of_birth_fkc;

ALTER TABLE player09 DROP CONSTRAINT player09_pkey;

ALTER TABLE year09 DROP CONSTRAINT year09_pkey;

DROP TABLE year09;

DROP TABLE player09;

CREATE TABLE year09 (
		value09 INT4 NOT NULL,
		version INT4 NOT NULL
	);

CREATE TABLE player09 (
		id INT8 NOT NULL,
		version INT4 NOT NULL,
		first_name VARCHAR(30) NOT NULL,
		last_name VARCHAR(30) NOT NULL,
		weight INT4,
		height INT4,
		estimated_value FLOAT4,
		current_team VARCHAR(30) NOT NULL,
		year_of_birth_fk INT4 NOT NULL
	);


	
	INSERT INTO year09 (value09, version) VALUES (1960, 0);
INSERT INTO year09 (value09, version) VALUES (1961, 0);
INSERT INTO year09 (value09, version) VALUES (1962, 0);
INSERT INTO year09 (value09, version) VALUES (1963, 0);
INSERT INTO year09 (value09, version) VALUES (1964, 0);
INSERT INTO year09 (value09, version) VALUES (1965, 0);
INSERT INTO year09 (value09, version) VALUES (1966, 0);
INSERT INTO year09 (value09, version) VALUES (1967, 0);
INSERT INTO year09 (value09, version) VALUES (1968, 0);
INSERT INTO year09 (value09, version) VALUES (1969, 0);
INSERT INTO year09 (value09, version) VALUES (1970, 0);
INSERT INTO year09 (value09, version) VALUES (1971, 0);
INSERT INTO year09 (value09, version) VALUES (1972, 0);
INSERT INTO year09 (value09, version) VALUES (1973, 0);
INSERT INTO year09 (value09, version) VALUES (1974, 0);
INSERT INTO year09 (value09, version) VALUES (1975, 0);
INSERT INTO year09 (value09, version) VALUES (1976, 0);
INSERT INTO year09 (value09, version) VALUES (1977, 0);
INSERT INTO year09 (value09, version) VALUES (1978, 0);
INSERT INTO year09 (value09, version) VALUES (1979, 0);
INSERT INTO year09 (value09, version) VALUES (1980, 0);
INSERT INTO year09 (value09, version) VALUES (1981, 0);
INSERT INTO year09 (value09, version) VALUES (1982, 0);
INSERT INTO year09 (value09, version) VALUES (1983, 0);
INSERT INTO year09 (value09, version) VALUES (1984, 0);
INSERT INTO year09 (value09, version) VALUES (1985, 0);
INSERT INTO year09 (value09, version) VALUES (1986, 0);
INSERT INTO year09 (value09, version) VALUES (1987, 0);
INSERT INTO year09 (value09, version) VALUES (1988, 0);
INSERT INTO year09 (value09, version) VALUES (1989, 0);
INSERT INTO year09 (value09, version) VALUES (1990, 0);
INSERT INTO year09 (value09, version) VALUES (1991, 0);
INSERT INTO year09 (value09, version) VALUES (1992, 0);
INSERT INTO year09 (value09, version) VALUES (1993, 0);
INSERT INTO year09 (value09, version) VALUES (1994, 0);
INSERT INTO year09 (value09, version) VALUES (1995, 0);
INSERT INTO year09 (value09, version) VALUES (1996, 0);
INSERT INTO year09 (value09, version) VALUES (1997, 0);
INSERT INTO year09 (value09, version) VALUES (1998, 0);
INSERT INTO year09 (value09, version) VALUES (1999, 0);
INSERT INTO year09 (value09, version) VALUES (2000, 0);
	
INSERT INTO player09 (id, version, first_name, last_name, height, weight, estimated_value, current_team, year_of_birth_fk) VALUES (1, 0, 'Oliver', 'Frank', 228, 116, 0.11, 'Tigers', 1968);
INSERT INTO player09 (id, version, first_name, last_name, height, weight, estimated_value, current_team, year_of_birth_fk) VALUES (2, 0, 'Elbert', 'Cole', 171, 100, 3.95, 'Bears', 1965);
INSERT INTO player09 (id, version, first_name, last_name, height, weight, estimated_value, current_team, year_of_birth_fk) VALUES (3, 0, 'Abel', 'Dennis', 208, 105, 30.51, 'Chargers', 1980);
INSERT INTO player09 (id, version, first_name, last_name, height, weight, estimated_value, current_team, year_of_birth_fk) VALUES (4, 0, 'Earl', 'Russel', 228, 116, 29.3, 'Tigers', 1981);
INSERT INTO player09 (id, version, first_name, last_name, height, weight, estimated_value, current_team, year_of_birth_fk) VALUES (5, 0, 'Nick', 'Garner', 228, 116, 44.73, 'Tigers', 1992);
INSERT INTO player09 (id, version, first_name, last_name, height, weight, estimated_value, current_team, year_of_birth_fk) VALUES (6, 0, 'Howard', 'Ramos', 228, 116, 47.84, 'Hawks', 1970);
INSERT INTO player09 (id, version, first_name, last_name, height, weight, estimated_value, current_team, year_of_birth_fk) VALUES (7, 0, 'Allan', 'Meyer', 229, 114, 33.67, 'Red Devils', 1969);
INSERT INTO player09 (id, version, first_name, last_name, height, weight, estimated_value, current_team, year_of_birth_fk) VALUES (8, 0, 'Ruben', 'Pope', 201, 86, 44.66, 'Cardinals', 1985);
INSERT INTO player09 (id, version, first_name, last_name, height, weight, estimated_value, current_team, year_of_birth_fk) VALUES (9, 0, 'Lester', 'Cross', 214, 69, 30.98, 'Braves', 1976);
INSERT INTO player09 (id, version, first_name, last_name, height, weight, estimated_value, current_team, year_of_birth_fk) VALUES (10, 0, 'Keith', 'Barnett', 190, 66, 5.81, 'Eagles', 1976);



ALTER TABLE player09 ADD CONSTRAINT player09_pkey PRIMARY KEY (id);

ALTER TABLE year09 ADD CONSTRAINT year09_pkey PRIMARY KEY (value09);

ALTER TABLE player09 ADD CONSTRAINT player09_year_of_birth_fkc FOREIGN KEY (year_of_birth_fk)
	REFERENCES year09 (value09);

