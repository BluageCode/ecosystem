/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.netfective.bluage.springmvc.ajax.util;
     

/**
 * Class Utils
 * @author NETFECTIVE TECHNOLOGY
 */
public final class Utils {

	/**
	* Default builder
	*/
	private Utils() {}
	
	/**
     * Return the <code>Class</code> object for the specified fully qualified
     * class name, from this web application s class loader.
     *
     * @param className Fully qualified class name to be loaded
     * @return Class object
     * @exception ClassNotFoundException if the class cannot be found
     */
    public static Class applicationClass(final String className) throws ClassNotFoundException {

        // Look up the class loader to be used
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        if (classLoader == null) {
            classLoader = Utils.class.getClassLoader();
        }

        // Attempt to load the specified class
        return classLoader.loadClass(className);
    }
    
    /**
     * Return the <code>Class</code> object for the specified fully qualified
     * class name, from this web application s class loader.
     *
     * @param className Fully qualified class name to be loaded
     * @return Class object
     */
    public static Class tagApplicationClass(final String className)  {

        try {
			return applicationClass(className);
		} catch (ClassNotFoundException e) {
			return null;
		}
    }
}
