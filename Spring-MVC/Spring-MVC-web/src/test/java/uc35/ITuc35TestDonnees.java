package uc35;

import org.junit.Test;
import test.common.AbstractBADocTestCase;


public class ITuc35TestDonnees extends AbstractBADocTestCase {
	
	@Override
	public  String getName(){
		return "UC-35";
	}
	
	@Test
	public void testITuc35TestDonnees() throws Exception {
		// On teste l'UC35 - Stand features/Application/Datagrid - Paginator BDD
		selenium.open("");
		selenium.click("link=Expand All");
		selenium.waitForPageToLoad("30000");
		selenium.click("link=UC35 - Paginator BDD");
		verifyTrue(selenium.isElementPresent("//h2[contains(text(),\"Standard Features > Application > DataGrid > UC35 > Paginator BDD\")]"));
		// On clique sur [Display Players]
		selenium.click("//*[contains(@id,'lnk_paginatorbdd')]");
		selenium.waitForPageToLoad("30000");
		// On teste les donn�es.
		assertTrue(selenium.isElementPresent("//table/thead//tr[th[contains(text(),\"Name\")] and th[contains(text(),\"Date of Birth\")] and th[contains(text(),\"Weight (kg)\")] and th[contains(text(),\"Height (cm)\")] and th[contains(text(),\"Estimated Value\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Ryan\")] and td//span[contains(text(),\"Harmon\")] and td//span[contains(text(),\"06/09/1991\")] and td//span[contains(text(),\"121\")] and td//span[contains(text(),\"155\")] and td//span[contains(text(),\"9.88\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Harry\")] and td//span[contains(text(),\"Davis\")] and td//span[contains(text(),\"04/14/1988\")] and td//span[contains(text(),\"159\")] and td//span[contains(text(),\"237\")] and td//span[contains(text(),\"9.69\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Loren\")] and td//span[contains(text(),\"Colon\")] and td//span[contains(text(),\"12/14/1971\")] and td//span[contains(text(),\"149\")] and td//span[contains(text(),\"221\")] and td//span[contains(text(),\"30.14\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Bob\")] and td//span[contains(text(),\"Clark\")] and td//span[contains(text(),\"09/04/1972\")] and td//span[contains(text(),\"105\")] and td//span[contains(text(),\"224\")] and td//span[contains(text(),\"24.02\")]]"));
		selenium.click("xpath=(//span/a)[8]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Gerald\")] and td//span[contains(text(),\"Fox\")] and td//span[contains(text(),\"12/23/1991\")] and td//span[contains(text(),\"72\")] and td//span[contains(text(),\"193\")] and td//span[contains(text(),\"1.7\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Louis\")] and td//span[contains(text(),\"Stokes\")] and td//span[contains(text(),\"07/17/1976\")] and td//span[contains(text(),\"105\")] and td//span[contains(text(),\"186\")] and td//span[contains(text(),\"30.97\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Cameron\")] and td//span[contains(text(),\"Horton\")] and td//span[contains(text(),\"11/22/1992\")] and td//span[contains(text(),\"131\")] and td//span[contains(text(),\"236\")] and td//span[contains(text(),\"13.26\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Shane\")] and td//span[contains(text(),\"Sandoval\")] and td//span[contains(text(),\"09/24/1975\")] and td//span[contains(text(),\"131\")] and td//span[contains(text(),\"182\")] and td//span[contains(text(),\"24.14\")]]"));
		selenium.click("xpath=(//span/a)[10]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Roosevelt\")] and td//span[contains(text(),\"Doyle\")] and td//span[contains(text(),\"11/10/1983\")] and td//span[contains(text(),\"114\")] and td//span[contains(text(),\"235\")] and td//span[contains(text(),\"8.55\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Shane\")] and td//span[contains(text(),\"Daniel\")] and td//span[contains(text(),\"02/28/1990\")] and td//span[contains(text(),\"125\")] and td//span[contains(text(),\"195\")] and td//span[contains(text(),\"4.54\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Alton\")] and td//span[contains(text(),\"Harris\")] and td//span[contains(text(),\"02/13/1970\")] and td//span[contains(text(),\"120\")] and td//span[contains(text(),\"196\")] and td//span[contains(text(),\"20.34\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Sammy\")] and td//span[contains(text(),\"Clark\")] and td//span[contains(text(),\"05/05/1987\")] and td//span[contains(text(),\"152\")] and td//span[contains(text(),\"156\")] and td//span[contains(text(),\"14.0\")]]"));
		selenium.click("xpath=(//span/a)[10]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Johnathan\")] and td//span[contains(text(),\"Rodgers\")] and td//span[contains(text(),\"02/27/1983\")] and td//span[contains(text(),\"115\")] and td//span[contains(text(),\"210\")] and td//span[contains(text(),\"10.21\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Jordan\")] and td//span[contains(text(),\"Vaughn\")] and td//span[contains(text(),\"08/22/1982\")] and td//span[contains(text(),\"122\")] and td//span[contains(text(),\"230\")] and td//span[contains(text(),\"14.67\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Tim\")] and td//span[contains(text(),\"Collier\")] and td//span[contains(text(),\"06/16/1986\")] and td//span[contains(text(),\"69\")] and td//span[contains(text(),\"194\")] and td//span[contains(text(),\"2.19\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Dominic\")] and td//span[contains(text(),\"Barrett\")] and td//span[contains(text(),\"02/29/1984\")] and td//span[contains(text(),\"152\")] and td//span[contains(text(),\"240\")] and td//span[contains(text(),\"20.61\")]]"));
		selenium.click("xpath=(//span/a)[10]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Omar\")] and td//span[contains(text(),\"Woods\")] and td//span[contains(text(),\"04/26/1974\")] and td//span[contains(text(),\"111\")] and td//span[contains(text(),\"195\")] and td//span[contains(text(),\"48.93\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Devin\")] and td//span[contains(text(),\"Reeves\")] and td//span[contains(text(),\"02/25/1974\")] and td//span[contains(text(),\"82\")] and td//span[contains(text(),\"194\")] and td//span[contains(text(),\"32.31\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Ralph\")] and td//span[contains(text(),\"Hampton\")] and td//span[contains(text(),\"02/29/1992\")] and td//span[contains(text(),\"114\")] and td//span[contains(text(),\"196\")] and td//span[contains(text(),\"45.97\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Charles\")] and td//span[contains(text(),\"Dunn\")] and td//span[contains(text(),\"11/06/1971\")] and td//span[contains(text(),\"157\")] and td//span[contains(text(),\"200\")] and td//span[contains(text(),\"32.32\")]]"));
		selenium.click("xpath=(//span/a)[10]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Simon\")] and td//span[contains(text(),\"Greene\")] and td//span[contains(text(),\"07/04/1975\")] and td//span[contains(text(),\"126\")] and td//span[contains(text(),\"163\")] and td//span[contains(text(),\"43.39\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Isaac\")] and td//span[contains(text(),\"Austin\")] and td//span[contains(text(),\"01/03/1971\")] and td//span[contains(text(),\"123\")] and td//span[contains(text(),\"174\")] and td//span[contains(text(),\"31.13\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Raul\")] and td//span[contains(text(),\"Gray\")] and td//span[contains(text(),\"04/14/1986\")] and td//span[contains(text(),\"119\")] and td//span[contains(text(),\"218\")] and td//span[contains(text(),\"22.45\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Bruce\")] and td//span[contains(text(),\"Robinson\")] and td//span[contains(text(),\"03/07/1984\")] and td//span[contains(text(),\"138\")] and td//span[contains(text(),\"193\")] and td//span[contains(text(),\"40.0\")]]"));
		selenium.click("xpath=(//span/a)[10]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"David\")] and td//span[contains(text(),\"Goodman\")] and td//span[contains(text(),\"10/29/1965\")] and td//span[contains(text(),\"108\")] and td//span[contains(text(),\"152\")] and td//span[contains(text(),\"32.27\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Larry\")] and td//span[contains(text(),\"Bradley\")] and td//span[contains(text(),\"04/20/1977\")] and td//span[contains(text(),\"117\")] and td//span[contains(text(),\"213\")] and td//span[contains(text(),\"40.84\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Nicholas\")] and td//span[contains(text(),\"Powell\")] and td//span[contains(text(),\"11/08/1983\")] and td//span[contains(text(),\"139\")] and td//span[contains(text(),\"152\")] and td//span[contains(text(),\"34.93\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Zachary\")] and td//span[contains(text(),\"Brewer\")] and td//span[contains(text(),\"10/22/1989\")] and td//span[contains(text(),\"108\")] and td//span[contains(text(),\"158\")] and td//span[contains(text(),\"31.3\")]]"));
		selenium.click("xpath=(//span/a)[10]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Tyrone\")] and td//span[contains(text(),\"Weber\")] and td//span[contains(text(),\"12/08/1981\")] and td//span[contains(text(),\"140\")] and td//span[contains(text(),\"157\")] and td//span[contains(text(),\"31.9\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Ed\")] and td//span[contains(text(),\"Young\")] and td//span[contains(text(),\"04/07/1969\")] and td//span[contains(text(),\"128\")] and td//span[contains(text(),\"219\")] and td//span[contains(text(),\"19.95\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Thomas\")] and td//span[contains(text(),\"Copeland\")] and td//span[contains(text(),\"04/06/1974\")] and td//span[contains(text(),\"113\")] and td//span[contains(text(),\"188\")] and td//span[contains(text(),\"24.83\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Lamar\")] and td//span[contains(text(),\"Lopez\")] and td//span[contains(text(),\"05/15/1979\")] and td//span[contains(text(),\"78\")] and td//span[contains(text(),\"180\")] and td//span[contains(text(),\"27.21\")]]"));
		selenium.click("xpath=(//span/a)[10]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Troy\")] and td//span[contains(text(),\"Stone\")] and td//span[contains(text(),\"08/24/1979\")] and td//span[contains(text(),\"65\")] and td//span[contains(text(),\"169\")] and td//span[contains(text(),\"42.52\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Arnold\")] and td//span[contains(text(),\"Ford\")] and td//span[contains(text(),\"06/02/1976\")] and td//span[contains(text(),\"79\")] and td//span[contains(text(),\"225\")] and td//span[contains(text(),\"48.24\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Stuart\")] and td//span[contains(text(),\"Munoz\")] and td//span[contains(text(),\"04/25/1973\")] and td//span[contains(text(),\"132\")] and td//span[contains(text(),\"192\")] and td//span[contains(text(),\"3.13\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Drew\")] and td//span[contains(text(),\"Nash\")] and td//span[contains(text(),\"11/27/1992\")] and td//span[contains(text(),\"86\")] and td//span[contains(text(),\"218\")] and td//span[contains(text(),\"33.53\")]]"));
		selenium.click("xpath=(//span/a)[10]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Grant\")] and td//span[contains(text(),\"Hudson\")] and td//span[contains(text(),\"04/02/1967\")] and td//span[contains(text(),\"66\")] and td//span[contains(text(),\"172\")] and td//span[contains(text(),\"9.65\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Joel\")] and td//span[contains(text(),\"Love\")] and td//span[contains(text(),\"10/07/1968\")] and td//span[contains(text(),\"66\")] and td//span[contains(text(),\"191\")] and td//span[contains(text(),\"6.26\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Jared\")] and td//span[contains(text(),\"Mullins\")] and td//span[contains(text(),\"07/09/1981\")] and td//span[contains(text(),\"143\")] and td//span[contains(text(),\"189\")] and td//span[contains(text(),\"0.13\")]]"));
	}


}
