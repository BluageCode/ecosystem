<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file="/include.jsp"  %>
<html >
<head >
<title  > 
<fmt:message key="cUC200_DatagridLoading_BLU_AGE_DOCUMENTATION"/>
</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"  > 
</meta>
<link href="<c:url value= "/presentation/_style/_style.css"/>" rel="stylesheet" type="text/css"  /> 
<link href="<c:url value= "/presentation/_include/jquery/rd_lib.css"/>" rel="stylesheet" type="text/css"  /> 
<script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/jquery.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/rd_setup.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/rd_lib.js"/>" space="preserve"  ></script><link rel="shortcut icon" href="<c:url value= "/presentation/_style/favicon.ico"/>"  /> 

</head>

<body class=" " > 
<div id="rd_ghost"  > 
<small  > 
<fmt:message key="cUC200_DatagridLoading___CSS_DEACTIVATED"/>
</small>
</div>
<div id="rd_jghost"  > 
<small  > 
<fmt:message key="cUC200_DatagridLoading___JAVASCRIPT_DEACTIVATED"/>
</small>
</div>
<div id="shape"  > 
<div class="ui-layout-center"  > 
<div class="east-north"  > 
	<form:form id="uC200_DatagridLoading" commandName="uC200_DatagridLoadingForm"  enctype="application/x-www-form-urlencoded" method="post">
<h2 class="rd_first"  > 
<fmt:message key="cUC200_DatagridLoading_ADVANCED_FEATURES__PERFORMANC"/>
</h2>
<table class="rd_tableform"  >
<tr  > 
<td rowspan="1" colspan="1"  >
<a id="lnk_dataloading_1"				href="<c:url value="/presentation/advancedfeatures/performance/uc200_datagridloading/UC200_DatagridLoading/lnk_dataloading_1.html"/>"
													 shape="rect"  >									 
<fmt:message key="cUC200_DatagridLoading_DATA_LOADING"/>
</a>
</td> 
<td rowspan="1" colspan="1"  >
<fmt:message key="cUC200_DatagridLoading____________________"/>
</td> 
<td rowspan="1" colspan="1"  >
<a id="lnk_dataloading_n1"				href="<c:url value="/presentation/advancedfeatures/performance/uc200_datagridloading/UC200_DatagridLoading/lnk_dataloading_n1.html"/>"
													 shape="rect"  >									 
<fmt:message key="cUC200_DatagridLoading_N1_DATA_LOADING"/>
</a>
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<a id="lnk_clear"				href="<c:url value="/presentation/advancedfeatures/performance/uc200_datagridloading/UC200_DatagridLoading/lnk_clear.html"/>"
													 shape="rect"  >									 
<fmt:message key="cUC200_DatagridLoading_CLEAR"/>
</a>
</td> 
</tr>
<tr  > 
</tr>
</table> 
<display:table uid="tab_players"  sort="list" requestURI="" name="sessionScope.uC200_DatagridLoadingForm.players" class="rd_tabledata"  >
	<c:set   var="rownum">
		<%=(tab_players !=	null) ? new Integer(tab_players_rowNum.intValue() - 1) : new Integer(0) %>
	</c:set>
	<c:set   var="idRow">
		<%=(tab_players != null ) ? tab_players_rowNum.intValue() - 1 : -1 %>
	</c:set>
	<c:set   var="idTab">
		<%=(tab_players != null ) ? "tab_players" : null %>
	</c:set>
	<c:set var="tableHashId">
		<%= new org.displaytag.util.ParamEncoder("tab_players").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE) %>
	</c:set>
	<c:set var="tableHashId2">
		<%=request.getParameter(new org.displaytag.util.ParamEncoder("tab_players").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE)) %>
	</c:set>	
	
<display:column format=""  titleKey="cUC200_DatagridLoading_LAST_NAME">
		<span id="txt_lastname1">
	<c:out value="${tab_players.lastName}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="cUC200_DatagridLoading_FIRST_NAME">
		<span id="txt_firstname1">
	<c:out value="${tab_players.firstName}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="cUC200_DatagridLoading_PLAYS">
		<span id="txt_plays">
	<c:out value="${tab_players.stats.plays}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="cUC200_DatagridLoading_SHOOTS">
		<span id="txt_shoots">
	<c:out value="${tab_players.stats.shoots}" />
		</span>
	</display:column>
</display:table>
	</form:form>
	<script language="javascript">
		function btn_click_cExecuteUC200_DatagridLoading(url) {
			UC200_DatagridLoadingvar = document.getElementById('uC200_DatagridLoading');
			UC200_DatagridLoadingvar.setAttribute('action', url);
			UC200_DatagridLoadingvar.submit();
		}
		function btn_click_cSetActionUC200_DatagridLoading(url) {
			UC200_DatagridLoadingvar = document.getElementById('uC200_DatagridLoading');
			UC200_DatagridLoadingvar.setAttribute('action', url);
		}
	</script>
</div>
<div class="east-center"  > 
<tiles:insertDefinition	name="presentation/advancedfeatures/performance/uc200_datagridloading/UC200_DatagridLoadingcmp_tabs" />
</div>
</div>
<div class="ui-layout-north"  > 
<tiles:insertDefinition	name="presentation/advancedfeatures/performance/uc200_datagridloading/UC200_DatagridLoadingcmp_header" />
</div>
<div class="ui-layout-south"  > 
<fmt:message key="cUC200_DatagridLoading_______"/>
<span id="rd_jversion"  > 
<fmt:message key="cUC200_DatagridLoading_BLU_AGE_DOCUMENTATION_4"/>
</span>
</div>
<div class="ui-layout-west"  > 
<tiles:insertDefinition	name="presentation/advancedfeatures/performance/uc200_datagridloading/UC200_DatagridLoadingcmp_treeview" />
</div>
</div>
</body>
</html>
