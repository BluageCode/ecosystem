/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.business.standardfeatures.application.datagrid.uc38_selectedlist.bos;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.inject.Inject;
import javax.inject.Named;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.LazyInitializationException;

import com.bluage.documentation.business.common.bos.TeamBO;
import com.bluage.documentation.business.standardfeatures.application.datagrid.uc38_selectedlist.entities.daofinder.Team38DAOFinderImpl;
import com.netfective.bluage.core.exception.ApplicationException;

/**
 * Team38BO
 *
* @see com.netfective.bluage.core.business.IBusinessObject
* * @see TeamBO
 */
@XmlRootElement

public class Team38BO
 extends TeamBO {
	
	/**
	 * Default SUID for Team38BO.
	 */
	private static final long serialVersionUID = 1L;
	

	@Inject
	@Named ("com.bluage.documentation.business.standardfeatures.application.datagrid.uc38_selectedlist.entities.daofinder.Team38Finder") 
	private Team38DAOFinderImpl finder;

																			/**
	 * Entity property: player38s
	 *
	 */
private Set<Player38BO> player38s = new HashSet<Player38BO>();



   /**
	* public default constructor for class Team38BO
	*/
	public Team38BO(){
		//NOP Team38BO
	}


																			/**
	 * Getter for player38s of class Team38BO.
	 *
	 * @return {@link java.util.Set<com.bluage.documentation.business.standardfeatures.application.datagrid.uc38_selectedlist.bos.Player38BO>}
	 * @generated
	 */ 
public  Set<Player38BO> getPlayer38s() {
	return this.player38s;
			}


	/**
	 * Setter for player38s of class Team38BO.
	 * @param value {@link java.util.Set<com.bluage.documentation.business.standardfeatures.application.datagrid.uc38_selectedlist.bos.Player38BO>}
	 * @generated
	 */

public  void setPlayer38s(final Set<Player38BO> player38s) {
	this.player38s = player38s;
}

	/**
	 * Remove an entity from the com.bluage.documentation.business.standardfeatures.application.datagrid.uc38_selectedlist.bos.Player38BO collection.
	 *
	 * @param entity
	 *		: {@link Set<Player38BO>} the entity
	 * @throws ApplicationException
     */
public void removeFromPlayer38s( Player38BO entity ) throws ApplicationException {
	try {
		if( getPlayer38s() != null && getPlayer38s().contains(entity) ) {
			getPlayer38s().remove( entity );
		entity.setTeam38( null );
		}
		} catch ( LazyInitializationException e ) {
				Team38BO bo = ( Team38BO ) finder.findByPrimaryKey( getId() );
			finder.getHibernateTemplate().initialize( bo.getPlayer38s() );
			setPlayer38s( bo.getPlayer38s() );
			// evict object from session to avoid NonUniqueObjectException
			finder.getHibernateTemplate().evict( bo );
		
			if( getPlayer38s() != null && getPlayer38s().contains( entity ) ) {
				getPlayer38s().remove( entity );
				entity.setTeam38( null );
			}
				}
	}
	
	/**
	 * Add entity to the com.bluage.documentation.business.standardfeatures.application.datagrid.uc38_selectedlist.bos.Player38BO collection.
	 *
	 * @param entity
	 *		{@link Player38BO} the entity
	 * @throws ApplicationException
     */
public void addToPlayer38s( Player38BO entity ) throws ApplicationException
	{
		try {
			if( getPlayer38s() != null && !getPlayer38s().contains( entity ) ) {
				getPlayer38s().add( entity );
				entity.setTeam38( this );
			}
			} catch ( LazyInitializationException e ) {
			Team38BO bo = ( Team38BO ) finder.findByPrimaryKey( getId() );
		finder.getHibernateTemplate().initialize( bo.getPlayer38s() );
		setPlayer38s( bo.getPlayer38s() );
		// evict object from session to avoid NonUniqueObjectException
		finder.getHibernateTemplate().evict( bo );
		if( getPlayer38s() != null && !getPlayer38s().contains( entity ) ) {
			getPlayer38s().add( entity );
			entity.setTeam38( this );
		}
			}
	}
	
	/**
	 * Add entitys to the com.bluage.documentation.business.standardfeatures.application.datagrid.uc38_selectedlist.bos.Player38BO collection
	 *
	 * @param entities			
	 *		: {@link java.util.List} A list of entities
	 * @throws ApplicationException 
     */	
public void addAllToPlayer38s( Collection<Player38BO> entities ) throws ApplicationException {
	for( Player38BO entity : entities) {
		if( getPlayer38s()!=null ) {
			addToPlayer38s( entity );
		}
	}
}
	
	/**
	 * Remove entities form the com.bluage.documentation.business.standardfeatures.application.datagrid.uc38_selectedlist.bos.Player38BO collection
	 *
	 * @param entities				
	 *		: {@link java.util.List} a list of entities
	 * @throws ApplicationException
     */	
public void removeAllFromPlayer38s( Collection<Player38BO> entities ) throws ApplicationException {
	for( Player38BO entity : entities) {
		if( getPlayer38s()!=null && getPlayer38s().contains( entity ) )	{		
			removeFromPlayer38s( entity );
		}
	}
}
	
	/**
	 * Remove all items form the collection com.bluage.documentation.business.standardfeatures.application.datagrid.uc38_selectedlist.bos.Player38BO
	 *
	 * @throws ApplicationException
     */	
public void removeAllFromPlayer38s() throws ApplicationException {
	Set<Player38BO> entities = this.getPlayer38s();
	if ( entities == null ) {
		return;
	}
	Iterator<Player38BO> it = entities.iterator();
	while ( it.hasNext() ) {
    	Player38BO entity = it.next(); 
		entity.setTeam38( null ); 
	}
	entities.clear();
}

/**
	 * Returns <code>true</code> if the argument is an Team38 instance and all identifiers for this entity
     * equal the identifiers of the argument entity. The <code>equals</code> method of the parent entity
     * will also need to return <code>true</code>. Returns <code>false</code> otherwise.
     *
     * @see TeamBO#hashCode()
	 * @param object  object to compare with current instance      
     * @return boolean true if equals false if not
     */
    public boolean equals(final Object object )
    {
		if ( this == object )
        {
            return true;
        }
        if ( !( object instanceof Team38BO ) )
        {
            return false;
        }
        final Team38BO that = (Team38BO)object;
    
    	            if (        				        			this.getId() == null
        	        ) {
        	return false;
        }
		return new EqualsBuilder()
			.append(this.getId(), that.getId())
			 	.isEquals();
	}
	/**

	 * Returns a hash code based on this entity's identifiers and the hash code of the parent entity.
     *
     * @see TeamBO#hashCode()
     * @return int
     */    
    public int hashCode() {
        return new HashCodeBuilder()
             .append(getId())
				 	.appendSuper(super.hashCode())
            .toHashCode();
    }
}

