/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.application.navigation.uc31_preaction ;

// Import declaration.
// Java imports.
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;

/**
 * Class : UC31_DisplayPlayersController
 */
@Controller
@Scope("prototype")
@SessionAttributes("uC31_DisplayPlayersForm")
@RequestMapping(value= "/presentation/standardfeatures/application/navigation/uc31_preaction")
public class UC31_DisplayPlayersController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC31_DisplayPlayersController.class);
	
	//Current form name
	private static final String U_C31__DISPLAY_PLAYERS_FORM = "uC31_DisplayPlayersForm";

	//CONSTANT: playersList table or repeater.
	private static final String PLAYERS_LIST = "playersList";
				
	/**
	 * Property:customDateEditorsUC31_DisplayPlayersController
	 */
	final private Map<String,CustomDateEditor> customDateEditorsUC31_DisplayPlayersController = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:uC31_DisplayPlayersValidator
	 */
	final private UC31_DisplayPlayersValidator uC31_DisplayPlayersValidator = new UC31_DisplayPlayersValidator();
	
	/**
	 * Pre Controller declaration : uC31_PreControllerDisplayPlayers
	 */
	@Autowired
	private UC31_PreControllerDisplayPlayersController uC31_PreControllerDisplayPlayers;

	
	// Initialise all the instances for UC31_DisplayPlayersController
		// Initialize the instance playersList for the state lnk_stadiums
		private List playersList; // Initialize the instance playersList for UC31_DisplayPlayersController
				
	/**
	* This method initialise the form : UC31_DisplayPlayersForm 
	* @return UC31_DisplayPlayersForm
	*/
	@ModelAttribute("UC31_DisplayPlayersFormInit")
	public void initUC31_DisplayPlayersForm(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, U_C31__DISPLAY_PLAYERS_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method UC31_DisplayPlayersForm."); //for lnk_stadiums 
		}
		UC31_DisplayPlayersForm uC31_DisplayPlayersForm;
	
		if(request.getSession().getAttribute(U_C31__DISPLAY_PLAYERS_FORM) != null){
			uC31_DisplayPlayersForm = (UC31_DisplayPlayersForm)request.getSession().getAttribute(U_C31__DISPLAY_PLAYERS_FORM);
		} else {
			uC31_DisplayPlayersForm = new UC31_DisplayPlayersForm();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form UC31_DisplayPlayersForm.");
		}
		uC31_DisplayPlayersForm = (UC31_DisplayPlayersForm)populateDestinationForm(request.getSession(), uC31_DisplayPlayersForm);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  UC31_DisplayPlayersForm.");
		}
		model.addAttribute(U_C31__DISPLAY_PLAYERS_FORM, uC31_DisplayPlayersForm);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param UC31_DisplayPlayersForm : The sceen form.
	 */
	@RequestMapping(value = "/UC31_DisplayPlayers.html" ,method = RequestMethod.GET)
	public void prepareUC31_DisplayPlayers(final HttpServletRequest request,final  Model model,final  @ModelAttribute("UC31_DisplayPlayersFormInit") UC31_DisplayPlayersForm uC31_DisplayPlayersForm){
		
		UC31_DisplayPlayersForm currentUC31_DisplayPlayersForm = uC31_DisplayPlayersForm;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(U_C31__DISPLAY_PLAYERS_FORM) == null){
			if(currentUC31_DisplayPlayersForm!=null){
				request.getSession().setAttribute(U_C31__DISPLAY_PLAYERS_FORM, currentUC31_DisplayPlayersForm);
			}else {
				currentUC31_DisplayPlayersForm = new UC31_DisplayPlayersForm();
				request.getSession().setAttribute(U_C31__DISPLAY_PLAYERS_FORM, currentUC31_DisplayPlayersForm);	
			}
		} else {
			currentUC31_DisplayPlayersForm = (UC31_DisplayPlayersForm) request.getSession().getAttribute(U_C31__DISPLAY_PLAYERS_FORM);
		}

				currentUC31_DisplayPlayersForm = (UC31_DisplayPlayersForm)populateDestinationForm(request.getSession(), currentUC31_DisplayPlayersForm);
		// Call all the Precontroller.
	if ( currentUC31_DisplayPlayersForm.isAlwaysCallPreControllers()) {
	
		// Calling the PreController : UC31_PreControllerDisplayPlayers.
		currentUC31_DisplayPlayersForm = (UC31_DisplayPlayersForm) uC31_PreControllerDisplayPlayers.uC31_PreControllerDisplayPlayersControllerInit(request, model, currentUC31_DisplayPlayersForm);
		
	}
	currentUC31_DisplayPlayersForm.setAlwaysCallPreControllers(true);
		model.addAttribute(U_C31__DISPLAY_PLAYERS_FORM, currentUC31_DisplayPlayersForm);
		request.getSession().setAttribute(U_C31__DISPLAY_PLAYERS_FORM, currentUC31_DisplayPlayersForm);
		
	}
	
			
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("UC31_DisplayPlayersController [ ");
			strBToS.append(PLAYERS_LIST);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(playersList);
			strBToS.append(END_TO_STRING_DELIMITER);
					// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(uC31_DisplayPlayersValidator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsUC31_DisplayPlayersController!=null); 
		return strBToS.toString();
	}
}
