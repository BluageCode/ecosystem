/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.netfective.bluage.springmvc.ajax.parser;


import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

import javax.swing.text.MutableAttributeSet;
import javax.swing.text.html.HTML;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.parser.ParserDelegator;

import org.apache.log4j.Logger;
/**
 * Class ResponseParserHTML
 * @interface ResponseParser
 * @author NETFECTIVE TECHNOLOGY
 */
public final class ResponseParserHTML implements ResponseParser {

    /**
    * Is <code>Pattern</code> constant : SCRIPT_START_PATTERN
    */
    public static final Pattern SCRIPT_START_PATTERN = Pattern.compile("<script",Pattern.CASE_INSENSITIVE);
    
    /**
    * Is <code>Pattern</code> constant : SCRIPT_END_PATTERN
    */
    public static final Pattern SCRIPT_END_PATTERN = Pattern.compile("</script",Pattern.CASE_INSENSITIVE);
    
    /**
    * Is <code>int</code> constant : NUMBER_THREE
    */
    public static final int NUMBER_THREE = 3;
    
    /**
    * Is <code>int</code> constant : NUMBER_FOUR
    */
    public static final int NUMBER_FOUR = 4;
    
    /**
    * Is <code>int</code> constant : NUMBER_FIVE
    */
    public static final int NUMBER_FIVE = 5;
    
    /**
    * Is <code>int</code> constant : NUMBER_NINE
    */
    public static final int NUMBER_NINE = 9;
    
    /**
     * LOGGER.
     */
    private static final  Logger LOGGER = Logger.getLogger(ResponseParserHTML.class);

    
    /**
    * Unique instance of this class
    */
    private static ResponseParser ourInstance = new ResponseParserHTML();
    
	/**
	* Default builder
	*/
    private ResponseParserHTML() {}

	/**
	* Getter for ourInstance
	* @return ResponseParserFactory
	*/
    public static ResponseParser getInstance() {
        return ourInstance;
    }

	/**
	* This method parse a HTML page
	* @param String :
	*			html
	* @return ResponseBean
	*/
    public ResponseBean parse(final String html) {
        // javax.swing.text.html.parser bug workaround.
        // Swing HTML parser interpretes incorrectly <> and </> substrings
        // therefore we replace <> and </> with dummy strings and replace them back after parsing.
		
        final String dummy = findDummy(html);
        String htmlCopy = html;
        htmlCopy = htmlCopy.replaceAll("<>", "<" + dummy + ">").replaceAll("</>", "</" + dummy + ">");

        // JSE 6 regression workaround : http://forum.java.sun.com/thread.jspa?threadID=5118473
        // let s use a deprecated DIR tag instead of SCRIPT
        htmlCopy = SCRIPT_START_PATTERN.matcher(htmlCopy).replaceAll("<DIR");
        htmlCopy = SCRIPT_END_PATTERN.matcher(htmlCopy).replaceAll("</DIR");

      	final ResponseBean responseBean = doParse(htmlCopy);
        final String htmlContent = responseBean.getHtmlContent();

        if (htmlContent != null){
            responseBean.setHtmlContent(htmlContent.replaceAll("<" + dummy + ">", "<>").replaceAll("</" + dummy + ">", "</>"));
		}
		
        int i =0;
        for (final Iterator iterator = responseBean.getScriptContents().iterator(); iterator.hasNext();) {
        	final String s = (String) iterator.next();
            responseBean.getScriptContents().set(i, s.replaceAll("<" + dummy + ">", "<>").replaceAll("</" + dummy + ">", "</>"));
            i++;
        }
        return responseBean;
    }

	/**
	* This method finds a unused mark, called dummy, in html code
	* @param String :
	*			html
	* @return String 
	*/
    private String findDummy(final String html) {
        String dummy;
        do {
            dummy = Double.toString(Math.random());
        } while (html.indexOf(dummy) != -1);

        return dummy;
    }

	/**
	* This method puts script codes (<code><DIR>---</DIR></code> and images links 
	* from html code into a <code>ResponseBean</code>
	* @param String :
	*			html
	* @return ResponseBean
	* @throw RuntimeException
	*/
    public ResponseBean doParse(final String html) {

        final ResponseBean res = new ResponseBean();
        try {
            final StringBuffer contentHTML = new StringBuffer();
            final List scripts = res.getScriptContents();
            final Set images = res.getImages();

           		final HTMLEditorKit.ParserCallback callback = new HTMLEditorKit.ParserCallback() {
				private boolean insideScript;
				private StringBuilder scriptContent = new StringBuilder();
				private int lastStop;
				/**
				 * 
				 * @param str
				 */
				private void append(final String str) {
					if (insideScript) {
						scriptContent.append(str);
					} else {
						contentHTML.append(str);
					}
				}
				
				/**
				 * 
				 * @param newPos
				 */
				public void appendSinceLastStop(final int newPos) {
					if (lastStop > newPos) {
						return;
					}
					append(html.substring(lastStop, newPos));
					lastStop = newPos;
				}
				
				/**
				 * 
				 */
				private void flushScript() {
					int posScriptEnd = scriptContent.indexOf(">");
					if (posScriptEnd == -1) {
						posScriptEnd = 0;
					}
					int posC1 = scriptContent.indexOf("<!--", posScriptEnd);
					int posC11 = scriptContent.indexOf("<![CDATA[", posScriptEnd);
					int posQ1 = scriptContent.indexOf("'", posScriptEnd);
					int posQ2 = scriptContent.indexOf("\"", posScriptEnd);
					if ((posC1 != -1) && (posQ2 == -1 || posC1 < posQ2) && (posQ1 == -1 || posC1 < posQ1)) {
						scriptContent.delete(posC1, posC1 + NUMBER_FOUR);
					}
					if ((posC11 != -1) && (posQ2 == -1 || posC11 < posQ2) && (posQ1 == -1 || posC11 < posQ1)) {
						scriptContent.delete(posC11, posC11 + NUMBER_NINE);
					}
					posQ1 = scriptContent.lastIndexOf("'", posScriptEnd);
					posQ2 = scriptContent.lastIndexOf("\"", posScriptEnd);
					int posC2 = scriptContent.indexOf("-->", posScriptEnd);
					int posC22 = scriptContent.indexOf("//]]>", posScriptEnd);
					if ((posC2 != -1) && (posQ2 == -1 || posC2 > posQ2) && (posQ1 == -1 || posC2 > posQ1)) {
						scriptContent.delete(posC2, posC2 + NUMBER_THREE);
					}
					if ((posC22 != -1) && (posQ2 == -1 || posC22 > posQ2) && (posQ1 == -1 || posC22 > posQ1)) {
						scriptContent.delete(posC22, posC22 + NUMBER_FIVE);
					}
					int len = scriptContent.length();
					if (len > 0 && scriptContent.charAt(len - 1) == '>') {
						int lastEndTagPos = scriptContent.lastIndexOf("</");
						if (lastEndTagPos != -1) {
							scriptContent.setLength(lastEndTagPos);
						}
					}
					scripts.add(scriptContent.toString());
					scriptContent.setLength(0);
				}
				
				/**
				 * 
				 * @param data
				 * @param pos
				 */
				public void handleText(final char[] data, final int pos) {
					appendSinceLastStop(pos);
				}
				
				/**
				 * @param data
				 * @param pos
				 */
				public void handleComment(final char[] data, final int pos) {
					appendSinceLastStop(pos);
					if (data == null) {
						flushScript();
					}
				}
				
				/**
				 * @param errorMsg
				 * @param pos
				 */
				public void handleError(final String errorMsg, final int pos) {}
				
				/**
				 * @param tag
				 * @param pos
				 */
				public void handleEndTag(final HTML.Tag tag, final int pos) {
					if (pos == -1) {
						return;
					}
					if (lastStop > pos) {
						return;
					}
					appendSinceLastStop(pos);
					if (tag == HTML.Tag.DIR && insideScript) {
						/** DIR is prevously replaced SCRIPT tag to **/
						int posScriptEnd = html.indexOf('>', pos);
						if (posScriptEnd != -1) {
							lastStop = posScriptEnd + 1;
						}
						insideScript = false;
						flushScript();
					}
				}
				
				/**
				 * @param tag
				 * @param attributes
				 * @param pos
				 */
				public void handleSimpleTag(final HTML.Tag tag, final MutableAttributeSet attributes, final int pos) {
					handleStartTag(tag, attributes, pos);
				}
				
				/**
				 * @param tag
				 * @param attributes
				 * @param pos
				 */
				public void handleStartTag(final HTML.Tag tag, final MutableAttributeSet attributes, final int pos) {
					appendSinceLastStop(pos);
					if (tag == HTML.Tag.DIR) {
						/** DIR is prevously replaced SCRIPT tag to **/
						insideScript = true;
					} else if (tag == HTML.Tag.IMG) {
						images.add(attributes.getAttribute(HTML.Attribute.SRC));
					}
				}
			};

            final Reader reader = new StringReader(html);
            new ParserDelegator().parse(reader, callback, false);
            callback.handleComment(null, html.length());

            res.setHtmlContent(contentHTML.toString());
            return res;

        } catch (IOException e) {
            LOGGER.error(e.toString(), e); // this should never happen
        }
        return res;
    }

}
