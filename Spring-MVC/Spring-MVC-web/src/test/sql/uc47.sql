ALTER TABLE player_team47 DROP CONSTRAINT player_team47_player_fkc;

ALTER TABLE player47 DROP CONSTRAINT player47_position_fkc;

ALTER TABLE player_team47 DROP CONSTRAINT player_team47_team_fkc;

ALTER TABLE position47 DROP CONSTRAINT position47_pkey;

ALTER TABLE player47 DROP CONSTRAINT player47_pkey;

ALTER TABLE team47 DROP CONSTRAINT team47_pkey;

DROP TABLE team47;

DROP TABLE player47;

DROP TABLE player_team47;

DROP TABLE position47;

CREATE TABLE player47 (
    id bigint NOT NULL,
    version integer NOT NULL,
    first_name character varying(30) NOT NULL,
    last_name character varying(30) NOT NULL,
    date_of_birth date,
    estimated_value real,
    weight integer,
    height integer,
    rookie boolean,
    position_fk character varying(2) NOT NULL
);

CREATE TABLE position47 (
    code character varying(2) NOT NULL,
    version integer NOT NULL,
    name character varying(20) NOT NULL
);

CREATE TABLE team47 (
    id bigint NOT NULL,
    version integer NOT NULL,
    city character varying(1024),
    name character varying(1024)
);

CREATE TABLE player_team47 (
	id bigint NOT NULL,
	version integer NOT NULL,
	player_fk bigint NOT NULL,
	team_fk bigint NOT NULL
);



ALTER TABLE ONLY player47
    ADD CONSTRAINT player47_pkey PRIMARY KEY (id);
	
ALTER TABLE ONLY position47
    ADD CONSTRAINT position47_pkey PRIMARY KEY (code);
	
ALTER TABLE ONLY team47
    ADD CONSTRAINT team47_pkey PRIMARY KEY (id);
	
ALTER TABLE ONLY player47
    ADD CONSTRAINT player47_position_fkc FOREIGN KEY (position_fk) REFERENCES position47(code);

ALTER TABLE ONLY player_team47
    ADD CONSTRAINT player_team47_player_fkc FOREIGN KEY (player_fk) REFERENCES player47(id);

ALTER TABLE ONLY player_team47
    ADD CONSTRAINT player_team47_team_fkc FOREIGN KEY (team_fk) REFERENCES team47(id);	

INSERT INTO position47 VALUES ('PG', 0, 'Point Guard');
INSERT INTO position47 VALUES ('SG', 0, 'Shooting Guard');
INSERT INTO position47 VALUES ('SF', 0, 'Small Forward');
INSERT INTO position47 VALUES ('PF', 0, 'Power Forward');
INSERT INTO position47 VALUES ('C', 0, 'Center');
	
INSERT INTO player47 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, position_fk) VALUES (201, 0, 'Harry', 'Davis', '1988-04-14', 9.6899996, 139, 237, false, 'PF');
INSERT INTO player47 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, position_fk) VALUES (202, 0, 'Loren', 'Colon', '1971-12-14', 30.139999, 132, 221, false, 'SF');
INSERT INTO player47 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, position_fk) VALUES (203, 0, 'Bob', 'Clark', '1972-09-04', 24.02, 105, 224, false, 'SG');
INSERT INTO player47 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, position_fk) VALUES (205, 0, 'Loren', 'Stokes', '1976-07-17', 30.969999, 105, 186, false, 'SG');
INSERT INTO player47 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, position_fk) VALUES (207, 0, 'Shane', 'Sandoval', '1975-09-24', 24.139999, 131, 182, false, 'PF');
INSERT INTO player47 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, position_fk) VALUES (208, 0, 'Roosevelt', 'Davis', '1983-11-10', 8.5500002, 114, 235, false, 'C');
INSERT INTO player47 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, position_fk) VALUES (210, 0, 'Alton', 'Davis', '1970-02-13', 20.34, 120, 196, false, 'C');
INSERT INTO player47 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, position_fk) VALUES (211, 0, 'Marion', 'Favis', '1970-02-13', 20.34, 120, 196, false, 'C');
		
INSERT INTO team47 VALUES (1, 0, 'Charlotte', 'Tigers');
INSERT INTO team47 VALUES (2, 0, 'Kansas	 City', 'Bears');
INSERT INTO team47 VALUES (3, 0, 'Detroit', 'Chargers');
INSERT INTO team47 VALUES (4, 0, 'Detroit', 'Hawks');
INSERT INTO team47 VALUES (5, 0, 'Omaha', 'Braves');	

INSERT INTO player_team47 VALUES (1, 0, 201, 1);
INSERT INTO player_team47 VALUES (1, 0, 201, 2);
INSERT INTO player_team47 VALUES (1, 0, 202, 2);
INSERT INTO player_team47 VALUES (1, 0, 203, 1);
INSERT INTO player_team47 VALUES (1, 0, 203, 2);
INSERT INTO player_team47 VALUES (1, 0, 203, 3);
INSERT INTO player_team47 VALUES (1, 0, 203, 5);
INSERT INTO player_team47 VALUES (1, 0, 205, 1);
INSERT INTO player_team47 VALUES (1, 0, 207, 2);
INSERT INTO player_team47 VALUES (1, 0, 208, 3);
INSERT INTO player_team47 VALUES (1, 0, 208, 4);
INSERT INTO player_team47 VALUES (1, 0, 210, 2);
INSERT INTO player_team47 VALUES (1, 0, 210, 5);
INSERT INTO player_team47 VALUES (1, 0, 211, 1);
INSERT INTO player_team47 VALUES (1, 0, 211, 3);
INSERT INTO player_team47 VALUES (1, 0, 211, 4);