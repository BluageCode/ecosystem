<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file="/include.jsp"  %>
<html >
<head >
<title  > 
<fmt:message key="cUC59_UMLtoSQLmapping_BLU_AGE_DOCUMENTATION"/>
</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"  > 
</meta>
<link href="<c:url value= "/presentation/_style/_style.css"/>" rel="stylesheet" type="text/css"  /> 
<link href="<c:url value= "/presentation/_include/jquery/rd_lib.css"/>" rel="stylesheet" type="text/css"  /> 
<script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/jquery.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/rd_setup.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/rd_lib.js"/>" space="preserve"  ></script><link rel="shortcut icon" href="<c:url value= "/presentation/_style/favicon.ico"/>"  /> 

</head>

<body class=" " > 
<div id="rd_ghost"  > 
<small  > 
<fmt:message key="cUC59_UMLtoSQLmapping___CSS_DEACTIVATED"/>
</small>
</div>
<div id="rd_jghost"  > 
<small  > 
<fmt:message key="cUC59_UMLtoSQLmapping___JAVASCRIPT_DEACTIVATED"/>
</small>
</div>
<div id="shape"  > 
<div class="ui-layout-center"  > 
<div class="east-north"  > 
	<form:form id="uC59_UMLtoSQLmapping" commandName="uC59_UMLtoSQLmappingForm"  enctype="application/x-www-form-urlencoded" method="post">
<h2 class="rd_first"  > 
<fmt:message key="cUC59_UMLtoSQLmapping_STANDARD_FEATURES__ENTITIES_"/>
</h2>
<display:table uid="tab_players" pagesize="10"  sort="list" requestURI="" name="sessionScope.uC59_UMLtoSQLmappingForm.players" class="rd_tabledata"  >
	<c:set   var="rownum">
		<%=(tab_players !=	null) ? new Integer(tab_players_rowNum.intValue() - 1) : new Integer(0) %>
	</c:set>
	<c:set   var="idRow">
		<%=(tab_players != null ) ? tab_players_rowNum.intValue() - 1 : -1 %>
	</c:set>
	<c:set   var="idTab">
		<%=(tab_players != null ) ? "tab_players" : null %>
	</c:set>
	<c:set var="tableHashId">
		<%= new org.displaytag.util.ParamEncoder("tab_players").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE) %>
	</c:set>
	<c:set var="tableHashId2">
		<%=request.getParameter(new org.displaytag.util.ParamEncoder("tab_players").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE)) %>
	</c:set>	
	
<display:column format=""  titleKey="cUC59_UMLtoSQLmapping_NAME">
		<span id="dyn_firstName">
	<c:out value="${tab_players.firstName}" />
		</span>
	<fmt:message key="cUC59_UMLtoSQLmapping___________________"/>
		<span id="dyn_lastName">
	<c:out value="${tab_players.lastName}" />
		</span>
	<fmt:message key="cUC59_UMLtoSQLmapping____________________133"/>
		<span id="dyn_nickname">
	<c:out value="${tab_players.nickname}" />
		</span>
	<fmt:message key="cUC59_UMLtoSQLmapping__________________"/>
</display:column>
	
<display:column format=""  titleKey="cUC59_UMLtoSQLmapping_BIRTHDAY">
<span id="dyn_dateOfBirth">
	<format:formatDateValidator value="${tab_players.dateOfBirth}" pattern="MM/dd/yyyy" />
</span>
	</display:column>
	
<display:column format=""  titleKey="cUC59_UMLtoSQLmapping_POSITION">
		<span id="dyn_mainPosition">
	<c:out value="${tab_players.mainPosition.name}" />
		</span>
	<fmt:message key="cUC59_UMLtoSQLmapping____________________134"/>
		<span id="dyn_secondaryPosition">
	<c:out value="${tab_players.secondaryPosition.name}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="cUC59_UMLtoSQLmapping_VALUE">
		<span id="dyn_value">
	<c:out value="${tab_players.estimatedValue}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="cUC59_UMLtoSQLmapping_ROOKIE">
<form:checkbox id="chx_isRookie[${rownum}]" path="players[${rownum}].rookie"  disabled="true"  
		/>
</display:column>
</display:table>
<div style="visibility:hidden;"  > 
<a id="lien_cache"onclick="javascript:btn_click_cExecuteUC59_UMLtoSQLmapping(this.href);return false;"				href="<c:url value="/presentation/standardfeatures/entities/uc59_umltosqltypemapping/UC59_UMLtoSQLmapping/lien_cache.html"/>"
							 shape="rect"  >									 
<fmt:message key="cUC59_UMLtoSQLmapping_HOME"/>
</a>
</div>
	</form:form>
	<script language="javascript">
		function btn_click_cExecuteUC59_UMLtoSQLmapping(url) {
			UC59_UMLtoSQLmappingvar = document.getElementById('uC59_UMLtoSQLmapping');
			UC59_UMLtoSQLmappingvar.setAttribute('action', url);
			UC59_UMLtoSQLmappingvar.submit();
		}
		function btn_click_cSetActionUC59_UMLtoSQLmapping(url) {
			UC59_UMLtoSQLmappingvar = document.getElementById('uC59_UMLtoSQLmapping');
			UC59_UMLtoSQLmappingvar.setAttribute('action', url);
		}
	</script>
</div>
<div class="east-center"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/entities/uc59_umltosqltypemapping/UC59_UMLtoSQLmappingcmp_tabs" />
</div>
</div>
<div class="ui-layout-north"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/entities/uc59_umltosqltypemapping/UC59_UMLtoSQLmappingcmp_header" />
</div>
<div class="ui-layout-south"  > 
<fmt:message key="cUC59_UMLtoSQLmapping_______"/>
<span id="rd_jversion"  > 
<fmt:message key="cUC59_UMLtoSQLmapping_BLU_AGE_DOCUMENTATION_135"/>
</span>
</div>
<div class="ui-layout-west"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/entities/uc59_umltosqltypemapping/UC59_UMLtoSQLmappingcmp_treeview" />
</div>
</div>
</body>
</html>
