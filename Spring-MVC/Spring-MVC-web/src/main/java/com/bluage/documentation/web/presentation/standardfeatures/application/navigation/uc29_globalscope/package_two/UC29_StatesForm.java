/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.application.navigation.uc29_globalscope.package_two;


import java.io.Serializable;
import java.util.List;

import com.bluage.documentation.business.standardfeatures.application.navigation.uc29_globalscope.bos.State29BO;
import com.netfective.bluage.core.web.AbstractForm;
import com.netfective.bluage.core.web.IForm;

/**
* Class : UC29_StatesForm
*/
public class UC29_StatesForm extends AbstractForm implements Serializable, IForm {
	
	/** Constant : serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	//CONSTANT : allStates
	private static final String ALL_STATES = "allStates";
	/**
	 * 	Property: allStates 
	 */
	private List<State29BO> allStates;
/**
	 * Default constructor : UC29_StatesForm
	 */
	public UC29_StatesForm() {
		super();
		// Initialize : allStates
		this.allStates = new java.util.ArrayList<com.bluage.documentation.business.standardfeatures.application.navigation.uc29_globalscope.bos.State29BO>();
					this.setAlwaysCallPreControllers(true);
	}
	
	/**
	 * 	Getter : allStates 
	 * 	@return : Return the allStates instance.
	 */
	public List<State29BO> getAllStates(){
		return allStates; // For UC29_StatesForm
	}
	
	/**
	 * 	Setter : allStates 
	 *  @param allStatesinstance : The instance to set.
	 */
	public void setAllStates(final List<State29BO> allStatesinstance){
		this.allStates = allStatesinstance;// For UC29_StatesForm
	}
/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UC29_StatesForm [ "+
ALL_STATES +" = " + allStates + "]";
	}
	
	/**
	 * Method setInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public void setInstanceByInstanceName(final String instanceName, final Object instance) {
		// Set the instance AllStates.
		if(ALL_STATES.equals(instanceName)){// For UC29_StatesForm
			this.setAllStates((List<State29BO>)instance); // For UC29_StatesForm
		}
			}
	
	/**
	 * Method getInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public Object getInstanceByInstanceName(final String instanceName){
		// Temporary object for tmpUC29_StatesForm.
		Object tmpUC29_StatesForm = null;
		
			
		// Get the instance AllStates for UC29_StatesForm.
		if(ALL_STATES.equals(instanceName)){ // For UC29_StatesForm
			tmpUC29_StatesForm = this.getAllStates(); // For UC29_StatesForm
		}
		return tmpUC29_StatesForm;// For UC29_StatesForm
	}
	
			}
