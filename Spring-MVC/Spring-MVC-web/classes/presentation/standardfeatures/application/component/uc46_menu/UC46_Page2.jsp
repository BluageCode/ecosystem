<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file="/include.jsp"  %>
<html >
<head >
<title  > 
<fmt:message key="cUC46_Page2_BLU_AGE_DOCUMENTATION"/>
</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"  > 
</meta>
<link href="<c:url value= "/presentation/_style/_style.css"/>" rel="stylesheet" type="text/css"  /> 
<link href="<c:url value= "/presentation/_include/jquery/rd_lib.css"/>" rel="stylesheet" type="text/css"  /> 
<script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/jquery.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/rd_setup.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/rd_lib.js"/>" space="preserve"  ></script><link rel="shortcut icon" href="<c:url value= "/presentation/_style/favicon.ico"/>"  /> 

</head>

<body class=" " > 
<div id="rd_ghost"  > 
<small  > 
<fmt:message key="cUC46_Page2___CSS_DEACTIVATED"/>
</small>
</div>
<div id="rd_jghost"  > 
<small  > 
<fmt:message key="cUC46_Page2___JAVASCRIPT_DEACTIVATED"/>
</small>
</div>
<div id="shape"  > 
<div class="ui-layout-center"  > 
<div class="east-north"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/application/component/uc46_menu/UC46_Page2menuComponent" />
	<form:form id="uC46_Page2" commandName="uC46_Page2Form"  enctype="application/x-www-form-urlencoded" method="post">
<h4 class="rd_first"  > 
<fmt:message key="cUC46_Page2_PAGE_TWO"/>
</h4>
<p  > 
<fmt:message key="cUC46_Page2_YOU_ARE_HERE__PAGE_TWO"/>
</p>
<div style="visibility:hidden;"  > 
<a id="lien_cache"onclick="javascript:btn_click_cExecuteUC46_Page2(this.href);return false;"				href="<c:url value="/presentation/standardfeatures/application/component/uc46_menu/UC46_Page2/lien_cache.html"/>"
							 shape="rect"  >									 
<fmt:message key="cUC46_Page2_HOME"/>
</a>
</div>
	</form:form>
	<script language="javascript">
		function btn_click_cExecuteUC46_Page2(url) {
			UC46_Page2var = document.getElementById('uC46_Page2');
			UC46_Page2var.setAttribute('action', url);
			UC46_Page2var.submit();
		}
		function btn_click_cSetActionUC46_Page2(url) {
			UC46_Page2var = document.getElementById('uC46_Page2');
			UC46_Page2var.setAttribute('action', url);
		}
	</script>
</div>
<div class="east-center"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/application/component/uc46_menu/UC46_Page2cmp_tabs" />
</div>
</div>
<div class="ui-layout-north"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/application/component/uc46_menu/UC46_Page2cmp_header" />
</div>
<div class="ui-layout-south"  > 
<fmt:message key="cUC46_Page2________"/>
<span id="rd_jversion"  > 
<fmt:message key="cUC46_Page2_BLU_AGE_DOCUMENTATION_84"/>
</span>
</div>
<div class="ui-layout-west"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/application/component/uc46_menu/UC46_Page2cmp_treeview" />
</div>
</div>
</body>
</html>
