/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.application.component.uc47_contents ;


import org.apache.log4j.Logger;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;

import com.netfective.bluage.core.validator.AbstractValidator;
import com.netfective.bluage.core.validator.ValidatorsUtil;
/**
* Class :UC47_ContentsDetailsValidator
*/
public class UC47_ContentsDetailsValidator extends AbstractValidator{
	
	// LOGGER for the class UC47_ContentsDetailsValidator
	private static final Logger LOGGER = Logger.getLogger( UC47_ContentsDetailsValidator.class);
	
	
	/**
	* Operation validate for UC47_ContentsDetailsForm
	* @param obj : the current form (UC47_ContentsDetailsForm)
	* @param errors : The spring errors to return for the form UC47_ContentsDetailsForm
	*/
	public void validate(final Object obj,final Errors errors){
		LOGGER.info("Beginning method : validate"); // UC47_ContentsDetailsValidator
		UC47_ContentsDetailsForm cUC47_ContentsDetailsForm = (UC47_ContentsDetailsForm)obj; // UC47_ContentsDetailsValidator
				ValidationUtils.rejectIfEmpty(errors, "playerToUpdate.height", "", "Please enter a player height.");

		if(errors.getFieldError("playerToUpdate.height")== null){
						if(LOGGER.isDebugEnabled()){
				LOGGER.debug("validation of the 'playerToUpdate.height' field with the RangeValidator validator");
			}
			
			if(ValidatorsUtil.rangeValidator(cUC47_ContentsDetailsForm.getPlayerToUpdate().getHeight(), "150", "240")== 0 ){
				errors.rejectValue("playerToUpdate.height", "", "Player height should be between 150 and 240 cms.");
			} 
				
		}
				ValidationUtils.rejectIfEmpty(errors, "playerToUpdate.firstName", "", "First name is required.");

		if(errors.getFieldError("playerToUpdate.firstName")== null){
						if(LOGGER.isDebugEnabled()){
				LOGGER.debug("validation of the 'playerToUpdate.firstName' field with the LengthValidator validator");
			}
			
			if(ValidatorsUtil.lengthValidator(cUC47_ContentsDetailsForm.getPlayerToUpdate().getFirstName(), 1, 30) == 0){
				errors.rejectValue("playerToUpdate.firstName", "", "First name should be less than 30 characters long.");
			} 
				
		}
				ValidationUtils.rejectIfEmpty(errors, "playerToUpdate.lastName", "", "Last name is required.");

		if(errors.getFieldError("playerToUpdate.lastName")== null){
						if(LOGGER.isDebugEnabled()){
				LOGGER.debug("validation of the 'playerToUpdate.lastName' field with the LengthValidator validator");
			}
			
			if(ValidatorsUtil.lengthValidator(cUC47_ContentsDetailsForm.getPlayerToUpdate().getLastName(), 1, 30) == 0){
				errors.rejectValue("playerToUpdate.lastName", "", "Last name should be less than 30 characters long.");
			} 
				
		}
				ValidationUtils.rejectIfEmpty(errors, "playerToUpdate.weight", "", "Please enter a player weight.");

		if(errors.getFieldError("playerToUpdate.weight")== null){
						if(LOGGER.isDebugEnabled()){
				LOGGER.debug("validation of the 'playerToUpdate.weight' field with the RangeValidator validator");
			}
			
			if(ValidatorsUtil.rangeValidator(cUC47_ContentsDetailsForm.getPlayerToUpdate().getWeight(), "60", "140")== 0 ){
				errors.rejectValue("playerToUpdate.weight", "", "Player weight should be between 60 and 140 kgs.");
			} 
				
		}
		LOGGER.info("Ending method : validate the form "+ cUC47_ContentsDetailsForm.getClass().getName()); // UC47_ContentsDetailsValidator
	}

	/**
	* Method to implements to use spring validators (UC47_ContentsDetailsForm)
	* @param aClass : Class for the form UC47_ContentsDetailsForm
	* @return boolean
	*/
	public boolean supports(final Class aClass){
		return (new UC47_ContentsDetailsForm()).getClass().equals(aClass); // UC47_ContentsDetailsValidator
	}
}
