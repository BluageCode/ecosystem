--<ScriptOptions statementTerminator=";"/>

ALTER TABLE team46 DROP CONSTRAINT team46_pkey;

DROP TABLE team46;

CREATE TABLE team46 (
		id INT8 NOT NULL,
		version INT4 NOT NULL,
		city VARCHAR(1024),
		name VARCHAR(1024)
	);

ALTER TABLE team46 ADD CONSTRAINT team46_pkey PRIMARY KEY (id);

