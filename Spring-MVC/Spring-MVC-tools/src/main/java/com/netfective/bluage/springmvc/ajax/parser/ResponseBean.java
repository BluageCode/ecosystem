/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.netfective.bluage.springmvc.ajax.parser;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
        

/**
 * Class ResponseBean
 * @author NETFECTIVE TECHNOLOGY
 */
public class ResponseBean {

	/**
	* Html content of a bean
	*/
    private String htmlContent;
    
	/**
	* List of all scripts of a bean
	*/
    private List scriptContents = new ArrayList();
    
	/**
	* Set of all image links
	*/
    private Set images = new HashSet();

	/**
	* Getter for htmlContent
	* @return String
	*/
    public String getHtmlContent() {
        return htmlContent;
    }

	/**
	* Setter for htmlContent
	* @param htmlContent :
	*	String
	*/
    public void setHtmlContent(final String htmlContent) {
        this.htmlContent = htmlContent;
    }

	/**
	* Getter for scriptContents
	* @return List
	*/
    public List getScriptContents() {
        return scriptContents;
    }

	/**
	* setter for scriptContents
	* @param List
	*/
    public void setScriptContents(final List scriptContents) {
        this.scriptContents = scriptContents;
    }


    /**
    * Getter for images
     * @return List : unique list of image source paths
     */
    public Set getImages() {
        return images;
    }

	/**
     * setter for images
     * @param List : unique list of image source paths
     */
    public void setImages(final Set images) {
        this.images = images;
    }
}
