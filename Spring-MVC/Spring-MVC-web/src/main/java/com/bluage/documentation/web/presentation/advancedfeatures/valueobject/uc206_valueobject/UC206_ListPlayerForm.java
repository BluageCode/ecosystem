/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.advancedfeatures.valueobject.uc206_valueobject;


import java.io.Serializable;
import java.util.List;

import com.bluage.documentation.business.advancedfeatures.valueobject.uc206_valueobject.vo.Player206VO;
import com.bluage.documentation.business.advancedfeatures.valueobject.uc206_valueobject.vo.Screen206VO;
import com.netfective.bluage.core.web.AbstractForm;
import com.netfective.bluage.core.web.IForm;

/**
* Class : UC206_ListPlayerForm
*/
public class UC206_ListPlayerForm extends AbstractForm implements Serializable, IForm {
	
	/** Constant : serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	//CONSTANT : playerVOs
	private static final String PLAYER_V_OS = "playerVOs";
	//CONSTANT : screen206
	private static final String SCREEN206 = "screen206";
	//CONSTANT : selectedPlayer
	private static final String SELECTED_PLAYER = "selectedPlayer";
	/**
	 * 	Property: playerVOs 
	 */
	private List<Player206VO> playerVOs;
	/**
	 * 	Property: screen206 
	 */
	private Screen206VO screen206;
	/**
	 * 	Property: selectedPlayer 
	 */
	private Player206VO selectedPlayer;
/**
	 * Default constructor : UC206_ListPlayerForm
	 */
	public UC206_ListPlayerForm() {
		super();
		// Initialize : playerVOs
		this.playerVOs = new java.util.ArrayList<com.bluage.documentation.business.advancedfeatures.valueobject.uc206_valueobject.vo.Player206VO>();
		// Initialize : screen206
		this.screen206 = null;
		// Initialize : selectedPlayer
		this.selectedPlayer = null;
					this.setAlwaysCallPreControllers(true);
	}
	
	/**
	 * 	Getter : playerVOs 
	 * 	@return : Return the playerVOs instance.
	 */
	public List<Player206VO> getPlayerVOs(){
		return playerVOs; // For UC206_ListPlayerForm
	}
	
	/**
	 * 	Setter : playerVOs 
	 *  @param playerVOsinstance : The instance to set.
	 */
	public void setPlayerVOs(final List<Player206VO> playerVOsinstance){
		this.playerVOs = playerVOsinstance;// For UC206_ListPlayerForm
	}
	/**
	 * 	Getter : screen206 
	 * 	@return : Return the screen206 instance.
	 */
	public Screen206VO getScreen206(){
		return screen206; // For UC206_ListPlayerForm
	}
	
	/**
	 * 	Setter : screen206 
	 *  @param screen206instance : The instance to set.
	 */
	public void setScreen206(final Screen206VO screen206instance){
		this.screen206 = screen206instance;// For UC206_ListPlayerForm
	}
	/**
	 * 	Getter : selectedPlayer 
	 * 	@return : Return the selectedPlayer instance.
	 */
	public Player206VO getSelectedPlayer(){
		return selectedPlayer; // For UC206_ListPlayerForm
	}
	
	/**
	 * 	Setter : selectedPlayer 
	 *  @param selectedPlayerinstance : The instance to set.
	 */
	public void setSelectedPlayer(final Player206VO selectedPlayerinstance){
		this.selectedPlayer = selectedPlayerinstance;// For UC206_ListPlayerForm
	}
/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UC206_ListPlayerForm [ "+
PLAYER_V_OS +" = " + playerVOs +SCREEN206 +" = " + screen206 +SELECTED_PLAYER +" = " + selectedPlayer + "]";
	}
	
	/**
	 * Method setInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public void setInstanceByInstanceName(final String instanceName, final Object instance) {
		// Set the instance PlayerVOs.
		if(PLAYER_V_OS.equals(instanceName)){// For UC206_ListPlayerForm
			this.setPlayerVOs((List<Player206VO>)instance); // For UC206_ListPlayerForm
		}
				// Set the instance Screen206.
		if(SCREEN206.equals(instanceName)){// For UC206_ListPlayerForm
			this.setScreen206((Screen206VO)instance); // For UC206_ListPlayerForm
		}
				// Set the instance SelectedPlayer.
		if(SELECTED_PLAYER.equals(instanceName)){// For UC206_ListPlayerForm
			this.setSelectedPlayer((Player206VO)instance); // For UC206_ListPlayerForm
		}
			}
	
	/**
	 * Method getInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public Object getInstanceByInstanceName(final String instanceName){
		// Temporary object for tmpUC206_ListPlayerForm.
		Object tmpUC206_ListPlayerForm = null;
		
			
		// Get the instance PlayerVOs for UC206_ListPlayerForm.
		if(PLAYER_V_OS.equals(instanceName)){ // For UC206_ListPlayerForm
			tmpUC206_ListPlayerForm = this.getPlayerVOs(); // For UC206_ListPlayerForm
		}
			
		// Get the instance Screen206 for UC206_ListPlayerForm.
		if(SCREEN206.equals(instanceName)){ // For UC206_ListPlayerForm
			tmpUC206_ListPlayerForm = this.getScreen206(); // For UC206_ListPlayerForm
		}
			
		// Get the instance SelectedPlayer for UC206_ListPlayerForm.
		if(SELECTED_PLAYER.equals(instanceName)){ // For UC206_ListPlayerForm
			tmpUC206_ListPlayerForm = this.getSelectedPlayer(); // For UC206_ListPlayerForm
		}
		return tmpUC206_ListPlayerForm;// For UC206_ListPlayerForm
	}
	
			}
