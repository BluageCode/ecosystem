/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.exception.uc73_exception ;


import org.apache.log4j.Logger;
import org.springframework.validation.Errors;

import com.netfective.bluage.core.validator.AbstractValidator;
/**
* Class :UC73_ManagePlayersValidator
*/
public class UC73_ManagePlayersValidator extends AbstractValidator{
	
	// LOGGER for the class UC73_ManagePlayersValidator
	private static final Logger LOGGER = Logger.getLogger( UC73_ManagePlayersValidator.class);
	
	
	/**
	* Operation validate for UC73_ManagePlayersForm
	* @param obj : the current form (UC73_ManagePlayersForm)
	* @param errors : The spring errors to return for the form UC73_ManagePlayersForm
	*/
	public void validate(final Object obj,final Errors errors){
		LOGGER.info("Beginning method : validate"); // UC73_ManagePlayersValidator
		UC73_ManagePlayersForm cUC73_ManagePlayersForm = (UC73_ManagePlayersForm)obj; // UC73_ManagePlayersValidator
		LOGGER.info("Ending method : validate the form "+ cUC73_ManagePlayersForm.getClass().getName()); // UC73_ManagePlayersValidator
	}

	/**
	* Method to implements to use spring validators (UC73_ManagePlayersForm)
	* @param aClass : Class for the form UC73_ManagePlayersForm
	* @return boolean
	*/
	public boolean supports(final Class aClass){
		return (new UC73_ManagePlayersForm()).getClass().equals(aClass); // UC73_ManagePlayersValidator
	}
}
