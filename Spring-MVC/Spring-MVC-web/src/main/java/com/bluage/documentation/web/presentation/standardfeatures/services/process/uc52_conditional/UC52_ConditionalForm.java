/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.services.process.uc52_conditional;


import java.io.Serializable;
import java.util.List;

import com.bluage.documentation.business.standardfeatures.services.process.uc52_conditional.bos.Player52BO;
import com.netfective.bluage.core.web.AbstractForm;
import com.netfective.bluage.core.web.IForm;

/**
* Class : UC52_ConditionalForm
*/
public class UC52_ConditionalForm extends AbstractForm implements Serializable, IForm {
	
	/** Constant : serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	//CONSTANT : newPlayer
	private static final String NEW_PLAYER = "newPlayer";
	//CONSTANT : players
	private static final String PLAYERS = "players";
	/**
	 * 	Property: newPlayer 
	 */
	private Player52BO newPlayer;
	/**
	 * 	Property: players 
	 */
	private List<Player52BO> players;
/**
	 * Default constructor : UC52_ConditionalForm
	 */
	public UC52_ConditionalForm() {
		super();
		// Initialize : newPlayer
		this.newPlayer = new Player52BO();
		// Initialize : players
		this.players = new java.util.ArrayList<com.bluage.documentation.business.standardfeatures.services.process.uc52_conditional.bos.Player52BO>();
					this.setAlwaysCallPreControllers(true);
	}
	
	/**
	 * 	Getter : newPlayer 
	 * 	@return : Return the newPlayer instance.
	 */
	public Player52BO getNewPlayer(){
		return newPlayer; // For UC52_ConditionalForm
	}
	
	/**
	 * 	Setter : newPlayer 
	 *  @param newPlayerinstance : The instance to set.
	 */
	public void setNewPlayer(final Player52BO newPlayerinstance){
		this.newPlayer = newPlayerinstance;// For UC52_ConditionalForm
	}
	/**
	 * 	Getter : players 
	 * 	@return : Return the players instance.
	 */
	public List<Player52BO> getPlayers(){
		return players; // For UC52_ConditionalForm
	}
	
	/**
	 * 	Setter : players 
	 *  @param playersinstance : The instance to set.
	 */
	public void setPlayers(final List<Player52BO> playersinstance){
		this.players = playersinstance;// For UC52_ConditionalForm
	}
/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UC52_ConditionalForm [ "+
NEW_PLAYER +" = " + newPlayer +PLAYERS +" = " + players + "]";
	}
	
	/**
	 * Method setInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public void setInstanceByInstanceName(final String instanceName, final Object instance) {
		// Set the instance NewPlayer.
		if(NEW_PLAYER.equals(instanceName)){// For UC52_ConditionalForm
			this.setNewPlayer((Player52BO)instance); // For UC52_ConditionalForm
		}
				// Set the instance Players.
		if(PLAYERS.equals(instanceName)){// For UC52_ConditionalForm
			this.setPlayers((List<Player52BO>)instance); // For UC52_ConditionalForm
		}
			}
	
	/**
	 * Method getInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public Object getInstanceByInstanceName(final String instanceName){
		// Temporary object for tmpUC52_ConditionalForm.
		Object tmpUC52_ConditionalForm = null;
		
			
		// Get the instance NewPlayer for UC52_ConditionalForm.
		if(NEW_PLAYER.equals(instanceName)){ // For UC52_ConditionalForm
			tmpUC52_ConditionalForm = this.getNewPlayer(); // For UC52_ConditionalForm
		}
			
		// Get the instance Players for UC52_ConditionalForm.
		if(PLAYERS.equals(instanceName)){ // For UC52_ConditionalForm
			tmpUC52_ConditionalForm = this.getPlayers(); // For UC52_ConditionalForm
		}
		return tmpUC52_ConditionalForm;// For UC52_ConditionalForm
	}
	
			}
