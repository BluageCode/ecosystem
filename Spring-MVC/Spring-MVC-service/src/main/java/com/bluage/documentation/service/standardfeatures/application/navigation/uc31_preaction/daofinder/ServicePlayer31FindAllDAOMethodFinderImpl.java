/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.service.standardfeatures.application.navigation.uc31_preaction.daofinder;


import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateTemplate;

import com.netfective.bluage.core.exception.ApplicationException;


/**
 * This class represents query invocation methods implementation
 * for ServicePlayer31FindAll service.
 *
 * @see com.bluage.documentation.service.standardfeatures.application.navigation.uc31_preaction.ServicePlayer31FindAll
 */
 
@Named ("com.bluage.documentation.service.standardfeatures.application.navigation.uc31_preaction.daofinder.ServicePlayer31FindAllDAOMethodFinderImpl")
public class ServicePlayer31FindAllDAOMethodFinderImpl {

	// LOGGER for ServicePlayer31FindAllDAOMethodFinderImpl
	private static final Logger LOGGER = Logger.getLogger(ServicePlayer31FindAllDAOMethodFinderImpl.class);
	
	/**
	* Hibernate Template (injected)
	*/
	@Inject
	private HibernateTemplate hibernateTemplate;

	
	/**
	 * setter for HibernateTemplate
	 * @param value : {@link HibernateTemplate}
	 */
	public void setHibernateTemplate( HibernateTemplate value )
	{
		hibernateTemplate = value;
	}
	
	
	/**    
	 * HQL operation.
	 * @return {@link List}
	 */
	public List player31FindAll( )
 throws ApplicationException {
Session session = hibernateTemplate.getSessionFactory().getCurrentSession();
		return 
				privatePlayer31FindAll(session);
	}


	/**    
	 * private delegation for HQL operation.
	 * @return {@link List}
	 */
	private List privatePlayer31FindAll( Session session)
 throws ApplicationException {
			Query query = session.createQuery(" SELECT p FROM Player31BO p JOIN FETCH p.position pos ");
			List returnValue = null;
		try {
			returnValue = query.list();
							} catch(HibernateException ex) {
			LOGGER.error("An error occured while excecuting the HQL operation : com.bluage.documentation.service.standardfeatures.application.navigation.uc31_preaction.player31FindAll");
			throw new ApplicationException(ex);
		}
		return returnValue;
	}

	




}
