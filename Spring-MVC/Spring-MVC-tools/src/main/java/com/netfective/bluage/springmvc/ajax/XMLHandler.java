/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.netfective.bluage.springmvc.ajax;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.netfective.bluage.springmvc.ajax.parser.ResponseBean;
import com.netfective.bluage.springmvc.ajax.parser.ResponseParser;
import com.netfective.bluage.springmvc.ajax.parser.ResponseParserFactory;
import com.netfective.bluage.springmvc.ajax.tag.TagUtils;
import com.netfective.bluage.springmvc.ajax.util.AjaxUtils;
import com.netfective.bluage.springmvc.ajax.util.Constants;



/**
 * Class XMLHandler
 * @author NETFECTIVE TECHNOLOGY
 */
public final class XMLHandler {

    /**
     * LOGGER.
     */
    private static final  Logger LOGGER = Logger.getLogger(XMLHandler.class);
    
    /**
    * Default builder
    */
    private XMLHandler(){}

	/**
	* 
	* @param BufferResponseWrapper :
	*			bufferReponseWrapper
	* @param Set :
	*			refreshZones
	* @throw TransformerException
	* @throw IOException
	*/
	public static void sendZones(final BufferResponseWrapper bufferResponseWrapper,
			final Set refreshZones) throws TransformerException, IOException {
		
		final Document doc = newDocument();
		final Element root = addRootElement(doc, Constants.XML_ZONES);

		final List scripts = new ArrayList();
		final Set images = new HashSet();

		for (final Iterator iterator = refreshZones.iterator(); iterator.hasNext();) {
			final String zone = (String) iterator.next();
			final String content = AjaxUtils.getZoneContent(zone, bufferResponseWrapper);

			// if zone added to refresh list but not present in content, then
			// exclude zone info in response
			if (content == null) {
				continue;
			}
			final Element zoneNode = doc.createElement(Constants.XML_ZONE);
			zoneNode.setAttribute(Constants.XML_NAME, TagUtils.getRealDivId(zone));

			handleZoneContent(content, zoneNode, doc, scripts, images, root);
		}

		for (final Iterator<String> iterator = scripts.iterator(); iterator.hasNext();) {
			String script = (String) iterator.next();
			final int posScriptEnd = script.indexOf('>');
			if (posScriptEnd != -1){
				script = script.substring(posScriptEnd + 1);
			}

			final Element scriptNode = doc.createElement(Constants.XML_SCRIPT);
			appendText(scriptNode, doc, script);
			root.appendChild(scriptNode);
		}

		for (final Iterator it = images.iterator(); it.hasNext();) {
			final String image = (String) it.next();
			final Element imageNode = doc.createElement(Constants.XML_IMAGE);
			appendText(imageNode, doc, image);
			root.appendChild(imageNode);
		}

		sendDOMDocument(bufferResponseWrapper.getOriginalResponse(), doc);
	}

	/**
	* Regroup all images and scripts. Append zoneNode at root
	* @param String :
	*			content
	* @param Element :
	*			zoneNode
	* @param Document :
	*			doc
	* @param List :
	*			scripts
	* @param Set :
	*			Images
	* @param Element :
	*			root
	*/
	private static void handleZoneContent(final String content, final Element zoneNode,
			final Document doc, final List scripts, final Set images, final Element root) {
		final ResponseParser parser = ResponseParserFactory.getInstance().getParser();
		final ResponseBean result = parser.parse(content);

		appendText(zoneNode, doc, result.getHtmlContent());
		final List scriptContents = result.getScriptContents();
		if (scriptContents != null && !scriptContents.isEmpty()){
			scripts.addAll(scriptContents);
		}
		images.addAll(result.getImages());
		root.appendChild(zoneNode);
	}

	/**
	* Add root element to doc
	*
	* @param Document :
	*			doc
	* @param String :
	*			rootTagName
	* @return Element : new root
	*/
	private static Element addRootElement(final Document doc, final String rootTagName) {
		final Element root = doc.createElement(rootTagName);
		doc.appendChild(root);
		return root;
	}

	/**
	* Send document in a <code>TransformerFactory</code> instance
	* @param HttpServletResponse :
	* 			originalResponse
	* @param Document :
	*			doc
	* @throws : TransformerException, IOException
	*/
	private static void sendDOMDocument(final HttpServletResponse originalResponse,
			final Document doc) throws TransformerException, IOException {

		final TransformerFactory transformerFactory = TransformerFactory.newInstance();		
		
		final Transformer transformer = transformerFactory.newTransformer();

		transformer.transform(new DOMSource(doc), new StreamResult(originalResponse.getOutputStream()));

		originalResponse.flushBuffer();
	}

	/**
	* Create a new document
	* @return Document
	* @throw ParserConfigurationException
	*/
	private static Document newDocument() {
		try {
			return DocumentBuilderFactory.newInstance().newDocumentBuilder()
					.newDocument();
		} catch (ParserConfigurationException e) {	
            LOGGER.error(e.toString(), e); // this should never happen
		}
		return null;
	}

	/**
	* Append text on <code>zoneNode</code>
	* @param Element :
	*			zoneNode
	* @param Document :
	*			doc
	* @param String :
	*			content
	* @throw ParserConfigurationException
	*/
	private static void appendText(final Element zoneNode, final Document doc,
			final String content) {
		zoneNode.appendChild(doc.createCDATASection(content.replaceAll("\r", "")));
	}

	/**
	* handleError
	* @param HttpServletResponse :
	*			response
	* @param Throwable :
	*			exception
	*/
	public static void handleError(final HttpServletResponse response, final Throwable exception){
		final Document doc = newDocument();

		final Element root = addRootElement(doc, Constants.XML_EXCEPTION);
		root.setAttribute(Constants.XML_TYPE, exception.getClass()
				.getName());
		final StringWriter sw = new StringWriter();
		exception.printStackTrace(new PrintWriter(sw));
		appendText(root, doc, sw.toString());

		try {
			sendDOMDocument(response, doc);
		} catch (TransformerException e) {
			 LOGGER.error(e.getMessage(), e);
		} catch (IOException e) {
			 LOGGER.error(e.getMessage() , e);
		}

	}

	/**
	* sendRedirect
	* @param BufferResponseWrapper :
	*			bufferResponseWrapper
	* @throw TransformerException, IOException
	*/
	public static void sendRedirect(final BufferResponseWrapper bufferResponseWrapper) throws TransformerException, IOException {
		final Document doc = newDocument();
		final Element root = addRootElement(doc, Constants.XML_REDIRECT);
		final String redirect = bufferResponseWrapper.getRedirect();
		appendText(root, doc, redirect);

		sendDOMDocument(bufferResponseWrapper.getOriginalResponse(), doc);
	}
}

