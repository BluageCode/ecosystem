package uc1002;


import org.junit.Test;

import test.common.AbstractBADocTestCase;


public class ITuc1002 extends AbstractBADocTestCase {

	@Override
	public  String getName(){
		return "UC-1002";
	}

	@Test
	public void testUC1002() throws Exception {
		// On teste l'UC03 - Getting started - Form Validation
		// On teste l'UC afin de v�rifier la pr�sence des diff�rents messages d'erreur pour les champs "Height"(150/240) et "Weight"(60/140).
		// Pour les valeurs en de�� des valeurs minimum.

		
		selenium.open("");	
		selenium.click("link=Expand All");
		selenium.waitForPageToLoad("300000");
		selenium.click("link=UC1002 - Validators");
		
		verifyTrue(selenium.isElementPresent("//h2[contains(text(),\"QA UC > UC1002 > Validators\")]"));
		// On entre des suites de caract�res valides dans les champs "First Name", "Last Name" et "Date of birth".
		selenium.type("//*[contains(@id,'txt_firstname')]", "Jean");
		selenium.type("//*[contains(@id,'txt_lastname')]", "Pierre");
		selenium.type("//*[contains(@id,'txt_dateofbirth')]", "12/14/1980");
		// On entre des valeurs en de�� des valeurs minimum accept�s pour les champs "EstimatedValue","Salary" "Height"(150/240) et "Weight"(60/140).
		selenium.type("//*[contains(@id,'txt_estimatedvalue')]", "aaa");
		selenium.click("//*[contains(@id,'lnk_create')]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie que l'ID du message d'erreur est pr�sente.
		assertTrue(selenium.isElementPresent("//*[contains(@id,'estimatedvalue_format')]"));
		// On v�rifie que l'ID du message d'erreur existe.
		assertEquals("The Estimated Value should be a Number.", selenium.getText("//*[contains(@id,'estimatedvalue_format')]"));
		selenium.type("//*[contains(@id,'txt_estimatedvalue')]", "101");
		selenium.type("//*[contains(@id,'txt_salary')]", "aaa");
		selenium.type("//*[contains(@id,'txt_weight')]", "fff");
		selenium.type("//*[contains(@id,'txt_height')]", "ddd");
		selenium.click("//*[contains(@id,'lnk_create')]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie que l'ID du message d'erreur est pr�sente.
		assertTrue(selenium.isElementPresent("//*[contains(@id,'playerToCreate.salary.errors')]"));
		// On v�rifie que l'ID du message d'erreur existe.
		assertEquals("Failed to convert property value of type java.lang.String to required type java.lang.Double for property playerToCreate.salary; nested exception is org.springframework.core.convert.ConversionFailedException: Unable to convert value \"aaa\" from type java.lang.String to type java.lang.Double; nested exception is java.lang.NumberFormatException: For input string: \"aaa\"", selenium.getText("//*[contains(@id,'playerToCreate.salary.errors')]"));
		// On v�rifie que l'ID du message d'erreur est pr�sente.
		assertTrue(selenium.isElementPresent("//*[contains(@id,'playerToCreate.weight.errors')]"));
		// On v�rifie que l'ID du message d'erreur existe.
		assertEquals("Failed to convert property value of type java.lang.String to required type java.lang.Integer for property playerToCreate.weight; nested exception is org.springframework.core.convert.ConversionFailedException: Unable to convert value \"fff\" from type java.lang.String to type java.lang.Integer; nested exception is java.lang.NumberFormatException: For input string: \"fff\"", selenium.getText("//*[contains(@id,'playerToCreate.weight.errors')]"));
		assertTrue(selenium.isElementPresent("//*[contains(@id,'playerToCreate.height.errors')]"));
		// On v�rifie que l'ID du message d'erreur existe.
		assertEquals("Failed to convert property value of type java.lang.String to required type java.lang.Integer for property playerToCreate.height; nested exception is org.springframework.core.convert.ConversionFailedException: Unable to convert value \"ddd\" from type java.lang.String to type java.lang.Integer; nested exception is java.lang.NumberFormatException: For input string: \"ddd\"", selenium.getText("//*[contains(@id,'playerToCreate.height.errors')]"));
		// Pour les valeurs sup�rieures aux valeurs maximum.
		selenium.type("//*[contains(@id,'txt_salary')]", "8700");
		selenium.type("//*[contains(@id,'txt_weight')]", "59");
		selenium.type("//*[contains(@id,'txt_height')]", "149");
		selenium.click("//*[contains(@id,'lnk_create')]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//*[contains(@id,'playerToCreate.salary.errors')]"));
		// On v�rifie que l'ID du message d'erreur existe.
		assertEquals("The salary should be between 0 and 8000$", selenium.getText("//*[contains(@id,'playerToCreate.salary.errors')]"));
		assertTrue(selenium.isElementPresent("//*[contains(@id,'playerToCreate.weight.errors')]"));
		// On v�rifie que l'ID du message d'erreur existe.
		assertEquals("Player weight should be between 60 and 140 kgs.", selenium.getText("//*[contains(@id,'playerToCreate.weight.errors')]"));
		// On v�rifie que l'ID du message d'erreur est pr�sente.
		assertTrue(selenium.isElementPresent("//*[contains(@id,'playerToCreate.height.errors')]"));
		// On v�rifie que l'ID du message d'erreur existe.
		assertEquals("Player height should be between 150 and 240 cms.", selenium.getText("//*[contains(@id,'playerToCreate.height.errors')]"));
		// On entre des suites de caract�res valides dans les champs "First Name", "Last Name" et "Date of birth".
		selenium.type("//*[contains(@id,'txt_firstname')]", "Jean");
		selenium.type("//*[contains(@id,'txt_lastname')]", "Pierre");
		selenium.type("//*[contains(@id,'txt_dateofbirth')]", "12/14/1980");
		// On entre des valeurs supp�rieures aux valeurs maximum accept�es pour les champs "Height"(150/240) et "Weight"(60/140).
		selenium.type("//*[contains(@id,'txt_weight')]", "141");
		selenium.type("//*[contains(@id,'txt_height')]", "241");
		selenium.click("//*[contains(@id,'lnk_create')]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie que l'ID du message d'erreur est pr�sente.
		assertTrue(selenium.isElementPresent("//*[contains(@id,'playerToCreate.weight.errors')]"));
		// On v�rifie que l'ID du message d'erreur existe.
		assertEquals("Player weight should be between 60 and 140 kgs.", selenium.getText("//*[contains(@id,'playerToCreate.weight.errors')]"));
		// On v�rifie que l'ID du message d'erreur est pr�sente.
		assertTrue(selenium.isElementPresent("//*[contains(@id,'playerToCreate.height.errors')]"));
		// On entre des suites de caract�res valides dans les champs "First Name", "Last Name" et "Date of birth".
		assertEquals("Player height should be between 150 and 240 cms.", selenium.getText("//*[contains(@id,'playerToCreate.height.errors')]"));
		selenium.type("//*[contains(@id,'txt_salary')]", "3000");
		selenium.type("//*[contains(@id,'txt_weight')]", "140");
		selenium.type("//*[contains(@id,'txt_height')]", "240");
		selenium.click("//*[contains(@id,'lnk_create')]");
		selenium.waitForPageToLoad("30000");
	}


}
