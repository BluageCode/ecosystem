/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.netfective.bluage.springmvc.ajax.util;


import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;

import org.apache.commons.beanutils.PropertyUtils;

/**
 * Class RequestUtils
 * @author NETFECTIVE TECHNOLOGY
 */
public final class RequestUtils {

	/**
	 * Maps lowercase JSP scope names to their PageContext integer constant
	 * values.
	 */
	private static Map scopes = new HashMap();

    /**
    * Default builder
    */
    private RequestUtils() {}

	/**
	 * set up the scope map values.
	 */
	static {
		scopes.put("page", Integer.valueOf(PageContext.PAGE_SCOPE));
		scopes.put("request", Integer.valueOf(PageContext.REQUEST_SCOPE));
		scopes.put("session", Integer.valueOf(PageContext.SESSION_SCOPE));
		scopes.put("application", Integer.valueOf(PageContext.APPLICATION_SCOPE));
	}

	/**
	* Return a property value of a bean named <code>name</code>
	 * @param PageContext :
	 *			pageContext
	 * @param String :
	 *			name
	 * @param String :
	 *			property
	 * @param String :
	 *			scope
	 * @return Object : the property value
	 * @throws JspException
	 */
	public static Object lookup(final PageContext pageContext, final String name,
			final String property, final String scope) throws JspException {

		// Look up the requested bean, and return if requested
		final Object bean = lookup(pageContext, name, scope);
		if (bean == null) {
			if (scope == null) {
				throw new JspException("Cannot find bean " + name
						+ " in any scope");
			} else {
				throw new JspException("Cannot find bean " + name
						+ " in scope " + scope);
			}
		}

		if (property == null) {
			return bean;
		}

		// Locate and return the specified property
		try {
			return PropertyUtils.getProperty(bean, property);
		} catch (IllegalAccessException e) {
			throw new JspException("Invalid access looking up property "
					+ property + " of bean " + name,e);
		} catch (InvocationTargetException e) {
			throw new JspException("Exception thrown by getter for property "
					+ property + "of bean " + name,e);
		} catch (NoSuchMethodException e) {
			throw new JspException("No getter method for property " + property
					+ " of bean " + name,e);
		}
	}

	/**
	 * Locate and return the specified bean, from an optionally specified scope,
	 * in the specified page context. If no such bean is found, return
	 * <code>null</code> instead.
	 * 
	 * @param PageContext :
	 *			pageContext : Page context to be searched
	 * @param String :
	 *			name : Name of the bean to be retrieved
	 * @param String :
	 *			scopeName : Scope to be searched (page, request, session, application) or
	 *            <code>null</code> to use <code>findAttribute()</code>
	 *            instead
	 * @return Object : JavaBean in the specified page context
	 * @exception JspException
	 *                if an invalid scope name is requested
	 */
	public static Object lookup(final PageContext pageContext, final String name,
			final String scopeName) throws JspException {

		if (scopeName == null) {
			return pageContext.findAttribute(name);
		}

		return pageContext.getAttribute(name, getScope(scopeName));
	}
	
	 /**
     * Converts the scope name into its corresponding PageContext constant value.
     * @param String :
     *			scopeName : Can be "page", "request", "session", or "application" in any
     * case.
     * @return int : The constant representing the scope (ie. PageContext.REQUEST_SCOPE).
     * @throws JspException if the scopeName is not a valid name.
     * @since Struts 1.1
     */
    public static int getScope(final String scopeName) throws JspException {
		final Integer scope = (Integer) scopes.get(scopeName.toLowerCase());

		if (scope == null) {
			throw new JspException("Invalid bean scope " + scopeName);
		}

		return scope.intValue();
    }
	
	/**
	 * Build url using encodeURL and <code>action</code>.
	 *  Add context path to the <b>action</b>
	 * @param PageContext :
	 *			pageContext
	 * @param String :
	 *			action
	 * @return String
	 */
	public static String buildUrl(final PageContext pageContext, final String action) {
		final HttpServletRequest request = (HttpServletRequest) pageContext
				.getRequest();
		return ((HttpServletResponse) pageContext.getResponse())
				.encodeURL(request.getContextPath() + action);

	}
	
	/**
	 * Build url using encodeURL and <code>action</code>.
	 *  Add context path and post data  to the <b>action</b>
	 * @param PageContext :
	 *			pageContext
	 * @param String :
	 *			action
	 * @param Map :
	 *			parameters
	 * @return String
	 */
	public static String buildUrlWithPostData(final PageContext pageContext,
			final String action,final  Map parameters) {
		final String url = buildUrl(pageContext, action);
		return url + getPostData(parameters);
	}

	
	/**
	 * Extract the query string from <b>parameters</b>
	 * @param Map :
	 *			parameters
	 * @return String
	 */
	public static String getPostData(final Map parameters) {

		final StringBuffer postdata = new StringBuffer("");
		if (parameters != null) {

			final Set keys = parameters.keySet();
			final Iterator iterator = keys.iterator();
			while (iterator.hasNext()) {
				final String key = (String) iterator.next();
				final String value = (String) parameters.get(key);

				if (key == null || value == null || "".equals(key)
						|| "".equals(value)){
					continue;
				}

				if ("".equals(postdata.toString())){
					postdata.append("?").append(key).append("=").append(value);
				} else {
					postdata.append("&").append(key).append("=").append(value);
				}
			}
		}
		return postdata.toString();
	}

}
