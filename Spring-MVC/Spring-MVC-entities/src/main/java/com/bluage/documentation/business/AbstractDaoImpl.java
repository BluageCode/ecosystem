/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package   com.bluage.documentation.business ;
 
//Import declaration 
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.bluage.documentation.business.exceptions.NonNullConstraintViolationException;
import com.bluage.documentation.business.exceptions.UniqueConstraintViolationException;
import com.netfective.bluage.core.business.IBusinessObject;
import com.netfective.bluage.core.exception.ApplicationException;
import com.netfective.bluage.core.exception.DBConstraintViolationException;
import com.netfective.bluage.core.exception.DBNullColumnNotAllowedException;
import com.netfective.bluage.core.exception.UnexpectedException;
import com.netfective.bluage.core.persistence.IPersistentObjectDAO;


/**
* Class :AbstractDaoImpl<T>
*/
public abstract class AbstractDaoImpl<T> extends HibernateDaoSupport implements IBusinessStringConstants,IPersistentObjectDAO<T>{
	
	/**
	* Property:LOGGER
	*/
	private static final Logger LOGGER = Logger.getLogger(AbstractDaoImpl.class);


	/**
	* Property:clearSession
	*/
	private boolean clearSession;


	/**
	* Property:refreshSession
	*/
	private boolean refreshSession;

	/**
	* Property:flushSession
	*/
	private boolean flushSession;

	/**
	* This method Clear Session 
	* @param value boolean
	*/
	public void setClearSession(final boolean value){

		this.clearSession = value;

	}


	/**
	* This method Clear Session 
	* @return boolean
	*/
	public boolean isClearSession(){

		return this.clearSession;
	}


	/**
	* This method Refresh Session 
	* @param value boolean
	*/
	public void setRefreshSession(final boolean value){

		this.refreshSession = value;

	}


	/**
	* This method Refresh Session 
	* @return boolean
	*/
	public boolean isRefreshSession(){

		return this.refreshSession;
	}


	/**
	* This method 
	* @param bo IBusinessObject
	* @return boolean
	* @throws ApplicationException exception

	*/
	public boolean create(final IBusinessObject bo)
		throws ApplicationException{

		this.logMethod(L_CREATE_METHOD,L_BEGIN_METHOD,bo);
		try{
			if(isClearSession()){
				this.flushThenClear();
				this.getHibernateTemplate().save(bo);

			}else{
				if(isRefreshSession()){
					this.getHibernateTemplate().save(bo);
					this.flushThenRefresh(bo);

				}else{
					this.getHibernateTemplate().save(bo);

				}

			}

		}
		catch (UniqueConstraintViolationException ex) {
			logError(L_CREATE_METHOD,ex);
			throw new DBConstraintViolationException(ex);
		}

		catch (NonNullConstraintViolationException ex) {
			logError(L_CREATE_METHOD,ex);
			throw new DBNullColumnNotAllowedException(ex);
		}

		catch (DataAccessException ex) {
			logError(L_CREATE_METHOD,ex);
			throw new UnexpectedException(ex);
		}
		this.logMethod(L_CREATE_METHOD,L_END_OF_METHOD,null);
		return true;
	}


	/**
	* This method updade
	* @param bo IBusinessObject
	* @return boolean
	* @throws ApplicationException exception
	*/
	public boolean update(final IBusinessObject bo)
		throws ApplicationException{

		this.logMethod(L_UPDATE_METHOD,L_BEGIN_METHOD,bo);
		try{
			if(isClearSession()){
				this.flushThenClear();
				this.getHibernateTemplate().update(bo);

			}else{
				if(isRefreshSession()){
					this.getHibernateTemplate().update(bo);
					this.flushThenRefresh(bo);

				}else{
					this.getHibernateTemplate().update(bo);

				}

			}

		}
		catch (UniqueConstraintViolationException ex) {
			logError(L_UPDATE_METHOD,ex);
			throw new DBConstraintViolationException(ex);
		}

		catch (NonNullConstraintViolationException ex) {
			logError(L_UPDATE_METHOD,ex);
			throw new DBNullColumnNotAllowedException(ex);
		}

		catch (DataAccessException ex) {
			logError(L_UPDATE_METHOD,ex);
			throw new UnexpectedException(ex);
		}
		this.logMethod(L_UPDATE_METHOD,L_END_OF_METHOD,null);
		return true;
	}


	/**
	* This method merge update 
	* @param bo IBusinessObject
	* @return IBusinessObject
	* @throws ApplicationException exception

	*/
	public IBusinessObject mergeUpdate(final IBusinessObject bo)
		throws ApplicationException{

		this.logMethod(L_MERGE_METHOD,L_BEGIN_METHOD,bo);
		IBusinessObject businessObject = null;
		try{
			businessObject = (IBusinessObject)this.getHibernateTemplate().merge(bo);

		}
		catch (UniqueConstraintViolationException ex) {
			logError(L_MERGE_METHOD,ex);
			throw new DBConstraintViolationException(ex);
		}

		catch (NonNullConstraintViolationException ex) {
			logError(L_MERGE_METHOD,ex);
			throw new DBNullColumnNotAllowedException(ex);
		}

		catch (DataAccessException ex) {
			logError(L_MERGE_METHOD,ex);
			throw new UnexpectedException(ex);
		}
		this.logMethod(L_MERGE_METHOD,L_END_OF_METHOD,null);
		return businessObject;
	}


	/**
	* This method  delete
	* @param bo IBusinessObject
	* @return boolean
	* @throws ApplicationException exception
	*/
	public boolean delete(final IBusinessObject bo)
		throws ApplicationException{

		this.logMethod(L_DELETE_METHOD,L_BEGIN_METHOD,bo);
		try{
			if(isClearSession()){
				flushThenClear();
				this.getHibernateTemplate().delete(bo);

			}else{
				if(isRefreshSession()){
					this.getHibernateTemplate().delete(bo);
					this.getHibernateTemplate().flush();

				}else{
					this.getHibernateTemplate().delete(bo);

				}

			}

		}
		catch (UniqueConstraintViolationException ex) {
			logError(L_DELETE_METHOD,ex);
			throw new DBConstraintViolationException(ex);
		}

		catch (NonNullConstraintViolationException ex) {
			logError(L_DELETE_METHOD,ex);
			throw new DBNullColumnNotAllowedException(ex);
		}

		catch (DataAccessException ex) {
			logError(L_DELETE_METHOD,ex);
			throw new UnexpectedException(ex);
		}
		this.logMethod(L_DELETE_METHOD,L_END_OF_METHOD,null);
		return true;
	}


	/**
	* This method upsert
	* @param bo IBusinessObject
	* @return boolean
	* @throws ApplicationException exception

	*/
	public boolean upsert(final IBusinessObject bo)
		throws ApplicationException{

		this.logMethod(L_UPSERT_METHOD,L_BEGIN_METHOD,bo);
		try{
			this.getHibernateTemplate().saveOrUpdate(bo);

		}
		catch (UniqueConstraintViolationException ex) {
			logError(L_UPSERT_METHOD,ex);
			throw new DBConstraintViolationException(ex);
		}

		catch (NonNullConstraintViolationException ex) {
			logError(L_UPSERT_METHOD,ex);
			throw new DBNullColumnNotAllowedException(ex);
		}

		catch (DataAccessException ex) {
			logError(L_UPSERT_METHOD,ex);
			throw new UnexpectedException(ex);
		}
		this.logMethod(L_UPSERT_METHOD,L_END_OF_METHOD,null);
		return true;
	}


	/**
	* This method create all 
	* @param objects List<T>
	* @return boolean
	* @throws ApplicationException exception
	*/
	public boolean createAll(final List<T> objects)
		throws ApplicationException{

		this.logMethod(L_CREATE_ALL_METHOD,L_BEGIN_METHOD,objects);
		try{
			if(isClearSession()){
				flushThenClear();

			}
			for(final Iterator<T> iter = objects.iterator();iter.hasNext();){
				final T object = (T)iter.next();
				if(isRefreshSession()){
					this.getHibernateTemplate().flush();

				}
				this.getHibernateTemplate().save(object);
				if(isRefreshSession()){
					this.getHibernateTemplate().flush();
					this.getHibernateTemplate().refresh(object);

				}

			}

		}
		catch (UniqueConstraintViolationException ex) {
			logError(L_CREATE_ALL_METHOD,ex);
			throw new DBConstraintViolationException(ex);
		}

		catch (NonNullConstraintViolationException ex) {
			logError(L_CREATE_ALL_METHOD,ex);
			throw new DBNullColumnNotAllowedException(ex);
		}

		catch (DataAccessException ex) {
			logError(L_CREATE_ALL_METHOD,ex);
			throw new UnexpectedException(ex);
		}
		this.logMethod(L_CREATE_ALL_METHOD,L_END_OF_METHOD,null);
		return true;
	}


	/**
	* This method update all
	* @param objects List<T>
	* @return boolean
	* @throws ApplicationException  exception

	*/
	public boolean updateAll(final List<T> objects)
		throws ApplicationException{

		this.logMethod(L_UPDATE_ALL_METHOD,L_BEGIN_METHOD,objects);
		try{
			if(isClearSession()){
				flushThenClear();

			}
			for(final Iterator<T> iter = objects.iterator();iter.hasNext();){
				final T object = (T)iter.next();
				this.getHibernateTemplate().update(object);
				if(isRefreshSession()){
					flushThenRefresh(object);

				}

			}

		}
		catch (UniqueConstraintViolationException ex) {
			logError(L_UPDATE_ALL_METHOD,ex);
			throw new DBConstraintViolationException(ex);
		}

		catch (NonNullConstraintViolationException ex) {
			logError(L_UPDATE_ALL_METHOD,ex);
			throw new DBNullColumnNotAllowedException(ex);
		}

		catch (DataAccessException ex) {
			logError(L_UPDATE_ALL_METHOD,ex);
			throw new UnexpectedException(ex);
		}
		this.logMethod(L_UPDATE_ALL_METHOD,L_END_OF_METHOD,null);
		return true;
	}


	/**
	* This method delete All 
	* @param objects List<T>
	* @return boolean
	* @throws ApplicationException exception
	*/
	public boolean deleteAll(final List<T> objects)
		throws ApplicationException{

		this.logMethod(L_DELETE_ALL_METHOD,L_BEGIN_METHOD,objects);
		try{
			if(isClearSession()){
				flushThenClear();
				this.getHibernateTemplate().deleteAll(objects);

			}else{
				if(isRefreshSession()){
					this.getHibernateTemplate().deleteAll(objects);
					this.getHibernateTemplate().flush();

				}else{
					this.getHibernateTemplate().deleteAll(objects);

				}

			}

		}
		catch (UniqueConstraintViolationException ex) {
			logError(L_DELETE_ALL_METHOD,ex);
			throw new DBConstraintViolationException(ex);
		}

		catch (NonNullConstraintViolationException ex) {
			logError(L_DELETE_ALL_METHOD,ex);
			throw new DBNullColumnNotAllowedException(ex);
		}

		catch (DataAccessException ex) {
			logError(L_DELETE_ALL_METHOD,ex);
			throw new UnexpectedException(ex);
		}
		this.logMethod(L_DELETE_ALL_METHOD,L_END_OF_METHOD,null);
		return true;
	}


	/**
	* This method log Error 
	* @param methodCallString String
	* @param ex Exception
	*/
	private void logError(final String methodCallString,final Exception ex){

		if(LOGGER.isDebugEnabled()){
			final StringBuilder sb = new StringBuilder();
			sb.append(L_END_OF_METHOD).append(this.getClass().getName()).append('.').append(methodCallString).append(L_WITH_ERROR);
			LOGGER.debug(sb.toString(),ex);

		}

	}


	/**
	* This method Method log
	* @param s  String
	* @param prefix String
	* @param object Object
	*/
	private void logMethod(final String methodCallString,final String prefix,final Object object){

		if(LOGGER.isDebugEnabled()){
			final StringBuilder sb = new StringBuilder();
			sb.append(prefix).append(this.getClass().getName()).append('.').append(methodCallString);
			LOGGER.debug(sb.toString());
			if(object != null){
				LOGGER.debug("bo or List of bo's = " + object);

			}

		}

	}


	/**
	* This method flush Then Clear 
	*/
	private void flushThenClear(){
		this.getHibernateTemplate().flush();
		this.getHibernateTemplate().clear();

	}


	/**
	* This method flush Then Refresh 
	* @param bo Object
	*/
	private void flushThenRefresh(final Object bo){
		this.getHibernateTemplate().flush();
		this.getHibernateTemplate().refresh(bo);

	}
	
	/**
	 * The object is flushed after a persistence operation, if refreshSession
	 * is true.
	 * 
	 * @param value
	 */
	public final void setFlushSession(final boolean value) {
		this.flushSession = value;
	}

	/**
	 * The object is flush after a persistence operation, if refreshSession
	 * is true.
	 * 
	 * @return boolean
	 */
	public final boolean isFlushSession() {
		return this.flushSession;
	}
}

