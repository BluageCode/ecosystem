/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.business.standardfeatures.services.hql.uc57_hqloperation.bos;

import javax.xml.bind.annotation.XmlRootElement;

import com.bluage.documentation.business.AbstractBusinessObject;

/**
 * Screen57BO
 *
* @see com.netfective.bluage.core.business.IBusinessObject
 */
@XmlRootElement

public class Screen57BO
 extends AbstractBusinessObject {
	
	/**
	 * Default SUID for Screen57BO.
	 */
	private static final long serialVersionUID = 1L;
	

						/**
	 * Entity property: firstresult
	 *
	 */
private Integer firstresult;

					/**
	 * Entity property: maxresult
	 *
	 */
private Integer maxresult;



   /**
	* public default constructor for class Screen57BO
	*/
	public Screen57BO(){
		//NOP Screen57BO
	}


						/**
	 * Getter for firstresult of class Screen57BO.
	 *
	 * @return {@link java.lang.Integer}
	 * @generated
	 */ 
public  Integer getFirstresult() {
	return this.firstresult;
	}


	/**
	 * Setter for firstresult of class Screen57BO.
	 * @param value {@link java.lang.Integer}
	 * @generated
	 */

public  void setFirstresult(final Integer firstresult) {
	this.firstresult = firstresult;
}

					/**
	 * Getter for maxresult of class Screen57BO.
	 *
	 * @return {@link java.lang.Integer}
	 * @generated
	 */ 
public  Integer getMaxresult() {
	return this.maxresult;
	}


	/**
	 * Setter for maxresult of class Screen57BO.
	 * @param value {@link java.lang.Integer}
	 * @generated
	 */

public  void setMaxresult(final Integer maxresult) {
	this.maxresult = maxresult;
}


}

