package uc200;

import org.junit.Test;
import test.common.AbstractBADocTestCase;

public class ITuc200 extends AbstractBADocTestCase {
	
	@Override
	public  String getName(){
		return "UC-200";
	}
	
	public String element(String tab,String prenom, String nom, int plays, int shoots)
	{
		return "//table[contains(@id,'"+tab+"')]/tbody/tr[td/span[contains(text(),\""+nom+"\")] and td/span[contains(text(),\""+prenom+"\")] and td/span[contains(text(),\""+String.valueOf(plays)+"\")] and td/span[contains(text(),\""+String.valueOf(shoots)+"\")]]";
	}
	
	

	@Test
	public void testUC200() throws Exception {
		// On teste l'UC200 - Advanced Features/Performance - Data loading (N+1)

		
		selenium.open("");	
		selenium.click("link=Expand All");
		selenium.waitForPageToLoad("300000");
		selenium.click("link=UC200 - Data loading (N+1 select)");
		assertTrue(selenium.isElementPresent("//h2[contains(text(),\"Advanced Features > Performance > UC200 > Datagrid N+1 Loading\")]"));
		assertEquals("0", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		// On teste les donn�es du tableau [Data Loading]
		selenium.click("//*[contains(@id,'lnk_dataloading_1')]");
		selenium.waitForPageToLoad("30000");
		assertEquals("20", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent(element("tab_players","Frank","Oliver", 32,12)));
		assertTrue(selenium.isElementPresent(element("tab_players","Cole","Elbert", 12,3)));
		assertTrue(selenium.isElementPresent(element("tab_players","Dennis","Abel", 20,1)));
		assertTrue(selenium.isElementPresent(element("tab_players","Dawson","Ricky", 21,20)));
		assertTrue(selenium.isElementPresent(element("tab_players","Russell","Earl", 12,3)));
		assertTrue(selenium.isElementPresent(element("tab_players","Ramos","Howard", 1,8)));
		assertTrue(selenium.isElementPresent(element("tab_players","Meyer","Allan", 32,21)));
		assertTrue(selenium.isElementPresent(element("tab_players","Pope","Ruben", 20,10)));
		assertTrue(selenium.isElementPresent(element("tab_players","Cross","Lester", 10,20)));
		assertTrue(selenium.isElementPresent(element("tab_players","Barnett","Keith", 27,18)));
		assertTrue(selenium.isElementPresent(element("tab_players","Tate","Ivan", 20,21)));
		assertTrue(selenium.isElementPresent(element("tab_players","Nash","Sylvester", 9,2)));
		assertTrue(selenium.isElementPresent(element("tab_players","Hodges","Michael", 9,19)));
		assertTrue(selenium.isElementPresent(element("tab_players","Grant","Jerome", 10,28)));
		assertTrue(selenium.isElementPresent(element("tab_players","Carpenter","Woodrow", 13,40)));
		assertTrue(selenium.isElementPresent(element("tab_players","Robbins","Roderick", 20,20)));
		assertTrue(selenium.isElementPresent(element("tab_players","Chavez","Christian", 32,83)));
		assertTrue(selenium.isElementPresent(element("tab_players","Conner","Cecil", 10,21)));
		assertTrue(selenium.isElementPresent(element("tab_players","Lambert","Stephen", 24,82)));
		assertTrue(selenium.isElementPresent(element("tab_players","Romero","Gustavo", 15,72)));
	
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Romero\")] and td/span[contains(text(),\"Gustavo\")] and td/span[contains(text(),\"15\")] and td/span[contains(text(),\"72\")]]"));
		// On teste les donn�es du tableau [N+1 Data Loading]
		selenium.click("//*[contains(@id,'lnk_dataloading_n1')]");
		selenium.waitForPageToLoad("30000");
		assertEquals("20", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent(element("tab_players","Frank","Oliver", 32,12)));
		assertTrue(selenium.isElementPresent(element("tab_players","Cole","Elbert", 12,3)));
		assertTrue(selenium.isElementPresent(element("tab_players","Dennis","Abel", 20,1)));
		assertTrue(selenium.isElementPresent(element("tab_players","Dawson","Ricky", 21,20)));
		assertTrue(selenium.isElementPresent(element("tab_players","Russell","Earl", 12,3)));
		assertTrue(selenium.isElementPresent(element("tab_players","Ramos","Howard", 1,8)));
		assertTrue(selenium.isElementPresent(element("tab_players","Meyer","Allan", 32,21)));
		assertTrue(selenium.isElementPresent(element("tab_players","Pope","Ruben", 20,10)));
		assertTrue(selenium.isElementPresent(element("tab_players","Cross","Lester", 10,20)));
		assertTrue(selenium.isElementPresent(element("tab_players","Barnett","Keith", 27,18)));
		assertTrue(selenium.isElementPresent(element("tab_players","Tate","Ivan", 20,21)));
		assertTrue(selenium.isElementPresent(element("tab_players","Nash","Sylvester", 9,2)));
		assertTrue(selenium.isElementPresent(element("tab_players","Hodges","Michael", 9,19)));
		assertTrue(selenium.isElementPresent(element("tab_players","Grant","Jerome", 10,28)));
		assertTrue(selenium.isElementPresent(element("tab_players","Carpenter","Woodrow", 13,40)));
		assertTrue(selenium.isElementPresent(element("tab_players","Robbins","Roderick", 20,20)));
		assertTrue(selenium.isElementPresent(element("tab_players","Chavez","Christian", 32,83)));
		assertTrue(selenium.isElementPresent(element("tab_players","Conner","Cecil", 10,21)));
		assertTrue(selenium.isElementPresent(element("tab_players","Lambert","Stephen", 24,82)));
		assertTrue(selenium.isElementPresent(element("tab_players","Romero","Gustavo", 15,72)));
		// On clique sur [Clear]
		selenium.click("//*[contains(@id,'lnk_clear')]");
		selenium.waitForPageToLoad("30000");
		assertEquals("0", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
	}


}
