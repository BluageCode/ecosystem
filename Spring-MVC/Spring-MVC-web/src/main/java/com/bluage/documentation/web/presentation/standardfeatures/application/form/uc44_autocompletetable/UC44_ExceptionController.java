/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.application.form.uc44_autocompletetable ;

// Import declaration.
// Java imports.
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;

/**
 * Class : UC44_ExceptionController
 */
@Controller
@Scope("prototype")
@SessionAttributes("uC44_ExceptionForm")
@RequestMapping(value= "/presentation/standardfeatures/application/form/uc44_autocompletetable")
public class UC44_ExceptionController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC44_ExceptionController.class);
	
	//Current form name
	private static final String U_C44__EXCEPTION_FORM = "uC44_ExceptionForm";

	
	/**
	 * Property:customDateEditorsUC44_ExceptionController
	 */
	final private Map<String,CustomDateEditor> customDateEditorsUC44_ExceptionController = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:uC44_ExceptionValidator
	 */
	final private UC44_ExceptionValidator uC44_ExceptionValidator = new UC44_ExceptionValidator();
	
	
	// Initialise all the instances for UC44_ExceptionController

	/**
	* This method initialise the form : UC44_ExceptionForm 
	* @return UC44_ExceptionForm
	*/
	@ModelAttribute("UC44_ExceptionFormInit")
	public void initUC44_ExceptionForm(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, U_C44__EXCEPTION_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method UC44_ExceptionForm."); //for lnk_edit 
		}
		UC44_ExceptionForm uC44_ExceptionForm;
	
		if(request.getSession().getAttribute(U_C44__EXCEPTION_FORM) != null){
			uC44_ExceptionForm = (UC44_ExceptionForm)request.getSession().getAttribute(U_C44__EXCEPTION_FORM);
		} else {
			uC44_ExceptionForm = new UC44_ExceptionForm();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form UC44_ExceptionForm.");
		}
		uC44_ExceptionForm = (UC44_ExceptionForm)populateDestinationForm(request.getSession(), uC44_ExceptionForm);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  UC44_ExceptionForm.");
		}
		model.addAttribute(U_C44__EXCEPTION_FORM, uC44_ExceptionForm);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param UC44_ExceptionForm : The sceen form.
	 */
	@RequestMapping(value = "/UC44_Exception.html" ,method = RequestMethod.GET)
	public void prepareUC44_Exception(final HttpServletRequest request,final  Model model,final  @ModelAttribute("UC44_ExceptionFormInit") UC44_ExceptionForm uC44_ExceptionForm){
		
		UC44_ExceptionForm currentUC44_ExceptionForm = uC44_ExceptionForm;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(U_C44__EXCEPTION_FORM) == null){
			if(currentUC44_ExceptionForm!=null){
				request.getSession().setAttribute(U_C44__EXCEPTION_FORM, currentUC44_ExceptionForm);
			}else {
				currentUC44_ExceptionForm = new UC44_ExceptionForm();
				request.getSession().setAttribute(U_C44__EXCEPTION_FORM, currentUC44_ExceptionForm);	
			}
		} else {
			currentUC44_ExceptionForm = (UC44_ExceptionForm) request.getSession().getAttribute(U_C44__EXCEPTION_FORM);
		}

		currentUC44_ExceptionForm = (UC44_ExceptionForm)populateDestinationForm(request.getSession(), currentUC44_ExceptionForm);
		// Call all the Precontroller.
	currentUC44_ExceptionForm.setAlwaysCallPreControllers(true);
		model.addAttribute(U_C44__EXCEPTION_FORM, currentUC44_ExceptionForm);
		request.getSession().setAttribute(U_C44__EXCEPTION_FORM, currentUC44_ExceptionForm);
		
	}
	
	
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("UC44_ExceptionController [ ");
		// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(uC44_ExceptionValidator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsUC44_ExceptionController!=null); 
		return strBToS.toString();
	}
}
