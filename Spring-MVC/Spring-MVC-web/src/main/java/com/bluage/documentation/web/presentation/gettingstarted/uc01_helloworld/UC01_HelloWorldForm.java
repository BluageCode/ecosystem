/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.gettingstarted.uc01_helloworld;


import java.io.Serializable;

import com.bluage.documentation.business.gettingstarted.uc01_helloworld.bos.HelloWorld01BO;
import com.netfective.bluage.core.web.AbstractForm;
import com.netfective.bluage.core.web.IForm;

/**
* Class : UC01_HelloWorldForm
*/
public class UC01_HelloWorldForm extends AbstractForm implements Serializable, IForm {
	
	/** Constant : serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	//CONSTANT : helloWorld
	private static final String HELLO_WORLD = "helloWorld";
	/**
	 * 	Property: helloWorld 
	 */
	private HelloWorld01BO helloWorld;
/**
	 * Default constructor : UC01_HelloWorldForm
	 */
	public UC01_HelloWorldForm() {
		super();
		// Initialize : helloWorld
		this.helloWorld = new HelloWorld01BO();
			this.setAlwaysCallPreControllers(true);
	}
	
	/**
	 * 	Getter : helloWorld 
	 * 	@return : Return the helloWorld instance.
	 */
	public HelloWorld01BO getHelloWorld(){
		return helloWorld; // For UC01_HelloWorldForm
	}
	
	/**
	 * 	Setter : helloWorld 
	 *  @param helloWorldinstance : The instance to set.
	 */
	public void setHelloWorld(final HelloWorld01BO helloWorldinstance){
		this.helloWorld = helloWorldinstance;// For UC01_HelloWorldForm
	}
/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UC01_HelloWorldForm [ "+
HELLO_WORLD +" = " + helloWorld + "]";
	}
	
	/**
	 * Method setInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public void setInstanceByInstanceName(final String instanceName, final Object instance) {
		// Set the instance HelloWorld.
		if(HELLO_WORLD.equals(instanceName)){// For UC01_HelloWorldForm
			this.setHelloWorld((HelloWorld01BO)instance); // For UC01_HelloWorldForm
		}
			}
	
	/**
	 * Method getInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public Object getInstanceByInstanceName(final String instanceName){
		// Temporary object for tmpUC01_HelloWorldForm.
		Object tmpUC01_HelloWorldForm = null;
		
			
		// Get the instance HelloWorld for UC01_HelloWorldForm.
		if(HELLO_WORLD.equals(instanceName)){ // For UC01_HelloWorldForm
			tmpUC01_HelloWorldForm = this.getHelloWorld(); // For UC01_HelloWorldForm
		}
		return tmpUC01_HelloWorldForm;// For UC01_HelloWorldForm
	}
	
	}
