package uc03;

import org.junit.Test;
import test.common.AbstractBADocTestCase;
import test.common.Tools;

public class ITuc03ChampsVides extends AbstractBADocTestCase {

	@Override
	public String getName() {
		return "UC-03";
	}

	@Test
	public void testITuc03ChampsVides() throws Exception {
		// On teste l'UC03 - Getting started - Form Validation
		selenium.open("");
		selenium.waitForPageToLoad("30000");
		selenium.click("link=Expand All");
		selenium.click("link=UC03 - Form Validation");
		// On vérife l'affichage de l'UC.
		verifyTrue(selenium
				.isElementPresent("//h2[contains(text(),\"Getting started > UC03 > Form Validation\")]"));
		assertTrue(selenium
				.isElementPresent("//label[contains(text(),'First Name (RequiredFieldValidator, LengthValidator)')]"));
		assertTrue(selenium
				.isElementPresent("xpath=(//*[contains(text(),'(*)')])[2]"));
		assertTrue(selenium
				.isElementPresent("//label[contains(text(),'Last Name (RequiredFieldValidator, LengthValidator)')]"));
		assertTrue(selenium
				.isElementPresent("xpath=(//label[contains(text(),'(*)')])"));
		assertTrue(selenium
				.isElementPresent("//label[contains(text(),'Date of Birth (MM/dd/yyyy)')]"));
		assertTrue(selenium
				.isElementPresent("//label[contains(text(),'Estimated value (CustomValidator number)')]"));
		assertTrue(selenium
				.isElementPresent("//label[contains(text(),'Weight (RangeValidator 60-140kgs)')]"));
		assertTrue(selenium
				.isElementPresent("//label[contains(text(),'Height (RangeValidator 150-240cms)')]"));
		assertTrue(selenium
				.isElementPresent("//label[contains(text(),'Position')]"));
		assertTrue(selenium
				.isElementPresent("//label[contains(text(),'Rookie')]"));
		// On vérife les positions:
		assertTrue(selenium
				.isElementPresent("//option[contains(text(),'Point Guard')]"));
		assertTrue(selenium
				.isElementPresent("//option[contains(text(),'Shooting Guard')]"));
		assertTrue(selenium
				.isElementPresent("//option[contains(text(),'Small Forward')]"));
		assertTrue(selenium
				.isElementPresent("//option[contains(text(),'Power Forward')]"));
		assertTrue(selenium
				.isElementPresent("//option[contains(text(),'Center')]"));
		// On teste l'UC en créant un joueur.
		// On ne rempli aucun champs, cliquer sur "Create Player"
		selenium.click("//*[contains(@id,'lnk_create')]");
		selenium.waitForPageToLoad("30000");
		if (Tools.getVersion() == Tools.SpringMCV) {
			System.out.println(Tools.getVersion());
			// On vérifie que l'ID du message d'erreur est présente.
			assertTrue(selenium.isElementPresent(Tools.IDSpringMCV(
					"playerToCreate", "firstName")));
			// On vérifie que le message d'erreur est présent.
			assertEquals("First name is required.", selenium.getText(Tools
					.IDSpringMCV("playerToCreate", "firstName")));
			// On vérifie que l'ID du message d'erreur est présente.
			assertTrue(selenium.isElementPresent(Tools.IDSpringMCV(
					"playerToCreate", "lastName")));
			// On vérifie que le message d'erreur est présent.
			assertEquals("Last name is required.", selenium.getText(Tools
					.IDSpringMCV("playerToCreate", "lastName")));
		}
		if (Tools.getVersion() == Tools.Struts) {
			System.out.println(Tools.getVersion());
			// On vérifie que l'ID du message d'erreur est présente.
			assertTrue(selenium.isElementPresent(Tools
					.IDStruts("txt_firstname")));
			// On vérifie que le message d'erreur est présent.
			assertEquals("First name is required.",
					selenium.getText(Tools.IDStruts("txt_firstname")));
			// On vérifie que l'ID du message d'erreur est présente.
			assertTrue(selenium
					.isElementPresent(Tools.IDStruts("txt_lastname")));
			// On vérifie que le message d'erreur est présent.
			assertEquals("Last name is required.",
					selenium.getText(Tools.IDStruts("txt_lastname")));
		}

	}

}
