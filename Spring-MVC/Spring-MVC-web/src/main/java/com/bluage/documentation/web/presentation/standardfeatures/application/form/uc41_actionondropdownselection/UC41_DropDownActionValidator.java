/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.application.form.uc41_actionondropdownselection ;


import org.apache.log4j.Logger;
import org.springframework.validation.Errors;

import com.netfective.bluage.core.validator.AbstractValidator;
/**
* Class :UC41_DropDownActionValidator
*/
public class UC41_DropDownActionValidator extends AbstractValidator{
	
	// LOGGER for the class UC41_DropDownActionValidator
	private static final Logger LOGGER = Logger.getLogger( UC41_DropDownActionValidator.class);
	
	
	/**
	* Operation validate for UC41_DropDownActionForm
	* @param obj : the current form (UC41_DropDownActionForm)
	* @param errors : The spring errors to return for the form UC41_DropDownActionForm
	*/
	public void validate(final Object obj,final Errors errors){
		LOGGER.info("Beginning method : validate"); // UC41_DropDownActionValidator
		UC41_DropDownActionForm cUC41_DropDownActionForm = (UC41_DropDownActionForm)obj; // UC41_DropDownActionValidator
		LOGGER.info("Ending method : validate the form "+ cUC41_DropDownActionForm.getClass().getName()); // UC41_DropDownActionValidator
	}

	/**
	* Method to implements to use spring validators (UC41_DropDownActionForm)
	* @param aClass : Class for the form UC41_DropDownActionForm
	* @return boolean
	*/
	public boolean supports(final Class aClass){
		return (new UC41_DropDownActionForm()).getClass().equals(aClass); // UC41_DropDownActionValidator
	}
}
