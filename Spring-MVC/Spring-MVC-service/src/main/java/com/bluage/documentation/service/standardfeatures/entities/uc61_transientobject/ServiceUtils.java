/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.service.standardfeatures.entities.uc61_transientobject;


import java.util.List;

import javax.inject.Named;

import com.bluage.documentation.service.AbstractService;
import com.netfective.bluage.core.exception.ApplicationException;

/**
 * Defines  application services for ServiceUtils
  */
@Named("com.bluage.documentation.service.standardfeatures.entities.uc61_transientobject.ServiceUtils")
public class ServiceUtils  extends AbstractService{


				


	/**
	 *  Default constructor 
	 */
	public ServiceUtils() {
		super(); // Call super for default constructor : ServiceUtils
	}
	



			
	/**    
	 * Process operation.
	 * @param liste {@link List}
		 * @return {@link List}
	 */
	public List transfertListe(List liste )
 throws ApplicationException {

				return privateTransfertListe(liste);
	}
	

	/**    
	 * Process operation.
	 * @param liste {@link List}
		 * @return {@link List}
	 */
	private List privateTransfertListe(List liste )
 throws ApplicationException {
				// Central buffer node declaration for operation transfertListe
			
						return liste;
		}

		}
