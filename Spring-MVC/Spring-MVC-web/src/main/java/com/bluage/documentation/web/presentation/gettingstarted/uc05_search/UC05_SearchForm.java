/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.gettingstarted.uc05_search;


import java.io.Serializable;
import java.util.List;

import com.bluage.documentation.business.gettingstarted.uc05_search.bos.Player05BO;
import com.netfective.bluage.core.web.AbstractForm;
import com.netfective.bluage.core.web.IForm;

/**
* Class : UC05_SearchForm
*/
public class UC05_SearchForm extends AbstractForm implements Serializable, IForm {
	
	/** Constant : serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	//CONSTANT : searchResult
	private static final String SEARCH_RESULT = "searchResult";
	//CONSTANT : hqlCriteria
	private static final String HQL_CRITERIA = "hqlCriteria";
	//CONSTANT : playerCriteria
	private static final String PLAYER_CRITERIA = "playerCriteria";
	//CONSTANT : allPositions
	private static final String ALL_POSITIONS = "allPositions";
	//CONSTANT : playerSelected
	private static final String PLAYER_SELECTED = "playerSelected";
	//CONSTANT : currentPlayer
	private static final String CURRENT_PLAYER = "currentPlayer";
	/**
	 * 	Property: searchResult 
	 */
	private List<Player05BO> searchResult;
	/**
	 * 	Property: hqlCriteria 
	 */
	private Player05BO hqlCriteria;
	/**
	 * 	Property: playerCriteria 
	 */
	private Player05BO playerCriteria;
	/**
	 * 	Property: allPositions 
	 */
	private List allPositions;
	/**
	 * 	Property: playerSelected 
	 */
	private Player05BO playerSelected;
	/**
	 * 	Property: currentPlayer 
	 */
	private Player05BO currentPlayer;
/**
	 * Default constructor : UC05_SearchForm
	 */
	public UC05_SearchForm() {
		super();
		// Initialize : searchResult
		this.searchResult = new java.util.ArrayList<com.bluage.documentation.business.gettingstarted.uc05_search.bos.Player05BO>();
		// Initialize : hqlCriteria
		this.hqlCriteria = new Player05BO();
		// Initialize : playerCriteria
		this.playerCriteria = new Player05BO();
		// Initialize : allPositions
		this.allPositions = new java.util.ArrayList();
		// Initialize : playerSelected
		this.playerSelected = null;
		// Initialize : currentPlayer
		this.currentPlayer = null;
					this.setAlwaysCallPreControllers(true);
	}
	
	/**
	 * 	Getter : searchResult 
	 * 	@return : Return the searchResult instance.
	 */
	public List<Player05BO> getSearchResult(){
		return searchResult; // For UC05_SearchForm
	}
	
	/**
	 * 	Setter : searchResult 
	 *  @param searchResultinstance : The instance to set.
	 */
	public void setSearchResult(final List<Player05BO> searchResultinstance){
		this.searchResult = searchResultinstance;// For UC05_SearchForm
	}
	/**
	 * 	Getter : hqlCriteria 
	 * 	@return : Return the hqlCriteria instance.
	 */
	public Player05BO getHqlCriteria(){
		return hqlCriteria; // For UC05_SearchForm
	}
	
	/**
	 * 	Setter : hqlCriteria 
	 *  @param hqlCriteriainstance : The instance to set.
	 */
	public void setHqlCriteria(final Player05BO hqlCriteriainstance){
		this.hqlCriteria = hqlCriteriainstance;// For UC05_SearchForm
	}
	/**
	 * 	Getter : playerCriteria 
	 * 	@return : Return the playerCriteria instance.
	 */
	public Player05BO getPlayerCriteria(){
		return playerCriteria; // For UC05_SearchForm
	}
	
	/**
	 * 	Setter : playerCriteria 
	 *  @param playerCriteriainstance : The instance to set.
	 */
	public void setPlayerCriteria(final Player05BO playerCriteriainstance){
		this.playerCriteria = playerCriteriainstance;// For UC05_SearchForm
	}
	/**
	 * 	Getter : allPositions 
	 * 	@return : Return the allPositions instance.
	 */
	public List getAllPositions(){
		return allPositions; // For UC05_SearchForm
	}
	
	/**
	 * 	Setter : allPositions 
	 *  @param allPositionsinstance : The instance to set.
	 */
	public void setAllPositions(final List allPositionsinstance){
		this.allPositions = allPositionsinstance;// For UC05_SearchForm
	}
	/**
	 * 	Getter : playerSelected 
	 * 	@return : Return the playerSelected instance.
	 */
	public Player05BO getPlayerSelected(){
		return playerSelected; // For UC05_SearchForm
	}
	
	/**
	 * 	Setter : playerSelected 
	 *  @param playerSelectedinstance : The instance to set.
	 */
	public void setPlayerSelected(final Player05BO playerSelectedinstance){
		this.playerSelected = playerSelectedinstance;// For UC05_SearchForm
	}
	/**
	 * 	Getter : currentPlayer 
	 * 	@return : Return the currentPlayer instance.
	 */
	public Player05BO getCurrentPlayer(){
		return currentPlayer; // For UC05_SearchForm
	}
	
	/**
	 * 	Setter : currentPlayer 
	 *  @param currentPlayerinstance : The instance to set.
	 */
	public void setCurrentPlayer(final Player05BO currentPlayerinstance){
		this.currentPlayer = currentPlayerinstance;// For UC05_SearchForm
	}
/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UC05_SearchForm [ "+
SEARCH_RESULT +" = " + searchResult +HQL_CRITERIA +" = " + hqlCriteria +PLAYER_CRITERIA +" = " + playerCriteria +ALL_POSITIONS +" = " + allPositions +PLAYER_SELECTED +" = " + playerSelected +CURRENT_PLAYER +" = " + currentPlayer + "]";
	}
	
	/**
	 * Method setInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public void setInstanceByInstanceName(final String instanceName, final Object instance) {
		// Set the instance SearchResult.
		if(SEARCH_RESULT.equals(instanceName)){// For UC05_SearchForm
			this.setSearchResult((List<Player05BO>)instance); // For UC05_SearchForm
		}
				// Set the instance HqlCriteria.
		if(HQL_CRITERIA.equals(instanceName)){// For UC05_SearchForm
			this.setHqlCriteria((Player05BO)instance); // For UC05_SearchForm
		}
				// Set the instance PlayerCriteria.
		if(PLAYER_CRITERIA.equals(instanceName)){// For UC05_SearchForm
			this.setPlayerCriteria((Player05BO)instance); // For UC05_SearchForm
		}
				// Set the instance AllPositions.
		if(ALL_POSITIONS.equals(instanceName)){// For UC05_SearchForm
			this.setAllPositions((List)instance); // For UC05_SearchForm
		}
				// Set the instance PlayerSelected.
		if(PLAYER_SELECTED.equals(instanceName)){// For UC05_SearchForm
			this.setPlayerSelected((Player05BO)instance); // For UC05_SearchForm
		}
				// Set the instance CurrentPlayer.
		if(CURRENT_PLAYER.equals(instanceName)){// For UC05_SearchForm
			this.setCurrentPlayer((Player05BO)instance); // For UC05_SearchForm
		}
			}
	
	/**
	 * Method getInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public Object getInstanceByInstanceName(final String instanceName){
		// Temporary object for tmpUC05_SearchForm.
		Object tmpUC05_SearchForm = null;
		
			
		// Get the instance SearchResult for UC05_SearchForm.
		if(SEARCH_RESULT.equals(instanceName)){ // For UC05_SearchForm
			tmpUC05_SearchForm = this.getSearchResult(); // For UC05_SearchForm
		}
			
		// Get the instance HqlCriteria for UC05_SearchForm.
		if(HQL_CRITERIA.equals(instanceName)){ // For UC05_SearchForm
			tmpUC05_SearchForm = this.getHqlCriteria(); // For UC05_SearchForm
		}
			
		// Get the instance PlayerCriteria for UC05_SearchForm.
		if(PLAYER_CRITERIA.equals(instanceName)){ // For UC05_SearchForm
			tmpUC05_SearchForm = this.getPlayerCriteria(); // For UC05_SearchForm
		}
			
		// Get the instance AllPositions for UC05_SearchForm.
		if(ALL_POSITIONS.equals(instanceName)){ // For UC05_SearchForm
			tmpUC05_SearchForm = this.getAllPositions(); // For UC05_SearchForm
		}
			
		// Get the instance PlayerSelected for UC05_SearchForm.
		if(PLAYER_SELECTED.equals(instanceName)){ // For UC05_SearchForm
			tmpUC05_SearchForm = this.getPlayerSelected(); // For UC05_SearchForm
		}
			
		// Get the instance CurrentPlayer for UC05_SearchForm.
		if(CURRENT_PLAYER.equals(instanceName)){ // For UC05_SearchForm
			tmpUC05_SearchForm = this.getCurrentPlayer(); // For UC05_SearchForm
		}
		return tmpUC05_SearchForm;// For UC05_SearchForm
	}
	
			}
