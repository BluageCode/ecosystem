--<ScriptOptions statementTerminator=";"/>

ALTER TABLE team33 DROP CONSTRAINT team33_pkey;

DROP TABLE team33;

CREATE TABLE team33 (
		id INT8 NOT NULL,
		version INT4 NOT NULL,
		city VARCHAR(1024),
		name VARCHAR(1024)
	);


INSERT INTO team33 (id, version, city, name) VALUES (1, 0, 'Charlotte', 'Tigers');
INSERT INTO team33 (id, version, city, name) VALUES (2, 0, 'Kansas City', 'Bears');
INSERT INTO team33 (id, version, city, name) VALUES (3, 0, 'Detroit', 'Chargers');
INSERT INTO team33 (id, version, city, name) VALUES (4, 0, 'Detroit', 'Hawks');
INSERT INTO team33 (id, version, city, name) VALUES (5, 0, 'Omaha', 'Braves');
INSERT INTO team33 (id, version, city, name) VALUES (6, 0, 'Raleigh', 'Eagles');
INSERT INTO team33 (id, version, city, name) VALUES (7, 0, 'Oakland', 'Cardinals');


ALTER TABLE team33 ADD CONSTRAINT team33_pkey PRIMARY KEY (id);
