<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="playerListing" language="groovy" pageWidth="595" pageHeight="842" columnWidth="535" leftMargin="20" rightMargin="20" topMargin="20" bottomMargin="20">
	<property name="ireport.zoom" value="1.6105100000000008"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<field name="firstName" class="java.lang.String"/>
	<field name="lastName" class="java.lang.String"/>
	<field name="dateOfBirth" class="java.util.Date"/>
	<field name="estimatedValue" class="java.lang.Float"/>
	<field name="weight" class="java.lang.Integer"/>
	<field name="height" class="java.lang.Integer"/>
	<field name="position.code" class="java.lang.String"/>
	<variable name="lastNameFirstLetter" class="java.lang.String" resetType="Group" resetGroup="LastNameFirstLetterGroup">
		<variableExpression><![CDATA[($F{lastName} == null || $F{lastName}.length() == 0) ? "" : $F{lastName}.substring(0,1)]]></variableExpression>
	</variable>
	<group name="LastNameFirstLetterGroup">
		<groupExpression><![CDATA[$V{lastNameFirstLetter}]]></groupExpression>
		<groupHeader>
			<band height="50">
				<textField evaluationTime="Auto">
					<reportElement mode="Opaque" x="0" y="5" width="100" height="40" backcolor="#9999FF"/>
					<textElement>
						<font size="30"/>
					</textElement>
					<textFieldExpression class="java.lang.String"><![CDATA[$V{lastNameFirstLetter}]]></textFieldExpression>
				</textField>
			</band>
		</groupHeader>
		<groupFooter>
			<band/>
		</groupFooter>
	</group>
	<background>
		<band/>
	</background>
	<title>
		<band height="72">
			<frame>
				<reportElement mode="Opaque" x="-20" y="-20" width="595" height="92" backcolor="#006699"/>
				<staticText>
					<reportElement x="20" y="20" width="234" height="43" forecolor="#FFFFFF"/>
					<textElement>
						<font size="34" isBold="true"/>
					</textElement>
					<text><![CDATA[Players Listing]]></text>
				</staticText>
				<staticText>
					<reportElement x="395" y="43" width="180" height="20" forecolor="#FFFFFF"/>
					<textElement textAlignment="Right">
						<font size="14" isBold="false"/>
					</textElement>
					<text><![CDATA[BLU AGE Documentation]]></text>
				</staticText>
			</frame>
		</band>
	</title>
	<pageHeader>
		<band height="13"/>
	</pageHeader>
	<columnHeader>
		<band height="21">
			<line>
				<reportElement x="-20" y="20" width="595" height="1" forecolor="#666666"/>
			</line>
			<staticText>
				<reportElement x="0" y="0" width="150" height="20" forecolor="#000000" backcolor="#FFFFFF"/>
				<textElement textAlignment="Left">
					<font size="12" isBold="false"/>
				</textElement>
				<text><![CDATA[Name]]></text>
			</staticText>
			<staticText>
				<reportElement x="150" y="0" width="100" height="20" forecolor="#000000" backcolor="#FFFFFF"/>
				<textElement textAlignment="Left">
					<font size="12" isBold="false"/>
				</textElement>
				<text><![CDATA[Date of Birth]]></text>
			</staticText>
			<staticText>
				<reportElement x="250" y="0" width="125" height="20" forecolor="#000000" backcolor="#FFFFFF"/>
				<textElement textAlignment="Left">
					<font size="12" isBold="false"/>
				</textElement>
				<text><![CDATA[Estimated Value (M$)]]></text>
			</staticText>
			<staticText>
				<reportElement x="375" y="0" width="70" height="20" forecolor="#000000" backcolor="#FFFFFF"/>
				<textElement textAlignment="Left">
					<font size="12" isBold="false"/>
				</textElement>
				<text><![CDATA[Weight (kg)]]></text>
			</staticText>
			<staticText>
				<reportElement x="445" y="0" width="60" height="20" forecolor="#000000" backcolor="#FFFFFF"/>
				<textElement textAlignment="Left">
					<font size="12" isBold="false"/>
				</textElement>
				<text><![CDATA[Height (cm)]]></text>
			</staticText>
			<staticText>
				<reportElement x="505" y="0" width="50" height="20" forecolor="#000000" backcolor="#FFFFFF"/>
				<textElement textAlignment="Left">
					<font size="12" isBold="false"/>
				</textElement>
				<text><![CDATA[Position]]></text>
			</staticText>
		</band>
	</columnHeader>
	<detail>
		<band height="20">
			<line>
				<reportElement positionType="FixRelativeToBottom" x="0" y="19" width="555" height="1"/>
			</line>
			<textField>
				<reportElement x="0" y="0" width="150" height="20"/>
				<textElement/>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{lastName} + ", " + $F{firstName}]]></textFieldExpression>
			</textField>
			<textField pattern="yyyy/MM/dd">
				<reportElement x="150" y="0" width="100" height="20"/>
				<textElement/>
				<textFieldExpression class="java.util.Date"><![CDATA[$F{dateOfBirth}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement x="250" y="0" width="125" height="20"/>
				<textElement/>
				<textFieldExpression class="java.lang.Float"><![CDATA[$F{estimatedValue}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="445" y="0" width="60" height="20"/>
				<textElement/>
				<textFieldExpression class="java.lang.Integer"><![CDATA[$F{height}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="505" y="0" width="50" height="20"/>
				<textElement/>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{position.code}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="375" y="0" width="70" height="20"/>
				<textElement/>
				<textFieldExpression class="java.lang.Integer"><![CDATA[$F{weight}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<columnFooter>
		<band/>
	</columnFooter>
	<pageFooter>
		<band height="17">
			<textField>
				<reportElement mode="Opaque" x="0" y="2" width="515" height="13" backcolor="#E6E6E6"/>
				<textElement textAlignment="Right"/>
				<textFieldExpression class="java.lang.String"><![CDATA["Page "+$V{PAGE_NUMBER}+" of"]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Report">
				<reportElement mode="Opaque" x="515" y="2" width="40" height="13" backcolor="#E6E6E6"/>
				<textElement/>
				<textFieldExpression class="java.lang.String"><![CDATA[" " + $V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<textField pattern="EEEEE dd MMMMM yyyy">
				<reportElement x="10" y="2" width="100" height="13"/>
				<textElement/>
				<textFieldExpression class="java.util.Date"><![CDATA[new java.util.Date()]]></textFieldExpression>
			</textField>
		</band>
	</pageFooter>
	<summary>
		<band/>
	</summary>
</jasperReport>
