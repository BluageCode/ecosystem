/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.service.advancedfeatures.valueobject.uc206_valueobject.voservice;


import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;

import com.bluage.documentation.business.advancedfeatures.valueobject.uc206_valueobject.vo.Player206VO;
import com.bluage.documentation.business.advancedfeatures.valueobject.uc206_valueobject.vo.daofinder.Player206VODAOFinderImpl;
import com.bluage.documentation.service.AbstractService;
import com.netfective.bluage.core.exception.ApplicationException;


/**
 * Defines  application VO services for ServicePlayer206VO
 *
 */
@Named ("com.bluage.documentation.service.advancedfeatures.valueobject.uc206_valueobject.voservice.ServicePlayer206VO")
public class ServicePlayer206VO  extends AbstractService{
			@Inject
		@Named ("com.bluage.documentation.business.advancedfeatures.valueobject.uc206_valueobject.vo.daofinder.Player206VODAOFinderImpl")
		private Player206VODAOFinderImpl player206VOFinder;
		

	/**
	 *  Default constructor 
	 */
	public ServicePlayer206VO() {
		super();
	}



			
	/**    
	 * Operation findAll
	 * @return {@link List}
	 */
	public List player206VOFindAll( )
 throws ApplicationException {

		return player206VOFinder.findAll();
	}

			
			
	/**    
	 * Operation findByPrimaryKey
	 * @param vo {@link Player206VO}
		 * @return {@link Player206VO}
	 */
	public Player206VO player206VOFindById(Player206VO vo )
 throws ApplicationException {

		return (Player206VO)player206VOFinder.findByPrimaryKey(getBoId(vo));
	}
	/**
	 *  Return the primary key from reflected business object.
	 */
	private Object getBoId(Player206VO vo) {
				return vo.getId();
		}

			
	
	/**    
	 * Operation findByProperties
	 * @param vo {@link Player206VO}
		 * @return {@link List}
	 */
	public List player206VOFindByProperties(Player206VO vo )
 throws ApplicationException {

		return player206VOFinder.findByProperties(voToCriterias(vo));
	}
	/**
	 * Transform a value object into criteria.
	 * @param voInstance {@link Player206VO} business object instance.
	 * @return a Map containing the search criteria.
	 */
	private Map<String, Object> voToCriterias(Player206VO voInstance) {
		Map<String, Object> criterias = new Hashtable<String, Object>();
    	voInstance.setObjMode("DAO");    	
    	if(voInstance.getId() != null) {
    		criterias.put("id", voInstance.getId());
    	}   	
    	if(voInstance.getFirstName() != null && !"".equals(voInstance.getFirstName())) {
			criterias.put("firstName",voInstance.getFirstName());
		}
    	if(voInstance.getLastName() != null && !"".equals(voInstance.getLastName())) {
			criterias.put("lastName",voInstance.getLastName());
		}
    	if(voInstance.getDateOfBirth() != null) {
    		criterias.put("dateOfBirth", voInstance.getDateOfBirth());
    	}   	
    	if(voInstance.getEstimatedValue() != null) {
    		criterias.put("estimatedValue", voInstance.getEstimatedValue());
    	}   	
    	if(voInstance.getWeight() != null) {
    		criterias.put("weight", voInstance.getWeight());
    	}   	
    	if(voInstance.getHeight() != null) {
    		criterias.put("height", voInstance.getHeight());
    	}   	
    	if(voInstance.getRookie() != null) {
    		criterias.put("rookie", voInstance.getRookie());
    	}   	
    	if(voInstance.getIdAsString() != null && !"".equals(voInstance.getIdAsString())) {
			criterias.put("idAsString",voInstance.getIdAsString());
		}
    	if(voInstance.getDateOfBirthAsString() != null && !"".equals(voInstance.getDateOfBirthAsString())) {
			criterias.put("dateOfBirthAsString",voInstance.getDateOfBirthAsString());
		}
    	if(voInstance.getEstimatedValueAsString() != null && !"".equals(voInstance.getEstimatedValueAsString())) {
			criterias.put("estimatedValueAsString",voInstance.getEstimatedValueAsString());
		}
    	if(voInstance.getWeightAsString() != null && !"".equals(voInstance.getWeightAsString())) {
			criterias.put("weightAsString",voInstance.getWeightAsString());
		}
    	if(voInstance.getHeightAsString() != null && !"".equals(voInstance.getHeightAsString())) {
			criterias.put("heightAsString",voInstance.getHeightAsString());
		}
	    if(voInstance.getPositionVO() != null && voInstance.getPositionVO().getCode() != null) {
    		criterias.put("positionVO", voInstance.getPositionVO());    		
    	}
					 		return criterias;
	}				

}
