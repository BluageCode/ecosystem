/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.entities.uc65_lazy ;

// Import declaration.
// Java imports.
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.bluage.documentation.business.standardfeatures.entities.uc65_lazy.bos.Team65BO;
import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;

/**
 * Class : UC65_DisplayPlayersController
 */
@Controller
@Scope("prototype")
@SessionAttributes("uC65_DisplayPlayersForm")
@RequestMapping(value= "/presentation/standardfeatures/entities/uc65_lazy")
public class UC65_DisplayPlayersController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC65_DisplayPlayersController.class);
	
	//Current form name
	private static final String U_C65__DISPLAY_PLAYERS_FORM = "uC65_DisplayPlayersForm";

	//CONSTANT: teamLazyTrue
	private static final String TEAM_LAZY_TRUE = "teamLazyTrue";
		//CONSTANT: players table or repeater.
	private static final String PLAYERS = "players";
				
	/**
	 * Property:customDateEditorsUC65_DisplayPlayersController
	 */
	final private Map<String,CustomDateEditor> customDateEditorsUC65_DisplayPlayersController = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:uC65_DisplayPlayersValidator
	 */
	final private UC65_DisplayPlayersValidator uC65_DisplayPlayersValidator = new UC65_DisplayPlayersValidator();
	
	
	// Initialise all the instances for UC65_DisplayPlayersController
		// Initialize the instance teamLazyTrue of type com.bluage.documentation.business.standardfeatures.entities.uc65_lazy.bos.Team65BO with the value : null for UC65_DisplayPlayersController
		private Team65BO teamLazyTrue;
			// Initialize the instance players for the state lnk_view_nofetch
		private List players; // Initialize the instance players for UC65_DisplayPlayersController
				
	/**
	* This method initialise the form : UC65_DisplayPlayersForm 
	* @return UC65_DisplayPlayersForm
	*/
	@ModelAttribute("UC65_DisplayPlayersFormInit")
	public void initUC65_DisplayPlayersForm(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, U_C65__DISPLAY_PLAYERS_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method UC65_DisplayPlayersForm."); //for lnk_view_nofetch 
		}
		UC65_DisplayPlayersForm uC65_DisplayPlayersForm;
	
		if(request.getSession().getAttribute(U_C65__DISPLAY_PLAYERS_FORM) != null){
			uC65_DisplayPlayersForm = (UC65_DisplayPlayersForm)request.getSession().getAttribute(U_C65__DISPLAY_PLAYERS_FORM);
		} else {
			uC65_DisplayPlayersForm = new UC65_DisplayPlayersForm();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form UC65_DisplayPlayersForm.");
		}
		uC65_DisplayPlayersForm = (UC65_DisplayPlayersForm)populateDestinationForm(request.getSession(), uC65_DisplayPlayersForm);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  UC65_DisplayPlayersForm.");
		}
		model.addAttribute(U_C65__DISPLAY_PLAYERS_FORM, uC65_DisplayPlayersForm);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param UC65_DisplayPlayersForm : The sceen form.
	 */
	@RequestMapping(value = "/UC65_DisplayPlayers.html" ,method = RequestMethod.GET)
	public void prepareUC65_DisplayPlayers(final HttpServletRequest request,final  Model model,final  @ModelAttribute("UC65_DisplayPlayersFormInit") UC65_DisplayPlayersForm uC65_DisplayPlayersForm){
		
		UC65_DisplayPlayersForm currentUC65_DisplayPlayersForm = uC65_DisplayPlayersForm;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(U_C65__DISPLAY_PLAYERS_FORM) == null){
			if(currentUC65_DisplayPlayersForm!=null){
				request.getSession().setAttribute(U_C65__DISPLAY_PLAYERS_FORM, currentUC65_DisplayPlayersForm);
			}else {
				currentUC65_DisplayPlayersForm = new UC65_DisplayPlayersForm();
				request.getSession().setAttribute(U_C65__DISPLAY_PLAYERS_FORM, currentUC65_DisplayPlayersForm);	
			}
		} else {
			currentUC65_DisplayPlayersForm = (UC65_DisplayPlayersForm) request.getSession().getAttribute(U_C65__DISPLAY_PLAYERS_FORM);
		}

				currentUC65_DisplayPlayersForm = (UC65_DisplayPlayersForm)populateDestinationForm(request.getSession(), currentUC65_DisplayPlayersForm);
		// Call all the Precontroller.
	currentUC65_DisplayPlayersForm.setAlwaysCallPreControllers(true);
		model.addAttribute(U_C65__DISPLAY_PLAYERS_FORM, currentUC65_DisplayPlayersForm);
		request.getSession().setAttribute(U_C65__DISPLAY_PLAYERS_FORM, currentUC65_DisplayPlayersForm);
		
	}
	
			
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("UC65_DisplayPlayersController [ ");
			strBToS.append(TEAM_LAZY_TRUE);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(teamLazyTrue);
			strBToS.append(END_TO_STRING_DELIMITER);
				strBToS.append(PLAYERS);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(players);
			strBToS.append(END_TO_STRING_DELIMITER);
					// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(uC65_DisplayPlayersValidator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsUC65_DisplayPlayersController!=null); 
		return strBToS.toString();
	}
}
