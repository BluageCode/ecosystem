--<ScriptOptions statementTerminator=";"/>
ALTER TABLE player230 DROP CONSTRAINT player230_pkey;

DROP TABLE player230;

CREATE TABLE player230 (
		id INT8 NOT NULL,
		version INT4 NOT NULL,
		name VARCHAR(64) NOT NULL,
		date_of_birth DATE,
		position VARCHAR(2),
		seasons INT4,
		pass_attemps INT4,
		pass_completed INT4,
		pass_received INT4,
		yards INT4,
		games INT4,
		passed boolean,
		receiver boolean,
		generic boolean,
		receiving_avg real,
		generic_avg real,
		scored boolean
	);


INSERT INTO player230 (id, version, name, date_of_birth, position, seasons, pass_attemps, pass_completed, pass_received, yards, games, passed, receiver, generic, receiving_avg, generic_avg, scored) VALUES (1, 0, 'Oliver Frank', '1976-07-17', 'WR', 12, 112, 111, 7, 99, 32,false,false,false, 0, 0,false);
INSERT INTO player230 (id, version, name, date_of_birth, position, seasons, pass_attemps, pass_completed, pass_received, yards, games, passed, receiver, generic, receiving_avg, generic_avg, scored) VALUES (2, 0, 'Elbert Cole', '1981-03-11', 'QB', 8, 88, 32, 54, 688, 88,false,false,false, 0, 0,true);
INSERT INTO player230 (id, version, name, date_of_birth, position, seasons, pass_attemps, pass_completed, pass_received, yards, games, passed, receiver, generic, receiving_avg, generic_avg, scored) VALUES (3, 0, 'Abel Dennis', '1974-04-07', 'TE', 13, null, null, 32, 981, 92,false,false,false, 0, 0,false);
INSERT INTO player230 (id, version, name, date_of_birth, position, seasons, pass_attemps, pass_completed, pass_received, yards, games, passed, receiver, generic, receiving_avg, generic_avg, scored) VALUES (4, 0, 'Earl Russel', '1988-11-01', 'RB', 5, null, null, 64, 432, 32,false,false,false, 0, 0,false);
INSERT INTO player230 (id, version, name, date_of_birth, position, seasons, pass_attemps, pass_completed, pass_received, yards, games, passed, receiver, generic, receiving_avg, generic_avg, scored) VALUES (5, 0, 'Nick Garner', '1986-01-28', 'G', 3, null, null, 51, 769, 12,false,false,false, 0, 0,false);
INSERT INTO player230 (id, version, name, date_of_birth, position, seasons, pass_attemps, pass_completed, pass_received, yards, games, passed, receiver, generic, receiving_avg, generic_avg, scored) VALUES (6, 0, 'Howard Ramos', '1976-07-19', 'T', 8, null, null, 12, 1241, 122,false,false,false, 0, 0,true);
INSERT INTO player230 (id, version, name, date_of_birth, position, seasons, pass_attemps, pass_completed, pass_received, yards, games, passed, receiver, generic, receiving_avg, generic_avg, scored) VALUES (7, 0, 'Allan Meyer', '1992-02-12', 'C', 1, null, null, 89, 1422, 51,false,false,false, 0, 0,false);
INSERT INTO player230 (id, version, name, date_of_birth, position, seasons, pass_attemps, pass_completed, pass_received, yards, games, passed, receiver, generic, receiving_avg, generic_avg, scored) VALUES (8, 0, 'Ruben Pope', '1982-07-21', 'QB', 7, 97, 69, 32, 509, 33,false,false,false, 0, 0,true);
INSERT INTO player230 (id, version, name, date_of_birth, position, seasons, pass_attemps, pass_completed, pass_received, yards, games, passed, receiver, generic, receiving_avg, generic_avg, scored) VALUES (9, 0, 'Lester Cross', '1989-12-04', 'WR', 11, null, null, 14, 989, 44,false,false,false, 0, 0,true);
INSERT INTO player230 (id, version, name, date_of_birth, position, seasons, pass_attemps, pass_completed, pass_received, yards, games, passed, receiver, generic, receiving_avg, generic_avg, scored) VALUES (10, 0, 'Keith Barnett', '1990-11-22', null, 4, null, null, 67, 1053, 99,false,false,false, 0, 0,false);

ALTER TABLE player230 ADD CONSTRAINT player230_pkey PRIMARY KEY (id);
