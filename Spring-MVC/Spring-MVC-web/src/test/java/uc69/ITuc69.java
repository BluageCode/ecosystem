package uc69;


import com.thoughtworks.selenium.Selenium;
import com.thoughtworks.selenium.SeleneseTestCase;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverBackedSelenium;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import test.common.AbstractBADocTestCase;
import test.common.Tools;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.regex.Pattern;

public class ITuc69 extends AbstractBADocTestCase {
	
	@Override
	public  String getName(){
		return "UC-69";
	}


	@Test
	public void testUC69() throws Exception {
		// On teste l'UC69 - Standard Features/Entities - Second Level Cache
	
		selenium.open("");	
		selenium.click("link=Expand All");
		selenium.waitForPageToLoad("30000");
		selenium.click("link=UC69 - Hibernate Cache");
		verifyTrue(selenium.isElementPresent("//h2[contains(text(),\"Standard Features > Entities > UC69 > Hibernate Cache\")]"));
		// On v�rifie les donn�es du tableau.
		assertTrue(selenium.isElementPresent("//table[contains(@id,'listTeam')]/tbody/tr[td/span[contains(text(),\"Tigers\")] and td/span[contains(text(),\"Charlotte\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'listTeam')]/tbody//tr[td//span[contains(text(),\"Kansas City\")] and td//span[contains(text(),\"Bears\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'listTeam')]/tbody//tr[td//span[contains(text(),\"Detroit\")] and td//span[contains(text(),\"Chargers\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'listTeam')]/tbody//tr[td//span[contains(text(),\"Detroit\")] and td//span[contains(text(),\"Hawks\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'listTeam')]/tbody//tr[td//span[contains(text(),\"Omaha\")] and td//span[contains(text(),\"Braves\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'listTeam')]/tbody//tr[td//span[contains(text(),\"Raleigh\")] and td//span[contains(text(),\"Eagles\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'listTeam')]/tbody//tr[td//span[contains(text(),\"Oakland\")] and td//span[contains(text(),\"Cardinals\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'listTeam')]/tbody//tr[td//span[contains(text(),\"Kansas City\")] and td//span[contains(text(),\"Red Devils\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'listTeam')]/tbody//tr[td//span[contains(text(),\"Fresno\")] and td//span[contains(text(),\"Mustangs\")]]"));
		// On index chaque ligne, puis on v�rifie les details.
		// Pour les Tigers Charlotte :
		Number indexTigersCharlotte = selenium.getElementIndex("//table[contains(@id,'listTeam')]/tbody/tr[td/span[contains(text(),\"Tigers\")] and td/span[contains(text(),\"Charlotte\")]]");
		selenium.click("//table[contains(@id,'listTeam')]/tbody/tr[" + indexTigersCharlotte + " + 1]//td//a[contains(text(),\"[details]\")]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie que la page s'affiche correctement.
		assertTrue(selenium.isElementPresent("//span[contains(@id,'txt_TeamName')]"));
		assertEquals("Tigers", selenium.getText("//span[contains(@id,'txt_TeamName')]"));
		assertTrue(selenium.isElementPresent("//span[contains(@id,'txt_Teamcity')]"));
		assertEquals("Charlotte", selenium.getText("//span[contains(@id,'txt_Teamcity')]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Oliver\")] and td//span[contains(text(),\"Frank\")] and td//span[contains(text(),\"10/31/1968\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Lester\")] and td//span[contains(text(),\"Cross\")] and td//span[contains(text(),\"02/21/1976\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Ivan\")] and td//span[contains(text(),\"Tate\")] and td//span[contains(text(),\"02/08/1972\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Julius\")] and td//span[contains(text(),\"Byrd\")] and td//span[contains(text(),\"09/13/1968\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Raul\")] and td//span[contains(text(),\"Weaver\")] and td//span[contains(text(),\"02/21/1967\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Jaime\")] and td//span[contains(text(),\"Mccormick\")] and td//span[contains(text(),\"10/27/1969\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Omar\")] and td//span[contains(text(),\"Gordon\")] and td//span[contains(text(),\"12/23/1981\")]]"));
		// On retourne sur la page du tableau Teams :
		assertEquals("[back]", selenium.getText("//a[contains(@id,'lnk_back')]"));
		selenium.click("//a[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
		// Pour les Bears Kansas City :
		Number indexBearsKansasCity = selenium.getElementIndex("//table[contains(@id,'listTeam')]/tbody/tr[td/span[contains(text(),\"Bears\")] and td/span[contains(text(),\"Kansas City\")]]");
		selenium.click("//table[contains(@id,'listTeam')]/tbody/tr[" + indexBearsKansasCity + " + 1]//td//a[contains(text(),\"[details]\")]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie que la page s'affiche correctement.
		assertTrue(selenium.isElementPresent("//span[contains(@id,'txt_TeamName')]"));
		assertEquals("Bears", selenium.getText("//span[contains(@id,'txt_TeamName')]"));
		assertTrue(selenium.isElementPresent("//span[contains(@id,'txt_Teamcity')]"));
		assertEquals("Kansas City", selenium.getText("//span[contains(@id,'txt_Teamcity')]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Elbert\")] and td//span[contains(text(),\"Cole\")] and td//span[contains(text(),\"04/07/1965\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Keith\")] and td//span[contains(text(),\"Barnett\")] and td//span[contains(text(),\"11/10/1976\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Ed\")] and td//span[contains(text(),\"Drake\")] and td//span[contains(text(),\"11/04/1972\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Dan\")] and td//span[contains(text(),\"Briggs\")] and td//span[contains(text(),\"05/05/1986\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Rex\")] and td//span[contains(text(),\"Lee\")] and td//span[contains(text(),\"05/11/1978\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Ronnie\")] and td//span[contains(text(),\"Scott\")] and td//span[contains(text(),\"02/16/1965\")]]"));
		// On retourne sur la page du tableau Teams :
		assertEquals("[back]", selenium.getText("//a[contains(@id,'lnk_back')]"));
		selenium.click("//a[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
		// Pour les Chargers Detroit :
		Number indexChargersDetroit = selenium.getElementIndex("//table[contains(@id,'listTeam')]/tbody/tr[td/span[contains(text(),\"Chargers\")] and td/span[contains(text(),\"Detroit\")]]");
		selenium.click("//table[contains(@id,'listTeam')]/tbody/tr[" + indexChargersDetroit + " + 1]//td//a[contains(text(),\"[details]\")]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie que la page s'affiche correctement.
		assertTrue(selenium.isElementPresent("//span[contains(@id,'txt_TeamName')]"));
		assertEquals("Chargers", selenium.getText("//span[contains(@id,'txt_TeamName')]"));
		assertTrue(selenium.isElementPresent("//span[contains(@id,'txt_Teamcity')]"));
		assertEquals("Detroit", selenium.getText("//span[contains(@id,'txt_Teamcity')]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Abel\")] and td//span[contains(text(),\"Dennis\")] and td//span[contains(text(),\"04/16/1980\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Tommy\")] and td//span[contains(text(),\"Wood\")] and td//span[contains(text(),\"01/20/1992\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Clyde\")] and td//span[contains(text(),\"Chambers\")] and td//span[contains(text(),\"08/01/1976\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Dave\")] and td//span[contains(text(),\"Pratt\")] and td//span[contains(text(),\"09/19/1975\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Malcolm\")] and td//span[contains(text(),\"Chavez\")] and td//span[contains(text(),\"07/09/1973\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Corey\")] and td//span[contains(text(),\"Rice\")] and td//span[contains(text(),\"03/20/1967\")]]"));
		// On retourne sur la page du tableau Teams :
		assertEquals("[back]", selenium.getText("//a[contains(@id,'lnk_back')]"));
		selenium.click("//a[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
		// Pour les Hawks Detroit :
		Number indexHawksDetroit = selenium.getElementIndex("//table[contains(@id,'listTeam')]/tbody/tr[td/span[contains(text(),\"Hawks\")] and td/span[contains(text(),\"Detroit\")]]");
		selenium.click("//table[contains(@id,'listTeam')]/tbody/tr[" + indexHawksDetroit + " + 1]//td//a[contains(text(),\"[details]\")]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie que la page s'affiche correctement.
		assertTrue(selenium.isElementPresent("//span[contains(@id,'txt_TeamName')]"));
		assertEquals("Hawks", selenium.getText("//span[contains(@id,'txt_TeamName')]"));
		assertTrue(selenium.isElementPresent("//span[contains(@id,'txt_Teamcity')]"));
		assertEquals("Detroit", selenium.getText("//span[contains(@id,'txt_Teamcity')]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Ricky\")] and td//span[contains(text(),\"Dawson\")] and td//span[contains(text(),\"03/03/1990\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Sylvester\")] and td//span[contains(text(),\"Nash\")] and td//span[contains(text(),\"07/10/1965\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Oliver\")] and td//span[contains(text(),\"Ballard\")] and td//span[contains(text(),\"07/18/1972\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Dale\")] and td//span[contains(text(),\"Rhodes\")] and td//span[contains(text(),\"10/12/1966\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Martin\")] and td//span[contains(text(),\"Boyd\")] and td//span[contains(text(),\"02/16/1965\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Everett\")] and td//span[contains(text(),\"Fields\")] and td//span[contains(text(),\"03/27/1976\")]]"));
		// On retourne sur la page du tableau Teams :
		assertEquals("[back]", selenium.getText("//a[contains(@id,'lnk_back')]"));
		selenium.click("//a[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
		// Pour les Braves Omaha :
		Number indexBravesOmaha = selenium.getElementIndex("//table[contains(@id,'listTeam')]/tbody/tr[td/span[contains(text(),\"Braves\")] and td/span[contains(text(),\"Omaha\")]]");
		selenium.click("//table[contains(@id,'listTeam')]/tbody/tr[" + indexBravesOmaha + " + 1]//td//a[contains(text(),\"[details]\")]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie que la page s'affiche correctement.
		assertTrue(selenium.isElementPresent("//span[contains(@id,'txt_TeamName')]"));
		assertEquals("Braves", selenium.getText("//span[contains(@id,'txt_TeamName')]"));
		assertTrue(selenium.isElementPresent("//span[contains(@id,'txt_Teamcity')]"));
		assertEquals("Omaha", selenium.getText("//span[contains(@id,'txt_Teamcity')]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Earl\")] and td//span[contains(text(),\"Russell\")] and td//span[contains(text(),\"12/15/1981\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Michael\")] and td//span[contains(text(),\"Hodges\")] and td//span[contains(text(),\"04/01/1988\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Myron\")] and td//span[contains(text(),\"Clarke\")] and td//span[contains(text(),\"12/31/1966\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Marvin\")] and td//span[contains(text(),\"Vaughn\")] and td//span[contains(text(),\"07/08/1982\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Joey\")] and td//span[contains(text(),\"Roberson\")] and td//span[contains(text(),\"12/18/1985\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Timothy\")] and td//span[contains(text(),\"Payne\")] and td//span[contains(text(),\"02/25/1987\")]]"));
		// On retourne sur la page du tableau Teams :
		assertEquals("[back]", selenium.getText("//a[contains(@id,'lnk_back')]"));
		selenium.click("//a[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
		// Pour les Eagles Raleigh
		Number indexEaglesRaleigh = selenium.getElementIndex("//table[contains(@id,'listTeam')]/tbody/tr[td/span[contains(text(),\"Eagles\")] and td/span[contains(text(),\"Raleigh\")]]");
		selenium.click("//table[contains(@id,'listTeam')]/tbody/tr[" + indexEaglesRaleigh + " + 1]//td//a[contains(text(),\"[details]\")]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie que la page s'affiche correctement.
		assertTrue(selenium.isElementPresent("//span[contains(@id,'txt_TeamName')]"));
		assertEquals("Eagles", selenium.getText("//span[contains(@id,'txt_TeamName')]"));
		assertTrue(selenium.isElementPresent("//span[contains(@id,'txt_Teamcity')]"));
		assertEquals("Raleigh", selenium.getText("//span[contains(@id,'txt_Teamcity')]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Nick\")] and td//span[contains(text(),\"Garner\")] and td//span[contains(text(),\"07/27/1992\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Jerome\")] and td//span[contains(text(),\"Grant\")] and td//span[contains(text(),\"06/28/1982\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Carroll\")] and td//span[contains(text(),\"Shaw\")] and td//span[contains(text(),\"03/07/1987\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Jay\")] and td//span[contains(text(),\"Owens\")] and td//span[contains(text(),\"06/18/1976\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Enrique\")] and td//span[contains(text(),\"Young\")] and td//span[contains(text(),\"07/26/1976\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Darin\")] and td//span[contains(text(),\"Lambert\")] and td//span[contains(text(),\"12/10/1986\")]]"));
		// On retourne sur la page du tableau Teams :
		assertEquals("[back]", selenium.getText("//a[contains(@id,'lnk_back')]"));
		selenium.click("//a[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
		// Pour les Cardinals Oakland :
		Number indexCardinalsOakland = selenium.getElementIndex("//table[contains(@id,'listTeam')]/tbody/tr[td/span[contains(text(),\"Cardinals\")] and td/span[contains(text(),\"Oakland\")]]");
		selenium.click("//table[contains(@id,'listTeam')]/tbody/tr[" + indexCardinalsOakland + " + 1]//td//a[contains(text(),\"[details]\")]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie que la page s'affiche correctement.
		assertTrue(selenium.isElementPresent("//span[contains(@id,'txt_TeamName')]"));
		assertEquals("Cardinals", selenium.getText("//span[contains(@id,'txt_TeamName')]"));
		assertTrue(selenium.isElementPresent("//span[contains(@id,'txt_Teamcity')]"));
		assertEquals("Oakland", selenium.getText("//span[contains(@id,'txt_Teamcity')]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Howard\")] and td//span[contains(text(),\"Ramos\")] and td//span[contains(text(),\"08/25/1970\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Woodrow\")] and td//span[contains(text(),\"Carpenter\")] and td//span[contains(text(),\"08/08/1987\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Phil\")] and td//span[contains(text(),\"Christensen\")] and td//span[contains(text(),\"02/23/1989\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Wendell\")] and td//span[contains(text(),\"Page\")] and td//span[contains(text(),\"12/12/1988\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Jake\")] and td//span[contains(text(),\"Lane\")] and td//span[contains(text(),\"10/13/1971\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Lance\")] and td//span[contains(text(),\"Mitchell\")] and td//span[contains(text(),\"02/07/1985\")]]"));
		// On retourne sur la page du tableau Teams :
		assertEquals("[back]", selenium.getText("//a[contains(@id,'lnk_back')]"));
		selenium.click("//a[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
		// Pour les Red Devils Kansas City :
		Number indexRedDevilsKansasCity = selenium.getElementIndex("//table[contains(@id,'listTeam')]/tbody/tr[td/span[contains(text(),\"Red Devils\")] and td/span[contains(text(),\"Kansas City\")]]");
		selenium.click("//table[contains(@id,'listTeam')]/tbody/tr[" + indexRedDevilsKansasCity + " + 1]//td//a[contains(text(),\"[details]\")]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie que la page s'affiche correctement.
		assertTrue(selenium.isElementPresent("//span[contains(@id,'txt_TeamName')]"));
		assertEquals("Red Devils", selenium.getText("//span[contains(@id,'txt_TeamName')]"));
		assertTrue(selenium.isElementPresent("//span[contains(@id,'txt_Teamcity')]"));
		assertEquals("Kansas City", selenium.getText("//span[contains(@id,'txt_Teamcity')]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Allan\")] and td//span[contains(text(),\"Meyer\")] and td//span[contains(text(),\"02/14/1969\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Charlie\")] and td//span[contains(text(),\"Clark\")] and td//span[contains(text(),\"08/31/1971\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Bob\")] and td//span[contains(text(),\"Fleming\")] and td//span[contains(text(),\"01/16/1972\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Leonard\")] and td//span[contains(text(),\"Chapman\")] and td//span[contains(text(),\"11/14/1988\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Preston\")] and td//span[contains(text(),\"Brown\")] and td//span[contains(text(),\"04/08/1969\")]]"));
		// On retourne sur la page du tableau Teams :
		assertEquals("[back]", selenium.getText("//a[contains(@id,'lnk_back')]"));
		selenium.click("//a[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
		// Pour les Mustangs Fresno :
		Number indexMustangsFresno = selenium.getElementIndex("//table[contains(@id,'listTeam')]/tbody/tr[td/span[contains(text(),\"Mustangs\")] and td/span[contains(text(),\"Fresno\")]]");
		selenium.click("//table[contains(@id,'listTeam')]/tbody/tr[" + indexMustangsFresno + " + 1]//td//a[contains(text(),\"[details]\")]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie que la page s'affiche correctement.
		assertTrue(selenium.isElementPresent("//span[contains(@id,'txt_TeamName')]"));
		assertEquals("Mustangs", selenium.getText("//span[contains(@id,'txt_TeamName')]"));
		assertTrue(selenium.isElementPresent("//span[contains(@id,'txt_Teamcity')]"));
		assertEquals("Fresno", selenium.getText("//span[contains(@id,'txt_Teamcity')]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Ruben\")] and td//span[contains(text(),\"Pope\")] and td//span[contains(text(),\"05/01/1985\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Marshall\")] and td//span[contains(text(),\"Norman\")] and td//span[contains(text(),\"06/25/1965\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Perry\")] and td//span[contains(text(),\"Vega\")] and td//span[contains(text(),\"08/29/1975\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Doyle\")] and td//span[contains(text(),\"Clarke\")] and td//span[contains(text(),\"09/01/1982\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Rex\")] and td//span[contains(text(),\"Daniel\")] and td//span[contains(text(),\"11/20/1983\")]]"));
		// On retourne sur la page du tableau Teams :
		assertEquals("[back]", selenium.getText("//a[contains(@id,'lnk_back')]"));
		selenium.click("//a[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
	}


}
