/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.service.standardfeatures.services.ajax.uc50_ajaxcall.daofinder;


import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.Logger;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateTemplate;

import com.netfective.bluage.core.exception.ApplicationException;


/**
 * This class represents query invocation methods implementation
 * for ServiceAjaxCall service.
 *
 * @see com.bluage.documentation.service.standardfeatures.services.ajax.uc50_ajaxcall.ServiceAjaxCall
 */
 
@Named ("com.bluage.documentation.service.standardfeatures.services.ajax.uc50_ajaxcall.daofinder.ServiceAjaxCallDAOMethodFinderImpl")
public class ServiceAjaxCallDAOMethodFinderImpl {

	// LOGGER for ServiceAjaxCallDAOMethodFinderImpl
	private static final Logger LOGGER = Logger.getLogger(ServiceAjaxCallDAOMethodFinderImpl.class);
	
	/**
	* Hibernate Template (injected)
	*/
	@Inject
	private HibernateTemplate hibernateTemplate;

	
	/**
	 * setter for HibernateTemplate
	 * @param value : {@link HibernateTemplate}
	 */
	public void setHibernateTemplate( HibernateTemplate value )
	{
		hibernateTemplate = value;
	}
	
	
	/**    
	 * HQL operation.
	 * @param name {@link String}
		 * @param value {@link Float}
		 * @param rook {@link Boolean}
		 * @return {@link List}
	 */
	public List findPlayers50(String name,Float value,Boolean rook )
 throws ApplicationException {
Session session = hibernateTemplate.getSessionFactory().getCurrentSession();
			return 
				privateFindPlayers50(name,value,rook, session);
	}

	
	/**    
	 * private delegation for HQL operation.
	 * @param name {@link String}
		 * @param value {@link Float}
		 * @param rook {@link Boolean}
		 * @return {@link List}
	 */
	private List privateFindPlayers50(String name,Float value,Boolean rook , Session session)
 throws ApplicationException {
			Query query = session.createQuery(" SELECT p FROM Player50BO p INNER JOIN p.team t where t.name = :name AND p.rookie = :rook AND p.estimatedValue > :value ");
query.setParameter( "name", name, Hibernate.STRING);
											query.setParameter( "value", value, Hibernate.FLOAT);
											query.setParameter( "rook", rook, Hibernate.BOOLEAN);
														List returnValue = null;
		try {
			returnValue = query.list();
							} catch(HibernateException ex) {
			LOGGER.error("An error occured while excecuting the HQL operation : com.bluage.documentation.service.standardfeatures.services.ajax.uc50_ajaxcall.findPlayers50");
			throw new ApplicationException(ex);
		}
		return returnValue;
	}

	




}
