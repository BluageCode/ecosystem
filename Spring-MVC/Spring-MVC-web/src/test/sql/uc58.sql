--<ScriptOptions statementTerminator=";"/>

ALTER TABLE team58 DROP CONSTRAINT team58_state_fkc;

ALTER TABLE player58 DROP CONSTRAINT player58_team58_fkc;

ALTER TABLE player58 DROP CONSTRAINT player58_team_fkc;

ALTER TABLE player58 DROP CONSTRAINT player58_pkey;

ALTER TABLE team58 DROP CONSTRAINT team58_pkey;

ALTER TABLE state58 DROP CONSTRAINT state58_pkey;

DROP TABLE team58;

DROP TABLE state58;

DROP TABLE player58;

CREATE TABLE team58 (
		id INT8 NOT NULL,
		version INT4 NOT NULL,
		city VARCHAR(1024),
		name VARCHAR(1024),
		state_fk VARCHAR(2)
	);

CREATE TABLE state58 (
		code VARCHAR(2) NOT NULL,
		version INT4 NOT NULL,
		name VARCHAR(20) NOT NULL
	);

CREATE TABLE player58 (
		id INT8 NOT NULL,
		version INT4 NOT NULL,
		first_name VARCHAR(30) NOT NULL,
		last_name VARCHAR(30) NOT NULL,
		date_of_birth DATE,
		estimated_value FLOAT4,
		weight INT4,
		height INT4,
		rookie BOOL,
		team_fk INT8 NOT NULL,
		team58_fk INT8
	);
	
INSERT INTO state58 (code, version, name) VALUES ('AK', 0, 'Alaska');
INSERT INTO state58 (code, version, name) VALUES ('AL', 0, 'Alabama');
INSERT INTO state58 (code, version, name) VALUES ('AR', 0, 'Arkansas');
INSERT INTO state58 (code, version, name) VALUES ('AZ', 0, 'Arizona');
INSERT INTO state58 (code, version, name) VALUES ('CA', 0, 'California');
INSERT INTO state58 (code, version, name) VALUES ('CO', 0, 'Colorado');
INSERT INTO state58 (code, version, name) VALUES ('CT', 0, 'Connecticut');
INSERT INTO state58 (code, version, name) VALUES ('DE', 0, 'Delaware');
INSERT INTO state58 (code, version, name) VALUES ('FL', 0, 'Florida');
INSERT INTO state58 (code, version, name) VALUES ('GA', 0, 'Georgia');

INSERT INTO team58 (id, version, city, name, state_fk) VALUES (5, 1, 'Omahasss', 'Braves', 'CA');
INSERT INTO team58 (id, version, city, name, state_fk) VALUES (1, 4, 'Charlotte', 'Tigers', 'AK');
INSERT INTO team58 (id, version, city, name, state_fk) VALUES (4, 11, 'Detroit', 'Hawksdee', 'DE');
INSERT INTO team58 (id, version, city, name, state_fk) VALUES (2, 3, 'Kansas City', 'Bears', 'CA');
INSERT INTO team58 (id, version, city, name, state_fk) VALUES (3, 3, 'Detroit', 'Chargers', 'FL');

INSERT INTO player58 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, team58_fk) VALUES (201, 0, 'Oliver', 'Frank', '1968-10-31', 0.11, 116, 228, false, 1, NULL);
INSERT INTO player58 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, team58_fk) VALUES (202, 0, 'Elbert', 'Cole', '1965-04-07', 3.95, 100, 171, false, 2, NULL);
INSERT INTO player58 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, team58_fk) VALUES (203, 0, 'Abel', 'Dennis', '1980-04-16', 30.51, 105, 208, false, 3, NULL);
INSERT INTO player58 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, team58_fk) VALUES (204, 0, 'Ricky', 'Dawson', '1990-03-03', 29.299999, 115, 168, true, 4, NULL);
INSERT INTO player58 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, team58_fk) VALUES (205, 0, 'Earl', 'Russell', '1981-12-15', 44.73, 60, 234, false, 5, NULL);
INSERT INTO player58 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, team58_fk) VALUES (206, 0, 'Nick', 'Garner', '1992-07-27', 47.84, 87, 221, true, 5, NULL);
INSERT INTO player58 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, team58_fk) VALUES (207, 0, 'Howard', 'Ramos', '1970-08-25', 10.76, 107, 199, false, 4, NULL);
INSERT INTO player58 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, team58_fk) VALUES (212, 0, 'Ivan', 'Tate', '1972-02-08', 48.490002, 143, 175, false, 1, NULL);
INSERT INTO player58 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, team58_fk) VALUES (213, 0, 'Tommy', 'Wood', '1992-01-20', 13.93, 77, 173, true, 3, NULL);
INSERT INTO player58 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, team58_fk) VALUES (214, 0, 'Sylvester', 'Nash', '1965-07-10', 37.73, 68, 184, false, 4, NULL);
INSERT INTO player58 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, team58_fk) VALUES (215, 0, 'Michael', 'Hodges', '1988-04-01', 25.35, 158, 204, false, 5, NULL);
INSERT INTO player58 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, team58_fk) VALUES (216, 0, 'Jerome', 'Grant', '1982-06-28', 28.940001, 105, 205, false, 4, NULL);
INSERT INTO player58 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, team58_fk) VALUES (217, 0, 'Woodrow', 'Carpenter', '1987-08-08', 41.759998, 69, 224, false, 3, NULL);



ALTER TABLE player58 ADD CONSTRAINT player58_pkey PRIMARY KEY (id);

ALTER TABLE team58 ADD CONSTRAINT team58_pkey PRIMARY KEY (id);

ALTER TABLE state58 ADD CONSTRAINT state58_pkey PRIMARY KEY (code);

ALTER TABLE team58 ADD CONSTRAINT team58_state_fkc FOREIGN KEY (state_fk)
	REFERENCES state58 (code);

ALTER TABLE player58 ADD CONSTRAINT player58_team58_fkc FOREIGN KEY (team58_fk)
	REFERENCES team58 (id);

ALTER TABLE player58 ADD CONSTRAINT player58_team_fkc FOREIGN KEY (team_fk)
	REFERENCES team58 (id);

