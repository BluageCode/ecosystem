/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.business.standardfeatures.application.datagrid.uc32_calculabletable.bos;

import java.util.Date;

import javax.inject.Inject;
import javax.inject.Named;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import com.bluage.documentation.business.common.bos.PersonBO;
import com.bluage.documentation.business.standardfeatures.application.datagrid.uc32_calculabletable.entities.daofinder.Player32DAOFinderImpl;

/**
 * Player32BO
 *
* @see com.netfective.bluage.core.business.IBusinessObject
* * @see PersonBO
 */
@XmlRootElement

public class Player32BO
 extends PersonBO {
	
	/**
	 * Default SUID for Player32BO.
	 */
	private static final long serialVersionUID = 1L;
	

	@Inject
	@Named ("com.bluage.documentation.business.standardfeatures.application.datagrid.uc32_calculabletable.entities.daofinder.Player32Finder") 
	private Player32DAOFinderImpl finder;

						/**
	 * Entity property: dateOfBirth
	 *
	 */
private Date dateOfBirth;

					/**
	 * Entity property: estimatedValue
	 *
	 */
private Float estimatedValue;

					/**
	 * Entity property: playedGames
	 *
	 */
private Integer playedGames;

					/**
	 * Entity property: numberOfBaskets
	 *
	 */
private Integer numberOfBaskets;

					/**
	 * Entity property: rookie
	 *
	 */
private Boolean rookie;

											/**
	 * Entity property: position
	 *
	 */
private Position32BO position;

					/**
	 * Entity property: efficacy
	 *
	 */
private float efficacy;



   /**
	* public default constructor for class Player32BO
	*/
	public Player32BO(){
		//NOP Player32BO
	}


						/**
	 * Getter for dateOfBirth of class Player32BO.
	 *
	 * @return {@link java.util.Date}
	 * @generated
	 */ 
public  Date getDateOfBirth() {
	return this.dateOfBirth;
	}


	/**
	 * Setter for dateOfBirth of class Player32BO.
	 * @param value {@link java.util.Date}
	 * @generated
	 */

public  void setDateOfBirth(final Date dateOfBirth) {
	this.dateOfBirth = dateOfBirth;
}

					/**
	 * Getter for estimatedValue of class Player32BO.
	 *
	 * @return {@link java.lang.Float}
	 * @generated
	 */ 
public  Float getEstimatedValue() {
	return this.estimatedValue;
	}


	/**
	 * Setter for estimatedValue of class Player32BO.
	 * @param value {@link java.lang.Float}
	 * @generated
	 */

public  void setEstimatedValue(final Float estimatedValue) {
	this.estimatedValue = estimatedValue;
}

					/**
	 * Getter for playedGames of class Player32BO.
	 *
	 * @return {@link java.lang.Integer}
	 * @generated
	 */ 
public  Integer getPlayedGames() {
	return this.playedGames;
	}


	/**
	 * Setter for playedGames of class Player32BO.
	 * @param value {@link java.lang.Integer}
	 * @generated
	 */

public  void setPlayedGames(final Integer playedGames) {
	this.playedGames = playedGames;
}

					/**
	 * Getter for numberOfBaskets of class Player32BO.
	 *
	 * @return {@link java.lang.Integer}
	 * @generated
	 */ 
public  Integer getNumberOfBaskets() {
	return this.numberOfBaskets;
	}


	/**
	 * Setter for numberOfBaskets of class Player32BO.
	 * @param value {@link java.lang.Integer}
	 * @generated
	 */

public  void setNumberOfBaskets(final Integer numberOfBaskets) {
	this.numberOfBaskets = numberOfBaskets;
}

					/**
	 * Getter for rookie of class Player32BO.
	 *
	 * @return {@link java.lang.Boolean}
	 * @generated
	 */ 
public  Boolean getRookie() {
	return this.rookie;
	}


	/**
	 * Setter for rookie of class Player32BO.
	 * @param value {@link java.lang.Boolean}
	 * @generated
	 */

public  void setRookie(final Boolean rookie) {
	this.rookie = rookie;
}

											/**
	 * Getter for position of class Player32BO.
	 *
	 * @return {@link com.bluage.documentation.business.standardfeatures.application.datagrid.uc32_calculabletable.bos.Position32BO}
	 * @generated
	 */ 
public  Position32BO getPosition() {
		Position32BO position0 = this.position;
		if(this.position == null) {
			if(DAO_MODE.equals(this.getObjMode( ) ) ) {
				position0 = null;
			} else {
				this.position = new Position32BO();
				this.position.setObjMode( this.getObjMode( ) );
				position0 = this.position;
			}
		} else {
			this.position.setObjMode( this.getObjMode( ) );
			if( HELPER_MODE.equals(this.getObjMode( ) ) ) {
				position0 = this.position;
			}
			if( DAO_MODE.equals(this.getObjMode()) && this.position.isEmpty ) {
				position0 = null;
			}
		}
		return position0;
	}


	/**
	 * Setter for position of class Player32BO.
	 * @param value {@link com.bluage.documentation.business.standardfeatures.application.datagrid.uc32_calculabletable.bos.Position32BO}
	 * @generated
	 */

public  void setPosition(final Position32BO position) {
	if(position != null) {
		position.isEmpty = false;
		position.setObjMode( this.getObjMode( ) );
	}
	this.isEmpty = false;
	this.position = position;
}

					/**
	 * Getter for efficacy of class Player32BO.
	 *
	 * @return {@link float}
	 * @generated
	 */ 
public  float getEfficacy() {
	return this.efficacy;
	}


	/**
	 * Setter for efficacy of class Player32BO.
	 * @param value {@link float}
	 * @generated
	 */

public  void setEfficacy(final float efficacy) {
	this.efficacy = efficacy;
}


/**
	 * Returns <code>true</code> if the argument is an Player32 instance and all identifiers for this entity
     * equal the identifiers of the argument entity. The <code>equals</code> method of the parent entity
     * will also need to return <code>true</code>. Returns <code>false</code> otherwise.
     *
     * @see PersonBO#hashCode()
	 * @param object  object to compare with current instance      
     * @return boolean true if equals false if not
     */
    public boolean equals(final Object object )
    {
		if ( this == object )
        {
            return true;
        }
        if ( !( object instanceof Player32BO ) )
        {
            return false;
        }
        final Player32BO that = (Player32BO)object;
    
    	            if (        				        			this.getId() == null
        	        ) {
        	return false;
        }
		return new EqualsBuilder()
			.append(this.getId(), that.getId())
			 	.isEquals();
	}
	/**

	 * Returns a hash code based on this entity's identifiers and the hash code of the parent entity.
     *
     * @see PersonBO#hashCode()
     * @return int
     */    
    public int hashCode() {
        return new HashCodeBuilder()
             .append(getId())
				 	.appendSuper(super.hashCode())
            .toHashCode();
    }
}

