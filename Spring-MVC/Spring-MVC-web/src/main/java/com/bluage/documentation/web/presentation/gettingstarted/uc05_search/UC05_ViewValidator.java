/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.gettingstarted.uc05_search ;


import org.apache.log4j.Logger;
import org.springframework.validation.Errors;

import com.netfective.bluage.core.validator.AbstractValidator;
/**
* Class :UC05_ViewValidator
*/
public class UC05_ViewValidator extends AbstractValidator{
	
	// LOGGER for the class UC05_ViewValidator
	private static final Logger LOGGER = Logger.getLogger( UC05_ViewValidator.class);
	
	
	/**
	* Operation validate for UC05_ViewForm
	* @param obj : the current form (UC05_ViewForm)
	* @param errors : The spring errors to return for the form UC05_ViewForm
	*/
	public void validate(final Object obj,final Errors errors){
		LOGGER.info("Beginning method : validate"); // UC05_ViewValidator
		UC05_ViewForm cUC05_ViewForm = (UC05_ViewForm)obj; // UC05_ViewValidator
		LOGGER.info("Ending method : validate the form "+ cUC05_ViewForm.getClass().getName()); // UC05_ViewValidator
	}

	/**
	* Method to implements to use spring validators (UC05_ViewForm)
	* @param aClass : Class for the form UC05_ViewForm
	* @return boolean
	*/
	public boolean supports(final Class aClass){
		return (new UC05_ViewForm()).getClass().equals(aClass); // UC05_ViewValidator
	}
}
