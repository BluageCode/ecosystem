/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.service.gettingstarted.uc03_formvalidation;


import javax.inject.Inject;
import javax.inject.Named;

import com.bluage.documentation.business.gettingstarted.uc03_formvalidation.bos.Position03BO;
import com.bluage.documentation.business.gettingstarted.uc03_formvalidation.entities.daofinder.Position03DAOFinderImpl;
import com.bluage.documentation.service.AbstractService;
import com.netfective.bluage.core.exception.ApplicationException;

/**
 * Defines  application services for ServicePosition03
  */
@Named("com.bluage.documentation.service.gettingstarted.uc03_formvalidation.ServicePosition03")
public class ServicePosition03  extends AbstractService{


			@Inject
		@Named ("com.bluage.documentation.business.gettingstarted.uc03_formvalidation.entities.daofinder.Position03DAOFinderImpl")
		private Position03DAOFinderImpl position03BOFinder;
	


	/**
	 *  Default constructor 
	 */
	public ServicePosition03() {
		super(); // Call super for default constructor : ServicePosition03
	}
	


			
				
	/**    
	 * Operation Crud
	 * @param bo {@link Position03BO}
		 * @return {@link Position03BO}
	 */
	public Position03BO position03FindByID(Position03BO bo )
 throws ApplicationException {

					return (Position03BO)position03BOFinder.findByPrimaryKey(getBoId(bo));
	}
	/**
	 *  Return the primary key from business object.
	 */
	private Object getBoId(Position03BO bo) {
				return bo.getCode();
		}

		}
