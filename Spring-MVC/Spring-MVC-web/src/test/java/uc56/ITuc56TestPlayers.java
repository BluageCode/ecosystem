package uc56;


import org.junit.Test;
import test.common.AbstractBADocTestCase;

public class ITuc56TestPlayers extends AbstractBADocTestCase {

	@Override
	public  String getName(){
		return "UC-56";
	}	

	@Test
	public void iTuc56testPlayers() throws Exception {
		// On teste l'UC56 - Standard Features/Services/SQL - SQL Operation.
			
		selenium.open("");	
		selenium.click("link=Expand All");
		selenium.waitForPageToLoad("30000");
		selenium.click("link=UC56 - SQL Operation");
		
		verifyTrue(selenium.isElementPresent("//h2[contains(text(),\"Standard Features > Services > SQL > UC56 > SQL Operation\")]"));
		verifyTrue(selenium.isElementPresent("//h4[contains(text(),'Regular results')]"));
		verifyTrue(selenium.isElementPresent("//h4[contains(text(),'Post processed results')]"));
		// On affiche toutes les teams.
		selenium.type("//*[contains(@id,'txt_name')]", "");
		selenium.click("//a[contains(@id,'lnk_searchTeams')]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie les joueurs pour chaque team.
		// On indexe la ligne de chaque team.
		Number indexTigers = selenium.getElementIndex("//table[contains(@id,'tab_searchResult')]/tbody/tr[td/span[contains(text(),\"Tigers\")] and td/span[contains(text(),\"Charlotte\")]]");
		selenium.click("//table[contains(@id,'tab_searchResult')]/tbody/tr[" + indexTigers + " + 1]/td[4]/a[contains(text(),\"[search Players]\")]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody//tr[td//span[contains(text(),\"Oliver\")] and td//span[contains(text(),\"Frank\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody//tr[td//span[contains(text(),\"Ivan\")] and td//span[contains(text(),\"Tate\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody//tr[td//span[contains(text(),\"Gregg\")] and td//span[contains(text(),\"Nash\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody//tr[td//span[contains(text(),\"Floyd\")] and td//span[contains(text(),\"Howell\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody//tr[td//span[contains(text(),\"Simon\")] and td//span[contains(text(),\"Klein\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody//tr[td//span[contains(text(),\"Blake\")] and td//span[contains(text(),\"Montgomery\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody//tr[td//span[contains(text(),\"Doyle\")] and td//span[contains(text(),\"Burns\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody//tr[td//span[contains(text(),\"Troy\")] and td//span[contains(text(),\"Rowe\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody//tr[td//span[contains(text(),\"Jesus\")] and td//span[contains(text(),\"Jimenez\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody//tr[td//span[contains(text(),\"Ross\")] and td//span[contains(text(),\"Guerrero\")]]"));
		selenium.click("xpath=(//span/a)[2]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody//tr[td//span[contains(text(),\"Eddie\")] and td//span[contains(text(),\"Boyd\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody//tr[td//span[contains(text(),\"Luther\")] and td//span[contains(text(),\"Peterson\")]]"));
		assertEquals("[back]", selenium.getText("//*[contains(@id,'lnk_back')]"));
		selenium.click("//*[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie qu'on est bien sur la premi�re page.
		assertTrue(selenium.isElementPresent("//h2[contains(@class,'rd_first')]"));
		selenium.type("//*[contains(@id,'txt_name')]", "");
		selenium.click("//a[contains(@id,'lnk_searchTeams')]");
		selenium.waitForPageToLoad("30000");
		Number indexBears = selenium.getElementIndex("//table[contains(@id,'tab_searchResult')]/tbody/tr[td/span[contains(text(),\"Bears\")] and td/span[contains(text(),\"Kansas City\")]]");
		selenium.click("//table[contains(@id,'tab_searchResult')]/tbody/tr[" + indexBears + " + 1]/td[4]/a[contains(text(),\"[search Players]\")]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody//tr[td//span[contains(text(),\"Keith\")] and td//span[contains(text(),\"Barnett\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody//tr[td//span[contains(text(),\"Daniel\")] and td//span[contains(text(),\"Vargas\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody//tr[td//span[contains(text(),\"Freddie\")] and td//span[contains(text(),\"Kelly\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody//tr[td//span[contains(text(),\"Sam\")] and td//span[contains(text(),\"Farmer\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody//tr[td//span[contains(text(),\"Randal\")] and td//span[contains(text(),\"Hampton\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody//tr[td//span[contains(text(),\"Felipe\")] and td//span[contains(text(),\"Olson\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody//tr[td//span[contains(text(),\"Gary\")] and td//span[contains(text(),\"Christensen\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody//tr[td//span[contains(text(),\"Aaron\")] and td//span[contains(text(),\"Mullins\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody//tr[td//span[contains(text(),\"Miguel\")] and td//span[contains(text(),\"Moore\")]]"));
		selenium.click("xpath=(//span/a)[2]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody//tr[td//span[contains(text(),\"Bruce\")] and td//span[contains(text(),\"Adkins\")]]"));
		selenium.click("//*[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//h2[contains(@class,'rd_first')]"));
		selenium.type("//*[contains(@id,'txt_name')]", "");
		selenium.click("//a[contains(@id,'lnk_searchTeams')]");
		selenium.waitForPageToLoad("30000");
		Number indexChargers = selenium.getElementIndex("//table[contains(@id,'tab_searchResult')]/tbody/tr[td/span[contains(text(),\"Chargers\")] and td/span[contains(text(),\"Detroit\")]]");
		selenium.click("//table[contains(@id,'tab_searchResult')]/tbody/tr[" + indexChargers + " + 1]/td[4]/a[contains(text(),\"[search Players]\")]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody//tr[td//span[contains(text(),\"Abel\")] and td//span[contains(text(),\"Dennis\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody//tr[td//span[contains(text(),\"Tommy\")] and td//span[contains(text(),\"Wood\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody//tr[td//span[contains(text(),\"Felipe\")] and td//span[contains(text(),\"Lawrence\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody//tr[td//span[contains(text(),\"Nelson\")] and td//span[contains(text(),\"Davis\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody//tr[td//span[contains(text(),\"Jeremiah\")] and td//span[contains(text(),\"Johnston\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody//tr[td//span[contains(text(),\"Enrique\")] and td//span[contains(text(),\"Wilkins\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody//tr[td//span[contains(text(),\"Gerald\")] and td//span[contains(text(),\"Jimenez\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody//tr[td//span[contains(text(),\"Michael\")] and td//span[contains(text(),\"Simon\")]]"));
		selenium.click("//*[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//h2[contains(@class,'rd_first')]"));
		selenium.type("//*[contains(@id,'txt_name')]", "");
		selenium.click("//a[contains(@id,'lnk_searchTeams')]");
		selenium.waitForPageToLoad("30000");
		Number indexHawks = selenium.getElementIndex("//table[contains(@id,'tab_searchResult')]/tbody/tr[td/span[contains(text(),\"Hawks\")] and td/span[contains(text(),\"Detroit\")]]");
		selenium.click("//table[contains(@id,'tab_searchResult')]/tbody/tr[" + indexHawks + " + 1]/td[4]/a[contains(text(),\"[search Players]\")]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody//tr[td//span[contains(text(),\"Ricky\")] and td//span[contains(text(),\"Dawson\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody//tr[td//span[contains(text(),\"Sylvester\")] and td//span[contains(text(),\"Nash\")]]"));
		selenium.click("//*[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//h2[contains(@class,'rd_first')]"));
		selenium.type("//*[contains(@id,'txt_name')]", "");
		selenium.click("//a[contains(@id,'lnk_searchTeams')]");
		selenium.waitForPageToLoad("30000");
		Number indexBraves = selenium.getElementIndex("//table[contains(@id,'tab_searchResult')]/tbody/tr[td/span[contains(text(),\"Braves\")] and td/span[contains(text(),\"Omaha\")]]");
		selenium.click("//table[contains(@id,'tab_searchResult')]/tbody/tr[" + indexBraves + " + 1]/td[4]/a[contains(text(),\"[search Players]\")]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody//tr[td//span[contains(text(),\"Earl\")] and td//span[contains(text(),\"Russell\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody//tr[td//span[contains(text(),\"Michael\")] and td//span[contains(text(),\"Hodges\")]]"));
		selenium.click("//*[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//h2[contains(@class,'rd_first')]"));
		selenium.type("//*[contains(@id,'txt_name')]", "");
		selenium.click("//a[contains(@id,'lnk_searchTeams')]");
		selenium.waitForPageToLoad("30000");
		Number indexEagles = selenium.getElementIndex("//table[contains(@id,'tab_searchResult')]/tbody/tr[td/span[contains(text(),\"Eagles\")] and td/span[contains(text(),\"Raleigh\")]]");
		selenium.click("//table[contains(@id,'tab_searchResult')]/tbody/tr[" + indexEagles + " + 1]/td[4]/a[contains(text(),\"[search Players]\")]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody//tr[td//span[contains(text(),\"Nick\")] and td//span[contains(text(),\"Garner\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody//tr[td//span[contains(text(),\"Jerome\")] and td//span[contains(text(),\"Grant\")]]"));
		selenium.click("//*[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//h2[contains(@class,'rd_first')]"));
		selenium.type("//*[contains(@id,'txt_name')]", "");
		selenium.click("//a[contains(@id,'lnk_searchTeams')]");
		selenium.waitForPageToLoad("30000");
		Number indexCardinals = selenium.getElementIndex("//table[contains(@id,'tab_searchResult')]/tbody/tr[td/span[contains(text(),\"Cardinals\")] and td/span[contains(text(),\"Oakland\")]]");
		selenium.click("//table[contains(@id,'tab_searchResult')]/tbody/tr[" + indexCardinals + " + 1]/td[4]/a[contains(text(),\"[search Players]\")]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody//tr[td//span[contains(text(),\"Howard\")] and td//span[contains(text(),\"Ramos\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody//tr[td//span[contains(text(),\"Woodrow\")] and td//span[contains(text(),\"Carpenter\")]]"));
		selenium.click("//*[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//h2[contains(@class,'rd_first')]"));
		selenium.type("//*[contains(@id,'txt_name')]", "");
		selenium.click("//a[contains(@id,'lnk_searchTeams')]");
		selenium.waitForPageToLoad("30000");
		Number indexRedDevils = selenium.getElementIndex("//table[contains(@id,'tab_searchResult')]/tbody/tr[td/span[contains(text(),\"Red Devils\")] and td/span[contains(text(),\"Kansas City\")]]");
		selenium.click("//table[contains(@id,'tab_searchResult')]/tbody/tr[" + indexRedDevils + " + 1]/td[4]/a[contains(text(),\"[search Players]\")]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody//tr[td//span[contains(text(),\"Allan\")] and td//span[contains(text(),\"Meyer\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody//tr[td//span[contains(text(),\"Roderick\")] and td//span[contains(text(),\"Robbins\")]]"));
		selenium.click("//*[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//h2[contains(@class,'rd_first')]"));
		selenium.type("//*[contains(@id,'txt_name')]", "");
		selenium.click("//a[contains(@id,'lnk_searchTeams')]");
		selenium.waitForPageToLoad("30000");
		Number indexMustangs = selenium.getElementIndex("//table[contains(@id,'tab_searchResult')]/tbody/tr[td/span[contains(text(),\"Mustangs\")] and td/span[contains(text(),\"Fresno\")]]");
		selenium.click("//table[contains(@id,'tab_searchResult')]/tbody/tr[" + indexMustangs + " + 1]/td[4]/a[contains(text(),\"[search Players]\")]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody//tr[td//span[contains(text(),\"Ruben\")] and td//span[contains(text(),\"Pope\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody//tr[td//span[contains(text(),\"Christian\")] and td//span[contains(text(),\"Chavez\")]]"));
		selenium.click("//*[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//h2[contains(@class,'rd_first')]"));
		selenium.type("//*[contains(@id,'txt_name')]", "");
		selenium.click("//a[contains(@id,'lnk_searchTeams')]");
		selenium.waitForPageToLoad("30000");
		Number indexWolverines = selenium.getElementIndex("//table[contains(@id,'tab_searchResult')]/tbody/tr[td/span[contains(text(),\"Wolverines\")] and td/span[contains(text(),\"Sacramento\")]]");
		selenium.click("//table[contains(@id,'tab_searchResult')]/tbody/tr[" + indexWolverines + " + 1]/td[4]/a[contains(text(),\"[search Players]\")]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody//tr[td//span[contains(text(),\"Lester\")] and td//span[contains(text(),\"Cross\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody//tr[td//span[contains(text(),\"Cecil\")] and td//span[contains(text(),\"Conner\")]]"));
		selenium.click("//*[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//h2[contains(@class,'rd_first')]"));
		// On teste la recherche post process
		// On affiche toutes les teams.
		selenium.type("//*[contains(@id,'txt_name')]", "");
		selenium.click("//a[contains(@id,'lnk_searchTeams')]");
		selenium.waitForPageToLoad("30000");
		selenium.type("//*[contains(@id,'txt_name')]", "T");
		selenium.click("//*[contains(text(),'[Search teams with post process]')]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_searchResultPostProcess')]/tbody/tr/td/span[contains(text(),'Tigers')]"));
		selenium.type("//*[contains(@id,'txt_name')]", "");
		selenium.click("//a[contains(@id,'lnk_searchTeams')]");
		selenium.waitForPageToLoad("30000");
		selenium.type("//*[contains(@id,'txt_name')]", "t");
		selenium.click("//*[contains(text(),'[Search teams with post process]')]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_searchResultPostProcess')]/tbody/tr/td/span[contains(text(),'Mustangs')]"));
	}


}
