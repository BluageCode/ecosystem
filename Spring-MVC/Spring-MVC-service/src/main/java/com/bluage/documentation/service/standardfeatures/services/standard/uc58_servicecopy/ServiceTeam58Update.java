/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.service.standardfeatures.services.standard.uc58_servicecopy;


import javax.inject.Inject;
import javax.inject.Named;

import com.bluage.documentation.business.standardfeatures.services.standard.uc58_servicecopy.bos.Team58BO;
import com.bluage.documentation.business.standardfeatures.services.standard.uc58_servicecopy.entities.dao.Team58DAOImpl;
import com.bluage.documentation.service.AbstractService;
import com.netfective.bluage.core.exception.ApplicationException;
import com.netfective.bluage.core.persistence.IPersistentObjectDAO;

/**
 * Defines  application services for ServiceTeam58Update
  */
@Named("com.bluage.documentation.service.standardfeatures.services.standard.uc58_servicecopy.ServiceTeam58Update")
public class ServiceTeam58Update  extends AbstractService{


	


	/**
	 *  Default constructor 
	 */
	public ServiceTeam58Update() {
		super(); // Call super for default constructor : ServiceTeam58Update
	}
	
			@Inject
	@Named ("com.bluage.documentation.business.standardfeatures.services.standard.uc58_servicecopy.entities.dao.Team58DAOImpl")
	private Team58DAOImpl dao;

	/**
	 * constructor with...
	 * 
	 * @param dao
	 *		: {@link IPersistentObjectDAO} the DAO
	 */
	public ServiceTeam58Update(Team58DAOImpl dao) {
		this.dao = dao;
	}

			
	/**    
	 * Operation Crud
	 * @param bo {@link Team58BO}
		 * @return {@link boolean}
	 */
	public boolean team58Update(Team58BO bo )
 throws ApplicationException {

					return dao.update(bo);
	}

	

}
