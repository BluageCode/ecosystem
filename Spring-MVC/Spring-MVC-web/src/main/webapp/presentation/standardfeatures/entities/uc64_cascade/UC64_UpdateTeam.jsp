<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file="/include.jsp"  %>
<html >
<head >
<title  > 
<fmt:message key="cUC64_UpdateTeam_BLU_AGE_DOCUMENTATION"/>
</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"  > 
</meta>
<link href="<c:url value= "/presentation/_style/_style.css"/>" rel="stylesheet" type="text/css"  /> 
<link href="<c:url value= "/presentation/_include/jquery/rd_lib.css"/>" rel="stylesheet" type="text/css"  /> 
<script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/jquery.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/rd_setup.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/rd_lib.js"/>" space="preserve"  ></script><link rel="shortcut icon" href="<c:url value= "/presentation/_style/favicon.ico"/>"  /> 

</head>

<body class=" " > 
<div id="rd_ghost"  > 
<small  > 
<fmt:message key="cUC64_UpdateTeam___CSS_DEACTIVATED"/>
</small>
</div>
<div id="rd_jghost"  > 
<small  > 
<fmt:message key="cUC64_UpdateTeam___JAVASCRIPT_DEACTIVATED"/>
</small>
</div>
<div id="shape"  > 
<div class="ui-layout-center"  > 
<div class="east-north"  > 
	<form:form id="uC64_UpdateTeam" commandName="uC64_UpdateTeamForm"  enctype="application/x-www-form-urlencoded" method="post">
<h2 class="rd_first"  > 
<fmt:message key="cUC64_UpdateTeam_STANDARD_FEATURES__ENTITIES_"/>
</h2>
<table class="rd_tableform"  >
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC64_UpdateTeam_NAME"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
<form:input id="txt_name" path="teamToUpdate.name"  size="30" 
		/>
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC64_UpdateTeam_CITY_"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
<form:input id="txt_city" path="teamToUpdate.city"  size="30" 
		/>
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC64_UpdateTeam_STATE"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
<form:select id="slt_state" path="teamToUpdate.state.code"   
		>
		<form:options items="${uC64_UpdateTeamForm.allStates}" itemValue="code" itemLabel="name" />
</form:select>
</td> 
<td rowspan="1" colspan="1"  >
<fmt:message key="cUC64_UpdateTeam_"/>
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<fmt:message key="cUC64_UpdateTeam__160"/>
</td> 
<td rowspan="1" colspan="1"  >
<a id="lnk_update"onclick="javascript:btn_click_cExecuteUC64_UpdateTeam(this.href);return false;"				href="<c:url value="/presentation/standardfeatures/entities/uc64_cascade/UC64_UpdateTeam/lnk_update.html"/>"
							 shape="rect"  >									 
<fmt:message key="cUC64_UpdateTeam_UPDATE_TEAM"/>
</a>
</td> 
<td rowspan="1" colspan="1"  >
<fmt:message key="cUC64_UpdateTeam__161"/>
</td> 
</tr>
</table> 
	</form:form>
	<script language="javascript">
		function btn_click_cExecuteUC64_UpdateTeam(url) {
			UC64_UpdateTeamvar = document.getElementById('uC64_UpdateTeam');
			UC64_UpdateTeamvar.setAttribute('action', url);
			UC64_UpdateTeamvar.submit();
		}
		function btn_click_cSetActionUC64_UpdateTeam(url) {
			UC64_UpdateTeamvar = document.getElementById('uC64_UpdateTeam');
			UC64_UpdateTeamvar.setAttribute('action', url);
		}
	</script>
</div>
<div class="east-center"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/entities/uc64_cascade/UC64_UpdateTeamcmp_tabs" />
</div>
</div>
<div class="ui-layout-north"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/entities/uc64_cascade/UC64_UpdateTeamcmp_header" />
</div>
<div class="ui-layout-south"  > 
<fmt:message key="cUC64_UpdateTeam___"/>
<span id="rd_jversion"  > 
<fmt:message key="cUC64_UpdateTeam_BLU_AGE_DOCUMENTATION_162"/>
</span>
</div>
<div class="ui-layout-west"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/entities/uc64_cascade/UC64_UpdateTeamcmp_treeview" />
</div>
</div>
</body>
</html>
