/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.gettingstarted.uc03_formvalidation ;


import org.apache.log4j.Logger;
import org.springframework.validation.Errors;

import com.netfective.bluage.core.validator.AbstractValidator;
/**
* Class :UC03_FormValidationMessageValidator
*/
public class UC03_FormValidationMessageValidator extends AbstractValidator{
	
	// LOGGER for the class UC03_FormValidationMessageValidator
	private static final Logger LOGGER = Logger.getLogger( UC03_FormValidationMessageValidator.class);
	
	
	/**
	* Operation validate for UC03_FormValidationMessageForm
	* @param obj : the current form (UC03_FormValidationMessageForm)
	* @param errors : The spring errors to return for the form UC03_FormValidationMessageForm
	*/
	public void validate(final Object obj,final Errors errors){
		LOGGER.info("Beginning method : validate"); // UC03_FormValidationMessageValidator
		UC03_FormValidationMessageForm cUC03_FormValidationMessageForm = (UC03_FormValidationMessageForm)obj; // UC03_FormValidationMessageValidator
		LOGGER.info("Ending method : validate the form "+ cUC03_FormValidationMessageForm.getClass().getName()); // UC03_FormValidationMessageValidator
	}

	/**
	* Method to implements to use spring validators (UC03_FormValidationMessageForm)
	* @param aClass : Class for the form UC03_FormValidationMessageForm
	* @return boolean
	*/
	public boolean supports(final Class aClass){
		return (new UC03_FormValidationMessageForm()).getClass().equals(aClass); // UC03_FormValidationMessageValidator
	}
}
