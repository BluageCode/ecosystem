<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file="/include.jsp"  %>
<html >
<head >
<title  > 
<fmt:message key="cUC28_Stadiums_BLU_AGE_DOCUMENTATION"/>
</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"  > 
</meta>
<link href="<c:url value= "/presentation/_style/_style.css"/>" rel="stylesheet" type="text/css"  /> 
<link href="<c:url value= "/presentation/_include/jquery/rd_lib.css"/>" rel="stylesheet" type="text/css"  /> 
<script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/jquery.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/rd_setup.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/rd_lib.js"/>" space="preserve"  ></script><link rel="shortcut icon" href="<c:url value= "/presentation/_style/favicon.ico"/>"  /> 

</head>

<body class=" " > 
<div id="rd_ghost"  > 
<small  > 
<fmt:message key="cUC28_Stadiums___CSS_DEACTIVATED"/>
</small>
</div>
<div id="rd_jghost"  > 
<small  > 
<fmt:message key="cUC28_Stadiums___JAVASCRIPT_DEACTIVATED"/>
</small>
</div>
<div id="shape"  > 
<div class="ui-layout-center"  > 
<div class="east-north"  > 
	<form:form id="uC28_Stadiums" commandName="uC28_StadiumsForm"  enctype="application/x-www-form-urlencoded" method="post">
<h2 class="rd_first"  > 
<fmt:message key="cUC28_Stadiums_STANDARD_FEATURES__APPLICATIO"/>
</h2>
<br clear="none"  /> 
<br clear="none"  /> 
<table class="rd_tableform2"  >
<tr  > 
<td rowspan="1" colspan="1"  >
<b  > 
<a id="lnk_teams"onclick="javascript:btn_click_cExecuteUC28_Stadiums(this.href);return false;"				href="<c:url value="/presentation/standardfeatures/application/navigation/uc28_packagescope/package_two/UC28_Stadiums/lnk_teams.html"/>"
							 shape="rect"  >									 
<fmt:message key="cUC28_Stadiums_TEAMS"/>
</a>
</b>
</td> 
<td rowspan="1" colspan="1"  >
<b  > 
<a id="lnk_players"onclick="javascript:btn_click_cExecuteUC28_Stadiums(this.href);return false;"				href="<c:url value="/presentation/standardfeatures/application/navigation/uc28_packagescope/package_two/UC28_Stadiums/lnk_players.html"/>"
							 shape="rect"  >									 
<fmt:message key="cUC28_Stadiums_PLAYERS"/>
</a>
</b>
</td> 
<td rowspan="1" colspan="1"  >
<b  > 
<a id="lnk_states"onclick="javascript:btn_click_cExecuteUC28_Stadiums(this.href);return false;"				href="<c:url value="//presentation/standardfeatures/application/navigation/uc28_packagescope/package_two/UC28_Onglets_2/lnk_states.html"/>"
							 shape="rect"  >									 
<fmt:message key="cUC28_Stadiums_STATES"/>
</a>
</b>
</td> 
<td rowspan="1" colspan="1"  >
<b  > 
<a id="lnk_stadiums"onclick="javascript:btn_click_cExecuteUC28_Stadiums(this.href);return false;"				href="<c:url value="//presentation/standardfeatures/application/navigation/uc28_packagescope/package_two/UC28_Onglets_2/lnk_stadiums.html"/>"
							 shape="rect"  >									 
<u  > 
<fmt:message key="cUC28_Stadiums_STADIUMS"/>
</u>
</a>
</b>
</td> 
</tr>
</table> 
<display:table uid="tab_stadiums"  sort="list" requestURI="" name="sessionScope.uC28_StadiumsForm.allStadiums" class="rd_tabledata"  >
	<c:set   var="rownum">
		<%=(tab_stadiums !=	null) ? new Integer(tab_stadiums_rowNum.intValue() - 1) : new Integer(0) %>
	</c:set>
	<c:set   var="idRow">
		<%=(tab_stadiums != null ) ? tab_stadiums_rowNum.intValue() - 1 : -1 %>
	</c:set>
	<c:set   var="idTab">
		<%=(tab_stadiums != null ) ? "tab_stadiums" : null %>
	</c:set>
	<c:set var="tableHashId">
		<%= new org.displaytag.util.ParamEncoder("tab_stadiums").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE) %>
	</c:set>
	<c:set var="tableHashId2">
		<%=request.getParameter(new org.displaytag.util.ParamEncoder("tab_stadiums").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE)) %>
	</c:set>	
	
<display:column format=""  titleKey="cUC28_Stadiums_NAME">
		<span id="dyn_name">
	<c:out value="${tab_stadiums.name}" />
		</span>
	</display:column>
</display:table>
	</form:form>
	<script language="javascript">
		function btn_click_cExecuteUC28_Stadiums(url) {
			UC28_Stadiumsvar = document.getElementById('uC28_Stadiums');
			UC28_Stadiumsvar.setAttribute('action', url);
			UC28_Stadiumsvar.submit();
		}
		function btn_click_cSetActionUC28_Stadiums(url) {
			UC28_Stadiumsvar = document.getElementById('uC28_Stadiums');
			UC28_Stadiumsvar.setAttribute('action', url);
		}
	</script>
</div>
<div class="east-center"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/application/navigation/uc28_packagescope/package_two/UC28_Stadiumscmp_tabs" />
</div>
</div>
<div class="ui-layout-north"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/application/navigation/uc28_packagescope/package_two/UC28_Stadiumscmp_header" />
</div>
<div class="ui-layout-south"  > 
<fmt:message key="cUC28_Stadiums___"/>
<span id="rd_jversion"  > 
<fmt:message key="cUC28_Stadiums_BLU_AGE_DOCUMENTATION_117"/>
</span>
</div>
<div class="ui-layout-west"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/application/navigation/uc28_packagescope/package_two/UC28_Stadiumscmp_treeview" />
</div>
</div>
</body>
</html>
