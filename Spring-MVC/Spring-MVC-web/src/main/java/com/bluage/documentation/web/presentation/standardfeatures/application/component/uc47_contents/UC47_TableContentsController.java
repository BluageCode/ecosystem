/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.application.component.uc47_contents ;

// Import declaration.
// Java imports.
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.bluage.documentation.business.standardfeatures.application.component.uc47_contents.bos.Player47BO;
import com.bluage.documentation.business.standardfeatures.application.component.uc47_contents.bos.Screen47BO;
import com.bluage.documentation.business.standardfeatures.application.component.uc47_contents.entities.daofinder.Player47DAOFinderImpl;
import com.bluage.documentation.service.standardfeatures.application.component.uc47_contents.ServicePlayer47;
import com.bluage.documentation.service.standardfeatures.application.component.uc47_contents.ServiceScreen47;
import com.bluage.documentation.service.standardfeatures.application.component.uc47_contents.ServiceTeam47;
import com.netfective.bluage.core.exception.ApplicationException;
import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;
import com.netfective.bluage.core.web.IForm;

/**
 * Class : UC47_TableContentsController
 */
@Controller
@Scope("prototype")
@SessionAttributes("uC47_TableContentsForm")
@RequestMapping(value= "/presentation/standardfeatures/application/component/uc47_contents")
public class UC47_TableContentsController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC47_TableContentsController.class);
	
	//Current form name
	private static final String U_C47__TABLE_CONTENTS_FORM = "uC47_TableContentsForm";

	//CONSTANT: allPlayers table or repeater.
	private static final String ALL_PLAYERS = "allPlayers";
				//CONSTANT: playerToUpdate
	private static final String PLAYER_TO_UPDATE = "playerToUpdate";
	//CONSTANT: listTeam
	private static final String LIST_TEAM = "listTeam";
	//CONSTANT: displayTable
	private static final String DISPLAY_TABLE = "displayTable";
	
	/**
	 * Property:customDateEditorsUC47_TableContentsController
	 */
	final private Map<String,CustomDateEditor> customDateEditorsUC47_TableContentsController = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:uC47_TableContentsValidator
	 */
	final private UC47_TableContentsValidator uC47_TableContentsValidator = new UC47_TableContentsValidator();
	
	/**
	 * Service declaration : serviceScreen47.
	 */
	@Autowired
	private ServiceScreen47 serviceScreen47;
	
	/**
	 * Service declaration : servicePlayer47.
	 */
	@Autowired
	private ServicePlayer47 servicePlayer47;
	
	/**
	 * Service declaration : serviceTeam47.
	 */
	@Autowired
	private ServiceTeam47 serviceTeam47;
	
	/**
	 * Pre Controller declaration : uC47_PreControllerFindAllPlayers
	 */
	@Autowired
	private UC47_PreControllerFindAllPlayersController uC47_PreControllerFindAllPlayers;

	/**
	 * Generic Finder : player47DAOFinderImpl.
	 */
	@Autowired
	private Player47DAOFinderImpl player47DAOFinderImpl;
	
	
	// Initialise all the instances for UC47_TableContentsController
		// Initialize the instance allPlayers for the state lnk_back
		private List allPlayers; // Initialize the instance allPlayers for UC47_TableContentsController
						// Declare the instance playerToUpdate
		private Player47BO playerToUpdate;
		// Declare the instance listTeam
		private List listTeam;
		// Declare the instance displayTable
		private Screen47BO displayTable;
								/**
	 * Operation : lnk_details
 	 * @param model : 
 	 * @param uC47_TableContents : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC47_TableContents/lnk_details.html",method = RequestMethod.POST)
	public String lnk_details(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC47_TableContentsForm") UC47_TableContentsForm  uC47_TableContentsForm, final BindingResult bindingResult ,final @RequestParam(ID_REQUEST_PARAM) Integer index){
		
		
		
		LOGGER.info("Begin method : lnk_details"); 
		infoLogger(uC47_TableContentsForm); 
		uC47_TableContentsForm.setAlwaysCallPreControllers(false); 
		// initialization for lnk_details
		init();
		
		String destinationPath = null; 
		

										 callFindPlayerbyID(request,uC47_TableContentsForm , index);
			
									 callReturnTeams(request,uC47_TableContentsForm , index);
			
 callDisplayTable(request,uC47_TableContentsForm );
											// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.application.component.uc47_contents.UC47_ContentsForm or not from lnk_details
			final UC47_ContentsForm uC47_ContentsForm =  new UC47_ContentsForm();
			// Populate the destination form with all the instances returned from lnk_details.
			final IForm uC47_ContentsForm2 = populateDestinationForm(request.getSession(), uC47_ContentsForm); 
					
			// Add the form to the model.
			model.addAttribute("uC47_ContentsForm", uC47_ContentsForm2); 
			
			request.getSession().setAttribute("uC47_ContentsForm", uC47_ContentsForm2);
			
			// "OK" CASE => destination screen path from lnk_details
			LOGGER.info("Go to the screen 'UC47_Contents'.");
			// Redirect (PRG) from lnk_details
			destinationPath =  "redirect:/presentation/standardfeatures/application/component/uc47_contents/UC47_Contents.html";
						
						
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_details 
	}
	
									/**
	 * Operation : lnk_edit
 	 * @param model : 
 	 * @param uC47_TableContents : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC47_TableContents/lnk_edit.html",method = RequestMethod.POST)
	public String lnk_edit(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC47_TableContentsForm") UC47_TableContentsForm  uC47_TableContentsForm, final BindingResult bindingResult ,final @RequestParam(ID_REQUEST_PARAM) Integer index){
		
		
		
		LOGGER.info("Begin method : lnk_edit"); 
		infoLogger(uC47_TableContentsForm); 
		uC47_TableContentsForm.setAlwaysCallPreControllers(false); 
		// initialization for lnk_edit
		init();
		
		String destinationPath = null; 
		

										 callFindPlayer(request,uC47_TableContentsForm , index);
			
									 callSearchTeamsnotplayedbyPlayer(request,uC47_TableContentsForm , index);
											// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.application.component.uc47_contents.UC47_ContentsDetailsForm or not from lnk_edit
			final UC47_ContentsDetailsForm uC47_ContentsDetailsForm =  new UC47_ContentsDetailsForm();
			// Populate the destination form with all the instances returned from lnk_edit.
			final IForm uC47_ContentsDetailsForm2 = populateDestinationForm(request.getSession(), uC47_ContentsDetailsForm); 
					
			// Add the form to the model.
			model.addAttribute("uC47_ContentsDetailsForm", uC47_ContentsDetailsForm2); 
			
			request.getSession().setAttribute("uC47_ContentsDetailsForm", uC47_ContentsDetailsForm2);
			
			// "OK" CASE => destination screen path from lnk_edit
			LOGGER.info("Go to the screen 'UC47_ContentsDetails'.");
			// Redirect (PRG) from lnk_edit
			destinationPath =  "redirect:/presentation/standardfeatures/application/component/uc47_contents/UC47_ContentsDetails.html";
						
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_edit 
	}
	
	
	/**
	* This method initialise the form : UC47_TableContentsForm 
	* @return UC47_TableContentsForm
	*/
	@ModelAttribute("UC47_TableContentsFormInit")
	public void initUC47_TableContentsForm(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, U_C47__TABLE_CONTENTS_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method UC47_TableContentsForm."); //for lnk_edit 
		}
		UC47_TableContentsForm uC47_TableContentsForm;
	
		if(request.getSession().getAttribute(U_C47__TABLE_CONTENTS_FORM) != null){
			uC47_TableContentsForm = (UC47_TableContentsForm)request.getSession().getAttribute(U_C47__TABLE_CONTENTS_FORM);
		} else {
			uC47_TableContentsForm = new UC47_TableContentsForm();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form UC47_TableContentsForm.");
		}
		uC47_TableContentsForm = (UC47_TableContentsForm)populateDestinationForm(request.getSession(), uC47_TableContentsForm);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  UC47_TableContentsForm.");
		}
		model.addAttribute(U_C47__TABLE_CONTENTS_FORM, uC47_TableContentsForm);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param UC47_TableContentsForm : The sceen form.
	 */
	@RequestMapping(value = "/UC47_TableContents.html" ,method = RequestMethod.GET)
	public void prepareUC47_TableContents(final HttpServletRequest request,final  Model model,final  @ModelAttribute("UC47_TableContentsFormInit") UC47_TableContentsForm uC47_TableContentsForm){
		
		UC47_TableContentsForm currentUC47_TableContentsForm = uC47_TableContentsForm;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(U_C47__TABLE_CONTENTS_FORM) == null){
			if(currentUC47_TableContentsForm!=null){
				request.getSession().setAttribute(U_C47__TABLE_CONTENTS_FORM, currentUC47_TableContentsForm);
			}else {
				currentUC47_TableContentsForm = new UC47_TableContentsForm();
				request.getSession().setAttribute(U_C47__TABLE_CONTENTS_FORM, currentUC47_TableContentsForm);	
			}
		} else {
			currentUC47_TableContentsForm = (UC47_TableContentsForm) request.getSession().getAttribute(U_C47__TABLE_CONTENTS_FORM);
		}

		try {
			List allPlayers = player47DAOFinderImpl.findAll();
			put(request.getSession(), ALL_PLAYERS, allPlayers);
		} catch (ApplicationException e) {
			errorLogger("An error while calling the generic finder for the instance : allPlayers.",e);
		}
				currentUC47_TableContentsForm = (UC47_TableContentsForm)populateDestinationForm(request.getSession(), currentUC47_TableContentsForm);
		// Call all the Precontroller.
	if ( currentUC47_TableContentsForm.isAlwaysCallPreControllers()) {
	
		// Calling the PreController : UC47_PreControllerFindAllPlayers.
		currentUC47_TableContentsForm = (UC47_TableContentsForm) uC47_PreControllerFindAllPlayers.uC47_PreControllerFindAllPlayersControllerInit(request, model, currentUC47_TableContentsForm);
		
	}
	currentUC47_TableContentsForm.setAlwaysCallPreControllers(true);
		model.addAttribute(U_C47__TABLE_CONTENTS_FORM, currentUC47_TableContentsForm);
		request.getSession().setAttribute(U_C47__TABLE_CONTENTS_FORM, currentUC47_TableContentsForm);
		
	}
	
										/**
	 * method callFindPlayerbyID
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 * @param index Integer the current index 
	 */
	private void callFindPlayerbyID(HttpServletRequest request,IForm form,Integer index) {
		if(index!=null){  
			 playerToUpdate = (Player47BO)((List)get(request.getSession(),form, "allPlayers")).get(index);  
		} else {
			 playerToUpdate= null; 
		}
				 
					if(index!=null){  
			 playerToUpdate = (Player47BO)((List)get(request.getSession(),form, "allPlayers")).get(index);  
		} else {
			 playerToUpdate= null; 
		}
				 
					try {
				// executing FindPlayerbyID in lnk_edit
				playerToUpdate = 	servicePlayer47.player47FindByID(
			playerToUpdate
			);  
 
				put(request.getSession(), PLAYER_TO_UPDATE,playerToUpdate);
								// processing variables FindPlayerbyID in lnk_edit
				put(request.getSession(), PLAYER_TO_UPDATE,playerToUpdate); 
			
			} catch (ApplicationException e) { 
				// error handling for operation player47FindByID called FindPlayerbyID
				errorLogger("An error occured during the execution of the operation : player47FindByID",e); 
		
			}
	}
									/**
	 * method callReturnTeams
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 * @param index Integer the current index 
	 */
	private void callReturnTeams(HttpServletRequest request,IForm form,Integer index) {
		listTeam = (List)get(request.getSession(),form, "listTeam");  
										 
					if(index!=null){  
			 playerToUpdate = (Player47BO)((List)get(request.getSession(),form, "allPlayers")).get(index);  
		} else {
			 playerToUpdate= null; 
		}
				 
					try {
				// executing ReturnTeams in lnk_edit
				listTeam = 	serviceTeam47.returnTeamByPlayer47(
			playerToUpdate
			);  
 
				put(request.getSession(), LIST_TEAM,listTeam);
								// processing variables ReturnTeams in lnk_edit
				put(request.getSession(), PLAYER_TO_UPDATE,playerToUpdate); 
			
			} catch (ApplicationException e) { 
				// error handling for operation returnTeamByPlayer47 called ReturnTeams
				errorLogger("An error occured during the execution of the operation : returnTeamByPlayer47",e); 
		
			}
	}
		/**
	 * method callDisplayTable
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callDisplayTable(HttpServletRequest request,IForm form) {
		displayTable = (Screen47BO)get(request.getSession(),form, "displayTable");  
										 
					try {
				// executing DisplayTable in lnk_edit
				displayTable = 	serviceScreen47.displayTableScreen47(
	);  
 
				put(request.getSession(), DISPLAY_TABLE,displayTable);
								// processing variables DisplayTable in lnk_edit

			} catch (ApplicationException e) { 
				// error handling for operation displayTableScreen47 called DisplayTable
				errorLogger("An error occured during the execution of the operation : displayTableScreen47",e); 
		
			}
	}
									/**
	 * method callFindPlayer
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 * @param index Integer the current index 
	 */
	private void callFindPlayer(HttpServletRequest request,IForm form,Integer index) {
		if(index!=null){  
			 playerToUpdate = (Player47BO)((List)get(request.getSession(),form, "allPlayers")).get(index);  
		} else {
			 playerToUpdate= null; 
		}
				 
					if(index!=null){  
			 playerToUpdate = (Player47BO)((List)get(request.getSession(),form, "allPlayers")).get(index);  
		} else {
			 playerToUpdate= null; 
		}
				 
					try {
				// executing FindPlayer in lnk_edit
				playerToUpdate = 	servicePlayer47.player47FindByID(
			playerToUpdate
			);  
 
				put(request.getSession(), PLAYER_TO_UPDATE,playerToUpdate);
								// processing variables FindPlayer in lnk_edit
				put(request.getSession(), PLAYER_TO_UPDATE,playerToUpdate); 
			
			} catch (ApplicationException e) { 
				// error handling for operation player47FindByID called FindPlayer
				errorLogger("An error occured during the execution of the operation : player47FindByID",e); 
		
			}
	}
									/**
	 * method callSearchTeamsnotplayedbyPlayer
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 * @param index Integer the current index 
	 */
	private void callSearchTeamsnotplayedbyPlayer(HttpServletRequest request,IForm form,Integer index) {
		listTeam = (List)get(request.getSession(),form, "listTeam");  
										 
					if(index!=null){  
			 playerToUpdate = (Player47BO)((List)get(request.getSession(),form, "allPlayers")).get(index);  
		} else {
			 playerToUpdate= null; 
		}
				 
					try {
				// executing SearchTeamsnotplayedbyPlayer in lnk_edit
				listTeam = 	serviceTeam47.searchTeamsNotPlayedByPlayer47(
			playerToUpdate
			);  
 
				put(request.getSession(), LIST_TEAM,listTeam);
								// processing variables SearchTeamsnotplayedbyPlayer in lnk_edit
				put(request.getSession(), PLAYER_TO_UPDATE,playerToUpdate); 
			
			} catch (ApplicationException e) { 
				// error handling for operation searchTeamsNotPlayedByPlayer47 called SearchTeamsnotplayedbyPlayer
				errorLogger("An error occured during the execution of the operation : searchTeamsNotPlayedByPlayer47",e); 
		
			}
	}
		
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("UC47_TableContentsController [ ");
			strBToS.append(ALL_PLAYERS);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(allPlayers);
			strBToS.append(END_TO_STRING_DELIMITER);
						strBToS.append(PLAYER_TO_UPDATE);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(playerToUpdate);
			strBToS.append(END_TO_STRING_DELIMITER);
			strBToS.append(LIST_TEAM);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(listTeam);
			strBToS.append(END_TO_STRING_DELIMITER);
			strBToS.append(DISPLAY_TABLE);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(displayTable);
			strBToS.append(END_TO_STRING_DELIMITER);
		// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(uC47_TableContentsValidator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsUC47_TableContentsController!=null); 
		return strBToS.toString();
	}
}
