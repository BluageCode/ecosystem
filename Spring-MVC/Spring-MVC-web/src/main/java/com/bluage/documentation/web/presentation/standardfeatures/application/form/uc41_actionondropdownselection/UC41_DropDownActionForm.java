/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.application.form.uc41_actionondropdownselection;


import java.io.Serializable;
import java.util.List;

import com.bluage.documentation.business.standardfeatures.application.form.uc41_actionondropdownselection.bos.Player41BO;
import com.bluage.documentation.business.standardfeatures.application.form.uc41_actionondropdownselection.bos.Position41BO;
import com.bluage.documentation.business.standardfeatures.application.form.uc41_actionondropdownselection.bos.Position41ForSelectBO;
import com.netfective.bluage.core.web.AbstractForm;
import com.netfective.bluage.core.web.IForm;

/**
* Class : UC41_DropDownActionForm
*/
public class UC41_DropDownActionForm extends AbstractForm implements Serializable, IForm {
	
	/** Constant : serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	//CONSTANT : position
	private static final String POSITION = "position";
	//CONSTANT : positions
	private static final String POSITIONS = "positions";
	//CONSTANT : sel_positions
	private static final String SEL_POSITIONS = "sel_positions";
	//CONSTANT : players
	private static final String PLAYERS = "players";
	/**
	 * 	Property: position 
	 */
	private Position41BO position;
	/**
	 * 	Property: positions 
	 */
	private Position41ForSelectBO positions;
	/**
	 * 	Property: sel_positions 
	 */
	private List sel_positions;
	/**
	 * 	Property: players 
	 */
	private List<Player41BO> players;
/**
	 * Default constructor : UC41_DropDownActionForm
	 */
	public UC41_DropDownActionForm() {
		super();
		// Initialize : position
		this.position = new Position41BO();
		// Initialize : positions
		this.positions = new Position41ForSelectBO();
		// Initialize : sel_positions
		this.sel_positions = new java.util.ArrayList();
		// Initialize : players
		this.players = new java.util.ArrayList<com.bluage.documentation.business.standardfeatures.application.form.uc41_actionondropdownselection.bos.Player41BO>();
					this.setAlwaysCallPreControllers(true);
	}
	
	/**
	 * 	Getter : position 
	 * 	@return : Return the position instance.
	 */
	public Position41BO getPosition(){
		return position; // For UC41_DropDownActionForm
	}
	
	/**
	 * 	Setter : position 
	 *  @param positioninstance : The instance to set.
	 */
	public void setPosition(final Position41BO positioninstance){
		this.position = positioninstance;// For UC41_DropDownActionForm
	}
	/**
	 * 	Getter : positions 
	 * 	@return : Return the positions instance.
	 */
	public Position41ForSelectBO getPositions(){
		return positions; // For UC41_DropDownActionForm
	}
	
	/**
	 * 	Setter : positions 
	 *  @param positionsinstance : The instance to set.
	 */
	public void setPositions(final Position41ForSelectBO positionsinstance){
		this.positions = positionsinstance;// For UC41_DropDownActionForm
	}
	/**
	 * 	Getter : sel_positions 
	 * 	@return : Return the sel_positions instance.
	 */
	public List getSel_positions(){
		return sel_positions; // For UC41_DropDownActionForm
	}
	
	/**
	 * 	Setter : sel_positions 
	 *  @param sel_positionsinstance : The instance to set.
	 */
	public void setSel_positions(final List sel_positionsinstance){
		this.sel_positions = sel_positionsinstance;// For UC41_DropDownActionForm
	}
	/**
	 * 	Getter : players 
	 * 	@return : Return the players instance.
	 */
	public List<Player41BO> getPlayers(){
		return players; // For UC41_DropDownActionForm
	}
	
	/**
	 * 	Setter : players 
	 *  @param playersinstance : The instance to set.
	 */
	public void setPlayers(final List<Player41BO> playersinstance){
		this.players = playersinstance;// For UC41_DropDownActionForm
	}
/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UC41_DropDownActionForm [ "+
POSITION +" = " + position +POSITIONS +" = " + positions +SEL_POSITIONS +" = " + sel_positions +PLAYERS +" = " + players + "]";
	}
	
	/**
	 * Method setInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public void setInstanceByInstanceName(final String instanceName, final Object instance) {
		// Set the instance Position.
		if(POSITION.equals(instanceName)){// For UC41_DropDownActionForm
			this.setPosition((Position41BO)instance); // For UC41_DropDownActionForm
		}
				// Set the instance Positions.
		if(POSITIONS.equals(instanceName)){// For UC41_DropDownActionForm
			this.setPositions((Position41ForSelectBO)instance); // For UC41_DropDownActionForm
		}
				// Set the instance Sel_positions.
		if(SEL_POSITIONS.equals(instanceName)){// For UC41_DropDownActionForm
			this.setSel_positions((List)instance); // For UC41_DropDownActionForm
		}
				// Set the instance Players.
		if(PLAYERS.equals(instanceName)){// For UC41_DropDownActionForm
			this.setPlayers((List<Player41BO>)instance); // For UC41_DropDownActionForm
		}
				// Parent instance name for UC41_DropDownActionForm
		if(POSITIONS.equals(instanceName) && positions != null){ // For UC41_DropDownActionForm
			sel_positions.clear(); // For UC41_DropDownActionForm
			sel_positions.addAll(positions.getAllPositions());// For UC41_DropDownActionForm
		}
		
	}
	
	/**
	 * Method getInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public Object getInstanceByInstanceName(final String instanceName){
		// Temporary object for tmpUC41_DropDownActionForm.
		Object tmpUC41_DropDownActionForm = null;
		
			
		// Get the instance Position for UC41_DropDownActionForm.
		if(POSITION.equals(instanceName)){ // For UC41_DropDownActionForm
			tmpUC41_DropDownActionForm = this.getPosition(); // For UC41_DropDownActionForm
		}
			
		// Get the instance Positions for UC41_DropDownActionForm.
		if(POSITIONS.equals(instanceName)){ // For UC41_DropDownActionForm
			tmpUC41_DropDownActionForm = this.getPositions(); // For UC41_DropDownActionForm
		}
			
		// Get the instance Sel_positions for UC41_DropDownActionForm.
		if(SEL_POSITIONS.equals(instanceName)){ // For UC41_DropDownActionForm
			tmpUC41_DropDownActionForm = this.getSel_positions(); // For UC41_DropDownActionForm
		}
			
		// Get the instance Players for UC41_DropDownActionForm.
		if(PLAYERS.equals(instanceName)){ // For UC41_DropDownActionForm
			tmpUC41_DropDownActionForm = this.getPlayers(); // For UC41_DropDownActionForm
		}
		return tmpUC41_DropDownActionForm;// For UC41_DropDownActionForm
	}
	
			}
