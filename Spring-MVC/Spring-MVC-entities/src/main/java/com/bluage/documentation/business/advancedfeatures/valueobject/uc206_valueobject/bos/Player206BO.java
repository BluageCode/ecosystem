/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.business.advancedfeatures.valueobject.uc206_valueobject.bos;

import java.util.Date;

import javax.inject.Inject;
import javax.inject.Named;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import com.bluage.documentation.business.AbstractBusinessObject;
import com.bluage.documentation.business.advancedfeatures.valueobject.uc206_valueobject.entities.daofinder.Player206DAOFinderImpl;

/**
 * Player206BO
 *
* @see com.netfective.bluage.core.business.IBusinessObject
 */
@XmlRootElement

public class Player206BO
 extends AbstractBusinessObject {
	
	/**
	 * Default SUID for Player206BO.
	 */
	private static final long serialVersionUID = 1L;
	

	@Inject
	@Named ("com.bluage.documentation.business.advancedfeatures.valueobject.uc206_valueobject.entities.daofinder.Player206Finder") 
	private Player206DAOFinderImpl finder;
	/**
	 * Hibernate optimistic lock for class Player206BO.
	 */
	private int version;

												/**
	 * Entity property: position
	 *
	 */
private Position206BO position;

					/**
	 * Entity property: id
	 *
	 */
private Long id;

					/**
	 * Entity property: firstName
	 *
	 */
private String firstName;

					/**
	 * Entity property: lastName
	 *
	 */
private String lastName;

					/**
	 * Entity property: dateOfBirth
	 *
	 */
private Date dateOfBirth;

					/**
	 * Entity property: estimatedValue
	 *
	 */
private Float estimatedValue;

					/**
	 * Entity property: height
	 *
	 */
private Integer height;

					/**
	 * Entity property: weight
	 *
	 */
private Integer weight;

					/**
	 * Entity property: rookie
	 *
	 */
private Boolean rookie = Boolean.FALSE;



   /**
	* public default constructor for class Player206BO
	*/
	public Player206BO(){
		//NOP Player206BO
	}

	
	/**
	 * Getter for version for class Player206BO
	 * @return int
	 */
	public int getVersion() {
		return this.version;
	}
	
	/**
	 * Setter for version for class Player206BO
	 * @param int version
	 */
	public void setVersion(final int version) {
		this.version = version;
	}

												/**
	 * Getter for position of class Player206BO.
	 *
	 * @return {@link com.bluage.documentation.business.advancedfeatures.valueobject.uc206_valueobject.bos.Position206BO}
	 * @generated
	 */ 
public  Position206BO getPosition() {
		Position206BO position0 = this.position;
		if(this.position == null) {
			if(DAO_MODE.equals(this.getObjMode( ) ) ) {
				position0 = null;
			} else {
				this.position = new Position206BO();
				this.position.setObjMode( this.getObjMode( ) );
				position0 = this.position;
			}
		} else {
			this.position.setObjMode( this.getObjMode( ) );
			if( HELPER_MODE.equals(this.getObjMode( ) ) ) {
				position0 = this.position;
			}
			if( DAO_MODE.equals(this.getObjMode()) && this.position.isEmpty ) {
				position0 = null;
			}
		}
		return position0;
	}


	/**
	 * Setter for position of class Player206BO.
	 * @param value {@link com.bluage.documentation.business.advancedfeatures.valueobject.uc206_valueobject.bos.Position206BO}
	 * @generated
	 */

public  void setPosition(final Position206BO position) {
	if(position != null) {
		position.isEmpty = false;
		position.setObjMode( this.getObjMode( ) );
	}
	this.isEmpty = false;
	this.position = position;
}

					/**
	 * Getter for id of class Player206BO.
	 *
	 * @return {@link java.lang.Long}
	 * @generated
	 */ 
public  Long getId() {
	return this.id;
	}


	/**
	 * Setter for id of class Player206BO.
	 * @param value {@link java.lang.Long}
	 * @generated
	 */

public  void setId(final Long id) {
	this.isEmpty = (id == null);
	this.id = id;
}

					/**
	 * Getter for firstName of class Player206BO.
	 *
	 * @return {@link java.lang.String}
	 * @generated
	 */ 
public  String getFirstName() {
	return this.firstName;
	}


	/**
	 * Setter for firstName of class Player206BO.
	 * @param value {@link java.lang.String}
	 * @generated
	 */

public  void setFirstName(final String firstName) {
	this.firstName = firstName;
}

					/**
	 * Getter for lastName of class Player206BO.
	 *
	 * @return {@link java.lang.String}
	 * @generated
	 */ 
public  String getLastName() {
	return this.lastName;
	}


	/**
	 * Setter for lastName of class Player206BO.
	 * @param value {@link java.lang.String}
	 * @generated
	 */

public  void setLastName(final String lastName) {
	this.lastName = lastName;
}

					/**
	 * Getter for dateOfBirth of class Player206BO.
	 *
	 * @return {@link java.util.Date}
	 * @generated
	 */ 
public  Date getDateOfBirth() {
	return this.dateOfBirth;
	}


	/**
	 * Setter for dateOfBirth of class Player206BO.
	 * @param value {@link java.util.Date}
	 * @generated
	 */

public  void setDateOfBirth(final Date dateOfBirth) {
	this.dateOfBirth = dateOfBirth;
}

					/**
	 * Getter for estimatedValue of class Player206BO.
	 *
	 * @return {@link java.lang.Float}
	 * @generated
	 */ 
public  Float getEstimatedValue() {
	return this.estimatedValue;
	}


	/**
	 * Setter for estimatedValue of class Player206BO.
	 * @param value {@link java.lang.Float}
	 * @generated
	 */

public  void setEstimatedValue(final Float estimatedValue) {
	this.estimatedValue = estimatedValue;
}

					/**
	 * Getter for height of class Player206BO.
	 *
	 * @return {@link java.lang.Integer}
	 * @generated
	 */ 
public  Integer getHeight() {
	return this.height;
	}


	/**
	 * Setter for height of class Player206BO.
	 * @param value {@link java.lang.Integer}
	 * @generated
	 */

public  void setHeight(final Integer height) {
	this.height = height;
}

					/**
	 * Getter for weight of class Player206BO.
	 *
	 * @return {@link java.lang.Integer}
	 * @generated
	 */ 
public  Integer getWeight() {
	return this.weight;
	}


	/**
	 * Setter for weight of class Player206BO.
	 * @param value {@link java.lang.Integer}
	 * @generated
	 */

public  void setWeight(final Integer weight) {
	this.weight = weight;
}

					/**
	 * Getter for rookie of class Player206BO.
	 *
	 * @return {@link java.lang.Boolean}
	 * @generated
	 */ 
public  Boolean getRookie() {
	return this.rookie;
	}


	/**
	 * Setter for rookie of class Player206BO.
	 * @param value {@link java.lang.Boolean}
	 * @generated
	 */

public  void setRookie(final Boolean rookie) {
	this.rookie = rookie;
}


/**
	 * Returns <code>true</code> if the argument is an Player206 instance and all identifiers for this entity
     * equal the identifiers of the argument entity. Returns <code>false</code> otherwise.
	 * @param object  object to compare with current instance      
     * @return boolean true if equals false if not
     */
    public boolean equals(final Object object )
    {
		if ( this == object )
        {
            return true;
        }
        if ( !( object instanceof Player206BO ) )
        {
            return false;
        }
        final Player206BO that = (Player206BO)object;
    
    	            if (        				        			this.getId() == null
        	        ) {
        	return false;
        }
		return new EqualsBuilder()
			.append(this.getId(), that.getId())
			 	.appendSuper(super.equals(object))
		 	.isEquals();
	}
	/**
     * Returns a hash code based on this entity's identifiers.     * @return int
     */    
    public int hashCode() {
        return new HashCodeBuilder()
             .append(getId())
		            .toHashCode();
    }
}

