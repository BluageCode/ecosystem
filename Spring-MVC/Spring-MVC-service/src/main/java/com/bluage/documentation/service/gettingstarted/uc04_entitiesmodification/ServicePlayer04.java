/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.service.gettingstarted.uc04_entitiesmodification;


import javax.inject.Inject;
import javax.inject.Named;

import com.bluage.documentation.business.gettingstarted.uc04_entitiesmodification.bos.Player04BO;
import com.bluage.documentation.business.gettingstarted.uc04_entitiesmodification.entities.dao.Player04DAOImpl;
import com.bluage.documentation.business.gettingstarted.uc04_entitiesmodification.entities.daofinder.Player04DAOFinderImpl;
import com.bluage.documentation.service.AbstractService;
import com.netfective.bluage.core.exception.ApplicationException;

/**
 * Defines  application services for ServicePlayer04
  */
@Named("com.bluage.documentation.service.gettingstarted.uc04_entitiesmodification.ServicePlayer04")
public class ServicePlayer04  extends AbstractService{


			@Inject
		@Named ("com.bluage.documentation.business.gettingstarted.uc04_entitiesmodification.entities.dao.Player04DAOImpl")
		private Player04DAOImpl player04BODao;
			@Inject
		@Named ("com.bluage.documentation.business.gettingstarted.uc04_entitiesmodification.entities.daofinder.Player04DAOFinderImpl")
		private Player04DAOFinderImpl player04BOFinder;
	


	/**
	 *  Default constructor 
	 */
	public ServicePlayer04() {
		super(); // Call super for default constructor : ServicePlayer04
	}
	


			
				
	/**    
	 * Operation Crud
	 * @param bo {@link Player04BO}
		 * @return {@link Player04BO}
	 */
	public Player04BO player04FindByID(Player04BO bo )
 throws ApplicationException {

					return (Player04BO)player04BOFinder.findByPrimaryKey(getBoId(bo));
	}
	/**
	 *  Return the primary key from business object.
	 */
	private Object getBoId(Player04BO bo) {
				return bo.getId();
		}

					
				
	/**    
	 * Operation Crud
	 * @param bo {@link Player04BO}
		 * @return {@link boolean}
	 */
	public boolean player04Update(Player04BO bo )
 throws ApplicationException {

					return player04BODao.update(bo);
	}

		}
