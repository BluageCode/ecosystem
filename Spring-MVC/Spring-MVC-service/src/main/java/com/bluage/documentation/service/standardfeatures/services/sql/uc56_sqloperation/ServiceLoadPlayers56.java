/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.service.standardfeatures.services.sql.uc56_sqloperation;


import javax.inject.Inject;
import javax.inject.Named;

import com.bluage.documentation.business.standardfeatures.services.sql.uc56_sqloperation.bos.Team56BO;
import com.bluage.documentation.service.AbstractService;
import com.bluage.documentation.service.standardfeatures.services.sql.uc56_sqloperation.daofinder.ServiceLoadPlayers56DAOMethodFinderImpl;
import com.netfective.bluage.core.exception.ApplicationException;

/**
 * Defines  application services for ServiceLoadPlayers56
  */
@Named("com.bluage.documentation.service.standardfeatures.services.sql.uc56_sqloperation.ServiceLoadPlayers56")
public class ServiceLoadPlayers56  extends AbstractService{


						@Inject
		@Named ("com.bluage.documentation.service.standardfeatures.services.sql.uc56_sqloperation.daofinder.ServiceLoadPlayers56DAOMethodFinder")
		private ServiceLoadPlayers56DAOMethodFinderImpl serviceLoadPlayers56Dao;



	/**
	 *  Default constructor 
	 */
	public ServiceLoadPlayers56() {
		super(); // Call super for default constructor : ServiceLoadPlayers56
	}
	



			
	/**    
	 * Operation SQL.
	 * @param team {@link String}
		 * @return {@link Team56BO}
	 */
	public Team56BO loadPlayers(String team )
 throws ApplicationException {
	
	return serviceLoadPlayers56Dao.loadPlayers(team);
	}

		
			
	/**    
	 * Process operation.
	 * @param team {@link Team56BO}
		 * @return {@link Team56BO}
	 */
	public Team56BO executeLoad(Team56BO team )
 throws ApplicationException {

				return privateExecuteLoad(team);
	}
	

	/**    
	 * Process operation.
	 * @param team {@link Team56BO}
		 * @return {@link Team56BO}
	 */
	private Team56BO privateExecuteLoad(Team56BO team )
 throws ApplicationException {
						// Return Type declaration.
				Team56BO searchResult = new Team56BO();
					// Central buffer node declaration for operation executeLoad
			String	 teamName =  "";
					// Calling an operation : 
teamName = team.getName();
/* Executing : */
teamName=teamName;
// Calling an operation : 
searchResult = loadPlayers(teamName);

						return searchResult;
		}

		}
