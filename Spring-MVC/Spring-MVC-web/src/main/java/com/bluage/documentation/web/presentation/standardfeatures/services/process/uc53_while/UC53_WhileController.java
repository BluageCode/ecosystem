/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.services.process.uc53_while ;

// Import declaration.
// Java imports.
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.bluage.documentation.business.standardfeatures.services.process.uc53_while.bos.Player53BO;
import com.bluage.documentation.service.standardfeatures.services.process.uc53_while.ServicePlayer53;
import com.netfective.bluage.core.exception.ApplicationException;
import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;
import com.netfective.bluage.core.web.IForm;

/**
 * Class : UC53_WhileController
 */
@Controller
@Scope("prototype")
@SessionAttributes("uC53_WhileForm")
@RequestMapping(value= "/presentation/standardfeatures/services/process/uc53_while")
public class UC53_WhileController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC53_WhileController.class);
	
	//Current form name
	private static final String U_C53__WHILE_FORM = "uC53_WhileForm";

		//CONSTANT: player
	private static final String PLAYER = "player";
	
	/**
	 * Property:customDateEditorsUC53_WhileController
	 */
	final private Map<String,CustomDateEditor> customDateEditorsUC53_WhileController = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:uC53_WhileValidator
	 */
	final private UC53_WhileValidator uC53_WhileValidator = new UC53_WhileValidator();
	
	/**
	 * Service declaration : servicePlayer53.
	 */
	@Autowired
	private ServicePlayer53 servicePlayer53;
	
	
	// Initialise all the instances for UC53_WhileController
			// Declare the instance player
		private Player53BO player;
			/**
	 * Operation : lnk_trainPlayer
 	 * @param model : 
 	 * @param uC53_While : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC53_While/lnk_trainPlayer.html",method = RequestMethod.POST)
	public String lnk_trainPlayer(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC53_WhileForm") UC53_WhileForm  uC53_WhileForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : lnk_trainPlayer"); 
		infoLogger(uC53_WhileForm); 
		uC53_WhileForm.setAlwaysCallPreControllers(false); 
		// initialization for lnk_trainPlayer
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state lnk_trainPlayer if the validation fail, you will be forward to the screen : "/presentation/standardfeatures/services/process/uc53_while/UC53_While"
		if(errorsMessages(request,response,uC53_WhileForm,bindingResult,"", uC53_WhileValidator, customDateEditorsUC53_WhileController)){ 
			return "/presentation/standardfeatures/services/process/uc53_while/UC53_While"; 
		}

					 callTrainPlayer(request,uC53_WhileForm );
						// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.services.process.uc53_while.UC53_WhileForm or not from lnk_trainPlayer
			// Populate the destination form with all the instances returned from lnk_trainPlayer.
			final IForm uC53_WhileForm2 = populateDestinationForm(request.getSession(), uC53_WhileForm); 
					
			// Add the form to the model.
			model.addAttribute("uC53_WhileForm", uC53_WhileForm2); 
			
			request.getSession().setAttribute("uC53_WhileForm", uC53_WhileForm2);
			
			// "OK" CASE => destination screen path from lnk_trainPlayer
			LOGGER.info("Go to the screen 'UC53_While'.");
			// Redirect (PRG) from lnk_trainPlayer
			destinationPath =  "redirect:/presentation/standardfeatures/services/process/uc53_while/UC53_While.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_trainPlayer 
	}
	
				/**
	 * Operation : lnk_showPlayer
 	 * @param model : 
 	 * @param uC53_While : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC53_While/lnk_showPlayer.html",method = RequestMethod.POST)
	public String lnk_showPlayer(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC53_WhileForm") UC53_WhileForm  uC53_WhileForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : lnk_showPlayer"); 
		infoLogger(uC53_WhileForm); 
		uC53_WhileForm.setAlwaysCallPreControllers(false); 
		// initialization for lnk_showPlayer
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state lnk_showPlayer if the validation fail, you will be forward to the screen : "/presentation/standardfeatures/services/process/uc53_while/UC53_While"
		if(errorsMessages(request,response,uC53_WhileForm,bindingResult,"", uC53_WhileValidator, customDateEditorsUC53_WhileController)){ 
			return "/presentation/standardfeatures/services/process/uc53_while/UC53_While"; 
		}

	 callFindFirstPlayer(request,uC53_WhileForm );
						// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.services.process.uc53_while.UC53_WhileForm or not from lnk_showPlayer
			// Populate the destination form with all the instances returned from lnk_showPlayer.
			final IForm uC53_WhileForm2 = populateDestinationForm(request.getSession(), uC53_WhileForm); 
					
			// Add the form to the model.
			model.addAttribute("uC53_WhileForm", uC53_WhileForm2); 
			
			request.getSession().setAttribute("uC53_WhileForm", uC53_WhileForm2);
			
			// "OK" CASE => destination screen path from lnk_showPlayer
			LOGGER.info("Go to the screen 'UC53_While'.");
			// Redirect (PRG) from lnk_showPlayer
			destinationPath =  "redirect:/presentation/standardfeatures/services/process/uc53_while/UC53_While.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_showPlayer 
	}
	
	
	/**
	* This method initialise the form : UC53_WhileForm 
	* @return UC53_WhileForm
	*/
	@ModelAttribute("UC53_WhileFormInit")
	public void initUC53_WhileForm(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, U_C53__WHILE_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method UC53_WhileForm."); //for lnk_showPlayer 
		}
		UC53_WhileForm uC53_WhileForm;
	
		if(request.getSession().getAttribute(U_C53__WHILE_FORM) != null){
			uC53_WhileForm = (UC53_WhileForm)request.getSession().getAttribute(U_C53__WHILE_FORM);
		} else {
			uC53_WhileForm = new UC53_WhileForm();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form UC53_WhileForm.");
		}
		uC53_WhileForm = (UC53_WhileForm)populateDestinationForm(request.getSession(), uC53_WhileForm);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  UC53_WhileForm.");
		}
		model.addAttribute(U_C53__WHILE_FORM, uC53_WhileForm);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param UC53_WhileForm : The sceen form.
	 */
	@RequestMapping(value = "/UC53_While.html" ,method = RequestMethod.GET)
	public void prepareUC53_While(final HttpServletRequest request,final  Model model,final  @ModelAttribute("UC53_WhileFormInit") UC53_WhileForm uC53_WhileForm){
		
		UC53_WhileForm currentUC53_WhileForm = uC53_WhileForm;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(U_C53__WHILE_FORM) == null){
			if(currentUC53_WhileForm!=null){
				request.getSession().setAttribute(U_C53__WHILE_FORM, currentUC53_WhileForm);
			}else {
				currentUC53_WhileForm = new UC53_WhileForm();
				request.getSession().setAttribute(U_C53__WHILE_FORM, currentUC53_WhileForm);	
			}
		} else {
			currentUC53_WhileForm = (UC53_WhileForm) request.getSession().getAttribute(U_C53__WHILE_FORM);
		}

		currentUC53_WhileForm = (UC53_WhileForm)populateDestinationForm(request.getSession(), currentUC53_WhileForm);
		// Call all the Precontroller.
	currentUC53_WhileForm.setAlwaysCallPreControllers(true);
		model.addAttribute(U_C53__WHILE_FORM, currentUC53_WhileForm);
		request.getSession().setAttribute(U_C53__WHILE_FORM, currentUC53_WhileForm);
		
	}
	
				/**
	 * method callTrainPlayer
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callTrainPlayer(HttpServletRequest request,IForm form) {
					player = (Player53BO)get(request.getSession(),form, "player");  
										 
					try {
				// executing TrainPlayer in lnk_showPlayer
	servicePlayer53.trainPlayer(
			player
			);  

								// processing variables TrainPlayer in lnk_showPlayer
				put(request.getSession(), PLAYER,player); 
			
			} catch (ApplicationException e) { 
				// error handling for operation trainPlayer called TrainPlayer
				errorLogger("An error occured during the execution of the operation : trainPlayer",e); 
		
			}
	}
		/**
	 * method callFindFirstPlayer
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callFindFirstPlayer(HttpServletRequest request,IForm form) {
		player = (Player53BO)get(request.getSession(),form, "player");  
										 
					try {
				// executing FindFirstPlayer in lnk_showPlayer
				player = 	servicePlayer53.findFirstPlayer(
	);  
 
				put(request.getSession(), PLAYER,player);
								// processing variables FindFirstPlayer in lnk_showPlayer

			} catch (ApplicationException e) { 
				// error handling for operation findFirstPlayer called FindFirstPlayer
				errorLogger("An error occured during the execution of the operation : findFirstPlayer",e); 
		
			}
	}
		
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("UC53_WhileController [ ");
				strBToS.append(PLAYER);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(player);
			strBToS.append(END_TO_STRING_DELIMITER);
		// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(uC53_WhileValidator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsUC53_WhileController!=null); 
		return strBToS.toString();
	}
}
