/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.service.standardfeatures.entities.uc61_transientobject;


import java.util.List;

import javax.inject.Named;

import com.bluage.documentation.business.standardfeatures.entities.uc61_transientobject.bos.Team61BO;
import com.bluage.documentation.service.AbstractService;
import com.netfective.bluage.core.exception.ApplicationException;

/**
 * Defines  application services for ServiceTeam
  */
@Named("com.bluage.documentation.service.standardfeatures.entities.uc61_transientobject.ServiceTeam")
public class ServiceTeam  extends AbstractService{


																


	/**
	 *  Default constructor 
	 */
	public ServiceTeam() {
		super(); // Call super for default constructor : ServiceTeam
	}
	



			
	/**    
	 * Process operation.
	 * @param teamToAdd {@link Team61BO}
		 * @param teams {@link List}
		 * @return {@link boolean}
	 */
	public boolean checkTeamId(Team61BO teamToAdd,List teams )
 throws ApplicationException {

				return privateCheckTeamId(teamToAdd,teams);
	}
	

	/**    
	 * Process operation.
	 * @param teamToAdd {@link Team61BO}
		 * @param teams {@link List}
		 * @return {@link boolean}
	 */
	private boolean privateCheckTeamId(Team61BO teamToAdd,List teams )
 throws ApplicationException {
								// Return Type declaration.
				boolean refAlreadyExist = false;
					// Central buffer node declaration for operation checkTeamId
			Team61BO	 currentTeam =  new Team61BO();
					if (teams.size()>0) {
for (java.util.Iterator iter0 = teams.iterator(); iter0.hasNext();) {
currentTeam = (com.bluage.documentation.business.standardfeatures.entities.uc61_transientobject.bos.Team61BO)iter0.next();
if (currentTeam.getId().equals(teamToAdd.getId())) {
/* Executing : */
refAlreadyExist=true;
break;
}
}
}

						return refAlreadyExist;
		}

		
			
	/**    
	 * Process operation.
	 * @param teamToAdd {@link Team61BO}
		 * @param teams {@link List}
		 * @return {@link List}
	 */
	public List addTeam(Team61BO teamToAdd,List teams )
 throws ApplicationException {

				return privateAddTeam(teamToAdd,teams);
	}
	

	/**    
	 * Process operation.
	 * @param teamToAdd {@link Team61BO}
		 * @param teams {@link List}
		 * @return {@link List}
	 */
	private List privateAddTeam(Team61BO teamToAdd,List teams )
 throws ApplicationException {
						// Central buffer node declaration for operation addTeam
			boolean	 nameTeamExist =  false;
					boolean	 refTeamIsNull =  false;
					boolean	 refTeamExist =  false;
					// Calling an operation : 
refTeamIsNull = checkTeamIdNull(teamToAdd);
// Test on exception : 
if (refTeamIsNull==true) {
ApplicationException app = new ApplicationException("Please enter an ID for the team to create.");
throw app;
}
// Calling an operation : 
refTeamExist = checkTeamId(teamToAdd, teams);
// Test on exception : 
if (refTeamExist==true) {
ApplicationException app = new ApplicationException("A team with the same id already exists.");
throw app;
}
// Calling an operation : 
nameTeamExist = checkTeamName(teamToAdd, teams);
// Test on exception : 
if (nameTeamExist==true) {
ApplicationException app = new ApplicationException("A team with the same name already exists.");
throw app;
}
/* Executing : */
teams.add(teamToAdd);

						return teams;
		}

		
			
	/**    
	 * Process operation.
	 * @return {@link Team61BO}
	 */
	public Team61BO newTeam( )
 throws ApplicationException {

				return privateNewTeam();
	}
	

	/**    
	 * Process operation.
	 * @return {@link Team61BO}
	 */
	private Team61BO privateNewTeam( )
 throws ApplicationException {
				// Return Type declaration.
				Team61BO teamToAdd = new Team61BO();
					// Central buffer node declaration for operation newTeam
			
						return teamToAdd;
		}

		
			
	/**    
	 * Process operation.
	 * @param teamToAdd {@link Team61BO}
		 * @param teams {@link List}
		 * @return {@link boolean}
	 */
	public boolean checkTeamName(Team61BO teamToAdd,List teams )
 throws ApplicationException {

				return privateCheckTeamName(teamToAdd,teams);
	}
	

	/**    
	 * Process operation.
	 * @param teamToAdd {@link Team61BO}
		 * @param teams {@link List}
		 * @return {@link boolean}
	 */
	private boolean privateCheckTeamName(Team61BO teamToAdd,List teams )
 throws ApplicationException {
								// Return Type declaration.
				boolean nameAlreadyExist = false;
					// Central buffer node declaration for operation checkTeamName
			Team61BO	 currentTeam =  new Team61BO();
					if (teams.size()>0) {
for (java.util.Iterator iter0 = teams.iterator(); iter0.hasNext();) {
currentTeam = (com.bluage.documentation.business.standardfeatures.entities.uc61_transientobject.bos.Team61BO)iter0.next();
if (currentTeam.getName().equals(teamToAdd.getName())) {
/* Executing : */
nameAlreadyExist=true;
break;
}
}
}

						return nameAlreadyExist;
		}

		
			
	/**    
	 * Process operation.
	 * @param teamToAdd {@link Team61BO}
		 * @return {@link boolean}
	 */
	public boolean checkTeamIdNull(Team61BO teamToAdd )
 throws ApplicationException {

				return privateCheckTeamIdNull(teamToAdd);
	}
	

	/**    
	 * Process operation.
	 * @param teamToAdd {@link Team61BO}
		 * @return {@link boolean}
	 */
	private boolean privateCheckTeamIdNull(Team61BO teamToAdd )
 throws ApplicationException {
						// Return Type declaration.
				boolean refTeamIsNull = false;
					// Central buffer node declaration for operation checkTeamIdNull
			if (teamToAdd.getId()!=null && teamToAdd.getId()>0) {
/* Executing : */
refTeamIsNull=false;
} else {
/* Executing : */
refTeamIsNull=true;
}

						return refTeamIsNull;
		}

		}
