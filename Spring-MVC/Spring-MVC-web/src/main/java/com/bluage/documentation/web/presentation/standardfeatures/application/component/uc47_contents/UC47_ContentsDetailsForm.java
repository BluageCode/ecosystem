/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.application.component.uc47_contents;


import java.io.Serializable;
import java.util.List;

import com.bluage.documentation.business.standardfeatures.application.component.uc47_contents.bos.Player47BO;
import com.bluage.documentation.business.standardfeatures.application.component.uc47_contents.bos.Team47BO;
import com.netfective.bluage.core.web.AbstractForm;
import com.netfective.bluage.core.web.IForm;

/**
* Class : UC47_ContentsDetailsForm
*/
public class UC47_ContentsDetailsForm extends AbstractForm implements Serializable, IForm {
	
	/** Constant : serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	//CONSTANT : allPositions
	private static final String ALL_POSITIONS = "allPositions";
	//CONSTANT : allPlayers
	private static final String ALL_PLAYERS = "allPlayers";
	//CONSTANT : teamToAdd
	private static final String TEAM_TO_ADD = "teamToAdd";
	//CONSTANT : listTeam
	private static final String LIST_TEAM = "listTeam";
	//CONSTANT : playerToUpdate
	private static final String PLAYER_TO_UPDATE = "playerToUpdate";
	/**
	 * 	Property: allPositions 
	 */
	private List allPositions;
	/**
	 * 	Property: allPlayers 
	 */
	private List allPlayers;
	/**
	 * 	Property: teamToAdd 
	 */
	private Team47BO teamToAdd;
	/**
	 * 	Property: listTeam 
	 */
	private List listTeam;
	/**
	 * 	Property: playerToUpdate 
	 */
	private Player47BO playerToUpdate;
/**
	 * Default constructor : UC47_ContentsDetailsForm
	 */
	public UC47_ContentsDetailsForm() {
		super();
		// Initialize : allPositions
		this.allPositions = new java.util.ArrayList();
		// Initialize : allPlayers
		this.allPlayers = null;
		// Initialize : teamToAdd
		this.teamToAdd = null;
		// Initialize : listTeam
		this.listTeam = new java.util.ArrayList();
		// Initialize : playerToUpdate
		this.playerToUpdate = new Player47BO();
			this.setAlwaysCallPreControllers(true);
	}
	
	/**
	 * 	Getter : allPositions 
	 * 	@return : Return the allPositions instance.
	 */
	public List getAllPositions(){
		return allPositions; // For UC47_ContentsDetailsForm
	}
	
	/**
	 * 	Setter : allPositions 
	 *  @param allPositionsinstance : The instance to set.
	 */
	public void setAllPositions(final List allPositionsinstance){
		this.allPositions = allPositionsinstance;// For UC47_ContentsDetailsForm
	}
	/**
	 * 	Getter : allPlayers 
	 * 	@return : Return the allPlayers instance.
	 */
	public List getAllPlayers(){
		return allPlayers; // For UC47_ContentsDetailsForm
	}
	
	/**
	 * 	Setter : allPlayers 
	 *  @param allPlayersinstance : The instance to set.
	 */
	public void setAllPlayers(final List allPlayersinstance){
		this.allPlayers = allPlayersinstance;// For UC47_ContentsDetailsForm
	}
	/**
	 * 	Getter : teamToAdd 
	 * 	@return : Return the teamToAdd instance.
	 */
	public Team47BO getTeamToAdd(){
		return teamToAdd; // For UC47_ContentsDetailsForm
	}
	
	/**
	 * 	Setter : teamToAdd 
	 *  @param teamToAddinstance : The instance to set.
	 */
	public void setTeamToAdd(final Team47BO teamToAddinstance){
		this.teamToAdd = teamToAddinstance;// For UC47_ContentsDetailsForm
	}
	/**
	 * 	Getter : listTeam 
	 * 	@return : Return the listTeam instance.
	 */
	public List getListTeam(){
		return listTeam; // For UC47_ContentsDetailsForm
	}
	
	/**
	 * 	Setter : listTeam 
	 *  @param listTeaminstance : The instance to set.
	 */
	public void setListTeam(final List listTeaminstance){
		this.listTeam = listTeaminstance;// For UC47_ContentsDetailsForm
	}
	/**
	 * 	Getter : playerToUpdate 
	 * 	@return : Return the playerToUpdate instance.
	 */
	public Player47BO getPlayerToUpdate(){
		return playerToUpdate; // For UC47_ContentsDetailsForm
	}
	
	/**
	 * 	Setter : playerToUpdate 
	 *  @param playerToUpdateinstance : The instance to set.
	 */
	public void setPlayerToUpdate(final Player47BO playerToUpdateinstance){
		this.playerToUpdate = playerToUpdateinstance;// For UC47_ContentsDetailsForm
	}
/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UC47_ContentsDetailsForm [ "+
ALL_POSITIONS +" = " + allPositions +ALL_PLAYERS +" = " + allPlayers +TEAM_TO_ADD +" = " + teamToAdd +LIST_TEAM +" = " + listTeam +PLAYER_TO_UPDATE +" = " + playerToUpdate + "]";
	}
	
	/**
	 * Method setInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public void setInstanceByInstanceName(final String instanceName, final Object instance) {
		// Set the instance AllPositions.
		if(ALL_POSITIONS.equals(instanceName)){// For UC47_ContentsDetailsForm
			this.setAllPositions((List)instance); // For UC47_ContentsDetailsForm
		}
				// Set the instance AllPlayers.
		if(ALL_PLAYERS.equals(instanceName)){// For UC47_ContentsDetailsForm
			this.setAllPlayers((List)instance); // For UC47_ContentsDetailsForm
		}
				// Set the instance TeamToAdd.
		if(TEAM_TO_ADD.equals(instanceName)){// For UC47_ContentsDetailsForm
			this.setTeamToAdd((Team47BO)instance); // For UC47_ContentsDetailsForm
		}
				// Set the instance ListTeam.
		if(LIST_TEAM.equals(instanceName)){// For UC47_ContentsDetailsForm
			this.setListTeam((List)instance); // For UC47_ContentsDetailsForm
		}
				// Set the instance PlayerToUpdate.
		if(PLAYER_TO_UPDATE.equals(instanceName)){// For UC47_ContentsDetailsForm
			this.setPlayerToUpdate((Player47BO)instance); // For UC47_ContentsDetailsForm
		}
			}
	
	/**
	 * Method getInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public Object getInstanceByInstanceName(final String instanceName){
		// Temporary object for tmpUC47_ContentsDetailsForm.
		Object tmpUC47_ContentsDetailsForm = null;
		
			
		// Get the instance AllPositions for UC47_ContentsDetailsForm.
		if(ALL_POSITIONS.equals(instanceName)){ // For UC47_ContentsDetailsForm
			tmpUC47_ContentsDetailsForm = this.getAllPositions(); // For UC47_ContentsDetailsForm
		}
			
		// Get the instance AllPlayers for UC47_ContentsDetailsForm.
		if(ALL_PLAYERS.equals(instanceName)){ // For UC47_ContentsDetailsForm
			tmpUC47_ContentsDetailsForm = this.getAllPlayers(); // For UC47_ContentsDetailsForm
		}
			
		// Get the instance TeamToAdd for UC47_ContentsDetailsForm.
		if(TEAM_TO_ADD.equals(instanceName)){ // For UC47_ContentsDetailsForm
			tmpUC47_ContentsDetailsForm = this.getTeamToAdd(); // For UC47_ContentsDetailsForm
		}
			
		// Get the instance ListTeam for UC47_ContentsDetailsForm.
		if(LIST_TEAM.equals(instanceName)){ // For UC47_ContentsDetailsForm
			tmpUC47_ContentsDetailsForm = this.getListTeam(); // For UC47_ContentsDetailsForm
		}
			
		// Get the instance PlayerToUpdate for UC47_ContentsDetailsForm.
		if(PLAYER_TO_UPDATE.equals(instanceName)){ // For UC47_ContentsDetailsForm
			tmpUC47_ContentsDetailsForm = this.getPlayerToUpdate(); // For UC47_ContentsDetailsForm
		}
		return tmpUC47_ContentsDetailsForm;// For UC47_ContentsDetailsForm
	}
	
	}
