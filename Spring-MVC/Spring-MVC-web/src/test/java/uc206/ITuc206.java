package uc206;

import org.junit.Test;
import test.common.AbstractBADocTestCase;

public class ITuc206 extends AbstractBADocTestCase {
	
	@Override
	public  String getName(){
		return "UC-206";
	}

	@Test
	public void testUC206() throws Exception {
		// On teste l'UC206 - Advanced Features - Value Object.
				
		selenium.open("");	

		selenium.click("link=Expand All");
		selenium.waitForPageToLoad("300000");
		selenium.click("link=UC206 - Value Object");
		
		assertTrue(selenium.isElementPresent("//h2[contains(text(),\"Advanced Features > Value Object > UC206 > Value Object\")]"));
		// On v�rifie que la page de garde de l'UC s'affiche correctement.
		assertTrue(selenium.isElementPresent("//a[contains(text(),\"[List Players]\")]"));
		assertTrue(selenium.isElementPresent("//a[contains(text(),\"[Create a Player]\")]"));
		assertTrue(selenium.isElementPresent("//a[contains(text(),\"[Search a Player]\")]"));
		// On commence par tester [List Player] et les informations de base de l'UC.
		selenium.click("//a[contains(text(),\"[List Players]\")]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie les joueurs affich�s ainsi que les liens.
		verifyEquals("8", selenium.getXpathCount("//table[contains(@id,'tab_searchResult')]/tbody/tr"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/span[contains(text(),'Gregg')] and td/span[contains(text(),'Nash')] and td/span[contains(text(),'Point Guard')] and td/span[contains(text(),'215')] and td/a[contains(text(),'[edit]')] and td/a[contains(text(),'[details]')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/span[contains(text(),'Howell')] and td/span[contains(text(),'Floyd')] and td/span[contains(text(),'Shooting Guard')] and td/span[contains(text(),'192')] and td/a[contains(text(),'[edit]')] and td/a[contains(text(),'[details]')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/span[contains(text(),'Klein')] and td/span[contains(text(),'Simon')] and td/span[contains(text(),'Small Forward')] and td/span[contains(text(),'203')] and td/a[contains(text(),'[edit]')] and td/a[contains(text(),'[details]')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/span[contains(text(),'Montgomery')] and td/span[contains(text(),'Blake')] and td/span[contains(text(),'Small Forward')] and td/span[contains(text(),'205')] and td/a[contains(text(),'[edit]')] and td/a[contains(text(),'[details]')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/span[contains(text(),'Burns')] and td/span[contains(text(),'Doyle')] and td/span[contains(text(),'Shooting Guard')] and td/span[contains(text(),'225')] and td/a[contains(text(),'[edit]')] and td/a[contains(text(),'[details]')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/span[contains(text(),'Rowe')] and td/span[contains(text(),'Troy')] and td/span[contains(text(),'Power Forward')] and td/span[contains(text(),'211')] and td/a[contains(text(),'[edit]')] and td/a[contains(text(),'[details]')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/span[contains(text(),'Jimenez')] and td/span[contains(text(),'Jesus')] and td/span[contains(text(),'Power Forward')] and td/span[contains(text(),'159')] and td/a[contains(text(),'[edit]')] and td/a[contains(text(),'[details]')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/span[contains(text(),'Guerrero')] and td/span[contains(text(),'Ross')] and td/span[contains(text(),'Small Forward')] and td/span[contains(text(),'171')] and td/a[contains(text(),'[edit]')] and td/a[contains(text(),'[details]')]]"));
		// On edite un joueur.
		// Pour ce faire, on indexe la ligne.
		Number index = selenium.getElementIndex("//table/tbody/tr[td/span[contains(text(),'Nash')] and td/span[contains(text(),'Gregg')] and td/span[contains(text(),'Point Guard')] and td/span[contains(text(),'215')] and td/a[contains(text(),'[edit]')] and td/a[contains(text(),'[details]')]]");
		selenium.click("//table[contains(@id,'tab_searchResult')]/tbody/tr[" + index + " + 1]/td/a[contains(text(),\"[edit]\")]");
		selenium.waitForPageToLoad("30000");
		// On teste les champs vides.
		selenium.type("//input[contains(@id,'txt_lastname')]", "");
		selenium.type("//input[contains(@id,'txt_firstname')]", "");
		selenium.click("//a[contains(text(),\"[update]\")]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie que les messages d'erreur s'affichent.
		verifyEquals("8", selenium.getXpathCount("//table[contains(@id,'tab_searchResult')]/tbody/tr"));
		assertTrue(selenium.isElementPresent("//span[contains(text(),'First name is required.')]"));
		assertTrue(selenium.isElementPresent("//span[contains(text(),'Last name is required.')]"));
		// On entre le nom d'origine dans les inputs.
		selenium.type("//input[contains(@id,'txt_lastname')]", "Nash");
		selenium.type("//input[contains(@id,'txt_firstname')]", "Gregg");
		selenium.click("//a[contains(text(),\"[update]\")]");
		selenium.waitForPageToLoad("30000");
		// On indexe la ligne que l'on vient de modifier.
		Number index1 = selenium.getElementIndex("//table/tbody/tr[td/span[contains(text(),'Nash')] and td/span[contains(text(),'Gregg')] and td/span[contains(text(),'Point Guard')] and td/span[contains(text(),'215')] and td/a[contains(text(),'[edit]')] and td/a[contains(text(),'[details]')]]");
		selenium.click("//table[contains(@id,'tab_searchResult')]/tbody/tr[" + index1 + " + 1]/td/a[contains(text(),\"[edit]\")]");
		selenium.waitForPageToLoad("30000");
		// On met un autre nom.
		selenium.type("//input[contains(@id,'txt_lastname')]", "Jordan");
		selenium.type("//input[contains(@id,'txt_firstname')]", "Michael");
		selenium.click("//a[contains(text(),\"[update]\")]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie que la ligne s'affiche correctement.
		verifyEquals("8", selenium.getXpathCount("//table[contains(@id,'tab_searchResult')]/tbody/tr"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/span[contains(text(),'Jordan')] and td/span[contains(text(),'Michael')] and td/span[contains(text(),'Point Guard')] and td/span[contains(text(),'215')] and td/a[contains(text(),'[edit]')] and td/a[contains(text(),'[details]')]]"));
		// On teste les d�tails.
		// Pour ce faire on indexe chaque ligne.
		// Puis on v�rifie les d�tails pour chaque joueur.
		// Pour Michael Jordan :
		Number indexJM = selenium.getElementIndex("//table/tbody/tr[td/span[contains(text(),'Jordan')] and td/span[contains(text(),'Michael')] and td/span[contains(text(),'Point Guard')] and td/span[contains(text(),'215')] and td/a[contains(text(),'[edit]')] and td/a[contains(text(),'[details]')]]");
		selenium.click("//table[contains(@id,'tab_searchResult')]/tbody/tr[" + indexJM + " + 1]/td/a[contains(text(),\"[details]\")]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie les d�tails de MJ :
		assertTrue(selenium.isElementPresent("//h2[contains(text(),\"Value Object\")]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/label[contains(text(),'First Name')] and td/span[contains(text(),'Michael')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/label[contains(text(),'Last Name')] and td/span[contains(text(),'Jordan')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/label[contains(text(),'Date of Birth')] and td/span[contains(text(),'01/11/1972')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/label[contains(text(),'Estimated value')] and td/span[contains(text(),'7')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/label[contains(text(),'Weight')] and td/span[contains(text(),'147')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/label[contains(text(),'Height')] and td/span[contains(text(),'215')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/label[contains(text(),'Position')] and td/span[contains(text(),'Point Guard')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/label[contains(text(),'Rookie')] and td/input[not(@checked)]]"));
		// On retourne sur la page de garde de l'UC.
		selenium.click("//a[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
		// Pour Floyd Howell :
		Number indexHF = selenium.getElementIndex("//table/tbody/tr[td/span[contains(text(),'Howell')] and td/span[contains(text(),'Floyd')] and td/span[contains(text(),'Shooting Guard')] and td/span[contains(text(),'192')] and td/a[contains(text(),'[edit]')] and td/a[contains(text(),'[details]')]]");
		selenium.click("//table[contains(@id,'tab_searchResult')]/tbody/tr[" + indexHF + " + 1]/td/a[contains(text(),\"[details]\")]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie les d�tails de FH :
		assertTrue(selenium.isElementPresent("//h2[contains(text(),\"Value Object\")]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/label[contains(text(),'First Name')] and td/span[contains(text(),'Floyd')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/label[contains(text(),'Last Name')] and td/span[contains(text(),'Howell')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/label[contains(text(),'Date of Birth')] and td/span[contains(text(),'08/25/1979')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/label[contains(text(),'Estimated value')] and td/span[contains(text(),'26')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/label[contains(text(),'Weight')] and td/span[contains(text(),'155')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/label[contains(text(),'Height')] and td/span[contains(text(),'192')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/label[contains(text(),'Position')] and td/span[contains(text(),'Shooting Guard')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/label[contains(text(),'Rookie')] and td/input[not(@checked)]]"));
		// On retourne sur la page de garde de l'UC.
		selenium.click("//a[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
		// Pour Simon Klein :
		Number indexKS = selenium.getElementIndex("//table/tbody/tr[td/span[contains(text(),'Klein')] and td/span[contains(text(),'Simon')] and td/span[contains(text(),'Small Forward')] and td/span[contains(text(),'203')] and td/a[contains(text(),'[edit]')] and td/a[contains(text(),'[details]')]]");
		selenium.click("//table[contains(@id,'tab_searchResult')]/tbody/tr[" + indexKS + " + 1]/td/a[contains(text(),\"[details]\")]");
		selenium.waitForPageToLoad("30000");
		Thread.sleep(1000);
		// On v�rifie les d�tails de SK :
		assertTrue(selenium.isElementPresent("//h2[contains(text(),\"Value Object\")]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/label[contains(text(),'First Name')] and td/span[contains(text(),'Simon')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/label[contains(text(),'Last Name')] and td/span[contains(text(),'Klein')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/label[contains(text(),'Date of Birth')] and td/span[contains(text(),'01/21/1987')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/label[contains(text(),'Estimated value')] and td/span[contains(text(),'22')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/label[contains(text(),'Weight')] and td/span[contains(text(),'63')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/label[contains(text(),'Height')] and td/span[contains(text(),'203')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/label[contains(text(),'Position')] and td/span[contains(text(),'Small Forward')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/label[contains(text(),'Rookie')] and td/input[not(@checked)]]"));
		// On retourne sur la page de garde de l'UC.
		selenium.click("//a[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
		// Pour Blake Montgomery :
		Number indexMB = selenium.getElementIndex("//table/tbody/tr[td/span[contains(text(),'Montgomery')] and td/span[contains(text(),'Blake')] and td/span[contains(text(),'Small Forward')] and td/span[contains(text(),'205')] and td/a[contains(text(),'[edit]')] and td/a[contains(text(),'[details]')]]");
		selenium.click("//table[contains(@id,'tab_searchResult')]/tbody/tr[" + indexMB + " + 1]/td/a[contains(text(),\"[details]\")]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie les d�tails de BM :
		assertTrue(selenium.isElementPresent("//h2[contains(text(),\"Value Object\")]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/label[contains(text(),'First Name')] and td/span[contains(text(),'Blake')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/label[contains(text(),'Last Name')] and td/span[contains(text(),'Montgomery')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/label[contains(text(),'Date of Birth')] and td/span[contains(text(),'11/16/1992')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/label[contains(text(),'Estimated value')] and td/span[contains(text(),'37')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/label[contains(text(),'Weight')] and td/span[contains(text(),'113')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/label[contains(text(),'Height')] and td/span[contains(text(),'205')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/label[contains(text(),'Position')] and td/span[contains(text(),'Small Forward')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/label[contains(text(),'Rookie')] and td/input[(@checked)]]"));
		// On retourne sur la page de garde de l'UC.
		selenium.click("//a[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
		// Pour Doyle Burns :
		Number indexBD = selenium.getElementIndex("//table/tbody/tr[td/span[contains(text(),'Burns')] and td/span[contains(text(),'Doyle')] and td/span[contains(text(),'Shooting Guard')] and td/span[contains(text(),'225')] and td/a[contains(text(),'[edit]')] and td/a[contains(text(),'[details]')]]");
		selenium.click("//table[contains(@id,'tab_searchResult')]/tbody/tr[" + indexBD + " + 1]/td/a[contains(text(),\"[details]\")]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie les d�tails de DB :
		assertTrue(selenium.isElementPresent("//h2[contains(text(),\"Value Object\")]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/label[contains(text(),'First Name')] and td/span[contains(text(),'Doyle')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/label[contains(text(),'Last Name')] and td/span[contains(text(),'Burns')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/label[contains(text(),'Date of Birth')] and td/span[contains(text(),'02/23/1990')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/label[contains(text(),'Estimated value')] and td/span[contains(text(),'39')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/label[contains(text(),'Weight')] and td/span[contains(text(),'153')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/label[contains(text(),'Height')] and td/span[contains(text(),'225')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/label[contains(text(),'Position')] and td/span[contains(text(),'Shooting Guard')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/label[contains(text(),'Rookie')] and td/input[(@checked)]]"));
		// On retourne sur la page de garde de l'UC.
		selenium.click("//a[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
		// Pour Troy Rowe :
		Number indexRT = selenium.getElementIndex("//table/tbody/tr[td/span[contains(text(),'Rowe')] and td/span[contains(text(),'Troy')] and td/span[contains(text(),'Power Forward')] and td/span[contains(text(),'211')] and td/a[contains(text(),'[edit]')] and td/a[contains(text(),'[details]')]]");
		selenium.click("//table[contains(@id,'tab_searchResult')]/tbody/tr[" + indexRT + " + 1]/td/a[contains(text(),\"[details]\")]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie les d�tails de TR :
		assertTrue(selenium.isElementPresent("//h2[contains(text(),\"Value Object\")]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/label[contains(text(),'First Name')] and td/span[contains(text(),'Troy')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/label[contains(text(),'Last Name')] and td/span[contains(text(),'Rowe')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/label[contains(text(),'Date of Birth')] and td/span[contains(text(),'02/12/1973')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/label[contains(text(),'Estimated value')] and td/span[contains(text(),'27')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/label[contains(text(),'Weight')] and td/span[contains(text(),'94')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/label[contains(text(),'Height')] and td/span[contains(text(),'211')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/label[contains(text(),'Position')] and td/span[contains(text(),'Power Forward')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/label[contains(text(),'Rookie')] and td/input[not(@checked)]]"));
		// On retourne sur la page de garde de l'UC.
		selenium.click("//a[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
		// Pour Jesus Jimenez : 
		Number indexJJ = selenium.getElementIndex("//table/tbody/tr[td/span[contains(text(),'Jimenez')] and td/span[contains(text(),'Jesus')] and td/span[contains(text(),'Power Forward')] and td/span[contains(text(),'159')] and td/a[contains(text(),'[edit]')] and td/a[contains(text(),'[details]')]]");
		selenium.click("//table[contains(@id,'tab_searchResult')]/tbody/tr[" + indexJJ + " + 1]/td/a[contains(text(),\"[details]\")]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie les d�tails de JJ :
		assertTrue(selenium.isElementPresent("//h2[contains(text(),\"Value Object\")]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/label[contains(text(),'First Name')] and td/span[contains(text(),'Jesus')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/label[contains(text(),'Last Name')] and td/span[contains(text(),'Jimenez')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/label[contains(text(),'Date of Birth')] and td/span[contains(text(),'09/01/1985')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/label[contains(text(),'Estimated value')] and td/span[contains(text(),'18')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/label[contains(text(),'Weight')] and td/span[contains(text(),'133')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/label[contains(text(),'Height')] and td/span[contains(text(),'159')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/label[contains(text(),'Position')] and td/span[contains(text(),'Power Forward')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/label[contains(text(),'Rookie')] and td/input[not(@checked)]]"));
		// On retourne sur la page de garde de l'UC.
		selenium.click("//a[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
		// Pour Ross Guerrero :
		Number indexGR = selenium.getElementIndex("//table/tbody/tr[td/span[contains(text(),'Guerrero')] and td/span[contains(text(),'Ross')] and td/span[contains(text(),'Small Forward')] and td/span[contains(text(),'171')] and td/a[contains(text(),'[edit]')] and td/a[contains(text(),'[details]')]]");
		selenium.click("//table[contains(@id,'tab_searchResult')]/tbody/tr[" + indexGR + " + 1]/td/a[contains(text(),\"[details]\")]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie les d�tails de RG :
		assertTrue(selenium.isElementPresent("//h2[contains(text(),\"Value Object\")]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/label[contains(text(),'First Name')] and td/span[contains(text(),'Ross')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/label[contains(text(),'Last Name')] and td/span[contains(text(),'Guerrero')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/label[contains(text(),'Date of Birth')] and td/span[contains(text(),'01/26/1992')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/label[contains(text(),'Estimated value')] and td/span[contains(text(),'14')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/label[contains(text(),'Weight')] and td/span[contains(text(),'136')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/label[contains(text(),'Height')] and td/span[contains(text(),'171')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/label[contains(text(),'Position')] and td/span[contains(text(),'Small Forward')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/label[contains(text(),'Rookie')] and td/input[(@checked)]]"));
		// On retourne sur la page de garde de l'UC.
		selenium.click("//a[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
		selenium.click("//a[contains(text(),'[back]')]");
		selenium.waitForPageToLoad("30000");
		// On teste le create player.
		selenium.click("//a[contains(text(),\"[Create a Player]\")]");
		selenium.waitForPageToLoad("30000");
		// On rempli les champs avec des suites de caract�res invalides 
		selenium.type("//*[contains(@id,'txt_firstname')]", "");
		selenium.type("//*[contains(@id,'txt_lastname')]", "");
		selenium.type("//*[contains(@id,'txt_dateofbirth')]", "14/12/1980");
		selenium.type("//*[contains(@id,'txt_weight')]", "20");
		selenium.type("//*[contains(@id,'txt_height')]", "10");
		selenium.click("//*[contains(@id,'lnk_create')]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie que les messages d'erreurs s'affichent.
		assertTrue(selenium.isElementPresent("//span[contains(text(),'First name is required.')]"));
		assertTrue(selenium.isElementPresent("//span[contains(text(),'Last name is required.')]"));
		assertTrue(selenium.isElementPresent("//span[contains(text(),'Invalid format.')]"));
		assertTrue(selenium.isElementPresent("//span[contains(text(),'Player weight should be between 60 and 140 kgs.')]"));
		assertTrue(selenium.isElementPresent("//span[contains(text(),'Player height should be between 150 and 240 cms.')]"));
		selenium.type("//*[contains(@id,'txt_firstname')]", "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
		selenium.type("//*[contains(@id,'txt_lastname')]", "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
		selenium.type("//*[contains(@id,'txt_dateofbirth')]", "");
		selenium.click("//*[contains(@id,'lnk_create')]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//span[contains(text(),'First name should be less than 30 characters long.')]"));
		assertTrue(selenium.isElementPresent("//span[contains(text(),'Last name should be less than 30 characters long.')]"));
		// On cr�e un joueur.
		selenium.type("//*[contains(@id,'txt_firstname')]", "Jean");
		selenium.type("//*[contains(@id,'txt_lastname')]", "Pierre");
		selenium.type("//*[contains(@id,'txt_dateofbirth')]", "12/14/1985");
		selenium.type("//*[contains(@id,'txt_estimatedvalue')]", "27");
		selenium.type("//*[contains(@id,'txt_weight')]", "130");
		selenium.type("//*[contains(@id,'txt_height')]", "194");
		selenium.click("//*[contains(@id,'lnk_create')]");
		selenium.waitForPageToLoad("30000");
		// On va v�rifier que le joueur est bien cr��.
		selenium.click("//a[contains(text(),\"[List Players]\")]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("9", selenium.getXpathCount("//table[contains(@id,'tab_searchResult')]/tbody/tr"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/span[contains(text(),'Michael')] and td/span[contains(text(),'Jordan')] and td/span[contains(text(),'Point Guard')] and td/span[contains(text(),'215')] and td/a[contains(text(),'[edit]')] and td/a[contains(text(),'[details]')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/span[contains(text(),'Howell')] and td/span[contains(text(),'Floyd')] and td/span[contains(text(),'Shooting Guard')] and td/span[contains(text(),'192')] and td/a[contains(text(),'[edit]')] and td/a[contains(text(),'[details]')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/span[contains(text(),'Klein')] and td/span[contains(text(),'Simon')] and td/span[contains(text(),'Small Forward')] and td/span[contains(text(),'203')] and td/a[contains(text(),'[edit]')] and td/a[contains(text(),'[details]')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/span[contains(text(),'Montgomery')] and td/span[contains(text(),'Blake')] and td/span[contains(text(),'Small Forward')] and td/span[contains(text(),'205')] and td/a[contains(text(),'[edit]')] and td/a[contains(text(),'[details]')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/span[contains(text(),'Burns')] and td/span[contains(text(),'Doyle')] and td/span[contains(text(),'Shooting Guard')] and td/span[contains(text(),'225')] and td/a[contains(text(),'[edit]')] and td/a[contains(text(),'[details]')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/span[contains(text(),'Rowe')] and td/span[contains(text(),'Troy')] and td/span[contains(text(),'Power Forward')] and td/span[contains(text(),'211')] and td/a[contains(text(),'[edit]')] and td/a[contains(text(),'[details]')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/span[contains(text(),'Jimenez')] and td/span[contains(text(),'Jesus')] and td/span[contains(text(),'Power Forward')] and td/span[contains(text(),'159')] and td/a[contains(text(),'[edit]')] and td/a[contains(text(),'[details]')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/span[contains(text(),'Guerrero')] and td/span[contains(text(),'Ross')] and td/span[contains(text(),'Small Forward')] and td/span[contains(text(),'171')] and td/a[contains(text(),'[edit]')] and td/a[contains(text(),'[details]')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/span[contains(text(),'Pierre')] and td/span[contains(text(),'Jean')] and td/span[contains(text(),'Point Guard')] and td/span[contains(text(),'194')] and td/a[contains(text(),'[edit]')] and td/a[contains(text(),'[details]')]]"));
		// On indexe la ligne de Jean Pierre
		Number indexJP = selenium.getElementIndex("//table/tbody/tr[td/span[contains(text(),'Pierre')] and td/span[contains(text(),'Jean')] and td/span[contains(text(),'Point Guard')] and td/span[contains(text(),'194')] and td/a[contains(text(),'[edit]')] and td/a[contains(text(),'[details]')]]");
		selenium.click("//table[contains(@id,'tab_searchResult')]/tbody/tr[" + indexJP + " + 1]/td/a[contains(text(),\"[details]\")]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie que les donn�es affich�es correspondent.
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/label[contains(text(),'First Name')] and td/span[contains(text(),'Jean')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/label[contains(text(),'Last Name')] and td/span[contains(text(),'Pierre')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/label[contains(text(),'Date of Birth')] and td/span[contains(text(),'12/14/1985')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/label[contains(text(),'Estimated value')] and td/span[contains(text(),'27')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/label[contains(text(),'Weight')] and td/span[contains(text(),'130')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/label[contains(text(),'Height')] and td/span[contains(text(),'194')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/label[contains(text(),'Position')] and td/span[contains(text(),'Point Guard')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/label[contains(text(),'Rookie')] and td/input[not(@checked)]]"));
		// On retourne sur la page de garde de l'UC.
		selenium.click("//a[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
		selenium.click("//a[contains(text(),'[back]')]");
		selenium.waitForPageToLoad("30000");
		// On test Search a Player :
		selenium.click("//a[contains(text(),\"[Search a Player]\")]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//h2[contains(text(),\"Value Object\")]"));
		// On v�rifie l'affichage :
		assertTrue(selenium.isElementPresent("//table/tbody[tr/td/label[contains(text(),'First Name')] and tr/td/label[contains(text(),'Last Name')] and tr/td/label[contains(text(),'Position')] and tr/td/label[contains(text(),'Rookie')] and tr/td/label[contains(text(),'Last Name begins with')]]"));
		// On teste la recherche par positions :
		selenium.select("//select[contains(@id,'select_position')]", "label=Point Guard");
		selenium.click("//a[contains(text(),'[Search with above properties')]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie l'affichage du tableau.
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_searchResult')]/thead/tr[th[contains(text(),'Last Name')] and th[contains(text(),'First Name')] and th[contains(text(),'Position')] and th[contains(text(),'Height (cm)')]]"));
		verifyEquals("2", selenium.getXpathCount("//table[contains(@id,'tab_searchResult')]/tbody/tr"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/span[contains(text(),'Michael')] and td/span[contains(text(),'Jordan')] and td/span[contains(text(),'Point Guard')] and td/span[contains(text(),'215')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/span[contains(text(),'Pierre')] and td/span[contains(text(),'Jean')] and td/span[contains(text(),'Point Guard')] and td/span[contains(text(),'194')]]"));
		// On teste une recherche avec rookie.
		selenium.select("//select[contains(@id,'select_position')]", "label=Point Guard");
		selenium.click("//*[contains(@id,'chk_rookie')]");
		selenium.click("//a[contains(text(),'[Search with above properties')]");
		selenium.waitForPageToLoad("30000");
		assertFalse(selenium.isElementPresent("//table[contains(@id,'myForm:tab_searchResult')]/tbody/tr | //table[contains(@id,'tab_searchResult_empty')]/tbody/tr/span"));
		// On change de position :
		selenium.select("//select[contains(@id,'select_position')]", "label=Shooting Guard");
		selenium.click("//*[contains(@id,'chk_rookie')]");
		selenium.click("//a[contains(text(),'[Search with above properties')]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("1", selenium.getXpathCount("//table[contains(@id,'tab_searchResult')]/tbody/tr"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/span[contains(text(),'Howell')] and td/span[contains(text(),'Floyd')] and td/span[contains(text(),'Shooting Guard')] and td/span[contains(text(),'192')]]"));
		// On teste une recherche avec rookie.
		selenium.select("//select[contains(@id,'select_position')]", "label=Shooting Guard");
		selenium.click("//*[contains(@id,'chk_rookie')]");
		selenium.click("//a[contains(text(),'[Search with above properties')]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("1", selenium.getXpathCount("//table[contains(@id,'tab_searchResult')]/tbody/tr"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/span[contains(text(),'Burns')] and td/span[contains(text(),'Doyle')] and td/span[contains(text(),'Shooting Guard')] and td/span[contains(text(),'225')]]"));
		// On teste la recherche de nom
		selenium.type("//input[contains(@id,'txt_namebeginswith')]", "J");
		selenium.click("//a[contains(text(),'[Search with an HQL operation]')]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("2", selenium.getXpathCount("//table[contains(@id,'tab_searchResult')]/tbody/tr"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/span[contains(text(),'Jesus')] and td/span[contains(text(),'Jimenez')] and td/span[contains(text(),'Power Forward')] and td/span[contains(text(),'159')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/span[contains(text(),'Jordan')] and td/span[contains(text(),'Michael')] and td/span[contains(text(),'Point Guard')] and td/span[contains(text(),'215')]]"));
		selenium.type("//input[contains(@id,'txt_namebeginswith')]", "Pierre");
		selenium.click("//a[contains(text(),'[Search with an HQL operation]')]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("1", selenium.getXpathCount("//table[contains(@id,'tab_searchResult')]/tbody/tr"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/span[contains(text(),'Jean')] and td/span[contains(text(),'Pierre')] and td/span[contains(text(),'Point Guard')] and td/span[contains(text(),'194')]]"));
		selenium.click("//a[contains(@id,'lnk_homePage')]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie que la page de garde de l'UC s'affiche correctement.
		assertTrue(selenium.isElementPresent("//a[contains(text(),\"[List Players]\")]"));
		assertTrue(selenium.isElementPresent("//a[contains(text(),\"[Create a Player]\")]"));
		assertTrue(selenium.isElementPresent("//a[contains(text(),\"[Search a Player]\")]"));
	}


}
