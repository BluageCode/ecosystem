<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file="/include.jsp"  %>
<html >
<head >
<title  > 
<fmt:message key="cUC63_Home_BLU_AGE_DOCUMENTATION"/>
</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"  > 
</meta>
<link href="<c:url value= "/presentation/_style/_style.css"/>" rel="stylesheet" type="text/css"  /> 
<link href="<c:url value= "/presentation/_include/jquery/rd_lib.css"/>" rel="stylesheet" type="text/css"  /> 
<script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/jquery.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/rd_setup.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/rd_lib.js"/>" space="preserve"  ></script><link rel="shortcut icon" href="<c:url value= "/presentation/_style/favicon.ico"/>"  /> 

</head>

<body class=" " > 
<div id="rd_ghost"  > 
<small  > 
<fmt:message key="cUC63_Home___CSS_DEACTIVATED"/>
</small>
</div>
<div id="rd_jghost"  > 
<small  > 
<fmt:message key="cUC63_Home___JAVASCRIPT_DEACTIVATED"/>
</small>
</div>
<div id="shape"  > 
<div class="ui-layout-center"  > 
<div class="east-north"  > 
	<form:form id="uC63_Home" commandName="uC63_HomeForm"  enctype="application/x-www-form-urlencoded" method="post">
<h2 class="rd_first"  > 
<fmt:message key="cUC63_Home_STANDARD_FEATURES__ENTITIES_"/>
</h2>
<h4  > 
<fmt:message key="cUC63_Home_TEAMS"/>
</h4>
<display:table uid="tab_teams" pagesize="4"  sort="list" requestURI="" name="sessionScope.uC63_HomeForm.allTeams" class="rd_tabledata"  >
	<c:set   var="rownum">
		<%=(tab_teams !=	null) ? new Integer(tab_teams_rowNum.intValue() - 1) : new Integer(0) %>
	</c:set>
	<c:set   var="idRow">
		<%=(tab_teams != null ) ? tab_teams_rowNum.intValue() - 1 : -1 %>
	</c:set>
	<c:set   var="idTab">
		<%=(tab_teams != null ) ? "tab_teams" : null %>
	</c:set>
	<c:set var="tableHashId">
		<%= new org.displaytag.util.ParamEncoder("tab_teams").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE) %>
	</c:set>
	<c:set var="tableHashId2">
		<%=request.getParameter(new org.displaytag.util.ParamEncoder("tab_teams").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE)) %>
	</c:set>	
	
<display:column format=""  titleKey="cUC63_Home_NAME">
		<span id="dyn_name">
	<c:out value="${tab_teams.name}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="cUC63_Home_CITY">
		<span id="dyn_city">
	<c:out value="${tab_teams.city}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="cUC63_Home_COACH">
		<span id="dyn_coach">
	<c:out value="${tab_teams.coach.firstName}" />
		</span>
	<fmt:message key="cUC63_Home__"/>
		<span id="dyn_coachname">
	<c:out value="${tab_teams.coach.lastName}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="cUC63_Home_STATE">
		<span id="dyn_statename">
	<c:out value="${tab_teams.state.name}" />
		</span>
	</display:column>
</display:table>
<h4  > 
<fmt:message key="cUC63_Home_COACHS"/>
</h4>
<display:table uid="tab_coach" pagesize="4"  sort="list" requestURI="" name="sessionScope.uC63_HomeForm.allCoachs" class="rd_tabledata"  >
	<c:set   var="rownum">
		<%=(tab_coach !=	null) ? new Integer(tab_coach_rowNum.intValue() - 1) : new Integer(0) %>
	</c:set>
	<c:set   var="idRow">
		<%=(tab_coach != null ) ? tab_coach_rowNum.intValue() - 1 : -1 %>
	</c:set>
	<c:set   var="idTab">
		<%=(tab_coach != null ) ? "tab_coach" : null %>
	</c:set>
	<c:set var="tableHashId">
		<%= new org.displaytag.util.ParamEncoder("tab_coach").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE) %>
	</c:set>
	<c:set var="tableHashId2">
		<%=request.getParameter(new org.displaytag.util.ParamEncoder("tab_coach").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE)) %>
	</c:set>	
	
<display:column format=""  titleKey="cUC63_Home_NAME_147">
		<span id="dyn_coach">
	<c:out value="${tab_coach.firstName}" />
		</span>
	<fmt:message key="cUC63_Home___148"/>
		<span id="dyn_coachname">
	<c:out value="${tab_coach.lastName}" />
		</span>
	</display:column>
</display:table>
<h4  > 
<fmt:message key="cUC63_Home_STATES"/>
</h4>
<display:table uid="tab_states" pagesize="4"  sort="list" requestURI="" name="sessionScope.uC63_HomeForm.allStates" class="rd_tabledata"  >
	<c:set   var="rownum">
		<%=(tab_states !=	null) ? new Integer(tab_states_rowNum.intValue() - 1) : new Integer(0) %>
	</c:set>
	<c:set   var="idRow">
		<%=(tab_states != null ) ? tab_states_rowNum.intValue() - 1 : -1 %>
	</c:set>
	<c:set   var="idTab">
		<%=(tab_states != null ) ? "tab_states" : null %>
	</c:set>
	<c:set var="tableHashId">
		<%= new org.displaytag.util.ParamEncoder("tab_states").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE) %>
	</c:set>
	<c:set var="tableHashId2">
		<%=request.getParameter(new org.displaytag.util.ParamEncoder("tab_states").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE)) %>
	</c:set>	
	
<display:column format=""  titleKey="cUC63_Home_NAME_149">
		<span id="dyn_coach">
	<c:out value="${tab_states.name}" />
		</span>
	</display:column>
</display:table>
<h4  > 
<fmt:message key="cUC63_Home_STADIUMS"/>
</h4>
<display:table uid="tab_stadiums" pagesize="4"  sort="list" requestURI="" name="sessionScope.uC63_HomeForm.allStadiums" class="rd_tabledata"  >
	<c:set   var="rownum">
		<%=(tab_stadiums !=	null) ? new Integer(tab_stadiums_rowNum.intValue() - 1) : new Integer(0) %>
	</c:set>
	<c:set   var="idRow">
		<%=(tab_stadiums != null ) ? tab_stadiums_rowNum.intValue() - 1 : -1 %>
	</c:set>
	<c:set   var="idTab">
		<%=(tab_stadiums != null ) ? "tab_stadiums" : null %>
	</c:set>
	<c:set var="tableHashId">
		<%= new org.displaytag.util.ParamEncoder("tab_stadiums").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE) %>
	</c:set>
	<c:set var="tableHashId2">
		<%=request.getParameter(new org.displaytag.util.ParamEncoder("tab_stadiums").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE)) %>
	</c:set>	
	
<display:column format=""  titleKey="cUC63_Home_NAME_150">
		<span id="dyn_coach">
	<c:out value="${tab_stadiums.name}" />
		</span>
	</display:column>
</display:table>
<div style="visibility:hidden;"  > 
<a id="lien_cache"onclick="javascript:btn_click_cExecuteUC63_Home(this.href);return false;"				href="<c:url value="/presentation/standardfeatures/entities/uc63_association/UC63_Home/lien_cache.html"/>"
							 shape="rect"  >									 
<fmt:message key="cUC63_Home_HOME"/>
</a>
</div>
	</form:form>
	<script language="javascript">
		function btn_click_cExecuteUC63_Home(url) {
			UC63_Homevar = document.getElementById('uC63_Home');
			UC63_Homevar.setAttribute('action', url);
			UC63_Homevar.submit();
		}
		function btn_click_cSetActionUC63_Home(url) {
			UC63_Homevar = document.getElementById('uC63_Home');
			UC63_Homevar.setAttribute('action', url);
		}
	</script>
</div>
<div class="east-center"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/entities/uc63_association/UC63_Homecmp_tabs" />
</div>
</div>
<div class="ui-layout-north"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/entities/uc63_association/UC63_Homecmp_header" />
</div>
<div class="ui-layout-south"  > 
<fmt:message key="cUC63_Home___"/>
<span id="rd_jversion"  > 
<fmt:message key="cUC63_Home_BLU_AGE_DOCUMENTATION_151"/>
</span>
</div>
<div class="ui-layout-west"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/entities/uc63_association/UC63_Homecmp_treeview" />
</div>
</div>
</body>
</html>
