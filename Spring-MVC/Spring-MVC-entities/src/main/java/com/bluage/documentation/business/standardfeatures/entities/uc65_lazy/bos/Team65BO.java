/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.business.standardfeatures.entities.uc65_lazy.bos;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.inject.Inject;
import javax.inject.Named;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.LazyInitializationException;

import com.bluage.documentation.business.common.bos.TeamBO;
import com.bluage.documentation.business.standardfeatures.entities.uc65_lazy.entities.daofinder.Team65DAOFinderImpl;
import com.netfective.bluage.core.exception.ApplicationException;

/**
 * Team65BO
 *
* @see com.netfective.bluage.core.business.IBusinessObject
* * @see TeamBO
 */
@XmlRootElement

public class Team65BO
 extends TeamBO {
	
	/**
	 * Default SUID for Team65BO.
	 */
	private static final long serialVersionUID = 1L;
	

	@Inject
	@Named ("com.bluage.documentation.business.standardfeatures.entities.uc65_lazy.entities.daofinder.Team65Finder") 
	private Team65DAOFinderImpl finder;

																			/**
	 * Entity property: professionals
	 *
	 */
private Set<Professional65BO> professionals = new HashSet<Professional65BO>();

																		/**
	 * Entity property: rookies
	 *
	 */
private Set<Rookie65BO> rookies = new HashSet<Rookie65BO>();



   /**
	* public default constructor for class Team65BO
	*/
	public Team65BO(){
		//NOP Team65BO
	}


																			/**
	 * Getter for professionals of class Team65BO.
	 *
	 * @return {@link java.util.Set<com.bluage.documentation.business.standardfeatures.entities.uc65_lazy.bos.Professional65BO>}
	 * @generated
	 */ 
public  Set<Professional65BO> getProfessionals() {
	return this.professionals;
			}


	/**
	 * Setter for professionals of class Team65BO.
	 * @param value {@link java.util.Set<com.bluage.documentation.business.standardfeatures.entities.uc65_lazy.bos.Professional65BO>}
	 * @generated
	 */

public  void setProfessionals(final Set<Professional65BO> professionals) {
	this.professionals = professionals;
}

	/**
	 * Remove an entity from the com.bluage.documentation.business.standardfeatures.entities.uc65_lazy.bos.Professional65BO collection.
	 *
	 * @param entity
	 *		: {@link Set<Professional65BO>} the entity
	 * @throws ApplicationException
     */
public void removeFromProfessionals( Professional65BO entity ) throws ApplicationException {
	try {
		if( getProfessionals() != null && getProfessionals().contains(entity) ) {
			getProfessionals().remove( entity );
		entity.setTeam65( null );
		}
		} catch ( LazyInitializationException e ) {
				Team65BO bo = ( Team65BO ) finder.findByPrimaryKey( getId() );
			finder.getHibernateTemplate().initialize( bo.getProfessionals() );
			setProfessionals( bo.getProfessionals() );
			// evict object from session to avoid NonUniqueObjectException
			finder.getHibernateTemplate().evict( bo );
		
			if( getProfessionals() != null && getProfessionals().contains( entity ) ) {
				getProfessionals().remove( entity );
				entity.setTeam65( null );
			}
				}
	}
	
	/**
	 * Add entity to the com.bluage.documentation.business.standardfeatures.entities.uc65_lazy.bos.Professional65BO collection.
	 *
	 * @param entity
	 *		{@link Professional65BO} the entity
	 * @throws ApplicationException
     */
public void addToProfessionals( Professional65BO entity ) throws ApplicationException
	{
		try {
			if( getProfessionals() != null && !getProfessionals().contains( entity ) ) {
				getProfessionals().add( entity );
				entity.setTeam65( this );
			}
			} catch ( LazyInitializationException e ) {
					Team65BO bo = ( Team65BO ) finder.findByPrimaryKey( getId() );
		finder.getHibernateTemplate().initialize( bo.getProfessionals() );
		setProfessionals( bo.getProfessionals() );
		// evict object from session to avoid NonUniqueObjectException
		finder.getHibernateTemplate().evict( bo );
		if( getProfessionals() != null && !getProfessionals().contains( entity ) ) {
			getProfessionals().add( entity );
			entity.setTeam65( this );
		}
			}
	}
	
	/**
	 * Add entitys to the com.bluage.documentation.business.standardfeatures.entities.uc65_lazy.bos.Professional65BO collection
	 *
	 * @param entities			
	 *		: {@link java.util.List} A list of entities
	 * @throws ApplicationException 
     */	
public void addAllToProfessionals( Collection<Professional65BO> entities ) throws ApplicationException {
	for( Professional65BO entity : entities) {
		if( getProfessionals()!=null ) {
			addToProfessionals( entity );
		}
	}
}
	
	/**
	 * Remove entities form the com.bluage.documentation.business.standardfeatures.entities.uc65_lazy.bos.Professional65BO collection
	 *
	 * @param entities				
	 *		: {@link java.util.List} a list of entities
	 * @throws ApplicationException
     */	
public void removeAllFromProfessionals( Collection<Professional65BO> entities ) throws ApplicationException {
	for( Professional65BO entity : entities) {
		if( getProfessionals()!=null && getProfessionals().contains( entity ) )	{		
			removeFromProfessionals( entity );
		}
	}
}
	
	/**
	 * Remove all items form the collection com.bluage.documentation.business.standardfeatures.entities.uc65_lazy.bos.Professional65BO
	 *
	 * @throws ApplicationException
     */	
public void removeAllFromProfessionals() throws ApplicationException {
	Set<Professional65BO> entities = this.getProfessionals();
	if ( entities == null ) {
		return;
	}
	Iterator<Professional65BO> it = entities.iterator();
	while ( it.hasNext() ) {
    	Professional65BO entity = it.next(); 
		entity.setTeam65( null ); 
	}
	entities.clear();
}
																		/**
	 * Getter for rookies of class Team65BO.
	 *
	 * @return {@link java.util.Set<com.bluage.documentation.business.standardfeatures.entities.uc65_lazy.bos.Rookie65BO>}
	 * @generated
	 */ 
public  Set<Rookie65BO> getRookies() {
	return this.rookies;
			}


	/**
	 * Setter for rookies of class Team65BO.
	 * @param value {@link java.util.Set<com.bluage.documentation.business.standardfeatures.entities.uc65_lazy.bos.Rookie65BO>}
	 * @generated
	 */

public  void setRookies(final Set<Rookie65BO> rookies) {
	this.rookies = rookies;
}

	/**
	 * Remove an entity from the com.bluage.documentation.business.standardfeatures.entities.uc65_lazy.bos.Rookie65BO collection.
	 *
	 * @param entity
	 *		: {@link Set<Rookie65BO>} the entity
	 * @throws ApplicationException
     */
public void removeFromRookies( Rookie65BO entity ) throws ApplicationException {
	try {
		if( getRookies() != null && getRookies().contains(entity) ) {
			getRookies().remove( entity );
		entity.setTeam65( null );
		}
		} catch ( LazyInitializationException e ) {
				Team65BO bo = ( Team65BO ) finder.findByPrimaryKey( getId() );
			finder.getHibernateTemplate().initialize( bo.getRookies() );
			setRookies( bo.getRookies() );
			// evict object from session to avoid NonUniqueObjectException
			finder.getHibernateTemplate().evict( bo );
		
			if( getRookies() != null && getRookies().contains( entity ) ) {
				getRookies().remove( entity );
				entity.setTeam65( null );
			}
				}
	}
	
	/**
	 * Add entity to the com.bluage.documentation.business.standardfeatures.entities.uc65_lazy.bos.Rookie65BO collection.
	 *
	 * @param entity
	 *		{@link Rookie65BO} the entity
	 * @throws ApplicationException
     */
public void addToRookies( Rookie65BO entity ) throws ApplicationException
	{
		try {
			if( getRookies() != null && !getRookies().contains( entity ) ) {
				getRookies().add( entity );
				entity.setTeam65( this );
			}
			} catch ( LazyInitializationException e ) {
					Team65BO bo = ( Team65BO ) finder.findByPrimaryKey( getId() );
		finder.getHibernateTemplate().initialize( bo.getRookies() );
		setRookies( bo.getRookies() );
		// evict object from session to avoid NonUniqueObjectException
		finder.getHibernateTemplate().evict( bo );
		if( getRookies() != null && !getRookies().contains( entity ) ) {
			getRookies().add( entity );
			entity.setTeam65( this );
		}
			}
	}
	
	/**
	 * Add entitys to the com.bluage.documentation.business.standardfeatures.entities.uc65_lazy.bos.Rookie65BO collection
	 *
	 * @param entities			
	 *		: {@link java.util.List} A list of entities
	 * @throws ApplicationException 
     */	
public void addAllToRookies( Collection<Rookie65BO> entities ) throws ApplicationException {
	for( Rookie65BO entity : entities) {
		if( getRookies()!=null ) {
			addToRookies( entity );
		}
	}
}
	
	/**
	 * Remove entities form the com.bluage.documentation.business.standardfeatures.entities.uc65_lazy.bos.Rookie65BO collection
	 *
	 * @param entities				
	 *		: {@link java.util.List} a list of entities
	 * @throws ApplicationException
     */	
public void removeAllFromRookies( Collection<Rookie65BO> entities ) throws ApplicationException {
	for( Rookie65BO entity : entities) {
		if( getRookies()!=null && getRookies().contains( entity ) )	{		
			removeFromRookies( entity );
		}
	}
}
	
	/**
	 * Remove all items form the collection com.bluage.documentation.business.standardfeatures.entities.uc65_lazy.bos.Rookie65BO
	 *
	 * @throws ApplicationException
     */	
public void removeAllFromRookies() throws ApplicationException {
	Set<Rookie65BO> entities = this.getRookies();
	if ( entities == null ) {
		return;
	}
	Iterator<Rookie65BO> it = entities.iterator();
	while ( it.hasNext() ) {
    	Rookie65BO entity = it.next(); 
		entity.setTeam65( null ); 
	}
	entities.clear();
}

/**
	 * Returns <code>true</code> if the argument is an Team65 instance and all identifiers for this entity
     * equal the identifiers of the argument entity. The <code>equals</code> method of the parent entity
     * will also need to return <code>true</code>. Returns <code>false</code> otherwise.
     *
     * @see TeamBO#hashCode()
	 * @param object  object to compare with current instance      
     * @return boolean true if equals false if not
     */
    public boolean equals(final Object object )
    {
		if ( this == object )
        {
            return true;
        }
        if ( !( object instanceof Team65BO ) )
        {
            return false;
        }
        final Team65BO that = (Team65BO)object;
    
    	            if (        				        			this.getId() == null
        	        ) {
        	return false;
        }
		return new EqualsBuilder()
			.append(this.getId(), that.getId())
			 	.isEquals();
	}
	/**

	 * Returns a hash code based on this entity's identifiers and the hash code of the parent entity.
     *
     * @see TeamBO#hashCode()
     * @return int
     */    
    public int hashCode() {
        return new HashCodeBuilder()
             .append(getId())
				 	.appendSuper(super.hashCode())
            .toHashCode();
    }
}

