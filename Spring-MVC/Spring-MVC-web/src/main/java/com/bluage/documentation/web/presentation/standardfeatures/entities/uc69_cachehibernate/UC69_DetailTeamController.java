/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.entities.uc69_cachehibernate ;

// Import declaration.
// Java imports.
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.bluage.documentation.business.standardfeatures.entities.uc69_cachehibernate.bos.Team69BO;
import com.bluage.documentation.service.standardfeatures.entities.uc69_cachehibernate.ServiceTeam;
import com.netfective.bluage.core.exception.ApplicationException;
import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;
import com.netfective.bluage.core.web.IForm;

/**
 * Class : UC69_DetailTeamController
 */
@Controller
@Scope("prototype")
@SessionAttributes("uC69_DetailTeamForm")
@RequestMapping(value= "/presentation/standardfeatures/entities/uc69_cachehibernate")
public class UC69_DetailTeamController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC69_DetailTeamController.class);
	
	//Current form name
	private static final String U_C69__DETAIL_TEAM_FORM = "uC69_DetailTeamForm";

	//CONSTANT: team
	private static final String TEAM = "team";
		//CONSTANT: players table or repeater.
	private static final String PLAYERS = "players";
				//CONSTANT: allTeams
	private static final String ALL_TEAMS = "allTeams";
	
	/**
	 * Property:customDateEditorsUC69_DetailTeamController
	 */
	final private Map<String,CustomDateEditor> customDateEditorsUC69_DetailTeamController = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:uC69_DetailTeamValidator
	 */
	final private UC69_DetailTeamValidator uC69_DetailTeamValidator = new UC69_DetailTeamValidator();
	
	/**
	 * Service declaration : serviceTeam.
	 */
	@Autowired
	private ServiceTeam serviceTeam;
	
	/**
	 * Pre Controller declaration : uC69_PreControllerFindAllTeams
	 */
	@Autowired
	private UC69_PreControllerFindAllTeamsController uC69_PreControllerFindAllTeams;

	
	// Initialise all the instances for UC69_DetailTeamController
		// Initialize the instance team of type com.bluage.documentation.business.standardfeatures.entities.uc69_cachehibernate.bos.Team69BO with the value : null for UC69_DetailTeamController
		private Team69BO team;
			// Initialize the instance players for the state lnk_detail
		private List players; // Initialize the instance players for UC69_DetailTeamController
						// Declare the instance allTeams
		private List allTeams;
			/**
	 * Operation : lnk_back
 	 * @param model : 
 	 * @param uC69_DetailTeam : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC69_DetailTeam/lnk_back.html",method = RequestMethod.POST)
	public String lnk_back(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC69_DetailTeamForm") UC69_DetailTeamForm  uC69_DetailTeamForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : lnk_back"); 
		infoLogger(uC69_DetailTeamForm); 
		uC69_DetailTeamForm.setAlwaysCallPreControllers(false); 
		// initialization for lnk_back
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state lnk_back if the validation fail, you will be forward to the screen : "/presentation/standardfeatures/entities/uc69_cachehibernate/UC69_DetailTeam"
		if(errorsMessages(request,response,uC69_DetailTeamForm,bindingResult,"", uC69_DetailTeamValidator, customDateEditorsUC69_DetailTeamController)){ 
			return "/presentation/standardfeatures/entities/uc69_cachehibernate/UC69_DetailTeam"; 
		}

	 callFindAllTeams(request,uC69_DetailTeamForm );
											// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.entities.uc69_cachehibernate.UC69_HomePageForm or not from lnk_back
			final UC69_HomePageForm uC69_HomePageForm =  new UC69_HomePageForm();
			// Populate the destination form with all the instances returned from lnk_back.
			final IForm uC69_HomePageForm2 = populateDestinationForm(request.getSession(), uC69_HomePageForm); 
					
			// Add the form to the model.
			model.addAttribute("uC69_HomePageForm", uC69_HomePageForm2); 
			
			request.getSession().setAttribute("uC69_HomePageForm", uC69_HomePageForm2);
			
			// "OK" CASE => destination screen path from lnk_back
			LOGGER.info("Go to the screen 'UC69_HomePage'.");
			// Redirect (PRG) from lnk_back
			destinationPath =  "redirect:/presentation/standardfeatures/entities/uc69_cachehibernate/UC69_HomePage.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_back 
	}
	
	
	/**
	* This method initialise the form : UC69_DetailTeamForm 
	* @return UC69_DetailTeamForm
	*/
	@ModelAttribute("UC69_DetailTeamFormInit")
	public void initUC69_DetailTeamForm(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, U_C69__DETAIL_TEAM_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method UC69_DetailTeamForm."); //for lnk_back 
		}
		UC69_DetailTeamForm uC69_DetailTeamForm;
	
		if(request.getSession().getAttribute(U_C69__DETAIL_TEAM_FORM) != null){
			uC69_DetailTeamForm = (UC69_DetailTeamForm)request.getSession().getAttribute(U_C69__DETAIL_TEAM_FORM);
		} else {
			uC69_DetailTeamForm = new UC69_DetailTeamForm();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form UC69_DetailTeamForm.");
		}
		uC69_DetailTeamForm = (UC69_DetailTeamForm)populateDestinationForm(request.getSession(), uC69_DetailTeamForm);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  UC69_DetailTeamForm.");
		}
		model.addAttribute(U_C69__DETAIL_TEAM_FORM, uC69_DetailTeamForm);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param UC69_DetailTeamForm : The sceen form.
	 */
	@RequestMapping(value = "/UC69_DetailTeam.html" ,method = RequestMethod.GET)
	public void prepareUC69_DetailTeam(final HttpServletRequest request,final  Model model,final  @ModelAttribute("UC69_DetailTeamFormInit") UC69_DetailTeamForm uC69_DetailTeamForm){
		
		UC69_DetailTeamForm currentUC69_DetailTeamForm = uC69_DetailTeamForm;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(U_C69__DETAIL_TEAM_FORM) == null){
			if(currentUC69_DetailTeamForm!=null){
				request.getSession().setAttribute(U_C69__DETAIL_TEAM_FORM, currentUC69_DetailTeamForm);
			}else {
				currentUC69_DetailTeamForm = new UC69_DetailTeamForm();
				request.getSession().setAttribute(U_C69__DETAIL_TEAM_FORM, currentUC69_DetailTeamForm);	
			}
		} else {
			currentUC69_DetailTeamForm = (UC69_DetailTeamForm) request.getSession().getAttribute(U_C69__DETAIL_TEAM_FORM);
		}

				currentUC69_DetailTeamForm = (UC69_DetailTeamForm)populateDestinationForm(request.getSession(), currentUC69_DetailTeamForm);
		// Call all the Precontroller.
	if ( currentUC69_DetailTeamForm.isAlwaysCallPreControllers()) {
	
		// Calling the PreController : UC69_PreControllerFindAllTeams.
		currentUC69_DetailTeamForm = (UC69_DetailTeamForm) uC69_PreControllerFindAllTeams.uC69_PreControllerFindAllTeamsControllerInit(request, model, currentUC69_DetailTeamForm);
		
	}
	currentUC69_DetailTeamForm.setAlwaysCallPreControllers(true);
		model.addAttribute(U_C69__DETAIL_TEAM_FORM, currentUC69_DetailTeamForm);
		request.getSession().setAttribute(U_C69__DETAIL_TEAM_FORM, currentUC69_DetailTeamForm);
		
	}
	
			/**
	 * method callFindAllTeams
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callFindAllTeams(HttpServletRequest request,IForm form) {
		allTeams = (List)get(request.getSession(),form, "allTeams");  
										 
					try {
				// executing FindAllTeams in lnk_back
				allTeams = 	serviceTeam.searchTeams(
	);  
 
				put(request.getSession(), ALL_TEAMS,allTeams);
								// processing variables FindAllTeams in lnk_back

			} catch (ApplicationException e) { 
				// error handling for operation searchTeams called FindAllTeams
				errorLogger("An error occured during the execution of the operation : searchTeams",e); 
		
			}
	}
		
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("UC69_DetailTeamController [ ");
			strBToS.append(TEAM);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(team);
			strBToS.append(END_TO_STRING_DELIMITER);
				strBToS.append(PLAYERS);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(players);
			strBToS.append(END_TO_STRING_DELIMITER);
						strBToS.append(ALL_TEAMS);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(allTeams);
			strBToS.append(END_TO_STRING_DELIMITER);
		// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(uC69_DetailTeamValidator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsUC69_DetailTeamController!=null); 
		return strBToS.toString();
	}
}
