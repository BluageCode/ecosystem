/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.netfective.bluage.springmvc.ajax;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
          

/**
 * Class BufferResponseWrapper
 * @superclass HttpServletResponseWrapper
 * @author NETFECTIVE TECHNOLOGY
 */
public class BufferResponseWrapper extends HttpServletResponseWrapper {

	/**
	* Principal print writer
	*/
    private PrintWriter pw;
    /**
    * Servlet ouput stream
    */
    private ServletOutputStream sos;
    /**
    * Write buffer for <code>pw</code>
    */
    private StringWriter writerBuffer;
    /**
    * <code>ByteArray</code> output stream for <code>sos</code>
    */
    private ByteArrayOutputStream streamBuffer;

	/**
	* Original servlet response
	*/
    private HttpServletResponse originalResponse;

	/**
	* Redirect link
	*/
    private String redirect;

	/**
	* Overided builder
	*/
    public BufferResponseWrapper(final HttpServletResponse httpServletResponse) {
        super(httpServletResponse);
        originalResponse = httpServletResponse;
    }

	/**
	* Getter for pw with initialisation if needed
	* @return PrintWriter
	* @throw IOException
	*/
    public PrintWriter getWriter() throws IOException {
        if (writerBuffer == null) {
            writerBuffer = new StringWriter();
            pw = new PrintWriter(writerBuffer);
        }
        return pw;
    }

	/**
	* Getter for sos and streamBuffer with initialisation if needed
	* @return ServletOutputStream
	* @throw IOException
	*/
    public ServletOutputStream getOutputStream() throws IOException {
        if (streamBuffer == null) {
            streamBuffer = new ByteArrayOutputStream();
            sos = new ServletOutputStream() {
            
				/**
				 * write method
				 * @param b int
				 */
                public void write(final int b) throws IOException {
                    streamBuffer.write(b);
                }

				/**
				 * write method
				 * @param b byte[]
				 */
                public void write(final byte[] b) throws IOException {
                    streamBuffer.write(b);
                }

				/**
				 * write method
				 * @param b byte[]
				 * @param off int
				 * @param len int
				 */
                public void write(final byte[] b, final int off, final int len) throws IOException {
                    streamBuffer.write(b, off, len);
                }
            };
        }
        return sos;
    }

    /**
     * Outputs the content to <code>OutputStream</code> if the application is using it or to the Writer otherwise.
     * @param String :
     *			content
     */
    public void output(final String content) throws IOException{
        if (streamBuffer!=null){
            streamBuffer.write(content.getBytes(originalResponse.getCharacterEncoding()));
        } else {
            writerBuffer.write(content);
        }
    }
	
	/**
	* Return <code>streamBuffer</code> content if <code>streamBuffer</code> is not null 
	* otherwise <code>writeBuffer</code> content
	*/
    public String getBuffer() {
		String bufferToReturn ="";
		if (streamBuffer != null) {
			try {
				bufferToReturn = streamBuffer.toString(originalResponse.getCharacterEncoding());
			} catch (UnsupportedEncodingException e) {
				return streamBuffer.toString();
			}
		} else if (writerBuffer != null) {
			bufferToReturn = writerBuffer.toString();
		}
		return bufferToReturn;
    }

	/**
	* Getter for originalResponse
	* @return HttpServletResponse
	*/
    public HttpServletResponse getOriginalResponse() {
        return originalResponse;
    }

	/**
	* Getter for redirect
	* @return String
	*/
    public String getRedirect() {
        return redirect;
    }

	/**
	* Setter for redirect, with redirect link checking
	* @param String : redirect
	* 
	*/
    public void sendRedirect(final String redirectString) throws IOException {
       	final String key = "aaxmlrequest=true";
        final int pos = redirectString.indexOf(key);
        if (pos !=-1){
            this.redirect = redirectString.substring(0,pos)+redirectString.substring(pos+key.length());
        } else {
        	this.redirect = redirectString;
        }
    }
	
	/**
	* Substring search. This substring is surounded by two given strings
	* @param String :
	*			firstDelimiter
	* @param String :
	*			lastDelimiter
	*/
    public String findSubstring(final String firstDelimiter, final String lastDelimiter){
        		String substringToReturn = null;
		String content;
		if (streamBuffer != null) {
			try {
				content = streamBuffer.toString("UTF-8");
			} catch (UnsupportedEncodingException e) {
				content = streamBuffer.toString();
			}
		} else if (writerBuffer != null) {
			content = writerBuffer.toString();
		} else {
			return null;
		}
		int p1 = content.indexOf(firstDelimiter);
		if (p1 != -1) {
			p1 += firstDelimiter.length();
			final int p2 = content.indexOf(lastDelimiter, p1);
			if (p2 != -1) {
				substringToReturn = content.substring(p1, p2);
			}
		}
		return substringToReturn;
    }

	/**
	* Not yet implemented
	*/
    public void setContentType(final String string) {
        // do nothing
    }

	/**
	* Not yet implemented
	*/
    public void flushBuffer() throws IOException {
        // do nothing
    }

	/**
	* Not yet implemented
	*/
    public void setCharacterEncoding(final String string) {
        // do nothing
    }

	/**
	* Not yet implemented
	*/
    public void setDateHeader(final String string, final long l) {
        //do nothing
    }

	/**
	* Not yet implemented
	*/
    public void addDateHeader(final String string, final long l) {
        //do nothing
    }

	/**
	* Not yet implemented
	*/
    public void setHeader(final String string, final String string1) {
        //do nothing
    }

	/**
	* Not yet implemented
	*/
    public void addHeader(final String string, final String string1) {
        //do nothing
    }

	/**
	* Not yet implemented
	*/
    public void setIntHeader(final String string, final int i) {
        //do nothing
    }

	/**
	* Not yet implemented
	*/
    public void addIntHeader(final String string, final int i) {
        //do nothing
    }

	/**
	* Getter for writerBuffer
	* @return StringWriter
	*/
    public StringWriter getWriterBuffer() {
        return writerBuffer;
    }

	/**
	* Getter for streamBuffer
	* @return ByteArrayOutputStream
	*/
    public ByteArrayOutputStream getStreamBuffer() {
        return streamBuffer;
    }
}

