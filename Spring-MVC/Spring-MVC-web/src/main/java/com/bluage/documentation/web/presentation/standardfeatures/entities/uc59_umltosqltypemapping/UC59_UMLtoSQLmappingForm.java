/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.entities.uc59_umltosqltypemapping;


import java.io.Serializable;
import java.util.List;

import com.bluage.documentation.business.standardfeatures.entities.uc59_umltosqltypemapping.bos.Player59BO;
import com.netfective.bluage.core.web.AbstractForm;
import com.netfective.bluage.core.web.IForm;

/**
* Class : UC59_UMLtoSQLmappingForm
*/
public class UC59_UMLtoSQLmappingForm extends AbstractForm implements Serializable, IForm {
	
	/** Constant : serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	//CONSTANT : players
	private static final String PLAYERS = "players";
	/**
	 * 	Property: players 
	 */
	private List<Player59BO> players;
/**
	 * Default constructor : UC59_UMLtoSQLmappingForm
	 */
	public UC59_UMLtoSQLmappingForm() {
		super();
		// Initialize : players
		this.players = new java.util.ArrayList<com.bluage.documentation.business.standardfeatures.entities.uc59_umltosqltypemapping.bos.Player59BO>();
					this.setAlwaysCallPreControllers(true);
	}
	
	/**
	 * 	Getter : players 
	 * 	@return : Return the players instance.
	 */
	public List<Player59BO> getPlayers(){
		return players; // For UC59_UMLtoSQLmappingForm
	}
	
	/**
	 * 	Setter : players 
	 *  @param playersinstance : The instance to set.
	 */
	public void setPlayers(final List<Player59BO> playersinstance){
		this.players = playersinstance;// For UC59_UMLtoSQLmappingForm
	}
/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UC59_UMLtoSQLmappingForm [ "+
PLAYERS +" = " + players + "]";
	}
	
	/**
	 * Method setInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public void setInstanceByInstanceName(final String instanceName, final Object instance) {
		// Set the instance Players.
		if(PLAYERS.equals(instanceName)){// For UC59_UMLtoSQLmappingForm
			this.setPlayers((List<Player59BO>)instance); // For UC59_UMLtoSQLmappingForm
		}
			}
	
	/**
	 * Method getInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public Object getInstanceByInstanceName(final String instanceName){
		// Temporary object for tmpUC59_UMLtoSQLmappingForm.
		Object tmpUC59_UMLtoSQLmappingForm = null;
		
			
		// Get the instance Players for UC59_UMLtoSQLmappingForm.
		if(PLAYERS.equals(instanceName)){ // For UC59_UMLtoSQLmappingForm
			tmpUC59_UMLtoSQLmappingForm = this.getPlayers(); // For UC59_UMLtoSQLmappingForm
		}
		return tmpUC59_UMLtoSQLmappingForm;// For UC59_UMLtoSQLmappingForm
	}
	
			}
