/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.common.components ;


import org.apache.log4j.Logger;
import org.springframework.validation.Errors;

import com.netfective.bluage.core.validator.AbstractValidator;
/**
* Class :cmp_tabsValidator
*/
public class cmp_tabsValidator extends AbstractValidator{
	
	// LOGGER for the class cmp_tabsValidator
	private static final Logger LOGGER = Logger.getLogger( cmp_tabsValidator.class);
	
	
	/**
	* Operation validate for cmp_tabsForm
	* @param obj : the current form (cmp_tabsForm)
	* @param errors : The spring errors to return for the form cmp_tabsForm
	*/
	public void validate(final Object obj,final Errors errors){
		LOGGER.info("Beginning method : validate"); // cmp_tabsValidator
		cmp_tabsForm ccmp_tabsForm = (cmp_tabsForm)obj; // cmp_tabsValidator
		LOGGER.info("Ending method : validate the form "+ ccmp_tabsForm.getClass().getName()); // cmp_tabsValidator
	}

	/**
	* Method to implements to use spring validators (cmp_tabsForm)
	* @param aClass : Class for the form cmp_tabsForm
	* @return boolean
	*/
	public boolean supports(final Class aClass){
		return (new cmp_tabsForm()).getClass().equals(aClass); // cmp_tabsValidator
	}
}
