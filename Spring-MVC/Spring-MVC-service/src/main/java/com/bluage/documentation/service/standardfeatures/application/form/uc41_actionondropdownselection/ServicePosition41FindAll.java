/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.service.standardfeatures.application.form.uc41_actionondropdownselection;


import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import com.bluage.documentation.business.standardfeatures.application.form.uc41_actionondropdownselection.entities.daofinder.Position41DAOFinderImpl;
import com.bluage.documentation.service.AbstractService;
import com.netfective.bluage.core.exception.ApplicationException;
import com.netfective.bluage.core.persistence.IPersistentObjectDAO;

/**
 * Defines  application services for ServicePosition41FindAll
  */
@Named("com.bluage.documentation.service.standardfeatures.application.form.uc41_actionondropdownselection.ServicePosition41FindAll")
public class ServicePosition41FindAll  extends AbstractService{


	


	/**
	 *  Default constructor 
	 */
	public ServicePosition41FindAll() {
		super(); // Call super for default constructor : ServicePosition41FindAll
	}
	
			@Inject
	@Named ("com.bluage.documentation.business.standardfeatures.application.form.uc41_actionondropdownselection.entities.daofinder.Position41DAOFinderImpl")
	private Position41DAOFinderImpl dao;

	/**
	 * constructor with...
	 * 
	 * @param dao
	 *		: {@link IPersistentObjectDAO} the DAO
	 */
	public ServicePosition41FindAll(Position41DAOFinderImpl dao) {
		this.dao = dao;
	}

							
	/**    
	 * Operation Crud
	 * @return {@link List}
	 */
	public List position41FindAll( )
 throws ApplicationException {

					return dao.findAll();
	}

	

}
