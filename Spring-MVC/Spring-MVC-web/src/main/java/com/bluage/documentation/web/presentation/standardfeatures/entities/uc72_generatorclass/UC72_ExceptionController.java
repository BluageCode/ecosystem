/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.entities.uc72_generatorclass ;

// Import declaration.
// Java imports.
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.bluage.documentation.service.standardfeatures.entities.uc72_generatorclass.ServicePlayer72;
import com.netfective.bluage.core.exception.ApplicationException;
import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;
import com.netfective.bluage.core.web.IForm;

/**
 * Class : UC72_ExceptionController
 */
@Controller
@Scope("prototype")
@SessionAttributes("uC72_ExceptionForm")
@RequestMapping(value= "/presentation/standardfeatures/entities/uc72_generatorclass")
public class UC72_ExceptionController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC72_ExceptionController.class);
	
	//Current form name
	private static final String U_C72__EXCEPTION_FORM = "uC72_ExceptionForm";

	//CONSTANT: listPlayers
	private static final String LIST_PLAYERS = "listPlayers";
	
	/**
	 * Property:customDateEditorsUC72_ExceptionController
	 */
	final private Map<String,CustomDateEditor> customDateEditorsUC72_ExceptionController = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:uC72_ExceptionValidator
	 */
	final private UC72_ExceptionValidator uC72_ExceptionValidator = new UC72_ExceptionValidator();
	
	/**
	 * Service declaration : servicePlayer72.
	 */
	@Autowired
	private ServicePlayer72 servicePlayer72;
	
	
	// Initialise all the instances for UC72_ExceptionController
		// Declare the instance listPlayers
		private List listPlayers;
					/**
	 * Operation : lnk_back
 	 * @param model : 
 	 * @param uC72_Exception : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC72_Exception/lnk_back.html",method = RequestMethod.GET)
	public String lnk_back(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC72_ExceptionForm") UC72_ExceptionForm  uC72_ExceptionForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : lnk_back"); 
		infoLogger(uC72_ExceptionForm); 
		uC72_ExceptionForm.setAlwaysCallPreControllers(false); 
		// initialization for lnk_back
		init();
		
		String destinationPath = null; 
		

	 callFindallplayers(request,uC72_ExceptionForm );
											// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.entities.uc72_generatorclass.UC72_AddPlayerForm or not from lnk_back
			final UC72_AddPlayerForm uC72_AddPlayerForm =  new UC72_AddPlayerForm();
			// Populate the destination form with all the instances returned from lnk_back.
			final IForm uC72_AddPlayerForm2 = populateDestinationForm(request.getSession(), uC72_AddPlayerForm); 
					
			// Add the form to the model.
			model.addAttribute("uC72_AddPlayerForm", uC72_AddPlayerForm2); 
			
			request.getSession().setAttribute("uC72_AddPlayerForm", uC72_AddPlayerForm2);
			
			// "OK" CASE => destination screen path from lnk_back
			LOGGER.info("Go to the screen 'UC72_AddPlayer'.");
			// Redirect (PRG) from lnk_back
			destinationPath =  "redirect:/presentation/standardfeatures/entities/uc72_generatorclass/UC72_AddPlayer.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_back 
	}
	
	
	/**
	* This method initialise the form : UC72_ExceptionForm 
	* @return UC72_ExceptionForm
	*/
	@ModelAttribute("UC72_ExceptionFormInit")
	public void initUC72_ExceptionForm(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, U_C72__EXCEPTION_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method UC72_ExceptionForm."); //for lnk_back 
		}
		UC72_ExceptionForm uC72_ExceptionForm;
	
		if(request.getSession().getAttribute(U_C72__EXCEPTION_FORM) != null){
			uC72_ExceptionForm = (UC72_ExceptionForm)request.getSession().getAttribute(U_C72__EXCEPTION_FORM);
		} else {
			uC72_ExceptionForm = new UC72_ExceptionForm();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form UC72_ExceptionForm.");
		}
		uC72_ExceptionForm = (UC72_ExceptionForm)populateDestinationForm(request.getSession(), uC72_ExceptionForm);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  UC72_ExceptionForm.");
		}
		model.addAttribute(U_C72__EXCEPTION_FORM, uC72_ExceptionForm);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param UC72_ExceptionForm : The sceen form.
	 */
	@RequestMapping(value = "/UC72_Exception.html" ,method = RequestMethod.GET)
	public void prepareUC72_Exception(final HttpServletRequest request,final  Model model,final  @ModelAttribute("UC72_ExceptionFormInit") UC72_ExceptionForm uC72_ExceptionForm){
		
		UC72_ExceptionForm currentUC72_ExceptionForm = uC72_ExceptionForm;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(U_C72__EXCEPTION_FORM) == null){
			if(currentUC72_ExceptionForm!=null){
				request.getSession().setAttribute(U_C72__EXCEPTION_FORM, currentUC72_ExceptionForm);
			}else {
				currentUC72_ExceptionForm = new UC72_ExceptionForm();
				request.getSession().setAttribute(U_C72__EXCEPTION_FORM, currentUC72_ExceptionForm);	
			}
		} else {
			currentUC72_ExceptionForm = (UC72_ExceptionForm) request.getSession().getAttribute(U_C72__EXCEPTION_FORM);
		}

		currentUC72_ExceptionForm = (UC72_ExceptionForm)populateDestinationForm(request.getSession(), currentUC72_ExceptionForm);
		// Call all the Precontroller.
	currentUC72_ExceptionForm.setAlwaysCallPreControllers(true);
		model.addAttribute(U_C72__EXCEPTION_FORM, currentUC72_ExceptionForm);
		request.getSession().setAttribute(U_C72__EXCEPTION_FORM, currentUC72_ExceptionForm);
		
	}
	
	/**
	 * method callFindallplayers
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callFindallplayers(HttpServletRequest request,IForm form) {
		listPlayers = (List)get(request.getSession(),form, "listPlayers");  
										 
					try {
				// executing Findallplayers in lnk_back
				listPlayers = 	servicePlayer72.player72FindAll(
	);  
 
				put(request.getSession(), LIST_PLAYERS,listPlayers);
								// processing variables Findallplayers in lnk_back

			} catch (ApplicationException e) { 
				// error handling for operation player72FindAll called Findallplayers
				errorLogger("An error occured during the execution of the operation : player72FindAll",e); 
		
			}
	}
		
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("UC72_ExceptionController [ ");
			strBToS.append(LIST_PLAYERS);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(listPlayers);
			strBToS.append(END_TO_STRING_DELIMITER);
		// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(uC72_ExceptionValidator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsUC72_ExceptionController!=null); 
		return strBToS.toString();
	}
}
