/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.web.presentation.standardfeatures.application.component.uc47_contents;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.tiles.Attribute;
import org.apache.tiles.AttributeContext;
import org.apache.tiles.ListAttribute;
import org.apache.tiles.context.TilesRequestContext;
import org.apache.tiles.preparer.ViewPreparer;

import com.netfective.bluage.core.common.IStringConstants;
import com.netfective.bluage.core.web.ComponentsBean;
/**
 * Class : CUC47_ContentsUC47_TableContentsViewPreparer
 * Prepare the component UC47_TableContents for the screen UC47_Contents.
 * 
 */
public class CUC47_ContentsUC47_TableContentsViewPreparer implements ViewPreparer , IStringConstants{

	private static final String UC47_CONTENTSUC47_TABLECONTENTS = "UC47_TableContentsComponentBean";
	
	private static final String COMPONENT_FORM_NAME = "com.bluage.documentation.web.presentation.standardfeatures.application.component.uc47_contents.UC47_TableContentsForm";
	
	private static final String COMPONENT_SHORT_FORM_NAME = "uC47_TableContentsForm";
	private static final String PARENT_SHORT_FORM_NAME = "uC47_ContentsForm";

	/**
	 * Property:LOGGER
	 */
	private static final Logger LOGGER = Logger.getLogger( CUC47_ContentsUC47_TableContentsViewPreparer.class);

	/**
	 * Method execute for CUC47_ContentsUC47_TableContentsViewPreparer
	 * @param tilesContext : The tiles context of the component  CUC47_ContentsUC47_TableContentsViewPreparer
	 * @param attributeContext : The attribute context of the component  CUC47_ContentsUC47_TableContentsViewPreparer
	 * @see org.apache.tiles.preparer.ViewPreparer#execute(org.apache.tiles.context.TilesRequestContext, org.apache.tiles.AttributeContext)
	 */
	public void execute(TilesRequestContext tilesContext, AttributeContext attributeContext) {
		
		LOGGER.info("Starting the component initialisation for CUC47_ContentsUC47_TableContentsViewPreparer "+ UC47_CONTENTSUC47_TABLECONTENTS +" Component Form name : "+COMPONENT_FORM_NAME);
		
		// Retrieve the attributs from the component bean UC47_TableContentsComponentBean
		final Attribute attrListInstanceNameCUC47_ContentsUC47_TableContentsViewPreparer = attributeContext.getAttribute(LIST_INSTANCES); // All the list instancs for UC47_TableContents
		final Attribute attrParentPathScreenNameCUC47_ContentsUC47_TableContentsViewPreparer = attributeContext.getAttribute(PARENT_PATH_SCREEN_NAME); // The parent screen UC47_TableContents
		final Attribute attrParentHelperNameCUC47_ContentsUC47_TableContentsViewPreparer = attributeContext.getAttribute(PARENT_HELPER_NAME); // The Parent helper name for UC47_TableContents

		// Init the component bean for UC47_TableContentsComponentBean
		final ComponentsBean compB = new ComponentsBean(); // Init the corresponding UC47_TableContentsComponentBean
		Map<String, String> tmpInstanceNameMp = new HashMap<String, String>(); // Init values to manage the UC47_TableContentsComponentBean

		// Fill the ComponentBean (UC47_TableContentsComponentBean)
		// Retrieve from the tiles configuration the parentHelperName UC47_TableContentsComponentBean
		compB.setParentHelperName((String) attrParentHelperNameCUC47_ContentsUC47_TableContentsViewPreparer.getValue()); //Set the parent helper for UC47_TableContentsComponentBean

		// Retrieve from the tiles configuration the parentPathScreenName
		compB.setParentPathScreenName((String) attrParentPathScreenNameCUC47_ContentsUC47_TableContentsViewPreparer.getValue()); // Set the parent screen path UC47_TableContentsComponentBean

		final List tmpInstanceNameList = (List) attrListInstanceNameCUC47_ContentsUC47_TableContentsViewPreparer.getValue(); // Retrieve the attribut list to populate UC47_TableContentsComponentBean

		// Transform the List to a Map, more efficient (UC47_TableContentsComponentBean)
		for (final Iterator iterator = tmpInstanceNameList.iterator(); iterator.hasNext();) {
			final ListAttribute object = (ListAttribute) iterator.next(); // For all the instances UC47_TableContentsComponentBean
			final Object[] strArr = (Object[]) ((List)object.getValue()).toArray(); // To array UC47_TableContentsComponentBean
			// The instances always are by pairs, the first is the source, the
			// second the destination.
			final String instanceSource = (String) ((Attribute)strArr[0]).getValue(); // Source screen instance name UC47_TableContentsComponentBean
			final String instanceDestination = (String) ((Attribute)strArr[1]).getValue();// Destination screen instance name UC47_TableContentsComponentBean

			tmpInstanceNameMp.put(instanceSource, instanceDestination); // Put in the instance UC47_TableContentsComponentBean
		}

		// Set.
		compB.setAttributs(tmpInstanceNameMp); // Set to the bean UC47_TableContentsComponentBean

		if (LOGGER.isDebugEnabled()) { // Display debug information for UC47_TableContentsComponentBean
			LOGGER.debug("The Component Bean  : " + compB + " will be saved in session and identified by : " + UC47_CONTENTSUC47_TABLECONTENTS);
		}

		// Get the main sreen : UC47_ContentsForm
		final UC47_ContentsForm componentScreenMainScreen = (UC47_ContentsForm) tilesContext.getSessionScope().get(PARENT_SHORT_FORM_NAME);

		// Init the component UC47_TableContentsForm
		final UC47_TableContentsForm componentScreen = new UC47_TableContentsForm(); // For the form UC47_TableContentsForm
		// 
		componentScreen.setInstanceByInstanceName("allPlayers", componentScreenMainScreen.getAllPlayers()); // Set the property allPlayers 
		// 
		componentScreen.setInstanceByInstanceName("playerToUpdate", componentScreenMainScreen.getPlayerToUpdate()); // Set the property playerToUpdate 
				
		tilesContext.getSessionScope().put(COMPONENT_SHORT_FORM_NAME, componentScreen); // For the component CUC47_ContentsUC47_TableContentsViewPreparer
		
		tilesContext.getSessionScope().put(UC47_CONTENTSUC47_TABLECONTENTS, compB);
		LOGGER.info("The component is initialized (CUC47_ContentsUC47_TableContentsViewPreparer).");
	}
}
