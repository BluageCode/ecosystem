--<ScriptOptions statementTerminator=";"/>

ALTER TABLE rookie51 DROP CONSTRAINT rookie51_team51_fkc;

ALTER TABLE professional51 DROP CONSTRAINT professional51_team51_fkc;

ALTER TABLE player51 DROP CONSTRAINT player51_team51_fkc;

ALTER TABLE team51 DROP CONSTRAINT team51_state_fkc;

ALTER TABLE player51 DROP CONSTRAINT player51_team_fkc;

ALTER TABLE state51 DROP CONSTRAINT state51_pkey;

ALTER TABLE player51 DROP CONSTRAINT player51_pkey;

ALTER TABLE team51 DROP CONSTRAINT team51_pkey;

ALTER TABLE rookie51 DROP CONSTRAINT rookie51_pkey;

ALTER TABLE professional51 DROP CONSTRAINT professional51_pkey;

DROP TABLE player51;

DROP TABLE rookie51;

DROP TABLE professional51;

DROP TABLE team51;

DROP TABLE state51;

CREATE TABLE player51 (
		id INT8 NOT NULL,
		version INT4 NOT NULL,
		first_name VARCHAR(30) NOT NULL,
		last_name VARCHAR(30) NOT NULL,
		date_of_birth DATE,
		estimated_value FLOAT4,
		weight INT4,
		height INT4,
		rookie BOOL,
		team_fk INT8 NOT NULL,
		team51_fk INT8
	);

CREATE TABLE rookie51 (
		id INT8 NOT NULL,
		version INT4 NOT NULL,
		first_name VARCHAR(30) NOT NULL,
		estimated_value FLOAT4,
		last_name VARCHAR(30) NOT NULL,
		university VARCHAR(1024),
		height INT4,
		weight INT4,
		date_of_birth DATE,
		team51_fk INT8
	);

CREATE TABLE professional51 (
		id INT8 NOT NULL,
		version INT4 NOT NULL,
		first_name VARCHAR(30) NOT NULL,
		weight INT4,
		height INT4,
		last_name VARCHAR(30) NOT NULL,
		estimated_value FLOAT4,
		date_of_birth DATE,
		salary INT4,
		team51_fk INT8
	);

CREATE TABLE team51 (
		id INT8 NOT NULL,
		version INT4 NOT NULL,
		city VARCHAR(1024),
		name VARCHAR(1024),
		state_fk VARCHAR(2)
	);

CREATE TABLE state51 (
		code VARCHAR(2) NOT NULL,
		version INT4 NOT NULL,
		name VARCHAR(20) NOT NULL
	);
	
INSERT INTO team51 VALUES (1, 0, 'Charlotte', 'Tigers', 'AK');
INSERT INTO team51 VALUES (2, 0, 'Kansas City', 'Bears', 'AL');
INSERT INTO team51 VALUES (3, 0, 'Detroit', 'Chargers', 'AL');

INSERT INTO state51 VALUES ('AK', 0, 'Alaska');
INSERT INTO state51 VALUES ('AL', 0, 'Alabama');
INSERT INTO state51 VALUES ('AR', 0, 'Arkansas');
INSERT INTO state51 VALUES ('AZ', 0, 'Arizona');
INSERT INTO state51 VALUES ('CT', 0, 'SDF');

INSERT INTO rookie51 VALUES (1, 0, 'Pocco', 0.25, 'Rosso', 'Columbia', 199, 105, '1988-03-14', 1);
INSERT INTO rookie51 VALUES (2, 0, 'Young', 0.23999999, 'Chester', 'Berkeley', 201, 113, '1989-06-21', 1);
INSERT INTO rookie51 VALUES (3, 0, 'Kido', 0.31, 'Dido', 'UCLA', 187, 88, '1988-11-05', 1);

INSERT INTO professional51 VALUES (1, 0, 'Oliver', 116, 228, 'Frank', 0.11, '1968-10-31', 87700, 1);
INSERT INTO professional51 VALUES (2, 0, 'Smith', 88, 197, 'Ricky', 0.55000001, '1970-07-31', 98500, 1);
INSERT INTO professional51 VALUES (3, 0, 'Ronson', 102, 201, 'Mark', 0.34, '1977-12-18', 76000, 1);

INSERT INTO player51 VALUES (201, 0, 'Oliver', 'Frank', '1968-10-31', 0.11, 116, 228, false, 1, 1);
INSERT INTO player51 VALUES (202, 0, 'Elbert', 'Cole', '1965-04-07', 3.95, 100, 171, false, 2, 2);
INSERT INTO player51 VALUES (203, 0, 'Abel', 'Dennis', '1980-04-16', 30.51, 105, 208, false, 1, 1);
INSERT INTO player51 VALUES (205, 0, 'Earl', 'Russell', '1981-12-15', 44.73, 60, 234, false, 2, 2);
INSERT INTO player51 VALUES (206, 0, 'Nick', 'Garner', '1992-07-27', 47.84, 87, 221, true, 2, 2);
INSERT INTO player51 VALUES (207, 0, 'Howard', 'Ramos', '1970-08-25', 10.76, 107, 199, false, 3, 3);
INSERT INTO player51 VALUES (208, 0, 'Allan', 'Meyer', '1969-02-14', 33.669998, 114, 229, false, 2, 2);
INSERT INTO player51 VALUES (209, 0, 'Ruben', 'Pope', '1985-05-01', 44.66, 86, 201, false, 1, 1);
INSERT INTO player51 VALUES (210, 0, 'Lester', 'Cross', '1976-02-21', 30.98, 69, 214, false, 1, 1);
INSERT INTO player51 VALUES (211, 0, 'Keith', 'Barnett', '1976-11-10', 5.8099999, 66, 190, false, 2, 2);
INSERT INTO player51 VALUES (212, 0, 'Ivan', 'Tate', '1972-02-08', 48.490002, 143, 175, false, 3, 3);
INSERT INTO player51 VALUES (213, 0, 'Tommy', 'Wood', '1992-01-20', 13.93, 77, 173, true, 2, 2);
INSERT INTO player51 VALUES (214, 0, 'Sylvester', 'Nash', '1965-07-10', 37.73, 68, 184, false, 2, 2);
INSERT INTO player51 VALUES (215, 0, 'Michael', 'Hodges', '1988-04-01', 25.35, 158, 204, false, 3, 3);
INSERT INTO player51 VALUES (216, 0, 'Jerome', 'Grant', '1982-06-28', 28.940001, 105, 205, false, 1, 1);
INSERT INTO player51 VALUES (217, 0, 'Woodrow', 'Carpenter', '1987-08-08', 41.759998, 69, 224, false, 3, 3);
INSERT INTO player51 VALUES (218, 0, 'Roderick', 'Robbins', '1975-07-29', 12.04, 115, 164, false, 3, 3);
INSERT INTO player51 VALUES (219, 0, 'Christian', 'Chavez', '1983-05-17', 37.880001, 97, 209, false, 1, 1);
INSERT INTO player51 VALUES (220, 0, 'Cecil', 'Conner', '1978-06-29', 39.709999, 160, 170, false, 2, 3);
INSERT INTO player51 VALUES (221, 0, 'Stephen', 'Lambert', '1977-03-14', 18.9, 107, 153, false, 3, 3);


ALTER TABLE state51 ADD CONSTRAINT state51_pkey PRIMARY KEY (code);

ALTER TABLE player51 ADD CONSTRAINT player51_pkey PRIMARY KEY (id);

ALTER TABLE team51 ADD CONSTRAINT team51_pkey PRIMARY KEY (id);

ALTER TABLE rookie51 ADD CONSTRAINT rookie51_pkey PRIMARY KEY (id);

ALTER TABLE professional51 ADD CONSTRAINT professional51_pkey PRIMARY KEY (id);

ALTER TABLE rookie51 ADD CONSTRAINT rookie51_team51_fkc FOREIGN KEY (team51_fk)
	REFERENCES team51 (id);

ALTER TABLE professional51 ADD CONSTRAINT professional51_team51_fkc FOREIGN KEY (team51_fk)
	REFERENCES team51 (id);

ALTER TABLE player51 ADD CONSTRAINT player51_team51_fkc FOREIGN KEY (team51_fk)
	REFERENCES team51 (id);

ALTER TABLE team51 ADD CONSTRAINT team51_state_fkc FOREIGN KEY (state_fk)
	REFERENCES state51 (code);

ALTER TABLE player51 ADD CONSTRAINT player51_team_fkc FOREIGN KEY (team_fk)
	REFERENCES team51 (id);

