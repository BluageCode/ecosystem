<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file="/include.jsp"  %>
<html >
<head >
<title  > 
<fmt:message key="cUC49_BAGS_BLU_AGE_DOCUMENTATION"/>
</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"  > 
</meta>
<link href="<c:url value= "/presentation/_style/_style.css"/>" rel="stylesheet" type="text/css"  /> 
<link href="<c:url value= "/presentation/_include/jquery/rd_lib.css"/>" rel="stylesheet" type="text/css"  /> 
<script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/jquery.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/rd_setup.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/rd_lib.js"/>" space="preserve"  ></script><link rel="shortcut icon" href="<c:url value= "/presentation/_style/favicon.ico"/>"  /> 

</head>

<body class=" " > 
<div id="rd_ghost"  > 
<small  > 
<fmt:message key="cUC49_BAGS___CSS_DEACTIVATED"/>
</small>
</div>
<div id="rd_jghost"  > 
<small  > 
<fmt:message key="cUC49_BAGS___JAVASCRIPT_DEACTIVATED"/>
</small>
</div>
<div id="shape"  > 
<div class="ui-layout-center"  > 
<div class="east-north"  > 
	<form:form id="uC49_BAGS" commandName="uC49_BAGSForm"  enctype="application/x-www-form-urlencoded" method="post">
<h2 class="rd_first"  > 
<fmt:message key="cUC49_BAGS_STANDARD_FEATURES__SERVICES_"/>
</h2>
<label  > 
<fmt:message key="cUC49_BAGS_BLU_AGE_GETTERS_AND_SETTERS"/>
</label>
<br clear="none"  /> 
<h4  > 
<fmt:message key="cUC49_BAGS_PLAYERS_LIST"/>
</h4>
<display:table uid="tab_players" pagesize="10"  sort="list" requestURI="" name="sessionScope.uC49_BAGSForm.allPlayers" class="rd_tabledata"  >
	<c:set   var="rownum">
		<%=(tab_players !=	null) ? new Integer(tab_players_rowNum.intValue() - 1) : new Integer(0) %>
	</c:set>
	<c:set   var="idRow">
		<%=(tab_players != null ) ? tab_players_rowNum.intValue() - 1 : -1 %>
	</c:set>
	<c:set   var="idTab">
		<%=(tab_players != null ) ? "tab_players" : null %>
	</c:set>
	<c:set var="tableHashId">
		<%= new org.displaytag.util.ParamEncoder("tab_players").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE) %>
	</c:set>
	<c:set var="tableHashId2">
		<%=request.getParameter(new org.displaytag.util.ParamEncoder("tab_players").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE)) %>
	</c:set>	
	
<display:column format=""  titleKey="cUC49_BAGS_NAME">
		<span id="dyn_firstName">
	<c:out value="${tab_players.firstName}" />
		</span>
	<fmt:message key="cUC49_BAGS_____"/>
		<span id="dyn_lastName">
	<c:out value="${tab_players.lastName}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="cUC49_BAGS_DATE_OF_BIRTH">
<span id="dyn_dateOfBirth">
	<format:formatDateValidator value="${tab_players.dateOfBirth}" pattern="MM/dd/yyyy" />
</span>
	</display:column>
	
<display:column format=""  titleKey="cUC49_BAGS_ESTIMATED_VALUE_M">
		<span id="dyn_estimatedValue">
	<c:out value="${tab_players.estimatedValue}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="cUC49_BAGS_WEIGHT_KG">
		<span id="dyn_weight">
	<c:out value="${tab_players.weight}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="cUC49_BAGS_HEIGHT_CM">
		<span id="dyn_height">
	<c:out value="${tab_players.height}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="cUC49_BAGS_ROOKIE">
<form:checkbox id="chk_rookie[${rownum}]" path="allPlayers[${rownum}].rookie"  name="checkboxRookie" disabled="true"  
		/>
</display:column>
	
<display:column format=""  titleKey="cUC49_BAGS_">
<a id="lnk_view[${rownum}]"onclick="javascript:btn_click_cExecuteUC49_BAGS(this.href);return false;"			href="<c:url value="/presentation/standardfeatures/services/bags/uc49_bags/UC49_BAGS/lnk_view.html?id="/><c:out value="${idRow}" />&tab=<c:out value="tab_players" />&<c:out value="${tableHashId}"/>=<c:out value="${tableHashId2}"/>"
	 shape="rect"  >									 
<fmt:message key="cUC49_BAGS_VIEW_TEAM"/>
</a>
</display:column>
</display:table>
	</form:form>
	<script language="javascript">
		function btn_click_cExecuteUC49_BAGS(url) {
			UC49_BAGSvar = document.getElementById('uC49_BAGS');
			UC49_BAGSvar.setAttribute('action', url);
			UC49_BAGSvar.submit();
		}
		function btn_click_cSetActionUC49_BAGS(url) {
			UC49_BAGSvar = document.getElementById('uC49_BAGS');
			UC49_BAGSvar.setAttribute('action', url);
		}
	</script>
</div>
<div class="east-center"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/services/bags/uc49_bags/UC49_BAGScmp_tabs" />
</div>
</div>
<div class="ui-layout-north"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/services/bags/uc49_bags/UC49_BAGScmp_header" />
</div>
<div class="ui-layout-south"  > 
<fmt:message key="cUC49_BAGS___"/>
<span id="rd_jversion"  > 
<fmt:message key="cUC49_BAGS_BLU_AGE_DOCUMENTATION_205"/>
</span>
</div>
<div class="ui-layout-west"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/services/bags/uc49_bags/UC49_BAGScmp_treeview" />
</div>
</div>
</body>
</html>
