/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.netfective.bluage.core.exception;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.builder.EqualsBuilder;

/**
 * Cette classe est un conteneur d erreurs de type BusinessError.
 * 
 * <DL>
 * <DT><B>Nom du Projet: </B>
 * <DD>Bluage</DD>
 * <DT><B>Service: </B>
 */
public class BusinessErrorContainer implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List errors = new ArrayList();
	private List applicationExceptions = new ArrayList();
	
	/**
	 * Constructeur par defaut pour un BusinessErrorContainer.
	 */
	public BusinessErrorContainer() {
	}

	/**
	 * setter for errors
	 * @param errors List
	 */
	public void setErrors(final List errors) {
		this.errors = errors;
	}


	/**
	 * Retourne true si ce BusinessErrorContainer est vide, c est a dire qu il
	 * ne contient pas de BusinessError, et false sinon.
	 * 
	 * @return true si ce BusinessErrorContainer est vide et false sinon.
	 */
	public boolean isEmpty() {
		return errors.isEmpty();
	}

	/**
	 * Retourne true si ApplicationExceptions est vide, c est a dire qu il ne
	 * contient pas de ApplicationException, et false sinon.
	 * 
	 * @return true si ce ApplicationExceptions est vide et false sinon.
	 */
	public boolean isEmptyApplicationExceptions() {
		return applicationExceptions.isEmpty();
	}

	/**
	 * Ajoute une BusinessError a ce BusinessErrorContainer.
	 * 
	 * @param error
	 *            La BusinessError a ajouter a ce BusinessErrorContainer.
	 */
	public void add(final BusinessErrorContainer error) {
		errors.add(error);
	}

	/**
	 * Ajoute une ApplicationException a ce BusinessErrorContainer.
	 * 
	 * @param ae
	 *            L ApplicationException a ajouter a ce BusinessErrorContainer.
	 */
	public void addApplicationException(final ApplicationException ae) {
		applicationExceptions.add(ae);
	}

	/**
	 * Retourne l ApplicationException stockee a la position i dans ce
	 * BusinessErrorContainer.
	 * 
	 * @return L ApplicationException stockee a la position i dans ce
	 *         BusinessErrorContainer.
	 */
	public ApplicationException getApplicationException(final int i) {
		return (ApplicationException) applicationExceptions.get(i);
	}

	/**
	 * Retourne la BusinessError stockee a la position i dans ce
	 * BusinessErrorContainer.
	 * 
	 * @return La BusinessError stockee a la position i dans ce
	 *         BusinessErrorContainer.
	 */
	public BusinessErrorContainer get(final int i) {
		return (BusinessErrorContainer) errors.get(i);
	}

	/**
	 * Vider la collection des erreurs
	 */
	public void clear() {
		errors = new ArrayList();
		applicationExceptions = new ArrayList();
	}

	/**
	 * Retourne un Iterator pour ce BusinessErrorContainer.
	 * 
	 * @return Un Iterator pour iterer sur les BusinessError stockees dans ce
	 *         BusinessErrorContainer.
	 */
	public Iterator iterator() {
		return errors.iterator();
	}

	/**
	 * @return
	 */
	public List getErrors() {
		return errors;
	}


	/**
	 * getter for applicationExceptions
	 * @return List applicationExceptions
	 */
	public List getApplicationExceptions() {
		return applicationExceptions;
	}
	
	@Override
	public int hashCode() {
		return System.identityHashCode(this);
	}
	
	/**
	 * equals method for BusinessErrorContainer
	 * @param object Object
	 * @return boolean 
	 */
	public boolean equals(final Object object) {
		if (this == object) {
			return true;
		}
		if (!(object instanceof BusinessErrorContainer)) {
			return false;
		}
		final BusinessErrorContainer other = (BusinessErrorContainer) object;

		return new EqualsBuilder()
		.append(this.getErrors(), other.getErrors())
		.append(this.getApplicationExceptions(), other.getApplicationExceptions()).isEquals();
	}
	
	
}

