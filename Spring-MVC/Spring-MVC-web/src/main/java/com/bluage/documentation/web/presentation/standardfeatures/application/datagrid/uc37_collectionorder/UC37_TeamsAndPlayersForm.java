/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.application.datagrid.uc37_collectionorder;


import java.io.Serializable;
import java.util.List;

import com.bluage.documentation.business.standardfeatures.application.datagrid.uc37_collectionorder.bos.Player37BO;
import com.bluage.documentation.business.standardfeatures.application.datagrid.uc37_collectionorder.bos.Team37BO;
import com.netfective.bluage.core.web.AbstractForm;
import com.netfective.bluage.core.web.IForm;

/**
* Class : UC37_TeamsAndPlayersForm
*/
public class UC37_TeamsAndPlayersForm extends AbstractForm implements Serializable, IForm {
	
	/** Constant : serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	//CONSTANT : players
	private static final String PLAYERS = "players";
	//CONSTANT : team
	private static final String TEAM = "team";
	/**
	 * 	Property: players 
	 */
	private List<Player37BO> players;
	/**
	 * 	Property: team 
	 */
	private Team37BO team;
/**
	 * Default constructor : UC37_TeamsAndPlayersForm
	 */
	public UC37_TeamsAndPlayersForm() {
		super();
		// Initialize : players
		this.players = new java.util.ArrayList<com.bluage.documentation.business.standardfeatures.application.datagrid.uc37_collectionorder.bos.Player37BO>();
		// Initialize : team
		this.team = new Team37BO();
					this.setAlwaysCallPreControllers(true);
	}
	
	/**
	 * 	Getter : players 
	 * 	@return : Return the players instance.
	 */
	public List<Player37BO> getPlayers(){
		return players; // For UC37_TeamsAndPlayersForm
	}
	
	/**
	 * 	Setter : players 
	 *  @param playersinstance : The instance to set.
	 */
	public void setPlayers(final List<Player37BO> playersinstance){
		this.players = playersinstance;// For UC37_TeamsAndPlayersForm
	}
	/**
	 * 	Getter : team 
	 * 	@return : Return the team instance.
	 */
	public Team37BO getTeam(){
		return team; // For UC37_TeamsAndPlayersForm
	}
	
	/**
	 * 	Setter : team 
	 *  @param teaminstance : The instance to set.
	 */
	public void setTeam(final Team37BO teaminstance){
		this.team = teaminstance;// For UC37_TeamsAndPlayersForm
	}
/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UC37_TeamsAndPlayersForm [ "+
PLAYERS +" = " + players +TEAM +" = " + team + "]";
	}
	
	/**
	 * Method setInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public void setInstanceByInstanceName(final String instanceName, final Object instance) {
		// Set the instance Players.
		if(PLAYERS.equals(instanceName)){// For UC37_TeamsAndPlayersForm
			this.setPlayers((List<Player37BO>)instance); // For UC37_TeamsAndPlayersForm
		}
				// Set the instance Team.
		if(TEAM.equals(instanceName)){// For UC37_TeamsAndPlayersForm
			this.setTeam((Team37BO)instance); // For UC37_TeamsAndPlayersForm
		}
				// Parent instance name for UC37_TeamsAndPlayersForm
		if(TEAM.equals(instanceName) && team != null){ // For UC37_TeamsAndPlayersForm
			players.clear(); // For UC37_TeamsAndPlayersForm
			players.addAll(team.getPlayer37s());// For UC37_TeamsAndPlayersForm
		}
		
	}
	
	/**
	 * Method getInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public Object getInstanceByInstanceName(final String instanceName){
		// Temporary object for tmpUC37_TeamsAndPlayersForm.
		Object tmpUC37_TeamsAndPlayersForm = null;
		
			
		// Get the instance Players for UC37_TeamsAndPlayersForm.
		if(PLAYERS.equals(instanceName)){ // For UC37_TeamsAndPlayersForm
			tmpUC37_TeamsAndPlayersForm = this.getPlayers(); // For UC37_TeamsAndPlayersForm
		}
			
		// Get the instance Team for UC37_TeamsAndPlayersForm.
		if(TEAM.equals(instanceName)){ // For UC37_TeamsAndPlayersForm
			tmpUC37_TeamsAndPlayersForm = this.getTeam(); // For UC37_TeamsAndPlayersForm
		}
		return tmpUC37_TeamsAndPlayersForm;// For UC37_TeamsAndPlayersForm
	}
	
			}
