/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.entities.uc64_cascade ;

// Import declaration.
// Java imports.
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;

/**
 * Class : UC64_exceptionController
 */
@Controller
@Scope("prototype")
@SessionAttributes("uC64_exceptionForm")
@RequestMapping(value= "/presentation/standardfeatures/entities/uc64_cascade")
public class UC64_exceptionController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC64_exceptionController.class);
	
	//Current form name
	private static final String U_C64_EXCEPTION_FORM = "uC64_exceptionForm";

	
	/**
	 * Property:customDateEditorsUC64_exceptionController
	 */
	final private Map<String,CustomDateEditor> customDateEditorsUC64_exceptionController = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:uC64_exceptionValidator
	 */
	final private UC64_exceptionValidator uC64_exceptionValidator = new UC64_exceptionValidator();
	
	
	// Initialise all the instances for UC64_exceptionController

	/**
	* This method initialise the form : UC64_exceptionForm 
	* @return UC64_exceptionForm
	*/
	@ModelAttribute("UC64_exceptionFormInit")
	public void initUC64_exceptionForm(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, U_C64_EXCEPTION_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method UC64_exceptionForm."); //for lnk_update 
		}
		UC64_exceptionForm uC64_exceptionForm;
	
		if(request.getSession().getAttribute(U_C64_EXCEPTION_FORM) != null){
			uC64_exceptionForm = (UC64_exceptionForm)request.getSession().getAttribute(U_C64_EXCEPTION_FORM);
		} else {
			uC64_exceptionForm = new UC64_exceptionForm();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form UC64_exceptionForm.");
		}
		uC64_exceptionForm = (UC64_exceptionForm)populateDestinationForm(request.getSession(), uC64_exceptionForm);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  UC64_exceptionForm.");
		}
		model.addAttribute(U_C64_EXCEPTION_FORM, uC64_exceptionForm);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param UC64_exceptionForm : The sceen form.
	 */
	@RequestMapping(value = "/UC64_exception.html" ,method = RequestMethod.GET)
	public void prepareUC64_exception(final HttpServletRequest request,final  Model model,final  @ModelAttribute("UC64_exceptionFormInit") UC64_exceptionForm uC64_exceptionForm){
		
		UC64_exceptionForm currentUC64_exceptionForm = uC64_exceptionForm;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(U_C64_EXCEPTION_FORM) == null){
			if(currentUC64_exceptionForm!=null){
				request.getSession().setAttribute(U_C64_EXCEPTION_FORM, currentUC64_exceptionForm);
			}else {
				currentUC64_exceptionForm = new UC64_exceptionForm();
				request.getSession().setAttribute(U_C64_EXCEPTION_FORM, currentUC64_exceptionForm);	
			}
		} else {
			currentUC64_exceptionForm = (UC64_exceptionForm) request.getSession().getAttribute(U_C64_EXCEPTION_FORM);
		}

		currentUC64_exceptionForm = (UC64_exceptionForm)populateDestinationForm(request.getSession(), currentUC64_exceptionForm);
		// Call all the Precontroller.
	currentUC64_exceptionForm.setAlwaysCallPreControllers(true);
		model.addAttribute(U_C64_EXCEPTION_FORM, currentUC64_exceptionForm);
		request.getSession().setAttribute(U_C64_EXCEPTION_FORM, currentUC64_exceptionForm);
		
	}
	
	
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("UC64_exceptionController [ ");
		// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(uC64_exceptionValidator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsUC64_exceptionController!=null); 
		return strBToS.toString();
	}
}
