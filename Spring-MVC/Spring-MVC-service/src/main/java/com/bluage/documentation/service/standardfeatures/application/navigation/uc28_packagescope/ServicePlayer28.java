/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.service.standardfeatures.application.navigation.uc28_packagescope;


import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import com.bluage.documentation.business.standardfeatures.application.navigation.uc28_packagescope.entities.daofinder.Player28DAOFinderImpl;
import com.bluage.documentation.service.AbstractService;
import com.netfective.bluage.core.exception.ApplicationException;

/**
 * Defines  application services for ServicePlayer28
  */
@Named("com.bluage.documentation.service.standardfeatures.application.navigation.uc28_packagescope.ServicePlayer28")
public class ServicePlayer28  extends AbstractService{


			@Inject
		@Named ("com.bluage.documentation.business.standardfeatures.application.navigation.uc28_packagescope.entities.daofinder.Player28DAOFinderImpl")
		private Player28DAOFinderImpl player28BOFinder;



	/**
	 *  Default constructor 
	 */
	public ServicePlayer28() {
		super(); // Call super for default constructor : ServicePlayer28
	}
	



								
	/**    
	 * Operation Crud
	 * @return {@link List}
	 */
	public List player28FindAll( )
 throws ApplicationException {

					return player28BOFinder.findAll();
	}

		}
