/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.application.form.uc43_captcha;


import java.io.Serializable;
import java.util.List;

import com.bluage.documentation.business.standardfeatures.application.form.uc43_captcha.bos.Player43BO;
import com.netfective.bluage.core.web.AbstractForm;
import com.netfective.bluage.core.web.IForm;

/**
* Class : UC43_AddPlayerForm
*/
public class UC43_AddPlayerForm extends AbstractForm implements Serializable, IForm {
	
	/** Constant : serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	//CONSTANT : captcha
	private static final String CAPTCHA = "captcha";
	//CONSTANT : listPlayers
	private static final String LIST_PLAYERS = "listPlayers";
	//CONSTANT : allPositions
	private static final String ALL_POSITIONS = "allPositions";
	//CONSTANT : playerToAdd
	private static final String PLAYER_TO_ADD = "playerToAdd";
	//CONSTANT : selectedPlayer
	private static final String SELECTED_PLAYER = "selectedPlayer";
	//CONSTANT : captcha
	private String captcha;
	/**
	 * 	Property: listPlayers 
	 */
	private List<Player43BO> listPlayers;
	/**
	 * 	Property: allPositions 
	 */
	private List allPositions;
	/**
	 * 	Property: playerToAdd 
	 */
	private Player43BO playerToAdd;
	/**
	 * 	Property: selectedPlayer 
	 */
	private Player43BO selectedPlayer;
/**
	 * Default constructor : UC43_AddPlayerForm
	 */
	public UC43_AddPlayerForm() {
		super();
		// Initialize : listPlayers
		this.listPlayers = new java.util.ArrayList<com.bluage.documentation.business.standardfeatures.application.form.uc43_captcha.bos.Player43BO>();
		// Initialize : allPositions
		this.allPositions = new java.util.ArrayList();
		// Initialize : playerToAdd
		this.playerToAdd = new Player43BO();
		// Initialize : selectedPlayer
		this.selectedPlayer = null;
					this.setAlwaysCallPreControllers(true);
	}
	
	/**
	 * 	Getter : listPlayers 
	 * 	@return : Return the listPlayers instance.
	 */
	public List<Player43BO> getListPlayers(){
		return listPlayers; // For UC43_AddPlayerForm
	}
	
	/**
	 * 	Setter : listPlayers 
	 *  @param listPlayersinstance : The instance to set.
	 */
	public void setListPlayers(final List<Player43BO> listPlayersinstance){
		this.listPlayers = listPlayersinstance;// For UC43_AddPlayerForm
	}
	/**
	 * 	Getter : allPositions 
	 * 	@return : Return the allPositions instance.
	 */
	public List getAllPositions(){
		return allPositions; // For UC43_AddPlayerForm
	}
	
	/**
	 * 	Setter : allPositions 
	 *  @param allPositionsinstance : The instance to set.
	 */
	public void setAllPositions(final List allPositionsinstance){
		this.allPositions = allPositionsinstance;// For UC43_AddPlayerForm
	}
	/**
	 * 	Getter : playerToAdd 
	 * 	@return : Return the playerToAdd instance.
	 */
	public Player43BO getPlayerToAdd(){
		return playerToAdd; // For UC43_AddPlayerForm
	}
	
	/**
	 * 	Setter : playerToAdd 
	 *  @param playerToAddinstance : The instance to set.
	 */
	public void setPlayerToAdd(final Player43BO playerToAddinstance){
		this.playerToAdd = playerToAddinstance;// For UC43_AddPlayerForm
	}
	/**
	 * 	Getter : selectedPlayer 
	 * 	@return : Return the selectedPlayer instance.
	 */
	public Player43BO getSelectedPlayer(){
		return selectedPlayer; // For UC43_AddPlayerForm
	}
	
	/**
	 * 	Setter : selectedPlayer 
	 *  @param selectedPlayerinstance : The instance to set.
	 */
	public void setSelectedPlayer(final Player43BO selectedPlayerinstance){
		this.selectedPlayer = selectedPlayerinstance;// For UC43_AddPlayerForm
	}
	/**
	 * getter for captcha
	 * @return String captcha
	 */
	public String getCaptcha() {
		return captcha;
	}

	/**
	 * setter for captcha
	 * @param captcha String
	 */
	public void setCaptcha(String captcha) {
		this.captcha = captcha;
	}
/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UC43_AddPlayerForm [ "+
LIST_PLAYERS +" = " + listPlayers +ALL_POSITIONS +" = " + allPositions +PLAYER_TO_ADD +" = " + playerToAdd +SELECTED_PLAYER +" = " + selectedPlayer + "]";
	}
	
	/**
	 * Method setInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public void setInstanceByInstanceName(final String instanceName, final Object instance) {
		// Set the instance ListPlayers.
		if(LIST_PLAYERS.equals(instanceName)){// For UC43_AddPlayerForm
			this.setListPlayers((List<Player43BO>)instance); // For UC43_AddPlayerForm
		}
				// Set the instance AllPositions.
		if(ALL_POSITIONS.equals(instanceName)){// For UC43_AddPlayerForm
			this.setAllPositions((List)instance); // For UC43_AddPlayerForm
		}
				// Set the instance PlayerToAdd.
		if(PLAYER_TO_ADD.equals(instanceName)){// For UC43_AddPlayerForm
			this.setPlayerToAdd((Player43BO)instance); // For UC43_AddPlayerForm
		}
				// Set the instance SelectedPlayer.
		if(SELECTED_PLAYER.equals(instanceName)){// For UC43_AddPlayerForm
			this.setSelectedPlayer((Player43BO)instance); // For UC43_AddPlayerForm
		}
				// Set the instancecaptcha
		if (CAPTCHA.equals(instanceName)) {
			this.setCaptcha((String)instance);
		}
	}
	
	/**
	 * Method getInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public Object getInstanceByInstanceName(final String instanceName){
		// Temporary object for tmpUC43_AddPlayerForm.
		Object tmpUC43_AddPlayerForm = null;
		
			
		// Get the instance ListPlayers for UC43_AddPlayerForm.
		if(LIST_PLAYERS.equals(instanceName)){ // For UC43_AddPlayerForm
			tmpUC43_AddPlayerForm = this.getListPlayers(); // For UC43_AddPlayerForm
		}
			
		// Get the instance AllPositions for UC43_AddPlayerForm.
		if(ALL_POSITIONS.equals(instanceName)){ // For UC43_AddPlayerForm
			tmpUC43_AddPlayerForm = this.getAllPositions(); // For UC43_AddPlayerForm
		}
			
		// Get the instance PlayerToAdd for UC43_AddPlayerForm.
		if(PLAYER_TO_ADD.equals(instanceName)){ // For UC43_AddPlayerForm
			tmpUC43_AddPlayerForm = this.getPlayerToAdd(); // For UC43_AddPlayerForm
		}
			
		// Get the instance SelectedPlayer for UC43_AddPlayerForm.
		if(SELECTED_PLAYER.equals(instanceName)){ // For UC43_AddPlayerForm
			tmpUC43_AddPlayerForm = this.getSelectedPlayer(); // For UC43_AddPlayerForm
		}
		// Get the instance captcha for UC43_AddPlayerForm.
		if (CAPTCHA.equals(instanceName)) {
			tmpUC43_AddPlayerForm = this.getCaptcha(); 
		}
		return tmpUC43_AddPlayerForm;// For UC43_AddPlayerForm
	}
	
			}
