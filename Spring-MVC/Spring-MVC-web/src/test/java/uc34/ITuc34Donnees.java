package uc34;

import org.junit.Test;
import test.common.AbstractBADocTestCase;

public class ITuc34Donnees extends AbstractBADocTestCase {
	
	@Override
	public  String getName(){
		return "UC-34";
	}

	@Test
	public void testITuc34Donnees() throws Exception {
		// On teste l'UC34 - Standard Features/Application/Datagrid - PaginationTable
		// On v�rifie que les donn�es des �quipes sont bien affich�es.
		selenium.open("");
		selenium.click("link=Expand All");
		selenium.waitForPageToLoad("30000");
		selenium.click("link=UC34 - Pagination Table");
		verifyTrue(selenium.isElementPresent("//h2[contains(text(),\"Standard Features > Application > DataGrid > UC34 > Pagination Table\")]"));
		// On v�rifie qu'on est sur la premi�re page.
		assertTrue(selenium.isElementPresent("//a/b[contains(text(),\"1\")]|//strong[contains(text(),\"1\")]"));
		// On v�rifie les donn�es.
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/thead/tr[th[contains(text(),\"City\")] and th[contains(text(),\"Name\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Charlotte\")] and td/span[contains(text(),\"Tigers\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Kansas City\")] and td/span[contains(text(),\"Bears\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Detroit\")] and td/span[contains(text(),\"Chargers\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Detroit\")] and td/span[contains(text(),\"Hawks\")]]"));
		// On va sur la page 2.
		selenium.click("xpath=(//span/a)[1]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/thead/tr[th[contains(text(),\"City\")] and th[contains(text(),\"Name\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Omaha\")] and td/span[contains(text(),\"Braves\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Raleigh\")] and td/span[contains(text(),\"Eagles\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Oakland\")] and td/span[contains(text(),\"Cardinals\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Kansas City\")] and td/span[contains(text(),\"Red Devils\")]]"));
		// On va sur la page 3.
		selenium.click("xpath=(//span/a)[4]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/thead/tr[th[contains(text(),\"City\")] and th[contains(text(),\"Name\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Fresno\")] and td/span[contains(text(),\"Mustangs\")]]"));
		// On testes toutes les donn�es de l'UC
		// Pour les Tigers Charlotte
		selenium.click("link=UC34 - Pagination Table");
		selenium.waitForPageToLoad("30000");
		verifyTrue(selenium.isElementPresent("//h2[contains(text(),\"Standard Features > Application > DataGrid > UC34 > Pagination Table\")]"));
		// On v�rifie qu'on est sur la premi�re page.
		assertTrue(selenium.isElementPresent("//a/b[contains(text(),\"1\")]|//strong[contains(text(),\"1\")]"));
		// On indexe la ligne de Kansas City.
		Number index = selenium.getElementIndex("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Charlotte\")]\n					and td/span[contains(text(),\"Tigers\")]]");
		System.out.println("variable index vaut : " + index);
		// On clique sur [view]
		selenium.click("//table[contains(@id,'tab_teams')]/tbody/tr[" + index + " +\n					1]/td/a[contains(text(),\"[view]\")]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie que l'on est sur la bonne page.
		assertEquals("Tigers", selenium.getText("//*[contains(@id,'txt_TeamName')]"));
		assertEquals("Charlotte", selenium.getText("//*[contains(@id,'txt_Teamcity')]"));
		// On v�rifie les donn�es.
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Oliver\")] and td//span[contains(text(),\"Frank\")] and td//span[contains(text(),\"10/31/1968\")] and td//span[contains(text(),\"0.11\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Abel\")] and td//span[contains(text(),\"Dennis\")] and td//span[contains(text(),\"04/16/1980\")] and td//span[contains(text(),\"30.51\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Lester\")] and td//span[contains(text(),\"Cross\")] and td//span[contains(text(),\"02/21/1976\")] and td//span[contains(text(),\"30.98\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Jerome\")] and td//span[contains(text(),\"Grant\")] and td//span[contains(text(),\"06/28/1982\")] and td//span[contains(text(),\"28.94\")]]"));
		selenium.click("xpath=(//span/a)[1]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Milton\")] and td//span[contains(text(),\"White\")] and td//span[contains(text(),\"12/20/1970\")] and td//span[contains(text(),\"25.4\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Nelson\")] and td//span[contains(text(),\"Cook\")] and td//span[contains(text(),\"03/01/1966\")] and td//span[contains(text(),\"34.95\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Vernon\")] and td//span[contains(text(),\"Davis\")] and td//span[contains(text(),\"10/19/1971\")] and td//span[contains(text(),\"2.24\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Earnest\")] and td//span[contains(text(),\"Maxwell\")] and td//span[contains(text(),\"01/20/1986\")] and td//span[contains(text(),\"42.16\")]]"));
		selenium.click("xpath=(//span/a)[4]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Dwayne\")] and td//span[contains(text(),\"Stevenson\")] and td//span[contains(text(),\"10/26/1968\")] and td//span[contains(text(),\"43.02\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Darryl\")] and td//span[contains(text(),\"Norris\")] and td//span[contains(text(),\"01/22/1965\")] and td//span[contains(text(),\"47.7\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Jerome\")] and td//span[contains(text(),\"Griffin\")] and td//span[contains(text(),\"12/19/1987\")] and td//span[contains(text(),\"9.15\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Stanley\")] and td//span[contains(text(),\"Wolfe\")] and td//span[contains(text(),\"12/14/1985\")] and td//span[contains(text(),\"0.51\")]]"));
		selenium.click("xpath=(//span/a)[5]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Edwin\")] and td//span[contains(text(),\"Reese\")] and td//span[contains(text(),\"06/20/1989\")] and td//span[contains(text(),\"10.7\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Salvador\")] and td//span[contains(text(),\"Bryant\")] and td//span[contains(text(),\"08/24/1965\")] and td//span[contains(text(),\"35.83\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Paul\")] and td//span[contains(text(),\"Page\")] and td//span[contains(text(),\"12/13/1969\")] and td//span[contains(text(),\"25.81\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Darnell\")] and td//span[contains(text(),\"Williamson\")] and td//span[contains(text(),\"06/15/1978\")] and td//span[contains(text(),\"45.08\")]]"));
		selenium.click("xpath=(//span/a)[6]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Joshua\")] and td//span[contains(text(),\"Hardy\")] and td//span[contains(text(),\"04/09/1975\")] and td//span[contains(text(),\"9.63\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Clarence\")] and td//span[contains(text(),\"Johnson\")] and td//span[contains(text(),\"05/25/1972\")] and td//span[contains(text(),\"2.02\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Dominic\")] and td//span[contains(text(),\"Martinez\")] and td//span[contains(text(),\"03/04/1967\")] and td//span[contains(text(),\"1.34\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Marvin\")] and td//span[contains(text(),\"Mitchell\")] and td//span[contains(text(),\"05/27/1981\")] and td//span[contains(text(),\"7.94\")]]"));
		selenium.click("xpath=(//span/a)[7]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Irvin\")] and td//span[contains(text(),\"Lee\")] and td//span[contains(text(),\"02/25/1988\")] and td//span[contains(text(),\"10.61\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Wayne\")] and td//span[contains(text(),\"Rose\")] and td//span[contains(text(),\"10/28/1981\")] and td//span[contains(text(),\"20.41\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Lucas\")] and td//span[contains(text(),\"Williamson\")] and td//span[contains(text(),\"06/29/1983\")] and td//span[contains(text(),\"38.36\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Walter\")] and td//span[contains(text(),\"Reyes\")] and td//span[contains(text(),\"12/09/1984\")] and td//span[contains(text(),\"46.15\")]]"));
		selenium.click("xpath=(//span/a)[7]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Rafael\")] and td//span[contains(text(),\"Steele\")] and td//span[contains(text(),\"11/25/1968\")] and td//span[contains(text(),\"15.48\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Rafael\")] and td//span[contains(text(),\"Ward\")] and td//span[contains(text(),\"02/08/1967\")] and td//span[contains(text(),\"38.77\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Kenny\")] and td//span[contains(text(),\"Rogers\")] and td//span[contains(text(),\"02/02/1965\")] and td//span[contains(text(),\"5.44\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Byron\")] and td//span[contains(text(),\"Morton\")] and td//span[contains(text(),\"03/28/1981\")] and td//span[contains(text(),\"21.4\")]]"));
		selenium.click("xpath=(//span/a)[7]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Bob\")] and td//span[contains(text(),\"Estrada\")] and td//span[contains(text(),\"10/20/1975\")] and td//span[contains(text(),\"39.59\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Rodolfo\")] and td//span[contains(text(),\"Wong\")] and td//span[contains(text(),\"08/10/1968\")] and td//span[contains(text(),\"26.97\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Floyd\")] and td//span[contains(text(),\"Hampton\")] and td//span[contains(text(),\"03/03/1988\")] and td//span[contains(text(),\"21.67\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Randal\")] and td//span[contains(text(),\"Mendez\")] and td//span[contains(text(),\"03/04/1974\")] and td//span[contains(text(),\"21.2\")]]"));
		selenium.click("xpath=(//span/a)[7]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Shannon\")] and td//span[contains(text(),\"Watkins\")] and td//span[contains(text(),\"02/07/1979\")] and td//span[contains(text(),\"25.66\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Travis\")] and td//span[contains(text(),\"Williamson\")] and td//span[contains(text(),\"08/23/1974\")] and td//span[contains(text(),\"20.3\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Matthew\")] and td//span[contains(text(),\"Haynes\")] and td//span[contains(text(),\"07/16/1982\")] and td//span[contains(text(),\"18.91\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Clayton\")] and td//span[contains(text(),\"Doyle\")] and td//span[contains(text(),\"12/10/1976\")] and td//span[contains(text(),\"36.24\")]]"));
		selenium.click("xpath=(//span/a)[7]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Nathan\")] and td//span[contains(text(),\"Banks\")] and td//span[contains(text(),\"10/10/1978\")] and td//span[contains(text(),\"7.15\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Wallace\")] and td//span[contains(text(),\"Wilkins\")] and td//span[contains(text(),\"02/10/1984\")] and td//span[contains(text(),\"12.66\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Carroll\")] and td//span[contains(text(),\"Pena\")] and td//span[contains(text(),\"08/27/1974\")] and td//span[contains(text(),\"40.28\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Spencer\")] and td//span[contains(text(),\"Ward\")] and td//span[contains(text(),\"11/13/1971\")] and td//span[contains(text(),\"35.48\")]]"));
		selenium.click("xpath=(//span/a)[7]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Courtney\")] and td//span[contains(text(),\"Perry\")] and td//span[contains(text(),\"09/16/1969\")] and td//span[contains(text(),\"4.98\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Bert\")] and td//span[contains(text(),\"Wheeler\")] and td//span[contains(text(),\"11/27/1977\")] and td//span[contains(text(),\"1.15\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Sidney\")] and td//span[contains(text(),\"Phelps\")] and td//span[contains(text(),\"05/05/1965\")] and td//span[contains(text(),\"35.44\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Juan\")] and td//span[contains(text(),\"Norris\")] and td//span[contains(text(),\"08/29/1968\")] and td//span[contains(text(),\"35.14\")]]"));
		selenium.click("xpath=(//span/a)[8]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Gustavo\")] and td//span[contains(text(),\"Saunders\")] and td//span[contains(text(),\"08/27/1983\")] and td//span[contains(text(),\"45.94\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Darnell\")] and td//span[contains(text(),\"Moreno\")] and td//span[contains(text(),\"07/09/1966\")] and td//span[contains(text(),\"43.34\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Hector\")] and td//span[contains(text(),\"Walters\")] and td//span[contains(text(),\"12/05/1970\")] and td//span[contains(text(),\"49.02\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Kelvin\")] and td//span[contains(text(),\"Stewart\")] and td//span[contains(text(),\"12/03/1976\")] and td//span[contains(text(),\"9.38\")]]"));
		selenium.click("xpath=(//span/a)[9]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("3", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Lance\")] and td//span[contains(text(),\"Boone\")] and td//span[contains(text(),\"04/08/1980\")] and td//span[contains(text(),\"28.66\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Felix\")] and td//span[contains(text(),\"Carlson\")] and td//span[contains(text(),\"08/07/1968\")] and td//span[contains(text(),\"49.46\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Allen\")] and td//span[contains(text(),\"Wolfe\")] and td//span[contains(text(),\"12/11/1988\")] and td//span[contains(text(),\"14.68\")]]"));
		// Pour les Chargers Detroit
		selenium.click("link=UC34 - Pagination Table");
		selenium.waitForPageToLoad("30000");
		verifyTrue(selenium.isElementPresent("//h2[contains(text(),\"Standard Features > Application > DataGrid > UC34 > Pagination Table\")]"));
		// On indexe la ligne de Detroit.
		 index = selenium.getElementIndex("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Detroit\")] and td/span[contains(text(),\"Chargers\")]]");
		System.out.println("variable index vaut : " + index);
		// On clique sur [view]
		selenium.click("//table[contains(@id,'tab_teams')]/tbody/tr[" + index + " + 1]/td/a[contains(text(),\"[view]\")]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie que l'on est sur la bonne page.
		assertEquals("Chargers", selenium.getText("//*[contains(@id,'txt_TeamName')]"));
		assertEquals("Detroit", selenium.getText("//*[contains(@id,'txt_Teamcity')]"));
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Michael\")] and td//span[contains(text(),\"Hodges\")] and td//span[contains(text(),\"04/01/1988\")] and td//span[contains(text(),\"25.35\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Roderick\")] and td//span[contains(text(),\"Robbins\")] and td//span[contains(text(),\"07/29/1975\")] and td//span[contains(text(),\"12.04\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Vernon\")] and td//span[contains(text(),\"Lawson\")] and td//span[contains(text(),\"09/13/1974\")] and td//span[contains(text(),\"22.48\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Matthew\")] and td//span[contains(text(),\"Fitzgerald\")] and td//span[contains(text(),\"08/09/1982\")] and td//span[contains(text(),\"33.61\")]]"));
		selenium.click("xpath=(//span/a)[1]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Timmy\")] and td//span[contains(text(),\"Miller\")] and td//span[contains(text(),\"08/07/1972\")] and td//span[contains(text(),\"35.17\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Earl\")] and td//span[contains(text(),\"Barrett\")] and td//span[contains(text(),\"09/15/1982\")] and td//span[contains(text(),\"45.34\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Freddie\")] and td//span[contains(text(),\"Williamson\")] and td//span[contains(text(),\"10/10/1976\")] and td//span[contains(text(),\"3.8\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Dale\")] and td//span[contains(text(),\"Colon\")] and td//span[contains(text(),\"09/28/1984\")] and td//span[contains(text(),\"9.25\")]]"));
		selenium.click("xpath=(//span/a)[4]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Rolando\")] and td//span[contains(text(),\"Waters\")] and td//span[contains(text(),\"02/26/1966\")] and td//span[contains(text(),\"7.1\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Gerald\")] and td//span[contains(text(),\"Sanders\")] and td//span[contains(text(),\"03/03/1976\")] and td//span[contains(text(),\"21.68\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Noel\")] and td//span[contains(text(),\"Oliver\")] and td//span[contains(text(),\"11/24/1984\")] and td//span[contains(text(),\"45.18\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Kirk\")] and td//span[contains(text(),\"Cox\")] and td//span[contains(text(),\"05/10/1973\")] and td//span[contains(text(),\"26.58\")]]"));
		selenium.click("xpath=(//span/a)[5]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Timmy\")] and td//span[contains(text(),\"Walker\")] and td//span[contains(text(),\"04/14/1969\")] and td//span[contains(text(),\"45.91\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Jon\")] and td//span[contains(text(),\"Hines\")] and td//span[contains(text(),\"12/27/1980\")] and td//span[contains(text(),\"24.88\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Ralph\")] and td//span[contains(text(),\"Burton\")] and td//span[contains(text(),\"11/13/1969\")] and td//span[contains(text(),\"7.52\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Abraham\")] and td//span[contains(text(),\"Page\")] and td//span[contains(text(),\"03/20/1978\")] and td//span[contains(text(),\"19.24\")]]"));
		selenium.click("xpath=(//span/a)[6]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Kim\")] and td//span[contains(text(),\"Hammond\")] and td//span[contains(text(),\"07/31/1979\")] and td//span[contains(text(),\"0.22\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Matthew\")] and td//span[contains(text(),\"Baker\")] and td//span[contains(text(),\"09/16/1985\")] and td//span[contains(text(),\"4.25\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Myron\")] and td//span[contains(text(),\"Roberts\")] and td//span[contains(text(),\"03/08/1973\")] and td//span[contains(text(),\"19.67\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Calvin\")] and td//span[contains(text(),\"Frazier\")] and td//span[contains(text(),\"12/09/1989\")] and td//span[contains(text(),\"12.98\")]]"));
		selenium.click("xpath=(//span/a)[7]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("2", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Leroy\")] and td//span[contains(text(),\"Bowman\")] and td//span[contains(text(),\"01/24/1980\")] and td//span[contains(text(),\"41.46\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Marcos\")] and td//span[contains(text(),\"Perry\")] and td//span[contains(text(),\"01/15/1968\")] and td//span[contains(text(),\"10.8\")]]"));
		// Pour les Hawks Detroit
		selenium.click("link=UC34 - Pagination Table");
		selenium.waitForPageToLoad("30000");
		verifyTrue(selenium.isElementPresent("//h2[contains(text(),\"Standard Features > Application > DataGrid > UC34 > Pagination Table\")]"));
		// On indexe la ligne de Detroit
		 index = selenium.getElementIndex("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Detroit\")] and td/span[contains(text(),\"Hawks\")]]");
		System.out.println("variable index vaut : " + index);
		// On clique sur [view]
		selenium.click("//table[contains(@id,'tab_teams')]/tbody/tr[" + index + " + 1]/td/a[contains(text(),\"[view]\")]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie que l'on est sur la bonne page.
		assertEquals("Hawks", selenium.getText("//*[contains(@id,'txt_TeamName')]"));
		assertEquals("Detroit", selenium.getText("//*[contains(@id,'txt_Teamcity')]"));
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Ricky\")] and td//span[contains(text(),\"Dawson\")] and td//span[contains(text(),\"03/03/1990\")] and td//span[contains(text(),\"29.3\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Howard\")] and td//span[contains(text(),\"Ramos\")] and td//span[contains(text(),\"08/25/1970\")] and td//span[contains(text(),\"10.76\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Ruben\")] and td//span[contains(text(),\"Pope\")] and td//span[contains(text(),\"05/01/1985\")] and td//span[contains(text(),\"44.66\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Ivan\")] and td//span[contains(text(),\"Tate\")] and td//span[contains(text(),\"02/08/1972\")] and td//span[contains(text(),\"48.49\")]]"));
		selenium.click("xpath=(//span/a)[1]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Woodrow\")] and td//span[contains(text(),\"Carpenter\")] and td//span[contains(text(),\"08/08/1987\")] and td//span[contains(text(),\"41.76\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Christian\")] and td//span[contains(text(),\"Chavez\")] and td//span[contains(text(),\"05/17/1983\")] and td//span[contains(text(),\"37.88\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Salvatore\")] and td//span[contains(text(),\"Robertson\")] and td//span[contains(text(),\"12/22/1979\")] and td//span[contains(text(),\"36.17\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Marshall\")] and td//span[contains(text(),\"Soto\")] and td//span[contains(text(),\"10/04/1977\")] and td//span[contains(text(),\"35.64\")]]"));
		selenium.click("xpath=(//span/a)[4]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Jordan\")] and td//span[contains(text(),\"Hayes\")] and td//span[contains(text(),\"07/11/1989\")] and td//span[contains(text(),\"44.42\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Timmy\")] and td//span[contains(text(),\"Carson\")] and td//span[contains(text(),\"09/13/1966\")] and td//span[contains(text(),\"13.62\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Alfonso\")] and td//span[contains(text(),\"Harrison\")] and td//span[contains(text(),\"07/28/1981\")] and td//span[contains(text(),\"47.43\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Brett\")] and td//span[contains(text(),\"Weaver\")] and td//span[contains(text(),\"10/04/1971\")] and td//span[contains(text(),\"14.83\")]]"));
		selenium.click("xpath=(//span/a)[5]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"John\")] and td//span[contains(text(),\"Lynch\")] and td//span[contains(text(),\"09/06/1980\")] and td//span[contains(text(),\"28.35\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Jody\")] and td//span[contains(text(),\"Elliott\")] and td//span[contains(text(),\"05/07/1972\")] and td//span[contains(text(),\"45.29\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Gilbert\")] and td//span[contains(text(),\"Johnston\")] and td//span[contains(text(),\"12/30/1981\")] and td//span[contains(text(),\"40.26\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Preston\")] and td//span[contains(text(),\"Medina\")] and td//span[contains(text(),\"02/15/1974\")] and td//span[contains(text(),\"0.33\")]]"));
		selenium.click("xpath=(//span/a)[6]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Jared\")] and td//span[contains(text(),\"Stevenson\")] and td//span[contains(text(),\"05/03/1974\")] and td//span[contains(text(),\"12.11\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Elmer\")] and td//span[contains(text(),\"Hansen\")] and td//span[contains(text(),\"07/12/1984\")] and td//span[contains(text(),\"12.86\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Wayne\")] and td//span[contains(text(),\"Clarke\")] and td//span[contains(text(),\"07/24/1965\")] and td//span[contains(text(),\"24.9\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Orlando\")] and td//span[contains(text(),\"Burns\")] and td//span[contains(text(),\"09/02/1973\")] and td//span[contains(text(),\"19.14\")]]"));
		// Pour les Mustangs Fresno
		selenium.click("link=UC34 - Pagination Table");
		selenium.waitForPageToLoad("30000");
		verifyTrue(selenium.isElementPresent("//h2[contains(text(),\"Standard Features > Application > DataGrid > UC34 > Pagination Table\")]"));
		// On clique sur 'Last''
		selenium.click("xpath=(//span/a)[4]");
		selenium.waitForPageToLoad("30000");
		// On indexe la ligne de Fresno.
		 index = selenium.getElementIndex("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Fresno\")] and td/span[contains(text(),\"Mustangs\")]]");
		System.out.println("variable index vaut : " + index);
		// On clique sur [view]
		selenium.click("//table[contains(@id,'tab_teams')]/tbody/tr[" + index + " + 1]/td/a[contains(text(),\"[view]\")]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie que l'on est sur la bonne page.
		assertEquals("Mustangs", selenium.getText("//*[contains(@id,'txt_TeamName')]"));
		verifyEquals("3", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Sergio\")] and td//span[contains(text(),\"Taylor\")] and td//span[contains(text(),\"08/29/1984\")] and td//span[contains(text(),\"25.29\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Pete\")] and td//span[contains(text(),\"Daniel\")] and td//span[contains(text(),\"03/10/1968\")] and td//span[contains(text(),\"17.11\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Lloyd\")] and td//span[contains(text(),\"Allison\")] and td//span[contains(text(),\"06/20/1975\")] and td//span[contains(text(),\"10.77\")]]"));
		// Pour les Red Devils Kansas City
		selenium.click("link=UC34 - Pagination Table");
		selenium.waitForPageToLoad("30000");
		verifyTrue(selenium.isElementPresent("//h2[contains(text(),\"Standard Features > Application > DataGrid > UC34 > Pagination Table\")]"));
		// On clique sur 'Next'
		selenium.click("xpath=(//span/a)[1]");
		selenium.waitForPageToLoad("30000");
		// On indexe la ligne de Kansas City.
		 index = selenium.getElementIndex("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Kansas City\")] and td/span[contains(text(),\"Red Devils\")]]");
		System.out.println("variable index vaut : " + index);
		// On clique sur [view]
		selenium.click("//table[contains(@id,'tab_teams')]/tbody/tr[" + index + " + 1]/td/a[contains(text(),\"[view]\")]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie que l'on est sur la bonne page.
		assertEquals("Red Devils", selenium.getText("//*[contains(@id,'txt_TeamName')]"));
		assertEquals("Kansas City", selenium.getText("//*[contains(@id,'txt_Teamcity')]"));
		// On v�rifie que rien ne s'affiche.
//		String url = selenium.getLocation();
//		String count = selenium.getEval("(storedVars['url'].indexOf('Struts') != -1) ? 1 : 0");
		verifyEquals(0, selenium.getXpathCount("//table[contains(@id,'tab_player')]//tbody//tr"));
		// Pour les Bears Kansas City
		selenium.click("link=UC34 - Pagination Table");
		selenium.waitForPageToLoad("30000");
		verifyTrue(selenium.isElementPresent("//h2[contains(text(),\"Standard Features > Application > DataGrid > UC34 > Pagination Table\")]"));
		// On indexe la ligne de Kansas City.
		 index = selenium.getElementIndex("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Kansas City\")] and td/span[contains(text(),\"Bears\")]]");
		System.out.println("variable index vaut : " + index);
		// On clique sur [view]
		selenium.click("//table[contains(@id,'tab_teams')]/tbody/tr[" + index + " +\n					1]/td/a[contains(text(),\"[view]\")]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie que l'on est sur la bonne page.
		assertEquals("Bears", selenium.getText("//*[contains(@id,'txt_TeamName')]"));
		assertEquals("Kansas City", selenium.getText("//*[contains(@id,'txt_Teamcity')]"));
		// On v�rifie les donn�es.
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Elbert\")] and\n					td//span[contains(text(),\"Cole\")] and\n					td//span[contains(text(),\"04/07/1965\")] and\n					td//span[contains(text(),\"3.95\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Earl\")] and\n					td//span[contains(text(),\"Russell\")] and\n					td//span[contains(text(),\"12/15/1981\")] and\n					td//span[contains(text(),\"44.73\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Nick\")] and\n					td//span[contains(text(),\"Garner\")] and\n					td//span[contains(text(),\"07/27/1992\")] and\n					td//span[contains(text(),\"47.84\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Keith\")] and\n					td//span[contains(text(),\"Barnett\")] and\n					td//span[contains(text(),\"11/10/1976\")] and\n					td//span[contains(text(),\"5.81\")]]"));
		selenium.click("xpath=(//span/a)[1]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Tommy\")] and\n					td//span[contains(text(),\"Wood\")] and\n					td//span[contains(text(),\"01/20/1992\")] and\n					td//span[contains(text(),\"13.93\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Sylvester\")]\n					and td//span[contains(text(),\"Nash\")] and\n					td//span[contains(text(),\"07/10/1965\")] and\n					td//span[contains(text(),\"37.73\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Cecil\")] and\n					td//span[contains(text(),\"Conner\")] and\n					td//span[contains(text(),\"06/29/1978\")] and\n					td//span[contains(text(),\"39.71\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Gustavo\")] and\n					td//span[contains(text(),\"Romero\")] and\n					td//span[contains(text(),\"02/10/1978\")] and\n					td//span[contains(text(),\"5.03\")]]"));
		selenium.click("xpath=(//span/a)[4]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Guadalupe\")]\n					and td//span[contains(text(),\"Banks\")] and\n					td//span[contains(text(),\"07/03/1986\")] and\n					td//span[contains(text(),\"30.21\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Todd\")] and\n					td//span[contains(text(),\"Gilbert\")] and\n					td//span[contains(text(),\"03/13/1976\")] and\n					td//span[contains(text(),\"0.42\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Gerardo\")] and\n					td//span[contains(text(),\"George\")] and\n					td//span[contains(text(),\"03/17/1982\")] and\n					td//span[contains(text(),\"11.32\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Jim\")] and\n					td//span[contains(text(),\"Riley\")] and\n					td//span[contains(text(),\"10/20/1971\")] and\n					td//span[contains(text(),\"43.98\")]]"));
		selenium.click("xpath=(//span/a)[5]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Antonio\")] and\n					td//span[contains(text(),\"Steele\")] and\n					td//span[contains(text(),\"03/05/1984\")] and\n					td//span[contains(text(),\"41.65\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Ralph\")] and\n					td//span[contains(text(),\"Crawford\")] and\n					td//span[contains(text(),\"10/06/1974\")] and\n					td//span[contains(text(),\"38.43\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Salvador\")] and\n					td//span[contains(text(),\"Watson\")] and\n					td//span[contains(text(),\"10/02/1984\")] and\n					td//span[contains(text(),\"49.9\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Dean\")] and\n					td//span[contains(text(),\"Stokes\")] and\n					td//span[contains(text(),\"10/15/1974\")] and\n					td//span[contains(text(),\"49.09\")]]"));
		selenium.click("xpath=(//span/a)[6]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Allen\")] and\n					td//span[contains(text(),\"Freeman\")] and\n					td//span[contains(text(),\"09/03/1969\")] and\n					td//span[contains(text(),\"33.28\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Willis\")] and\n					td//span[contains(text(),\"Francis\")] and\n					td//span[contains(text(),\"06/21/1977\")] and\n					td//span[contains(text(),\"23.91\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Pablo\")] and\n					td//span[contains(text(),\"Rhodes\")] and\n					td//span[contains(text(),\"06/17/1982\")] and\n					td//span[contains(text(),\"31.6\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Dana\")] and\n					td//span[contains(text(),\"Hamilton\")] and\n					td//span[contains(text(),\"05/30/1983\")] and\n					td//span[contains(text(),\"33.6\")]]"));
		selenium.click("xpath=(//span/a)[7]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Cecil\")] and\n					td//span[contains(text(),\"Wells\")] and\n					td//span[contains(text(),\"12/26/1989\")] and\n					td//span[contains(text(),\"38.61\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Ricardo\")] and\n					td//span[contains(text(),\"Oliver\")] and\n					td//span[contains(text(),\"05/02/1968\")] and\n					td//span[contains(text(),\"20.5\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Julian\")] and\n					td//span[contains(text(),\"Gibbs\")] and\n					td//span[contains(text(),\"07/01/1985\")] and\n					td//span[contains(text(),\"23.34\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Leroy\")] and\n					td//span[contains(text(),\"Moore\")] and\n					td//span[contains(text(),\"07/26/1987\")] and\n					td//span[contains(text(),\"45.8\")]]"));
		selenium.click("xpath=(//span/a)[7]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Sammy\")] and\n					td//span[contains(text(),\"Mendez\")] and\n					td//span[contains(text(),\"07/01/1986\")] and\n					td//span[contains(text(),\"2.27\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Bob\")] and\n					td//span[contains(text(),\"Jacobs\")] and\n					td//span[contains(text(),\"05/21/1988\")] and\n					td//span[contains(text(),\"13.23\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Shannon\")] and\n					td//span[contains(text(),\"Barton\")] and\n					td//span[contains(text(),\"03/10/1980\")] and\n					td//span[contains(text(),\"23.81\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Justin\")] and\n					td//span[contains(text(),\"Johnson\")] and\n					td//span[contains(text(),\"02/27/1988\")] and\n					td//span[contains(text(),\"47.95\")]]"));
		selenium.click("xpath=(//span/a)[7]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Everett\")] and\n					td//span[contains(text(),\"Sherman\")] and\n					td//span[contains(text(),\"11/03/1978\")] and\n					td//span[contains(text(),\"23.28\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Lonnie\")] and\n					td//span[contains(text(),\"Brady\")] and\n					td//span[contains(text(),\"12/11/1987\")] and\n					td//span[contains(text(),\"11.83\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Frank\")] and\n					td//span[contains(text(),\"Strickland\")] and\n					td//span[contains(text(),\"06/27/1970\")] and\n					td//span[contains(text(),\"21.88\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Leonard\")] and\n					td//span[contains(text(),\"Stephens\")] and\n					td//span[contains(text(),\"12/02/1965\")] and\n					td//span[contains(text(),\"13.12\")]]"));
		selenium.click("xpath=(//span/a)[7]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Tim\")] and\n					td//span[contains(text(),\"Taylor\")] and\n					td//span[contains(text(),\"01/26/1985\")] and\n					td//span[contains(text(),\"8.16\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Saul\")] and\n					td//span[contains(text(),\"Zimmerman\")] and\n					td//span[contains(text(),\"12/28/1988\")] and\n					td//span[contains(text(),\"45.82\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Bobby\")] and\n					td//span[contains(text(),\"Jefferson\")] and\n					td//span[contains(text(),\"10/06/1985\")] and\n					td//span[contains(text(),\"17.4\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Bradford\")] and\n					td//span[contains(text(),\"Porter\")] and\n					td//span[contains(text(),\"06/02/1977\")] and\n					td//span[contains(text(),\"16.19\")]]"));
		selenium.click("xpath=(//span/a)[7]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Marvin\")] and\n					td//span[contains(text(),\"Davis\")] and\n					td//span[contains(text(),\"06/13/1980\")] and\n					td//span[contains(text(),\"11.0\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Ed\")] and\n					td//span[contains(text(),\"Delgado\")] and\n					td//span[contains(text(),\"02/02/1976\")] and\n					td//span[contains(text(),\"4.43\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Joshua\")] and\n					td//span[contains(text(),\"Bailey\")] and\n					td//span[contains(text(),\"03/25/1980\")] and\n					td//span[contains(text(),\"41.71\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Gordon\")] and\n					td//span[contains(text(),\"Greene\")] and\n					td//span[contains(text(),\"05/07/1970\")] and\n					td//span[contains(text(),\"17.45\")]]"));
		selenium.click("xpath=(//span/a)[7]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Joseph\")] and\n					td//span[contains(text(),\"Cunningham\")] and\n					td//span[contains(text(),\"11/22/1975\")] and\n					td//span[contains(text(),\"0.32\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Alfredo\")] and\n					td//span[contains(text(),\"Hicks\")] and\n					td//span[contains(text(),\"08/21/1965\")] and\n					td//span[contains(text(),\"39.15\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Salvatore\")]\n					and td//span[contains(text(),\"Townsend\")] and\n					td//span[contains(text(),\"01/19/1974\")] and\n					td//span[contains(text(),\"22.71\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Troy\")] and\n					td//span[contains(text(),\"Perry\")] and\n					td//span[contains(text(),\"06/19/1988\")] and\n					td//span[contains(text(),\"8.76\")]]"));
		selenium.click("xpath=(//span/a)[8]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Jamie\")] and\n					td//span[contains(text(),\"Leonard\")] and\n					td//span[contains(text(),\"08/27/1980\")] and\n					td//span[contains(text(),\"23.27\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Earl\")] and\n					td//span[contains(text(),\"Clark\")] and\n					td//span[contains(text(),\"11/20/1970\")] and\n					td//span[contains(text(),\"7.19\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Bruce\")] and\n					td//span[contains(text(),\"Holloway\")] and\n					td//span[contains(text(),\"03/20/1981\")] and\n					td//span[contains(text(),\"41.21\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Phil\")] and\n					td//span[contains(text(),\"Thompson\")] and\n					td//span[contains(text(),\"02/09/1970\")] and\n					td//span[contains(text(),\"5.47\")]]"));
		selenium.click("xpath=(//span/a)[9]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("1", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Van\")] and\n					td//span[contains(text(),\"Pena\")] and\n					td//span[contains(text(),\"10/06/1981\")] and\n					td//span[contains(text(),\"8.44\")]]"));
		// Pour les Cardinals Oakland
		selenium.click("link=UC34 - Pagination Table");
		selenium.waitForPageToLoad("30000");
		verifyTrue(selenium.isElementPresent("//h2[contains(text(),\"Standard Features > Application > DataGrid > UC34 > Pagination Table\")]"));
		// On clique sur 'Next'
		selenium.click("xpath=(//span/a)[1]");
		selenium.waitForPageToLoad("30000");
		// On indexe la ligne de Oakland.
		 index = selenium.getElementIndex("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Oakland\")] and td/span[contains(text(),\"Cardinals\")]]");
		System.out.println("variable index vaut : " + index);
		// On clique sur [view]
		selenium.click("//table[contains(@id,'tab_teams')]/tbody/tr[" + index + " + 1]/td/a[contains(text(),\"[view]\")]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie que l'on est sur la bonne page.
		assertEquals("Cardinals", selenium.getText("//*[contains(@id,'txt_TeamName')]"));
		assertEquals("Oakland", selenium.getText("//*[contains(@id,'txt_Teamcity')]"));
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Arthur\")] and td//span[contains(text(),\"Payne\")] and td//span[contains(text(),\"11/20/1981\")] and td//span[contains(text(),\"30.86\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Milton\")] and td//span[contains(text(),\"Johnson\")] and td//span[contains(text(),\"07/02/1969\")] and td//span[contains(text(),\"22.22\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Sylvester\")] and td//span[contains(text(),\"Rowe\")] and td//span[contains(text(),\"12/16/1970\")] and td//span[contains(text(),\"34.46\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Myron\")] and td//span[contains(text(),\"Atkins\")] and td//span[contains(text(),\"10/20/1973\")] and td//span[contains(text(),\"45.39\")]]"));
		// Pour les Braves Omaha
		selenium.click("link=UC34 - Pagination Table");
		selenium.waitForPageToLoad("30000");
		verifyTrue(selenium.isElementPresent("//h2[contains(text(),\"Standard Features > Application > DataGrid > UC34 > Pagination Table\")]"));
		// On clique sur 'Next'
		selenium.click("xpath=(//span/a)[1]");
		selenium.waitForPageToLoad("30000");
		// On indexe la ligne de Omaha.
		 index = selenium.getElementIndex("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Omaha\")] and td/span[contains(text(),\"Braves\")]]");
		System.out.println("variable index vaut : " + index);
		// On clique sur [view]
		selenium.click("//table[contains(@id,'tab_teams')]/tbody/tr[" + index + " + 1]/td/a[contains(text(),\"[view]\")]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie que l'on est sur la bonne page.
		assertEquals("Braves", selenium.getText("//*[contains(@id,'txt_TeamName')]"));
		assertEquals("Omaha", selenium.getText("//*[contains(@id,'txt_Teamcity')]"));
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Allan\")] and td//span[contains(text(),\"Meyer\")] and td//span[contains(text(),\"02/14/1969\")] and td//span[contains(text(),\"33.67\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Stephen\")] and td//span[contains(text(),\"Lambert\")] and td//span[contains(text(),\"03/14/1977\")] and td//span[contains(text(),\"18.9\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Ralph\")] and td//span[contains(text(),\"Castillo\")] and td//span[contains(text(),\"07/29/1975\")] and td//span[contains(text(),\"36.13\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Stuart\")] and td//span[contains(text(),\"Ford\")] and td//span[contains(text(),\"09/22/1983\")] and td//span[contains(text(),\"2.96\")]]"));
		selenium.click("xpath=(//span/a)[1]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Archie\")] and td//span[contains(text(),\"Roberson\")] and td//span[contains(text(),\"08/11/1974\")] and td//span[contains(text(),\"7.15\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Don\")] and td//span[contains(text(),\"Griffith\")] and td//span[contains(text(),\"10/31/1975\")] and td//span[contains(text(),\"24.25\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Edward\")] and td//span[contains(text(),\"Hogan\")] and td//span[contains(text(),\"02/02/1986\")] and td//span[contains(text(),\"23.24\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Abraham\")] and td//span[contains(text(),\"Drake\")] and td//span[contains(text(),\"11/27/1985\")] and td//span[contains(text(),\"21.77\")]]"));
		selenium.click("xpath=(//span/a)[4]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Juan\")] and td//span[contains(text(),\"Dawson\")] and td//span[contains(text(),\"01/14/1987\")] and td//span[contains(text(),\"9.98\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Owen\")] and td//span[contains(text(),\"Little\")] and td//span[contains(text(),\"10/27/1981\")] and td//span[contains(text(),\"45.87\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Erick\")] and td//span[contains(text(),\"Mccarthy\")] and td//span[contains(text(),\"12/07/1970\")] and td//span[contains(text(),\"3.76\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Johnathan\")] and td//span[contains(text(),\"Norman\")] and td//span[contains(text(),\"08/20/1978\")] and td//span[contains(text(),\"17.62\")]]"));
		selenium.click("xpath=(//span/a)[5]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Bobby\")] and td//span[contains(text(),\"Weaver\")] and td//span[contains(text(),\"11/25/1979\")] and td//span[contains(text(),\"3.11\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Bernard\")] and td//span[contains(text(),\"Wise\")] and td//span[contains(text(),\"02/01/1970\")] and td//span[contains(text(),\"48.48\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Alberto\")] and td//span[contains(text(),\"Dunn\")] and td//span[contains(text(),\"11/23/1969\")] and td//span[contains(text(),\"15.04\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Wallace\")] and td//span[contains(text(),\"Perez\")] and td//span[contains(text(),\"04/12/1974\")] and td//span[contains(text(),\"31.3\")]]"));
		selenium.click("xpath=(//span/a)[6]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("2", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Corey\")] and td//span[contains(text(),\"Herrera\")] and td//span[contains(text(),\"06/25/1977\")] and td//span[contains(text(),\"45.86\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Reginald\")] and td//span[contains(text(),\"Gray\")] and td//span[contains(text(),\"08/23/1986\")] and td//span[contains(text(),\"9.26\")]]"));
		// Pour les Eagles Raleigh
		selenium.click("link=UC34 - Pagination Table");
		selenium.waitForPageToLoad("30000");
		verifyTrue(selenium.isElementPresent("//h2[contains(text(),\"Standard Features > Application > DataGrid > UC34 > Pagination Table\")]"));
		// On clique sur 'Next'
		selenium.click("xpath=(//span/a)[1]");
		selenium.waitForPageToLoad("30000");
		// On indexe la ligne de Raleigh.
	    index = selenium.getElementIndex("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Raleigh\")] and td/span[contains(text(),\"Eagles\")]]");
		System.out.println("variable index vaut : " + index);
		// On clique sur [view]
		selenium.click("//table[contains(@id,'tab_teams')]/tbody/tr[" + index + " + 1]/td/a[contains(text(),\"[view]\")]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie que l'on est sur la bonne page.
		assertEquals("Eagles", selenium.getText("//*[contains(@id,'txt_TeamName')]"));
		assertEquals("Raleigh", selenium.getText("//*[contains(@id,'txt_Teamcity')]"));
		// On v�rifie que rien n'est affich�.
//		 url = selenium.getLocation();
//		 count = selenium.getEval("(storedVars['url'].indexOf('Struts') != -1) ? 1 : 0");
		verifyEquals(0, selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));

	}


}
