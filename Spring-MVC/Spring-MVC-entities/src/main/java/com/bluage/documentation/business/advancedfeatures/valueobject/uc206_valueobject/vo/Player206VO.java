/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.business.advancedfeatures.valueobject.uc206_valueobject.vo;

import java.util.Date;

import com.bluage.documentation.business.AbstractValueObject;
import com.netfective.bluage.core.exception.ApplicationException;
import com.netfective.bluage.core.vo.ConvertField;
/**
 * Player206VO
 *
 *
 *
 */
public class Player206VO
 extends AbstractValueObject {

	/**
	 * Default SUID.
	 */
	private static final long serialVersionUID = 2762801599256899970L;
	
	/**
	 * Used to transfer back and forth the Hibernate optimistic lock.
	 */
	private int version;
	
	/**
	 * Getter for version
	 * @return version
	 */
	public int getVersion() {
		return this.version;
	}
	
	/**
	 * Setter for version
	 * @param int version
	 */
	public void setVersion(int value) {
		this.version = value;
	}
						private Long id
;
	
	/**
	 * Getter for id.
	 * 
	 *
	 * @return {@link Long}
	 */
	public Long getId()
	{
		return this.id;
	}

	/**
	 * Setter for id.
	 * 
	 *
	 * @param value: {@link Long}
	 */
	public void setId(Long value) 
	{
		this.id = value;
	}

	
						private String firstName
;
	
	/**
	 * Getter for firstName.
	 * 
	 *
	 * @return {@link String}
	 */
	public String getFirstName()
	{
		return this.firstName;
	}

	/**
	 * Setter for firstName.
	 * 
	 *
	 * @param value: {@link String}
	 */
	public void setFirstName(String value) 
	{
		this.firstName = value;
	}

	
						private String lastName
;
	
	/**
	 * Getter for lastName.
	 * 
	 *
	 * @return {@link String}
	 */
	public String getLastName()
	{
		return this.lastName;
	}

	/**
	 * Setter for lastName.
	 * 
	 *
	 * @param value: {@link String}
	 */
	public void setLastName(String value) 
	{
		this.lastName = value;
	}

	
						private Date dateOfBirth
;
	
	/**
	 * Getter for dateOfBirth.
	 * 
	 *
	 * @return {@link Date}
	 */
	public Date getDateOfBirth()
	{
		return this.dateOfBirth;
	}

	/**
	 * Setter for dateOfBirth.
	 * 
	 *
	 * @param value: {@link Date}
	 */
	public void setDateOfBirth(Date value) 
	{
		this.dateOfBirth = value;
	}

	
						private Float estimatedValue
;
	
	/**
	 * Getter for estimatedValue.
	 * 
	 *
	 * @return {@link Float}
	 */
	public Float getEstimatedValue()
	{
		return this.estimatedValue;
	}

	/**
	 * Setter for estimatedValue.
	 * 
	 *
	 * @param value: {@link Float}
	 */
	public void setEstimatedValue(Float value) 
	{
		this.estimatedValue = value;
	}

	
						private Integer weight
;
	
	/**
	 * Getter for weight.
	 * 
	 *
	 * @return {@link Integer}
	 */
	public Integer getWeight()
	{
		return this.weight;
	}

	/**
	 * Setter for weight.
	 * 
	 *
	 * @param value: {@link Integer}
	 */
	public void setWeight(Integer value) 
	{
		this.weight = value;
	}

	
						private Integer height
;
	
	/**
	 * Getter for height.
	 * 
	 *
	 * @return {@link Integer}
	 */
	public Integer getHeight()
	{
		return this.height;
	}

	/**
	 * Setter for height.
	 * 
	 *
	 * @param value: {@link Integer}
	 */
	public void setHeight(Integer value) 
	{
		this.height = value;
	}

	
						private Boolean rookie
;
	
	/**
	 * Getter for rookie.
	 * 
	 *
	 * @return {@link Boolean}
	 */
	public Boolean getRookie()
	{
		return this.rookie;
	}

	/**
	 * Setter for rookie.
	 * 
	 *
	 * @param value: {@link Boolean}
	 */
	public void setRookie(Boolean value) 
	{
		this.rookie = value;
	}

	
						private String idAsString
;
	
	/**
	 * Getter for idAsString.
	 * 
	 *
	 * @return {@link String}
	 */
	public String getIdAsString()
	{
		return this.idAsString;
	}

	/**
	 * Setter for idAsString.
	 * 
	 *
	 * @param value: {@link String}
	 */
	public void setIdAsString(String value) 
	{
		this.idAsString = value;
	}

	
	/**
	 * FieldAsString for idAsString
	 */
				/**
	 *Pattern for idAsString
	 *use ConvertField
	 */
	private final static String ID_STRING_PATTERN=DEFAULT_LONG_PATTERN;
	/**
	 *idLongConvertToString
	 */
	
	public String idLongConvertToString(java.lang.Long input) throws ApplicationException {
		return ConvertField.fieldConvertToString(input,  ID_STRING_PATTERN);
	}	
	
	/**
	 *isIdValidString
	 */
	
	public Boolean isIdValidString(String testString){
return ConvertField.isValidNumFieldString(testString, ID_STRING_PATTERN);
}	

/**
	 *idConvertStringToLong
	 */
		
	public java.lang.Long idConvertStringToLong(String testString) throws ApplicationException {
 		return ConvertField.convertStringToLongField(testString, ID_STRING_PATTERN);
	}
						private String dateOfBirthAsString
;
	
	/**
	 * Getter for dateOfBirthAsString.
	 * 
	 *
	 * @return {@link String}
	 */
	public String getDateOfBirthAsString()
	{
		return this.dateOfBirthAsString;
	}

	/**
	 * Setter for dateOfBirthAsString.
	 * 
	 *
	 * @param value: {@link String}
	 */
	public void setDateOfBirthAsString(String value) 
	{
		this.dateOfBirthAsString = value;
	}

	
	/**
	 * FieldAsString for dateOfBirthAsString
	 */
				/**
	 *Pattern for dateOfBirthAsString
	 *use ConvertField
	 */
	private final static String DATEOFBIRTH_STRING_PATTERN=DEFAULT_DATE_PATTERN;
	/**
	 *dateOfBirthDateConvertToString
	 */
	
	public String dateOfBirthDateConvertToString(java.util.Date input) throws ApplicationException {
		return ConvertField.fieldDateConvertToString(input,  DATEOFBIRTH_STRING_PATTERN);
	}	
	
	/**
	 *isDateOfBirthValidString
	 */
	
	public Boolean isDateOfBirthValidString(String testString){
return ConvertField.isValidNumFieldString(testString, DATEOFBIRTH_STRING_PATTERN);
}	

/**
	 *dateOfBirthConvertStringToDate
	 */
		
	public java.util.Date dateOfBirthConvertStringToDate(String testString) throws ApplicationException {
 		return ConvertField.convertStringToDateField(testString, DATEOFBIRTH_STRING_PATTERN);
	}
						private String estimatedValueAsString
;
	
	/**
	 * Getter for estimatedValueAsString.
	 * 
	 *
	 * @return {@link String}
	 */
	public String getEstimatedValueAsString()
	{
		return this.estimatedValueAsString;
	}

	/**
	 * Setter for estimatedValueAsString.
	 * 
	 *
	 * @param value: {@link String}
	 */
	public void setEstimatedValueAsString(String value) 
	{
		this.estimatedValueAsString = value;
	}

	
	/**
	 * FieldAsString for estimatedValueAsString
	 */
				/**
	 *Pattern for estimatedValueAsString
	 *use ConvertField
	 */
	private final static String ESTIMATEDVALUE_STRING_PATTERN=DEFAULT_FLOAT_PATTERN;
	/**
	 *estimatedValueFloatConvertToString
	 */
	
	public String estimatedValueFloatConvertToString(java.lang.Float input) throws ApplicationException {
		return ConvertField.fieldConvertToString(input,  ESTIMATEDVALUE_STRING_PATTERN);
	}	
	
	/**
	 *isEstimatedValueValidString
	 */
	
	public Boolean isEstimatedValueValidString(String testString){
return ConvertField.isValidNumFieldString(testString, ESTIMATEDVALUE_STRING_PATTERN);
}	

/**
	 *estimatedValueConvertStringToFloat
	 */
		
	public java.lang.Float estimatedValueConvertStringToFloat(String testString) throws ApplicationException {
 		return ConvertField.convertStringToFloatField(testString, ESTIMATEDVALUE_STRING_PATTERN);
	}
						private String weightAsString
;
	
	/**
	 * Getter for weightAsString.
	 * 
	 *
	 * @return {@link String}
	 */
	public String getWeightAsString()
	{
		return this.weightAsString;
	}

	/**
	 * Setter for weightAsString.
	 * 
	 *
	 * @param value: {@link String}
	 */
	public void setWeightAsString(String value) 
	{
		this.weightAsString = value;
	}

	
	/**
	 * FieldAsString for weightAsString
	 */
				/**
	 *Pattern for weightAsString
	 *use ConvertField
	 */
	private final static String WEIGHT_STRING_PATTERN=DEFAULT_INTEGER_PATTERN;
	/**
	 *weightIntegerConvertToString
	 */
	
	public String weightIntegerConvertToString(java.lang.Integer input) throws ApplicationException {
		return ConvertField.fieldConvertToString(input,  WEIGHT_STRING_PATTERN);
	}	
	
	/**
	 *isWeightValidString
	 */
	
	public Boolean isWeightValidString(String testString){
return ConvertField.isValidNumFieldString(testString, WEIGHT_STRING_PATTERN);
}	

/**
	 *weightConvertStringToInteger
	 */
		
	public java.lang.Integer weightConvertStringToInteger(String testString) throws ApplicationException {
 		return ConvertField.convertStringToIntegerField(testString, WEIGHT_STRING_PATTERN);
	}
						private String heightAsString
;
	
	/**
	 * Getter for heightAsString.
	 * 
	 *
	 * @return {@link String}
	 */
	public String getHeightAsString()
	{
		return this.heightAsString;
	}

	/**
	 * Setter for heightAsString.
	 * 
	 *
	 * @param value: {@link String}
	 */
	public void setHeightAsString(String value) 
	{
		this.heightAsString = value;
	}

	
	/**
	 * FieldAsString for heightAsString
	 */
				/**
	 *Pattern for heightAsString
	 *use ConvertField
	 */
	private final static String HEIGHT_STRING_PATTERN=DEFAULT_INTEGER_PATTERN;
	/**
	 *heightIntegerConvertToString
	 */
	
	public String heightIntegerConvertToString(java.lang.Integer input) throws ApplicationException {
		return ConvertField.fieldConvertToString(input,  HEIGHT_STRING_PATTERN);
	}	
	
	/**
	 *isHeightValidString
	 */
	
	public Boolean isHeightValidString(String testString){
return ConvertField.isValidNumFieldString(testString, HEIGHT_STRING_PATTERN);
}	

/**
	 *heightConvertStringToInteger
	 */
		
	public java.lang.Integer heightConvertStringToInteger(String testString) throws ApplicationException {
 		return ConvertField.convertStringToIntegerField(testString, HEIGHT_STRING_PATTERN);
	}
													private Position206VO positionVO
;
	
	/**
	 * Getter for positionVO.
	 * 
	 *
	 * @return {@link Position206VO}
	 */
	public Position206VO getPositionVO()
	{
		return this.positionVO;
	}

	/**
	 * Setter for positionVO.
	 * 
	 *
	 * @param value: {@link Position206VO}
	 */
	public void setPositionVO(Position206VO value) 
	{
		this.positionVO = value;
	}

	
/**
	 * Returns <code>true</code> if the argument is an Player206VO instance and all identifiers for this vo
     * equal the identifiers of the argument vo. Returns <code>false</code> otherwise.
	 * @param object  object to compare with current instance      
     * @return boolean true if equals false if not
     */
    public boolean equals( Object object )
    {
		if ( this == object )
        {
            return true;
        }
        if ( !( object instanceof Player206VO ) )
        {
            return false;
        }
        final Player206VO that = (Player206VO)object;
			if ( this.id == null || that.getId() == null || !that.getId().equals( this.id ) )
	        {
	            return false;
	        }
		return true;
	}
	/**
     * Returns a hash code based on this entity's identifiers.     * @return int
     */    
    public int hashCode()
    {
			int hashCode = 0;
			hashCode = H_FACTOR * hashCode + ( getId() == null ? 0 : getId().hashCode() );
			return hashCode;
	}
}
