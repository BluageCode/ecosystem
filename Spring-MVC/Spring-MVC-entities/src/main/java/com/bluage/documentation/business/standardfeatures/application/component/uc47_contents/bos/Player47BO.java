/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.business.standardfeatures.application.component.uc47_contents.bos;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.inject.Inject;
import javax.inject.Named;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.LazyInitializationException;

import com.bluage.documentation.business.common.bos.PlayerBO;
import com.bluage.documentation.business.standardfeatures.application.component.uc47_contents.entities.daofinder.Player47DAOFinderImpl;
import com.netfective.bluage.core.exception.ApplicationException;

/**
 * Player47BO
 *
* @see com.netfective.bluage.core.business.IBusinessObject
* * @see PlayerBO
 */
@XmlRootElement

public class Player47BO
 extends PlayerBO {
	
	/**
	 * Default SUID for Player47BO.
	 */
	private static final long serialVersionUID = 1L;
	

	@Inject
	@Named ("com.bluage.documentation.business.standardfeatures.application.component.uc47_contents.entities.daofinder.Player47Finder") 
	private Player47DAOFinderImpl finder;

												/**
	 * Entity property: position
	 *
	 */
private Position47BO position;

																		/**
	 * Entity property: playerTeams
	 *
	 */
private Set<PlayerTeam47BO> playerTeams = new HashSet<PlayerTeam47BO>();



   /**
	* public default constructor for class Player47BO
	*/
	public Player47BO(){
		//NOP Player47BO
	}


												/**
	 * Getter for position of class Player47BO.
	 *
	 * @return {@link com.bluage.documentation.business.standardfeatures.application.component.uc47_contents.bos.Position47BO}
	 * @generated
	 */ 
public  Position47BO getPosition() {
		Position47BO position0 = this.position;
		if(this.position == null) {
			if(DAO_MODE.equals(this.getObjMode( ) ) ) {
				position0 = null;
			} else {
				this.position = new Position47BO();
				this.position.setObjMode( this.getObjMode( ) );
				position0 = this.position;
			}
		} else {
			this.position.setObjMode( this.getObjMode( ) );
			if( HELPER_MODE.equals(this.getObjMode( ) ) ) {
				position0 = this.position;
			}
			if( DAO_MODE.equals(this.getObjMode()) && this.position.isEmpty ) {
				position0 = null;
			}
		}
		return position0;
	}


	/**
	 * Setter for position of class Player47BO.
	 * @param value {@link com.bluage.documentation.business.standardfeatures.application.component.uc47_contents.bos.Position47BO}
	 * @generated
	 */

public  void setPosition(final Position47BO position) {
	if(position != null) {
		position.isEmpty = false;
		position.setObjMode( this.getObjMode( ) );
	}
	this.isEmpty = false;
	this.position = position;
}

																		/**
	 * Getter for playerTeams of class Player47BO.
	 *
	 * @return {@link java.util.Set<com.bluage.documentation.business.standardfeatures.application.component.uc47_contents.bos.PlayerTeam47BO>}
	 * @generated
	 */ 
public  Set<PlayerTeam47BO> getPlayerTeams() {
	return this.playerTeams;
			}


	/**
	 * Setter for playerTeams of class Player47BO.
	 * @param value {@link java.util.Set<com.bluage.documentation.business.standardfeatures.application.component.uc47_contents.bos.PlayerTeam47BO>}
	 * @generated
	 */

public  void setPlayerTeams(final Set<PlayerTeam47BO> playerTeams) {
	this.playerTeams = playerTeams;
}

	/**
	 * Remove an entity from the com.bluage.documentation.business.standardfeatures.application.component.uc47_contents.bos.PlayerTeam47BO collection.
	 *
	 * @param entity
	 *		: {@link Set<PlayerTeam47BO>} the entity
	 * @throws ApplicationException
     */
public void removeFromPlayerTeams( PlayerTeam47BO entity ) throws ApplicationException {
	try {
		if( getPlayerTeams() != null && getPlayerTeams().contains(entity) ) {
			getPlayerTeams().remove( entity );
		entity.setPlayer( null );
		}
		} catch ( LazyInitializationException e ) {
				Player47BO bo = ( Player47BO ) finder.findByPrimaryKey( getId() );
			finder.getHibernateTemplate().initialize( bo.getPlayerTeams() );
			setPlayerTeams( bo.getPlayerTeams() );
			// evict object from session to avoid NonUniqueObjectException
			finder.getHibernateTemplate().evict( bo );
		
			if( getPlayerTeams() != null && getPlayerTeams().contains( entity ) ) {
				getPlayerTeams().remove( entity );
				entity.setPlayer( null );
			}
				}
	}
	
	/**
	 * Add entity to the com.bluage.documentation.business.standardfeatures.application.component.uc47_contents.bos.PlayerTeam47BO collection.
	 *
	 * @param entity
	 *		{@link PlayerTeam47BO} the entity
	 * @throws ApplicationException
     */
public void addToPlayerTeams( PlayerTeam47BO entity ) throws ApplicationException
	{
		try {
			if( getPlayerTeams() != null && !getPlayerTeams().contains( entity ) ) {
				getPlayerTeams().add( entity );
				entity.setPlayer( this );
			}
			} catch ( LazyInitializationException e ) {
					Player47BO bo = ( Player47BO ) finder.findByPrimaryKey( getId() );
		finder.getHibernateTemplate().initialize( bo.getPlayerTeams() );
		setPlayerTeams( bo.getPlayerTeams() );
		// evict object from session to avoid NonUniqueObjectException
		finder.getHibernateTemplate().evict( bo );
		if( getPlayerTeams() != null && !getPlayerTeams().contains( entity ) ) {
			getPlayerTeams().add( entity );
			entity.setPlayer( this );
		}
			}
	}
	
	/**
	 * Add entitys to the com.bluage.documentation.business.standardfeatures.application.component.uc47_contents.bos.PlayerTeam47BO collection
	 *
	 * @param entities			
	 *		: {@link java.util.List} A list of entities
	 * @throws ApplicationException 
     */	
public void addAllToPlayerTeams( Collection<PlayerTeam47BO> entities ) throws ApplicationException {
	for( PlayerTeam47BO entity : entities) {
		if( getPlayerTeams()!=null ) {
			addToPlayerTeams( entity );
		}
	}
}
	
	/**
	 * Remove entities form the com.bluage.documentation.business.standardfeatures.application.component.uc47_contents.bos.PlayerTeam47BO collection
	 *
	 * @param entities				
	 *		: {@link java.util.List} a list of entities
	 * @throws ApplicationException
     */	
public void removeAllFromPlayerTeams( Collection<PlayerTeam47BO> entities ) throws ApplicationException {
	for( PlayerTeam47BO entity : entities) {
		if( getPlayerTeams()!=null && getPlayerTeams().contains( entity ) )	{		
			removeFromPlayerTeams( entity );
		}
	}
}
	
	/**
	 * Remove all items form the collection com.bluage.documentation.business.standardfeatures.application.component.uc47_contents.bos.PlayerTeam47BO
	 *
	 * @throws ApplicationException
     */	
public void removeAllFromPlayerTeams() throws ApplicationException {
	Set<PlayerTeam47BO> entities = this.getPlayerTeams();
	if ( entities == null ) {
		return;
	}
	Iterator<PlayerTeam47BO> it = entities.iterator();
	while ( it.hasNext() ) {
    	PlayerTeam47BO entity = it.next(); 
		entity.setPlayer( null ); 
	}
	entities.clear();
}

/**
	 * Returns <code>true</code> if the argument is an Player47 instance and all identifiers for this entity
     * equal the identifiers of the argument entity. The <code>equals</code> method of the parent entity
     * will also need to return <code>true</code>. Returns <code>false</code> otherwise.
     *
     * @see PlayerBO#hashCode()
	 * @param object  object to compare with current instance      
     * @return boolean true if equals false if not
     */
    public boolean equals(final Object object )
    {
		if ( this == object )
        {
            return true;
        }
        if ( !( object instanceof Player47BO ) )
        {
            return false;
        }
        final Player47BO that = (Player47BO)object;
    
    	            if (        				        			this.getId() == null
        	        ) {
        	return false;
        }
		return new EqualsBuilder()
			.append(this.getId(), that.getId())
			 	.isEquals();
	}
	/**

	 * Returns a hash code based on this entity's identifiers and the hash code of the parent entity.
     *
     * @see PlayerBO#hashCode()
     * @return int
     */    
    public int hashCode() {
        return new HashCodeBuilder()
             .append(getId())
				 	.appendSuper(super.hashCode())
            .toHashCode();
    }
}

