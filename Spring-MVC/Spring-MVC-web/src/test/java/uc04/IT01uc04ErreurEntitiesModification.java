package uc04;

import org.junit.Test;
import test.common.AbstractBADocTestCase;

public class IT01uc04ErreurEntitiesModification extends AbstractBADocTestCase {

	
	@Override
	public  String getName(){
		return "UC-04";
	}

	@Test
	public void testIT01uc04ErreurEntitiesModification() throws Exception {
		// On teste l'UC04 - Getting started - Entities Modification
		selenium.open("");
		selenium.waitForPageToLoad("30000");
		selenium.click("link=Expand All");
		selenium.click("link=UC04 - Entities Modification");
		verifyTrue(selenium.isElementPresent("//h2[contains(text(),\"Getting started > UC04 > Entities Modification\")]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/thead/tr[th[contains(text(),\"Name\")] and th[contains(text(),\"Date of Birth\")] and th[contains(text(),\"Weight (kg)\")] and th[contains(text(),\"Height (cm)\")] and th[contains(text(),\"Estimated Value (M$)\")] and th[contains(text(),\"Position\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Laplace\")] and td/span[contains(text(),\"Bernard\")] and td/span[contains(text(),\"07/14/1980\")] and td/span[contains(text(),\"95\")] and td/span[contains(text(),\"191\")] and td/span[contains(text(),\"37.32\")] and td/span[contains(text(),\"PF\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Sorel\")] and td/span[contains(text(),\"Julien\")] and td/span[contains(text(),\"01/20/1975\")] and td/span[contains(text(),\"87\")] and td/span[contains(text(),\"182\")] and td/span[contains(text(),\"23.55\")] and td/span[contains(text(),\"C\")]]"));
		selenium.click("link=[details]");
		selenium.waitForPageToLoad("30000");
		// On vérifie l'affichage :
		assertTrue(selenium.isElementPresent("//label[contains(text(),'First Name (RequiredFieldValidator, LengthValidator)')]"));
		assertTrue(selenium.isElementPresent("xpath=(//*[contains(text(),'(*)')])[2]"));
		assertTrue(selenium.isElementPresent("//label[contains(text(),'Last Name (RequiredFieldValidator, LengthValidator)')]"));
		assertTrue(selenium.isElementPresent("xpath=(//label[contains(text(),'(*)')])"));
		assertTrue(selenium.isElementPresent("//label[contains(text(),'Date of Birth (MM/dd/yyyy)')]"));
		assertTrue(selenium.isElementPresent("//label[contains(text(),'Estimated value')]"));
		assertTrue(selenium.isElementPresent("//label[contains(text(),'Weight (RangeValidator)')]"));
		assertTrue(selenium.isElementPresent("//label[contains(text(),'Height (RangeValidator)')]"));
		assertTrue(selenium.isElementPresent("//label[contains(text(),'Position')]"));
		assertTrue(selenium.isElementPresent("//label[contains(text(),'Rookie')]"));
		// On vérife les positions:
		assertTrue(selenium.isElementPresent("//option[contains(text(),'Point Guard')]"));
		assertTrue(selenium.isElementPresent("//option[contains(text(),'Shooting Guard')]"));
		assertTrue(selenium.isElementPresent("//option[contains(text(),'Small Forward')]"));
		assertTrue(selenium.isElementPresent("//option[contains(text(),'Power Forward')]"));
		assertTrue(selenium.isElementPresent("//option[contains(text(),'Center')]"));
		// On entre des valeurs en deçà des valeurs minimum pour les champs "Height"(150/240) et "Weight"(60/140).
		selenium.type("//table[@class=\"rd_tableform\"]/tbody/tr[5]/td[2]/input", "59");
		selenium.type("//table[@class=\"rd_tableform\"]/tbody/tr[6]/td[2]/input", "149");
		// On entre une date de naissance erronée.
		selenium.type("//table[@class=\"rd_tableform\"]/tbody/tr[3]/td[2]/input", "14/07/1980");
		// On selectionne "rookie"
		selenium.click("//*[contains(@id,'chk_rookie')]");
		// On change la position du joueur.
		selenium.select("//*[contains(@id,'slt_position')]", "label=Shooting Guard");
		selenium.click("//*[contains(@id,'lnk_update')]");
		selenium.waitForPageToLoad("30000");
		// On vérifie que le message d'erreur existe.
		assertTrue(selenium.isElementPresent("//table[@class=\"rd_tableform\"]/tbody/tr[3]/td[3]/span"));
		// On vérifie que le message d'erreur affiché pour "dateofbirth" est le bon.
		assertEquals("Failed to convert property value of type java.lang.String to required type java.util.Date for property playerToUpdate.dateOfBirth; nested exception is java.lang.IllegalArgumentException: Invalid format.", selenium.getText("//*[contains(@id,'error_txt_dateofbirth')]|//*[contains(@id,'playerToUpdate.dateOfBirth.errors')]"));
		// On vérifie que le message d'erreur existe.
		assertTrue(selenium.isElementPresent("//table[@class=\"rd_tableform\"]/tbody/tr[6]/td[3]/span"));
		// On vérifie que le message d'erreur affiché pour "Height" est le bon.
		assertEquals("Player height should be between 150 and 240 cms.", selenium.getText("//*[contains(@id,'error_txt_height')]|//*[contains(@id,'playerToUpdate.height.errors')]"));
		// On vérifie que le message d'erreur existe.
		assertTrue(selenium.isElementPresent("//table[@class=\"rd_tableform\"]/tbody/tr[5]/td[3]/span"));
		// On vérifie que le message d'erreur affiché pour "Weight" est le bon.
		assertEquals("Player weight should be between 60 and 140 kgs.", selenium.getText("//*[contains(@id,'error_txt_weight')]|//*[contains(@id,'playerToUpdate.weight.errors')]"));
		// On entre une date valide dans "date of birth".
		selenium.type("//table[@class=\"rd_tableform\"]/tbody/tr[3]/td[2]/input", "07/14/1980");
		// On entre des valeurs supérieures aux valeurs maximales dans les champs "Height"(150/240) et "Weight"(60/140).
		selenium.type("//table[@class=\"rd_tableform\"]/tbody/tr[6]/td[2]/input", "241");
		selenium.type("//table[@class=\"rd_tableform\"]/tbody/tr[5]/td[2]/input", "141");
		selenium.click("//*[contains(@id,'lnk_update')]");
		selenium.waitForPageToLoad("30000");
		// On vérifie que le message d'erreur existe.
		assertTrue(selenium.isElementPresent("//table[@class=\"rd_tableform\"]/tbody/tr[6]/td[3]/span"));
		// On vérifie que le message d'erreur affiché pour "Height" est le bon.
		assertEquals("Player height should be between 150 and 240 cms.", selenium.getText("//*[contains(@id,'error_txt_height')]|//*[contains(@id,'playerToUpdate.height.errors')]"));
		// On vérifie que le message d'erreur existe.
		assertTrue(selenium.isElementPresent("//table[@class=\"rd_tableform\"]/tbody/tr[5]/td[3]/span"));
		// On vérifie que le message d'erreur affiché pour "Weight" est le bon.
		assertEquals("Player weight should be between 60 and 140 kgs.", selenium.getText("//*[contains(@id,'error_txt_weight')]|//*[contains(@id,'playerToUpdate.weight.errors')]"));
	}

}
