/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.service.standardfeatures.application.form.uc45_autocomplete;


import javax.inject.Named;

import com.bluage.documentation.business.standardfeatures.application.form.uc45_autocomplete.bos.Player45BO;
import com.bluage.documentation.service.AbstractService;
import com.netfective.bluage.core.exception.ApplicationException;

/**
 * Defines  application services for ServicePlayer45
  */
@Named("com.bluage.documentation.service.standardfeatures.application.form.uc45_autocomplete.ServicePlayer45")
public class ServicePlayer45  extends AbstractService{


				


	/**
	 *  Default constructor 
	 */
	public ServicePlayer45() {
		super(); // Call super for default constructor : ServicePlayer45
	}
	



			
	/**    
	 * Process operation.
	 * @return {@link Player45BO}
	 */
	public Player45BO initPlayers45( )
 throws ApplicationException {

				return privateInitPlayers45();
	}
	

	/**    
	 * Process operation.
	 * @return {@link Player45BO}
	 */
	private Player45BO privateInitPlayers45( )
 throws ApplicationException {
				// Return Type declaration.
				Player45BO player = new Player45BO();
					// Central buffer node declaration for operation initPlayers45
			
						return player;
		}

		}
