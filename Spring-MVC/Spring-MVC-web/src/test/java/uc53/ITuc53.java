package uc53;

import org.junit.Test;
import test.common.AbstractBADocTestCase;

public class ITuc53 extends AbstractBADocTestCase {
	
	@Override
	public  String getName(){
		return "UC-53";
	}	


	@Test
	public void testUC53() throws Exception {
		// On teste l'UC53 - Standard Features/Services/Process - While
		
		selenium.open("");	
		selenium.click("link=Expand All");
		selenium.waitForPageToLoad("30000");
		selenium.click("link=UC53 - While");
		verifyTrue(selenium.isElementPresent("//h2[contains(text(),\"Standard Features > Services > Process > UC53 > While\")]"));
		assertTrue(selenium.isElementPresent("//table//tbody//tr[th[contains(text(),\"Last Name\")] and th[contains(text(),\"First Name\")] and th[contains(text(),\"Rookie\")] and th[contains(text(),\"Number of matches played\")]]"));
		selenium.click("//*[contains(@id,'lnk_showPlayer')]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//table//tbody//tr[td/span[contains(text(),\"Smith\")] and td/span[contains(text(),\"John\")] and td//input[@checked] and td/span[contains(text(),\"23\")]]"));
		selenium.click("//*[contains(@id,'lnk_trainPlayer')]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//table//tbody/tr[td/span[contains(text(),\"Smith\")] and td/span[contains(text(),\"John\")] and td//input[not(@checked)] and td/span[contains(text(),\"50\")]]"));
	}


}
