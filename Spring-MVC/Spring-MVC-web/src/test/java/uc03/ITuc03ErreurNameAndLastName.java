package uc03;

import org.junit.Test;
import test.common.AbstractBADocTestCase;

public class ITuc03ErreurNameAndLastName extends AbstractBADocTestCase {
	
	@Override
	public  String getName(){
		return "UC-03";
	}

	@Test
	public void testITuc03ErreurNameAndLastName() throws Exception {
		// On teste l'UC03 - Getting started - Form Validation
		// On teste le nombre maximum de caractères autorisés pour les champs "First Name" & "Last Name".
		selenium.open("");
		selenium.waitForPageToLoad("30000");
		selenium.click("link=Expand All");
		selenium.click("link=UC03 - Form Validation");
		verifyTrue(selenium.isElementPresent("//h2[contains(text(),\"Getting started > UC03 > Form Validation\")]"));
		// On rempli les champs "First Name" et "Last Name" avec plus de 30 caractères.
		selenium.type("//*[contains(@id,'txt_firstname')]", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
		selenium.type("//*[contains(@id,'txt_lastname')]", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
		// On entre des valeurs autorisées dans les champs "Height" (150/240) et "Weight" (60/140)
		selenium.type("//*[contains(@id,'txt_weight')]", "100");
		selenium.type("//*[contains(@id,'txt_height')]", "181");
		// On clique sur "Create Player"
		selenium.click("//*[contains(@id,'lnk_create')]");
		selenium.waitForPageToLoad("30000");
		// On vérifie que l'ID du message d'erreur est présente.
		assertTrue(selenium.isElementPresent("//*[contains(@id,'playerToCreate.firstName.errors')]"));
		// On vérifie que le message d'erreur est présent.
		assertEquals("First name should be less than 30 characters long.", selenium.getText("//*[contains(@id,'playerToCreate.firstName.errors')]"));
		// On vérifie que l'ID du message d'erreur est présente.
		assertTrue(selenium.isElementPresent("//*[contains(@id,'playerToCreate.lastName.errors')]"));
		// On vérifie que le message d'erreur est présent.
		assertEquals("Last name should be less than 30 characters long.", selenium.getText("//*[contains(@id,'playerToCreate.lastName.errors')]"));
	}

	
}
