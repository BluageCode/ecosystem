--<ScriptOptions statementTerminator=";"/>

ALTER TABLE player52 DROP CONSTRAINT player52_pkey;

DROP TABLE player52;

CREATE TABLE player52 (
		id INT8 NOT NULL,
		version INT4 NOT NULL,
		first_name VARCHAR(50) NOT NULL,
		last_name VARCHAR(50) NOT NULL,
		rookie BOOL,
		value FLOAT4
	);
	
INSERT INTO player52 VALUES (100, 0, 'Manu', 'Vega', false, 18.5);
INSERT INTO player52 VALUES (101, 0, 'Hernest', 'Glod', true, 39.900002);



ALTER TABLE player52 ADD CONSTRAINT player52_pkey PRIMARY KEY (id);

