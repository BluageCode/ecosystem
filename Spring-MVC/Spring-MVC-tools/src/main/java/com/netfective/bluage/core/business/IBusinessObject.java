/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.netfective.bluage.core.business;
import java.io.Serializable;

/**
 * Cette classe est la racine de tous les Objets Metiers (Business Object).
 * 
 * <DL>
 * <DT><B>Nom du Projet: </B>
 * <DD>BluAge</DD>
 * <DT><B>Service: </B>
 * <DD>NETFECTIVE</DD>
 * </DL>
 * 
 */
public interface IBusinessObject extends Serializable {

	String DAO_MODE = "dao";

	String HELPER_MODE = "helper";
	
	/**
	 * setter for objMode
	 * @param mode String
	 */
	void setObjMode(String mode);

	/**
	 * getter for objMode
	 * @return mode Stirng
	 */
	String getObjMode();


}
