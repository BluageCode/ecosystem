/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.netfective.bluage.springmvc.ajax.parser;

/**
 * Class ResponseParserFactory
 * @author NETFECTIVE TECHNOLOGY
 */
public final class ResponseParserFactory {

    /**
    * Unique instance of this class
    */
    private static ResponseParserFactory ourInstance = new ResponseParserFactory();

	/**
	* Default builder
	*/
    private ResponseParserFactory() {
    }

	/**
	* Getter for ourInstance
	* @return ResponseParserFactory
	*/
    public static ResponseParserFactory getInstance() {
        return ourInstance;
    }

	/**
	* Getter for an instance of ResponseParserHTML
	* @return ResponseParserHTML
	*/
    public ResponseParser getParser(){
        return ResponseParserHTML.getInstance();
    }
}

