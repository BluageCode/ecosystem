/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.qauc.uc1001_tables ;

// Import declaration.
// Java imports.
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.bluage.documentation.business.qauc.uc1001_tables.bos.Team1001BO;
import com.bluage.documentation.business.qauc.uc1001_tables.entities.daofinder.Team1001DAOFinderImpl;
import com.bluage.documentation.service.qauc.uc1001_tables.ServiceTeam1001;
import com.netfective.bluage.core.exception.ApplicationException;
import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;
import com.netfective.bluage.core.web.IForm;

/**
 * Class : UC1001_TablesController
 */
@Controller
@Scope("prototype")
@SessionAttributes("uC1001_TablesForm")
@RequestMapping(value= "/presentation/qauc/uc1001_tables")
public class UC1001_TablesController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC1001_TablesController.class);
	
	//Current form name
	private static final String U_C1001__TABLES_FORM = "uC1001_TablesForm";

	//CONSTANT: allTeams table or repeater.
	private static final String ALL_TEAMS = "allTeams";
				//CONSTANT: teamDetails
	private static final String TEAM_DETAILS = "teamDetails";
	//CONSTANT: selectedTeam
	private static final String SELECTED_TEAM = "selectedTeam";
	
	/**
	 * Property:customDateEditorsUC1001_TablesController
	 */
	final private Map<String,CustomDateEditor> customDateEditorsUC1001_TablesController = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:uC1001_TablesValidator
	 */
	final private UC1001_TablesValidator uC1001_TablesValidator = new UC1001_TablesValidator();
	
	/**
	 * Service declaration : serviceTeam1001.
	 */
	@Autowired
	private ServiceTeam1001 serviceTeam1001;
	
	/**
	 * Generic Finder : team1001DAOFinderImpl.
	 */
	@Autowired
	private Team1001DAOFinderImpl team1001DAOFinderImpl;
	
	
	// Initialise all the instances for UC1001_TablesController
		// Initialize the instance allTeams for the state lnk_update
		private List allTeams; // Initialize the instance allTeams for UC1001_TablesController
						// Declare the instance teamDetails
		private Team1001BO teamDetails;
		// Declare the instance selectedTeam
		private Team1001BO selectedTeam;
			/**
	 * Operation : lnk_view
 	 * @param model : 
 	 * @param uC1001_Tables : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC1001_Tables/lnk_view.html",method = RequestMethod.POST)
	public String lnk_view(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC1001_TablesForm") UC1001_TablesForm  uC1001_TablesForm, final BindingResult bindingResult ,final @RequestParam(ID_REQUEST_PARAM) Integer index){
		
		
		
		LOGGER.info("Begin method : lnk_view"); 
		infoLogger(uC1001_TablesForm); 
		uC1001_TablesForm.setAlwaysCallPreControllers(false); 
		// initialization for lnk_view
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state lnk_view if the validation fail, you will be forward to the screen : "/presentation/qauc/uc1001_tables/UC1001_Tables"
		if(errorsMessages(request,response,uC1001_TablesForm,bindingResult,"selectedTeam", uC1001_TablesValidator, customDateEditorsUC1001_TablesController)){ 
			return "/presentation/qauc/uc1001_tables/UC1001_Tables"; 
		}

										 callFindteamByID(request,uC1001_TablesForm , index);
											// Init the destination form com.bluage.documentation.web.presentation.qauc.uc1001_tables.UC1001_ViewTeamForm or not from lnk_view
			final UC1001_ViewTeamForm uC1001_ViewTeamForm =  new UC1001_ViewTeamForm();
			// Populate the destination form with all the instances returned from lnk_view.
			final IForm uC1001_ViewTeamForm2 = populateDestinationForm(request.getSession(), uC1001_ViewTeamForm); 
					
			// Add the form to the model.
			model.addAttribute("uC1001_ViewTeamForm", uC1001_ViewTeamForm2); 
			
			request.getSession().setAttribute("uC1001_ViewTeamForm", uC1001_ViewTeamForm2);
			
			// "OK" CASE => destination screen path from lnk_view
			LOGGER.info("Go to the screen 'UC1001_ViewTeam'.");
			// Redirect (PRG) from lnk_view
			destinationPath =  "redirect:/presentation/qauc/uc1001_tables/UC1001_ViewTeam.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_view 
	}
	
	
	/**
	* This method initialise the form : UC1001_TablesForm 
	* @return UC1001_TablesForm
	*/
	@ModelAttribute("UC1001_TablesFormInit")
	public void initUC1001_TablesForm(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, U_C1001__TABLES_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method UC1001_TablesForm."); //for lnk_view 
		}
		UC1001_TablesForm uC1001_TablesForm;
	
		if(request.getSession().getAttribute(U_C1001__TABLES_FORM) != null){
			uC1001_TablesForm = (UC1001_TablesForm)request.getSession().getAttribute(U_C1001__TABLES_FORM);
		} else {
			uC1001_TablesForm = new UC1001_TablesForm();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form UC1001_TablesForm.");
		}
		uC1001_TablesForm = (UC1001_TablesForm)populateDestinationForm(request.getSession(), uC1001_TablesForm);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  UC1001_TablesForm.");
		}
		model.addAttribute(U_C1001__TABLES_FORM, uC1001_TablesForm);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param UC1001_TablesForm : The sceen form.
	 */
	@RequestMapping(value = "/UC1001_Tables.html" ,method = RequestMethod.GET)
	public void prepareUC1001_Tables(final HttpServletRequest request,final  Model model,final  @ModelAttribute("UC1001_TablesFormInit") UC1001_TablesForm uC1001_TablesForm){
		
		UC1001_TablesForm currentUC1001_TablesForm = uC1001_TablesForm;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(U_C1001__TABLES_FORM) == null){
			if(currentUC1001_TablesForm!=null){
				request.getSession().setAttribute(U_C1001__TABLES_FORM, currentUC1001_TablesForm);
			}else {
				currentUC1001_TablesForm = new UC1001_TablesForm();
				request.getSession().setAttribute(U_C1001__TABLES_FORM, currentUC1001_TablesForm);	
			}
		} else {
			currentUC1001_TablesForm = (UC1001_TablesForm) request.getSession().getAttribute(U_C1001__TABLES_FORM);
		}

		try {
			List allTeams = team1001DAOFinderImpl.findAll();
			put(request.getSession(), ALL_TEAMS, allTeams);
		} catch (ApplicationException e) {
			errorLogger("An error while calling the generic finder for the instance : allTeams.",e);
		}
				currentUC1001_TablesForm = (UC1001_TablesForm)populateDestinationForm(request.getSession(), currentUC1001_TablesForm);
		// Call all the Precontroller.
	currentUC1001_TablesForm.setAlwaysCallPreControllers(true);
		model.addAttribute(U_C1001__TABLES_FORM, currentUC1001_TablesForm);
		request.getSession().setAttribute(U_C1001__TABLES_FORM, currentUC1001_TablesForm);
		
	}
	
										/**
	 * method callFindteamByID
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 * @param index Integer the current index 
	 */
	private void callFindteamByID(HttpServletRequest request,IForm form,Integer index) {
		teamDetails = (Team1001BO)get(request.getSession(),form, "teamDetails");  
										 
					if(index!=null){  
			 selectedTeam = (Team1001BO)((List)get(request.getSession(),form, "allTeams")).get(index);  
		} else {
			 selectedTeam= null; 
		}
				 
					try {
				// executing FindteamByID in lnk_view
				teamDetails = 	serviceTeam1001.teamFindByID(
			selectedTeam
			);  
 
				put(request.getSession(), TEAM_DETAILS,teamDetails);
								// processing variables FindteamByID in lnk_view
				put(request.getSession(), SELECTED_TEAM,selectedTeam); 
			
			} catch (ApplicationException e) { 
				// error handling for operation teamFindByID called FindteamByID
				errorLogger("An error occured during the execution of the operation : teamFindByID",e); 
		
			}
	}
		
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("UC1001_TablesController [ ");
			strBToS.append(ALL_TEAMS);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(allTeams);
			strBToS.append(END_TO_STRING_DELIMITER);
						strBToS.append(TEAM_DETAILS);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(teamDetails);
			strBToS.append(END_TO_STRING_DELIMITER);
			strBToS.append(SELECTED_TEAM);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(selectedTeam);
			strBToS.append(END_TO_STRING_DELIMITER);
		// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(uC1001_TablesValidator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsUC1001_TablesController!=null); 
		return strBToS.toString();
	}
}
