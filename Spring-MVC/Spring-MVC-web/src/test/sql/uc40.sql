--<ScriptOptions statementTerminator=";"/>

ALTER TABLE state40 DROP CONSTRAINT state40_pkey;

DROP TABLE state40;

CREATE TABLE state40 (
		id INT4 NOT NULL,
		version INT4 NOT NULL,
		code VARCHAR(1024) NOT NULL,
		name VARCHAR(1024) NOT NULL
	);
	
INSERT INTO state40 (id, version, code, name) VALUES (1, 0, 'AK', 'Alaska');
INSERT INTO state40 (id, version, code, name) VALUES (2, 0, 'AR', 'Arkansas');
INSERT INTO state40 (id, version, code, name) VALUES (3, 0, 'AZ', 'Arizona');
INSERT INTO state40 (id, version, code, name) VALUES (4, 0, 'CA', 'California');
INSERT INTO state40 (id, version, code, name) VALUES (5, 0, 'CO', 'Colorado');
INSERT INTO state40 (id, version, code, name) VALUES (6, 0, 'CT', 'Connecticut');
INSERT INTO state40 (id, version, code, name) VALUES (7, 0, 'DE', 'Delaware');


ALTER TABLE state40 ADD CONSTRAINT state40_pkey PRIMARY KEY (id);

