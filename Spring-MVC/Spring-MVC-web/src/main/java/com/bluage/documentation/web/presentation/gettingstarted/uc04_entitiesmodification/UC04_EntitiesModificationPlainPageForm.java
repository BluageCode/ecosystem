/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.gettingstarted.uc04_entitiesmodification;


import java.io.Serializable;
import java.util.List;

import com.bluage.documentation.business.gettingstarted.uc04_entitiesmodification.bos.Player04BO;
import com.netfective.bluage.core.web.AbstractForm;
import com.netfective.bluage.core.web.IForm;

/**
* Class : UC04_EntitiesModificationPlainPageForm
*/
public class UC04_EntitiesModificationPlainPageForm extends AbstractForm implements Serializable, IForm {
	
	/** Constant : serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	//CONSTANT : allPositions
	private static final String ALL_POSITIONS = "allPositions";
	//CONSTANT : playerToUpdate
	private static final String PLAYER_TO_UPDATE = "playerToUpdate";
	/**
	 * 	Property: allPositions 
	 */
	private List allPositions;
	/**
	 * 	Property: playerToUpdate 
	 */
	private Player04BO playerToUpdate;
/**
	 * Default constructor : UC04_EntitiesModificationPlainPageForm
	 */
	public UC04_EntitiesModificationPlainPageForm() {
		super();
		// Initialize : allPositions
		this.allPositions = new java.util.ArrayList();
		// Initialize : playerToUpdate
		this.playerToUpdate = new Player04BO();
			this.setAlwaysCallPreControllers(true);
	}
	
	/**
	 * 	Getter : allPositions 
	 * 	@return : Return the allPositions instance.
	 */
	public List getAllPositions(){
		return allPositions; // For UC04_EntitiesModificationPlainPageForm
	}
	
	/**
	 * 	Setter : allPositions 
	 *  @param allPositionsinstance : The instance to set.
	 */
	public void setAllPositions(final List allPositionsinstance){
		this.allPositions = allPositionsinstance;// For UC04_EntitiesModificationPlainPageForm
	}
	/**
	 * 	Getter : playerToUpdate 
	 * 	@return : Return the playerToUpdate instance.
	 */
	public Player04BO getPlayerToUpdate(){
		return playerToUpdate; // For UC04_EntitiesModificationPlainPageForm
	}
	
	/**
	 * 	Setter : playerToUpdate 
	 *  @param playerToUpdateinstance : The instance to set.
	 */
	public void setPlayerToUpdate(final Player04BO playerToUpdateinstance){
		this.playerToUpdate = playerToUpdateinstance;// For UC04_EntitiesModificationPlainPageForm
	}
/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UC04_EntitiesModificationPlainPageForm [ "+
ALL_POSITIONS +" = " + allPositions +PLAYER_TO_UPDATE +" = " + playerToUpdate + "]";
	}
	
	/**
	 * Method setInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public void setInstanceByInstanceName(final String instanceName, final Object instance) {
		// Set the instance AllPositions.
		if(ALL_POSITIONS.equals(instanceName)){// For UC04_EntitiesModificationPlainPageForm
			this.setAllPositions((List)instance); // For UC04_EntitiesModificationPlainPageForm
		}
				// Set the instance PlayerToUpdate.
		if(PLAYER_TO_UPDATE.equals(instanceName)){// For UC04_EntitiesModificationPlainPageForm
			this.setPlayerToUpdate((Player04BO)instance); // For UC04_EntitiesModificationPlainPageForm
		}
			}
	
	/**
	 * Method getInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public Object getInstanceByInstanceName(final String instanceName){
		// Temporary object for tmpUC04_EntitiesModificationPlainPageForm.
		Object tmpUC04_EntitiesModificationPlainPageForm = null;
		
			
		// Get the instance AllPositions for UC04_EntitiesModificationPlainPageForm.
		if(ALL_POSITIONS.equals(instanceName)){ // For UC04_EntitiesModificationPlainPageForm
			tmpUC04_EntitiesModificationPlainPageForm = this.getAllPositions(); // For UC04_EntitiesModificationPlainPageForm
		}
			
		// Get the instance PlayerToUpdate for UC04_EntitiesModificationPlainPageForm.
		if(PLAYER_TO_UPDATE.equals(instanceName)){ // For UC04_EntitiesModificationPlainPageForm
			tmpUC04_EntitiesModificationPlainPageForm = this.getPlayerToUpdate(); // For UC04_EntitiesModificationPlainPageForm
		}
		return tmpUC04_EntitiesModificationPlainPageForm;// For UC04_EntitiesModificationPlainPageForm
	}
	
	}
