package uc209;

import org.junit.Test;
import test.common.AbstractBADocTestCase;

public class ITuc209 extends AbstractBADocTestCase {
	

	@Override
	public  String getName(){
		return "UC-209";
	}
	
	@Test
	public void testUC209() throws Exception {
		// On teste l'UC 209 - External Tools - JavaMelody
		
		selenium.open("");	
		
		selenium.click("link=Expand All");
		selenium.waitForPageToLoad("300000");
		selenium.click("link=UC209 - JavaMelody");
		
		verifyTrue(selenium.isElementPresent("//h2[contains(text(),\"Advanced Features > External Tools > UC209 > Monitoring with JavaMelody\")]"));
		assertTrue(selenium.isElementPresent("//p//a[contains(text(),\"http://javamelody.googlecode.com/\")]"));
		assertTrue(selenium.isElementPresent("//p//img[contains(@alt,'Screen JavaMelody')]"));
	}


}
