/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.service.qauc.uc1002_validators;


import javax.inject.Inject;
import javax.inject.Named;

import com.bluage.documentation.business.qauc.uc1002_validators.bos.Player1002BO;
import com.bluage.documentation.business.qauc.uc1002_validators.entities.dao.Player1002DAOImpl;
import com.bluage.documentation.service.AbstractService;
import com.netfective.bluage.core.exception.ApplicationException;

/**
 * Defines  application services for ServicePlayer1002
  */
@Named("com.bluage.documentation.service.qauc.uc1002_validators.ServicePlayer1002")
public class ServicePlayer1002  extends AbstractService{


									@Inject
		@Named ("com.bluage.documentation.business.qauc.uc1002_validators.entities.dao.Player1002DAOImpl")
		private Player1002DAOImpl player1002BODao;
	


	/**
	 *  Default constructor 
	 */
	public ServicePlayer1002() {
		super(); // Call super for default constructor : ServicePlayer1002
	}
	


			
				
	/**    
	 * Operation Crud
	 * @param bo {@link Player1002BO}
		 * @return {@link boolean}
	 */
	public boolean player1002Create(Player1002BO bo )
 throws ApplicationException {

					return player1002BODao.create(bo);
	}

		
			
	/**    
	 * Process operation.
	 * @return {@link void}
	 */
	public void back( )
 throws ApplicationException {

			privateBack();
	}
	

	/**    
	 * Process operation.
	 * @return {@link void}
	 */
	private void privateBack( )
 throws ApplicationException {
// Central buffer node declaration for operation back
			
		}

		
			
	/**    
	 * Process operation.
	 * @param playerToCreate {@link Player1002BO}
		 * @return {@link void}
	 */
	public void createPlayer1002(Player1002BO playerToCreate )
 throws ApplicationException {

			privateCreatePlayer1002(playerToCreate);
	}
	

	/**    
	 * Process operation.
	 * @param playerToCreate {@link Player1002BO}
		 * @return {@link void}
	 */
	private void privateCreatePlayer1002(Player1002BO playerToCreate )
 throws ApplicationException {
		// Central buffer node declaration for operation createPlayer1002
			// Calling an operation : 
player1002Create(playerToCreate);

		}

		}
