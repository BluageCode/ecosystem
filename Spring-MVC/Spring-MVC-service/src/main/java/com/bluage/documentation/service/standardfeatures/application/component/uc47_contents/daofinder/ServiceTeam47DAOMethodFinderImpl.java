/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.service.standardfeatures.application.component.uc47_contents.daofinder;


import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.Logger;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateTemplate;

import com.netfective.bluage.core.exception.ApplicationException;


/**
 * This class represents query invocation methods implementation
 * for ServiceTeam47 service.
 *
 * @see com.bluage.documentation.service.standardfeatures.application.component.uc47_contents.ServiceTeam47
 */
 
@Named ("com.bluage.documentation.service.standardfeatures.application.component.uc47_contents.daofinder.ServiceTeam47DAOMethodFinderImpl")
public class ServiceTeam47DAOMethodFinderImpl {

	// LOGGER for ServiceTeam47DAOMethodFinderImpl
	private static final Logger LOGGER = Logger.getLogger(ServiceTeam47DAOMethodFinderImpl.class);
	
	/**
	* Hibernate Template (injected)
	*/
	@Inject
	private HibernateTemplate hibernateTemplate;

	
	/**
	 * setter for HibernateTemplate
	 * @param value : {@link HibernateTemplate}
	 */
	public void setHibernateTemplate( HibernateTemplate value )
	{
		hibernateTemplate = value;
	}
	
	
	/**    
	 * HQL operation.
	 * @param id {@link Long}
		 * @return {@link List}
	 */
	public List returnTeam47(Long id )
 throws ApplicationException {
Session session = hibernateTemplate.getSessionFactory().getCurrentSession();
			return 
				privateReturnTeam47(id, session);
	}

	
	/**    
	 * private delegation for HQL operation.
	 * @param id {@link Long}
		 * @return {@link List}
	 */
	private List privateReturnTeam47(Long id , Session session)
 throws ApplicationException {
			Query query = session.createQuery(" select t from Team47BO t " +
" inner join t.playerTeam pt " +
" inner join pt.player p " +
" where p.id = :id ");
query.setParameter( "id", id, Hibernate.LONG);
														List returnValue = null;
		try {
			returnValue = query.list();
							} catch(HibernateException ex) {
			LOGGER.error("An error occured while excecuting the HQL operation : com.bluage.documentation.service.standardfeatures.application.component.uc47_contents.returnTeam47");
			throw new ApplicationException(ex);
		}
		return returnValue;
	}
	
	/**    
	 * HQL operation.
	 * @param id {@link Long}
		 * @return {@link List}
	 */
	public List searchTeamsNotPlayed(Long id )
 throws ApplicationException {
Session session = hibernateTemplate.getSessionFactory().getCurrentSession();
			return 
				privateSearchTeamsNotPlayed(id, session);
	}

	
	/**    
	 * private delegation for HQL operation.
	 * @param id {@link Long}
		 * @return {@link List}
	 */
	private List privateSearchTeamsNotPlayed(Long id , Session session)
 throws ApplicationException {
			Query query = session.createQuery(" select ta from Team47BO ta where ta.id not IN (select t from Team47BO t inner join t.playerTeam pt inner join pt.player p where p.id = :id) " +
" ");
query.setParameter( "id", id, Hibernate.LONG);
														List returnValue = null;
		try {
			returnValue = query.list();
							} catch(HibernateException ex) {
			LOGGER.error("An error occured while excecuting the HQL operation : com.bluage.documentation.service.standardfeatures.application.component.uc47_contents.searchTeamsNotPlayed");
			throw new ApplicationException(ex);
		}
		return returnValue;
	}

	




}
