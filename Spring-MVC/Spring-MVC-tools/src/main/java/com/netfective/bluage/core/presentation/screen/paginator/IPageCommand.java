/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.netfective.bluage.core.presentation.screen.paginator;

import java.util.List;

import com.netfective.bluage.core.exception.ApplicationException;

/**
 * Interface for page command
 */
public interface IPageCommand {
	/** REWIND */
	String REWIND = "rewind";

	/** FAST_FORWARD */
	String FAST_FORWARD = "fastForward";

	/** PREVIOUS */
	String PREVIOUS = "previous";

	/** NEXT */
	String NEXT = "next";

	/** FORMAT_0_OF_1 */
	String FORMAT_0_OF_1 = "{0} of {1}";

	/** DEFAULT_PAGE_SIZE */
	int DEFAULT_PAGE_SIZE = 10;

	/**
	 * returns the results
	 * 
	 * @return List
	 */
	List returnResults() throws ApplicationException;

	/**
	 * sets the Rewind & fast forward step
	 * 
	 * @param rewindFastForwardBy
	 */
	void setRewindFastForwardBy(int rewindFastForwardBy);

	/**
	 * sets the number of items per page
	 * 
	 * @param itemsPerPage
	 */
	void setItemsPerPage(int itemsPerPage);

	/**
	 * sets the current page
	 * 
	 * @param currentPage
	 */
	void setCurrentPage(int currentPage);

	/**
	 * rewind paginator
	 * 
	 * @param aRewindFastForwardBy
	 */
	void rewind(int aRewindFastForwardBy);

	/**
	 * forward paginator
	 * 
	 * @param aRewindFastForwardBy
	 */
	void forward(int aRewindFastForwardBy);

	/**
	 * sets the number of pages
	 * 
	 * @param numberOfPages
	 */
	void setNumberOfPages(int numberOfPages);

	/**
	 * sets the number of count
	 * 
	 * @param numberOfCount
	 */
	void setNumberOfCount(int numberOfCount);

	/**
	 * renders paginator
	 * 
	 * @param pageNumber
	 */
	void renderPaginator(int pageNumber);

	/**
	 * sets max page
	 * 
	 * @param maxPage
	 */
	void setMaxPage(int maxPage);

	/**
	 * returns the rewindFastForwardBy
	 * 
	 * @return int
	 */
	int getRewindFastForwardBy();

	/**
	 * returns the numberOfCount
	 * 
	 * @return int
	 */
	int getNumberOfCount();

	/**
	 * returns the itemsPerPage
	 * 
	 * @return int
	 */
	int getItemsPerPage();

	/**
	 * returns the sortLinkNatural
	 * 
	 * @return int
	 */
	int getSortLinkNatural();

	/**
	 * returns the currentPage
	 * 
	 * @return int
	 */
	int getCurrentPage();

	/**
	 * returns the numberOfPages
	 * 
	 * @return int
	 */
	int getNumberOfPages();

	/**
	 * returns the maxPage
	 * 
	 * @return int
	 */
	int getMaxPage();

	/**
	 * returns the recordStatus
	 * 
	 * @return String
	 */
	String getRecordStatus();

	/**
	 * rewind
	 * 
	 * @return String
	 */
	String rewind();

	/**
	 * next
	 * 
	 * @return String
	 */
	String next();

	/**
	 * previous
	 * 
	 * @return String
	 */
	String previous();

	/**
	 * fastForward
	 * 
	 * @return String
	 */
	String fastForward();
	
	/**
	 * Method getNumberOfItems.
	 * @return int
	 */
	int getNumberOfItems();
	
	/**
	 * Method setNumberOfItems.
	 * @param numberOfItems int
	 */
	void setNumberOfItems(int numberOfItems);
	
}
