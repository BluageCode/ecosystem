package uc45;

import org.junit.Test;
import test.common.AbstractBADocTestCase;

public class ITuc45 extends AbstractBADocTestCase {
	
	@Override
	public  String getName(){
		return "UC-45";
	}
	
	@Test
	public void testITuc45() throws Exception {
		// On teste l'UC45 - Standard Features/Application/Form - Auto compl�te
		selenium.open("");
		selenium.click("link=Expand All");
		selenium.waitForPageToLoad("30000");
		selenium.click("link=UC45 - Auto Complete");
		verifyTrue(selenium.isElementPresent("//h2[contains(text(),\"Standard Features > Application > Form > UC45 > Auto Complete\")]"));
		// On clique sur [Display Players]
		selenium.click("//a[contains(@id,'lnk_autocomplete')]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//table/tbody/tr/td/label"));
		assertEquals("First Name", selenium.getText("//table/tbody/tr/td/label"));
		// On teste les donn�es pour chaque lettre de l'alphabet.
//		selenium.type("//input[contains(@id,'txt_firstName')]", "A");
		selenium.typeKeys("//input[contains(@id,'txt_firstName')]", "A");
		selenium.focus("//input[contains(@id,'txt_firstName')]");
		Thread.sleep(1000);
		// On v�rifie les r�sultats :
		verifyEquals("8", selenium.getXpathCount("//table[contains(@id,'suggest')]/tbody//tr//td[2] | //div[contains(@id,'txt_firstName_div')]//li[@style='']"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'suggest')]/tbody//tr//td[contains(text(),\"Abel\")] | //div[contains(@id,'txt_firstName_div')]//li[contains(text(),\"Abel\")]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'suggest')]/tbody//tr//td[contains(text(),\"Allan\")] | //div[contains(@id,'txt_firstName_div')]//li[contains(text(),\"Allan\")]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'suggest')]/tbody//tr//td[contains(text(),\"Allen\")] | //div[contains(@id,'txt_firstName_div')]//li[contains(text(),\"Allen\")]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'suggest')]/tbody//tr//td[contains(text(),\"Arthur\")] | //div[contains(@id,'txt_firstName_div')]//li[contains(text(),\"Arthur\")]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'suggest')]/tbody//tr//td[contains(text(),\"Abraham\")] | //div[contains(@id,'txt_firstName_div')]//li[contains(text(),\"Abraham\")]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'suggest')]/tbody//tr//td[contains(text(),\"Alfonso\")] | //div[contains(@id,'txt_firstName_div')]//li[contains(text(),\"Alfonso\")]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'suggest')]/tbody//tr//td[contains(text(),\"Antonio\")] | //div[contains(@id,'txt_firstName_div')]//li[contains(text(),\"Antonio\")]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'suggest')]/tbody//tr//td[contains(text(),\"Archie\")] | //div[contains(@id,'txt_firstName_div')]//li[contains(text(),\"Archie\")]"));
		// la lettre B:
		
    	selenium.type("//input[contains(@id,'txt_firstName')]", "");
		selenium.typeKeys("//input[contains(@id,'txt_firstName')]", "B");
		selenium.focus("//input[contains(@id,'txt_firstName')]");

		Thread.sleep(1000);
		// On v�rifie les r�sultats :
		verifyEquals("2", selenium.getXpathCount("//table[contains(@id,'suggest')]/tbody//tr//td[2] | //div[contains(@id,'txt_firstName_div')]//li[@style='']"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'suggest')]/tbody//tr//td[contains(text(),\"Brett\")] | //div[contains(@id,'txt_firstName_div')]//li[contains(text(),\"Brett\")]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'suggest')]/tbody//tr//td[contains(text(),\"Bob\")] | //div[contains(@id,'txt_firstName_div')]//li[contains(text(),\"Bob\")]"));
		// la lettre C :
		selenium.type("//input[contains(@id,'txt_firstName')]", "");
		selenium.typeKeys("//input[contains(@id,'txt_firstName')]", "C");
		selenium.focus("//input[contains(@id,'txt_firstName')]");
		Thread.sleep(1000);
		// On v�rifie les r�sultats :
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'suggest')]/tbody//tr//td[2] | //div[contains(@id,'txt_firstName_div')]//li[@style='']"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'suggest')]/tbody//tr//td[contains(text(),\"Christian\")] | //div[contains(@id,'txt_firstName_div')]//li[contains(text(),\"Christian\")]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'suggest')]/tbody//tr//td[contains(text(),\"Cecil\")] | //div[contains(@id,'txt_firstName_div')]//li[contains(text(),\"Cecil\")]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'suggest')]/tbody//tr//td[contains(text(),\"Cecil\")] | //div[contains(@id,'txt_firstName_div')]//li[contains(text(),\"Cecil\")]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'suggest')]/tbody//tr//td[contains(text(),\"Clarence\")] | //div[contains(@id,'txt_firstName_div')]//li[contains(text(),\"Clarence\")]"));
		// La lettre D :
		selenium.type("//input[contains(@id,'txt_firstName')]", "");
		selenium.typeKeys("//input[contains(@id,'txt_firstName')]", "D");
		selenium.focus("//input[contains(@id,'txt_firstName')]");
		Thread.sleep(1000);
		// On v�rifie les r�sultats :
		verifyEquals("8", selenium.getXpathCount("//table[contains(@id,'suggest')]/tbody//tr//td[2] | //div[contains(@id,'txt_firstName_div')]//li[@style='']"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'suggest')]/tbody//tr//td[contains(text(),\"Darryl\")] | //div[contains(@id,'txt_firstName_div')]//li[contains(text(),\"Darryl\")]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'suggest')]/tbody//tr//td[contains(text(),\"Dwayne\")] | //div[contains(@id,'txt_firstName_div')]//li[contains(text(),\"Dwayne\")]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'suggest')]/tbody//tr//td[contains(text(),\"Darryl\")] | //div[contains(@id,'txt_firstName_div')]//li[contains(text(),\"Darryl\")]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'suggest')]/tbody//tr//td[contains(text(),\"Dana\")] | //div[contains(@id,'txt_firstName_div')]//li[contains(text(),\"Dana\")]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'suggest')]/tbody//tr//td[contains(text(),\"Darnell\")] | //div[contains(@id,'txt_firstName_div')]//li[contains(text(),\"Darnell\")]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'suggest')]/tbody//tr//td[contains(text(),\"Dominic\")] | //div[contains(@id,'txt_firstName_div')]//li[contains(text(),\"Dominic\")]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'suggest')]/tbody//tr//td[contains(text(),\"Don\")] | //div[contains(@id,'txt_firstName_div')]//li[contains(text(),\"Don\")]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'suggest')]/tbody//tr//td[contains(text(),\"Dean\")] | //div[contains(@id,'txt_firstName_div')]//li[contains(text(),\"Dean\")]"));
		// La lettre E :
		selenium.type("//input[contains(@id,'txt_firstName')]", "");
		selenium.typeKeys("//input[contains(@id,'txt_firstName')]", "E");
		selenium.focus("//input[contains(@id,'txt_firstName')]");
		Thread.sleep(1000);
		// On v�rifie les r�sultats :
		verifyEquals("6", selenium.getXpathCount("//table[contains(@id,'suggest')]/tbody//tr//td[2] | //div[contains(@id,'txt_firstName_div')]//li[@style='']"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'suggest')]/tbody//tr//td[contains(text(),\"Edwin\")] | //div[contains(@id,'txt_firstName_div')]//li[contains(text(),\"Edwin\")]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'suggest')]/tbody//tr//td[contains(text(),\"Edward\")] | //div[contains(@id,'txt_firstName_div')]//li[contains(text(),\"Edward\")]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'suggest')]/tbody//tr//td[contains(text(),\"Elbert\")] | //div[contains(@id,'txt_firstName_div')]//li[contains(text(),\"Elbert\")]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'suggest')]/tbody//tr//td[contains(text(),\"Earl\")] | //div[contains(@id,'txt_firstName_div')]//li[contains(text(),\"Earl\")]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'suggest')]/tbody//tr//td[contains(text(),\"Earnest\")] | //div[contains(@id,'txt_firstName_div')]//li[contains(text(),\"Earnest\")]"));
		// La lettre F :
		selenium.type("//input[contains(@id,'txt_firstName')]", "");
		selenium.typeKeys("//input[contains(@id,'txt_firstName')]", "F");
		selenium.focus("//input[contains(@id,'txt_firstName')]");
		Thread.sleep(1000);
		// On v�rifie les r�sultats :
		verifyEquals("1", selenium.getXpathCount("//table[contains(@id,'suggest')]/tbody//tr//td[2] | //div[contains(@id,'txt_firstName_div')]//li[@style='']"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'suggest')]/tbody//tr//td[contains(text(),\"Freddie\")] | //div[contains(@id,'txt_firstName_div')]//li[contains(text(),\"Freddie\")]"));
		// La lettre G :
		selenium.type("//input[contains(@id,'txt_firstName')]", "");
		selenium.typeKeys("//input[contains(@id,'txt_firstName')]", "G");
		selenium.focus("//input[contains(@id,'txt_firstName')]");
		Thread.sleep(1000);
		// On v�rifie les r�sultats :
		verifyEquals("5", selenium.getXpathCount("//table[contains(@id,'suggest')]/tbody//tr//td[2] | //div[contains(@id,'txt_firstName_div')]//li[@style='']"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'suggest')]/tbody//tr//td[contains(text(),\"Gilbert\")] | //div[contains(@id,'txt_firstName_div')]//li[contains(text(),\"Gilbert\")]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'suggest')]/tbody//tr//td[contains(text(),\"Gustavo\")] | //div[contains(@id,'txt_firstName_div')]//li[contains(text(),\"Gustavo\")]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'suggest')]/tbody//tr//td[contains(text(),\"Guadalupe\")] | //div[contains(@id,'txt_firstName_div')]//li[contains(text(),\"Guadalupe\")]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'suggest')]/tbody//tr//td[contains(text(),\"Gerardo\")] | //div[contains(@id,'txt_firstName_div')]//li[contains(text(),\"Gerardo\")]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'suggest')]/tbody//tr//td[contains(text(),\"Gerald\")] | //div[contains(@id,'txt_firstName_div')]//li[contains(text(),\"Gerald\")]"));
		// La lettre H
		selenium.type("//input[contains(@id,'txt_firstName')]", "");
		selenium.typeKeys("//input[contains(@id,'txt_firstName')]", "H");
		selenium.focus("//input[contains(@id,'txt_firstName')]");
		Thread.sleep(1000);
		// On v�rifie les r�sultats :
		verifyEquals("1", selenium.getXpathCount("//table[contains(@id,'suggest')]/tbody//tr//td[2] | //div[contains(@id,'txt_firstName_div')]//li[@style='']"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'suggest')]/tbody//tr//td[contains(text(),\"Howard\")] | //div[contains(@id,'txt_firstName_div')]//li[contains(text(),\"Howard\")]"));
		// La lettre I :
		selenium.type("//input[contains(@id,'txt_firstName')]", "");
		selenium.typeKeys("//input[contains(@id,'txt_firstName')]", "I");
		selenium.focus("//input[contains(@id,'txt_firstName')]");
		Thread.sleep(1000);
		// On v�rifie les r�sultats :
		verifyEquals("2", selenium.getXpathCount("//table[contains(@id,'suggest')]/tbody//tr//td[2] | //div[contains(@id,'txt_firstName_div')]//li[@style='']"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'suggest')]/tbody//tr//td[contains(text(),\"Irvin\")] | //div[contains(@id,'txt_firstName_div')]//li[contains(text(),\"Irvin\")]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'suggest')]/tbody//tr//td[contains(text(),\"Ivan\")] | //div[contains(@id,'txt_firstName_div')]//li[contains(text(),\"Ivan\")]"));
		// La lettre J :
		selenium.type("//input[contains(@id,'txt_firstName')]", "");
		selenium.typeKeys("//input[contains(@id,'txt_firstName')]", "J");
		selenium.focus("//input[contains(@id,'txt_firstName')]");
		Thread.sleep(1000);
		// On v�rifie les r�sultats :
		verifyEquals("10", selenium.getXpathCount("//table[contains(@id,'suggest')]/tbody//tr//td[2] | //div[contains(@id,'txt_firstName_div')]//li[@style='']"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'suggest')]/tbody//tr//td[contains(text(),\"Jared\")] | //div[contains(@id,'txt_firstName_div')]//li[contains(text(),\"Jared\")]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'suggest')]/tbody//tr//td[contains(text(),\"Jerome\")] | //div[contains(@id,'txt_firstName_div')]//li[contains(text(),\"Jerome\")]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'suggest')]/tbody//tr//td[contains(text(),\"Jerome\")] | //div[contains(@id,'txt_firstName_div')]//li[contains(text(),\"Jerome\")]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'suggest')]/tbody//tr//td[contains(text(),\"Jim\")] | //div[contains(@id,'txt_firstName_div')]//li[contains(text(),\"Jim\")]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'suggest')]/tbody//tr//td[contains(text(),\"Jody\")] | //div[contains(@id,'txt_firstName_div')]//li[contains(text(),\"Jody\")]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'suggest')]/tbody//tr//td[contains(text(),\"John\")] | //div[contains(@id,'txt_firstName_div')]//li[contains(text(),\"John\")]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'suggest')]/tbody//tr//td[contains(text(),\"Jordan\")] | //div[contains(@id,'txt_firstName_div')]//li[contains(text(),\"Jordan\")]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'suggest')]/tbody//tr//td[contains(text(),\"Joshua\")] | //div[contains(@id,'txt_firstName_div')]//li[contains(text(),\"Joshua\")]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'suggest')]/tbody//tr//td[contains(text(),\"Juan\")] | //div[contains(@id,'txt_firstName_div')]//li[contains(text(),\"Juan\")]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'suggest')]/tbody//tr//td[contains(text(),\"Julian\")] | //div[contains(@id,'txt_firstName_div')]//li[contains(text(),\"Julian\")]"));
		// La lettre k :
		selenium.type("//input[contains(@id,'txt_firstName')]", "");
		selenium.typeKeys("//input[contains(@id,'txt_firstName')]", "K");
		selenium.focus("//input[contains(@id,'txt_firstName')]");
		Thread.sleep(1000);
		// On v�rifie les r�sultats :
		verifyEquals("2", selenium.getXpathCount("//table[contains(@id,'suggest')]/tbody//tr//td[2] | //div[contains(@id,'txt_firstName_div')]//li[@style='']"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'suggest')]/tbody//tr//td[contains(text(),\"Kirk\")] | //div[contains(@id,'txt_firstName_div')]//li[contains(text(),\"Kirk\")]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'suggest')]/tbody//tr//td[contains(text(),\"Keith\")] | //div[contains(@id,'txt_firstName_div')]//li[contains(text(),\"Keith\")]"));
		// La lettre L :
		selenium.type("//input[contains(@id,'txt_firstName')]", "");
		selenium.typeKeys("//input[contains(@id,'txt_firstName')]", "L");
		selenium.focus("//input[contains(@id,'txt_firstName')]");
		Thread.sleep(1000);
		// On v�rifie les r�sultats :
		verifyEquals("3", selenium.getXpathCount("//table[contains(@id,'suggest')]/tbody//tr//td[2] | //div[contains(@id,'txt_firstName_div')]//li[@style='']"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'suggest')]/tbody//tr//td[contains(text(),\"Lucas\")] | //div[contains(@id,'txt_firstName_div')]//li[contains(text(),\"Lucas\")]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'suggest')]/tbody//tr//td[contains(text(),\"Leroy\")] | //div[contains(@id,'txt_firstName_div')]//li[contains(text(),\"Leroy\")]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'suggest')]/tbody//tr//td[contains(text(),\"Lester\")] | //div[contains(@id,'txt_firstName_div')]//li[contains(text(),\"Lester\")]"));
		// La lettre M :
		selenium.type("//input[contains(@id,'txt_firstName')]", "");
		selenium.typeKeys("//input[contains(@id,'txt_firstName')]", "M");
		selenium.focus("//input[contains(@id,'txt_firstName')]");
		Thread.sleep(1000);
		// On v�rifie les r�sultats :
		verifyEquals("6", selenium.getXpathCount("//table[contains(@id,'suggest')]/tbody//tr//td[2] | //div[contains(@id,'txt_firstName_div')]//li[@style='']"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'suggest')]/tbody//tr//td[contains(text(),\"Michael\")] | //div[contains(@id,'txt_firstName_div')]//li[contains(text(),\"Michael\")]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'suggest')]/tbody//tr//td[contains(text(),\"Marshall\")] | //div[contains(@id,'txt_firstName_div')]//li[contains(text(),\"Marshall\")]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'suggest')]/tbody//tr//td[contains(text(),\"Marvin\")] | //div[contains(@id,'txt_firstName_div')]//li[contains(text(),\"Marvin\")]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'suggest')]/tbody//tr//td[contains(text(),\"Milton\")] | //div[contains(@id,'txt_firstName_div')]//li[contains(text(),\"Milton\")]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'suggest')]/tbody//tr//td[contains(text(),\"Milton\")] | //div[contains(@id,'txt_firstName_div')]//li[contains(text(),\"Milton\")]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'suggest')]/tbody//tr//td[contains(text(),\"Matthew\")] | //div[contains(@id,'txt_firstName_div')]//li[contains(text(),\"Matthew\")]"));
		// La lettre N :
		selenium.type("//input[contains(@id,'txt_firstName')]", "");
		selenium.typeKeys("//input[contains(@id,'txt_firstName')]", "N");
		selenium.focus("//input[contains(@id,'txt_firstName')]");
		Thread.sleep(1000);
		// On v�rifie les r�sultats :
		verifyEquals("2", selenium.getXpathCount("//table[contains(@id,'suggest')]/tbody//tr//td[2] | //div[contains(@id,'txt_firstName_div')]//li[@style='']"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'suggest')]/tbody//tr//td[contains(text(),\"Nelson\")] | //div[contains(@id,'txt_firstName_div')]//li[contains(text(),\"Nelson\")]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'suggest')]/tbody//tr//td[contains(text(),\"Noel\")] | //div[contains(@id,'txt_firstName_div')]//li[contains(text(),\"Noel\")]"));
		// La lettre O :
		selenium.type("//input[contains(@id,'txt_firstName')]", "");
		selenium.typeKeys("//input[contains(@id,'txt_firstName')]", "O");
		selenium.focus("//input[contains(@id,'txt_firstName')]");
		Thread.sleep(1000);
		// On v�rifie les r�sultats :
		verifyEquals("2", selenium.getXpathCount("//table[contains(@id,'suggest')]/tbody//tr//td[2] | //div[contains(@id,'txt_firstName_div')]//li[@style='']"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'suggest')]/tbody//tr//td[contains(text(),\"Owen\")] | //div[contains(@id,'txt_firstName_div')]//li[contains(text(),\"Owen\")]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'suggest')]/tbody//tr//td[contains(text(),\"Oliver\")] | //div[contains(@id,'txt_firstName_div')]//li[contains(text(),\"Oliver\")]"));
		// La lettre P :
		selenium.type("//input[contains(@id,'txt_firstName')]", "");
		selenium.typeKeys("//input[contains(@id,'txt_firstName')]", "P");
		selenium.focus("//input[contains(@id,'txt_firstName')]");
		Thread.sleep(1000);
		// On v�rifie les r�sultats :
		verifyEquals("3", selenium.getXpathCount("//table[contains(@id,'suggest')]/tbody//tr//td[2] | //div[contains(@id,'txt_firstName_div')]//li[@style='']"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'suggest')]/tbody//tr//td[contains(text(),\"Paul\")] | //div[contains(@id,'txt_firstName_div')]//li[contains(text(),\"Paul\")]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'suggest')]/tbody//tr//td[contains(text(),\"Preston\")] | //div[contains(@id,'txt_firstName_div')]//li[contains(text(),\"Preston\")]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'suggest')]/tbody//tr//td[contains(text(),\"Pablo\")] | //div[contains(@id,'txt_firstName_div')]//li[contains(text(),\"Pablo\")]"));
		// La lettre Q :
		selenium.type("//input[contains(@id,'txt_firstName')]", "");
		selenium.typeKeys("//input[contains(@id,'txt_firstName')]", "Q");
		selenium.focus("//input[contains(@id,'txt_firstName')]");
		Thread.sleep(1000);
		// On v�rifie les r�sultats :
		verifyEquals("0", selenium.getXpathCount("//table[contains(@id,'suggest')]/tbody//tr//td[2] | //div[contains(@id,'txt_firstName_div')]//li[@style='']"));
		// La lettre R :
		selenium.type("//input[contains(@id,'txt_firstName')]", "");
		selenium.typeKeys("//input[contains(@id,'txt_firstName')]", "R");
		selenium.focus("//input[contains(@id,'txt_firstName')]");
		Thread.sleep(1000);
		// On v�rifie les r�sultats :
		verifyEquals("9", selenium.getXpathCount("//table[contains(@id,'suggest')]/tbody//tr//td[2] | //div[contains(@id,'txt_firstName_div')]//li[@style='']"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'suggest')]/tbody//tr//td[contains(text(),\"Ricky\")] | //div[contains(@id,'txt_firstName_div')]//li[contains(text(),\"Ricky\")]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'suggest')]/tbody//tr//td[contains(text(),\"Roderick\")] | //div[contains(@id,'txt_firstName_div')]//li[contains(text(),\"Roderick\")]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'suggest')]/tbody//tr//td[contains(text(),\"Raffy\")] | //div[contains(@id,'txt_firstName_div')]//li[contains(text(),\"Raffy\")]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'suggest')]/tbody//tr//td[contains(text(),\"Rafael\")] | //div[contains(@id,'txt_firstName_div')]//li[contains(text(),\"Rafael\")]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'suggest')]/tbody//tr//td[contains(text(),\"Ramon\")] | //div[contains(@id,'txt_firstName_div')]//li[contains(text(),\"Ramon\")]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'suggest')]/tbody//tr//td[contains(text(),\"Ricardo\")] | //div[contains(@id,'txt_firstName_div')]//li[contains(text(),\"Ricardo\")]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'suggest')]/tbody//tr//td[contains(text(),\"Rolando\")] | //div[contains(@id,'txt_firstName_div')]//li[contains(text(),\"Rolando\")]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'suggest')]/tbody//tr//td[contains(text(),\"Ruben\")] | //div[contains(@id,'txt_firstName_div')]//li[contains(text(),\"Ruben\")]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'suggest')]/tbody//tr//td[contains(text(),\"Ralph\")] | //div[contains(@id,'txt_firstName_div')]//li[contains(text(),\"Ralph\")]"));
		// La lettre S :
        selenium.type("//input[contains(@id,'txt_firstName')]", "");
		selenium.typeKeys("//input[contains(@id,'txt_firstName')]", "S");
		selenium.focus("//input[contains(@id,'txt_firstName')]");
		Thread.sleep(1000);
		// On v�rifie les r�sultats :
		verifyEquals("8", selenium.getXpathCount("//table[contains(@id,'suggest')]/tbody//tr//td[2] | //div[contains(@id,'txt_firstName_div')]//li[@style='']"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'suggest')]/tbody//tr//td[contains(text(),\"Salvatore\")] | //div[contains(@id,'txt_firstName_div')]//li[contains(text(),\"Salvatore\")]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'suggest')]/tbody//tr//td[contains(text(),\"Stanley\")] | //div[contains(@id,'txt_firstName_div')]//li[contains(text(),\"Stanley\")]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'suggest')]/tbody//tr//td[contains(text(),\"Salvador\")] | //div[contains(@id,'txt_firstName_div')]//li[contains(text(),\"Salvador\")]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'suggest')]/tbody//tr//td[contains(text(),\"Sammy\")] | //div[contains(@id,'txt_firstName_div')]//li[contains(text(),\"Sammy\")]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'suggest')]/tbody//tr//td[contains(text(),\"Shannon\")] | //div[contains(@id,'txt_firstName_div')]//li[contains(text(),\"Shannon\")]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'suggest')]/tbody//tr//td[contains(text(),\"Stephen\")] | //div[contains(@id,'txt_firstName_div')]//li[contains(text(),\"Stephen\")]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'suggest')]/tbody//tr//td[contains(text(),\"Stuart\")] | //div[contains(@id,'txt_firstName_div')]//li[contains(text(),\"Stuart\")]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'suggest')]/tbody//tr//td[contains(text(),\"Salvador\")] | //div[contains(@id,'txt_firstName_div')]//li[contains(text(),\"Salvador\")]"));
		// La lettre T :
		selenium.type("//input[contains(@id,'txt_firstName')]", "");
		selenium.typeKeys("//input[contains(@id,'txt_firstName')]", "T");
		selenium.focus("//input[contains(@id,'txt_firstName')]");
		Thread.sleep(1000);
		// On v�rifie les r�sultats :
		verifyEquals("2", selenium.getXpathCount("//table[contains(@id,'suggest')]/tbody//tr//td[2] | //div[contains(@id,'txt_firstName_div')]//li[@style='']"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'suggest')]/tbody//tr//td[contains(text(),\"Todd\")] | //div[contains(@id,'txt_firstName_div')]//li[contains(text(),\"Todd\")]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'suggest')]/tbody//tr//td[contains(text(),\"Timmy\")] | //div[contains(@id,'txt_firstName_div')]//li[contains(text(),\"Timmy\")]"));
		// La lettre U :
        selenium.type("//input[contains(@id,'txt_firstName')]", "");
		selenium.typeKeys("//input[contains(@id,'txt_firstName')]", "U");
		selenium.focus("//input[contains(@id,'txt_firstName')]");
		Thread.sleep(1000);
		// On v�rifie les r�sultats :
		verifyEquals("0", selenium.getXpathCount("//table[contains(@id,'suggest')]/tbody//tr//td[2] | //div[contains(@id,'txt_firstName_div')]//li[@style='']"));
		// La lettre V :
		selenium.type("//input[contains(@id,'txt_firstName')]", "");
		selenium.typeKeys("//input[contains(@id,'txt_firstName')]", "V");
		selenium.focus("//input[contains(@id,'txt_firstName')]");
		Thread.sleep(1000);
		// On v�rifie les r�sultats :
		verifyEquals("2", selenium.getXpathCount("//table[contains(@id,'suggest')]/tbody//tr//td[2] | //div[contains(@id,'txt_firstName_div')]//li[@style='']"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'suggest')]/tbody//tr//td[contains(text(),\"Vernon\")] | //div[contains(@id,'txt_firstName_div')]//li[contains(text(),\"Vernon\")]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'suggest')]/tbody//tr//td[contains(text(),\"Vernon\")] | //div[contains(@id,'txt_firstName_div')]//li[contains(text(),\"Vernon\")]"));
		// La lettre W :
        selenium.type("//input[contains(@id,'txt_firstName')]", "");
		selenium.typeKeys("//input[contains(@id,'txt_firstName')]", "W");
		selenium.focus("//input[contains(@id,'txt_firstName')]");
		Thread.sleep(1000);
		// On v�rifie les r�sultats :
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'suggest')]/tbody//tr//td[2] | //div[contains(@id,'txt_firstName_div')]//li[@style='']"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'suggest')]/tbody//tr//td[contains(text(),\"Woodrow\")] | //div[contains(@id,'txt_firstName_div')]//li[contains(text(),\"Woodrow\")]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'suggest')]/tbody//tr//td[contains(text(),\"Wayne\")] | //div[contains(@id,'txt_firstName_div')]//li[contains(text(),\"Wayne\")]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'suggest')]/tbody//tr//td[contains(text(),\"Willis\")] | //div[contains(@id,'txt_firstName_div')]//li[contains(text(),\"Willis\")]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'suggest')]/tbody//tr//td[contains(text(),\"Walter\")] | //div[contains(@id,'txt_firstName_div')]//li[contains(text(),\"Walter\")]"));
		// la lettre X :
		selenium.type("//input[contains(@id,'txt_firstName')]", "");
		selenium.typeKeys("//input[contains(@id,'txt_firstName')]", "X");
		selenium.focus("//input[contains(@id,'txt_firstName')]");
		Thread.sleep(1000);
		// On v�rifie les r�sultats :
		verifyEquals("0", selenium.getXpathCount("//table[contains(@id,'suggest')]/tbody//tr//td[2] | //div[contains(@id,'txt_firstName_div')]//li[@style='']"));
		// La lettre Y :
		selenium.type("//input[contains(@id,'txt_firstName')]", "");
		selenium.typeKeys("//input[contains(@id,'txt_firstName')]", "Y");
		selenium.focus("//input[contains(@id,'txt_firstName')]");
		Thread.sleep(1000);
		// On v�rifie les r�sultats :
		verifyEquals("0", selenium.getXpathCount("//table[contains(@id,'suggest')]/tbody//tr//td[2] | //div[contains(@id,'txt_firstName_div')]//li[@style='']"));
		// La lettre Z :
		selenium.type("//input[contains(@id,'txt_firstName')]", "");
		selenium.typeKeys("//input[contains(@id,'txt_firstName')]", "Z");
		selenium.focus("//input[contains(@id,'txt_firstName')]");
		Thread.sleep(1000);
		// On v�rifie les r�sultats :
		verifyEquals("0", selenium.getXpathCount("//table[contains(@id,'suggest')]/tbody//tr//td[2] | //div[contains(@id,'txt_firstName_div')]//li[@style='']"));

	}

}
