/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.advancedfeatures.valueobject.uc206_valueobject ;

// Import declaration.
// Java imports.
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.bluage.documentation.business.advancedfeatures.valueobject.uc206_valueobject.vo.Screen206VO;
import com.bluage.documentation.service.advancedfeatures.valueobject.uc206_valueobject.ServiceScreen206;
import com.netfective.bluage.core.exception.ApplicationException;
import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;
import com.netfective.bluage.core.web.IForm;

/**
 * Class : UC206_SearchPlayerController
 */
@Controller
@Scope("prototype")
@SessionAttributes("uC206_SearchPlayerForm")
@RequestMapping(value= "/presentation/advancedfeatures/valueobject/uc206_valueobject")
public class UC206_SearchPlayerController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC206_SearchPlayerController.class);
	
	//Current form name
	private static final String U_C206__SEARCH_PLAYER_FORM = "uC206_SearchPlayerForm";

					//CONSTANT: listPosition206
	private static final String LIST_POSITION206 = "listPosition206";
				//CONSTANT: playerVOs
	private static final String PLAYER_V_OS = "playerVOs";
	//CONSTANT: screen206
	private static final String SCREEN206 = "screen206";
	
	/**
	 * Property:customDateEditorsUC206_SearchPlayerController
	 */
	final private Map<String,CustomDateEditor> customDateEditorsUC206_SearchPlayerController = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:uC206_SearchPlayerValidator
	 */
	final private UC206_SearchPlayerValidator uC206_SearchPlayerValidator = new UC206_SearchPlayerValidator();
	
	/**
	 * Service declaration : serviceScreen206.
	 */
	@Autowired
	private ServiceScreen206 serviceScreen206;
	
	
	// Initialise all the instances for UC206_SearchPlayerController
							// Initialize the instance listPosition206 for the state lnk_createPlayer
		private List listPosition206; // Initialize the instance listPosition206 for UC206_SearchPlayerController
						// Declare the instance playerVOs
		private List playerVOs;
		// Declare the instance screen206
		private Screen206VO screen206;
			/**
	 * Operation : lnk_searchProperties
 	 * @param model : 
 	 * @param uC206_SearchPlayer : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC206_SearchPlayer/lnk_searchProperties.html",method = RequestMethod.POST)
	public String lnk_searchProperties(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC206_SearchPlayerForm") UC206_SearchPlayerForm  uC206_SearchPlayerForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : lnk_searchProperties"); 
		infoLogger(uC206_SearchPlayerForm); 
		uC206_SearchPlayerForm.setAlwaysCallPreControllers(false); 
		// initialization for lnk_searchProperties
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state lnk_searchProperties if the validation fail, you will be forward to the screen : "/presentation/advancedfeatures/valueobject/uc206_valueobject/UC206_SearchPlayer"
		if(errorsMessages(request,response,uC206_SearchPlayerForm,bindingResult,"", uC206_SearchPlayerValidator, customDateEditorsUC206_SearchPlayerController)){ 
			return "/presentation/advancedfeatures/valueobject/uc206_valueobject/UC206_SearchPlayer"; 
		}

					 callSearchPlayerByProperties(request,uC206_SearchPlayerForm );
						// Init the destination form com.bluage.documentation.web.presentation.advancedfeatures.valueobject.uc206_valueobject.UC206_SearchPlayerForm or not from lnk_searchProperties
			// Populate the destination form with all the instances returned from lnk_searchProperties.
			final IForm uC206_SearchPlayerForm2 = populateDestinationForm(request.getSession(), uC206_SearchPlayerForm); 
					
			// Add the form to the model.
			model.addAttribute("uC206_SearchPlayerForm", uC206_SearchPlayerForm2); 
			
			request.getSession().setAttribute("uC206_SearchPlayerForm", uC206_SearchPlayerForm2);
			
			// "OK" CASE => destination screen path from lnk_searchProperties
			LOGGER.info("Go to the screen 'UC206_SearchPlayer'.");
			// Redirect (PRG) from lnk_searchProperties
			destinationPath =  "redirect:/presentation/advancedfeatures/valueobject/uc206_valueobject/UC206_SearchPlayer.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_searchProperties 
	}
	
				/**
	 * Operation : lnk_searchCriteria
 	 * @param model : 
 	 * @param uC206_SearchPlayer : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC206_SearchPlayer/lnk_searchCriteria.html",method = RequestMethod.POST)
	public String lnk_searchCriteria(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC206_SearchPlayerForm") UC206_SearchPlayerForm  uC206_SearchPlayerForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : lnk_searchCriteria"); 
		infoLogger(uC206_SearchPlayerForm); 
		uC206_SearchPlayerForm.setAlwaysCallPreControllers(false); 
		// initialization for lnk_searchCriteria
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state lnk_searchCriteria if the validation fail, you will be forward to the screen : "/presentation/advancedfeatures/valueobject/uc206_valueobject/UC206_SearchPlayer"
		if(errorsMessages(request,response,uC206_SearchPlayerForm,bindingResult,"", uC206_SearchPlayerValidator, customDateEditorsUC206_SearchPlayerController)){ 
			return "/presentation/advancedfeatures/valueobject/uc206_valueobject/UC206_SearchPlayer"; 
		}

					 callSearchTeamByCriteria(request,uC206_SearchPlayerForm );
						// Init the destination form com.bluage.documentation.web.presentation.advancedfeatures.valueobject.uc206_valueobject.UC206_SearchPlayerForm or not from lnk_searchCriteria
			// Populate the destination form with all the instances returned from lnk_searchCriteria.
			final IForm uC206_SearchPlayerForm2 = populateDestinationForm(request.getSession(), uC206_SearchPlayerForm); 
					
			// Add the form to the model.
			model.addAttribute("uC206_SearchPlayerForm", uC206_SearchPlayerForm2); 
			
			request.getSession().setAttribute("uC206_SearchPlayerForm", uC206_SearchPlayerForm2);
			
			// "OK" CASE => destination screen path from lnk_searchCriteria
			LOGGER.info("Go to the screen 'UC206_SearchPlayer'.");
			// Redirect (PRG) from lnk_searchCriteria
			destinationPath =  "redirect:/presentation/advancedfeatures/valueobject/uc206_valueobject/UC206_SearchPlayer.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_searchCriteria 
	}
	
	
	/**
	* This method initialise the form : UC206_SearchPlayerForm 
	* @return UC206_SearchPlayerForm
	*/
	@ModelAttribute("UC206_SearchPlayerFormInit")
	public void initUC206_SearchPlayerForm(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, U_C206__SEARCH_PLAYER_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method UC206_SearchPlayerForm."); //for lnk_searchCriteria 
		}
		UC206_SearchPlayerForm uC206_SearchPlayerForm;
	
		if(request.getSession().getAttribute(U_C206__SEARCH_PLAYER_FORM) != null){
			uC206_SearchPlayerForm = (UC206_SearchPlayerForm)request.getSession().getAttribute(U_C206__SEARCH_PLAYER_FORM);
		} else {
			uC206_SearchPlayerForm = new UC206_SearchPlayerForm();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form UC206_SearchPlayerForm.");
		}
		uC206_SearchPlayerForm = (UC206_SearchPlayerForm)populateDestinationForm(request.getSession(), uC206_SearchPlayerForm);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  UC206_SearchPlayerForm.");
		}
		model.addAttribute(U_C206__SEARCH_PLAYER_FORM, uC206_SearchPlayerForm);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param UC206_SearchPlayerForm : The sceen form.
	 */
	@RequestMapping(value = "/UC206_SearchPlayer.html" ,method = RequestMethod.GET)
	public void prepareUC206_SearchPlayer(final HttpServletRequest request,final  Model model,final  @ModelAttribute("UC206_SearchPlayerFormInit") UC206_SearchPlayerForm uC206_SearchPlayerForm){
		
		UC206_SearchPlayerForm currentUC206_SearchPlayerForm = uC206_SearchPlayerForm;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(U_C206__SEARCH_PLAYER_FORM) == null){
			if(currentUC206_SearchPlayerForm!=null){
				request.getSession().setAttribute(U_C206__SEARCH_PLAYER_FORM, currentUC206_SearchPlayerForm);
			}else {
				currentUC206_SearchPlayerForm = new UC206_SearchPlayerForm();
				request.getSession().setAttribute(U_C206__SEARCH_PLAYER_FORM, currentUC206_SearchPlayerForm);	
			}
		} else {
			currentUC206_SearchPlayerForm = (UC206_SearchPlayerForm) request.getSession().getAttribute(U_C206__SEARCH_PLAYER_FORM);
		}

					currentUC206_SearchPlayerForm = (UC206_SearchPlayerForm)populateDestinationForm(request.getSession(), currentUC206_SearchPlayerForm);
		// Call all the Precontroller.
	currentUC206_SearchPlayerForm.setAlwaysCallPreControllers(true);
		model.addAttribute(U_C206__SEARCH_PLAYER_FORM, currentUC206_SearchPlayerForm);
		request.getSession().setAttribute(U_C206__SEARCH_PLAYER_FORM, currentUC206_SearchPlayerForm);
		
	}
	
							/**
	 * method callSearchPlayerByProperties
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callSearchPlayerByProperties(HttpServletRequest request,IForm form) {
		playerVOs = (List)get(request.getSession(),form, "playerVOs");  
										 
					screen206 = (Screen206VO)get(request.getSession(),form, "screen206");  
										 
					try {
				// executing SearchPlayerByProperties in lnk_searchCriteria
				playerVOs = 	serviceScreen206.searchPlayer206ByProperties(
			screen206
			);  
 
				put(request.getSession(), PLAYER_V_OS,playerVOs);
								// processing variables SearchPlayerByProperties in lnk_searchCriteria
				put(request.getSession(), SCREEN206,screen206); 
			
			} catch (ApplicationException e) { 
				// error handling for operation searchPlayer206ByProperties called SearchPlayerByProperties
				errorLogger("An error occured during the execution of the operation : searchPlayer206ByProperties",e); 
		
			}
	}
					/**
	 * method callSearchTeamByCriteria
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callSearchTeamByCriteria(HttpServletRequest request,IForm form) {
		playerVOs = (List)get(request.getSession(),form, "playerVOs");  
										 
					screen206 = (Screen206VO)get(request.getSession(),form, "screen206");  
										 
					try {
				// executing SearchTeamByCriteria in lnk_searchCriteria
				playerVOs = 	serviceScreen206.searchPlayer206ByCriteria(
			screen206
			);  
 
				put(request.getSession(), PLAYER_V_OS,playerVOs);
								// processing variables SearchTeamByCriteria in lnk_searchCriteria
				put(request.getSession(), SCREEN206,screen206); 
			
			} catch (ApplicationException e) { 
				// error handling for operation searchPlayer206ByCriteria called SearchTeamByCriteria
				errorLogger("An error occured during the execution of the operation : searchPlayer206ByCriteria",e); 
		
			}
	}
		
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("UC206_SearchPlayerController [ ");
							strBToS.append(LIST_POSITION206);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(listPosition206);
			strBToS.append(END_TO_STRING_DELIMITER);
						strBToS.append(PLAYER_V_OS);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(playerVOs);
			strBToS.append(END_TO_STRING_DELIMITER);
			strBToS.append(SCREEN206);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(screen206);
			strBToS.append(END_TO_STRING_DELIMITER);
		// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(uC206_SearchPlayerValidator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsUC206_SearchPlayerController!=null); 
		return strBToS.toString();
	}
}
