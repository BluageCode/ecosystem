/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.service.standardfeatures.application.form.uc41_actionondropdownselection;


import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import com.bluage.documentation.business.standardfeatures.application.form.uc41_actionondropdownselection.bos.Position41BO;
import com.bluage.documentation.business.standardfeatures.application.form.uc41_actionondropdownselection.entities.daofinder.Player41DAOFinderImpl;
import com.bluage.documentation.service.AbstractService;
import com.bluage.documentation.service.standardfeatures.application.form.uc41_actionondropdownselection.daofinder.ServicePlayer41DAOMethodFinderImpl;
import com.netfective.bluage.core.exception.ApplicationException;

/**
 * Defines  application services for ServicePlayer41
  */
@Named("com.bluage.documentation.service.standardfeatures.application.form.uc41_actionondropdownselection.ServicePlayer41")
public class ServicePlayer41  extends AbstractService{


			@Inject
		@Named ("com.bluage.documentation.service.standardfeatures.application.form.uc41_actionondropdownselection.daofinder.ServicePlayer41DAOMethodFinder")
		private ServicePlayer41DAOMethodFinderImpl servicePlayer41Dao;
					@Inject
		@Named ("com.bluage.documentation.business.standardfeatures.application.form.uc41_actionondropdownselection.entities.daofinder.Player41DAOFinderImpl")
		private Player41DAOFinderImpl player41BOFinder;
						


	/**
	 *  Default constructor 
	 */
	public ServicePlayer41() {
		super(); // Call super for default constructor : ServicePlayer41
	}
	



								
	/**    
	 * Operation Crud
	 * @return {@link List}
	 */
	public List findAllPlayers41( )
 throws ApplicationException {

					return player41BOFinder.findAll();
	}

		
				
	/**    
	 * Operation HQL.
	 * @param code {@link String}
		 * @return {@link List}
	 */
	public List findPlayers41ByPosition41(String code )
 throws ApplicationException {
	
	return servicePlayer41Dao.findPlayers41ByPosition41(code);
	}

		
			
	/**    
	 * Process operation.
	 * @param position {@link Position41BO}
		 * @return {@link List}
	 */
	public List loadPlayers41(Position41BO position )
 throws ApplicationException {

				return privateLoadPlayers41(position);
	}
	

	/**    
	 * Process operation.
	 * @param position {@link Position41BO}
		 * @return {@link List}
	 */
	private List privateLoadPlayers41(Position41BO position )
 throws ApplicationException {
						// Return Type declaration.
				List players = new java.util.ArrayList();
					// Central buffer node declaration for operation loadPlayers41
			String	 code =  "";
					// Calling an operation : 
code = position.getCode();
if ("ALL".equals(code)) {
// Calling an operation : 
players = findAllPlayers41();
} else {
// Calling an operation : 
players = findPlayers41ByPosition41(code);
}

						return players;
		}

		
			
	/**    
	 * Process operation.
	 * @return {@link void}
	 */
	public void doNothing( )
 throws ApplicationException {

			privateDoNothing();
	}
	

	/**    
	 * Process operation.
	 * @return {@link void}
	 */
	private void privateDoNothing( )
 throws ApplicationException {
// Central buffer node declaration for operation doNothing
			
		}

		
			
	/**    
	 * Process operation.
	 * @param player {@link List}
		 * @param position {@link Position41BO}
		 * @return {@link void}
	 */
	public void doNothingBo(List player,Position41BO position )
 throws ApplicationException {

			privateDoNothingBo(player,position);
	}
	

	/**    
	 * Process operation.
	 * @param player {@link List}
		 * @param position {@link Position41BO}
		 * @return {@link void}
	 */
	private void privateDoNothingBo(List player,Position41BO position )
 throws ApplicationException {
				// Central buffer node declaration for operation doNothingBo
			
		}

		}
