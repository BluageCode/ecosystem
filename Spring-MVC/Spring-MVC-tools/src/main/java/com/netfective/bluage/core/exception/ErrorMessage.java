/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.netfective.bluage.core.exception;

/**
 * La classe represente les differents types d exception system
 * 
 * <DL>
 * <DT><B>Nom du Projet: </B><DD>Bluage</DD>
 * <DT><B>Service: </B>
 * 
 */

public class ErrorMessage {
	
	private String key;
	private String value;

	/** 
	 * constructor for ErrorMessage
	 * @param key String
	 * @param value String
	 */
	public ErrorMessage(final String key, final String value){
		this.key = key;
		this.value = value;
	}
	/**
	 * @return
	 */
	public String getKey() {
		return key;
	}

	/**
	 * @return
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @param string
	 */
	public void setKey(final String string) {
		key = string;
	}

	/**
	 * @param string
	 */
	public void setValue(final String string) {
		value = string;
	}
}
