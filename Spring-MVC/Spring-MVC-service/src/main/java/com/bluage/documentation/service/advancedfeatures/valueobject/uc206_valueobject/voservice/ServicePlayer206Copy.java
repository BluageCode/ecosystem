/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.service.advancedfeatures.valueobject.uc206_valueobject.voservice;


import javax.inject.Named;

import com.bluage.documentation.business.advancedfeatures.valueobject.uc206_valueobject.bos.Player206BO;
import com.bluage.documentation.business.advancedfeatures.valueobject.uc206_valueobject.vo.Player206VO;
import com.bluage.documentation.service.AbstractVOService;
import com.netfective.bluage.core.exception.ApplicationException;


/**
 * Defines  application VO services for ServicePlayer206Copy
 *
 */
					@Named ("com.bluage.documentation.service.advancedfeatures.valueobject.uc206_valueobject.voservice.ServicePlayer206Copy")
public class ServicePlayer206Copy  extends AbstractVOService<Player206BO,Player206VO> {

	/**
	 * The associated BO
	 */
	private Class<Player206BO> refBo = Player206BO.class;
	
	/**
	 * The corresponding VO
	 */
	private Class<Player206VO> refVo = Player206VO.class;
	
	/**
	* returns the associated BO
	* @return Class
	*/
	@Override
	public Class<Player206BO> getRefBo() {
		return refBo;
	}

	/**
	* returns the corresponding VO
	* @return Class
	*/
	@Override
	public Class<Player206VO> getRefVo() {
		return refVo;
	}

	/**
	 *  Default constructor 
	 */
	public ServicePlayer206Copy() {
		super();
	}



			
	/**    
	 * VO Copy operation.
	 * @param vo {@link Player206VO}
		 * @return {@link Player206BO}
	 */
	public Player206BO player206CopyVO2BO(Player206VO vo )
 throws ApplicationException {
		return copyVO2BO(vo);
	}

		
			
	/**    
	 * VO Copy operation.
	 * @param bo {@link Player206BO}
		 * @return {@link Player206VO}
	 */
	public Player206VO player206CopyBO2VO(Player206BO bo )
 throws ApplicationException {
		return copyBO2VO(bo);
	}

		}
