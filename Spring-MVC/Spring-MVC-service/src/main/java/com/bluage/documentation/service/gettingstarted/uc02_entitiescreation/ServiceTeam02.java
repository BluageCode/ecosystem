/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.service.gettingstarted.uc02_entitiescreation;


import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import com.bluage.documentation.business.gettingstarted.uc02_entitiescreation.bos.State02BO;
import com.bluage.documentation.business.gettingstarted.uc02_entitiescreation.bos.Team02BO;
import com.bluage.documentation.business.gettingstarted.uc02_entitiescreation.entities.dao.Team02DAOImpl;
import com.bluage.documentation.business.gettingstarted.uc02_entitiescreation.entities.daofinder.Team02DAOFinderImpl;
import com.bluage.documentation.service.AbstractService;
import com.netfective.bluage.core.exception.ApplicationException;

/**
 * Defines  application services for ServiceTeam02
  */
@Named("com.bluage.documentation.service.gettingstarted.uc02_entitiescreation.ServiceTeam02")
public class ServiceTeam02  extends AbstractService{


			@Inject
		@Named ("com.bluage.documentation.business.gettingstarted.uc02_entitiescreation.entities.daofinder.Team02DAOFinderImpl")
		private Team02DAOFinderImpl team02BOFinder;
		@Inject
		@Named ("com.bluage.documentation.business.gettingstarted.uc02_entitiescreation.entities.dao.Team02DAOImpl")
		private Team02DAOImpl team02BODao;
					@Inject
		@Named ("com.bluage.documentation.service.gettingstarted.uc02_entitiescreation.ServiceState02")
		private ServiceState02  instanceServiceState02;
						


	/**
	 *  Default constructor 
	 */
	public ServiceTeam02() {
		super(); // Call super for default constructor : ServiceTeam02
	}
	



								
	/**    
	 * Operation Crud
	 * @return {@link List}
	 */
	public List team02FindAll( )
 throws ApplicationException {

					return team02BOFinder.findAll();
	}

					
				
	/**    
	 * Operation Crud
	 * @param bo {@link Team02BO}
		 * @return {@link boolean}
	 */
	public boolean team02Create(Team02BO bo )
 throws ApplicationException {

			team02BODao.setRefreshSession(true);
				return team02BODao.create(bo);
	}

					
				
	/**    
	 * Operation Crud
	 * @param bo {@link Team02BO}
		 * @return {@link boolean}
	 */
	public boolean team02Delete(Team02BO bo )
 throws ApplicationException {

					return team02BODao.delete(bo);
	}

		
			
	/**    
	 * Process operation.
	 * @param teamToCreate {@link Team02BO}
		 * @return {@link void}
	 */
	public void createTeam02(Team02BO teamToCreate )
 throws ApplicationException {

			privateCreateTeam02(teamToCreate);
	}
	

	/**    
	 * Process operation.
	 * @param teamToCreate {@link Team02BO}
		 * @return {@link void}
	 */
	private void privateCreateTeam02(Team02BO teamToCreate )
 throws ApplicationException {
		// Central buffer node declaration for operation createTeam02
			State02BO	 stateResult =  new State02BO();
					State02BO	 state =  new State02BO();
						// Calling an operation : 
state = teamToCreate.getState();
// Calling an operation : 
stateResult = instanceServiceState02.state02FindByID(state);
// Calling an operation : 
teamToCreate.setState(stateResult);
// Calling an operation : 
team02Create(teamToCreate);

		}

		}
