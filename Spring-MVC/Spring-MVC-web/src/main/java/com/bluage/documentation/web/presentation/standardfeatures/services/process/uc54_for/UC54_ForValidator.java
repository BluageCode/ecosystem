/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.services.process.uc54_for ;


import org.apache.log4j.Logger;
import org.springframework.validation.Errors;

import com.netfective.bluage.core.validator.AbstractValidator;
/**
* Class :UC54_ForValidator
*/
public class UC54_ForValidator extends AbstractValidator{
	
	// LOGGER for the class UC54_ForValidator
	private static final Logger LOGGER = Logger.getLogger( UC54_ForValidator.class);
	
	
	/**
	* Operation validate for UC54_ForForm
	* @param obj : the current form (UC54_ForForm)
	* @param errors : The spring errors to return for the form UC54_ForForm
	*/
	public void validate(final Object obj,final Errors errors){
		LOGGER.info("Beginning method : validate"); // UC54_ForValidator
		UC54_ForForm cUC54_ForForm = (UC54_ForForm)obj; // UC54_ForValidator
		LOGGER.info("Ending method : validate the form "+ cUC54_ForForm.getClass().getName()); // UC54_ForValidator
	}

	/**
	* Method to implements to use spring validators (UC54_ForForm)
	* @param aClass : Class for the form UC54_ForForm
	* @return boolean
	*/
	public boolean supports(final Class aClass){
		return (new UC54_ForForm()).getClass().equals(aClass); // UC54_ForValidator
	}
}
