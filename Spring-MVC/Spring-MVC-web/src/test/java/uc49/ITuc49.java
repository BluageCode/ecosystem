package uc49;

import org.junit.Test;
import test.common.AbstractBADocTestCase;

public class ITuc49 extends AbstractBADocTestCase {
	
	@Override
	public  String getName(){
		return "UC-49";
	}
	
	@Test
	public void testITuc49() throws Exception {
		// On teste l'UC49 - Standard Features/Services/BAGS - BAGS
		selenium.open("");
		selenium.click("link=Expand All");
		selenium.waitForPageToLoad("30000");
		selenium.click("link=UC49 - BAGS");
		// On teste l'affichage : 
		assertTrue(selenium.isElementPresent("//h2[contains(text(),'Standard Features > Services > BAGS > UC49 > BAGS')]"));
		assertTrue(selenium.isElementPresent("//*[contains(text(),'Blu Age Getters and Setters')]"));
		assertTrue(selenium.isElementPresent("//h4[contains(text(),'Players list')]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/thead/tr[th[contains(text(),'Name')] and th[contains(text(),'Date of Birth')] and th[contains(text(),'Weight (kg)')] and th[contains(text(),'Height (cm)')] and th[contains(text(),'Rookie')]]"));
	}

	
}
