/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.service.common.utility;


import javax.inject.Named;

import com.bluage.documentation.service.AbstractService;
import com.netfective.bluage.core.exception.ApplicationException;

/**
 * Defines  application services for ServiceUtility
  */
@Named("com.bluage.documentation.service.common.utility.ServiceUtility")
public class ServiceUtility  extends AbstractService{


				


	/**
	 *  Default constructor 
	 */
	public ServiceUtility() {
		super(); // Call super for default constructor : ServiceUtility
	}
	



			
	/**    
	 * Process operation.
	 * @return {@link void}
	 */
	public void doNothing( )
 throws ApplicationException {

			privateDoNothing();
	}
	

	/**    
	 * Process operation.
	 * @return {@link void}
	 */
	private void privateDoNothing( )
 throws ApplicationException {
// Central buffer node declaration for operation doNothing
			
		}

		}
