/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.application.datagrid.uc35_paginatorbdd ;

// Import declaration.
// Java imports.
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.displaytag.tags.TableTagParameters;
import org.displaytag.util.ParamEncoder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.bluage.documentation.business.standardfeatures.application.datagrid.uc35_paginatorbdd.entities.daofinder.Player35DAOFinderImpl;
import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;

/**
 * Class : UC35_PaginatorBDDController
 */
@Controller
@Scope("prototype")
@SessionAttributes("uC35_PaginatorBDDForm")
@RequestMapping(value= "/presentation/standardfeatures/application/datagrid/uc35_paginatorbdd")
public class UC35_PaginatorBDDController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC35_PaginatorBDDController.class);
	
	//Current form name
	private static final String U_C35__PAGINATOR_B_D_D_FORM = "uC35_PaginatorBDDForm";

	//CONSTANT: listPlayers table or repeater.
	private static final String LIST_PLAYERS = "listPlayers";
				
	/**
	 * Property:customDateEditorsUC35_PaginatorBDDController
	 */
	final private Map<String,CustomDateEditor> customDateEditorsUC35_PaginatorBDDController = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:uC35_PaginatorBDDValidator
	 */
	final private UC35_PaginatorBDDValidator uC35_PaginatorBDDValidator = new UC35_PaginatorBDDValidator();
	
	/**
	 * Generic Finder : player35DAOFinderImpl.
	 */
	@Autowired
	private Player35DAOFinderImpl player35DAOFinderImpl;
	
	
	// Initialise all the instances for UC35_PaginatorBDDController
		// Initialize the instance listPlayers for the state lnk_paginatorbdd
		private List listPlayers; // Initialize the instance listPlayers for UC35_PaginatorBDDController
				
	/**
	* This method initialise the form : UC35_PaginatorBDDForm 
	* @return UC35_PaginatorBDDForm
	*/
	@ModelAttribute("UC35_PaginatorBDDFormInit")
	public void initUC35_PaginatorBDDForm(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, U_C35__PAGINATOR_B_D_D_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method UC35_PaginatorBDDForm."); //for lnk_paginatorbdd 
		}
		UC35_PaginatorBDDForm uC35_PaginatorBDDForm;
	
		if(request.getSession().getAttribute(U_C35__PAGINATOR_B_D_D_FORM) != null){
			uC35_PaginatorBDDForm = (UC35_PaginatorBDDForm)request.getSession().getAttribute(U_C35__PAGINATOR_B_D_D_FORM);
		} else {
			uC35_PaginatorBDDForm = new UC35_PaginatorBDDForm();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form UC35_PaginatorBDDForm.");
		}
		uC35_PaginatorBDDForm = (UC35_PaginatorBDDForm)populateDestinationForm(request.getSession(), uC35_PaginatorBDDForm);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  UC35_PaginatorBDDForm.");
		}
		model.addAttribute(U_C35__PAGINATOR_B_D_D_FORM, uC35_PaginatorBDDForm);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param UC35_PaginatorBDDForm : The sceen form.
	 */
	@RequestMapping(value = "/UC35_PaginatorBDD.html" ,method = RequestMethod.GET)
	public void prepareUC35_PaginatorBDD(final HttpServletRequest request,final  Model model,final  @ModelAttribute("UC35_PaginatorBDDFormInit") UC35_PaginatorBDDForm uC35_PaginatorBDDForm){
		
		UC35_PaginatorBDDForm currentUC35_PaginatorBDDForm = uC35_PaginatorBDDForm;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(U_C35__PAGINATOR_B_D_D_FORM) == null){
			if(currentUC35_PaginatorBDDForm!=null){
				request.getSession().setAttribute(U_C35__PAGINATOR_B_D_D_FORM, currentUC35_PaginatorBDDForm);
			}else {
				currentUC35_PaginatorBDDForm = new UC35_PaginatorBDDForm();
				request.getSession().setAttribute(U_C35__PAGINATOR_B_D_D_FORM, currentUC35_PaginatorBDDForm);	
			}
		} else {
			currentUC35_PaginatorBDDForm = (UC35_PaginatorBDDForm) request.getSession().getAttribute(U_C35__PAGINATOR_B_D_D_FORM);
		}

		// SGBDR paged list.
		listPlayersTable(request, currentUC35_PaginatorBDDForm);
				currentUC35_PaginatorBDDForm = (UC35_PaginatorBDDForm)populateDestinationForm(request.getSession(), currentUC35_PaginatorBDDForm);
		// Call all the Precontroller.
	currentUC35_PaginatorBDDForm.setAlwaysCallPreControllers(true);
		model.addAttribute(U_C35__PAGINATOR_B_D_D_FORM, currentUC35_PaginatorBDDForm);
		request.getSession().setAttribute(U_C35__PAGINATOR_B_D_D_FORM, currentUC35_PaginatorBDDForm);
		
	}
	
	// Static field for the table using SGBDR pagination.
	private static final String ID_TABLE0 = "id_table0";
	
	/**
	 * Methode listPlayersTable to paginate the list named : listPlayers.
	 * @param request : The HTTPServletRequest.
	 * @param uC35_PaginatorBDDForm  : The current form.
	 */
	private void listPlayersTable(final HttpServletRequest request,final  UC35_PaginatorBDDForm uC35_PaginatorBDDForm) {
		// Find the table identifier in the request.
		String tmpParameter = request.getParameter((new ParamEncoder(ID_TABLE0).encodeParameterName(TableTagParameters.PARAMETER_PAGE)));
		// Check if null => this is the first time we access to this page.
		if (tmpParameter != null) {
			// Get the request parameter value.
			Integer currentPageIndex = (Integer.parseInt(tmpParameter) - 1);
			// Update the form.
			uC35_PaginatorBDDForm.updateListPlayersPageCommand(currentPageIndex);
		}
		// Populate the list in the form.
		put(request.getSession(), LIST_PLAYERS, uC35_PaginatorBDDForm.getListPlayers());
	}
			
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("UC35_PaginatorBDDController [ ");
			strBToS.append(LIST_PLAYERS);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(listPlayers);
			strBToS.append(END_TO_STRING_DELIMITER);
					// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(uC35_PaginatorBDDValidator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsUC35_PaginatorBDDController!=null); 
		return strBToS.toString();
	}
}
