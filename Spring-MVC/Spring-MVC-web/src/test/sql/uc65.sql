--<ScriptOptions statementTerminator=";"/>

ALTER TABLE professional65 DROP CONSTRAINT professional65_team65_fkc;

ALTER TABLE rookie65 DROP CONSTRAINT rookie65_team65_fkc;

ALTER TABLE professional65 DROP CONSTRAINT professional65_pkey;

ALTER TABLE rookie65 DROP CONSTRAINT rookie65_pkey;

ALTER TABLE team65 DROP CONSTRAINT team65_pkey;

DROP TABLE professional65;

DROP TABLE rookie65;

DROP TABLE team65;

CREATE TABLE professional65 (
		id INT8 NOT NULL,
		version INT4 NOT NULL,
		first_name VARCHAR(30) NOT NULL,
		last_name VARCHAR(30) NOT NULL,
		date_of_birth DATE,
		estimated_value FLOAT4,
		weight INT4,
		height INT4,
		salary INT4,
		team65_fk INT8 NOT NULL
	);

CREATE TABLE rookie65 (
		id INT8 NOT NULL,
		version INT4 NOT NULL,
		first_name VARCHAR(30) NOT NULL,
		last_name VARCHAR(30) NOT NULL,
		date_of_birth DATE,
		estimated_value FLOAT4,
		weight INT4,
		height INT4,
		university VARCHAR(1024),
		team65_fk INT8 NOT NULL
	);

CREATE TABLE team65 (
		id INT8 NOT NULL,
		version INT4 NOT NULL,
		city VARCHAR(1024),
		name VARCHAR(1024)
	);
	
INSERT INTO team65 (id, version, city, name) VALUES (1, 0, 'Charlotte', 'Tigers');
INSERT INTO team65 (id, version, city, name) VALUES (2, 0, 'Kansas City', 'Bears');
INSERT INTO team65 (id, version, city, name) VALUES (3, 0, 'Detroit', 'Chargers');
INSERT INTO team65 (id, version, city, name) VALUES (4, 0, 'Detroit', 'Hawks');
INSERT INTO team65 (id, version, city, name) VALUES (5, 0, 'Omaha', 'Braves');
INSERT INTO team65 (id, version, city, name) VALUES (6, 0, 'Raleigh', 'Eagles');
INSERT INTO team65 (id, version, city, name) VALUES (7, 0, 'Oakland', 'Cardinals');
INSERT INTO team65 (id, version, city, name) VALUES (8, 0, 'Kansas City', 'Red Devils');
INSERT INTO team65 (id, version, city, name) VALUES (9, 0, 'Fresno', 'Mustangs');
INSERT INTO team65 (id, version, city, name) VALUES (10, 0, 'Sacramento', 'Wolverines');

INSERT INTO professional65 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, salary, team65_fk) VALUES (201, 0, 'Oliver', 'Frank', '1968-10-31', 0.11, 116, 228, 87700, 1);
INSERT INTO professional65 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, salary, team65_fk) VALUES (202, 0, 'Elbert', 'Cole', '1965-04-07', 3.95, 100, 171, 74100, 2);
INSERT INTO professional65 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, salary, team65_fk) VALUES (203, 0, 'Abel', 'Dennis', '1980-04-16', 30.51, 105, 208, 61309, 3);
INSERT INTO professional65 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, salary, team65_fk) VALUES (204, 0, 'Ricky', 'Dawson', '1990-03-03', 29.299999, 115, 168, 13659, 4);
INSERT INTO professional65 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, salary, team65_fk) VALUES (205, 0, 'Earl', 'Russell', '1981-12-15', 44.73, 60, 234, 47198, 5);
INSERT INTO professional65 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, salary, team65_fk) VALUES (207, 0, 'Howard', 'Ramos', '1970-08-25', 10.76, 107, 199, 19465, 3);
INSERT INTO professional65 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, salary, team65_fk) VALUES (208, 0, 'Allan', 'Meyer', '1969-02-14', 33.669998, 114, 229, 49136, 1);
INSERT INTO professional65 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, salary, team65_fk) VALUES (209, 0, 'Ruben', 'Pope', '1985-05-01', 44.66, 86, 201, 42924, 5);
INSERT INTO professional65 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, salary, team65_fk) VALUES (210, 0, 'Lester', 'Cross', '1976-02-21', 30.98, 69, 214, 98034, 1);
INSERT INTO professional65 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, salary, team65_fk) VALUES (211, 0, 'Keith', 'Barnett', '1976-11-10', 5.8099999, 66, 190, 57315, 2);
INSERT INTO professional65 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, salary, team65_fk) VALUES (212, 0, 'Ivan', 'Tate', '1972-02-08', 48.490002, 143, 175, 38569, 3);
INSERT INTO professional65 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, salary, team65_fk) VALUES (213, 0, 'Tommy', 'Wood', '1992-01-20', 13.93, 77, 173, 48264, 4);
INSERT INTO professional65 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, salary, team65_fk) VALUES (214, 0, 'Sylvester', 'Nash', '1965-07-10', 37.73, 68, 184, 47496, 5);
INSERT INTO professional65 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, salary, team65_fk) VALUES (215, 0, 'Michael', 'Hodges', '1988-04-01', 25.35, 158, 204, 48643, 6);
INSERT INTO professional65 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, salary, team65_fk) VALUES (216, 0, 'Jerome', 'Grant', '1982-06-28', 28.940001, 105, 205, 94752, 7);
INSERT INTO professional65 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, salary, team65_fk) VALUES (217, 0, 'Woodrow', 'Carpenter', '1987-08-08', 41.759998, 69, 224, 44502, 8);
INSERT INTO professional65 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, salary, team65_fk) VALUES (218, 0, 'Roderick', 'Robbins', '1975-07-29', 12.04, 115, 164, 47613, 2);
INSERT INTO professional65 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, salary, team65_fk) VALUES (219, 0, 'Christian', 'Chavez', '1983-05-17', 37.880001, 97, 209, 57274, 9);
INSERT INTO professional65 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, salary, team65_fk) VALUES (206, 0, 'Nick', 'Garner', '1992-07-27', 47.84, 87, 221, 31846, 10);

INSERT INTO rookie65 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, university, team65_fk) VALUES (359, 0, 'Darren', 'Wolfe', '1966-12-25', 49.459999, 88, 215, NULL, 1);
INSERT INTO rookie65 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, university, team65_fk) VALUES (360, 0, 'Michael', 'Chambers', '1985-12-11', 2.9200001, 97, 230, NULL, 2);
INSERT INTO rookie65 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, university, team65_fk) VALUES (361, 0, 'Gilbert', 'Mckinney', '1969-11-15', 13.19, 141, 178, NULL, 3);
INSERT INTO rookie65 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, university, team65_fk) VALUES (362, 0, 'Lamar', 'Hansen', '1985-04-10', 3.5599999, 102, 227, NULL, 4);
INSERT INTO rookie65 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, university, team65_fk) VALUES (363, 0, 'Michael', 'Franklin', '1973-11-28', 19.5, 146, 199, NULL, 5);
INSERT INTO rookie65 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, university, team65_fk) VALUES (364, 0, 'Norman', 'Osborne', '1975-09-04', 37.060001, 92, 168, NULL, 6);
INSERT INTO rookie65 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, university, team65_fk) VALUES (365, 0, 'Derrick', 'Holt', '1969-06-20', 34.400002, 127, 216, NULL, 1);
INSERT INTO rookie65 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, university, team65_fk) VALUES (366, 0, 'Clayton', 'Bishop', '1970-05-09', 22.389999, 60, 189, NULL, 2);
INSERT INTO rookie65 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, university, team65_fk) VALUES (367, 0, 'Jean', 'Walton', '1983-12-26', 2.9300001, 108, 220, NULL, 3);
INSERT INTO rookie65 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, university, team65_fk) VALUES (368, 0, 'Nelson', 'Hernandez', '1983-10-21', 37.880001, 110, 206, NULL, 4);
INSERT INTO rookie65 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, university, team65_fk) VALUES (369, 0, 'Lucas', 'Obrien', '1969-08-16', 44.02, 69, 179, NULL, 5);
INSERT INTO rookie65 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, university, team65_fk) VALUES (370, 0, 'Dominick', 'Burton', '1984-03-05', 32.529999, 108, 184, NULL, 6);
INSERT INTO rookie65 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, university, team65_fk) VALUES (371, 0, 'Damon', 'Richardson', '1967-12-05', 33.990002, 78, 223, NULL, 7);
INSERT INTO rookie65 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, university, team65_fk) VALUES (372, 0, 'Grant', 'Gardner', '1979-08-03', 31.92, 122, 225, 'FALSE', 8);
INSERT INTO rookie65 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, university, team65_fk) VALUES (373, 0, 'Garrett', 'Townsend', '1978-07-01', 7.25, 60, 175, 'FALSE', 9);
INSERT INTO rookie65 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, university, team65_fk) VALUES (374, 0, 'Bryant', 'Hodges', '1970-02-27', 4.6700001, 111, 237, 'FALSE', 10);


ALTER TABLE professional65 ADD CONSTRAINT professional65_pkey PRIMARY KEY (id);

ALTER TABLE rookie65 ADD CONSTRAINT rookie65_pkey PRIMARY KEY (id);

ALTER TABLE team65 ADD CONSTRAINT team65_pkey PRIMARY KEY (id);

ALTER TABLE professional65 ADD CONSTRAINT professional65_team65_fkc FOREIGN KEY (team65_fk)
	REFERENCES team65 (id);

ALTER TABLE rookie65 ADD CONSTRAINT rookie65_team65_fkc FOREIGN KEY (team65_fk)
	REFERENCES team65 (id);

