<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file="/include.jsp"  %>
<html >
<head >
<title  > 
<fmt:message key="cUC03_FormValidation_BLU_AGE_DOCUMENTATION"/>
</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"  > 
</meta>
<link href="<c:url value= "/presentation/_style/_style.css"/>" rel="stylesheet" type="text/css"  /> 
<link href="<c:url value= "/presentation/_include/jquery/rd_lib.css"/>" rel="stylesheet" type="text/css"  /> 
<script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/jquery.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/rd_setup.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/rd_lib.js"/>" space="preserve"  ></script><link rel="shortcut icon" href="<c:url value= "/presentation/_style/favicon.ico"/>"  /> 

</head>

<body class=" " > 
<div id="rd_ghost"  > 
<small  > 
<fmt:message key="cUC03_FormValidation___CSS_DEACTIVATED"/>
</small>
</div>
<div id="rd_jghost"  > 
<small  > 
<fmt:message key="cUC03_FormValidation___JAVASCRIPT_DEACTIVATED"/>
</small>
</div>
<div id="shape"  > 
<div class="ui-layout-center"  > 
<div class="east-north"  > 
	<form:form id="uC03_FormValidation" commandName="uC03_FormValidationForm"  enctype="application/x-www-form-urlencoded" method="post">
<h2 class="rd_first"  > 
<fmt:message key="cUC03_FormValidation_GETTING_STARTED__UC03__FORM_"/>
</h2>
<table class="rd_tableform"  >
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC03_FormValidation_FIRST_NAME_REQUIREDFIELDVALID"/>
</label>
<label style="background: none;color: red;"  > 
<fmt:message key="cUC03_FormValidation_"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
<form:input id="txt_firstname" path="playerToCreate.firstName"  size="30" 
		/>
</td> 
<td rowspan="1" colspan="1"  >
<form:errors path="playerToCreate.firstName"/>
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC03_FormValidation_LAST_NAME_REQUIREDFIELDVALIDA"/>
</label>
<label style="background: none;color: red;"  > 
<fmt:message key="cUC03_FormValidation__47"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
<form:input id="txt_lastname" path="playerToCreate.lastName"  size="30" 
		/>
</td> 
<td rowspan="1" colspan="1"  >
<form:errors path="playerToCreate.lastName"/>
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC03_FormValidation_DATE_OF_BIRTH_MMDDYYYY"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
<form:input id="txt_dateofbirth" path="playerToCreate.dateOfBirth"  size="12" 
		/>
</td> 
<td rowspan="1" colspan="1"  >
<form:errors path="playerToCreate.dateOfBirth"/>
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC03_FormValidation_ESTIMATED_VALUE_CUSTOMVALIDAT"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
<form:input id="txt_estimatedvalue" path="playerToCreate.estimatedValue"  size="12" 
		/>
<fmt:message key="cUC03_FormValidation_M"/>
</td> 
<td rowspan="1" colspan="1"  >
	<div id="estimatedvalue_format"  style="display:none;" >
			<c:out value="The Estimated Value should be a Number" />
	</div>

       <script type="text/javascript">
       function validateValue(playerValue) {
        reg = new RegExp("^[0-9]+[\.]?[0-9]*$","");
        return playerValue=="" || reg.test(playerValue);
       }       
       </script>
      
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC03_FormValidation_WEIGHT_RANGEVALIDATOR_60140K"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
<form:input id="txt_weight" path="playerToCreate.weight"  size="12" 
		/>
<fmt:message key="cUC03_FormValidation_KG"/>
</td> 
<td rowspan="1" colspan="1"  >
<form:errors path="playerToCreate.weight"/>
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC03_FormValidation_HEIGHT_RANGEVALIDATOR_150240"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
<form:input id="txt_height" path="playerToCreate.height"  size="3" 
		/>
<fmt:message key="cUC03_FormValidation_CM"/>
</td> 
<td rowspan="1" colspan="1"  >
<form:errors path="playerToCreate.height"/>
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC03_FormValidation_POSITION"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
<form:select id="slt_position" path="playerToCreate.position.code"   
		>
		<form:options items="${uC03_FormValidationForm.allPositions}" itemValue="code" itemLabel="name" />
</form:select>
</td> 
<td rowspan="1" colspan="1"  >
<fmt:message key="cUC03_FormValidation__48"/>
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC03_FormValidation_ROOKIE"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
<form:checkbox id="chk_rookie" path="playerToCreate.rookie"   
		/>
</td> 
<td rowspan="1" colspan="1"  >
<fmt:message key="cUC03_FormValidation__49"/>
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<fmt:message key="cUC03_FormValidation__50"/>
</td> 
<td rowspan="1" colspan="1"  >
<a id="lnk_create"onclick="javascript:btn_click_cExecuteUC03_FormValidation(this.href);return false;"				href="<c:url value="/presentation/gettingstarted/uc03_formvalidation/UC03_FormValidation/lnk_create.html"/>"
							 shape="rect"  >									 
<fmt:message key="cUC03_FormValidation_CREATE_PLAYER"/>
</a>
</td> 
<td rowspan="1" colspan="1"  >
<fmt:message key="cUC03_FormValidation__51"/>
</td> 
</tr>
</table> 
	</form:form>
	<script language="javascript">
		function btn_click_cExecuteUC03_FormValidation(url) {
			if(validateValue(document.getElementById("txt_estimatedvalue").value)==false) {
				document.getElementById("estimatedvalue_format").style.display="block";
				return false;
			}
			UC03_FormValidationvar = document.getElementById('uC03_FormValidation');
			UC03_FormValidationvar.setAttribute('action', url);
			UC03_FormValidationvar.submit();
		}
		function btn_click_cSetActionUC03_FormValidation(url) {
			UC03_FormValidationvar = document.getElementById('uC03_FormValidation');
			UC03_FormValidationvar.setAttribute('action', url);
		}
	</script>
<label style="background: none;color: red;"  > 
<fmt:message key="cUC03_FormValidation__52"/>
</label>
<fmt:message key="cUC03_FormValidation_REQUIRED_ENTRY_____"/>
</div>
<div class="east-center"  > 
<tiles:insertDefinition	name="presentation/gettingstarted/uc03_formvalidation/UC03_FormValidationcmp_tabs" />
</div>
</div>
<div class="ui-layout-north"  > 
<tiles:insertDefinition	name="presentation/gettingstarted/uc03_formvalidation/UC03_FormValidationcmp_header" />
</div>
<div class="ui-layout-south"  > 
<fmt:message key="cUC03_FormValidation_______"/>
<span id="rd_jversion"  > 
<fmt:message key="cUC03_FormValidation_BLU_AGE_DOCUMENTATION_53"/>
</span>
</div>
<div class="ui-layout-west"  > 
<tiles:insertDefinition	name="presentation/gettingstarted/uc03_formvalidation/UC03_FormValidationcmp_treeview" />
</div>
</div>
</body>
</html>
