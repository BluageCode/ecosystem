/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.business.common.entities.daofinder;


import javax.inject.Named;

import com.bluage.documentation.business.AbstractDaoFinderImpl;
import com.bluage.documentation.business.common.bos.TeamBO;

/**
 * This class represents the implementation of 
 * the search methods for the entity {@link Team}.
 *
 * @see		AbstractDaoFinderImpl
 */
 
@Named ("com.bluage.documentation.business.common.entities.daofinder.TeamDAOFinderImpl") 
public class TeamDAOFinderImpl extends AbstractDaoFinderImpl<TeamBO>{
	
   /**
	* Default order
	*/
	private static final String DEFAULT_ORDER ="";

	/**
	* The associated BO 
	*/
	private final Class<TeamBO> refBo = com.bluage.documentation.business.common.bos.TeamBO.class;
	

	/**
	* Returns the associated BO
	* @return Class<TeamBO>
	*/
	@Override
	public final Class<TeamBO> getRefBo(){
		return this.refBo;
	}
	
	/**
	* Returns the default order by instruction
	* @return String
	*/
	@Override
	public final String getDefaultOrder(){
		return this.DEFAULT_ORDER;
	}
}
