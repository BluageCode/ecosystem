/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.netfective.bluage.core.exception;

/**
 * Class DBConstraintViolationException
 * @superclass TechnicalException
 * @author NETFECTIVE TECHNOLOGY
 */
public class DBConstraintViolationException extends TechnicalException {

	/**
	 * serial UID
	 */
	private static final long serialVersionUID = -5194511226181721502L;
	

	/**
	 * Default constructor of ApplicationException.
	 */
	public DBConstraintViolationException() {
		super();
	}

	// Constructors who do not take the name of the class and the method where exception occurred.

	/**
	 * Constructor of ApplicationException with nested exception.
	 * 
	 * @param innerException	Nested exception.
	 */
	public DBConstraintViolationException(final Exception innerException) {
		super(innerException);
	}

	/**
	 * Constructor of ApplicationException with the exception message and nested exception.
	 * 
	 * @param mess				The exception message 
	 * @param innerException	Nested exception.
	 */
	public DBConstraintViolationException(final String mess, final Exception innerException) {
		super(mess, innerException);
	}
	
	/**
	 * Constructor of ApplicationException with the exception message and nested exception.
	 * 
	 * @param mess				The exception message 
	 * @param innerException	Nested exception.
	 */
	public DBConstraintViolationException(final String mess, final Throwable innerException) {
		super(mess, innerException);
	}

	// Constructors that take parameters as the class name and the method where the exception occurred. 
	// They are used in the case when using an API that has no exception in signing and dating a 
	// RuntimeException, in this case it is not about to have the class and method in the stack.
	
	/**
	 * Constructor of ApplicationException without exception message.
	 * 
	 * @param className		Class name where an exception occured
	 * @param methodName	Method name where an exception occured
	 */
	public DBConstraintViolationException(final String className, final String methodName) {
		super(className, methodName);
	}

	/**
	 * Constructor of ApplicationException with exception message
	 * 
	 * @param mess			Exception message
	 * @param className		Class name where an exception occured
	 * @param methodName	Method name where an exception occured
	 */
	public DBConstraintViolationException(final String mess, final String className, final String methodName) {
		super(mess, className, methodName);
	}

	/**
	 * Constructor of ApplicationException with nested exception.
	 * 
	 * @param className			Class name where an exception occured
	 * @param methodName		Method name where an exception occured
	 * @param innerException	Nested exception
	 */
	public DBConstraintViolationException(final String className, final String methodName, final Exception innerException) {
		super(className, methodName, innerException);	
	}

	/**
	 * Constructor ApplicationException with the exception message and nested exception
	 * 
	 * @param mess				Exception message
	 * @param className			Class name where an exception occured
	 * @param methodName		Method name where an exception occured
	 * @param innerException	Nested exception
	 */
	public DBConstraintViolationException(final String mess, final String className, final String methodName, final Exception innerException) {
		super(mess, className, methodName, innerException);	
	}
	
	/**
	 * Constructor of DBConstraintViolationException with message.
	 * 
	 * @param message.
	 */
	public DBConstraintViolationException(final String message) {
		super(message);
	}
}
