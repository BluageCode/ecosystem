--<ScriptOptions statementTerminator=";"/>

ALTER TABLE player200 DROP CONSTRAINT player200_stats_fkc;

ALTER TABLE stats200 DROP CONSTRAINT stats200_pkey;

ALTER TABLE player200 DROP CONSTRAINT player200_pkey;

DROP TABLE player200;

DROP TABLE stats200;

CREATE TABLE player200 (
		id INT8 NOT NULL,
		version INT4 NOT NULL,
		first_name VARCHAR(1024),
		last_name VARCHAR(1024),
		stats_fk INT8 NOT NULL
	);

CREATE TABLE stats200 (
		id INT8 NOT NULL,
		version INT4 NOT NULL,
		shoots INT4,
		plays INT4
	);
	
INSERT INTO stats200 (id, version, shoots, plays) VALUES (201, 0, 12, 32);
INSERT INTO stats200 (id, version, shoots, plays) VALUES (202, 0, 3, 12);
INSERT INTO stats200 (id, version, shoots, plays) VALUES (203, 0, 1, 20);
INSERT INTO stats200 (id, version, shoots, plays) VALUES (204, 0, 20, 21);
INSERT INTO stats200 (id, version, shoots, plays) VALUES (205, 0, 3, 12);
INSERT INTO stats200 (id, version, shoots, plays) VALUES (206, 0, 8, 1);
INSERT INTO stats200 (id, version, shoots, plays) VALUES (207, 0, 21, 32);
INSERT INTO stats200 (id, version, shoots, plays) VALUES (208, 0, 10, 20);
INSERT INTO stats200 (id, version, shoots, plays) VALUES (209, 0, 20, 10);
INSERT INTO stats200 (id, version, shoots, plays) VALUES (210, 0, 18, 27);
INSERT INTO stats200 (id, version, shoots, plays) VALUES (211, 0, 21, 20);
INSERT INTO stats200 (id, version, shoots, plays) VALUES (212, 0, 2, 9);
INSERT INTO stats200 (id, version, shoots, plays) VALUES (213, 0, 19, 9);
INSERT INTO stats200 (id, version, shoots, plays) VALUES (214, 0, 28, 10);
INSERT INTO stats200 (id, version, shoots, plays) VALUES (215, 0, 40, 13);
INSERT INTO stats200 (id, version, shoots, plays) VALUES (216, 0, 20, 20);
INSERT INTO stats200 (id, version, shoots, plays) VALUES (217, 0, 83, 32);
INSERT INTO stats200 (id, version, shoots, plays) VALUES (218, 0, 21, 10);
INSERT INTO stats200 (id, version, shoots, plays) VALUES (219, 0, 82, 24);
INSERT INTO stats200 (id, version, shoots, plays) VALUES (220, 0, 72, 15);

INSERT INTO player200 (id, version, first_name, last_name, stats_fk) VALUES (201, 0, 'Oliver', 'Frank', 201);
INSERT INTO player200 (id, version, first_name, last_name, stats_fk) VALUES (202, 0, 'Elbert', 'Cole', 202);
INSERT INTO player200 (id, version, first_name, last_name, stats_fk) VALUES (203, 0, 'Abel', 'Dennis', 203);
INSERT INTO player200 (id, version, first_name, last_name, stats_fk) VALUES (204, 0, 'Ricky', 'Dawson', 204);
INSERT INTO player200 (id, version, first_name, last_name, stats_fk) VALUES (205, 0, 'Earl', 'Russell', 205);
INSERT INTO player200 (id, version, first_name, last_name, stats_fk) VALUES (206, 0, 'Howard', 'Ramos', 206);
INSERT INTO player200 (id, version, first_name, last_name, stats_fk) VALUES (207, 0, 'Allan', 'Meyer', 207);
INSERT INTO player200 (id, version, first_name, last_name, stats_fk) VALUES (208, 0, 'Ruben', 'Pope', 208);
INSERT INTO player200 (id, version, first_name, last_name, stats_fk) VALUES (209, 0, 'Lester', 'Cross', 209);
INSERT INTO player200 (id, version, first_name, last_name, stats_fk) VALUES (210, 0, 'Keith', 'Barnett', 210);
INSERT INTO player200 (id, version, first_name, last_name, stats_fk) VALUES (211, 0, 'Ivan', 'Tate', 211);
INSERT INTO player200 (id, version, first_name, last_name, stats_fk) VALUES (212, 0, 'Sylvester', 'Nash', 212);
INSERT INTO player200 (id, version, first_name, last_name, stats_fk) VALUES (213, 0, 'Michael', 'Hodges', 213);
INSERT INTO player200 (id, version, first_name, last_name, stats_fk) VALUES (214, 0, 'Jerome', 'Grant', 214);
INSERT INTO player200 (id, version, first_name, last_name, stats_fk) VALUES (215, 0, 'Woodrow', 'Carpenter', 215);
INSERT INTO player200 (id, version, first_name, last_name, stats_fk) VALUES (216, 0, 'Roderick', 'Robbins', 216);
INSERT INTO player200 (id, version, first_name, last_name, stats_fk) VALUES (217, 0, 'Christian', 'Chavez', 217);
INSERT INTO player200 (id, version, first_name, last_name, stats_fk) VALUES (218, 0, 'Cecil', 'Conner', 218);
INSERT INTO player200 (id, version, first_name, last_name, stats_fk) VALUES (219, 0, 'Stephen', 'Lambert', 219);
INSERT INTO player200 (id, version, first_name, last_name, stats_fk) VALUES (220, 0, 'Gustavo', 'Romero', 220);


ALTER TABLE stats200 ADD CONSTRAINT stats200_pkey PRIMARY KEY (id);

ALTER TABLE player200 ADD CONSTRAINT player200_pkey PRIMARY KEY (id);

ALTER TABLE player200 ADD CONSTRAINT player200_stats_fkc FOREIGN KEY (stats_fk)
	REFERENCES stats200 (id);

