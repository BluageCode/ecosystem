--<ScriptOptions statementTerminator=";"/>

ALTER TABLE player35 DROP CONSTRAINT player35_team_fkc;

ALTER TABLE player35 DROP CONSTRAINT player35_team35_fkc;

ALTER TABLE team35 DROP CONSTRAINT team35_pkey;

ALTER TABLE position35 DROP CONSTRAINT position35_pkey;

ALTER TABLE player35 DROP CONSTRAINT player35_pkey;

DROP TABLE player35;

DROP TABLE team35;

DROP TABLE position35;

CREATE TABLE player35 (
		id INT8 NOT NULL,
		version INT4 NOT NULL,
		first_name VARCHAR(30) NOT NULL,
		last_name VARCHAR(30) NOT NULL,
		date_of_birth DATE,
		estimated_value FLOAT4,
		weight INT4,
		height INT4,
		rookie BOOL,
		team35_fk INT8,
		idx INT4,
		team_fk INT8,
		team35_player35_idx INT4,
		team35_player35__idx INT4
	);

CREATE TABLE team35 (
		id INT8 NOT NULL,
		version INT4 NOT NULL,
		city VARCHAR(1024),
		name VARCHAR(1024)
	);

CREATE TABLE position35 (
		code VARCHAR(2) NOT NULL,
		version INT4 NOT NULL,
		name VARCHAR(20) NOT NULL
	);

INSERT INTO team35 VALUES (21, 0, 'New York', 'Tigers');
INSERT INTO team35 VALUES (1, 1, 'Charlottes', 'Tigers');
INSERT INTO team35 VALUES (2, 1, 'Kansas Toto', 'Bears');
INSERT INTO team35 VALUES (4, 0, 'Detroit', 'Hawks');
INSERT INTO team35 VALUES (5, 0, 'Omaha', 'Braves');

	
INSERT INTO player35 VALUES (235, 0, 'Ryan', 'Harmon', '1991-06-09', 9.8800001, 121, 155, true, 5, 5, 5, NULL, NULL);
INSERT INTO player35 VALUES (201, 0, 'Harry', 'Davis', '1988-04-14', 9.6899996, 159, 237, false, 21, 21, 21, NULL, NULL);
INSERT INTO player35 VALUES (202, 0, 'Loren', 'Colon', '1971-12-14', 30.139999, 149, 221, false, 1, 1, 1, NULL, NULL);
INSERT INTO player35 VALUES (203, 0, 'Bob', 'Clark', '1972-09-04', 24.02, 105, 224, false, 1, 1, 1, NULL, NULL);
INSERT INTO player35 VALUES (204, 0, 'Gerald', 'Fox', '1991-12-23', 1.7, 72, 193, true, 1, 1, 1, NULL, NULL);
INSERT INTO player35 VALUES (205, 0, 'Louis', 'Stokes', '1976-07-17', 30.969999, 105, 186, false, 1, 1, 1, NULL, NULL);
INSERT INTO player35 VALUES (206, 0, 'Cameron', 'Horton', '1992-11-22', 13.26, 131, 236, true, 1, 1, 1, NULL, NULL);
INSERT INTO player35 VALUES (207, 0, 'Shane', 'Sandoval', '1975-09-24', 24.139999, 131, 182, false, 1, 1, 1, NULL, NULL);
INSERT INTO player35 VALUES (208, 0, 'Roosevelt', 'Doyle', '1983-11-10', 8.5500002, 114, 235, false, 1, 1, 1, NULL, NULL);
INSERT INTO player35 VALUES (209, 0, 'Shane', 'Daniel', '1990-02-28', 4.54, 125, 195, true, 2, 2, 2, NULL, NULL);
INSERT INTO player35 VALUES (210, 0, 'Alton', 'Harris', '1970-02-13', 20.34, 120, 196, false, 2, 2, 2, NULL, NULL);
INSERT INTO player35 VALUES (211, 0, 'Sammy', 'Clark', '1987-05-05', 14, 152, 156, false, 2, 2, 2, NULL, NULL);
INSERT INTO player35 VALUES (212, 0, 'Johnathan', 'Rodgers', '1983-02-27', 10.21, 115, 210, false, 2, 2, 2, NULL, NULL);
INSERT INTO player35 VALUES (213, 0, 'Jordan', 'Vaughn', '1982-08-22', 14.67, 122, 230, false, 2, 2, 2, NULL, NULL);
INSERT INTO player35 VALUES (236, 0, 'Tim', 'Collier', '1986-06-16', 2.1900001, 69, 194, false, 5, 5, 5, NULL, NULL);
INSERT INTO player35 VALUES (237, 0, 'Dominic', 'Barrett', '1984-02-29', 20.610001, 152, 240, false, 5, 5, 5, NULL, NULL);
INSERT INTO player35 VALUES (214, 0, 'Omar', 'Woods', '1974-04-26', 48.93, 111, 195, false, 2, 2, 2, NULL, NULL);
INSERT INTO player35 VALUES (215, 0, 'Devin', 'Reeves', '1974-02-25', 32.310001, 82, 194, false, 2, 2, 2, NULL, NULL);
INSERT INTO player35 VALUES (216, 0, 'Ralph', 'Hampton', '1992-02-29', 45.970001, 114, 196, true, 4, 4, 4, NULL, NULL);
INSERT INTO player35 VALUES (217, 0, 'Charles', 'Dunn', '1971-11-06', 32.32, 157, 200, false, 4, 4, 4, NULL, NULL);
INSERT INTO player35 VALUES (218, 0, 'Simon', 'Greene', '1975-07-04', 43.389999, 126, 163, false, 4, 4, 4, NULL, NULL);
INSERT INTO player35 VALUES (219, 0, 'Isaac', 'Austin', '1971-01-03', 31.129999, 123, 174, false, 4, 4, 4, NULL, NULL);
INSERT INTO player35 VALUES (220, 0, 'Raul', 'Gray', '1986-04-14', 22.450001, 119, 218, false, 4, 4, 4, NULL, NULL);
INSERT INTO player35 VALUES (221, 0, 'Bruce', 'Robinson', '1984-03-07', 40, 138, 193, false, 4, 4, 4, NULL, NULL);
INSERT INTO player35 VALUES (222, 0, 'David', 'Goodman', '1965-10-29', 32.27, 108, 152, false, 4, 4, 4, NULL, NULL);
INSERT INTO player35 VALUES (223, 0, 'Larry', 'Bradley', '1977-04-20', 40.84, 117, 213, false, 4, 4, 4, NULL, NULL);
INSERT INTO player35 VALUES (224, 0, 'Nicholas', 'Powell', '1983-11-08', 34.93, 139, 152, false, 4, 4, 4, NULL, NULL);
INSERT INTO player35 VALUES (225, 0, 'Zachary', 'Brewer', '1989-10-22', 31.299999, 108, 158, false, 4, 4, 4, NULL, NULL);
INSERT INTO player35 VALUES (226, 0, 'Tyrone', 'Weber', '1981-12-08', 31.9, 140, 157, false, 2, 2, 2, NULL, NULL);
INSERT INTO player35 VALUES (227, 0, 'Ed', 'Young', '1969-04-07', 19.950001, 128, 219, false, 2, 2, 2, NULL, NULL);
INSERT INTO player35 VALUES (228, 0, 'Thomas', 'Copeland', '1974-04-06', 24.83, 113, 188, false, 2, 2, 2, NULL, NULL);
INSERT INTO player35 VALUES (229, 0, 'Lamar', 'Lopez', '1979-05-15', 27.209999, 78, 180, false, 5, 2, 2, NULL, NULL);
INSERT INTO player35 VALUES (230, 0, 'Troy', 'Stone', '1979-08-24', 42.52, 65, 169, false, 5, 5, 5, NULL, NULL);
INSERT INTO player35 VALUES (231, 0, 'Arnold', 'Ford', '1976-06-02', 48.240002, 79, 225, false, 5, 5, 5, NULL, NULL);
INSERT INTO player35 VALUES (232, 0, 'Stuart', 'Munoz', '1973-04-25', 3.1300001, 132, 192, false, 5, 5, 5, NULL, NULL);
INSERT INTO player35 VALUES (233, 0, 'Drew', 'Nash', '1992-11-27', 33.529999, 86, 218, true, 5, 5, 5, NULL, NULL);
INSERT INTO player35 VALUES (234, 0, 'Grant', 'Hudson', '1967-04-02', 9.6499996, 66, 172, false, 5, 5, 5, NULL, NULL);
INSERT INTO player35 VALUES (238, 0, 'Joel', 'Love', '1968-10-07', 6.2600002, 66, 191, false, 5, 5, 5, NULL, NULL);
INSERT INTO player35 VALUES (239, 0, 'Jared', 'Mullins', '1981-07-09', 0.13, 143, 189, false, 5, 5, 5, NULL, NULL);

ALTER TABLE team35 ADD CONSTRAINT team35_pkey PRIMARY KEY (id);

ALTER TABLE position35 ADD CONSTRAINT position35_pkey PRIMARY KEY (code);

ALTER TABLE player35 ADD CONSTRAINT player35_pkey PRIMARY KEY (id);

ALTER TABLE player35 ADD CONSTRAINT player35_team_fkc FOREIGN KEY (team_fk)
	REFERENCES team35 (id);

ALTER TABLE player35 ADD CONSTRAINT player35_team35_fkc FOREIGN KEY (team35_fk)
	REFERENCES team35 (id);