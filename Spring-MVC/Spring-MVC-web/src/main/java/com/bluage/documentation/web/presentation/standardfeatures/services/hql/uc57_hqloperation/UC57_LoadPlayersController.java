/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.services.hql.uc57_hqloperation ;

// Import declaration.
// Java imports.
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.bluage.documentation.business.standardfeatures.services.hql.uc57_hqloperation.bos.Team57BO;
import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;

/**
 * Class : UC57_LoadPlayersController
 */
@Controller
@Scope("prototype")
@SessionAttributes("uC57_LoadPlayersForm")
@RequestMapping(value= "/presentation/standardfeatures/services/hql/uc57_hqloperation")
public class UC57_LoadPlayersController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC57_LoadPlayersController.class);
	
	//Current form name
	private static final String U_C57__LOAD_PLAYERS_FORM = "uC57_LoadPlayersForm";

	//CONSTANT: team
	private static final String TEAM = "team";
		//CONSTANT: players table or repeater.
	private static final String PLAYERS = "players";
				
	/**
	 * Property:customDateEditorsUC57_LoadPlayersController
	 */
	final private Map<String,CustomDateEditor> customDateEditorsUC57_LoadPlayersController = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:uC57_LoadPlayersValidator
	 */
	final private UC57_LoadPlayersValidator uC57_LoadPlayersValidator = new UC57_LoadPlayersValidator();
	
	
	// Initialise all the instances for UC57_LoadPlayersController
		// Initialize the instance team of type com.bluage.documentation.business.standardfeatures.services.hql.uc57_hqloperation.bos.Team57BO with the value : null for UC57_LoadPlayersController
		private Team57BO team;
			// Initialize the instance players for the state lnk_loadPlayers
		private List players; // Initialize the instance players for UC57_LoadPlayersController
				
	/**
	* This method initialise the form : UC57_LoadPlayersForm 
	* @return UC57_LoadPlayersForm
	*/
	@ModelAttribute("UC57_LoadPlayersFormInit")
	public void initUC57_LoadPlayersForm(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, U_C57__LOAD_PLAYERS_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method UC57_LoadPlayersForm."); //for lnk_loadPlayers 
		}
		UC57_LoadPlayersForm uC57_LoadPlayersForm;
	
		if(request.getSession().getAttribute(U_C57__LOAD_PLAYERS_FORM) != null){
			uC57_LoadPlayersForm = (UC57_LoadPlayersForm)request.getSession().getAttribute(U_C57__LOAD_PLAYERS_FORM);
		} else {
			uC57_LoadPlayersForm = new UC57_LoadPlayersForm();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form UC57_LoadPlayersForm.");
		}
		uC57_LoadPlayersForm = (UC57_LoadPlayersForm)populateDestinationForm(request.getSession(), uC57_LoadPlayersForm);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  UC57_LoadPlayersForm.");
		}
		model.addAttribute(U_C57__LOAD_PLAYERS_FORM, uC57_LoadPlayersForm);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param UC57_LoadPlayersForm : The sceen form.
	 */
	@RequestMapping(value = "/UC57_LoadPlayers.html" ,method = RequestMethod.GET)
	public void prepareUC57_LoadPlayers(final HttpServletRequest request,final  Model model,final  @ModelAttribute("UC57_LoadPlayersFormInit") UC57_LoadPlayersForm uC57_LoadPlayersForm){
		
		UC57_LoadPlayersForm currentUC57_LoadPlayersForm = uC57_LoadPlayersForm;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(U_C57__LOAD_PLAYERS_FORM) == null){
			if(currentUC57_LoadPlayersForm!=null){
				request.getSession().setAttribute(U_C57__LOAD_PLAYERS_FORM, currentUC57_LoadPlayersForm);
			}else {
				currentUC57_LoadPlayersForm = new UC57_LoadPlayersForm();
				request.getSession().setAttribute(U_C57__LOAD_PLAYERS_FORM, currentUC57_LoadPlayersForm);	
			}
		} else {
			currentUC57_LoadPlayersForm = (UC57_LoadPlayersForm) request.getSession().getAttribute(U_C57__LOAD_PLAYERS_FORM);
		}

				currentUC57_LoadPlayersForm = (UC57_LoadPlayersForm)populateDestinationForm(request.getSession(), currentUC57_LoadPlayersForm);
		// Call all the Precontroller.
	currentUC57_LoadPlayersForm.setAlwaysCallPreControllers(true);
		model.addAttribute(U_C57__LOAD_PLAYERS_FORM, currentUC57_LoadPlayersForm);
		request.getSession().setAttribute(U_C57__LOAD_PLAYERS_FORM, currentUC57_LoadPlayersForm);
		
	}
	
			
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("UC57_LoadPlayersController [ ");
			strBToS.append(TEAM);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(team);
			strBToS.append(END_TO_STRING_DELIMITER);
				strBToS.append(PLAYERS);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(players);
			strBToS.append(END_TO_STRING_DELIMITER);
					// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(uC57_LoadPlayersValidator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsUC57_LoadPlayersController!=null); 
		return strBToS.toString();
	}
}
