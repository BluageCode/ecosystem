/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.application.navigation.uc28_packagescope.package_one;


import java.io.Serializable;
import java.util.List;

import com.bluage.documentation.business.standardfeatures.application.navigation.uc28_packagescope.bos.Team28BO;
import com.netfective.bluage.core.web.AbstractForm;
import com.netfective.bluage.core.web.IForm;

/**
* Class : UC28_TeamsForm
*/
public class UC28_TeamsForm extends AbstractForm implements Serializable, IForm {
	
	/** Constant : serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	//CONSTANT : allStates
	private static final String ALL_STATES = "allStates";
	//CONSTANT : allTeams
	private static final String ALL_TEAMS = "allTeams";
	//CONSTANT : allStadiums
	private static final String ALL_STADIUMS = "allStadiums";
	/**
	 * 	Property: allStates 
	 */
	private List allStates;
	/**
	 * 	Property: allTeams 
	 */
	private List<Team28BO> allTeams;
	/**
	 * 	Property: allStadiums 
	 */
	private List allStadiums;
/**
	 * Default constructor : UC28_TeamsForm
	 */
	public UC28_TeamsForm() {
		super();
		// Initialize : allStates
		this.allStates = null;
		// Initialize : allTeams
		this.allTeams = new java.util.ArrayList<com.bluage.documentation.business.standardfeatures.application.navigation.uc28_packagescope.bos.Team28BO>();
		// Initialize : allStadiums
		this.allStadiums = null;
					this.setAlwaysCallPreControllers(true);
	}
	
	/**
	 * 	Getter : allStates 
	 * 	@return : Return the allStates instance.
	 */
	public List getAllStates(){
		return allStates; // For UC28_TeamsForm
	}
	
	/**
	 * 	Setter : allStates 
	 *  @param allStatesinstance : The instance to set.
	 */
	public void setAllStates(final List allStatesinstance){
		this.allStates = allStatesinstance;// For UC28_TeamsForm
	}
	/**
	 * 	Getter : allTeams 
	 * 	@return : Return the allTeams instance.
	 */
	public List<Team28BO> getAllTeams(){
		return allTeams; // For UC28_TeamsForm
	}
	
	/**
	 * 	Setter : allTeams 
	 *  @param allTeamsinstance : The instance to set.
	 */
	public void setAllTeams(final List<Team28BO> allTeamsinstance){
		this.allTeams = allTeamsinstance;// For UC28_TeamsForm
	}
	/**
	 * 	Getter : allStadiums 
	 * 	@return : Return the allStadiums instance.
	 */
	public List getAllStadiums(){
		return allStadiums; // For UC28_TeamsForm
	}
	
	/**
	 * 	Setter : allStadiums 
	 *  @param allStadiumsinstance : The instance to set.
	 */
	public void setAllStadiums(final List allStadiumsinstance){
		this.allStadiums = allStadiumsinstance;// For UC28_TeamsForm
	}
/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UC28_TeamsForm [ "+
ALL_STATES +" = " + allStates +ALL_TEAMS +" = " + allTeams +ALL_STADIUMS +" = " + allStadiums + "]";
	}
	
	/**
	 * Method setInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public void setInstanceByInstanceName(final String instanceName, final Object instance) {
		// Set the instance AllStates.
		if(ALL_STATES.equals(instanceName)){// For UC28_TeamsForm
			this.setAllStates((List)instance); // For UC28_TeamsForm
		}
				// Set the instance AllTeams.
		if(ALL_TEAMS.equals(instanceName)){// For UC28_TeamsForm
			this.setAllTeams((List<Team28BO>)instance); // For UC28_TeamsForm
		}
				// Set the instance AllStadiums.
		if(ALL_STADIUMS.equals(instanceName)){// For UC28_TeamsForm
			this.setAllStadiums((List)instance); // For UC28_TeamsForm
		}
			}
	
	/**
	 * Method getInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public Object getInstanceByInstanceName(final String instanceName){
		// Temporary object for tmpUC28_TeamsForm.
		Object tmpUC28_TeamsForm = null;
		
			
		// Get the instance AllStates for UC28_TeamsForm.
		if(ALL_STATES.equals(instanceName)){ // For UC28_TeamsForm
			tmpUC28_TeamsForm = this.getAllStates(); // For UC28_TeamsForm
		}
			
		// Get the instance AllTeams for UC28_TeamsForm.
		if(ALL_TEAMS.equals(instanceName)){ // For UC28_TeamsForm
			tmpUC28_TeamsForm = this.getAllTeams(); // For UC28_TeamsForm
		}
			
		// Get the instance AllStadiums for UC28_TeamsForm.
		if(ALL_STADIUMS.equals(instanceName)){ // For UC28_TeamsForm
			tmpUC28_TeamsForm = this.getAllStadiums(); // For UC28_TeamsForm
		}
		return tmpUC28_TeamsForm;// For UC28_TeamsForm
	}
	
			}
