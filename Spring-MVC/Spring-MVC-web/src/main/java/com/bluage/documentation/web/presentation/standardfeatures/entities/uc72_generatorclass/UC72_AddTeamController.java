/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.entities.uc72_generatorclass ;

// Import declaration.
// Java imports.
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.bluage.documentation.business.standardfeatures.entities.uc72_generatorclass.bos.Team72BO;
import com.bluage.documentation.business.standardfeatures.entities.uc72_generatorclass.entities.daofinder.State72DAOFinderImpl;
import com.bluage.documentation.service.standardfeatures.entities.uc72_generatorclass.ServiceTeam72;
import com.netfective.bluage.core.exception.ApplicationException;
import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;
import com.netfective.bluage.core.web.IForm;

/**
 * Class : UC72_AddTeamController
 */
@Controller
@Scope("prototype")
@SessionAttributes("uC72_AddTeamForm")
@RequestMapping(value= "/presentation/standardfeatures/entities/uc72_generatorclass")
public class UC72_AddTeamController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC72_AddTeamController.class);
	
	//Current form name
	private static final String U_C72__ADD_TEAM_FORM = "uC72_AddTeamForm";

		//CONSTANT: allStates
	private static final String ALL_STATES = "allStates";
							//CONSTANT: selectedTeam
	private static final String SELECTED_TEAM = "selectedTeam";
	//CONSTANT: listTeams
	private static final String LIST_TEAMS = "listTeams";
	//CONSTANT: teamToCreate
	private static final String TEAM_TO_CREATE = "teamToCreate";
	
	/**
	 * Property:customDateEditorsUC72_AddTeamController
	 */
	final private Map<String,CustomDateEditor> customDateEditorsUC72_AddTeamController = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:uC72_AddTeamValidator
	 */
	final private UC72_AddTeamValidator uC72_AddTeamValidator = new UC72_AddTeamValidator();
	
	/**
	 * Service declaration : serviceTeam72.
	 */
	@Autowired
	private ServiceTeam72 serviceTeam72;
	
	/**
	 * Generic Finder : state72DAOFinderImpl.
	 */
	@Autowired
	private State72DAOFinderImpl state72DAOFinderImpl;
	
	
	// Initialise all the instances for UC72_AddTeamController
			// Initialize the instance allStates for the state lnk_delete
		private List allStates; // Initialize the instance allStates for UC72_AddTeamController
										// Declare the instance selectedTeam
		private Team72BO selectedTeam;
		// Declare the instance listTeams
		private List listTeams;
		// Declare the instance teamToCreate
		private Team72BO teamToCreate;
								/**
	 * Operation : lnk_delete
 	 * @param model : 
 	 * @param uC72_AddTeam : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC72_AddTeam/lnk_delete.html",method = RequestMethod.POST)
	public String lnk_delete(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC72_AddTeamForm") UC72_AddTeamForm  uC72_AddTeamForm, final BindingResult bindingResult ,final @RequestParam(ID_REQUEST_PARAM) Integer index){
		
		
		
		LOGGER.info("Begin method : lnk_delete"); 
		infoLogger(uC72_AddTeamForm); 
		uC72_AddTeamForm.setAlwaysCallPreControllers(false); 
		// initialization for lnk_delete
		init();
		
		String destinationPath = null; 
		

										 callDeteleteam(request,uC72_AddTeamForm , index);
			
 callSearchallteams(request,uC72_AddTeamForm );
						// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.entities.uc72_generatorclass.UC72_AddTeamForm or not from lnk_delete
			// Populate the destination form with all the instances returned from lnk_delete.
			final IForm uC72_AddTeamForm2 = populateDestinationForm(request.getSession(), uC72_AddTeamForm); 
					
			// Add the form to the model.
			model.addAttribute("uC72_AddTeamForm", uC72_AddTeamForm2); 
			
			request.getSession().setAttribute("uC72_AddTeamForm", uC72_AddTeamForm2);
			
			// "OK" CASE => destination screen path from lnk_delete
			LOGGER.info("Go to the screen 'UC72_AddTeam'.");
			// Redirect (PRG) from lnk_delete
			destinationPath =  "redirect:/presentation/standardfeatures/entities/uc72_generatorclass/UC72_AddTeam.html";
						
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_delete 
	}
	
				/**
	 * Operation : lnk_create
 	 * @param model : 
 	 * @param uC72_AddTeam : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC72_AddTeam/lnk_create.html",method = RequestMethod.POST)
	public String lnk_create(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC72_AddTeamForm") UC72_AddTeamForm  uC72_AddTeamForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : lnk_create"); 
		infoLogger(uC72_AddTeamForm); 
		uC72_AddTeamForm.setAlwaysCallPreControllers(false); 
		// initialization for lnk_create
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state lnk_create if the validation fail, you will be forward to the screen : "/presentation/standardfeatures/entities/uc72_generatorclass/UC72_AddTeam"
		if(errorsMessages(request,response,uC72_AddTeamForm,bindingResult,"", uC72_AddTeamValidator, customDateEditorsUC72_AddTeamController)){ 
			return "/presentation/standardfeatures/entities/uc72_generatorclass/UC72_AddTeam"; 
		}

					 call(request,uC72_AddTeamForm );
			
 callFindallteams(request,uC72_AddTeamForm );
						// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.entities.uc72_generatorclass.UC72_AddTeamForm or not from lnk_create
			// Populate the destination form with all the instances returned from lnk_create.
			final IForm uC72_AddTeamForm2 = populateDestinationForm(request.getSession(), uC72_AddTeamForm); 
					
			// Add the form to the model.
			model.addAttribute("uC72_AddTeamForm", uC72_AddTeamForm2); 
			
			request.getSession().setAttribute("uC72_AddTeamForm", uC72_AddTeamForm2);
			
			// "OK" CASE => destination screen path from lnk_create
			LOGGER.info("Go to the screen 'UC72_AddTeam'.");
			// Redirect (PRG) from lnk_create
			destinationPath =  "redirect:/presentation/standardfeatures/entities/uc72_generatorclass/UC72_AddTeam.html";
						
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_create 
	}
	
	
	/**
	* This method initialise the form : UC72_AddTeamForm 
	* @return UC72_AddTeamForm
	*/
	@ModelAttribute("UC72_AddTeamFormInit")
	public void initUC72_AddTeamForm(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, U_C72__ADD_TEAM_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method UC72_AddTeamForm."); //for lnk_create 
		}
		UC72_AddTeamForm uC72_AddTeamForm;
	
		if(request.getSession().getAttribute(U_C72__ADD_TEAM_FORM) != null){
			uC72_AddTeamForm = (UC72_AddTeamForm)request.getSession().getAttribute(U_C72__ADD_TEAM_FORM);
		} else {
			uC72_AddTeamForm = new UC72_AddTeamForm();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form UC72_AddTeamForm.");
		}
		uC72_AddTeamForm = (UC72_AddTeamForm)populateDestinationForm(request.getSession(), uC72_AddTeamForm);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  UC72_AddTeamForm.");
		}
		model.addAttribute(U_C72__ADD_TEAM_FORM, uC72_AddTeamForm);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param UC72_AddTeamForm : The sceen form.
	 */
	@RequestMapping(value = "/UC72_AddTeam.html" ,method = RequestMethod.GET)
	public void prepareUC72_AddTeam(final HttpServletRequest request,final  Model model,final  @ModelAttribute("UC72_AddTeamFormInit") UC72_AddTeamForm uC72_AddTeamForm){
		
		UC72_AddTeamForm currentUC72_AddTeamForm = uC72_AddTeamForm;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(U_C72__ADD_TEAM_FORM) == null){
			if(currentUC72_AddTeamForm!=null){
				request.getSession().setAttribute(U_C72__ADD_TEAM_FORM, currentUC72_AddTeamForm);
			}else {
				currentUC72_AddTeamForm = new UC72_AddTeamForm();
				request.getSession().setAttribute(U_C72__ADD_TEAM_FORM, currentUC72_AddTeamForm);	
			}
		} else {
			currentUC72_AddTeamForm = (UC72_AddTeamForm) request.getSession().getAttribute(U_C72__ADD_TEAM_FORM);
		}

		try {
			List allStates = state72DAOFinderImpl.findAll();
			put(request.getSession(), ALL_STATES, allStates);
		} catch (ApplicationException e) {
			errorLogger("An error while calling the generic finder for the instance : allStates.",e);
		}
					currentUC72_AddTeamForm = (UC72_AddTeamForm)populateDestinationForm(request.getSession(), currentUC72_AddTeamForm);
		// Call all the Precontroller.
	currentUC72_AddTeamForm.setAlwaysCallPreControllers(true);
		model.addAttribute(U_C72__ADD_TEAM_FORM, currentUC72_AddTeamForm);
		request.getSession().setAttribute(U_C72__ADD_TEAM_FORM, currentUC72_AddTeamForm);
		
	}
	
											/**
	 * method callDeteleteam
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 * @param index Integer the current index 
	 */
	private void callDeteleteam(HttpServletRequest request,IForm form,Integer index) {
					if(index!=null){  
			 selectedTeam = (Team72BO)((List)get(request.getSession(),form, "listTeams")).get(index);  
		} else {
			 selectedTeam= null; 
		}
				 
					try {
				// executing Deteleteam in lnk_create
	serviceTeam72.team72Delete(
			selectedTeam
			);  

													// processing variables Deteleteam in lnk_create
				put(request.getSession(), SELECTED_TEAM,selectedTeam); 
			
			} catch (ApplicationException e) { 
				// error handling for operation team72Delete called Deteleteam
				errorLogger("An error occured during the execution of the operation : team72Delete",e); 
		
			}
	}
		/**
	 * method callFindallteams
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callFindallteams(HttpServletRequest request,IForm form) {
		listTeams = (List)get(request.getSession(),form, "listTeams");  
										 
					try {
				// executing Findallteams in lnk_create
				listTeams = 	serviceTeam72.team72FindAll(
	);  
 
				put(request.getSession(), LIST_TEAMS,listTeams);
								// processing variables Findallteams in lnk_create

			} catch (ApplicationException e) { 
				// error handling for operation team72FindAll called Findallteams
				errorLogger("An error occured during the execution of the operation : team72FindAll",e); 
		
			}
	}
		/**
	 * method callSearchallteams
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callSearchallteams(HttpServletRequest request,IForm form) {
		listTeams = (List)get(request.getSession(),form, "listTeams");  
										 
					try {
				// executing Searchallteams in lnk_create
				listTeams = 	serviceTeam72.team72FindAll(
	);  
 
				put(request.getSession(), LIST_TEAMS,listTeams);
								// processing variables Searchallteams in lnk_create

			} catch (ApplicationException e) { 
				// error handling for operation team72FindAll called Searchallteams
				errorLogger("An error occured during the execution of the operation : team72FindAll",e); 
		
			}
	}
					/**
	 * method call
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void call(HttpServletRequest request,IForm form) {
					teamToCreate = (Team72BO)get(request.getSession(),form, "teamToCreate");  
										 
					try {
				// executing  in lnk_create
	serviceTeam72.createTeam(
			teamToCreate
			);  

								// processing variables  in lnk_create
				put(request.getSession(), TEAM_TO_CREATE,teamToCreate); 
			
			} catch (ApplicationException e) { 
				// error handling for operation createTeam called 
				errorLogger("An error occured during the execution of the operation : createTeam",e); 
		
			}
	}
		
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("UC72_AddTeamController [ ");
				strBToS.append(ALL_STATES);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(allStates);
			strBToS.append(END_TO_STRING_DELIMITER);
									strBToS.append(SELECTED_TEAM);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(selectedTeam);
			strBToS.append(END_TO_STRING_DELIMITER);
			strBToS.append(LIST_TEAMS);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(listTeams);
			strBToS.append(END_TO_STRING_DELIMITER);
			strBToS.append(TEAM_TO_CREATE);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(teamToCreate);
			strBToS.append(END_TO_STRING_DELIMITER);
		// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(uC72_AddTeamValidator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsUC72_AddTeamController!=null); 
		return strBToS.toString();
	}
}
