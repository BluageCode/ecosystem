package uc1001;

import org.junit.Test;

import test.common.AbstractBADocTestCase;

public class ITuc1001 extends AbstractBADocTestCase {



	
	@Override
	public  String getName(){
		return "UC-1001";
	}


	@Test
	public void testUC1001() throws Exception {
		// On teste l'UC1001 - Standard Features/Application/Datagrid - PaginationTable
		// On vérifie que les données des équipes sont bien affichées.
		
		
		selenium.open("");	

		selenium.click("link=Expand All");
		selenium.waitForPageToLoad("300000");
		selenium.click("link=UC1001 - Tables");
		

		verifyTrue(selenium.isElementPresent("//h2[contains(text(),\"QA UC > UC1001 > Tables\")]"));
		// On vérifie les données.
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/thead/tr[th[contains(text(),\"City\")] and th[contains(text(),\"Name\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Charlotte\")] and td/span[contains(text(),\"Tigers\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Kansas City\")] and td/span[contains(text(),\"Bears\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Detroit\")] and td/span[contains(text(),\"Chargers\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Detroit\")] and td/span[contains(text(),\"Hawks\")]]"));
		// On testes toutes les données de l'UC
		// Pour les Tigers Charlotte
		selenium.click("link=UC1001 - Tables");
		selenium.waitForPageToLoad("30000");
		verifyTrue(selenium.isElementPresent("//h2[contains(text(),\"QA UC > UC1001 > Tables\")]"));
		// On indexe la ligne de Kansas City.
		Number index = selenium.getElementIndex("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Charlotte\")]\n					and td/span[contains(text(),\"Tigers\")]]");
		System.out.println("variable index vaut : " + index);
		// On clique sur [view]
		selenium.click("//table[contains(@id,'tab_teams')]/tbody/tr[" + index + " +\n1]/td/a[contains(text(),\"[view]\")]");
		selenium.waitForPageToLoad("30000");
		// On vérifie que l'on est sur la bonne page.
		assertTrue(selenium.isTextPresent("Tigers"));
		assertTrue(selenium.isTextPresent("Charlotte"));
//		assertEquals("Tigers", selenium.getText("//*[contains(@id,'txt_TeamName')]"));
//		assertEquals("Charlotte", selenium.getText("//*[contains(@id,'txt_Teamcity')]"));
		// On vérifie les données.
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]//tbody//tr[td/span[contains(text(),\"Oliver\")] and td/span[contains(text(),\"Frank\")] and td/span[contains(text(),\"10/31/1968\")] and td/span[contains(text(),\"0.11\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]//tbody//tr[td/span[contains(text(),\"Abel\")] and td/span[contains(text(),\"Dennis\")] and td/span[contains(text(),\"04/16/1980\")] and td/span[contains(text(),\"30.51\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]//tbody//tr[td/span[contains(text(),\"Lester\")] and td/span[contains(text(),\"Cross\")] and td/span[contains(text(),\"02/21/1976\")] and td/span[contains(text(),\"30.98\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]//tbody//tr[td/span[contains(text(),\"Jerome\")] and td/span[contains(text(),\"Grant\")] and td/span[contains(text(),\"06/28/1982\")] and td/span[contains(text(),\"28.94\")]]"));
		// Pour les Chargers Detroit
		selenium.click("//*[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
		verifyTrue(selenium.isElementPresent("//h2[contains(text(),\"QA UC > UC1001 > Tables\")]"));
		// On indexe la ligne de Detroit.
		index = selenium.getElementIndex("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Detroit\")] and td/span[contains(text(),\"Chargers\")]]");
		System.out.println("variable index vaut : " + index);
		// On clique sur [view]
		selenium.click("//table[contains(@id,'tab_teams')]/tbody/tr[" + index + " + 1]/td/a[contains(text(),\"[view]\")]");
		selenium.waitForPageToLoad("30000");
		// On vérifie que l'on est sur la bonne page.
		assertTrue(selenium.isTextPresent("Chargers"));
		assertTrue(selenium.isTextPresent("Detroit"));
//		assertEquals("Chargers", selenium.getText("//*[contains(@id,'txt_TeamName')]"));
//		assertEquals("Detroit", selenium.getText("//*[contains(@id,'txt_Teamcity')]"));
		verifyEquals("2", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]//tbody//tr[td/span[contains(text(),\"Michael\")] and td/span[contains(text(),\"Hodges\")] and td/span[contains(text(),\"04/01/1988\")] and td/span[contains(text(),\"25.35\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]//tbody//tr[td/span[contains(text(),\"Roderick\")] and td/span[contains(text(),\"Robbins\")] and td/span[contains(text(),\"07/29/1975\")] and td/span[contains(text(),\"12.04\")]]"));
		// Pour les Hawks Detroit
		selenium.click("//*[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
		verifyTrue(selenium.isElementPresent("//h2[contains(text(),\"QA UC > UC1001 > Tables\")]"));
		// On indexe la ligne de Detroit
		index = selenium.getElementIndex("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Detroit\")] and td/span[contains(text(),\"Hawks\")]]");
		System.out.println("variable index vaut : " + index);
		// On clique sur [view]
		selenium.click("//table[contains(@id,'tab_teams')]/tbody/tr[" + index + " + 1]/td/a[contains(text(),\"[view]\")]");
		selenium.waitForPageToLoad("30000");
		// On vérifie que l'on est sur la bonne page.
		assertTrue(selenium.isTextPresent("Hawks"));
		assertTrue(selenium.isTextPresent("Detroit"));
//		assertEquals("Hawks", selenium.getText("//*[contains(@id,'txt_TeamName')]"));
//		assertEquals("Detroit", selenium.getText("//*[contains(@id,'txt_Teamcity')]"));
		verifyEquals("5", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]//tbody//tr[td/span[contains(text(),\"Ricky\")] and td/span[contains(text(),\"Dawson\")] and td/span[contains(text(),\"03/03/1990\")] and td/span[contains(text(),\"29.3\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]//tbody//tr[td/span[contains(text(),\"Howard\")] and td/span[contains(text(),\"Ramos\")] and td/span[contains(text(),\"08/25/1970\")] and td/span[contains(text(),\"10.76\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]//tbody//tr[td/span[contains(text(),\"Ruben\")] and td/span[contains(text(),\"Pope\")] and td/span[contains(text(),\"05/01/1985\")] and td/span[contains(text(),\"44.66\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]//tbody//tr[td/span[contains(text(),\"Ivan\")] and td/span[contains(text(),\"Tate\")] and td/span[contains(text(),\"02/08/1972\")] and td/span[contains(text(),\"48.49\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]//tbody//tr[td/span[contains(text(),\"Christian\")] and td/span[contains(text(),\"Chavez\")] and td/span[contains(text(),\"05/17/1983\")] and td/span[contains(text(),\"37.88\")]]"));
		// Pour les Bears Kansas City
		selenium.click("//*[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
		verifyTrue(selenium.isElementPresent("//h2[contains(text(),\"QA UC > UC1001 > Tables\")]"));
		// On indexe la ligne de Kansas City.
		index = selenium.getElementIndex("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Kansas City\")] and td/span[contains(text(),\"Bears\")]]");
		System.out.println("variable index vaut : " + index);
		// On clique sur [view]
		selenium.click("//table[contains(@id,'tab_teams')]/tbody/tr[" + index + " +\n					1]/td/a[contains(text(),\"[view]\")]");
		selenium.waitForPageToLoad("30000");
		// On vérifie que l'on est sur la bonne page.
		assertTrue(selenium.isTextPresent("Bears"));
		assertTrue(selenium.isTextPresent("Kansas City"));
//		assertEquals("Bears", selenium.getText("//*[contains(@id,'txt_TeamName')]"));
//		assertEquals("Kansas City", selenium.getText("//*[contains(@id,'txt_Teamcity')]"));
		// On vérifie les données.
		verifyEquals("7", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]//tbody//tr[td/span[contains(text(),\"Elbert\")] and\n					td/span[contains(text(),\"Cole\")] and\n					td/span[contains(text(),\"04/07/1965\")] and\n					td/span[contains(text(),\"3.95\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]//tbody//tr[td/span[contains(text(),\"Earl\")] and\n					td/span[contains(text(),\"Russell\")] and\n					td/span[contains(text(),\"12/15/1981\")] and\n					td/span[contains(text(),\"44.73\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]//tbody//tr[td/span[contains(text(),\"Nick\")] and\n					td/span[contains(text(),\"Garner\")] and\n					td/span[contains(text(),\"07/27/1992\")] and\n					td/span[contains(text(),\"47.84\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]//tbody//tr[td/span[contains(text(),\"Keith\")] and\n					td/span[contains(text(),\"Barnett\")] and\n					td/span[contains(text(),\"11/10/1976\")] and\n					td/span[contains(text(),\"5.81\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]//tbody//tr[td/span[contains(text(),\"Tommy\")] and\n					td/span[contains(text(),\"Wood\")] and\n					td/span[contains(text(),\"01/20/1992\")] and\n					td/span[contains(text(),\"13.93\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]//tbody//tr[td/span[contains(text(),\"Sylvester\")]\n					and td/span[contains(text(),\"Nash\")] and\n					td/span[contains(text(),\"07/10/1965\")] and\n					td/span[contains(text(),\"37.73\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]//tbody//tr[td/span[contains(text(),\"Cecil\")] and\n					td/span[contains(text(),\"Conner\")] and\n					td/span[contains(text(),\"06/29/1978\")] and\n					td/span[contains(text(),\"39.71\")]]"));
		// Pour les Braves Omaha
		selenium.click("//*[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
		verifyTrue(selenium.isElementPresent("//h2[contains(text(),\"QA UC > UC1001 > Tables\")]"));
		// On indexe la ligne de Omaha.
		index = selenium.getElementIndex("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Omaha\")] and td/span[contains(text(),\"Braves\")]]");
		System.out.println("variable index vaut : " + index);
		// On clique sur [view]
		selenium.click("//table[contains(@id,'tab_teams')]/tbody/tr[" + index + " + 1]/td/a[contains(text(),\"[view]\")]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isTextPresent("Braves"));
		assertTrue(selenium.isTextPresent("Omaha"));
//		assertEquals("Braves", selenium.getText("//*[contains(@id,'txt_TeamName')]"));
//		assertEquals("Omaha", selenium.getText("//*[contains(@id,'txt_Teamcity')]"));
		verifyEquals("2", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]//tbody//tr[td/span[contains(text(),\"Allan\")] and td/span[contains(text(),\"Meyer\")] and td/span[contains(text(),\"02/14/1969\")] and td/span[contains(text(),\"33.67\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]//tbody//tr[td/span[contains(text(),\"Woodrow\")] and td/span[contains(text(),\"Carpenter\")] and td/span[contains(text(),\"08/08/1987\")] and td/span[contains(text(),\"41.76\")]]"));

	}


}
