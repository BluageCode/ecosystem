/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.common.home ;


import org.apache.log4j.Logger;
import org.springframework.validation.Errors;

import com.netfective.bluage.core.validator.AbstractValidator;
/**
* Class :homeValidator
*/
public class homeValidator extends AbstractValidator{
	
	// LOGGER for the class homeValidator
	private static final Logger LOGGER = Logger.getLogger( homeValidator.class);
	
	
	/**
	* Operation validate for homeForm
	* @param obj : the current form (homeForm)
	* @param errors : The spring errors to return for the form homeForm
	*/
	public void validate(final Object obj,final Errors errors){
		LOGGER.info("Beginning method : validate"); // homeValidator
		homeForm chomeForm = (homeForm)obj; // homeValidator
		LOGGER.info("Ending method : validate the form "+ chomeForm.getClass().getName()); // homeValidator
	}

	/**
	* Method to implements to use spring validators (homeForm)
	* @param aClass : Class for the form homeForm
	* @return boolean
	*/
	public boolean supports(final Class aClass){
		return (new homeForm()).getClass().equals(aClass); // homeValidator
	}
}
