/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.business.standardfeatures.application.component.uc47_contents.bos;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import com.bluage.documentation.business.AbstractBusinessObject;

/**
 * Screen47BO
 *
* @see com.netfective.bluage.core.business.IBusinessObject
 */
@XmlRootElement

public class Screen47BO
 extends AbstractBusinessObject {
	
	/**
	 * Default SUID for Screen47BO.
	 */
	private static final long serialVersionUID = 1L;
	

						/**
	 * Entity property: table
	 *
	 */
private Boolean table = Boolean.FALSE;

					/**
	 * Entity property: allPlayers
	 *
	 */
private List allPlayers;



   /**
	* public default constructor for class Screen47BO
	*/
	public Screen47BO(){
		//NOP Screen47BO
	}


						/**
	 * Getter for table of class Screen47BO.
	 *
	 * @return {@link java.lang.Boolean}
	 * @generated
	 */ 
public  Boolean getTable() {
	return this.table;
	}


	/**
	 * Setter for table of class Screen47BO.
	 * @param value {@link java.lang.Boolean}
	 * @generated
	 */

public  void setTable(final Boolean table) {
	this.table = table;
}

					/**
	 * Getter for allPlayers of class Screen47BO.
	 *
	 * @return {@link java.util.List}
	 * @generated
	 */ 
public  List getAllPlayers() {
	return this.allPlayers;
	}


	/**
	 * Setter for allPlayers of class Screen47BO.
	 * @param value {@link java.util.List}
	 * @generated
	 */

public  void setAllPlayers(final List allPlayers) {
	this.allPlayers = allPlayers;
}


}

