<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file="/include.jsp"  %>
<html >
<head >
<title  > 
<fmt:message key="cUC206_ListPlayer_BLU_AGE_DOCUMENTATION"/>
</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"  > 
</meta>
<link href="<c:url value= "/presentation/_style/_style.css"/>" rel="stylesheet" type="text/css"  /> 
<link href="<c:url value= "/presentation/_include/jquery/rd_lib.css"/>" rel="stylesheet" type="text/css"  /> 
<script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/jquery.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/rd_setup.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/rd_lib.js"/>" space="preserve"  ></script><link rel="shortcut icon" href="<c:url value= "/presentation/_style/favicon.ico"/>"  /> 

</head>

<body class=" " > 
<div id="rd_ghost"  > 
<small  > 
<fmt:message key="cUC206_ListPlayer___CSS_DEACTIVATED"/>
</small>
</div>
<div id="rd_jghost"  > 
<small  > 
<fmt:message key="cUC206_ListPlayer___JAVASCRIPT_DEACTIVATED"/>
</small>
</div>
<div id="shape"  > 
<div class="ui-layout-center"  > 
<div class="east-north"  > 
	<form:form id="uC206_ListPlayer" commandName="uC206_ListPlayerForm"  enctype="application/x-www-form-urlencoded" method="post">
<h2 class="rd_first"  > 
<fmt:message key="cUC206_ListPlayer_ADVANCED_FEATURES__VALUE_OBJE"/>
</h2>
<display:table uid="tab_searchResult" pagesize="10"  sort="list" requestURI="" name="sessionScope.uC206_ListPlayerForm.playerVOs" class="rd_tabledata"  >
	<c:set   var="rownum">
		<%=(tab_searchResult !=	null) ? new Integer(tab_searchResult_rowNum.intValue() - 1) : new Integer(0) %>
	</c:set>
	<c:set   var="idRow">
		<%=(tab_searchResult != null ) ? tab_searchResult_rowNum.intValue() - 1 : -1 %>
	</c:set>
	<c:set   var="idTab">
		<%=(tab_searchResult != null ) ? "tab_searchResult" : null %>
	</c:set>
	<c:set var="tableHashId">
		<%= new org.displaytag.util.ParamEncoder("tab_searchResult").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE) %>
	</c:set>
	<c:set var="tableHashId2">
		<%=request.getParameter(new org.displaytag.util.ParamEncoder("tab_searchResult").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE)) %>
	</c:set>	
	
<display:column format=""  titleKey="cUC206_ListPlayer_LAST_NAME">
<c:if test="${uC206_ListPlayerForm.selectedRow!=idRow || uC206_ListPlayerForm.selectedTab!=idTab}">
		<span id="txt_lastname1">
	<c:out value="${tab_searchResult.lastName}" />
		</span>
	</c:if>
<c:if test="${uC206_ListPlayerForm.selectedRow==idRow && uC206_ListPlayerForm.selectedTab==idTab}">
<form:input id="txt_lastname[${rownum}]" path="playerVOs[${rownum}].lastName"  
		/>
<form:errors path="playerVOs[${rownum}].lastName"/>
</c:if>
</display:column>
	
<display:column format=""  titleKey="cUC206_ListPlayer_FIRST_NAME">
<c:if test="${uC206_ListPlayerForm.selectedRow!=idRow || uC206_ListPlayerForm.selectedTab!=idTab}">
		<span id="txt_firstname1">
	<c:out value="${tab_searchResult.firstName}" />
		</span>
	</c:if>
<c:if test="${uC206_ListPlayerForm.selectedRow==idRow && uC206_ListPlayerForm.selectedTab==idTab}">
<form:input id="txt_firstname[${rownum}]" path="playerVOs[${rownum}].firstName"  
		/>
<form:errors path="playerVOs[${rownum}].firstName"/>
</c:if>
</display:column>
	
<display:column format=""  titleKey="cUC206_ListPlayer_POSITION">
		<span id="txt_position">
	<c:out value="${tab_searchResult.positionVO.name}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="cUC206_ListPlayer_HEIGHT_CM">
		<span id="txt_height">
	<c:out value="${tab_searchResult.height}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="cUC206_ListPlayer_ROOKIE">
<form:checkbox id="chk_isRookie[${rownum}]" path="playerVOs[${rownum}].rookie"  name="isRookie" disabled="true"  
		/>
</display:column>
	
<display:column format=""  titleKey="cUC206_ListPlayer_">
<c:if test="${uC206_ListPlayerForm.selectedRow!=idRow || uC206_ListPlayerForm.selectedTab!=idTab}">
<a id="lnk_edit[${rownum}]"onclick="javascript:btn_click_cExecuteUC206_ListPlayer(this.href);return false;"			href="<c:url value="/presentation/advancedfeatures/valueobject/uc206_valueobject/UC206_ListPlayer/lnk_edit.html?id="/><c:out value="${idRow}" />&tab=<c:out value="tab_searchResult" />&<c:out value="${tableHashId}"/>=<c:out value="${tableHashId2}"/>"
	 shape="rect"  >									 
<fmt:message key="cUC206_ListPlayer_EDIT"/>
</a>
</c:if>
<c:if test="${uC206_ListPlayerForm.selectedRow==idRow && uC206_ListPlayerForm.selectedTab==idTab}">
<a id="lnk_update[${rownum}]"onclick="javascript:btn_click_cExecuteUC206_ListPlayer(this.href);return false;"			href="<c:url value="/presentation/advancedfeatures/valueobject/uc206_valueobject/UC206_ListPlayer/lnk_update.html?id="/><c:out value="${idRow}" />&tab=<c:out value="tab_searchResult" />&<c:out value="${tableHashId}"/>=<c:out value="${tableHashId2}"/>"
	 shape="rect"  >									 
<fmt:message key="cUC206_ListPlayer_UPDATE"/>
</a>
</c:if>
</display:column>
	
<display:column format=""  titleKey="cUC206_ListPlayer__16">
<a id="lnk_details[${rownum}]"onclick="javascript:btn_click_cExecuteUC206_ListPlayer(this.href);return false;"			href="<c:url value="/presentation/advancedfeatures/valueobject/uc206_valueobject/UC206_ListPlayer/lnk_details.html?id="/><c:out value="${idRow}" />&tab=<c:out value="tab_searchResult" />&<c:out value="${tableHashId}"/>=<c:out value="${tableHashId2}"/>"
	 shape="rect"  >									 
<fmt:message key="cUC206_ListPlayer_DETAILS"/>
</a>
</display:column>
</display:table>
<br clear="none"  /> 
<br clear="none"  /> 
<a id="lnk_homePage"				href="<c:url value="/presentation/advancedfeatures/valueobject/uc206_valueobject/UC206_HomePage.html"/>"
													 shape="rect"  >									 
<fmt:message key="cUC206_ListPlayer_BACK"/>
</a>
	</form:form>
	<script language="javascript">
		function btn_click_cExecuteUC206_ListPlayer(url) {
			UC206_ListPlayervar = document.getElementById('uC206_ListPlayer');
			UC206_ListPlayervar.setAttribute('action', url);
			UC206_ListPlayervar.submit();
		}
		function btn_click_cSetActionUC206_ListPlayer(url) {
			UC206_ListPlayervar = document.getElementById('uC206_ListPlayer');
			UC206_ListPlayervar.setAttribute('action', url);
		}
	</script>
</div>
<div class="east-center"  > 
<tiles:insertDefinition	name="presentation/advancedfeatures/valueobject/uc206_valueobject/UC206_ListPlayercmp_tabs" />
</div>
</div>
<div class="ui-layout-north"  > 
<tiles:insertDefinition	name="presentation/advancedfeatures/valueobject/uc206_valueobject/UC206_ListPlayercmp_header" />
</div>
<div class="ui-layout-south"  > 
<fmt:message key="cUC206_ListPlayer_______"/>
<span id="rd_jversion"  > 
<fmt:message key="cUC206_ListPlayer_BLU_AGE_DOCUMENTATION_17"/>
</span>
</div>
<div class="ui-layout-west"  > 
<tiles:insertDefinition	name="presentation/advancedfeatures/valueobject/uc206_valueobject/UC206_ListPlayercmp_treeview" />
</div>
</div>
</body>
</html>
