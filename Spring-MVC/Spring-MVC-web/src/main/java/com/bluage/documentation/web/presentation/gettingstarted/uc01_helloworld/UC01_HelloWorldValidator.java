/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.gettingstarted.uc01_helloworld ;


import org.apache.log4j.Logger;
import org.springframework.validation.Errors;

import com.netfective.bluage.core.validator.AbstractValidator;
/**
* Class :UC01_HelloWorldValidator
*/
public class UC01_HelloWorldValidator extends AbstractValidator{
	
	// LOGGER for the class UC01_HelloWorldValidator
	private static final Logger LOGGER = Logger.getLogger( UC01_HelloWorldValidator.class);
	
	
	/**
	* Operation validate for UC01_HelloWorldForm
	* @param obj : the current form (UC01_HelloWorldForm)
	* @param errors : The spring errors to return for the form UC01_HelloWorldForm
	*/
	public void validate(final Object obj,final Errors errors){
		LOGGER.info("Beginning method : validate"); // UC01_HelloWorldValidator
		UC01_HelloWorldForm cUC01_HelloWorldForm = (UC01_HelloWorldForm)obj; // UC01_HelloWorldValidator
		LOGGER.info("Ending method : validate the form "+ cUC01_HelloWorldForm.getClass().getName()); // UC01_HelloWorldValidator
	}

	/**
	* Method to implements to use spring validators (UC01_HelloWorldForm)
	* @param aClass : Class for the form UC01_HelloWorldForm
	* @return boolean
	*/
	public boolean supports(final Class aClass){
		return (new UC01_HelloWorldForm()).getClass().equals(aClass); // UC01_HelloWorldValidator
	}
}
