/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.business.standardfeatures.entities.uc65_lazy.bos;

import java.util.Date;

import javax.inject.Inject;
import javax.inject.Named;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import com.bluage.documentation.business.common.bos.PersonBO;
import com.bluage.documentation.business.standardfeatures.entities.uc65_lazy.entities.daofinder.Rookie65DAOFinderImpl;

/**
 * Rookie65BO
 *
* @see com.netfective.bluage.core.business.IBusinessObject
* * @see PersonBO
 */
@XmlRootElement

public class Rookie65BO
 extends PersonBO {
	
	/**
	 * Default SUID for Rookie65BO.
	 */
	private static final long serialVersionUID = 1L;
	

	@Inject
	@Named ("com.bluage.documentation.business.standardfeatures.entities.uc65_lazy.entities.daofinder.Rookie65Finder") 
	private Rookie65DAOFinderImpl finder;

						/**
	 * Entity property: dateOfBirth
	 *
	 */
private Date dateOfBirth;

					/**
	 * Entity property: estimatedValue
	 *
	 */
private Float estimatedValue;

					/**
	 * Entity property: weight
	 *
	 */
private Integer weight;

					/**
	 * Entity property: height
	 *
	 */
private Integer height;

					/**
	 * Entity property: university
	 *
	 */
private String university;

											/**
	 * Entity property: team65
	 *
	 */
private Team65BO team65;



   /**
	* public default constructor for class Rookie65BO
	*/
	public Rookie65BO(){
		//NOP Rookie65BO
	}


						/**
	 * Getter for dateOfBirth of class Rookie65BO.
	 *
	 * @return {@link java.util.Date}
	 * @generated
	 */ 
public  Date getDateOfBirth() {
	return this.dateOfBirth;
	}


	/**
	 * Setter for dateOfBirth of class Rookie65BO.
	 * @param value {@link java.util.Date}
	 * @generated
	 */

public  void setDateOfBirth(final Date dateOfBirth) {
	this.dateOfBirth = dateOfBirth;
}

					/**
	 * Getter for estimatedValue of class Rookie65BO.
	 *
	 * @return {@link java.lang.Float}
	 * @generated
	 */ 
public  Float getEstimatedValue() {
	return this.estimatedValue;
	}


	/**
	 * Setter for estimatedValue of class Rookie65BO.
	 * @param value {@link java.lang.Float}
	 * @generated
	 */

public  void setEstimatedValue(final Float estimatedValue) {
	this.estimatedValue = estimatedValue;
}

					/**
	 * Getter for weight of class Rookie65BO.
	 *
	 * @return {@link java.lang.Integer}
	 * @generated
	 */ 
public  Integer getWeight() {
	return this.weight;
	}


	/**
	 * Setter for weight of class Rookie65BO.
	 * @param value {@link java.lang.Integer}
	 * @generated
	 */

public  void setWeight(final Integer weight) {
	this.weight = weight;
}

					/**
	 * Getter for height of class Rookie65BO.
	 *
	 * @return {@link java.lang.Integer}
	 * @generated
	 */ 
public  Integer getHeight() {
	return this.height;
	}


	/**
	 * Setter for height of class Rookie65BO.
	 * @param value {@link java.lang.Integer}
	 * @generated
	 */

public  void setHeight(final Integer height) {
	this.height = height;
}

					/**
	 * Getter for university of class Rookie65BO.
	 *
	 * @return {@link java.lang.String}
	 * @generated
	 */ 
public  String getUniversity() {
	return this.university;
	}


	/**
	 * Setter for university of class Rookie65BO.
	 * @param value {@link java.lang.String}
	 * @generated
	 */

public  void setUniversity(final String university) {
	this.university = university;
}

											/**
	 * Getter for team65 of class Rookie65BO.
	 *
	 * @return {@link com.bluage.documentation.business.standardfeatures.entities.uc65_lazy.bos.Team65BO}
	 * @generated
	 */ 
public  Team65BO getTeam65() {
		Team65BO team650 = this.team65;
		if(this.team65 == null) {
			if(DAO_MODE.equals(this.getObjMode( ) ) ) {
				team650 = null;
			} else {
				this.team65 = new Team65BO();
				this.team65.setObjMode( this.getObjMode( ) );
				team650 = this.team65;
			}
		} else {
			this.team65.setObjMode( this.getObjMode( ) );
			if( HELPER_MODE.equals(this.getObjMode( ) ) ) {
				team650 = this.team65;
			}
			if( DAO_MODE.equals(this.getObjMode()) && this.team65.isEmpty ) {
				team650 = null;
			}
		}
		return team650;
	}


	/**
	 * Setter for team65 of class Rookie65BO.
	 * @param value {@link com.bluage.documentation.business.standardfeatures.entities.uc65_lazy.bos.Team65BO}
	 * @generated
	 */

public  void setTeam65(final Team65BO team65) {
	if(team65 != null) {
		team65.isEmpty = false;
		team65.setObjMode( this.getObjMode( ) );
	}
	this.isEmpty = false;
	this.team65 = team65;
}


/**
	 * Returns <code>true</code> if the argument is an Rookie65 instance and all identifiers for this entity
     * equal the identifiers of the argument entity. The <code>equals</code> method of the parent entity
     * will also need to return <code>true</code>. Returns <code>false</code> otherwise.
     *
     * @see PersonBO#hashCode()
	 * @param object  object to compare with current instance      
     * @return boolean true if equals false if not
     */
    public boolean equals(final Object object )
    {
		if ( this == object )
        {
            return true;
        }
        if ( !( object instanceof Rookie65BO ) )
        {
            return false;
        }
        final Rookie65BO that = (Rookie65BO)object;
    
    	            if (        				        			this.getId() == null
        	        ) {
        	return false;
        }
		return new EqualsBuilder()
			.append(this.getId(), that.getId())
			 	.isEquals();
	}
	/**

	 * Returns a hash code based on this entity's identifiers and the hash code of the parent entity.
     *
     * @see PersonBO#hashCode()
     * @return int
     */    
    public int hashCode() {
        return new HashCodeBuilder()
             .append(getId())
				 	.appendSuper(super.hashCode())
            .toHashCode();
    }
}

