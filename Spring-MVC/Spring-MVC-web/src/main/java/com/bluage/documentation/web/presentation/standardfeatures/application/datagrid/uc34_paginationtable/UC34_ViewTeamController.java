/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.application.datagrid.uc34_paginationtable ;

// Import declaration.
// Java imports.
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.bluage.documentation.business.standardfeatures.application.datagrid.uc34_paginationtable.bos.Team34BO;
import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;

/**
 * Class : UC34_ViewTeamController
 */
@Controller
@Scope("prototype")
@SessionAttributes("uC34_ViewTeamForm")
@RequestMapping(value= "/presentation/standardfeatures/application/datagrid/uc34_paginationtable")
public class UC34_ViewTeamController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC34_ViewTeamController.class);
	
	//Current form name
	private static final String U_C34__VIEW_TEAM_FORM = "uC34_ViewTeamForm";

	//CONSTANT: teamDetails
	private static final String TEAM_DETAILS = "teamDetails";
		//CONSTANT: listplayers table or repeater.
	private static final String LISTPLAYERS = "listplayers";
				
	/**
	 * Property:customDateEditorsUC34_ViewTeamController
	 */
	final private Map<String,CustomDateEditor> customDateEditorsUC34_ViewTeamController = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:uC34_ViewTeamValidator
	 */
	final private UC34_ViewTeamValidator uC34_ViewTeamValidator = new UC34_ViewTeamValidator();
	
	
	// Initialise all the instances for UC34_ViewTeamController
		// Initialize the instance teamDetails of type com.bluage.documentation.business.standardfeatures.application.datagrid.uc34_paginationtable.bos.Team34BO with the value : null for UC34_ViewTeamController
		private Team34BO teamDetails;
			// Initialize the instance listplayers for the state lnk_view
		private List listplayers; // Initialize the instance listplayers for UC34_ViewTeamController
				
	/**
	* This method initialise the form : UC34_ViewTeamForm 
	* @return UC34_ViewTeamForm
	*/
	@ModelAttribute("UC34_ViewTeamFormInit")
	public void initUC34_ViewTeamForm(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, U_C34__VIEW_TEAM_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method UC34_ViewTeamForm."); //for lnk_view 
		}
		UC34_ViewTeamForm uC34_ViewTeamForm;
	
		if(request.getSession().getAttribute(U_C34__VIEW_TEAM_FORM) != null){
			uC34_ViewTeamForm = (UC34_ViewTeamForm)request.getSession().getAttribute(U_C34__VIEW_TEAM_FORM);
		} else {
			uC34_ViewTeamForm = new UC34_ViewTeamForm();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form UC34_ViewTeamForm.");
		}
		uC34_ViewTeamForm = (UC34_ViewTeamForm)populateDestinationForm(request.getSession(), uC34_ViewTeamForm);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  UC34_ViewTeamForm.");
		}
		model.addAttribute(U_C34__VIEW_TEAM_FORM, uC34_ViewTeamForm);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param UC34_ViewTeamForm : The sceen form.
	 */
	@RequestMapping(value = "/UC34_ViewTeam.html" ,method = RequestMethod.GET)
	public void prepareUC34_ViewTeam(final HttpServletRequest request,final  Model model,final  @ModelAttribute("UC34_ViewTeamFormInit") UC34_ViewTeamForm uC34_ViewTeamForm){
		
		UC34_ViewTeamForm currentUC34_ViewTeamForm = uC34_ViewTeamForm;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(U_C34__VIEW_TEAM_FORM) == null){
			if(currentUC34_ViewTeamForm!=null){
				request.getSession().setAttribute(U_C34__VIEW_TEAM_FORM, currentUC34_ViewTeamForm);
			}else {
				currentUC34_ViewTeamForm = new UC34_ViewTeamForm();
				request.getSession().setAttribute(U_C34__VIEW_TEAM_FORM, currentUC34_ViewTeamForm);	
			}
		} else {
			currentUC34_ViewTeamForm = (UC34_ViewTeamForm) request.getSession().getAttribute(U_C34__VIEW_TEAM_FORM);
		}

				currentUC34_ViewTeamForm = (UC34_ViewTeamForm)populateDestinationForm(request.getSession(), currentUC34_ViewTeamForm);
		// Call all the Precontroller.
	currentUC34_ViewTeamForm.setAlwaysCallPreControllers(true);
		model.addAttribute(U_C34__VIEW_TEAM_FORM, currentUC34_ViewTeamForm);
		request.getSession().setAttribute(U_C34__VIEW_TEAM_FORM, currentUC34_ViewTeamForm);
		
	}
	
			
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("UC34_ViewTeamController [ ");
			strBToS.append(TEAM_DETAILS);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(teamDetails);
			strBToS.append(END_TO_STRING_DELIMITER);
				strBToS.append(LISTPLAYERS);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(listplayers);
			strBToS.append(END_TO_STRING_DELIMITER);
					// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(uC34_ViewTeamValidator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsUC34_ViewTeamController!=null); 
		return strBToS.toString();
	}
}
