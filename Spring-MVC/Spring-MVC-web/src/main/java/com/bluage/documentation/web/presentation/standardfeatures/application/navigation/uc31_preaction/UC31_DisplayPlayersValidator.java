/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.application.navigation.uc31_preaction ;


import org.apache.log4j.Logger;
import org.springframework.validation.Errors;

import com.netfective.bluage.core.validator.AbstractValidator;
/**
* Class :UC31_DisplayPlayersValidator
*/
public class UC31_DisplayPlayersValidator extends AbstractValidator{
	
	// LOGGER for the class UC31_DisplayPlayersValidator
	private static final Logger LOGGER = Logger.getLogger( UC31_DisplayPlayersValidator.class);
	
	
	/**
	* Operation validate for UC31_DisplayPlayersForm
	* @param obj : the current form (UC31_DisplayPlayersForm)
	* @param errors : The spring errors to return for the form UC31_DisplayPlayersForm
	*/
	public void validate(final Object obj,final Errors errors){
		LOGGER.info("Beginning method : validate"); // UC31_DisplayPlayersValidator
		UC31_DisplayPlayersForm cUC31_DisplayPlayersForm = (UC31_DisplayPlayersForm)obj; // UC31_DisplayPlayersValidator
		LOGGER.info("Ending method : validate the form "+ cUC31_DisplayPlayersForm.getClass().getName()); // UC31_DisplayPlayersValidator
	}

	/**
	* Method to implements to use spring validators (UC31_DisplayPlayersForm)
	* @param aClass : Class for the form UC31_DisplayPlayersForm
	* @return boolean
	*/
	public boolean supports(final Class aClass){
		return (new UC31_DisplayPlayersForm()).getClass().equals(aClass); // UC31_DisplayPlayersValidator
	}
}
