/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.service;


import org.apache.log4j.Logger;

import com.netfective.bluage.core.exception.ApplicationException;
import com.netfective.bluage.core.vo.CopyUtil;


/**
 * Abstract Copy Service Definition
 *
 */
 public abstract class AbstractVOService<T,U> extends AbstractService{

	/**
	 * The logger used for errors and exceptions management of the class
	 */
	private static final Logger LOGGER = Logger.getLogger("AbstractVOService.class");
	
	/**
	 * BO
	 * @return
	 */
	public abstract Class<? extends T> getRefBo();
	
	/**
	 * VO
	 * @return
	 */
	public abstract Class<? extends U> getRefVo();
	
	/**
	 * copyVO2BO
	 * @param vo
	 * @return
	 * @throws ApplicationException
	 */
	public T copyVO2BO(U vo) throws ApplicationException {
		T dstObject;
		try {
			dstObject = this.getRefBo().newInstance();
		} catch (InstantiationException e) {
LOGGER.error(e.getMessage(), e);
			throw new ApplicationException(e);
		} catch (IllegalAccessException e) {
			LOGGER.error(e.getMessage(), e);
			throw new ApplicationException(e);
		}
		CopyUtil.copyBeanToDest(vo, dstObject);
		return dstObject;
	}
	
	/**
	 * copyBO2VO
	 * @param bo
	 * @return
	 * @throws ApplicationException
	 */
	public U copyBO2VO(T bo)
	throws ApplicationException {
		U dstObject;
		try {
		dstObject = this.getRefVo().newInstance();
		} catch (InstantiationException e) {
LOGGER.error(e.getMessage(), e);
			throw new ApplicationException(e);
		} catch (IllegalAccessException e) {
			LOGGER.error(e.getMessage(), e);
			throw new ApplicationException(e);
		}
		CopyUtil.copyBeanToDest(bo, dstObject);
		return dstObject;
	}
}
