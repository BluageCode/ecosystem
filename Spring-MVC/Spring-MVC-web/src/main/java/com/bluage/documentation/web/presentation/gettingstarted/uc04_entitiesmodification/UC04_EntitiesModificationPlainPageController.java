/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.gettingstarted.uc04_entitiesmodification ;

// Import declaration.
// Java imports.
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.bluage.documentation.business.gettingstarted.uc04_entitiesmodification.bos.Player04BO;
import com.bluage.documentation.business.gettingstarted.uc04_entitiesmodification.entities.daofinder.Position04DAOFinderImpl;
import com.bluage.documentation.service.gettingstarted.uc04_entitiesmodification.ServicePlayer04;
import com.netfective.bluage.core.exception.ApplicationException;
import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;
import com.netfective.bluage.core.web.IForm;

/**
 * Class : UC04_EntitiesModificationPlainPageController
 */
@Controller
@Scope("prototype")
@SessionAttributes("uC04_EntitiesModificationPlainPageForm")
@RequestMapping(value= "/presentation/gettingstarted/uc04_entitiesmodification")
public class UC04_EntitiesModificationPlainPageController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC04_EntitiesModificationPlainPageController.class);
	
	//Current form name
	private static final String U_C04__ENTITIES_MODIFICATION_PLAIN_PAGE_FORM = "uC04_EntitiesModificationPlainPageForm";

		//CONSTANT: allPositions
	private static final String ALL_POSITIONS = "allPositions";
				//CONSTANT: playerToUpdate
	private static final String PLAYER_TO_UPDATE = "playerToUpdate";
	
	/**
	 * Property:customDateEditorsUC04_EntitiesModificationPlainPageController
	 */
	final private Map<String,CustomDateEditor> customDateEditorsUC04_EntitiesModificationPlainPageController = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:uC04_EntitiesModificationPlainPageValidator
	 */
	final private UC04_EntitiesModificationPlainPageValidator uC04_EntitiesModificationPlainPageValidator = new UC04_EntitiesModificationPlainPageValidator();
	
	/**
	 * Service declaration : servicePlayer04.
	 */
	@Autowired
	private ServicePlayer04 servicePlayer04;
	
	/**
	 * Generic Finder : position04DAOFinderImpl.
	 */
	@Autowired
	private Position04DAOFinderImpl position04DAOFinderImpl;
	
	
	// Initialise all the instances for UC04_EntitiesModificationPlainPageController
			// Initialize the instance allPositions for the state btn_cancel
		private List allPositions; // Initialize the instance allPositions for UC04_EntitiesModificationPlainPageController
						// Declare the instance playerToUpdate
		private Player04BO playerToUpdate;
			/**
	 * Operation : lnk_update
 	 * @param model : 
 	 * @param uC04_EntitiesModificationPlainPage : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC04_EntitiesModificationPlainPage/lnk_update.html",method = RequestMethod.POST)
	public String lnk_update(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC04_EntitiesModificationPlainPageForm") UC04_EntitiesModificationPlainPageForm  uC04_EntitiesModificationPlainPageForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : lnk_update"); 
		infoLogger(uC04_EntitiesModificationPlainPageForm); 
		uC04_EntitiesModificationPlainPageForm.setAlwaysCallPreControllers(false); 
		// initialization for lnk_update
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state lnk_update if the validation fail, you will be forward to the screen : "/presentation/gettingstarted/uc04_entitiesmodification/UC04_EntitiesModificationPlainPage"
		if(errorsMessages(request,response,uC04_EntitiesModificationPlainPageForm,bindingResult,"", uC04_EntitiesModificationPlainPageValidator, customDateEditorsUC04_EntitiesModificationPlainPageController)){ 
			return "/presentation/gettingstarted/uc04_entitiesmodification/UC04_EntitiesModificationPlainPage"; 
		}

					 callUpdatePlayer(request,uC04_EntitiesModificationPlainPageForm );
											// Init the destination form com.bluage.documentation.web.presentation.gettingstarted.uc04_entitiesmodification.UC04_EntitiesModificationForm or not from lnk_update
			final UC04_EntitiesModificationForm uC04_EntitiesModificationForm =  new UC04_EntitiesModificationForm();
			// Populate the destination form with all the instances returned from lnk_update.
			final IForm uC04_EntitiesModificationForm2 = populateDestinationForm(request.getSession(), uC04_EntitiesModificationForm); 
					
			// Add the form to the model.
			model.addAttribute("uC04_EntitiesModificationForm", uC04_EntitiesModificationForm2); 
			
			request.getSession().setAttribute("uC04_EntitiesModificationForm", uC04_EntitiesModificationForm2);
			
			// "OK" CASE => destination screen path from lnk_update
			LOGGER.info("Go to the screen 'UC04_EntitiesModification'.");
			// Redirect (PRG) from lnk_update
			destinationPath =  "redirect:/presentation/gettingstarted/uc04_entitiesmodification/UC04_EntitiesModification.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_update 
	}
	
	
	/**
	* This method initialise the form : UC04_EntitiesModificationPlainPageForm 
	* @return UC04_EntitiesModificationPlainPageForm
	*/
	@ModelAttribute("UC04_EntitiesModificationPlainPageFormInit")
	public void initUC04_EntitiesModificationPlainPageForm(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, U_C04__ENTITIES_MODIFICATION_PLAIN_PAGE_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method UC04_EntitiesModificationPlainPageForm."); //for lnk_update 
		}
		UC04_EntitiesModificationPlainPageForm uC04_EntitiesModificationPlainPageForm;
	
		if(request.getSession().getAttribute(U_C04__ENTITIES_MODIFICATION_PLAIN_PAGE_FORM) != null){
			uC04_EntitiesModificationPlainPageForm = (UC04_EntitiesModificationPlainPageForm)request.getSession().getAttribute(U_C04__ENTITIES_MODIFICATION_PLAIN_PAGE_FORM);
		} else {
			uC04_EntitiesModificationPlainPageForm = new UC04_EntitiesModificationPlainPageForm();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form UC04_EntitiesModificationPlainPageForm.");
		}
		uC04_EntitiesModificationPlainPageForm = (UC04_EntitiesModificationPlainPageForm)populateDestinationForm(request.getSession(), uC04_EntitiesModificationPlainPageForm);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  UC04_EntitiesModificationPlainPageForm.");
		}
		model.addAttribute(U_C04__ENTITIES_MODIFICATION_PLAIN_PAGE_FORM, uC04_EntitiesModificationPlainPageForm);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param UC04_EntitiesModificationPlainPageForm : The sceen form.
	 */
	@RequestMapping(value = "/UC04_EntitiesModificationPlainPage.html" ,method = RequestMethod.GET)
	public void prepareUC04_EntitiesModificationPlainPage(final HttpServletRequest request,final  Model model,final  @ModelAttribute("UC04_EntitiesModificationPlainPageFormInit") UC04_EntitiesModificationPlainPageForm uC04_EntitiesModificationPlainPageForm){
		
		UC04_EntitiesModificationPlainPageForm currentUC04_EntitiesModificationPlainPageForm = uC04_EntitiesModificationPlainPageForm;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(U_C04__ENTITIES_MODIFICATION_PLAIN_PAGE_FORM) == null){
			if(currentUC04_EntitiesModificationPlainPageForm!=null){
				request.getSession().setAttribute(U_C04__ENTITIES_MODIFICATION_PLAIN_PAGE_FORM, currentUC04_EntitiesModificationPlainPageForm);
			}else {
				currentUC04_EntitiesModificationPlainPageForm = new UC04_EntitiesModificationPlainPageForm();
				request.getSession().setAttribute(U_C04__ENTITIES_MODIFICATION_PLAIN_PAGE_FORM, currentUC04_EntitiesModificationPlainPageForm);	
			}
		} else {
			currentUC04_EntitiesModificationPlainPageForm = (UC04_EntitiesModificationPlainPageForm) request.getSession().getAttribute(U_C04__ENTITIES_MODIFICATION_PLAIN_PAGE_FORM);
		}

		try {
			List allPositions = position04DAOFinderImpl.findAll();
			put(request.getSession(), ALL_POSITIONS, allPositions);
		} catch (ApplicationException e) {
			errorLogger("An error while calling the generic finder for the instance : allPositions.",e);
		}
		currentUC04_EntitiesModificationPlainPageForm = (UC04_EntitiesModificationPlainPageForm)populateDestinationForm(request.getSession(), currentUC04_EntitiesModificationPlainPageForm);
		// Call all the Precontroller.
	currentUC04_EntitiesModificationPlainPageForm.setAlwaysCallPreControllers(true);
		model.addAttribute(U_C04__ENTITIES_MODIFICATION_PLAIN_PAGE_FORM, currentUC04_EntitiesModificationPlainPageForm);
		request.getSession().setAttribute(U_C04__ENTITIES_MODIFICATION_PLAIN_PAGE_FORM, currentUC04_EntitiesModificationPlainPageForm);
		
	}
	
				/**
	 * method callUpdatePlayer
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callUpdatePlayer(HttpServletRequest request,IForm form) {
					playerToUpdate = (Player04BO)get(request.getSession(),form, "playerToUpdate");  
										 
					try {
				// executing UpdatePlayer in lnk_update
	servicePlayer04.player04Update(
			playerToUpdate
			);  

													// processing variables UpdatePlayer in lnk_update
				put(request.getSession(), PLAYER_TO_UPDATE,playerToUpdate); 
			
			} catch (ApplicationException e) { 
				// error handling for operation player04Update called UpdatePlayer
				errorLogger("An error occured during the execution of the operation : player04Update",e); 
		
			}
	}
		/**
	* This method Binder.
	* @param binder : 
	*		 WebDataBinder
	*/
	@InitBinder
	protected void initBinder(final WebDataBinder binder){
			customDateEditorsUC04_EntitiesModificationPlainPageController.put("playerToUpdate.dateOfBirth", new CustomDateEditor(new SimpleDateFormat("MM/dd/yyyy"), true, "Invalid format."));
			binder.registerCustomEditor(Date.class, "playerToUpdate.dateOfBirth", customDateEditorsUC04_EntitiesModificationPlainPageController.get("playerToUpdate.dateOfBirth"));
	}
	
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("UC04_EntitiesModificationPlainPageController [ ");
				strBToS.append(ALL_POSITIONS);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(allPositions);
			strBToS.append(END_TO_STRING_DELIMITER);
						strBToS.append(PLAYER_TO_UPDATE);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(playerToUpdate);
			strBToS.append(END_TO_STRING_DELIMITER);
		// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(uC04_EntitiesModificationPlainPageValidator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsUC04_EntitiesModificationPlainPageController!=null); 
		return strBToS.toString();
	}
}
