/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.netfective.bluage.springmvc.ajax;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.transform.TransformerException;

import org.apache.log4j.Logger;

import com.netfective.bluage.springmvc.ajax.util.AjaxUtils;



/**
 * Class AjaxFilter
 * @interface Filter
 * @author NETFECTIVE TECHNOLOGY
 */
public class AjaxFilter implements Filter {
    /**
     * LOGGER.
     */
    private static final  Logger LOGGER = Logger.getLogger(AjaxFilter.class);

	/**
	 * Called by the web container to indicate to a filter that it is being placed into service.
	 * @param filterConfig
	 */
	public void init(final FilterConfig filterConfig) throws ServletException {
	}

	/**
	 * The doFilter method of the Filter is called by the container each time a request/response pair is passed through the chain
	 * due to a client request for a resource at the end of the chain.
	 * 
	 * @param servletRequest ServletRequest
	 * @param servletResponse ServletResponse
	 * @param filterChain FilterChain
	 * @throws IOException
	 * @throws ServletException
	 */
	public void doFilter(final ServletRequest servletRequest,
			final ServletResponse servletResponse, final FilterChain filterChain)
			throws IOException, ServletException {

		final HttpServletRequest request = (HttpServletRequest) servletRequest;
		final HttpServletResponse response = (HttpServletResponse) servletResponse;

		// If isn t ajax request continue filter chain
		if (!AjaxUtils.isAjaxRequest(servletRequest)) {
			filterChain.doFilter(servletRequest, response);
			return;
		}

		AjaxUtils.addAjaxCapabilities(request, response);

		final BufferResponseWrapper bufferResponseWrapper = new BufferResponseWrapper(response);

		final Set refreshZones = new HashSet();
		AjaxUtils.setZonesToRefresh(request, refreshZones);
		AjaxUtils.getRefreshZonesFromURL(request);
		
		try {
			filterChain.doFilter(request, bufferResponseWrapper);

			if (bufferResponseWrapper.getRedirect() != null) {
				XMLHandler.sendRedirect(bufferResponseWrapper);
			} else {
				XMLHandler.sendZones(bufferResponseWrapper, refreshZones);
			}
		} 
		catch (ServletException e) {
			LOGGER.error(e.getMessage(), e);
			XMLHandler.handleError(response, e);
		}
		catch (IOException e) {
			LOGGER.error(e.getMessage(), e);
			XMLHandler.handleError(response, e);
		}
		catch (TransformerException e) {
			LOGGER.error(e.getMessage(), e);
			XMLHandler.handleError(response, e);
		}
	}

	/**
	 * Called by the web container to indicate to a filter that it is being taken out of service.
	 */
	public void destroy() {
	}
}

