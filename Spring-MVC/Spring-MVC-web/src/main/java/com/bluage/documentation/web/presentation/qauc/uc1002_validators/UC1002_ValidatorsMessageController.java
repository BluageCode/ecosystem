/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.qauc.uc1002_validators ;

// Import declaration.
// Java imports.
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.bluage.documentation.service.qauc.uc1002_validators.ServicePlayer1002;
import com.netfective.bluage.core.exception.ApplicationException;
import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;
import com.netfective.bluage.core.web.IForm;

/**
 * Class : UC1002_ValidatorsMessageController
 */
@Controller
@Scope("prototype")
@SessionAttributes("uC1002_ValidatorsMessageForm")
@RequestMapping(value= "/presentation/qauc/uc1002_validators")
public class UC1002_ValidatorsMessageController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC1002_ValidatorsMessageController.class);
	
	//Current form name
	private static final String U_C1002__VALIDATORS_MESSAGE_FORM = "uC1002_ValidatorsMessageForm";

	
	/**
	 * Property:customDateEditorsUC1002_ValidatorsMessageController
	 */
	final private Map<String,CustomDateEditor> customDateEditorsUC1002_ValidatorsMessageController = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:uC1002_ValidatorsMessageValidator
	 */
	final private UC1002_ValidatorsMessageValidator uC1002_ValidatorsMessageValidator = new UC1002_ValidatorsMessageValidator();
	
	/**
	 * Service declaration : servicePlayer1002.
	 */
	@Autowired
	private ServicePlayer1002 servicePlayer1002;
	
	
	// Initialise all the instances for UC1002_ValidatorsMessageController
					/**
	 * Operation : lnk_back
 	 * @param model : 
 	 * @param uC1002_ValidatorsMessage : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC1002_ValidatorsMessage/lnk_back.html",method = RequestMethod.GET)
	public String lnk_back(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC1002_ValidatorsMessageForm") UC1002_ValidatorsMessageForm  uC1002_ValidatorsMessageForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : lnk_back"); 
		infoLogger(uC1002_ValidatorsMessageForm); 
		uC1002_ValidatorsMessageForm.setAlwaysCallPreControllers(false); 
		// initialization for lnk_back
		init();
		
		String destinationPath = null; 
		

	 callGoBack(request,uC1002_ValidatorsMessageForm );
											// Init the destination form com.bluage.documentation.web.presentation.qauc.uc1002_validators.UC1002_ValidatorsForm or not from lnk_back
			final UC1002_ValidatorsForm uC1002_ValidatorsForm =  new UC1002_ValidatorsForm();
			// Populate the destination form with all the instances returned from lnk_back.
			final IForm uC1002_ValidatorsForm2 = populateDestinationForm(request.getSession(), uC1002_ValidatorsForm); 
					
			// Add the form to the model.
			model.addAttribute("uC1002_ValidatorsForm", uC1002_ValidatorsForm2); 
			
			request.getSession().setAttribute("uC1002_ValidatorsForm", uC1002_ValidatorsForm2);
			
			// "OK" CASE => destination screen path from lnk_back
			LOGGER.info("Go to the screen 'UC1002_Validators'.");
			// Redirect (PRG) from lnk_back
			destinationPath =  "redirect:/presentation/qauc/uc1002_validators/UC1002_Validators.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_back 
	}
	
	
	/**
	* This method initialise the form : UC1002_ValidatorsMessageForm 
	* @return UC1002_ValidatorsMessageForm
	*/
	@ModelAttribute("UC1002_ValidatorsMessageFormInit")
	public void initUC1002_ValidatorsMessageForm(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, U_C1002__VALIDATORS_MESSAGE_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method UC1002_ValidatorsMessageForm."); //for lnk_back 
		}
		UC1002_ValidatorsMessageForm uC1002_ValidatorsMessageForm;
	
		if(request.getSession().getAttribute(U_C1002__VALIDATORS_MESSAGE_FORM) != null){
			uC1002_ValidatorsMessageForm = (UC1002_ValidatorsMessageForm)request.getSession().getAttribute(U_C1002__VALIDATORS_MESSAGE_FORM);
		} else {
			uC1002_ValidatorsMessageForm = new UC1002_ValidatorsMessageForm();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form UC1002_ValidatorsMessageForm.");
		}
		uC1002_ValidatorsMessageForm = (UC1002_ValidatorsMessageForm)populateDestinationForm(request.getSession(), uC1002_ValidatorsMessageForm);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  UC1002_ValidatorsMessageForm.");
		}
		model.addAttribute(U_C1002__VALIDATORS_MESSAGE_FORM, uC1002_ValidatorsMessageForm);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param UC1002_ValidatorsMessageForm : The sceen form.
	 */
	@RequestMapping(value = "/UC1002_ValidatorsMessage.html" ,method = RequestMethod.GET)
	public void prepareUC1002_ValidatorsMessage(final HttpServletRequest request,final  Model model,final  @ModelAttribute("UC1002_ValidatorsMessageFormInit") UC1002_ValidatorsMessageForm uC1002_ValidatorsMessageForm){
		
		UC1002_ValidatorsMessageForm currentUC1002_ValidatorsMessageForm = uC1002_ValidatorsMessageForm;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(U_C1002__VALIDATORS_MESSAGE_FORM) == null){
			if(currentUC1002_ValidatorsMessageForm!=null){
				request.getSession().setAttribute(U_C1002__VALIDATORS_MESSAGE_FORM, currentUC1002_ValidatorsMessageForm);
			}else {
				currentUC1002_ValidatorsMessageForm = new UC1002_ValidatorsMessageForm();
				request.getSession().setAttribute(U_C1002__VALIDATORS_MESSAGE_FORM, currentUC1002_ValidatorsMessageForm);	
			}
		} else {
			currentUC1002_ValidatorsMessageForm = (UC1002_ValidatorsMessageForm) request.getSession().getAttribute(U_C1002__VALIDATORS_MESSAGE_FORM);
		}

		currentUC1002_ValidatorsMessageForm = (UC1002_ValidatorsMessageForm)populateDestinationForm(request.getSession(), currentUC1002_ValidatorsMessageForm);
		// Call all the Precontroller.
	currentUC1002_ValidatorsMessageForm.setAlwaysCallPreControllers(true);
		model.addAttribute(U_C1002__VALIDATORS_MESSAGE_FORM, currentUC1002_ValidatorsMessageForm);
		request.getSession().setAttribute(U_C1002__VALIDATORS_MESSAGE_FORM, currentUC1002_ValidatorsMessageForm);
		
	}
	
	/**
	 * method callGoBack
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callGoBack(HttpServletRequest request,IForm form) {
					try {
				// executing GoBack in lnk_back
	servicePlayer1002.back(
	);  

								// processing variables GoBack in lnk_back

			} catch (ApplicationException e) { 
				// error handling for operation back called GoBack
				errorLogger("An error occured during the execution of the operation : back",e); 
		
			}
	}
		
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("UC1002_ValidatorsMessageController [ ");
		// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(uC1002_ValidatorsMessageValidator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsUC1002_ValidatorsMessageController!=null); 
		return strBToS.toString();
	}
}
