/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.netfective.bluage.core.vo;


import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang.StringUtils;

import com.netfective.bluage.core.exception.ApplicationException;

/**
 * ConvertField : helper to convert from objects to string and vice versa
 * 
 * @author netfective
 *         NOTICE : Due to Sun's compiler bug 6481655 (see
 *         http://bugs.sun.com/bugdatabase/view_bug.do?bug_id=6481655) :
 *         Util.<Object>cast(null); => Compiles OK
 *         (Util.<Object>cast(null)).getClass(); => Does not compile (produces
 *         Util.java:9: illegal start of expression
 *         (Util.<Object>cast(null)).getClass();
 * 
 *         It is not possible to make direct returns within some of the methods
 *         below. Assigning an intermediate variable is mandatory in order to
 *         keep the code compilable with jdk 6 compiler; This may lead to
 *         unwanted code quality static analyzer warning ( typically :
 *         "unnecessary local before return")
 * 
 *         An identified workaround is to use a jdk 7 compiler in jdk 6
 *         compatibility mode
 * 
 */
public final class ConvertField {

	/**
	 * private constructor for ConvertField
	 */
	private ConvertField(){}
	
/**
	 * isValidNumFieldString
	 * 
	 * @param testString
	 * @param pattern
	 * @return
	 */
	public static Boolean isValidNumFieldString(final String testString,
			final String pattern) {
		Boolean result = null;
		try {
			final DecimalFormat sdformat = new DecimalFormat(pattern);
			final String resultString = sdformat.format(sdformat.parse(testString));
			result = testString.equals(resultString);
		} catch (ParseException e) {
			result = Boolean.FALSE;
		}
		return result;
	}

/**
	 * isValidDateFieldString
	 * 
	 * @param testString
	 * @param pattern
	 * @return
	 */
	public static Boolean isValidDateFieldString(final String testString,
			final String pattern) {
		Boolean result = null;
		try {
			if (testString != null) {
				final SimpleDateFormat sdformat = new SimpleDateFormat(pattern);
				final String resultString = sdformat.format(sdformat
						.parse(testString));
				result = testString.equals(resultString);
			} else {
				result = Boolean.FALSE;
			}
		} catch (ParseException e) {
			result = Boolean.FALSE;
		}
		return result;
	}

	/**
	 * convertStringToLongField
	 * 
	 * @param testString
	 * @param pattern
	 * @return
	 * @throws ApplicationException
	 */
	public static long convertStringToLongField(final String testString,
			final String pattern) throws ApplicationException {
		// Please see note in cartridge about sun's compiler bug preventing from
		// writing a direct return
		final Long long1 = ConvertField.<Long>convertStringToNumberField(testString,
				pattern);
		return long1.longValue();
	}

	/**
	 * convertStringToIntField
	 * 
	 * @param testString
	 * @param pattern
	 * @return
	 * @throws ApplicationException
	 */
	public static int convertStringToIntField(final String testString, final String pattern)
			throws ApplicationException {
		// Please see note in cartridge about sun's compiler bug preventing from
		// writing a direct return
		final Number convertStringToNumberField = ConvertField
				.<Long>convertStringToNumberField(testString, pattern);
		return convertStringToNumberField.intValue();
	}

	/**
	 * convertStringToIntField
	 * 
	 * @param testString
	 * @param pattern
	 * @return
	 * @throws ApplicationException
	 */
	public static Integer convertStringToIntegerField(final String testString,
			final String pattern) throws ApplicationException {
		return convertStringToIntField(testString,pattern);
	}

	/**
	 * convertStringToIntField
	 * 
	 * @param testString
	 * @param pattern
	 * @return
	 * @throws ApplicationException
	 */
	public static float convertStringToFloatField(final String testString,
			final String pattern) throws ApplicationException {
		// Please see note in cartridge about sun's compiler bug preventing from
		// writing a direct return
		final Number convertStringToNumberField = ConvertField
				.<Number>convertStringToNumberField(testString, pattern);
		return convertStringToNumberField.floatValue();
	}


	/**
	 * convertStringToDateField
	 * 
	 * @param testString
	 * @param pattern
	 * @return
	 * @throws ApplicationException
	 */

	public static Date convertStringToDateField(final String testString,
			final String pattern) throws ApplicationException {		
		try {
			return (Date) new SimpleDateFormat(pattern).parse(testString);
		} catch (ParseException e) {
			throw new ApplicationException(e);
		}
	}



	/**
	 * convertStringToBigDecimalField
	 * 
	 * @param testString
	 * @param pattern
	 * @return
	 * @throws ApplicationException
	 */
	public static BigDecimal convertStringToBigDecimalField(final String testString,
			final String pattern) throws ApplicationException {
		// Please see note in cartridge about sun's compiler bug preventing from
		// writing a direct return
		final Number convertStringToNumberField = ConvertField
				.<Number>convertStringToNumberField(testString, pattern);
		return BigDecimal.valueOf(convertStringToNumberField.doubleValue());
	}

	/**
	 * convertStringToBigDecimalField
	 * 
	 * @param testString
	 * @param pattern
	 * @return
	 * @throws ApplicationException
	 */
	public static BigInteger convertStringToBigIntegerField(final String testString,
			final String pattern) throws ApplicationException {
		return new BigInteger(testString);
		
	}

	/**
	 * convertStringToTimestampField
	 * 
	 * @param testString
	 * @param pattern
	 * @return
	 * @throws ApplicationException
	 */

	public static java.sql.Timestamp convertStringToTimestampField(final String testString,
			final String pattern) throws ApplicationException {
		try {
			return (java.sql.Timestamp) new SimpleDateFormat(pattern).parse(testString);
		} catch (ParseException e) {
			throw new ApplicationException(e);
		}
	}
	
		/**
	 * convertStringToDateTimeField
	 * @param testString
	 * @param pattern
	 * @return
	 * @throws ApplicationException
	 */
	public static java.sql.Timestamp convertStringToDateTimeField(final String testString,final String pattern) throws ApplicationException{
		return convertStringToTimestampField(testString,pattern); 
	}
	
	/**
	 * convertStringToTimeField
	 * @param testString
	 * @param pattern
	 * @return
	 * @throws ApplicationException
	 */
	public static Date convertStringToTimeField(final String testString,final String pattern) throws ApplicationException{
		return convertStringToDateField(testString,pattern);
	}

	
	/**
	 * convertStringToDoubleField
	 * 
	 * @param testString
	 * @param pattern
	 * @return
	 * @throws ApplicationException
	 */

	public static double convertStringToDoubleField(final String testString,
			final String pattern) throws ApplicationException {
		// Please see note in cartridge about sun's compiler bug preventing from
		// writing a direct return
		final Number convertStringToNumberField = ConvertField
				.<Number>convertStringToNumberField(testString, pattern);
		return convertStringToNumberField.doubleValue();
	}
	
		/**
	 * convertStringToDecimalField
	 * @param testString
	 * @param pattern
	 * @return
	 * @throws ApplicationException
	 */
	public static Double convertStringToDecimalField(final String testString,final String pattern)  throws ApplicationException{
		return convertStringToDoubleField(testString,pattern);
	}

	/**
	 * convertStringToDoubleField
	 * 
	 * @param testString
	 * @param pattern
	 * @return
	 * @throws ApplicationException
	 */

	public static short convertStringToShortField(final String testString,
			final String pattern) throws ApplicationException {
				// Please see note in cartridge about sun's compiler bug preventing from
		// writing a direct return
		final Long convertStringToNumberField = ConvertField
				.<Long>convertStringToNumberField(testString, pattern);
		return convertStringToNumberField.shortValue();
	}
	
	/**
	 * convertStringToByteField
	 * @param testString
	 * @param pattern
	 * @return
	 * @throws ApplicationException
	 */
	public static byte convertStringToByteField(final String testString,
			final String pattern) throws ApplicationException{
		// Please see note in cartridge about sun's compiler bug preventing from
		// writing a direct return
		final Long convertStringToNumberField = ConvertField
				.<Long>convertStringToNumberField(testString, pattern);
		return convertStringToNumberField.byteValue();
	}
	
		/**
	 * convertStringToBlobField
	 * @param testString
	 * @param pattern
	 * @return
	 * @throws ApplicationException
	 */
	public static byte[] convertStringToBlobField(final String testString,
			final String pattern)  {
		return testString.getBytes();
	}
	
	/**
	 * convertStringToClobField
	 * @param testString
	 * @param pattern
	 * @return
	 * @throws ApplicationException
	 */
	public static String convertStringToClobField(final String testString,
			final String pattern) throws ApplicationException {
		return testString;
	}
	
		/**
	 * convertStringToCharacterField
	 * @param testString
	 * @param pattern
	 * @return
	 */
	public static Character convertStringToCharacterField(final String testString,final String pattern) {
		return convertStringToCharField(testString,pattern);
	}
	
		/**
	 * convertStringToCharacterField
	 * @param testString
	 * @param pattern
	 * @return
	 */
	public static char convertStringToCharField(final String testString,final String pattern) {
		Character result = null;
		final char ch = Character.MIN_VALUE;
		if(testString !=null && testString.length()>0){
			result = testString.charAt(0);
		} else {
			result = ch;
		}
		return result;
	}
	
	/**
	 * convertStringToField
	 * 
	 * @param <T>
	 * @param testString
	 * @param pattern
	 * @return
	 * @throws ApplicationException 
	 * @throws ApplicationException
	 */
	public static <T extends Number> T convertStringToNumberField(
			final String testString,final String pattern) throws ApplicationException {
		try {
			return (T) new DecimalFormat(pattern).parse(testString);
		} catch (ParseException e) {
			throw new ApplicationException(e);
		}
	}
	
	/**
	 * fieldConvertToString
	 * 
	 * @param input
	 * @param pattern
	 * @return
	 * @throws ApplicationException
	 */

	public static String fieldConvertToString(final Object input, final String pattern) {
		String returnString = StringUtils.EMPTY;
		if(input !=null){
			returnString = new DecimalFormat(pattern).format(input);
		}
		return returnString;
	}
	

	/**
	 * fieldDateConvertToString
	 * 
	 * @param input
	 * @param pattern
	 * @return
	 * @throws ApplicationException
	 */
	public static String fieldDateConvertToString(final Date input,final String pattern)
			throws ApplicationException {
		String returnString = StringUtils.EMPTY;
		if (input != null) {
			returnString = new SimpleDateFormat(pattern).format(input);
		}
		return returnString;
	}
	
	/**
	 * fieldByteArrayConvertToString
	 * 
	 * @param input
	 * @param pattern
	 * @return
	 * @throws ApplicationException
	 */
	public static String fieldByteArrayConvertToString(final byte[] input, final String pattern) throws ApplicationException {
		return new String(input);
	}
}

