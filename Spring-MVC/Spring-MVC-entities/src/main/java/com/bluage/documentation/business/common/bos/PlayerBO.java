/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.business.common.bos;

import java.util.Date;

import javax.inject.Inject;
import javax.inject.Named;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import com.bluage.documentation.business.common.entities.daofinder.PlayerDAOFinderImpl;

/**
 * PlayerBO
 *
* @see com.netfective.bluage.core.business.IBusinessObject
* * @see PersonBO
 */
@XmlRootElement

public class PlayerBO
 extends PersonBO {
	
	/**
	 * Default SUID for PlayerBO.
	 */
	private static final long serialVersionUID = 1L;
	

	@Inject
	@Named ("com.bluage.documentation.business.common.entities.daofinder.PlayerFinder") 
	private PlayerDAOFinderImpl finder;

						/**
	 * Entity property: dateOfBirth
	 *
	 */
private Date dateOfBirth;

					/**
	 * Entity property: estimatedValue
	 *
	 */
private Float estimatedValue;

					/**
	 * Entity property: height
	 *
	 */
private Integer height;

					/**
	 * Entity property: weight
	 *
	 */
private Integer weight;

					/**
	 * Entity property: rookie
	 *
	 */
private Boolean rookie = Boolean.FALSE;



   /**
	* public default constructor for class PlayerBO
	*/
	public PlayerBO(){
		//NOP PlayerBO
	}


						/**
	 * Getter for dateOfBirth of class PlayerBO.
	 *
	 * @return {@link java.util.Date}
	 * @generated
	 */ 
public  Date getDateOfBirth() {
	return this.dateOfBirth;
	}


	/**
	 * Setter for dateOfBirth of class PlayerBO.
	 * @param value {@link java.util.Date}
	 * @generated
	 */

public  void setDateOfBirth(final Date dateOfBirth) {
	this.dateOfBirth = dateOfBirth;
}

					/**
	 * Getter for estimatedValue of class PlayerBO.
	 *
	 * @return {@link java.lang.Float}
	 * @generated
	 */ 
public  Float getEstimatedValue() {
	return this.estimatedValue;
	}


	/**
	 * Setter for estimatedValue of class PlayerBO.
	 * @param value {@link java.lang.Float}
	 * @generated
	 */

public  void setEstimatedValue(final Float estimatedValue) {
	this.estimatedValue = estimatedValue;
}

					/**
	 * Getter for height of class PlayerBO.
	 *
	 * @return {@link java.lang.Integer}
	 * @generated
	 */ 
public  Integer getHeight() {
	return this.height;
	}


	/**
	 * Setter for height of class PlayerBO.
	 * @param value {@link java.lang.Integer}
	 * @generated
	 */

public  void setHeight(final Integer height) {
	this.height = height;
}

					/**
	 * Getter for weight of class PlayerBO.
	 *
	 * @return {@link java.lang.Integer}
	 * @generated
	 */ 
public  Integer getWeight() {
	return this.weight;
	}


	/**
	 * Setter for weight of class PlayerBO.
	 * @param value {@link java.lang.Integer}
	 * @generated
	 */

public  void setWeight(final Integer weight) {
	this.weight = weight;
}

					/**
	 * Getter for rookie of class PlayerBO.
	 *
	 * @return {@link java.lang.Boolean}
	 * @generated
	 */ 
public  Boolean getRookie() {
	return this.rookie;
	}


	/**
	 * Setter for rookie of class PlayerBO.
	 * @param value {@link java.lang.Boolean}
	 * @generated
	 */

public  void setRookie(final Boolean rookie) {
	this.rookie = rookie;
}


/**
	 * Returns <code>true</code> if the argument is an Player instance and all identifiers for this entity
     * equal the identifiers of the argument entity. The <code>equals</code> method of the parent entity
     * will also need to return <code>true</code>. Returns <code>false</code> otherwise.
     *
     * @see PersonBO#hashCode()
	 * @param object  object to compare with current instance      
     * @return boolean true if equals false if not
     */
    public boolean equals(final Object object )
    {
		if ( this == object )
        {
            return true;
        }
        if ( !( object instanceof PlayerBO ) )
        {
            return false;
        }
        final PlayerBO that = (PlayerBO)object;
    
    	            if (        				        			this.getId() == null
        	        ) {
        	return false;
        }
		return new EqualsBuilder()
			.append(this.getId(), that.getId())
			 	.isEquals();
	}
	/**

	 * Returns a hash code based on this entity's identifiers and the hash code of the parent entity.
     *
     * @see PersonBO#hashCode()
     * @return int
     */    
    public int hashCode() {
        return new HashCodeBuilder()
             .append(getId())
				 	.appendSuper(super.hashCode())
            .toHashCode();
    }
}

