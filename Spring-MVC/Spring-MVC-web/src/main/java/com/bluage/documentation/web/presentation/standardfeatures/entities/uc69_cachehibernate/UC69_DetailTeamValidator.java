/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.entities.uc69_cachehibernate ;


import org.apache.log4j.Logger;
import org.springframework.validation.Errors;

import com.netfective.bluage.core.validator.AbstractValidator;
/**
* Class :UC69_DetailTeamValidator
*/
public class UC69_DetailTeamValidator extends AbstractValidator{
	
	// LOGGER for the class UC69_DetailTeamValidator
	private static final Logger LOGGER = Logger.getLogger( UC69_DetailTeamValidator.class);
	
	
	/**
	* Operation validate for UC69_DetailTeamForm
	* @param obj : the current form (UC69_DetailTeamForm)
	* @param errors : The spring errors to return for the form UC69_DetailTeamForm
	*/
	public void validate(final Object obj,final Errors errors){
		LOGGER.info("Beginning method : validate"); // UC69_DetailTeamValidator
		UC69_DetailTeamForm cUC69_DetailTeamForm = (UC69_DetailTeamForm)obj; // UC69_DetailTeamValidator
		LOGGER.info("Ending method : validate the form "+ cUC69_DetailTeamForm.getClass().getName()); // UC69_DetailTeamValidator
	}

	/**
	* Method to implements to use spring validators (UC69_DetailTeamForm)
	* @param aClass : Class for the form UC69_DetailTeamForm
	* @return boolean
	*/
	public boolean supports(final Class aClass){
		return (new UC69_DetailTeamForm()).getClass().equals(aClass); // UC69_DetailTeamValidator
	}
}
