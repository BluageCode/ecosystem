/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.advancedfeatures.externaltools.uc209_javamelody ;

// Import declaration.
// Java imports.
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;

/**
 * Class : UC209_HomePageController
 */
@Controller
@Scope("prototype")
@SessionAttributes("uC209_HomePageForm")
@RequestMapping(value= "/presentation/advancedfeatures/externaltools/uc209_javamelody")
public class UC209_HomePageController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC209_HomePageController.class);
	
	//Current form name
	private static final String U_C209__HOME_PAGE_FORM = "uC209_HomePageForm";

	
	/**
	 * Property:customDateEditorsUC209_HomePageController
	 */
	final private Map<String,CustomDateEditor> customDateEditorsUC209_HomePageController = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:uC209_HomePageValidator
	 */
	final private UC209_HomePageValidator uC209_HomePageValidator = new UC209_HomePageValidator();
	
	
	// Initialise all the instances for UC209_HomePageController

	/**
	* This method initialise the form : UC209_HomePageForm 
	* @return UC209_HomePageForm
	*/
	@ModelAttribute("UC209_HomePageFormInit")
	public void initUC209_HomePageForm(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, U_C209__HOME_PAGE_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method UC209_HomePageForm."); //for lnk_clear 
		}
		UC209_HomePageForm uC209_HomePageForm;
	
		if(request.getSession().getAttribute(U_C209__HOME_PAGE_FORM) != null){
			uC209_HomePageForm = (UC209_HomePageForm)request.getSession().getAttribute(U_C209__HOME_PAGE_FORM);
		} else {
			uC209_HomePageForm = new UC209_HomePageForm();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form UC209_HomePageForm.");
		}
		uC209_HomePageForm = (UC209_HomePageForm)populateDestinationForm(request.getSession(), uC209_HomePageForm);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  UC209_HomePageForm.");
		}
		model.addAttribute(U_C209__HOME_PAGE_FORM, uC209_HomePageForm);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param UC209_HomePageForm : The sceen form.
	 */
	@RequestMapping(value = "/UC209_HomePage.html" ,method = RequestMethod.GET)
	public void prepareUC209_HomePage(final HttpServletRequest request,final  Model model,final  @ModelAttribute("UC209_HomePageFormInit") UC209_HomePageForm uC209_HomePageForm){
		
		UC209_HomePageForm currentUC209_HomePageForm = uC209_HomePageForm;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(U_C209__HOME_PAGE_FORM) == null){
			if(currentUC209_HomePageForm!=null){
				request.getSession().setAttribute(U_C209__HOME_PAGE_FORM, currentUC209_HomePageForm);
			}else {
				currentUC209_HomePageForm = new UC209_HomePageForm();
				request.getSession().setAttribute(U_C209__HOME_PAGE_FORM, currentUC209_HomePageForm);	
			}
		} else {
			currentUC209_HomePageForm = (UC209_HomePageForm) request.getSession().getAttribute(U_C209__HOME_PAGE_FORM);
		}

		currentUC209_HomePageForm = (UC209_HomePageForm)populateDestinationForm(request.getSession(), currentUC209_HomePageForm);
		// Call all the Precontroller.
	currentUC209_HomePageForm.setAlwaysCallPreControllers(true);
		model.addAttribute(U_C209__HOME_PAGE_FORM, currentUC209_HomePageForm);
		request.getSession().setAttribute(U_C209__HOME_PAGE_FORM, currentUC209_HomePageForm);
		
	}
	
	
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("UC209_HomePageController [ ");
		// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(uC209_HomePageValidator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsUC209_HomePageController!=null); 
		return strBToS.toString();
	}
}
