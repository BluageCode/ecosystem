/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.service.standardfeatures.application.datagrid.uc32_calculabletable;


import javax.inject.Inject;
import javax.inject.Named;

import com.bluage.documentation.business.standardfeatures.application.datagrid.uc32_calculabletable.bos.Player32BO;
import com.bluage.documentation.business.standardfeatures.application.datagrid.uc32_calculabletable.entities.dao.Player32DAOImpl;
import com.bluage.documentation.service.AbstractService;
import com.netfective.bluage.core.exception.ApplicationException;
import com.netfective.bluage.core.persistence.IPersistentObjectDAO;

/**
 * Defines  application services for Serviceplayer32Update
  */
@Named("com.bluage.documentation.service.standardfeatures.application.datagrid.uc32_calculabletable.Serviceplayer32Update")
public class Serviceplayer32Update  extends AbstractService{


	


	/**
	 *  Default constructor 
	 */
	public Serviceplayer32Update() {
		super(); // Call super for default constructor : Serviceplayer32Update
	}
	
			@Inject
	@Named ("com.bluage.documentation.business.standardfeatures.application.datagrid.uc32_calculabletable.entities.dao.Player32DAOImpl")
	private Player32DAOImpl dao;

	/**
	 * constructor with...
	 * 
	 * @param dao
	 *		: {@link IPersistentObjectDAO} the DAO
	 */
	public Serviceplayer32Update(Player32DAOImpl dao) {
		this.dao = dao;
	}

			
	/**    
	 * Operation Crud
	 * @param bo {@link Player32BO}
		 * @return {@link boolean}
	 */
	public boolean player32Update(Player32BO bo )
 throws ApplicationException {

					return dao.update(bo);
	}

	

}
