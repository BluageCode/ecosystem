<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file="/include.jsp"  %>
<html >
<head >
<title  > 
<fmt:message key="cUC41_DropDownAction_BLU_AGE_DOCUMENTATION"/>
</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"  > 
</meta>
<link href="<c:url value= "/presentation/_style/_style.css"/>" rel="stylesheet" type="text/css"  /> 
<link href="<c:url value= "/presentation/_include/jquery/rd_lib.css"/>" rel="stylesheet" type="text/css"  /> 
<script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/jquery.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/rd_setup.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/rd_lib.js"/>" space="preserve"  ></script><link rel="shortcut icon" href="<c:url value= "/presentation/_style/favicon.ico"/>"  /> 

</head>

<body class=" " > 
<div id="rd_ghost"  > 
<small  > 
<fmt:message key="cUC41_DropDownAction___CSS_DEACTIVATED"/>
</small>
</div>
<div id="rd_jghost"  > 
<small  > 
<fmt:message key="cUC41_DropDownAction___JAVASCRIPT_DEACTIVATED"/>
</small>
</div>
<div id="shape"  > 
<div class="ui-layout-center"  > 
<div class="east-north"  > 
	<form:form id="uC41_DropDownAction" commandName="uC41_DropDownActionForm"  enctype="application/x-www-form-urlencoded" method="post">
<form:hidden id="hid_positions" path="positions.toRemove"/>
<h2 class="rd_first"  > 
<fmt:message key="cUC41_DropDownAction_STANDARD_FEATURES__APPLICATIO"/>
</h2>
<fmt:message key="cUC41_DropDownAction_______POSITION_______"/>
<form:select id="sel_position" path="position.code"   
	onchange="<%=\"btn_click_cExecuteUC41_DropDownAction('\"+request.getContextPath()+\"/presentation/standardfeatures/application/form/uc41_actionondropdownselection/UC41_DropDownAction/sel_position.html');\"%>"
		>
		<form:option value="" label="---"/>
	<form:options items="${uC41_DropDownActionForm.sel_positions}" itemValue="code" itemLabel="name" />
</form:select>
<fmt:message key="cUC41_DropDownAction_______DISPLAY_POSITION___"/>
<form:checkbox id="chx_displayPosition" path="position.displayPosition"   
	onclick="<%=\"btn_click_cExecuteUC41_DropDownAction('\"+request.getContextPath()+\"/presentation/standardfeatures/application/form/uc41_actionondropdownselection/UC41_DropDownAction/chx_displayPosition.html');\"%>"
		/>
<br clear="none"  /> 
<br clear="none"  /> 
<display:table uid="tab_players" pagesize="10"  sort="list" requestURI="" name="sessionScope.uC41_DropDownActionForm.players" class="rd_tabledata"  >
	<c:set   var="rownum">
		<%=(tab_players !=	null) ? new Integer(tab_players_rowNum.intValue() - 1) : new Integer(0) %>
	</c:set>
	<c:set   var="idRow">
		<%=(tab_players != null ) ? tab_players_rowNum.intValue() - 1 : -1 %>
	</c:set>
	<c:set   var="idTab">
		<%=(tab_players != null ) ? "tab_players" : null %>
	</c:set>
	<c:set var="tableHashId">
		<%= new org.displaytag.util.ParamEncoder("tab_players").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE) %>
	</c:set>
	<c:set var="tableHashId2">
		<%=request.getParameter(new org.displaytag.util.ParamEncoder("tab_players").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE)) %>
	</c:set>	
	
<display:column format=""  titleKey="cUC41_DropDownAction_LAST_NAME">
		<span id="txt_lastname1">
	<c:out value="${tab_players.lastName}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="cUC41_DropDownAction_FIRST_NAME">
		<span id="txt_firstname1">
	<c:out value="${tab_players.firstName}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="cUC41_DropDownAction_DETAILS">
<c:if test="${uC41_DropDownActionForm.position.displayPosition=='true'}"> 
		<span id="txt_position">
	<c:out value="${tab_players.position.name}" />
		</span>
	<fmt:message key="cUC41_DropDownAction____________________"/>
</c:if>
		<span id="txt_height">
	<c:out value="${tab_players.height}" />
		</span>
	<fmt:message key="cUC41_DropDownAction_CMS__________"/>
		<span id="txt_weight">
	<c:out value="${tab_players.weight}" />
		</span>
	<fmt:message key="cUC41_DropDownAction_KGS________"/>
</display:column>
</display:table>
<div style="visibility:hidden;"  > 
<a id="lien_cache"onclick="javascript:btn_click_cExecuteUC41_DropDownAction(this.href);return false;"				href="<c:url value="/presentation/standardfeatures/application/form/uc41_actionondropdownselection/UC41_DropDownAction/lien_cache.html"/>"
							 shape="rect"  >									 
<fmt:message key="cUC41_DropDownAction_HOME"/>
</a>
</div>
	</form:form>
	<script language="javascript">
		function btn_click_cExecuteUC41_DropDownAction(url) {
			UC41_DropDownActionvar = document.getElementById('uC41_DropDownAction');
			UC41_DropDownActionvar.setAttribute('action', url);
			UC41_DropDownActionvar.submit();
		}
		function btn_click_cSetActionUC41_DropDownAction(url) {
			UC41_DropDownActionvar = document.getElementById('uC41_DropDownAction');
			UC41_DropDownActionvar.setAttribute('action', url);
		}
	</script>
</div>
<div class="east-center"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/application/form/uc41_actionondropdownselection/UC41_DropDownActioncmp_tabs" />
</div>
</div>
<div class="ui-layout-north"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/application/form/uc41_actionondropdownselection/UC41_DropDownActioncmp_header" />
</div>
<div class="ui-layout-south"  > 
<fmt:message key="cUC41_DropDownAction_______"/>
<span id="rd_jversion"  > 
<fmt:message key="cUC41_DropDownAction_BLU_AGE_DOCUMENTATION_102"/>
</span>
</div>
<div class="ui-layout-west"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/application/form/uc41_actionondropdownselection/UC41_DropDownActioncmp_treeview" />
</div>
</div>
</body>
</html>
