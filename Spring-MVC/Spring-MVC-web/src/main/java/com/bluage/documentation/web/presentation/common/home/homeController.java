/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.common.home ;

// Import declaration.
// Java imports.
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;

/**
 * Class : homeController
 */
@Controller
@Scope("prototype")
@SessionAttributes("homeForm")
@RequestMapping(value= "/presentation/common/home")
public class homeController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(homeController.class);
	
	//Current form name
	private static final String HOME_FORM = "homeForm";

	
	/**
	 * Property:customDateEditorshomeController
	 */
	final private Map<String,CustomDateEditor> customDateEditorshomeController = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:homeValidator
	 */
	final private homeValidator homeValidator = new homeValidator();
	
	
	// Initialise all the instances for homeController

	/**
	* This method initialise the form : homeForm 
	* @return homeForm
	*/
	@ModelAttribute("homeFormInit")
	public void inithomeForm(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, HOME_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method homeForm."); //for lnk_back 
		}
		homeForm homeForm;
	
		if(request.getSession().getAttribute(HOME_FORM) != null){
			homeForm = (homeForm)request.getSession().getAttribute(HOME_FORM);
		} else {
			homeForm = new homeForm();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form homeForm.");
		}
		homeForm = (homeForm)populateDestinationForm(request.getSession(), homeForm);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  homeForm.");
		}
		model.addAttribute(HOME_FORM, homeForm);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param homeForm : The sceen form.
	 */
	@RequestMapping(value = "/home.html" ,method = RequestMethod.GET)
	public void preparehome(final HttpServletRequest request,final  Model model,final  @ModelAttribute("homeFormInit") homeForm homeForm){
		
		homeForm currenthomeForm = homeForm;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(HOME_FORM) == null){
			if(currenthomeForm!=null){
				request.getSession().setAttribute(HOME_FORM, currenthomeForm);
			}else {
				currenthomeForm = new homeForm();
				request.getSession().setAttribute(HOME_FORM, currenthomeForm);	
			}
		} else {
			currenthomeForm = (homeForm) request.getSession().getAttribute(HOME_FORM);
		}

		currenthomeForm = (homeForm)populateDestinationForm(request.getSession(), currenthomeForm);
		// Call all the Precontroller.
	currenthomeForm.setAlwaysCallPreControllers(true);
		model.addAttribute(HOME_FORM, currenthomeForm);
		request.getSession().setAttribute(HOME_FORM, currenthomeForm);
		
	}
	
	
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("homeController [ ");
		// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(homeValidator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorshomeController!=null); 
		return strBToS.toString();
	}
}
