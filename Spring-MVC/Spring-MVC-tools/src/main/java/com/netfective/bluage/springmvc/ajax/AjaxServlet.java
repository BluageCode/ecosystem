/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.netfective.bluage.springmvc.ajax;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Class AjaxServlet
 * @superclass HttpServlet
 * @author NETFECTIVE TECHNOLOGY
 */
public class AjaxServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	* Post servlet method
	* @param HttpServletRequest :
	*			request
	* @param HttpServletResponse :
	*			response
	* @throws IOException, ServletException
	*/
	public void doPost(final HttpServletRequest request, final HttpServletResponse response)
			throws IOException, ServletException {
		super.doPost(request, response);
	}
	
	/**
	* Get servlet method
	* @param HttpServletRequest :
	*			req
	* @param HttpServletResponse :
	*			resp
	* @throws IOException, ServletException
	*/
	public void doGet(final HttpServletRequest req, final HttpServletResponse resp) 
			throws ServletException ,IOException {
		super.doGet(req, resp);
	}
	
	/**
	* Service servlet method
	* @param ServletRequest :
	*			arg0
	* @param ServletResponse :
	*			arg1
	* @throws IOException, ServletException
	*/
	public void service(final ServletRequest arg0, final ServletResponse arg1)
			throws ServletException, IOException {
		super.service(arg0, arg1);
	}
}

