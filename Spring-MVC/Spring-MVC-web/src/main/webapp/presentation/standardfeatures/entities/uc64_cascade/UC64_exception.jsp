<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file="/include.jsp"  %>
<html >
<head >
<title  > 
<fmt:message key="cUC64_exception_BLU_AGE_DOCUMENTATION"/>
</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"  > 
</meta>
<link href="<c:url value= "/presentation/_style/_style.css"/>" rel="stylesheet" type="text/css"  /> 
<link href="<c:url value= "/presentation/_include/jquery/rd_lib.css"/>" rel="stylesheet" type="text/css"  /> 
<script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/jquery.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/rd_setup.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/rd_lib.js"/>" space="preserve"  ></script><link rel="shortcut icon" href="<c:url value= "/presentation/_style/favicon.ico"/>"  /> 

</head>

<body class=" " > 
<div id="rd_ghost"  > 
<small  > 
<fmt:message key="cUC64_exception___CSS_DEACTIVATED"/>
</small>
</div>
<div id="rd_jghost"  > 
<small  > 
<fmt:message key="cUC64_exception___JAVASCRIPT_DEACTIVATED"/>
</small>
</div>
<div id="shape"  > 
<div class="ui-layout-center"  > 
<div class="east-north"  > 
	<form:form id="uC64_exception" commandName="uC64_exceptionForm"  enctype="application/x-www-form-urlencoded" method="post">
<h2 class="rd_first"  > 
<fmt:message key="cUC64_exception_STANDARD_FEATURES__ENTITIES_"/>
</h2>
<h5  > 
<fmt:message key="cUC64_exception_AN_EXCEPTION_HAS_OCCURRED"/>
</h5>
<h5  > 
<fmt:message key="cUC64_exception_ERROR_MESSAGE"/>
</h5>
	<c:if test="${not empty messages}">
		<div id="Errors" >
			<c:forEach var="error" items="${messages}">
				<c:out value="${error}" escapeXml="false" />
				<br />
			</c:forEach>
		</div>
	</c:if>
	<% (request.getSession()).setAttribute("messages", null); %>
<a id="btn_back"				href="<c:url value="/presentation/standardfeatures/entities/uc64_cascade/UC64_HomePage.html"/>"
													 shape="rect"  >									 
<fmt:message key="cUC64_exception_BACK_"/>
</a>
	</form:form>
	<script language="javascript">
		function btn_click_cExecuteUC64_exception(url) {
			UC64_exceptionvar = document.getElementById('uC64_exception');
			UC64_exceptionvar.setAttribute('action', url);
			UC64_exceptionvar.submit();
		}
		function btn_click_cSetActionUC64_exception(url) {
			UC64_exceptionvar = document.getElementById('uC64_exception');
			UC64_exceptionvar.setAttribute('action', url);
		}
	</script>
</div>
<div class="east-center"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/entities/uc64_cascade/UC64_exceptioncmp_tabs" />
</div>
</div>
<div class="ui-layout-north"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/entities/uc64_cascade/UC64_exceptioncmp_header" />
</div>
<div class="ui-layout-south"  > 
<fmt:message key="cUC64_exception___"/>
<span id="rd_jversion"  > 
<fmt:message key="cUC64_exception_BLU_AGE_DOCUMENTATION_152"/>
</span>
</div>
<div class="ui-layout-west"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/entities/uc64_cascade/UC64_exceptioncmp_treeview" />
</div>
</div>
</body>
</html>
