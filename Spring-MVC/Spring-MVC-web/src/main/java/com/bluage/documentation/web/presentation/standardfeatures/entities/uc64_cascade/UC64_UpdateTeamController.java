/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.entities.uc64_cascade ;

// Import declaration.
// Java imports.
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.bluage.documentation.business.standardfeatures.entities.uc64_cascade.bo.Team64BO;
import com.bluage.documentation.business.standardfeatures.entities.uc64_cascade.entities.daofinder.State64DAOFinderImpl;
import com.bluage.documentation.service.standardfeatures.entities.uc64_cascade.ServiceTeam64;
import com.netfective.bluage.core.exception.ApplicationException;
import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;
import com.netfective.bluage.core.web.IForm;

/**
 * Class : UC64_UpdateTeamController
 */
@Controller
@Scope("prototype")
@SessionAttributes("uC64_UpdateTeamForm")
@RequestMapping(value= "/presentation/standardfeatures/entities/uc64_cascade")
public class UC64_UpdateTeamController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC64_UpdateTeamController.class);
	
	//Current form name
	private static final String U_C64__UPDATE_TEAM_FORM = "uC64_UpdateTeamForm";

		//CONSTANT: allStates
	private static final String ALL_STATES = "allStates";
				//CONSTANT: teamToUpdate
	private static final String TEAM_TO_UPDATE = "teamToUpdate";
	
	/**
	 * Property:customDateEditorsUC64_UpdateTeamController
	 */
	final private Map<String,CustomDateEditor> customDateEditorsUC64_UpdateTeamController = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:uC64_UpdateTeamValidator
	 */
	final private UC64_UpdateTeamValidator uC64_UpdateTeamValidator = new UC64_UpdateTeamValidator();
	
	/**
	 * Service declaration : serviceTeam64.
	 */
	@Autowired
	private ServiceTeam64 serviceTeam64;
	
	/**
	 * Generic Finder : state64DAOFinderImpl.
	 */
	@Autowired
	private State64DAOFinderImpl state64DAOFinderImpl;
	
	
	// Initialise all the instances for UC64_UpdateTeamController
			// Initialize the instance allStates for the state lnk_deleteState
		private List allStates; // Initialize the instance allStates for UC64_UpdateTeamController
						// Declare the instance teamToUpdate
		private Team64BO teamToUpdate;
			/**
	 * Operation : lnk_update
 	 * @param model : 
 	 * @param uC64_UpdateTeam : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC64_UpdateTeam/lnk_update.html",method = RequestMethod.POST)
	public String lnk_update(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC64_UpdateTeamForm") UC64_UpdateTeamForm  uC64_UpdateTeamForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : lnk_update"); 
		infoLogger(uC64_UpdateTeamForm); 
		uC64_UpdateTeamForm.setAlwaysCallPreControllers(false); 
		// initialization for lnk_update
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state lnk_update if the validation fail, you will be forward to the screen : "/presentation/standardfeatures/entities/uc64_cascade/UC64_UpdateTeam"
		if(errorsMessages(request,response,uC64_UpdateTeamForm,bindingResult,"", uC64_UpdateTeamValidator, customDateEditorsUC64_UpdateTeamController)){ 
			return "/presentation/standardfeatures/entities/uc64_cascade/UC64_UpdateTeam"; 
		}

					 callTeamUpdate(request,uC64_UpdateTeamForm );
											// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.entities.uc64_cascade.UC64_HomePageForm or not from lnk_update
			final UC64_HomePageForm uC64_HomePageForm =  new UC64_HomePageForm();
			// Populate the destination form with all the instances returned from lnk_update.
			final IForm uC64_HomePageForm2 = populateDestinationForm(request.getSession(), uC64_HomePageForm); 
					
			// Add the form to the model.
			model.addAttribute("uC64_HomePageForm", uC64_HomePageForm2); 
			
			request.getSession().setAttribute("uC64_HomePageForm", uC64_HomePageForm2);
			
			// "OK" CASE => destination screen path from lnk_update
			LOGGER.info("Go to the screen 'UC64_HomePage'.");
			// Redirect (PRG) from lnk_update
			destinationPath =  "redirect:/presentation/standardfeatures/entities/uc64_cascade/UC64_HomePage.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_update 
	}
	
	
	/**
	* This method initialise the form : UC64_UpdateTeamForm 
	* @return UC64_UpdateTeamForm
	*/
	@ModelAttribute("UC64_UpdateTeamFormInit")
	public void initUC64_UpdateTeamForm(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, U_C64__UPDATE_TEAM_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method UC64_UpdateTeamForm."); //for lnk_update 
		}
		UC64_UpdateTeamForm uC64_UpdateTeamForm;
	
		if(request.getSession().getAttribute(U_C64__UPDATE_TEAM_FORM) != null){
			uC64_UpdateTeamForm = (UC64_UpdateTeamForm)request.getSession().getAttribute(U_C64__UPDATE_TEAM_FORM);
		} else {
			uC64_UpdateTeamForm = new UC64_UpdateTeamForm();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form UC64_UpdateTeamForm.");
		}
		uC64_UpdateTeamForm = (UC64_UpdateTeamForm)populateDestinationForm(request.getSession(), uC64_UpdateTeamForm);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  UC64_UpdateTeamForm.");
		}
		model.addAttribute(U_C64__UPDATE_TEAM_FORM, uC64_UpdateTeamForm);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param UC64_UpdateTeamForm : The sceen form.
	 */
	@RequestMapping(value = "/UC64_UpdateTeam.html" ,method = RequestMethod.GET)
	public void prepareUC64_UpdateTeam(final HttpServletRequest request,final  Model model,final  @ModelAttribute("UC64_UpdateTeamFormInit") UC64_UpdateTeamForm uC64_UpdateTeamForm){
		
		UC64_UpdateTeamForm currentUC64_UpdateTeamForm = uC64_UpdateTeamForm;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(U_C64__UPDATE_TEAM_FORM) == null){
			if(currentUC64_UpdateTeamForm!=null){
				request.getSession().setAttribute(U_C64__UPDATE_TEAM_FORM, currentUC64_UpdateTeamForm);
			}else {
				currentUC64_UpdateTeamForm = new UC64_UpdateTeamForm();
				request.getSession().setAttribute(U_C64__UPDATE_TEAM_FORM, currentUC64_UpdateTeamForm);	
			}
		} else {
			currentUC64_UpdateTeamForm = (UC64_UpdateTeamForm) request.getSession().getAttribute(U_C64__UPDATE_TEAM_FORM);
		}

		try {
			List allStates = state64DAOFinderImpl.findAll();
			put(request.getSession(), ALL_STATES, allStates);
		} catch (ApplicationException e) {
			errorLogger("An error while calling the generic finder for the instance : allStates.",e);
		}
		currentUC64_UpdateTeamForm = (UC64_UpdateTeamForm)populateDestinationForm(request.getSession(), currentUC64_UpdateTeamForm);
		// Call all the Precontroller.
	currentUC64_UpdateTeamForm.setAlwaysCallPreControllers(true);
		model.addAttribute(U_C64__UPDATE_TEAM_FORM, currentUC64_UpdateTeamForm);
		request.getSession().setAttribute(U_C64__UPDATE_TEAM_FORM, currentUC64_UpdateTeamForm);
		
	}
	
				/**
	 * method callTeamUpdate
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callTeamUpdate(HttpServletRequest request,IForm form) {
					teamToUpdate = (Team64BO)get(request.getSession(),form, "teamToUpdate");  
										 
					try {
				// executing TeamUpdate in lnk_update
	serviceTeam64.teamUpdate(
			teamToUpdate
			);  

													// processing variables TeamUpdate in lnk_update
				put(request.getSession(), TEAM_TO_UPDATE,teamToUpdate); 
			
			} catch (ApplicationException e) { 
				// error handling for operation teamUpdate called TeamUpdate
				errorLogger("An error occured during the execution of the operation : teamUpdate",e); 
		
			}
	}
		
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("UC64_UpdateTeamController [ ");
				strBToS.append(ALL_STATES);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(allStates);
			strBToS.append(END_TO_STRING_DELIMITER);
						strBToS.append(TEAM_TO_UPDATE);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(teamToUpdate);
			strBToS.append(END_TO_STRING_DELIMITER);
		// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(uC64_UpdateTeamValidator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsUC64_UpdateTeamController!=null); 
		return strBToS.toString();
	}
}
