package uc38;


import org.junit.Test;
import test.common.AbstractBADocTestCase;


public class ITuc38 extends AbstractBADocTestCase {

	@Override
	public  String getName(){
		return "UC-38";
	}
	
	@Test
	public void testITuc38() throws Exception {
		// On teste l'UC38 - Standard Features/Application/Datagrid - Selected List.
		selenium.open("");
		selenium.click("link=Expand All");
		selenium.waitForPageToLoad("30000");
		selenium.click("link=UC38 - Selected List");
		verifyTrue(selenium.isElementPresent("//h2[contains(text(),\"Standard Features > Application > DataGrid > UC38 > Selected List\")]"));
		assertEquals("[Display Players]", selenium.getText("//a[contains(@id,'lnk_display')]"));
		selenium.click("//a[contains(@id,'lnk_display')]");
		selenium.waitForPageToLoad("30000");
		verifyTrue(selenium.isElementPresent("//h2[contains(text(),\"Standard Features > Application > DataGrid > UC38 > Selected List\")]"));
		// On vérifie les données.
		assertTrue(selenium.isElementPresent("//table//thead//tr[th[contains(text(),\"Name\")] and th[contains(text(),\"Date of Birth\")] and th[contains(text(),\"Weight (kg)\")] and th[contains(text(),\"Height (cm)\")] and th[contains(text(),\"Estimated Value (M$)\")] and th[contains(text(),\"\")]]"));
		Number num=12;
		num= selenium.getXpathCount("//table//tbody//tr");
		assertTrue(selenium.isElementPresent("//table//tbody//tr[td//span[contains(text(),\"Owen\")] and td//span[contains(text(),\"Little\")] and td//span[contains(text(),\"135\")] and td//span[contains(text(),\"183\")] and td//span[contains(text(),\"45.87\")] and td//span[contains(text(),\"10/27/1981\")] and td//span[contains(text(),\"Braves\")]]"));
		assertTrue(selenium.isElementPresent("//table//tbody//tr[td//span[contains(text(),\"Irvin\")] and td//span[contains(text(),\"Lee\")] and td//span[contains(text(),\"88\")] and td//span[contains(text(),\"157\")] and td//span[contains(text(),\"10.61\")] and td//span[contains(text(),\"02/25/1988\")] and td//span[contains(text(),\"Tigers\")]]"));
		assertTrue(selenium.isElementPresent("//table//tbody//tr[td//span[contains(text(),\"Wayne\")] and td//span[contains(text(),\"Rose\")] and td//span[contains(text(),\"115\")] and td//span[contains(text(),\"238\")] and td//span[contains(text(),\"20.41\")] and td//span[contains(text(),\"10/28/1981\")] and td//span[contains(text(),\"Tigers\")]]"));
		assertTrue(selenium.isElementPresent("//table//tbody//tr[td//span[contains(text(),\"Lucas\")] and td//span[contains(text(),\"Williamson\")] and td//span[contains(text(),\"73\")] and td//span[contains(text(),\"151\")] and td//span[contains(text(),\"38.36\")] and td//span[contains(text(),\"06/29/1983\")] and td//span[contains(text(),\"Tigers\")]]"));
		assertTrue(selenium.isElementPresent("//table//tbody//tr[td//span[contains(text(),\"Walter\")] and td//span[contains(text(),\"Reyes\")] and td//span[contains(text(),\"104\")] and td//span[contains(text(),\"193\")] and td//span[contains(text(),\"46.15\")] and td//span[contains(text(),\"12/09/1984\")] and td//span[contains(text(),\"Tigers\")]]"));
		assertTrue(selenium.isElementPresent("//table//tbody//tr[td//span[contains(text(),\"Kirk\")] and td//span[contains(text(),\"Cox\")] and td//span[contains(text(),\"60\")] and td//span[contains(text(),\"179\")] and td//span[contains(text(),\"26.58\")] and td//span[contains(text(),\"05/10/1973\")] and td//span[contains(text(),\"Chargers\")]]"));
		assertTrue(selenium.isElementPresent("//table//tbody//tr[td//span[contains(text(),\"Arthur\")] and td//span[contains(text(),\"Payne\")] and td//span[contains(text(),\"66\")] and td//span[contains(text(),\"190\")] and td//span[contains(text(),\"30.86\")] and td//span[contains(text(),\"11/20/1981\")] and td//span[contains(text(),\"Braves\")]]"));
		assertTrue(selenium.isElementPresent("//table//tbody//tr[td//span[contains(text(),\"Leroy\")] and td//span[contains(text(),\"Moore\")] and td//span[contains(text(),\"89\")] and td//span[contains(text(),\"183\")] and td//span[contains(text(),\"45.8\")] and td//span[contains(text(),\"07/26/1987\")] and td//span[contains(text(),\"Bears\")]]"));
		assertTrue(selenium.isElementPresent("//table//tbody//tr[td//span[contains(text(),\"Milton\")] and td//span[contains(text(),\"Johnson\")] and td//span[contains(text(),\"68\")] and td//span[contains(text(),\"205\")] and td//span[contains(text(),\"22.22\")] and td//span[contains(text(),\"07/02/1969\")] and td//span[contains(text(),\"Braves\")]]"));
		assertTrue(selenium.isElementPresent("//table//tbody//tr[td//span[contains(text(),\"Sammy\")] and td//span[contains(text(),\"Mendez\")] and td//span[contains(text(),\"96\")] and td//span[contains(text(),\"234\")] and td//span[contains(text(),\"2.27\")] and td//span[contains(text(),\"07/01/1986\")] and td//span[contains(text(),\"Bears\")]]"));
		assertTrue(selenium.isElementPresent("//table//tbody//tr[td//span[contains(text(),\"Rafael\")] and td//span[contains(text(),\"Steele\")] and td//span[contains(text(),\"66\")] and td//span[contains(text(),\"153\")] and td//span[contains(text(),\"15.48\")] and td//span[contains(text(),\"11/25/1968\")] and td//span[contains(text(),\"Tigers\")]]"));
		assertTrue(selenium.isElementPresent("//table//tbody//tr[td//span[contains(text(),\"Bob\")] and td//span[contains(text(),\"Jacobs\")] and td//span[contains(text(),\"72\")] and td//span[contains(text(),\"172\")] and td//span[contains(text(),\"13.23\")] and td//span[contains(text(),\"05/21/1988\")] and td//span[contains(text(),\"Bears\")]]"));
		// On va vérifier l'edition des informations des joueurs.
		// Pour Owen Little :
		Number indexOwenL = selenium.getElementIndex("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Owen\")] and td/span[contains(text(),\"Little\")]]");
		System.out.println("variable index vaut : " + indexOwenL);
		selenium.click("//table[contains(@id,'tab_players')]/tbody/tr[" + indexOwenL + " + 1]//td/a[contains(text(),\"[edit]\")]");
		selenium.waitForPageToLoad("30000");
		selenium.type("//input[contains(@id,'txt_lastname')]", "Blanc");
		selenium.type("//input[contains(@id,'txt_firstname')]", "Michel");
		selenium.type("//input[contains(@id,'txt_dateofbirth')]", "12/02/1975");
		selenium.select("//*[contains(@id,'sel_team')]", "label=Tigers");
		selenium.click("//table[contains(@id,'tab_players')]/tbody/tr[" + indexOwenL + " + 1]//td/a[contains(text(),\"[update]\")]");
		selenium.waitForPageToLoad("30000");
		// On vérifie que les modifications s'affichent correctement.
		Number num2= 12;
		num2 = selenium.getXpathCount("//table//tbody//tr");
		assertTrue(selenium.isElementPresent("//table//tbody//tr[td//span[contains(text(),\"Blanc\")] and td//span[contains(text(),\"Michel\")] and td//span[contains(text(),\"135\")] and td//span[contains(text(),\"183\")] and td//span[contains(text(),\"45.87\")] and td//span[contains(text(),\"12/02/1975\")] and td//span[contains(text(),\"Tigers\")]]"));
		assertTrue(selenium.isElementPresent("//table//tbody//tr[td//span[contains(text(),\"Irvin\")] and td//span[contains(text(),\"Lee\")] and td//span[contains(text(),\"88\")] and td//span[contains(text(),\"157\")] and td//span[contains(text(),\"10.61\")] and td//span[contains(text(),\"02/25/1988\")] and td//span[contains(text(),\"Tigers\")]]"));
		assertTrue(selenium.isElementPresent("//table//tbody//tr[td//span[contains(text(),\"Wayne\")] and td//span[contains(text(),\"Rose\")] and td//span[contains(text(),\"115\")] and td//span[contains(text(),\"238\")] and td//span[contains(text(),\"20.41\")] and td//span[contains(text(),\"10/28/1981\")] and td//span[contains(text(),\"Tigers\")]]"));
		assertTrue(selenium.isElementPresent("//table//tbody//tr[td//span[contains(text(),\"Lucas\")] and td//span[contains(text(),\"Williamson\")] and td//span[contains(text(),\"73\")] and td//span[contains(text(),\"151\")] and td//span[contains(text(),\"38.36\")] and td//span[contains(text(),\"06/29/1983\")] and td//span[contains(text(),\"Tigers\")]]"));
		assertTrue(selenium.isElementPresent("//table//tbody//tr[td//span[contains(text(),\"Walter\")] and td//span[contains(text(),\"Reyes\")] and td//span[contains(text(),\"104\")] and td//span[contains(text(),\"193\")] and td//span[contains(text(),\"46.15\")] and td//span[contains(text(),\"12/09/1984\")] and td//span[contains(text(),\"Tigers\")]]"));
		assertTrue(selenium.isElementPresent("//table//tbody//tr[td//span[contains(text(),\"Kirk\")] and td//span[contains(text(),\"Cox\")] and td//span[contains(text(),\"60\")] and td//span[contains(text(),\"179\")] and td//span[contains(text(),\"26.58\")] and td//span[contains(text(),\"05/10/1973\")] and td//span[contains(text(),\"Chargers\")]]"));
		assertTrue(selenium.isElementPresent("//table//tbody//tr[td//span[contains(text(),\"Arthur\")] and td//span[contains(text(),\"Payne\")] and td//span[contains(text(),\"66\")] and td//span[contains(text(),\"190\")] and td//span[contains(text(),\"30.86\")] and td//span[contains(text(),\"11/20/1981\")] and td//span[contains(text(),\"Braves\")]]"));
		assertTrue(selenium.isElementPresent("//table//tbody//tr[td//span[contains(text(),\"Leroy\")] and td//span[contains(text(),\"Moore\")] and td//span[contains(text(),\"89\")] and td//span[contains(text(),\"183\")] and td//span[contains(text(),\"45.8\")] and td//span[contains(text(),\"07/26/1987\")] and td//span[contains(text(),\"Bears\")]]"));
		assertTrue(selenium.isElementPresent("//table//tbody//tr[td//span[contains(text(),\"Milton\")] and td//span[contains(text(),\"Johnson\")] and td//span[contains(text(),\"68\")] and td//span[contains(text(),\"205\")] and td//span[contains(text(),\"22.22\")] and td//span[contains(text(),\"07/02/1969\")] and td//span[contains(text(),\"Braves\")]]"));
		assertTrue(selenium.isElementPresent("//table//tbody//tr[td//span[contains(text(),\"Sammy\")] and td//span[contains(text(),\"Mendez\")] and td//span[contains(text(),\"96\")] and td//span[contains(text(),\"234\")] and td//span[contains(text(),\"2.27\")] and td//span[contains(text(),\"07/01/1986\")] and td//span[contains(text(),\"Bears\")]]"));
		assertTrue(selenium.isElementPresent("//table//tbody//tr[td//span[contains(text(),\"Rafael\")] and td//span[contains(text(),\"Steele\")] and td//span[contains(text(),\"66\")] and td//span[contains(text(),\"153\")] and td//span[contains(text(),\"15.48\")] and td//span[contains(text(),\"11/25/1968\")] and td//span[contains(text(),\"Tigers\")]]"));
		assertTrue(selenium.isElementPresent("//table//tbody//tr[td//span[contains(text(),\"Bob\")] and td//span[contains(text(),\"Jacobs\")] and td//span[contains(text(),\"72\")] and td//span[contains(text(),\"172\")] and td//span[contains(text(),\"13.23\")] and td//span[contains(text(),\"05/21/1988\")] and td//span[contains(text(),\"Bears\")]]"));
		// On teste la modification champs ${space}
		// Pour ce faire, on index la ligne d'un joueur.
		// Pour Irvin Lee :
		Number indexIrvinL = selenium.getElementIndex("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Irvin\")] and td/span[contains(text(),\"Lee\")]]");
		System.out.println("variable index vaut : " + indexIrvinL);
		selenium.click("//table[contains(@id,'tab_players')]/tbody/tr[" + indexIrvinL + " + 1]//td/a[contains(text(),\"[edit]\")]");
		selenium.waitForPageToLoad("30000");
		// On entre des valeurs dans les inputs vides.
		selenium.type("//input[contains(@id,'txt_lastname')]", "Lee");
		selenium.type("//input[contains(@id,'txt_firstname')]", "Irvino");
		selenium.type("//input[contains(@id,'txt_dateofbirth')]", "02/25/1988");
		selenium.click("//table[contains(@id,'tab_players')]/tbody/tr[" + indexIrvinL + " + 1]//td/a[contains(text(),\"[update]\")]");
		selenium.waitForPageToLoad("30000");
		// On vérifie que la ligne s'affiche correctement.
		Number num3=12;
		num3 = selenium.getXpathCount("//table//tbody//tr");
		assertTrue(selenium.isElementPresent("//table//tbody//tr[td//span[contains(text(),\"Irvino\")] and td//span[contains(text(),\"Lee\")] and td//span[contains(text(),\"88\")] and td//span[contains(text(),\"157\")] and td//span[contains(text(),\"10.61\")] and td//span[contains(text(),\"02/25/1988\")] and td//span[contains(text(),\"Tigers\")]]"));
		// On entre maintenant un format de date invalide.
		// On modifie la ligne de Michel Blanc.
		Number indexMichelB = selenium.getElementIndex("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Michel\")] and td/span[contains(text(),\"Blanc\")]]");
		System.out.println("variable index vaut : " + indexMichelB);
		selenium.click("//table[contains(@id,'tab_players')]/tbody/tr[" + indexMichelB + " + 1]//td/a[contains(text(),\"[edit]\")]");
		selenium.waitForPageToLoad("30000");
		selenium.type("//input[contains(@id,'txt_lastname')]", "Owen");
		selenium.type("//input[contains(@id,'txt_firstname')]", "Little");
		selenium.type("//input[contains(@id,'txt_dateofbirth')]", "10/27/1981");
		selenium.select("//*[contains(@id,'sel_team')]", "label=Braves");
		selenium.click("//table[contains(@id,'tab_players')]/tbody/tr[" + indexMichelB + " + 1]//td/a[contains(text(),\"[update]\")]");
		selenium.waitForPageToLoad("30000");
		// On vérifie que les données sont affichées.
		Number num4=12;
		num4 = selenium.getXpathCount("//table//tbody//tr");
		assertTrue(selenium.isElementPresent("//table//tbody//tr[td//span[contains(text(),\"Owen\")] and td//span[contains(text(),\"Little\")] and td//span[contains(text(),\"135\")] and td//span[contains(text(),\"183\")] and td//span[contains(text(),\"45.87\")] and td//span[contains(text(),\"10/27/1981\")] and td//span[contains(text(),\"Braves\")]]"));
		assertTrue(selenium.isElementPresent("//table//tbody//tr[td//span[contains(text(),\"Irvino\")] and td//span[contains(text(),\"Lee\")] and td//span[contains(text(),\"88\")] and td//span[contains(text(),\"157\")] and td//span[contains(text(),\"10.61\")] and td//span[contains(text(),\"02/25/1988\")] and td//span[contains(text(),\"Tigers\")]]"));
		assertTrue(selenium.isElementPresent("//table//tbody//tr[td//span[contains(text(),\"Wayne\")] and td//span[contains(text(),\"Rose\")] and td//span[contains(text(),\"115\")] and td//span[contains(text(),\"238\")] and td//span[contains(text(),\"20.41\")] and td//span[contains(text(),\"10/28/1981\")] and td//span[contains(text(),\"Tigers\")]]"));
		assertTrue(selenium.isElementPresent("//table//tbody//tr[td//span[contains(text(),\"Lucas\")] and td//span[contains(text(),\"Williamson\")] and td//span[contains(text(),\"73\")] and td//span[contains(text(),\"151\")] and td//span[contains(text(),\"38.36\")] and td//span[contains(text(),\"06/29/1983\")] and td//span[contains(text(),\"Tigers\")]]"));
		assertTrue(selenium.isElementPresent("//table//tbody//tr[td//span[contains(text(),\"Walter\")] and td//span[contains(text(),\"Reyes\")] and td//span[contains(text(),\"104\")] and td//span[contains(text(),\"193\")] and td//span[contains(text(),\"46.15\")] and td//span[contains(text(),\"12/09/1984\")] and td//span[contains(text(),\"Tigers\")]]"));
		assertTrue(selenium.isElementPresent("//table//tbody//tr[td//span[contains(text(),\"Kirk\")] and td//span[contains(text(),\"Cox\")] and td//span[contains(text(),\"60\")] and td//span[contains(text(),\"179\")] and td//span[contains(text(),\"26.58\")] and td//span[contains(text(),\"05/10/1973\")] and td//span[contains(text(),\"Chargers\")]]"));
		assertTrue(selenium.isElementPresent("//table//tbody//tr[td//span[contains(text(),\"Arthur\")] and td//span[contains(text(),\"Payne\")] and td//span[contains(text(),\"66\")] and td//span[contains(text(),\"190\")] and td//span[contains(text(),\"30.86\")] and td//span[contains(text(),\"11/20/1981\")] and td//span[contains(text(),\"Braves\")]]"));
		assertTrue(selenium.isElementPresent("//table//tbody//tr[td//span[contains(text(),\"Leroy\")] and td//span[contains(text(),\"Moore\")] and td//span[contains(text(),\"89\")] and td//span[contains(text(),\"183\")] and td//span[contains(text(),\"45.8\")] and td//span[contains(text(),\"07/26/1987\")] and td//span[contains(text(),\"Bears\")]]"));
		assertTrue(selenium.isElementPresent("//table//tbody//tr[td//span[contains(text(),\"Milton\")] and td//span[contains(text(),\"Johnson\")] and td//span[contains(text(),\"68\")] and td//span[contains(text(),\"205\")] and td//span[contains(text(),\"22.22\")] and td//span[contains(text(),\"07/02/1969\")] and td//span[contains(text(),\"Braves\")]]"));
		assertTrue(selenium.isElementPresent("//table//tbody//tr[td//span[contains(text(),\"Sammy\")] and td//span[contains(text(),\"Mendez\")] and td//span[contains(text(),\"96\")] and td//span[contains(text(),\"234\")] and td//span[contains(text(),\"2.27\")] and td//span[contains(text(),\"07/01/1986\")] and td//span[contains(text(),\"Bears\")]]"));
		assertTrue(selenium.isElementPresent("//table//tbody//tr[td//span[contains(text(),\"Rafael\")] and td//span[contains(text(),\"Steele\")] and td//span[contains(text(),\"66\")] and td//span[contains(text(),\"153\")] and td//span[contains(text(),\"15.48\")] and td//span[contains(text(),\"11/25/1968\")] and td//span[contains(text(),\"Tigers\")]]"));
		assertTrue(selenium.isElementPresent("//table//tbody//tr[td//span[contains(text(),\"Bob\")] and td//span[contains(text(),\"Jacobs\")] and td//span[contains(text(),\"72\")] and td//span[contains(text(),\"172\")] and td//span[contains(text(),\"13.23\")] and td//span[contains(text(),\"05/21/1988\")] and td//span[contains(text(),\"Bears\")]]"));
		// On vérifie les messages d'erreur.
		// Pour ce faire, on index la ligne d'un joueur.
		Number indexWilliamsonL = selenium.getElementIndex("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Williamson\")] and td/span[contains(text(),\"Lucas\")]]");
		System.out.println("variable index vaut : " + indexWilliamsonL);
		selenium.click("//table[contains(@id,'tab_players')]/tbody/tr[" + indexWilliamsonL + " + 1]//td/a[contains(text(),\"[edit]\")]");
		selenium.waitForPageToLoad("30000");
		// On efface les noms prénoms et date of birth.
		selenium.type("//input[contains(@id,'txt_lastname')]", "");
		selenium.type("//input[contains(@id,'txt_firstname')]", "");
		selenium.type("//input[contains(@id,'txt_dateofbirth')]", "");
		selenium.click("//table[contains(@id,'tab_players')]/tbody/tr[" + indexWilliamsonL + " + 1]//td/a[contains(text(),\"[update]\")]");
		selenium.waitForPageToLoad("30000");
		// On vérifie la présence des messages d'erreur.
//		assertTrue(selenium.isElementPresent("//table/tbody/tr/td/span[contains(text(),\"Last name is required.\")]"));
//		assertTrue(selenium.isElementPresent("//table/tbody/tr/td/span[contains(text(),\"First name is required.\")]"));
//		assertTrue(selenium.isElementPresent("//table/tbody/tr/td/span[contains(text(),\"Date of birth is required.\")]"));
	}


}
