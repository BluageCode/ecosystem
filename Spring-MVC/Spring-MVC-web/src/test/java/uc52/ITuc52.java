package uc52;

import org.junit.Test;
import test.common.AbstractBADocTestCase;

public class ITuc52 extends AbstractBADocTestCase {
	
	@Override
	public  String getName(){
		return "UC-52";
	}	


	@Test
	public void testUC52() throws Exception {
		// On teste l'UC52 - Standard Features/Services/Process - Conditional
				
		selenium.open("");	
		selenium.click("link=Expand All");
		selenium.waitForPageToLoad("30000");
		selenium.click("link=UC52 - Conditional");
		
		assertTrue(selenium.isElementPresent("//h2[contains(text(),\"Standard Features > Services > Process > UC52 > Conditional\")]"));
		// On vérifie les données de base de l'UC.
		selenium.click("//a[contains(@id,'lnk_showPlayers')]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_PlayersList')]/tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_PlayersList')]/tbody/tr[td/span[contains(text(),\"Vega\")]and td/span[contains(text(),\"Manu\")] and td/span[contains(text(),\"18.5\")] and td//input[not(@checked)]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_PlayersList')]/tbody/tr[td/span[contains(text(),\"Glod\")] and td/span[contains(text(),\"Hernest\")] and td/span[contains(text(),\"39.9\")] and td//input[@checked]]"));
		// On crée deux joueurs.
		// Un premier qui n'est pas Rookie
		selenium.type("//input[contains(@id,'txt_firstName')]", "Jean");
		selenium.type("//input[contains(@id,'txt_lastName')]", "Paul");
		selenium.type("//input[contains(@id,'txt_value')]", "42.3");
		selenium.click("//*[contains(@id,'lnk_addPlayer')]");
		selenium.waitForPageToLoad("30000");
		// Un second qui est rookie.
		selenium.type("//input[contains(@id,'txt_firstName')]", "Jean");
		selenium.type("//input[contains(@id,'txt_lastName')]", "Pierre");
		selenium.type("//input[contains(@id,'txt_value')]", "42.3");
		selenium.click("//*[contains(@id,'chx_rookie')]");
		// On vérifie que le message du changement de la valeur estimée du rookie est présent
		assertTrue(selenium.isElementPresent("//label[contains(text(),'The estimated value of a rookie is diminished by 10%')]"));
		selenium.click("//*[contains(@id,'lnk_addPlayer')]");
		selenium.waitForPageToLoad("30000");
		// On vérifie qu'ils sont bien affichés.
		selenium.click("//*[contains(@id,'lnk_showPlayers')]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_PlayersList')]/thead/tr[th[contains(text(),\"Last Name\")]and th[contains(text(),\"First Name\")] and th[contains(text(),\"Estimated Value (M$)\")] and th[contains(text(),\"Rookie\")]]"));
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_PlayersList')]/tbody/tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_PlayersList')]/tbody/tr[td/span[contains(text(),\"Vega\")]and td/span[contains(text(),\"Manu\")] and td/span[contains(text(),\"18.5\")] and td//input[not(@checked)]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_PlayersList')]/tbody/tr[td/span[contains(text(),\"Glod\")] and td/span[contains(text(),\"Hernest\")] and td/span[contains(text(),\"39.9\")] and td//input[@checked]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_PlayersList')]/tbody/tr[td/span[contains(text(),\"Paul\")] and td/span[contains(text(),\"Jean\")] and td/span[contains(text(),\"42.3\")] and td//input[not(@checked)]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_PlayersList')]/tbody/tr[td/span[contains(text(),\"Pierre\")] and td/span[contains(text(),\"Jean\")] and td/span[contains(text(),\"38.07\")] and td//input[@checked]]"));
	}


}
