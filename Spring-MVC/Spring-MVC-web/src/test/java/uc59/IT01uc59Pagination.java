package uc59;


import org.junit.Test;
import test.common.AbstractBADocTestCase;

public class IT01uc59Pagination extends AbstractBADocTestCase {
	

	@Override
	public  String getName(){
		return "UC-59";
	}	


	@Test
	public void testIT01uc59Pagination() throws Exception {
		// On teste l'UC59 - Standard Features/Entities - UML to SQL Mapping
		selenium.open("");	
		selenium.click("link=Expand All");
		selenium.waitForPageToLoad("30000");
		selenium.click("link=UC59 - UML to SQL Mapping");
		verifyTrue(selenium.isElementPresent("//h2[contains(text(),\"Standard Features > Entities > UC59 > UML to SQL Mapping\")]"));
		// On effectue les tests de pagination.
		// On teste les liens.
		// On va sur la page 2.
		selenium.click("xpath=(//span/a)[1]");
		selenium.waitForPageToLoad("30000");
		// On vérifie qu'on est sur la page 2.
		assertTrue(selenium.isElementPresent("//a/b[contains(text(),\"2\")]|//strong[contains(text(),\"2\")]"));
		// On teste le lien Prev.
		selenium.click("xpath=(//span/a)[2]");
		selenium.waitForPageToLoad("30000");
		// On vérifie que l'on est sur la première page.
		assertTrue(selenium.isElementPresent("//a/b[contains(text(),\"1\")]|//strong[contains(text(),\"1\")]"));
		// On teste le lien Last.
		selenium.click("xpath=(//span/a)[3]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//a/b[contains(text(),\"2\")]|//strong[contains(text(),\"2\")]"));
		// On teste le lien First
		selenium.click("xpath=(//span/a)[1]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//a/b[contains(text(),\"1\")]|//strong[contains(text(),\"1\")]"));
	}


}
