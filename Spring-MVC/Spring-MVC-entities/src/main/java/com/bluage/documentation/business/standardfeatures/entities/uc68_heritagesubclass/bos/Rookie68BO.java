/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.business.standardfeatures.entities.uc68_heritagesubclass.bos;

import javax.inject.Inject;
import javax.inject.Named;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import com.bluage.documentation.business.standardfeatures.entities.uc68_heritagesubclass.entities.daofinder.Rookie68DAOFinderImpl;

/**
 * Rookie68BO
 *
* @see com.netfective.bluage.core.business.IBusinessObject
* * @see Player68BO
 */
@XmlRootElement

public class Rookie68BO
 extends Player68BO {
	
	/**
	 * Default SUID for Rookie68BO.
	 */
	private static final long serialVersionUID = 1L;
	

	@Inject
	@Named ("com.bluage.documentation.business.standardfeatures.entities.uc68_heritagesubclass.entities.daofinder.Rookie68Finder") 
	private Rookie68DAOFinderImpl finder;

						/**
	 * Entity property: university
	 *
	 */
private String university;



   /**
	* public default constructor for class Rookie68BO
	*/
	public Rookie68BO(){
		//NOP Rookie68BO
	}


						/**
	 * Getter for university of class Rookie68BO.
	 *
	 * @return {@link java.lang.String}
	 * @generated
	 */ 
public  String getUniversity() {
	return this.university;
	}


	/**
	 * Setter for university of class Rookie68BO.
	 * @param value {@link java.lang.String}
	 * @generated
	 */

public  void setUniversity(final String university) {
	this.university = university;
}


/**
	 * Returns <code>true</code> if the argument is an Rookie68 instance and all identifiers for this entity
     * equal the identifiers of the argument entity. The <code>equals</code> method of the parent entity
     * will also need to return <code>true</code>. Returns <code>false</code> otherwise.
     *
     * @see Player68BO#hashCode()
	 * @param object  object to compare with current instance      
     * @return boolean true if equals false if not
     */
    public boolean equals(final Object object )
    {
		if ( this == object )
        {
            return true;
        }
        if ( !( object instanceof Rookie68BO ) )
        {
            return false;
        }
        final Rookie68BO that = (Rookie68BO)object;
    
    	            if (        				        			this.getId() == null
        	        ) {
        	return false;
        }
		return new EqualsBuilder()
			.append(this.getId(), that.getId())
			 	.isEquals();
	}
	/**

	 * Returns a hash code based on this entity's identifiers and the hash code of the parent entity.
     *
     * @see Player68BO#hashCode()
     * @return int
     */    
    public int hashCode() {
        return new HashCodeBuilder()
             .append(getId())
				 	.appendSuper(super.hashCode())
            .toHashCode();
    }
}

