<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:xml="http://www.w3.org/XML/1998/namespace">
	<head>
		<title></title>
	</head>

	<body>
		<p><strong>Summary</strong></p>
		<p>In this scenario, we will see how to model service copy.</p>
		<p><strong>Scenario goal</strong></p>
	<ul>
		<li>To see the different possibilities to get nay informations without using the same Service </li>
	</ul>
		<p><strong>Description</strong></p>
		<p>
			Create <i>Team58</i> and <i>State58</i> entities with their corresponding attributes.<br/>
			Create their associated business objects in the <i>bos</i> package.<br/>
			Add <b>OneToMany</b> association between the two entities.
			<br/><br/>
			<img src="01-BoDiagram.png" alt="Conditional Class Diagram"/>
		</p>
		<p>
			In the service layer, some services have been defined to search the list of instances of the entity <i>Team58</i> and get a copy of each returned team.
		</p>
		<div style="padding-left: 20px">
			- Service <i>Serviceteam58FindByID</i>: returns information about each team, selected by id.<br/>
			- Service <i>ServiceCopyTeam58</i>: creates a copy for each selected team.<br/>
			- Service <i>ServiceTeam58Update</i>: updates the information for each selected team.<br/>
		</div>
		<p>
			Firstly, apply the stereotype <b>copy</b> to the interface <i>ServiceCopyTeam58</i>.<br/>
			Create the operation <i>copyBo58</i>, that has the business object <i>Team58BO</i> as parameter and return type.<br/>
			<br/>
			<img src="02-Services-Summary.png" alt="Conditional Service"/>
		</p>
		<p>
		Create an activity diagram for each one of these screens: 
		<i>UC58_DisplayTeams58</i><br/><br/>
		 <img src="03-Screen-UC58_DisplayTeams58.png" alt="Service copy Diagram" /><br/><br/>
		 In this screen, we will display the list of all teams.<br/>
		<i>UC58_CopyDetailsTeam58</i><br/> <br/>
		<img src="03-Screen-UC58_CopyDetailsTeam58.png" alt="Service copy Diagram" /><br/><br/>
		In this screen, we will display the information of each selected team and a copy of it.<br/><br/>
		Call the operation <i>team58FindByID</i> in the screen <i>UC58_DisplayTeams</i> and make a control flow named "lnk_view" from the screen to the operation.<br/>
		Name the input instance of the operation <i>selectedTeam</i> and the output <i>teamDetails</i>.<br/>
		Call the operation <i>copyBo58</i>.<br/>
		Its input instance get the same name as the output of the operation <i>team58FindByID</i>.
		Name this output instance <i>teamDetailsCopy</i>.
		Make a control flow from the operation <i>team58FindByID</i> to <i>copyBO</i>.<br/>
		Add another control flow from <i>copyBO</i> to the screen <i>UC58_CopyDetailsTeam58</i>.
		</p>
		<p>
			See the <b>Mockup</b> tab for the configuration of the html file.
		</p>
	</body>
</html>