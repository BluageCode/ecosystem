--<ScriptOptions statementTerminator=";"/>

ALTER TABLE player05 DROP CONSTRAINT player05_position_fkc;

ALTER TABLE position05 DROP CONSTRAINT position05_pkey;

ALTER TABLE player05 DROP CONSTRAINT player05_pkey;

DROP TABLE player05;

DROP TABLE position05;

CREATE TABLE player05 (
		id INT8 NOT NULL,
		version INT4 NOT NULL,
		first_name VARCHAR(30) NOT NULL,
		last_name VARCHAR(30) NOT NULL,
		date_of_birth DATE,
		estimated_value FLOAT4,
		weight INT4,
		height INT4,
		rookie BOOL,
		position_fk VARCHAR(2) NOT NULL
	);

CREATE TABLE position05 (
		code VARCHAR(2) NOT NULL,
		version INT4 NOT NULL,
		name VARCHAR(20) NOT NULL
	);



INSERT INTO position05 VALUES ('PG', 0, 'Point Guard');
INSERT INTO position05 VALUES ('SG', 0, 'Shooting Guard');
INSERT INTO position05 VALUES ('SF', 0, 'Small Forward');
INSERT INTO position05 VALUES ('PF', 0, 'Power Forward');
INSERT INTO position05 VALUES ('C', 0, 'Center');

INSERT INTO player05 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, position_fk) VALUES (201, 0, 'Harry', 'Davis', '1988-04-14', 9.6899996, 159, 237, false, 'PF');
INSERT INTO player05 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, position_fk) VALUES (202, 0, 'Loren', 'Colon', '1971-12-14', 30.139999, 149, 221, false, 'SF');
INSERT INTO player05 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, position_fk) VALUES (203, 0, 'Bob', 'Clark', '1972-09-04', 24.02, 105, 224, false, 'SG');
INSERT INTO player05 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, position_fk) VALUES (205, 0, 'Loren', 'Stokes', '1976-07-17', 30.969999, 105, 186, false, 'SG');
INSERT INTO player05 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, position_fk) VALUES (207, 0, 'Shane', 'Sandoval', '1975-09-24', 24.139999, 131, 182, false, 'PF');
INSERT INTO player05 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, position_fk) VALUES (208, 0, 'Roosevelt', 'Davis', '1983-11-10', 8.5500002, 114, 235, false, 'C');
INSERT INTO player05 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, position_fk) VALUES (210, 0, 'Alton', 'Davis', '1970-02-13', 20.34, 120, 196, false, 'C');
INSERT INTO player05 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, position_fk) VALUES (211, 0, 'Marion', 'Favis', '1970-02-13', 20.34, 120, 196, false, 'C');

ALTER TABLE position05 ADD CONSTRAINT position05_pkey PRIMARY KEY (code);

ALTER TABLE player05 ADD CONSTRAINT player05_pkey PRIMARY KEY (id);

ALTER TABLE player05 ADD CONSTRAINT player05_position_fkc FOREIGN KEY (position_fk)
	REFERENCES position05 (code);
