/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.entities.uc40_defaultsort;


import java.io.Serializable;
import java.util.List;

import com.bluage.documentation.business.standardfeatures.entities.uc40_defaultsort.bos.State40BO;
import com.netfective.bluage.core.web.AbstractForm;
import com.netfective.bluage.core.web.IForm;

/**
* Class : UC40_DefaultSortForm
*/
public class UC40_DefaultSortForm extends AbstractForm implements Serializable, IForm {
	
	/** Constant : serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	//CONSTANT : stateInstance
	private static final String STATE_INSTANCE = "stateInstance";
	//CONSTANT : currentState40
	private static final String CURRENT_STATE40 = "currentState40";
	/**
	 * 	Property: stateInstance 
	 */
	private List<State40BO> stateInstance;
	/**
	 * 	Property: currentState40 
	 */
	private State40BO currentState40;
/**
	 * Default constructor : UC40_DefaultSortForm
	 */
	public UC40_DefaultSortForm() {
		super();
		// Initialize : stateInstance
		this.stateInstance = new java.util.ArrayList<com.bluage.documentation.business.standardfeatures.entities.uc40_defaultsort.bos.State40BO>();
		// Initialize : currentState40
		this.currentState40 = new State40BO();
					this.setAlwaysCallPreControllers(true);
	}
	
	/**
	 * 	Getter : stateInstance 
	 * 	@return : Return the stateInstance instance.
	 */
	public List<State40BO> getStateInstance(){
		return stateInstance; // For UC40_DefaultSortForm
	}
	
	/**
	 * 	Setter : stateInstance 
	 *  @param stateInstanceinstance : The instance to set.
	 */
	public void setStateInstance(final List<State40BO> stateInstanceinstance){
		this.stateInstance = stateInstanceinstance;// For UC40_DefaultSortForm
	}
	/**
	 * 	Getter : currentState40 
	 * 	@return : Return the currentState40 instance.
	 */
	public State40BO getCurrentState40(){
		return currentState40; // For UC40_DefaultSortForm
	}
	
	/**
	 * 	Setter : currentState40 
	 *  @param currentState40instance : The instance to set.
	 */
	public void setCurrentState40(final State40BO currentState40instance){
		this.currentState40 = currentState40instance;// For UC40_DefaultSortForm
	}
/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UC40_DefaultSortForm [ "+
STATE_INSTANCE +" = " + stateInstance +CURRENT_STATE40 +" = " + currentState40 + "]";
	}
	
	/**
	 * Method setInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public void setInstanceByInstanceName(final String instanceName, final Object instance) {
		// Set the instance StateInstance.
		if(STATE_INSTANCE.equals(instanceName)){// For UC40_DefaultSortForm
			this.setStateInstance((List<State40BO>)instance); // For UC40_DefaultSortForm
		}
				// Set the instance CurrentState40.
		if(CURRENT_STATE40.equals(instanceName)){// For UC40_DefaultSortForm
			this.setCurrentState40((State40BO)instance); // For UC40_DefaultSortForm
		}
			}
	
	/**
	 * Method getInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public Object getInstanceByInstanceName(final String instanceName){
		// Temporary object for tmpUC40_DefaultSortForm.
		Object tmpUC40_DefaultSortForm = null;
		
			
		// Get the instance StateInstance for UC40_DefaultSortForm.
		if(STATE_INSTANCE.equals(instanceName)){ // For UC40_DefaultSortForm
			tmpUC40_DefaultSortForm = this.getStateInstance(); // For UC40_DefaultSortForm
		}
			
		// Get the instance CurrentState40 for UC40_DefaultSortForm.
		if(CURRENT_STATE40.equals(instanceName)){ // For UC40_DefaultSortForm
			tmpUC40_DefaultSortForm = this.getCurrentState40(); // For UC40_DefaultSortForm
		}
		return tmpUC40_DefaultSortForm;// For UC40_DefaultSortForm
	}
	
			}
