/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.service.standardfeatures.services.process.uc53_while;


import javax.inject.Inject;
import javax.inject.Named;

import com.bluage.documentation.business.standardfeatures.services.process.uc53_while.bos.Player53BO;
import com.bluage.documentation.business.standardfeatures.services.process.uc53_while.entities.daofinder.Player53DAOFinderImpl;
import com.bluage.documentation.service.AbstractService;
import com.netfective.bluage.core.exception.ApplicationException;
import com.netfective.bluage.core.persistence.IPersistentObjectDAO;

/**
 * Defines  application services for ServicePlayer53FindByID
  */
@Named("com.bluage.documentation.service.standardfeatures.services.process.uc53_while.ServicePlayer53FindByID")
public class ServicePlayer53FindByID  extends AbstractService{


	


	/**
	 *  Default constructor 
	 */
	public ServicePlayer53FindByID() {
		super(); // Call super for default constructor : ServicePlayer53FindByID
	}
	
			@Inject
	@Named ("com.bluage.documentation.business.standardfeatures.services.process.uc53_while.entities.daofinder.Player53DAOFinderImpl")
	private Player53DAOFinderImpl dao;

	/**
	 * constructor with...
	 * 
	 * @param dao
	 *		: {@link IPersistentObjectDAO} the DAO
	 */
	public ServicePlayer53FindByID(Player53DAOFinderImpl dao) {
		this.dao = dao;
	}

			
	/**    
	 * Operation Crud
	 * @param bo {@link Player53BO}
		 * @return {@link Player53BO}
	 */
	public Player53BO player53FindByID(Player53BO bo )
 throws ApplicationException {

					return (Player53BO)dao.findByPrimaryKey(getBoId(bo));
	}
	/**
	 *  Return the primary key from business object.
	 */
	private Object getBoId(Player53BO bo) {
				return bo.getId();
		}

	

}
