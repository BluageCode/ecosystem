/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.services.process.uc54_for ;

// Import declaration.
// Java imports.
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.bluage.documentation.service.standardfeatures.services.process.uc54_for.ServicePlayer54;
import com.bluage.documentation.service.standardfeatures.services.process.uc54_for.ServicePlayer54FindAll;
import com.netfective.bluage.core.exception.ApplicationException;
import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;
import com.netfective.bluage.core.web.IForm;

/**
 * Class : UC54_ForController
 */
@Controller
@Scope("prototype")
@SessionAttributes("uC54_ForForm")
@RequestMapping(value= "/presentation/standardfeatures/services/process/uc54_for")
public class UC54_ForController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC54_ForController.class);
	
	//Current form name
	private static final String U_C54__FOR_FORM = "uC54_ForForm";

							//CONSTANT: teamPlayers
	private static final String TEAM_PLAYERS = "teamPlayers";
	//CONSTANT: players
	private static final String PLAYERS = "players";
	
	/**
	 * Property:customDateEditorsUC54_ForController
	 */
	final private Map<String,CustomDateEditor> customDateEditorsUC54_ForController = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:uC54_ForValidator
	 */
	final private UC54_ForValidator uC54_ForValidator = new UC54_ForValidator();
	
	/**
	 * Service declaration : servicePlayer54FindAll.
	 */
	@Autowired
	private ServicePlayer54FindAll servicePlayer54FindAll;
	
	/**
	 * Service declaration : servicePlayer54.
	 */
	@Autowired
	private ServicePlayer54 servicePlayer54;
	
	
	// Initialise all the instances for UC54_ForController
										// Declare the instance teamPlayers
		private List teamPlayers;
		// Declare the instance players
		private List players;
			/**
	 * Operation : lnk_selectPlayers
 	 * @param model : 
 	 * @param uC54_For : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC54_For/lnk_selectPlayers.html",method = RequestMethod.POST)
	public String lnk_selectPlayers(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC54_ForForm") UC54_ForForm  uC54_ForForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : lnk_selectPlayers"); 
		infoLogger(uC54_ForForm); 
		uC54_ForForm.setAlwaysCallPreControllers(false); 
		// initialization for lnk_selectPlayers
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state lnk_selectPlayers if the validation fail, you will be forward to the screen : "/presentation/standardfeatures/services/process/uc54_for/UC54_For"
		if(errorsMessages(request,response,uC54_ForForm,bindingResult,"", uC54_ForValidator, customDateEditorsUC54_ForController)){ 
			return "/presentation/standardfeatures/services/process/uc54_for/UC54_For"; 
		}

					 callBuildTeamSelection(request,uC54_ForForm );
						// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.services.process.uc54_for.UC54_ForForm or not from lnk_selectPlayers
			// Populate the destination form with all the instances returned from lnk_selectPlayers.
			final IForm uC54_ForForm2 = populateDestinationForm(request.getSession(), uC54_ForForm); 
					
			// Add the form to the model.
			model.addAttribute("uC54_ForForm", uC54_ForForm2); 
			
			request.getSession().setAttribute("uC54_ForForm", uC54_ForForm2);
			
			// "OK" CASE => destination screen path from lnk_selectPlayers
			LOGGER.info("Go to the screen 'UC54_For'.");
			// Redirect (PRG) from lnk_selectPlayers
			destinationPath =  "redirect:/presentation/standardfeatures/services/process/uc54_for/UC54_For.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_selectPlayers 
	}
	
				/**
	 * Operation : lnk_showPlayers
 	 * @param model : 
 	 * @param uC54_For : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC54_For/lnk_showPlayers.html",method = RequestMethod.POST)
	public String lnk_showPlayers(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC54_ForForm") UC54_ForForm  uC54_ForForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : lnk_showPlayers"); 
		infoLogger(uC54_ForForm); 
		uC54_ForForm.setAlwaysCallPreControllers(false); 
		// initialization for lnk_showPlayers
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state lnk_showPlayers if the validation fail, you will be forward to the screen : "/presentation/standardfeatures/services/process/uc54_for/UC54_For"
		if(errorsMessages(request,response,uC54_ForForm,bindingResult,"", uC54_ForValidator, customDateEditorsUC54_ForController)){ 
			return "/presentation/standardfeatures/services/process/uc54_for/UC54_For"; 
		}

	 callFindAllPlayers(request,uC54_ForForm );
						// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.services.process.uc54_for.UC54_ForForm or not from lnk_showPlayers
			// Populate the destination form with all the instances returned from lnk_showPlayers.
			final IForm uC54_ForForm2 = populateDestinationForm(request.getSession(), uC54_ForForm); 
					
			// Add the form to the model.
			model.addAttribute("uC54_ForForm", uC54_ForForm2); 
			
			request.getSession().setAttribute("uC54_ForForm", uC54_ForForm2);
			
			// "OK" CASE => destination screen path from lnk_showPlayers
			LOGGER.info("Go to the screen 'UC54_For'.");
			// Redirect (PRG) from lnk_showPlayers
			destinationPath =  "redirect:/presentation/standardfeatures/services/process/uc54_for/UC54_For.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_showPlayers 
	}
	
	
	/**
	* This method initialise the form : UC54_ForForm 
	* @return UC54_ForForm
	*/
	@ModelAttribute("UC54_ForFormInit")
	public void initUC54_ForForm(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, U_C54__FOR_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method UC54_ForForm."); //for lnk_showPlayers 
		}
		UC54_ForForm uC54_ForForm;
	
		if(request.getSession().getAttribute(U_C54__FOR_FORM) != null){
			uC54_ForForm = (UC54_ForForm)request.getSession().getAttribute(U_C54__FOR_FORM);
		} else {
			uC54_ForForm = new UC54_ForForm();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form UC54_ForForm.");
		}
		uC54_ForForm = (UC54_ForForm)populateDestinationForm(request.getSession(), uC54_ForForm);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  UC54_ForForm.");
		}
		model.addAttribute(U_C54__FOR_FORM, uC54_ForForm);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param UC54_ForForm : The sceen form.
	 */
	@RequestMapping(value = "/UC54_For.html" ,method = RequestMethod.GET)
	public void prepareUC54_For(final HttpServletRequest request,final  Model model,final  @ModelAttribute("UC54_ForFormInit") UC54_ForForm uC54_ForForm){
		
		UC54_ForForm currentUC54_ForForm = uC54_ForForm;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(U_C54__FOR_FORM) == null){
			if(currentUC54_ForForm!=null){
				request.getSession().setAttribute(U_C54__FOR_FORM, currentUC54_ForForm);
			}else {
				currentUC54_ForForm = new UC54_ForForm();
				request.getSession().setAttribute(U_C54__FOR_FORM, currentUC54_ForForm);	
			}
		} else {
			currentUC54_ForForm = (UC54_ForForm) request.getSession().getAttribute(U_C54__FOR_FORM);
		}

						currentUC54_ForForm = (UC54_ForForm)populateDestinationForm(request.getSession(), currentUC54_ForForm);
		// Call all the Precontroller.
	currentUC54_ForForm.setAlwaysCallPreControllers(true);
		model.addAttribute(U_C54__FOR_FORM, currentUC54_ForForm);
		request.getSession().setAttribute(U_C54__FOR_FORM, currentUC54_ForForm);
		
	}
	
								/**
	 * method callBuildTeamSelection
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callBuildTeamSelection(HttpServletRequest request,IForm form) {
		teamPlayers = (List)get(request.getSession(),form, "teamPlayers");  
										 
					players = (List)get(request.getSession(),form, "players");  
										 
					try {
				// executing BuildTeamSelection in lnk_showPlayers
				teamPlayers = 	servicePlayer54.buildTeamSelection(
			players
			);  
 
				put(request.getSession(), TEAM_PLAYERS,teamPlayers);
								// processing variables BuildTeamSelection in lnk_showPlayers
				put(request.getSession(), PLAYERS,players); 
			
			} catch (ApplicationException e) { 
				// error handling for operation buildTeamSelection called BuildTeamSelection
				errorLogger("An error occured during the execution of the operation : buildTeamSelection",e); 
		
			}
	}
		/**
	 * method callFindAllPlayers
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callFindAllPlayers(HttpServletRequest request,IForm form) {
		players = (List)get(request.getSession(),form, "players");  
										 
					try {
				// executing FindAllPlayers in lnk_showPlayers
				players = 	servicePlayer54FindAll.player54FindAll(
	);  
 
				put(request.getSession(), PLAYERS,players);
								// processing variables FindAllPlayers in lnk_showPlayers

			} catch (ApplicationException e) { 
				// error handling for operation player54FindAll called FindAllPlayers
				errorLogger("An error occured during the execution of the operation : player54FindAll",e); 
		
			}
	}
		
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("UC54_ForController [ ");
									strBToS.append(TEAM_PLAYERS);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(teamPlayers);
			strBToS.append(END_TO_STRING_DELIMITER);
			strBToS.append(PLAYERS);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(players);
			strBToS.append(END_TO_STRING_DELIMITER);
		// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(uC54_ForValidator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsUC54_ForController!=null); 
		return strBToS.toString();
	}
}
