/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.service.advancedfeatures.valueobject.uc206_valueobject.voservice;


import javax.inject.Named;

import com.bluage.documentation.business.advancedfeatures.valueobject.uc206_valueobject.bos.Position206BO;
import com.bluage.documentation.business.advancedfeatures.valueobject.uc206_valueobject.vo.Position206VO;
import com.bluage.documentation.service.AbstractVOService;
import com.netfective.bluage.core.exception.ApplicationException;


/**
 * Defines  application VO services for ServicePosition206Copy
 *
 */
					@Named ("com.bluage.documentation.service.advancedfeatures.valueobject.uc206_valueobject.voservice.ServicePosition206Copy")
public class ServicePosition206Copy  extends AbstractVOService<Position206BO,Position206VO> {

	/**
	 * The associated BO
	 */
	private Class<Position206BO> refBo = Position206BO.class;
	
	/**
	 * The corresponding VO
	 */
	private Class<Position206VO> refVo = Position206VO.class;
	
	/**
	* returns the associated BO
	* @return Class
	*/
	@Override
	public Class<Position206BO> getRefBo() {
		return refBo;
	}

	/**
	* returns the corresponding VO
	* @return Class
	*/
	@Override
	public Class<Position206VO> getRefVo() {
		return refVo;
	}

	/**
	 *  Default constructor 
	 */
	public ServicePosition206Copy() {
		super();
	}



			
	/**    
	 * VO Copy operation.
	 * @param vo {@link Position206VO}
		 * @return {@link Position206BO}
	 */
	public Position206BO position206CopyVO2BO(Position206VO vo )
 throws ApplicationException {
		return copyVO2BO(vo);
	}

		
			
	/**    
	 * VO Copy operation.
	 * @param bo {@link Position206BO}
		 * @return {@link Position206VO}
	 */
	public Position206VO position206CopyBO2VO(Position206BO bo )
 throws ApplicationException {
		return copyBO2VO(bo);
	}

		}
