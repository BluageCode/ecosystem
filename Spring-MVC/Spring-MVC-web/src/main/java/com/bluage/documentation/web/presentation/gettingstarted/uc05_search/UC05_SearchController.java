/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.gettingstarted.uc05_search ;

// Import declaration.
// Java imports.
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.bluage.documentation.business.gettingstarted.uc05_search.bos.Player05BO;
import com.bluage.documentation.business.gettingstarted.uc05_search.entities.daofinder.Position05DAOFinderImpl;
import com.bluage.documentation.service.gettingstarted.uc05_search.ServiceSearch;
import com.netfective.bluage.core.exception.ApplicationException;
import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;
import com.netfective.bluage.core.web.IForm;

/**
 * Class : UC05_SearchController
 */
@Controller
@Scope("prototype")
@SessionAttributes("uC05_SearchForm")
@RequestMapping(value= "/presentation/gettingstarted/uc05_search")
public class UC05_SearchController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC05_SearchController.class);
	
	//Current form name
	private static final String U_C05__SEARCH_FORM = "uC05_SearchForm";

						//CONSTANT: allPositions
	private static final String ALL_POSITIONS = "allPositions";
				//CONSTANT: searchResult
	private static final String SEARCH_RESULT = "searchResult";
	//CONSTANT: hqlCriteria
	private static final String HQL_CRITERIA = "hqlCriteria";
	//CONSTANT: playerCriteria
	private static final String PLAYER_CRITERIA = "playerCriteria";
	//CONSTANT: currentPlayer
	private static final String CURRENT_PLAYER = "currentPlayer";
	//CONSTANT: playerSelected
	private static final String PLAYER_SELECTED = "playerSelected";
	
	/**
	 * Property:customDateEditorsUC05_SearchController
	 */
	final private Map<String,CustomDateEditor> customDateEditorsUC05_SearchController = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:uC05_SearchValidator
	 */
	final private UC05_SearchValidator uC05_SearchValidator = new UC05_SearchValidator();
	
	/**
	 * Service declaration : serviceSearch.
	 */
	@Autowired
	private ServiceSearch serviceSearch;
	
	/**
	 * Generic Finder : position05DAOFinderImpl.
	 */
	@Autowired
	private Position05DAOFinderImpl position05DAOFinderImpl;
	
	
	// Initialise all the instances for UC05_SearchController
								// Initialize the instance allPositions for the state lnk_reset
		private List allPositions; // Initialize the instance allPositions for UC05_SearchController
						// Declare the instance searchResult
		private List searchResult;
		// Declare the instance hqlCriteria
		private Player05BO hqlCriteria;
		// Declare the instance playerCriteria
		private Player05BO playerCriteria;
		// Declare the instance currentPlayer
		private Player05BO currentPlayer;
		// Declare the instance playerSelected
		private Player05BO playerSelected;
			/**
	 * Operation : lnk_searchCriteria
 	 * @param model : 
 	 * @param uC05_Search : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC05_Search/lnk_searchCriteria.html",method = RequestMethod.POST)
	public String lnk_searchCriteria(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC05_SearchForm") UC05_SearchForm  uC05_SearchForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : lnk_searchCriteria"); 
		infoLogger(uC05_SearchForm); 
		uC05_SearchForm.setAlwaysCallPreControllers(false); 
		// initialization for lnk_searchCriteria
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state lnk_searchCriteria if the validation fail, you will be forward to the screen : "/presentation/gettingstarted/uc05_search/UC05_Search"
		if(errorsMessages(request,response,uC05_SearchForm,bindingResult,"", uC05_SearchValidator, customDateEditorsUC05_SearchController)){ 
			return "/presentation/gettingstarted/uc05_search/UC05_Search"; 
		}

					 callFindplayerbycriteria(request,uC05_SearchForm );
						// Init the destination form com.bluage.documentation.web.presentation.gettingstarted.uc05_search.UC05_SearchForm or not from lnk_searchCriteria
			// Populate the destination form with all the instances returned from lnk_searchCriteria.
			final IForm uC05_SearchForm2 = populateDestinationForm(request.getSession(), uC05_SearchForm); 
					
			// Add the form to the model.
			model.addAttribute("uC05_SearchForm", uC05_SearchForm2); 
			
			request.getSession().setAttribute("uC05_SearchForm", uC05_SearchForm2);
			
			// "OK" CASE => destination screen path from lnk_searchCriteria
			LOGGER.info("Go to the screen 'UC05_Search'.");
			// Redirect (PRG) from lnk_searchCriteria
			destinationPath =  "redirect:/presentation/gettingstarted/uc05_search/UC05_Search.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_searchCriteria 
	}
	
				/**
	 * Operation : lnk_searchProperties
 	 * @param model : 
 	 * @param uC05_Search : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC05_Search/lnk_searchProperties.html",method = RequestMethod.POST)
	public String lnk_searchProperties(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC05_SearchForm") UC05_SearchForm  uC05_SearchForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : lnk_searchProperties"); 
		infoLogger(uC05_SearchForm); 
		uC05_SearchForm.setAlwaysCallPreControllers(false); 
		// initialization for lnk_searchProperties
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state lnk_searchProperties if the validation fail, you will be forward to the screen : "/presentation/gettingstarted/uc05_search/UC05_Search"
		if(errorsMessages(request,response,uC05_SearchForm,bindingResult,"", uC05_SearchValidator, customDateEditorsUC05_SearchController)){ 
			return "/presentation/gettingstarted/uc05_search/UC05_Search"; 
		}

					 callFindplayerbyproperties(request,uC05_SearchForm );
						// Init the destination form com.bluage.documentation.web.presentation.gettingstarted.uc05_search.UC05_SearchForm or not from lnk_searchProperties
			// Populate the destination form with all the instances returned from lnk_searchProperties.
			final IForm uC05_SearchForm2 = populateDestinationForm(request.getSession(), uC05_SearchForm); 
					
			// Add the form to the model.
			model.addAttribute("uC05_SearchForm", uC05_SearchForm2); 
			
			request.getSession().setAttribute("uC05_SearchForm", uC05_SearchForm2);
			
			// "OK" CASE => destination screen path from lnk_searchProperties
			LOGGER.info("Go to the screen 'UC05_Search'.");
			// Redirect (PRG) from lnk_searchProperties
			destinationPath =  "redirect:/presentation/gettingstarted/uc05_search/UC05_Search.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_searchProperties 
	}
	
				/**
	 * Operation : lnk_view
 	 * @param model : 
 	 * @param uC05_Search : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC05_Search/lnk_view.html",method = RequestMethod.POST)
	public String lnk_view(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC05_SearchForm") UC05_SearchForm  uC05_SearchForm, final BindingResult bindingResult ,final @RequestParam(ID_REQUEST_PARAM) Integer index){
		
		
		
		LOGGER.info("Begin method : lnk_view"); 
		infoLogger(uC05_SearchForm); 
		uC05_SearchForm.setAlwaysCallPreControllers(false); 
		// initialization for lnk_view
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state lnk_view if the validation fail, you will be forward to the screen : "/presentation/gettingstarted/uc05_search/UC05_Search"
		if(errorsMessages(request,response,uC05_SearchForm,bindingResult,"playerSelected", uC05_SearchValidator, customDateEditorsUC05_SearchController)){ 
			return "/presentation/gettingstarted/uc05_search/UC05_Search"; 
		}

										 callViewselectedplayer(request,uC05_SearchForm , index);
											// Init the destination form com.bluage.documentation.web.presentation.gettingstarted.uc05_search.UC05_ViewForm or not from lnk_view
			final UC05_ViewForm uC05_ViewForm =  new UC05_ViewForm();
			// Populate the destination form with all the instances returned from lnk_view.
			final IForm uC05_ViewForm2 = populateDestinationForm(request.getSession(), uC05_ViewForm); 
					
			// Add the form to the model.
			model.addAttribute("uC05_ViewForm", uC05_ViewForm2); 
			
			request.getSession().setAttribute("uC05_ViewForm", uC05_ViewForm2);
			
			// "OK" CASE => destination screen path from lnk_view
			LOGGER.info("Go to the screen 'UC05_View'.");
			// Redirect (PRG) from lnk_view
			destinationPath =  "redirect:/presentation/gettingstarted/uc05_search/UC05_View.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_view 
	}
	
	
	/**
	* This method initialise the form : UC05_SearchForm 
	* @return UC05_SearchForm
	*/
	@ModelAttribute("UC05_SearchFormInit")
	public void initUC05_SearchForm(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, U_C05__SEARCH_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method UC05_SearchForm."); //for lnk_view 
		}
		UC05_SearchForm uC05_SearchForm;
	
		if(request.getSession().getAttribute(U_C05__SEARCH_FORM) != null){
			uC05_SearchForm = (UC05_SearchForm)request.getSession().getAttribute(U_C05__SEARCH_FORM);
		} else {
			uC05_SearchForm = new UC05_SearchForm();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form UC05_SearchForm.");
		}
		uC05_SearchForm = (UC05_SearchForm)populateDestinationForm(request.getSession(), uC05_SearchForm);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  UC05_SearchForm.");
		}
		model.addAttribute(U_C05__SEARCH_FORM, uC05_SearchForm);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param UC05_SearchForm : The sceen form.
	 */
	@RequestMapping(value = "/UC05_Search.html" ,method = RequestMethod.GET)
	public void prepareUC05_Search(final HttpServletRequest request,final  Model model,final  @ModelAttribute("UC05_SearchFormInit") UC05_SearchForm uC05_SearchForm){
		
		UC05_SearchForm currentUC05_SearchForm = uC05_SearchForm;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(U_C05__SEARCH_FORM) == null){
			if(currentUC05_SearchForm!=null){
				request.getSession().setAttribute(U_C05__SEARCH_FORM, currentUC05_SearchForm);
			}else {
				currentUC05_SearchForm = new UC05_SearchForm();
				request.getSession().setAttribute(U_C05__SEARCH_FORM, currentUC05_SearchForm);	
			}
		} else {
			currentUC05_SearchForm = (UC05_SearchForm) request.getSession().getAttribute(U_C05__SEARCH_FORM);
		}

		try {
			List allPositions = position05DAOFinderImpl.findAll();
			put(request.getSession(), ALL_POSITIONS, allPositions);
		} catch (ApplicationException e) {
			errorLogger("An error while calling the generic finder for the instance : allPositions.",e);
		}
					currentUC05_SearchForm = (UC05_SearchForm)populateDestinationForm(request.getSession(), currentUC05_SearchForm);
		// Call all the Precontroller.
	currentUC05_SearchForm.setAlwaysCallPreControllers(true);
		model.addAttribute(U_C05__SEARCH_FORM, currentUC05_SearchForm);
		request.getSession().setAttribute(U_C05__SEARCH_FORM, currentUC05_SearchForm);
		
	}
	
							/**
	 * method callFindplayerbycriteria
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callFindplayerbycriteria(HttpServletRequest request,IForm form) {
		searchResult = (List)get(request.getSession(),form, "searchResult");  
										 
					hqlCriteria = (Player05BO)get(request.getSession(),form, "hqlCriteria");  
										 
					try {
				// executing Findplayerbycriteria in lnk_view
				searchResult = 	serviceSearch.player05ExecuteSearch(
			hqlCriteria
			);  
 
				put(request.getSession(), SEARCH_RESULT,searchResult);
								// processing variables Findplayerbycriteria in lnk_view
				put(request.getSession(), HQL_CRITERIA,hqlCriteria); 
			
			} catch (ApplicationException e) { 
				// error handling for operation player05ExecuteSearch called Findplayerbycriteria
				errorLogger("An error occured during the execution of the operation : player05ExecuteSearch",e); 
		
			}
	}
					/**
	 * method callFindplayerbyproperties
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callFindplayerbyproperties(HttpServletRequest request,IForm form) {
		searchResult = (List)get(request.getSession(),form, "searchResult");  
										 
					playerCriteria = (Player05BO)get(request.getSession(),form, "playerCriteria");  
										 
					try {
				// executing Findplayerbyproperties in lnk_view
				searchResult = 	serviceSearch.player05ExecuteSearchByProperties(
			playerCriteria
			);  
 
				put(request.getSession(), SEARCH_RESULT,searchResult);
								// processing variables Findplayerbyproperties in lnk_view
				put(request.getSession(), PLAYER_CRITERIA,playerCriteria); 
			
			} catch (ApplicationException e) { 
				// error handling for operation player05ExecuteSearchByProperties called Findplayerbyproperties
				errorLogger("An error occured during the execution of the operation : player05ExecuteSearchByProperties",e); 
		
			}
	}
									/**
	 * method callViewselectedplayer
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 * @param index Integer the current index 
	 */
	private void callViewselectedplayer(HttpServletRequest request,IForm form,Integer index) {
		currentPlayer = (Player05BO)get(request.getSession(),form, "currentPlayer");  
										 
					if(index!=null){  
			 playerSelected = (Player05BO)((List)get(request.getSession(),form, "searchResult")).get(index);  
		} else {
			 playerSelected= null; 
		}
				 
					try {
				// executing Viewselectedplayer in lnk_view
				currentPlayer = 	serviceSearch.playerSearchById(
			playerSelected
			);  
 
				put(request.getSession(), CURRENT_PLAYER,currentPlayer);
								// processing variables Viewselectedplayer in lnk_view
				put(request.getSession(), PLAYER_SELECTED,playerSelected); 
			
			} catch (ApplicationException e) { 
				// error handling for operation playerSearchById called Viewselectedplayer
				errorLogger("An error occured during the execution of the operation : playerSearchById",e); 
		
			}
	}
		
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("UC05_SearchController [ ");
								strBToS.append(ALL_POSITIONS);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(allPositions);
			strBToS.append(END_TO_STRING_DELIMITER);
						strBToS.append(SEARCH_RESULT);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(searchResult);
			strBToS.append(END_TO_STRING_DELIMITER);
			strBToS.append(HQL_CRITERIA);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(hqlCriteria);
			strBToS.append(END_TO_STRING_DELIMITER);
			strBToS.append(PLAYER_CRITERIA);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(playerCriteria);
			strBToS.append(END_TO_STRING_DELIMITER);
			strBToS.append(CURRENT_PLAYER);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(currentPlayer);
			strBToS.append(END_TO_STRING_DELIMITER);
			strBToS.append(PLAYER_SELECTED);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(playerSelected);
			strBToS.append(END_TO_STRING_DELIMITER);
		// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(uC05_SearchValidator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsUC05_SearchController!=null); 
		return strBToS.toString();
	}
}
