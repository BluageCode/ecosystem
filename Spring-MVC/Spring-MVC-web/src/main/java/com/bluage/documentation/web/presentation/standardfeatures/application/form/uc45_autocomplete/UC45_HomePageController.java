/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.application.form.uc45_autocomplete ;

// Import declaration.
// Java imports.
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.bluage.documentation.business.standardfeatures.application.form.uc45_autocomplete.bos.Player45BO;
import com.bluage.documentation.service.standardfeatures.application.form.uc45_autocomplete.ServicePlayer45;
import com.bluage.documentation.service.standardfeatures.application.form.uc45_autocomplete.ServicePlayer45FindAll;
import com.netfective.bluage.core.exception.ApplicationException;
import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;
import com.netfective.bluage.core.web.IForm;

/**
 * Class : UC45_HomePageController
 */
@Controller
@Scope("prototype")
@SessionAttributes("uC45_HomePageForm")
@RequestMapping(value= "/presentation/standardfeatures/application/form/uc45_autocomplete")
public class UC45_HomePageController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC45_HomePageController.class);
	
	//Current form name
	private static final String U_C45__HOME_PAGE_FORM = "uC45_HomePageForm";

	//CONSTANT: name
	private static final String NAME = "name";
	//CONSTANT: instance_players
	private static final String INSTANCE_PLAYERS = "instance_players";
	
	/**
	 * Property:customDateEditorsUC45_HomePageController
	 */
	final private Map<String,CustomDateEditor> customDateEditorsUC45_HomePageController = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:uC45_HomePageValidator
	 */
	final private UC45_HomePageValidator uC45_HomePageValidator = new UC45_HomePageValidator();
	
	/**
	 * Service declaration : servicePlayer45.
	 */
	@Autowired
	private ServicePlayer45 servicePlayer45;
	
	/**
	 * Service declaration : servicePlayer45FindAll.
	 */
	@Autowired
	private ServicePlayer45FindAll servicePlayer45FindAll;
	
	
	// Initialise all the instances for UC45_HomePageController
		// Declare the instance name
		private Player45BO name;
		// Declare the instance instance_players
		private List instance_players;
					/**
	 * Operation : lnk_autocomplete
 	 * @param model : 
 	 * @param uC45_HomePage : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC45_HomePage/lnk_autocomplete.html",method = RequestMethod.GET)
	public String lnk_autocomplete(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC45_HomePageForm") UC45_HomePageForm  uC45_HomePageForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : lnk_autocomplete"); 
		infoLogger(uC45_HomePageForm); 
		uC45_HomePageForm.setAlwaysCallPreControllers(false); 
		// initialization for lnk_autocomplete
		init();
		
		String destinationPath = null; 
		

	 callPlayersfind(request,uC45_HomePageForm );
			
 callInitplayer(request,uC45_HomePageForm );
											// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.application.form.uc45_autocomplete.uc45_autocompleteForm or not from lnk_autocomplete
			final uc45_autocompleteForm uc45_autocompleteForm =  new uc45_autocompleteForm();
			// Populate the destination form with all the instances returned from lnk_autocomplete.
			final IForm uc45_autocompleteForm2 = populateDestinationForm(request.getSession(), uc45_autocompleteForm); 
					
			// Add the form to the model.
			model.addAttribute("uc45_autocompleteForm", uc45_autocompleteForm2); 
			
			request.getSession().setAttribute("uc45_autocompleteForm", uc45_autocompleteForm2);
			
			// "OK" CASE => destination screen path from lnk_autocomplete
			LOGGER.info("Go to the screen 'uc45_autocomplete'.");
			// Redirect (PRG) from lnk_autocomplete
			destinationPath =  "redirect:/presentation/standardfeatures/application/form/uc45_autocomplete/uc45_autocomplete.html";
						
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_autocomplete 
	}
	
	
	/**
	* This method initialise the form : UC45_HomePageForm 
	* @return UC45_HomePageForm
	*/
	@ModelAttribute("UC45_HomePageFormInit")
	public void initUC45_HomePageForm(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, U_C45__HOME_PAGE_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method UC45_HomePageForm."); //for lnk_autocomplete 
		}
		UC45_HomePageForm uC45_HomePageForm;
	
		if(request.getSession().getAttribute(U_C45__HOME_PAGE_FORM) != null){
			uC45_HomePageForm = (UC45_HomePageForm)request.getSession().getAttribute(U_C45__HOME_PAGE_FORM);
		} else {
			uC45_HomePageForm = new UC45_HomePageForm();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form UC45_HomePageForm.");
		}
		uC45_HomePageForm = (UC45_HomePageForm)populateDestinationForm(request.getSession(), uC45_HomePageForm);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  UC45_HomePageForm.");
		}
		model.addAttribute(U_C45__HOME_PAGE_FORM, uC45_HomePageForm);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param UC45_HomePageForm : The sceen form.
	 */
	@RequestMapping(value = "/UC45_HomePage.html" ,method = RequestMethod.GET)
	public void prepareUC45_HomePage(final HttpServletRequest request,final  Model model,final  @ModelAttribute("UC45_HomePageFormInit") UC45_HomePageForm uC45_HomePageForm){
		
		UC45_HomePageForm currentUC45_HomePageForm = uC45_HomePageForm;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(U_C45__HOME_PAGE_FORM) == null){
			if(currentUC45_HomePageForm!=null){
				request.getSession().setAttribute(U_C45__HOME_PAGE_FORM, currentUC45_HomePageForm);
			}else {
				currentUC45_HomePageForm = new UC45_HomePageForm();
				request.getSession().setAttribute(U_C45__HOME_PAGE_FORM, currentUC45_HomePageForm);	
			}
		} else {
			currentUC45_HomePageForm = (UC45_HomePageForm) request.getSession().getAttribute(U_C45__HOME_PAGE_FORM);
		}

		currentUC45_HomePageForm = (UC45_HomePageForm)populateDestinationForm(request.getSession(), currentUC45_HomePageForm);
		// Call all the Precontroller.
	currentUC45_HomePageForm.setAlwaysCallPreControllers(true);
		model.addAttribute(U_C45__HOME_PAGE_FORM, currentUC45_HomePageForm);
		request.getSession().setAttribute(U_C45__HOME_PAGE_FORM, currentUC45_HomePageForm);
		
	}
	
	/**
	 * method callInitplayer
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callInitplayer(HttpServletRequest request,IForm form) {
		name = (Player45BO)get(request.getSession(),form, "name");  
										 
					try {
				// executing Initplayer in lnk_autocomplete
				name = 	servicePlayer45.initPlayers45(
	);  
 
				put(request.getSession(), NAME,name);
								// processing variables Initplayer in lnk_autocomplete

			} catch (ApplicationException e) { 
				// error handling for operation initPlayers45 called Initplayer
				errorLogger("An error occured during the execution of the operation : initPlayers45",e); 
		
			}
	}
		/**
	 * method callPlayersfind
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callPlayersfind(HttpServletRequest request,IForm form) {
		instance_players = (List)get(request.getSession(),form, "instance_players");  
										 
					try {
				// executing Playersfind in lnk_autocomplete
				instance_players = 	servicePlayer45FindAll.player45FindAll(
	);  
 
				put(request.getSession(), INSTANCE_PLAYERS,instance_players);
								// processing variables Playersfind in lnk_autocomplete

			} catch (ApplicationException e) { 
				// error handling for operation player45FindAll called Playersfind
				errorLogger("An error occured during the execution of the operation : player45FindAll",e); 
		
			}
	}
		
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("UC45_HomePageController [ ");
			strBToS.append(NAME);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(name);
			strBToS.append(END_TO_STRING_DELIMITER);
			strBToS.append(INSTANCE_PLAYERS);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(instance_players);
			strBToS.append(END_TO_STRING_DELIMITER);
		// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(uC45_HomePageValidator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsUC45_HomePageController!=null); 
		return strBToS.toString();
	}
}
