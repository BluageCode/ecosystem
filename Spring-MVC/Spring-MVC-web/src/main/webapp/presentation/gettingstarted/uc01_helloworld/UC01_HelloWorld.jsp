<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file="/include.jsp"  %>
<html >
<head >
<title  > 
<fmt:message key="cUC01_HelloWorld_BLU_AGE_DOCUMENTATION"/>
</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"  > 
</meta>
<link href="<c:url value= "/presentation/_style/_style.css"/>" rel="stylesheet" type="text/css"  /> 
<link href="<c:url value= "/presentation/_include/jquery/rd_lib.css"/>" rel="stylesheet" type="text/css"  /> 
<script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/jquery.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/rd_setup.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/rd_lib.js"/>" space="preserve"  ></script><link rel="shortcut icon" href="<c:url value= "/presentation/_style/favicon.ico"/>"  /> 

</head>

<body class=" " > 
<div id="rd_ghost"  > 
<small  > 
<fmt:message key="cUC01_HelloWorld___CSS_DEACTIVATED"/>
</small>
</div>
<div id="rd_jghost"  > 
<small  > 
<fmt:message key="cUC01_HelloWorld___JAVASCRIPT_DEACTIVATED"/>
</small>
</div>
<div id="shape"  > 
<div class="ui-layout-center"  > 
<div class="east-north"  > 
	<form:form id="uC01_HelloWorld" commandName="uC01_HelloWorldForm"  enctype="application/x-www-form-urlencoded" method="post">
<h2 class="rd_first"  > 
<fmt:message key="cUC01_HelloWorld_GETTING_STARTED__UC01__HELLO"/>
</h2>
<table width="100%"  >
<tr  > 
<td align="center" width="50%" rowspan="1" colspan="1"  >
<a id="lnk_display"onclick="javascript:btn_click_cExecuteUC01_HelloWorld(this.href);return false;"				href="<c:url value="/presentation/gettingstarted/uc01_helloworld/UC01_HelloWorld/lnk_display.html"/>"
							 shape="rect"  >									 
<fmt:message key="cUC01_HelloWorld_DISPLAY_HELLO_WORLD"/>
</a>
</td> 
<td align="center" rowspan="1" colspan="1"  >
<a id="lnk_reset"onclick="javascript:btn_click_cExecuteUC01_HelloWorld(this.href);return false;"				href="<c:url value="/presentation/gettingstarted/uc01_helloworld/UC01_HelloWorld/lnk_reset.html"/>"
							 shape="rect"  >									 
<fmt:message key="cUC01_HelloWorld_RESET"/>
</a>
</td> 
</tr>
<tr  > 
<td colspan="2" align="center" rowspan="1"  >
		<span id="dyn_helloworld">
	<c:out value="${uC01_HelloWorldForm.helloWorld.text}" />
		</span>
	</td> 
</tr>
</table> 
	</form:form>
	<script language="javascript">
		function btn_click_cExecuteUC01_HelloWorld(url) {
			UC01_HelloWorldvar = document.getElementById('uC01_HelloWorld');
			UC01_HelloWorldvar.setAttribute('action', url);
			UC01_HelloWorldvar.submit();
		}
		function btn_click_cSetActionUC01_HelloWorld(url) {
			UC01_HelloWorldvar = document.getElementById('uC01_HelloWorld');
			UC01_HelloWorldvar.setAttribute('action', url);
		}
	</script>
</div>
<div class="east-center"  > 
<tiles:insertDefinition	name="presentation/gettingstarted/uc01_helloworld/UC01_HelloWorldcmp_tabs" />
</div>
</div>
<div class="ui-layout-north"  > 
<tiles:insertDefinition	name="presentation/gettingstarted/uc01_helloworld/UC01_HelloWorldcmp_header" />
</div>
<div class="ui-layout-south"  > 
<fmt:message key="cUC01_HelloWorld_______"/>
<span id="rd_jversion"  > 
<fmt:message key="cUC01_HelloWorld_BLU_AGE_DOCUMENTATION_36"/>
</span>
</div>
<div class="ui-layout-west"  > 
<tiles:insertDefinition	name="presentation/gettingstarted/uc01_helloworld/UC01_HelloWorldcmp_treeview" />
</div>
</div>
</body>
</html>
