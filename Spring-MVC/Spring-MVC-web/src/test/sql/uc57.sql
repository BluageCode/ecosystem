--<ScriptOptions statementTerminator=";"/>

ALTER TABLE team57 DROP CONSTRAINT team57_state_fkc;

ALTER TABLE player57 DROP CONSTRAINT player57_team57_fkc;

ALTER TABLE team57 DROP CONSTRAINT team57_pkey;

ALTER TABLE player57 DROP CONSTRAINT player57_pkey;

ALTER TABLE state57 DROP CONSTRAINT state57_pkey;

DROP TABLE team57;

DROP TABLE player57;

DROP TABLE state57;

CREATE TABLE team57 (
		id INT8 NOT NULL,
		version INT4 NOT NULL,
		city VARCHAR(1024),
		name VARCHAR(1024),
		budget INT4,
		state_fk VARCHAR(1024) NOT NULL
	);

CREATE TABLE player57 (
		id INT8 NOT NULL,
		version INT4 NOT NULL,
		first_name VARCHAR(1024) NOT NULL,
		last_name VARCHAR(1024),
		date_of_birth DATE,
		estimated_value FLOAT4,
		weight INT4,
		height INT4,
		rookie BOOL,
		team57_fk INT8
	);

CREATE TABLE state57 (
		code VARCHAR(1024) NOT NULL,
		version INT4 NOT NULL,
		name VARCHAR(1024) NOT NULL
	);
	

INSERT INTO state57 (code, version, name) VALUES ('AK', 0, 'Alaska');
INSERT INTO state57 (code, version, name) VALUES ('AL', 0, 'Alabama');
INSERT INTO state57 (code, version, name) VALUES ('AR', 0, 'Arkansas');
INSERT INTO state57 (code, version, name) VALUES ('AZ', 0, 'Arizona');
INSERT INTO state57 (code, version, name) VALUES ('CA', 0, 'California');
INSERT INTO state57 (code, version, name) VALUES ('CO', 0, 'Colorado');
INSERT INTO state57 (code, version, name) VALUES ('CT', 0, 'Connecticut');
INSERT INTO state57 (code, version, name) VALUES ('DE', 0, 'Delaware');
INSERT INTO state57 (code, version, name) VALUES ('FL', 0, 'Florida');
INSERT INTO state57 (code, version, name) VALUES ('GA', 0, 'Georgia');
INSERT INTO state57 (code, version, name) VALUES ('HI', 0, 'Hawaii');
INSERT INTO state57 (code, version, name) VALUES ('IA', 0, 'Iowa');
INSERT INTO state57 (code, version, name) VALUES ('ID', 0, 'Idaho');
INSERT INTO state57 (code, version, name) VALUES ('IL', 0, 'Illinois');
INSERT INTO state57 (code, version, name) VALUES ('IN', 0, 'Indiana');

INSERT INTO team57 (id, version, city, name, budget, state_fk) VALUES (1, 0, 'Charlotte', 'Tigers', 1391371, 'AK');
INSERT INTO team57 (id, version, city, name, budget, state_fk) VALUES (2, 0, 'Kansas City', 'Bears', 9274552, 'AL');
INSERT INTO team57 (id, version, city, name, budget, state_fk) VALUES (3, 0, 'Detroit', 'Chargers', 4496712, 'AR');
INSERT INTO team57 (id, version, city, name, budget, state_fk) VALUES (4, 0, 'Detroit', 'Hawks', 5019847, 'AK');
INSERT INTO team57 (id, version, city, name, budget, state_fk) VALUES (5, 0, 'Omaha', 'Braves', 2988752, 'AR');
INSERT INTO team57 (id, version, city, name, budget, state_fk) VALUES (6, 0, 'Raleigh', 'Eagles', 4565410, 'CA');
INSERT INTO team57 (id, version, city, name, budget, state_fk) VALUES (7, 0, 'Oakland', 'Cardinals', 5521097, 'CO');
INSERT INTO team57 (id, version, city, name, budget, state_fk) VALUES (8, 0, 'Kansas City', 'Red Devils', 5321865, 'CT');
INSERT INTO team57 (id, version, city, name, budget, state_fk) VALUES (9, 0, 'Fresno', 'Mustangs', 6190468, 'DE');
INSERT INTO team57 (id, version, city, name, budget, state_fk) VALUES (10, 0, 'Sacramento', 'Wolverines', 5890145, 'FL');

INSERT INTO player57 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team57_fk) VALUES (201, 0, 'Oliver', 'Frank', '1968-10-31', 0.11, 116, 228, false, 1);
INSERT INTO player57 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team57_fk) VALUES (202, 0, 'Elbert', 'Cole', '1965-04-07', 3.95, 100, 171, false, 2);
INSERT INTO player57 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team57_fk) VALUES (204, 0, 'Ricky', 'Dawson', '1990-03-03', 29.299999, 115, 168, true, 4);
INSERT INTO player57 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team57_fk) VALUES (211, 0, 'Keith', 'Barnett', '1976-11-10', 5.8099999, 66, 190, false, 2);
INSERT INTO player57 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team57_fk) VALUES (203, 0, 'Abel', 'Dennis', '1980-04-16', 30.51, 105, 208, false, 3);
INSERT INTO player57 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team57_fk) VALUES (205, 0, 'Earl', 'Russell', '1981-12-15', 44.73, 60, 234, false, 5);
INSERT INTO player57 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team57_fk) VALUES (206, 0, 'Nick', 'Garner', '1992-07-27', 47.84, 87, 221, true, 6);
INSERT INTO player57 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team57_fk) VALUES (207, 0, 'Howard', 'Ramos', '1970-08-25', 10.76, 107, 199, false, 7);
INSERT INTO player57 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team57_fk) VALUES (208, 0, 'Allan', 'Meyer', '1969-02-14', 33.669998, 114, 229, false, 8);
INSERT INTO player57 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team57_fk) VALUES (209, 0, 'Ruben', 'Pope', '1985-05-01', 44.66, 86, 201, false, 9);
INSERT INTO player57 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team57_fk) VALUES (210, 0, 'Lester', 'Cross', '1976-02-21', 30.98, 69, 214, false, 10);
INSERT INTO player57 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team57_fk) VALUES (212, 0, 'Ivan', 'Tate', '1972-02-08', 48.490002, 143, 175, false, 1);
INSERT INTO player57 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team57_fk) VALUES (213, 0, 'Tommy', 'Wood', '1992-01-20', 13.93, 77, 173, true, 3);
INSERT INTO player57 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team57_fk) VALUES (214, 0, 'Sylvester', 'Nash', '1965-07-10', 37.73, 68, 184, false, 4);
INSERT INTO player57 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team57_fk) VALUES (215, 0, 'Michael', 'Hodges', '1988-04-01', 25.35, 158, 204, false, 5);
INSERT INTO player57 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team57_fk) VALUES (216, 0, 'Jerome', 'Grant', '1982-06-28', 28.940001, 105, 205, false, 6);
INSERT INTO player57 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team57_fk) VALUES (217, 0, 'Woodrow', 'Carpenter', '1987-08-08', 41.759998, 69, 224, false, 7);
INSERT INTO player57 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team57_fk) VALUES (218, 0, 'Roderick', 'Robbins', '1975-07-29', 12.04, 115, 164, false, 8);
INSERT INTO player57 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team57_fk) VALUES (219, 0, 'Christian', 'Chavez', '1983-05-17', 37.880001, 97, 209, false, 9);
INSERT INTO player57 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team57_fk) VALUES (220, 0, 'Cecil', 'Conner', '1978-06-29', 39.709999, 160, 170, false, 10);
INSERT INTO player57 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team57_fk) VALUES (401, 0, 'Gregg', 'Nash', '1972-01-11', 6.8600001, 147, 215, false, 1);
INSERT INTO player57 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team57_fk) VALUES (402, 0, 'Floyd', 'Howell', '1979-08-25', 25.709999, 155, 192, false, 1);
INSERT INTO player57 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team57_fk) VALUES (403, 0, 'Simon', 'Klein', '1987-01-21', 22.24, 63, 203, false, 1);
INSERT INTO player57 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team57_fk) VALUES (404, 0, 'Blake', 'Montgomery', '1992-11-16', 37.470001, 113, 205, true, 1);
INSERT INTO player57 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team57_fk) VALUES (405, 0, 'Doyle', 'Burns', '1990-02-23', 39.130001, 153, 225, true, 1);
INSERT INTO player57 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team57_fk) VALUES (406, 0, 'Troy', 'Rowe', '1973-02-12', 26.82, 94, 211, false, 1);
INSERT INTO player57 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team57_fk) VALUES (407, 0, 'Jesus', 'Jimenez', '1985-09-01', 17.73, 133, 159, false, 1);
INSERT INTO player57 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team57_fk) VALUES (408, 0, 'Ross', 'Guerrero', '1992-01-26', 13.68, 136, 171, true, 1);
INSERT INTO player57 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team57_fk) VALUES (409, 0, 'Eddie', 'Boyd', '1967-10-05', 21.719999, 118, 232, false, 1);
INSERT INTO player57 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team57_fk) VALUES (410, 0, 'Luther', 'Peterson', '1985-06-09', 1.04, 83, 181, false, 1);
INSERT INTO player57 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team57_fk) VALUES (411, 0, 'Daniel', 'Vargas', '1979-04-21', 30.43, 117, 155, false, 2);
INSERT INTO player57 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team57_fk) VALUES (412, 0, 'Freddie', 'Kelly', '1967-03-22', 34.400002, 81, 196, false, 2);
INSERT INTO player57 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team57_fk) VALUES (413, 0, 'Sam', 'Farmer', '1987-11-29', 37.580002, 98, 201, false, 2);
INSERT INTO player57 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team57_fk) VALUES (414, 0, 'Randal', 'Hampton', '1967-12-01', 31.219999, 121, 202, false, 2);
INSERT INTO player57 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team57_fk) VALUES (415, 0, 'Felipe', 'Olson', '1976-09-28', 8.0600004, 129, 189, false, 2);
INSERT INTO player57 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team57_fk) VALUES (416, 0, 'Gary', 'Christensen', '1974-03-13', 21.52, 145, 170, false, 2);
INSERT INTO player57 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team57_fk) VALUES (417, 0, 'Aaron', 'Mullins', '1972-01-15', 8.54, 86, 239, false, 2);
INSERT INTO player57 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team57_fk) VALUES (418, 0, 'Miguel', 'Moore', '1967-03-26', 9.4799995, 67, 173, false, 2);
INSERT INTO player57 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team57_fk) VALUES (419, 0, 'Bruce', 'Adkins', '1986-05-24', 43.720001, 150, 209, false, 2);
INSERT INTO player57 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team57_fk) VALUES (420, 0, 'Felipe', 'Lawrence', '1981-04-19', 33.66, 150, 219, false, 3);
INSERT INTO player57 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team57_fk) VALUES (421, 0, 'Nelson', 'Davis', '1978-05-16', 20.84, 119, 171, false, 3);
INSERT INTO player57 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team57_fk) VALUES (422, 0, 'Jeremiah', 'Johnston', '1983-12-15', 14.57, 149, 200, false, 3);
INSERT INTO player57 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team57_fk) VALUES (423, 0, 'Enrique', 'Wilkins', '1967-01-21', 47.740002, 75, 240, false, 3);
INSERT INTO player57 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team57_fk) VALUES (424, 0, 'Gerald', 'Jimenez', '1970-01-22', 29.26, 63, 166, false, 3);
INSERT INTO player57 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team57_fk) VALUES (425, 0, 'Michael', 'Simon', '1975-12-23', 43.950001, 118, 222, false, 3);



ALTER TABLE team57 ADD CONSTRAINT team57_pkey PRIMARY KEY (id);

ALTER TABLE player57 ADD CONSTRAINT player57_pkey PRIMARY KEY (id);

ALTER TABLE state57 ADD CONSTRAINT state57_pkey PRIMARY KEY (code);

ALTER TABLE team57 ADD CONSTRAINT team57_state_fkc FOREIGN KEY (state_fk)
	REFERENCES state57 (code);

ALTER TABLE player57 ADD CONSTRAINT player57_team57_fkc FOREIGN KEY (team57_fk)
	REFERENCES team57 (id);

