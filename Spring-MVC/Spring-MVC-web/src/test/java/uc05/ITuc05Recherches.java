package uc05;

import org.junit.Test;
import test.common.AbstractBADocTestCase;

public class ITuc05Recherches extends AbstractBADocTestCase {

	
	@Override
	public  String getName(){
		return "UC-05";
	}

	@Test
	public void testITuc05Recherches() throws Exception {
		// On teste l'UC05 - Getting started - Search
		// On teste l'UC en mettant un nom et en selectionnant une position. [search with abvoe properties]
		selenium.open("");
		selenium.waitForPageToLoad("30000");
		selenium.click("link=Expand All");
		selenium.click("link=UC05 - Search");
		// On vérife l'affichage :
		assertTrue(selenium.isElementPresent("//label[contains(text(),'First Name')]"));
		assertTrue(selenium.isElementPresent("//label[contains(text(),'Last Name')]"));
		assertTrue(selenium.isElementPresent("//label[contains(text(),'Position')]"));
		assertTrue(selenium.isElementPresent("//label[contains(text(),'Last Name begins with')]"));
		assertTrue(selenium.isElementPresent("//label[contains(text(),'Height between')]"));
		// On vérife les positions:
		assertTrue(selenium.isElementPresent("//option[contains(text(),'Point Guard')]"));
		assertTrue(selenium.isElementPresent("//option[contains(text(),'Shooting Guard')]"));
		assertTrue(selenium.isElementPresent("//option[contains(text(),'Small Forward')]"));
		assertTrue(selenium.isElementPresent("//option[contains(text(),'Power Forward')]"));
		assertTrue(selenium.isElementPresent("//option[contains(text(),'Center')]"));
		// On vérifie que rien ne s'affiche.
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_searchResult')]/thead/tr[th[contains(text(),\"Last Name\")] and th[contains(text(),\"First Name\")] and th[contains(text(),\"Position\")] and th[contains(text(),\"Height (cm)\")]]"));
//		String url = selenium.getLocation();
//		String count = selenium.getEval("(storedVars['url'].indexOf('Struts') != -1) ? 1 : 0");
		verifyEquals(0, selenium.getXpathCount("//table[contains(@id,'tab_searchResult')]//tbody//tr"));
		verifyTrue(selenium.isElementPresent("//h2[contains(text(),\"Search\")]"));
		selenium.type("//*[contains(@id,'txt_firstname')]", "Loren");
		selenium.type("//*[contains(@id,'txt_lastname')]", "Colon");
		// On fait une recherche sans sélectionner de position.
		selenium.click("//*[contains(@id,'lnk_searchProperties')]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("1", selenium.getXpathCount("//table[contains(@id,'tab_searchResult')]//tbody//tr"));
		// Puis on sélectionne une position, et on refait la recherche.
		selenium.select("//*[contains(@id,'select_position')]", "label=Small Forward");
		selenium.click("//*[contains(@id,'lnk_searchProperties')]");
		selenium.waitForPageToLoad("30000");
		// On vérifie que le joueur s'affiche.
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_searchResult')]/tbody/tr[td/span[contains(text(),\"Colon\")] and td/span[contains(text(),\"Loren\")] and td/span[contains(text(),\"Small Forward\")] and td/span[contains(text(),\"221\")]]"));
		// Avec les champs de recherche de nom vides. On ne selectionne qu'une position.
		// On va vérifier ici les détails des joueurs.
//		selenium.click("link=UC05 - Search");
//		selenium.waitForPageToLoad("30000");
//		verifyTrue(selenium.isElementPresent("//h2[contains(text(),\"Search\")]"));
		// Dans les premiers champs de recherche : [Search with above properties]
		// On recherche tous les joueurs SM.
		selenium.select("//*[contains(@id,'select_position')]", "label=Small Forward");
		selenium.click("//*[contains(@id,'lnk_searchProperties')]");
		selenium.waitForPageToLoad("30000");
		// On vérifie si le bon résultat est affiché.
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_searchResult')]/tbody/tr[td/span[contains(text(),\"Colon\")] and td/span[contains(text(),\"Loren\")] and td/span[contains(text(),\"Small Forward\")] and td/span[contains(text(),\"221\")]]"));
		// On crée un index pour la ligne que l'on vient de vérifier.
		Number index = selenium.getElementIndex("//table[contains(@id,'tab_searchResult')]/tbody/tr[td/span[contains(text(),\"Colon\")] and td/span[contains(text(),\"Loren\")] and td/span[contains(text(),\"Small Forward\")] and td/span[contains(text(),\"221\")]]");
		System.out.println("variable index vaut : " + index);
		// On regarde les détails de ce joueur.
		selenium.click("//table[contains(@id,'tab_searchResult')]/tbody/tr[" + index + " + 1]/td/a[contains(text(),\"[view]\")]");
		selenium.waitForPageToLoad("30000");
		// On vérifie que l'on est sur la bonne page.
		assertTrue(selenium.isElementPresent("//table[@class=\"table-form\"]/tbody/tr[td/span[contains(text(),\"Loren\")] and td/span[contains(text(),\"Colon\")]]"));
		// On retourne sur la page principale.
		selenium.click("//*[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
		// On vérifie que l'on est sur la bonne page.
		assertTrue(selenium.isElementPresent("//form/h2[@class=\"rd_first\"]"));
		// On va tester ici la recherche HQL - champs vides.
		// On fait une recherche les champs vides.
//		selenium.click("link=UC05 - Search");
//		selenium.waitForPageToLoad("30000");
//		verifyTrue(selenium.isElementPresent("//h2[contains(text(),\"Search\")]"));
		selenium.click("//*[contains(@id,'lnk_searchCriteria')]");
		selenium.waitForPageToLoad("30000");
		// On vérifie que les données s'affichent.
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_searchResult')]/tbody/tr"));
		// On récupère la valeur minimum à rechercher
		String min = selenium.getValue("xpath=//input[contains(@id,'txt_minheight')]");
		// On récupère la valeur maximum à rechercher
		String max = selenium.getValue("xpath=//input[contains(@id,'txt_maxheight')]");
		// On récupère le nombre de ligne du tableau et donc des résultats
//		Number nbreLigne = selenium.getXpathCount("//table[contains(@id,'tab_searchResult')]/tbody/tr");
		// Initialisation de la variable d'itération
		String iterateur = "1";
		// On parcourt les lignes du tableau en utilisant l'itérateur
		// selenium.while("${iterateur} <= ${nbreLigne}");
		// Récuparétation de la height de la ligne en cours
		String height = selenium.getText("//table[contains(@id,'tab_searchResult')]/tbody/tr[" + iterateur + "]/td[4]/span");
		// On vérifie que la taille est supérieure ou égale à la valeur minimale
		assertEquals("true", selenium.getEval("new Number(" + min + ") <= new Number(" + height + ")"));
		// On vérifie que la taille est inférieure ou égale à la valeur maximale
		assertEquals("true", selenium.getEval("new Number(" + height + ") <= new Number(" + max + ")"));
		// On incrémente l'itérateur de 1
		iterateur = selenium.getExpression(iterateur + " + 1");
		// Fin de la boucle
		// selenium.endWhile();
	}

	
}
