/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.gettingstarted.uc05_search ;

// Import declaration.
// Java imports.
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.bluage.documentation.business.gettingstarted.uc05_search.bos.Player05BO;
import com.bluage.documentation.service.gettingstarted.uc05_search.ServiceSearch;
import com.netfective.bluage.core.exception.ApplicationException;
import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;
import com.netfective.bluage.core.web.IForm;

/**
 * Class : UC05_ViewController
 */
@Controller
@Scope("prototype")
@SessionAttributes("uC05_ViewForm")
@RequestMapping(value= "/presentation/gettingstarted/uc05_search")
public class UC05_ViewController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC05_ViewController.class);
	
	//Current form name
	private static final String U_C05__VIEW_FORM = "uC05_ViewForm";

	//CONSTANT: currentPlayer
	private static final String CURRENT_PLAYER = "currentPlayer";
		
	/**
	 * Property:customDateEditorsUC05_ViewController
	 */
	final private Map<String,CustomDateEditor> customDateEditorsUC05_ViewController = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:uC05_ViewValidator
	 */
	final private UC05_ViewValidator uC05_ViewValidator = new UC05_ViewValidator();
	
	/**
	 * Service declaration : serviceSearch.
	 */
	@Autowired
	private ServiceSearch serviceSearch;
	
	
	// Initialise all the instances for UC05_ViewController
		// Initialize the instance currentPlayer of type com.bluage.documentation.business.gettingstarted.uc05_search.bos.Player05BO with the value : null for UC05_ViewController
		private Player05BO currentPlayer;
						/**
	 * Operation : lnk_back
 	 * @param model : 
 	 * @param uC05_View : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC05_View/lnk_back.html",method = RequestMethod.GET)
	public String lnk_back(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC05_ViewForm") UC05_ViewForm  uC05_ViewForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : lnk_back"); 
		infoLogger(uC05_ViewForm); 
		uC05_ViewForm.setAlwaysCallPreControllers(false); 
		// initialization for lnk_back
		init();
		
		String destinationPath = null; 
		

	 callBacktoSearchpage(request,uC05_ViewForm );
											// Init the destination form com.bluage.documentation.web.presentation.gettingstarted.uc05_search.UC05_SearchForm or not from lnk_back
			final UC05_SearchForm uC05_SearchForm =  new UC05_SearchForm();
			// Populate the destination form with all the instances returned from lnk_back.
			final IForm uC05_SearchForm2 = populateDestinationForm(request.getSession(), uC05_SearchForm); 
					
			// Add the form to the model.
			model.addAttribute("uC05_SearchForm", uC05_SearchForm2); 
			
			request.getSession().setAttribute("uC05_SearchForm", uC05_SearchForm2);
			
			// "OK" CASE => destination screen path from lnk_back
			LOGGER.info("Go to the screen 'UC05_Search'.");
			// Redirect (PRG) from lnk_back
			destinationPath =  "redirect:/presentation/gettingstarted/uc05_search/UC05_Search.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_back 
	}
	
	
	/**
	* This method initialise the form : UC05_ViewForm 
	* @return UC05_ViewForm
	*/
	@ModelAttribute("UC05_ViewFormInit")
	public void initUC05_ViewForm(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, U_C05__VIEW_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method UC05_ViewForm."); //for lnk_back 
		}
		UC05_ViewForm uC05_ViewForm;
	
		if(request.getSession().getAttribute(U_C05__VIEW_FORM) != null){
			uC05_ViewForm = (UC05_ViewForm)request.getSession().getAttribute(U_C05__VIEW_FORM);
		} else {
			uC05_ViewForm = new UC05_ViewForm();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form UC05_ViewForm.");
		}
		uC05_ViewForm = (UC05_ViewForm)populateDestinationForm(request.getSession(), uC05_ViewForm);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  UC05_ViewForm.");
		}
		model.addAttribute(U_C05__VIEW_FORM, uC05_ViewForm);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param UC05_ViewForm : The sceen form.
	 */
	@RequestMapping(value = "/UC05_View.html" ,method = RequestMethod.GET)
	public void prepareUC05_View(final HttpServletRequest request,final  Model model,final  @ModelAttribute("UC05_ViewFormInit") UC05_ViewForm uC05_ViewForm){
		
		UC05_ViewForm currentUC05_ViewForm = uC05_ViewForm;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(U_C05__VIEW_FORM) == null){
			if(currentUC05_ViewForm!=null){
				request.getSession().setAttribute(U_C05__VIEW_FORM, currentUC05_ViewForm);
			}else {
				currentUC05_ViewForm = new UC05_ViewForm();
				request.getSession().setAttribute(U_C05__VIEW_FORM, currentUC05_ViewForm);	
			}
		} else {
			currentUC05_ViewForm = (UC05_ViewForm) request.getSession().getAttribute(U_C05__VIEW_FORM);
		}

		currentUC05_ViewForm = (UC05_ViewForm)populateDestinationForm(request.getSession(), currentUC05_ViewForm);
		// Call all the Precontroller.
	currentUC05_ViewForm.setAlwaysCallPreControllers(true);
		model.addAttribute(U_C05__VIEW_FORM, currentUC05_ViewForm);
		request.getSession().setAttribute(U_C05__VIEW_FORM, currentUC05_ViewForm);
		
	}
	
	/**
	 * method callBacktoSearchpage
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callBacktoSearchpage(HttpServletRequest request,IForm form) {
					try {
				// executing BacktoSearchpage in lnk_back
	serviceSearch.back(
	);  

								// processing variables BacktoSearchpage in lnk_back

			} catch (ApplicationException e) { 
				// error handling for operation back called BacktoSearchpage
				errorLogger("An error occured during the execution of the operation : back",e); 
		
			}
	}
		
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("UC05_ViewController [ ");
			strBToS.append(CURRENT_PLAYER);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(currentPlayer);
			strBToS.append(END_TO_STRING_DELIMITER);
			// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(uC05_ViewValidator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsUC05_ViewController!=null); 
		return strBToS.toString();
	}
}
