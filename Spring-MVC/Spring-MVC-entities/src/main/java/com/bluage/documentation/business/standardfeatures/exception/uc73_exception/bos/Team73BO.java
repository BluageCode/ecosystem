/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.business.standardfeatures.exception.uc73_exception.bos;

import javax.inject.Inject;
import javax.inject.Named;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import com.bluage.documentation.business.common.bos.TeamBO;
import com.bluage.documentation.business.standardfeatures.exception.uc73_exception.entities.daofinder.Team73DAOFinderImpl;

/**
 * Team73BO
 *
* @see com.netfective.bluage.core.business.IBusinessObject
* * @see TeamBO
 */
@XmlRootElement

public class Team73BO
 extends TeamBO {
	
	/**
	 * Default SUID for Team73BO.
	 */
	private static final long serialVersionUID = 1L;
	

	@Inject
	@Named ("com.bluage.documentation.business.standardfeatures.exception.uc73_exception.entities.daofinder.Team73Finder") 
	private Team73DAOFinderImpl finder;

	
	private boolean playerExiste = false;

   /**
	* public default constructor for class Team73BO
	*/
	public Team73BO(){
		//NOP Team73BO
	}


	
		
	/**
	 * Getter for playerExiste of class Team73BO.
	 * @return {@link boolean}
	 */
	public boolean isPlayerExiste()
	{
		return this.playerExiste;
	}
	
	/**
	 * @deprecated Use isPlayerExiste() instead.
	 */
	public boolean getPlayerExiste()
	{
		return this.playerExiste;
	}
	
	/**
	 * Setter for playerExiste of class Team73BO.
	 * @param value: {@link boolean}
	 */
	public void setPlayerExiste(boolean playerExiste) 
	{
		this.playerExiste = playerExiste;
	}
/**
	 * Returns <code>true</code> if the argument is an Team73 instance and all identifiers for this entity
     * equal the identifiers of the argument entity. The <code>equals</code> method of the parent entity
     * will also need to return <code>true</code>. Returns <code>false</code> otherwise.
     *
     * @see TeamBO#hashCode()
	 * @param object  object to compare with current instance      
     * @return boolean true if equals false if not
     */
    public boolean equals(final Object object )
    {
		if ( this == object )
        {
            return true;
        }
        if ( !( object instanceof Team73BO ) )
        {
            return false;
        }
        final Team73BO that = (Team73BO)object;
    
    	            if (        				        			this.getId() == null
        	        ) {
        	return false;
        }
		return new EqualsBuilder()
			.append(this.getId(), that.getId())
			 	.isEquals();
	}
	/**

	 * Returns a hash code based on this entity's identifiers and the hash code of the parent entity.
     *
     * @see TeamBO#hashCode()
     * @return int
     */    
    public int hashCode() {
        return new HashCodeBuilder()
             .append(getId())
				 	.appendSuper(super.hashCode())
            .toHashCode();
    }
}

