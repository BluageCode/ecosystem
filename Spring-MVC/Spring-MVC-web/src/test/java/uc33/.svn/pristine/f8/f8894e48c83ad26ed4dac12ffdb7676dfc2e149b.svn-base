package uc33;

import org.junit.Test;
import test.common.AbstractBADocTestCase;



public class ITuc33 extends AbstractBADocTestCase {

	@Override
	public  String getName(){
		return "UC-33";
	}

	@Test
	public void testITuc33() throws Exception {
		// On teste l'UC33 - Standard Features/Application/Datagrid - Edition Table
		selenium.open("");
		selenium.click("link=Expand All");
		selenium.waitForPageToLoad("30000");
		selenium.click("link=UC33 - Editable Table");
		verifyTrue(selenium.isElementPresent("//h2[contains(text(),\"Standard Features > Application > DataGrid > UC33 > EdiTable Table\")]"));
		// On vérifie que les données sont affichées.
		assertTrue(selenium.isElementPresent("xpath=//table[contains(@id,'tab_teams')]/thead/tr[th[contains(text(),\"Name\")] and th[contains(text(),\"City\")]]"));
		assertEquals("7", selenium.getXpathCount("//table[contains(@id,'tab_teams')]/tbody/tr"));
		assertTrue(selenium.isElementPresent("xpath=//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Tigers\")] and td/span[contains(text(),\"Charlotte\")]]"));
		assertTrue(selenium.isElementPresent("xpath=//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Bears\")] and td/span[contains(text(),\"Kansas City\")]]"));
		assertTrue(selenium.isElementPresent("xpath=//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Chargers\")] and td/span[contains(text(),\"Detroit\")]]"));
		assertTrue(selenium.isElementPresent("xpath=//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Hawks\")] and td/span[contains(text(),\"Detroit\")]]"));
		assertTrue(selenium.isElementPresent("xpath=//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Braves\")] and td/span[contains(text(),\"Omaha\")]]"));
		assertTrue(selenium.isElementPresent("xpath=//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Eagles\")] and td/span[contains(text(),\"Raleigh\")]]"));
		assertTrue(selenium.isElementPresent("xpath=//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Cardinals\")] and td/span[contains(text(),\"Oakland\")]]"));
		// On vérifie que la ligne sur laquelle on va tester existe. (Braves/Ohama)
		assertTrue(selenium.isElementPresent("xpath=//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Braves\")] and td/span[contains(text(),\"Omaha\")]]"));
		// On indexe cette ligne.
		Number index = selenium.getElementIndex("xpath=//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Braves\")] and td/span[contains(text(),\"Omaha\")]]");
		selenium.click("xpath=//table[contains(@id,'tab_teams')]/tbody/tr[" + index + " + 1]/td[3]/a[contains(text(),\"[edit]\")]");
		selenium.waitForPageToLoad("30000");
		// On laisse les deux champs vides.
		selenium.type("xpath=//table[contains(@id,'tab_teams')]/tbody[1]/tr[" + index + " + 1]/td[1]/input[1]", "");
		selenium.type("xpath=//table[contains(@id,'tab_teams')]/tbody[1]/tr[" + index + " +1]/td[2]/input[1]", "");
		selenium.click("xpath=//table[contains(@id,'tab_teams')]/tbody/tr[" + index + " + 1]/td[3]/a[contains(text(),\"[update]\")]");
		selenium.waitForPageToLoad("30000");
		// On vérifie la présence des messages d'erreur.
		assertTrue(selenium.isElementPresent("xpath=//table[contains(@id,'tab_teams')]/tbody/tr[td//span[contains(text(),'Team name is required.')] and td//span[contains(text(),'City is required.')]]"));
		// On indexe la ligne vide.
		Number index1 = selenium.getElementIndex("xpath=//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"\")] and td/span[contains(text(),\"\")]]");
		selenium.click("xpath=//table[contains(@id,'tab_teams')]/tbody/tr[" + index1 + " + 1]/td[3]/a[contains(text(),\"[edit]\")]");
		selenium.waitForPageToLoad("30000");
		// On rempli les deux champs avec de nouvelles données.
		selenium.type("xpath=//table[contains(@id,'tab_teams')]/tbody[1]/tr[" + index1 + " +1]/td[1]/input[1]", "Charlotte");
		selenium.type("xpath=//table[contains(@id,'tab_teams')]/tbody[1]/tr[" + index1 + " +1]/td[2]/input[1]", "Tigers");
		selenium.click("xpath=//table[contains(@id,'tab_teams')]/tbody/tr[" + index1 + " + 1]/td[3]/a[contains(text(),\"[update]\")]");
		selenium.waitForPageToLoad("30000");
		// On vérifie que nos nouvelles données existent.
		assertTrue(selenium.isElementPresent("xpath=//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Omaha\")] and td/span[contains(text(),\"Braves\")]]"));
		// On recrée un nouvel index pour la ligne.
		Number index3 = selenium.getElementIndex("xpath=//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Braves\")] and td/span[contains(text(),\"Omaha\")]]");
		selenium.click("xpath=//table[contains(@id,'tab_teams')]/tbody/tr[" + index3 + " + 1]/td[3]/a[contains(text(),\"[edit]\")]");
		selenium.waitForPageToLoad("30000");
		// On réecrit les données d'origine.
		selenium.type("xpath=//table[contains(@id,'tab_teams')]/tbody[1]/tr[" + index3 + " +1]/td[1]/input[1]", "Braves");
		selenium.type("xpath=//table[contains(@id,'tab_teams')]/tbody[1]/tr[" + index3 + " +1]/td[2]/input[1]", "Omaha");
		selenium.click("xpath=//table[contains(@id,'tab_teams')]/tbody/tr[" + index3 + " + 1]/td[3]/a[contains(text(),\"[cancel]\")]");
		selenium.waitForPageToLoad("30000");
		// On vérifie que les données d'origine existent.
		assertEquals("7", selenium.getXpathCount("//table[contains(@id,'tab_teams')]/tbody/tr"));
		assertTrue(selenium.isElementPresent("xpath=//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Tigers\")] and td/span[contains(text(),\"Charlotte\")]]"));
		assertTrue(selenium.isElementPresent("xpath=//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Bears\")] and td/span[contains(text(),\"Kansas City\")]]"));
		assertTrue(selenium.isElementPresent("xpath=//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Chargers\")] and td/span[contains(text(),\"Detroit\")]]"));
		assertTrue(selenium.isElementPresent("xpath=//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Hawks\")] and td/span[contains(text(),\"Detroit\")]]"));
		assertTrue(selenium.isElementPresent("xpath=//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Braves\")] and td/span[contains(text(),\"Omaha\")]]"));
		assertTrue(selenium.isElementPresent("xpath=//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Eagles\")] and td/span[contains(text(),\"Raleigh\")]]"));
		assertTrue(selenium.isElementPresent("xpath=//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Cardinals\")] and td/span[contains(text(),\"Oakland\")]]"));
	}


}
