ALTER TABLE player72 DROP CONSTRAINT player72_position_fkc;

ALTER TABLE team72 DROP CONSTRAINT team72_state_fkc;

ALTER TABLE state72 DROP CONSTRAINT state72_pkey;

ALTER TABLE player72 DROP CONSTRAINT player72_pkey;

ALTER TABLE team72 DROP CONSTRAINT team72_pkey;

ALTER TABLE position72 DROP CONSTRAINT position72_pkey;

DROP TABLE state72;

DROP TABLE position72;

DROP TABLE team72;

DROP TABLE player72;


CREATE TABLE position72 (
    code character varying(2) NOT NULL,
    version integer NOT NULL,
    name character varying(20) NOT NULL
);



CREATE TABLE player72 (
    id bigint NOT NULL,
    version integer NOT NULL,
    first_name character varying(30) NOT NULL,
    last_name character varying(30) NOT NULL,
    date_of_birth date,
    rookie boolean,
    position_fk character varying(2) NOT NULL
);


ALTER TABLE ONLY position72
    ADD CONSTRAINT position72_pkey PRIMARY KEY (code);

ALTER TABLE ONLY player72
    ADD CONSTRAINT player72_pkey PRIMARY KEY (id);

ALTER TABLE ONLY player72
    ADD CONSTRAINT player72_position_fkc FOREIGN KEY (position_fk) REFERENCES position72(code);

INSERT INTO position72 VALUES ('PG', 0, 'Point Guard');
INSERT INTO position72 VALUES ('SG', 0, 'Shooting Guard');
INSERT INTO position72 VALUES ('SF', 0, 'Small Forward');
INSERT INTO position72 VALUES ('PF', 0, 'Power Forward');
INSERT INTO position72 VALUES ('C', 0, 'Center');

INSERT INTO player72 VALUES (1, 0, 'Oliver', 'Frank', '1968-10-31', false,'PG');
INSERT INTO player72 VALUES (2, 0, 'Elbert', 'Cole', '1965-04-07', false,'SG');
INSERT INTO player72 VALUES (3, 0, 'Ricky', 'Dawson', '1990-03-03', true,'SF');

CREATE TABLE state72 (
    code character varying(2) NOT NULL,
    version integer NOT NULL,
    name character varying(20) NOT NULL
);

	
CREATE TABLE team72 (
    id bigint NOT NULL,
    version integer NOT NULL,
    city character varying(1024),
    name character varying(1024),
    state_fk character varying(2) NOT NULL
);


ALTER TABLE ONLY state72
    ADD CONSTRAINT state72_pkey PRIMARY KEY (code);
	
ALTER TABLE ONLY team72
    ADD CONSTRAINT team72_pkey PRIMARY KEY (id);
	
ALTER TABLE ONLY team72
    ADD CONSTRAINT team72_state_fkc FOREIGN KEY (state_fk) REFERENCES state72(code);

INSERT INTO state72 (code, version, name) VALUES ('AL', 0, 'Alabama');
INSERT INTO state72 (code, version, name) VALUES ('AK', 0, 'Alaska');
INSERT INTO state72 (code, version, name) VALUES ('AZ', 0, 'Arizona');
INSERT INTO state72 (code, version, name) VALUES ('AR', 0, 'Arkansas');
INSERT INTO state72 (code, version, name) VALUES ('CA', 0, 'California');
INSERT INTO state72 (code, version, name) VALUES ('CO', 0, 'Colorado');
INSERT INTO state72 (code, version, name) VALUES ('CT', 0, 'Connecticut');
INSERT INTO state72 (code, version, name) VALUES ('DE', 0, 'Delaware');
INSERT INTO state72 (code, version, name) VALUES ('FL', 0, 'Florida');
INSERT INTO state72 (code, version, name) VALUES ('GI', 0, 'Gironde');
INSERT INTO state72 (code, version, name) VALUES ('HI', 0, 'Hawaii');
INSERT INTO state72 (code, version, name) VALUES ('ID', 0, 'Idaho');
INSERT INTO state72 (code, version, name) VALUES ('IL', 0, 'Illinois');
INSERT INTO state72 (code, version, name) VALUES ('IN', 0, 'Indiana');
INSERT INTO state72 (code, version, name) VALUES ('IA', 0, 'Iowa');
INSERT INTO state72 (code, version, name) VALUES ('KS', 0, 'Kansas');
INSERT INTO state72 (code, version, name) VALUES ('KY', 0, 'Kentucky');
INSERT INTO state72 (code, version, name) VALUES ('LA', 0, 'Louisiana');
INSERT INTO state72 (code, version, name) VALUES ('ME', 0, 'Maine');
INSERT INTO state72 (code, version, name) VALUES ('MD', 0, 'Maryland');
INSERT INTO state72 (code, version, name) VALUES ('MA', 0, 'Massachusetts');
INSERT INTO state72 (code, version, name) VALUES ('MI', 0, 'Michigan');
INSERT INTO state72 (code, version, name) VALUES ('MN', 0, 'Minnesota');
INSERT INTO state72 (code, version, name) VALUES ('MS', 0, 'Mississippi');
INSERT INTO state72 (code, version, name) VALUES ('MO', 0, 'Missouri');
INSERT INTO state72 (code, version, name) VALUES ('MT', 0, 'Montana');
INSERT INTO state72 (code, version, name) VALUES ('NE', 0, 'Nebraska');
INSERT INTO state72 (code, version, name) VALUES ('NV', 0, 'Nevada');
INSERT INTO state72 (code, version, name) VALUES ('NH', 0, 'New Hampshire');
INSERT INTO state72 (code, version, name) VALUES ('NJ', 0, 'New Jersey');
INSERT INTO state72 (code, version, name) VALUES ('NM', 0, 'New Mexico');
INSERT INTO state72 (code, version, name) VALUES ('NY', 0, 'New York');
INSERT INTO state72 (code, version, name) VALUES ('NC', 0, 'North Carolina');
INSERT INTO state72 (code, version, name) VALUES ('ND', 0, 'North Dakota');
INSERT INTO state72 (code, version, name) VALUES ('OH', 0, 'Ohio');
INSERT INTO state72 (code, version, name) VALUES ('OK', 0, 'Oklahoma');
INSERT INTO state72 (code, version, name) VALUES ('OR', 0, 'Oregon');
INSERT INTO state72 (code, version, name) VALUES ('PA', 0, 'Pennsylvania');
INSERT INTO state72 (code, version, name) VALUES ('RI', 0, 'Rhode Island');
INSERT INTO state72 (code, version, name) VALUES ('SC', 0, 'South Carolina');
INSERT INTO state72 (code, version, name) VALUES ('SD', 0, 'South Dakota');
INSERT INTO state72 (code, version, name) VALUES ('TN', 0, 'Tennessee');
INSERT INTO state72 (code, version, name) VALUES ('TX', 0, 'Texas');
INSERT INTO state72 (code, version, name) VALUES ('UT', 0, 'Utah');
INSERT INTO state72 (code, version, name) VALUES ('VT', 0, 'Vermont');
INSERT INTO state72 (code, version, name) VALUES ('VA', 0, 'Virginia');
INSERT INTO state72 (code, version, name) VALUES ('WA', 0, 'Washington');
INSERT INTO state72 (code, version, name) VALUES ('WV', 0, 'West Virginia');
INSERT INTO state72 (code, version, name) VALUES ('WI', 0, 'Wisconsin');
INSERT INTO state72 (code, version, name) VALUES ('WY', 0, 'Wyoming');

INSERT INTO team72 VALUES (296, 0, 'Chicago', 'Bulls', 'IL');
INSERT INTO team72 VALUES (297, 0, 'Bordeaux', 'Girondins', 'GI');
INSERT INTO team72 VALUES (301, 0, 'Cleveland', 'Cavaliers', 'OH');
INSERT INTO team72 VALUES (303, 0, 'Boston', 'Celtics', 'MA');
INSERT INTO team72 VALUES (305, 0, 'Anchorage', 'Furious Penguins', 'AK');
INSERT INTO team72 VALUES (306, 0, 'Juneau', 'Wild Meese', 'AK');
	
