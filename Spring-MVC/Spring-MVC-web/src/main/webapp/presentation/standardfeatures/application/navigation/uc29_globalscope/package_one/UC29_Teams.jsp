<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file="/include.jsp"  %>
<html >
<head >
<title  > 
<fmt:message key="cUC29_Teams_BLU_AGE_DOCUMENTATION"/>
</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"  > 
</meta>
<link href="<c:url value= "/presentation/_style/_style.css"/>" rel="stylesheet" type="text/css"  /> 
<link href="<c:url value= "/presentation/_include/jquery/rd_lib.css"/>" rel="stylesheet" type="text/css"  /> 
<script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/jquery.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/rd_setup.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/rd_lib.js"/>" space="preserve"  ></script><link rel="shortcut icon" href="<c:url value= "/presentation/_style/favicon.ico"/>"  /> 

</head>

<body class=" " > 
<div id="rd_ghost"  > 
<small  > 
<fmt:message key="cUC29_Teams___CSS_DEACTIVATED"/>
</small>
</div>
<div id="rd_jghost"  > 
<small  > 
<fmt:message key="cUC29_Teams___JAVASCRIPT_DEACTIVATED"/>
</small>
</div>
<div id="shape"  > 
<div class="ui-layout-center"  > 
<div class="east-north"  > 
	<form:form id="uC29_Teams" commandName="uC29_TeamsForm"  enctype="application/x-www-form-urlencoded" method="post">
<h2 class="rd_first"  > 
<fmt:message key="cUC29_Teams_STANDARD_FEATURES__APPLICATIO"/>
</h2>
<br clear="none"  /> 
<br clear="none"  /> 
<table class="rd_tableform2"  >
<tr  > 
<td rowspan="1" colspan="1"  >
<b  > 
<a id="lnk_teams_global"onclick="javascript:btn_click_cExecuteUC29_Teams(this.href);return false;"				href="<c:url value="//presentation/standardfeatures/application/navigation/uc29_globalscope/package_one/UC29_Onglets_1/lnk_teams_global.html"/>"
							 shape="rect"  >									 
<u  > 
<fmt:message key="cUC29_Teams_TEAMS"/>
</u>
</a>
</b>
</td> 
<td rowspan="1" colspan="1"  >
<b  > 
<a id="lnk_players_global"onclick="javascript:btn_click_cExecuteUC29_Teams(this.href);return false;"				href="<c:url value="//presentation/standardfeatures/application/navigation/uc29_globalscope/package_one/UC29_Onglets_1/lnk_players_global.html"/>"
							 shape="rect"  >									 
<fmt:message key="cUC29_Teams_PLAYERS"/>
</a>
</b>
</td> 
<td rowspan="1" colspan="1"  >
<b  > 
<a id="lnk_states_global"onclick="javascript:btn_click_cExecuteUC29_Teams(this.href);return false;"				href="<c:url value="//presentation/standardfeatures/application/navigation/uc29_globalscope/package_two/UC29_Onglets_2/lnk_states_global.html"/>"
							 shape="rect"  >									 
<fmt:message key="cUC29_Teams_STATES"/>
</a>
</b>
</td> 
<td rowspan="1" colspan="1"  >
<b  > 
<a id="lnk_stadiums_global"onclick="javascript:btn_click_cExecuteUC29_Teams(this.href);return false;"				href="<c:url value="//presentation/standardfeatures/application/navigation/uc29_globalscope/package_two/UC29_Onglets_2/lnk_stadiums_global.html"/>"
							 shape="rect"  >									 
<fmt:message key="cUC29_Teams_STADIUMS"/>
</a>
</b>
</td> 
</tr>
</table> 
<display:table uid="tab_teams"  sort="list" requestURI="" name="sessionScope.uC29_TeamsForm.allTeams" class="rd_tabledata"  >
	<c:set   var="rownum">
		<%=(tab_teams !=	null) ? new Integer(tab_teams_rowNum.intValue() - 1) : new Integer(0) %>
	</c:set>
	<c:set   var="idRow">
		<%=(tab_teams != null ) ? tab_teams_rowNum.intValue() - 1 : -1 %>
	</c:set>
	<c:set   var="idTab">
		<%=(tab_teams != null ) ? "tab_teams" : null %>
	</c:set>
	<c:set var="tableHashId">
		<%= new org.displaytag.util.ParamEncoder("tab_teams").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE) %>
	</c:set>
	<c:set var="tableHashId2">
		<%=request.getParameter(new org.displaytag.util.ParamEncoder("tab_teams").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE)) %>
	</c:set>	
	
<display:column format=""  titleKey="cUC29_Teams_NAME">
		<span id="dyn_name">
	<c:out value="${tab_teams.name}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="cUC29_Teams_CITY">
		<span id="dyn_city">
	<c:out value="${tab_teams.city}" />
		</span>
	</display:column>
</display:table>
	</form:form>
	<script language="javascript">
		function btn_click_cExecuteUC29_Teams(url) {
			UC29_Teamsvar = document.getElementById('uC29_Teams');
			UC29_Teamsvar.setAttribute('action', url);
			UC29_Teamsvar.submit();
		}
		function btn_click_cSetActionUC29_Teams(url) {
			UC29_Teamsvar = document.getElementById('uC29_Teams');
			UC29_Teamsvar.setAttribute('action', url);
		}
	</script>
</div>
<div class="east-center"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/application/navigation/uc29_globalscope/package_one/UC29_Teamscmp_tabs" />
</div>
</div>
<div class="ui-layout-north"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/application/navigation/uc29_globalscope/package_one/UC29_Teamscmp_header" />
</div>
<div class="ui-layout-south"  > 
<fmt:message key="cUC29_Teams___"/>
<span id="rd_jversion"  > 
<fmt:message key="cUC29_Teams_BLU_AGE_DOCUMENTATION_120"/>
</span>
</div>
<div class="ui-layout-west"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/application/navigation/uc29_globalscope/package_one/UC29_Teamscmp_treeview" />
</div>
</div>
</body>
</html>
