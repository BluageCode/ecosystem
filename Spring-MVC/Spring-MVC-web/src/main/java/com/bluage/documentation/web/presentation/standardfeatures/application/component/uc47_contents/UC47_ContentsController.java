/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.application.component.uc47_contents ;

// Import declaration.
// Java imports.
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.bluage.documentation.business.standardfeatures.application.component.uc47_contents.bos.Player47BO;
import com.bluage.documentation.business.standardfeatures.application.component.uc47_contents.bos.Screen47BO;
import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;

/**
 * Class : UC47_ContentsController
 */
@Controller
@Scope("prototype")
@SessionAttributes("uC47_ContentsForm")
@RequestMapping(value= "/presentation/standardfeatures/application/component/uc47_contents")
public class UC47_ContentsController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC47_ContentsController.class);
	
	//Current form name
	private static final String U_C47__CONTENTS_FORM = "uC47_ContentsForm";

	//CONSTANT: displayTable
	private static final String DISPLAY_TABLE = "displayTable";
		//CONSTANT: playerToUpdate
	private static final String PLAYER_TO_UPDATE = "playerToUpdate";
		//CONSTANT: listTeam table or repeater.
	private static final String LIST_TEAM = "listTeam";
				
	/**
	 * Property:customDateEditorsUC47_ContentsController
	 */
	final private Map<String,CustomDateEditor> customDateEditorsUC47_ContentsController = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:uC47_ContentsValidator
	 */
	final private UC47_ContentsValidator uC47_ContentsValidator = new UC47_ContentsValidator();
	
	/**
	 * Pre Controller declaration : uC47_PreControllerFindAllPlayers
	 */
	@Autowired
	private UC47_PreControllerFindAllPlayersController uC47_PreControllerFindAllPlayers;

	
	// Initialise all the instances for UC47_ContentsController
		// Initialize the instance displayTable of type com.bluage.documentation.business.standardfeatures.application.component.uc47_contents.bos.Screen47BO with the value : null for UC47_ContentsController
		private Screen47BO displayTable;
			// Initialize the instance playerToUpdate of type com.bluage.documentation.business.standardfeatures.application.component.uc47_contents.bos.Player47BO with the value : null for UC47_ContentsController
		private Player47BO playerToUpdate;
			// Initialize the instance listTeam for the state lnk_goToPage2
		private List listTeam; // Initialize the instance listTeam for UC47_ContentsController
				
	/**
	* This method initialise the form : UC47_ContentsForm 
	* @return UC47_ContentsForm
	*/
	@ModelAttribute("UC47_ContentsFormInit")
	public void initUC47_ContentsForm(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, U_C47__CONTENTS_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method UC47_ContentsForm."); //for lnk_goToPage2 
		}
		UC47_ContentsForm uC47_ContentsForm;
	
		if(request.getSession().getAttribute(U_C47__CONTENTS_FORM) != null){
			uC47_ContentsForm = (UC47_ContentsForm)request.getSession().getAttribute(U_C47__CONTENTS_FORM);
		} else {
			uC47_ContentsForm = new UC47_ContentsForm();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form UC47_ContentsForm.");
		}
		uC47_ContentsForm = (UC47_ContentsForm)populateDestinationForm(request.getSession(), uC47_ContentsForm);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  UC47_ContentsForm.");
		}
		model.addAttribute(U_C47__CONTENTS_FORM, uC47_ContentsForm);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param UC47_ContentsForm : The sceen form.
	 */
	@RequestMapping(value = "/UC47_Contents.html" ,method = RequestMethod.GET)
	public void prepareUC47_Contents(final HttpServletRequest request,final  Model model,final  @ModelAttribute("UC47_ContentsFormInit") UC47_ContentsForm uC47_ContentsForm){
		
		UC47_ContentsForm currentUC47_ContentsForm = uC47_ContentsForm;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(U_C47__CONTENTS_FORM) == null){
			if(currentUC47_ContentsForm!=null){
				request.getSession().setAttribute(U_C47__CONTENTS_FORM, currentUC47_ContentsForm);
			}else {
				currentUC47_ContentsForm = new UC47_ContentsForm();
				request.getSession().setAttribute(U_C47__CONTENTS_FORM, currentUC47_ContentsForm);	
			}
		} else {
			currentUC47_ContentsForm = (UC47_ContentsForm) request.getSession().getAttribute(U_C47__CONTENTS_FORM);
		}

		currentUC47_ContentsForm = (UC47_ContentsForm)populateDestinationForm(request.getSession(), currentUC47_ContentsForm);
		// Call all the Precontroller.
	if ( currentUC47_ContentsForm.isAlwaysCallPreControllers()) {
	
		// Calling the PreController : UC47_PreControllerFindAllPlayers.
		currentUC47_ContentsForm = (UC47_ContentsForm) uC47_PreControllerFindAllPlayers.uC47_PreControllerFindAllPlayersControllerInit(request, model, currentUC47_ContentsForm);
		
	}
	currentUC47_ContentsForm.setAlwaysCallPreControllers(true);
		model.addAttribute(U_C47__CONTENTS_FORM, currentUC47_ContentsForm);
		request.getSession().setAttribute(U_C47__CONTENTS_FORM, currentUC47_ContentsForm);
		
	}
	
	
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("UC47_ContentsController [ ");
			strBToS.append(DISPLAY_TABLE);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(displayTable);
			strBToS.append(END_TO_STRING_DELIMITER);
				strBToS.append(PLAYER_TO_UPDATE);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(playerToUpdate);
			strBToS.append(END_TO_STRING_DELIMITER);
				strBToS.append(LIST_TEAM);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(listTeam);
			strBToS.append(END_TO_STRING_DELIMITER);
					// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(uC47_ContentsValidator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsUC47_ContentsController!=null); 
		return strBToS.toString();
	}
}
