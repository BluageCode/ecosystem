/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.entities.uc68_heritagesubclass ;


import org.apache.log4j.Logger;
import org.springframework.validation.Errors;

import com.netfective.bluage.core.validator.AbstractValidator;
/**
* Class :UC68_HeritageSubclassValidator
*/
public class UC68_HeritageSubclassValidator extends AbstractValidator{
	
	// LOGGER for the class UC68_HeritageSubclassValidator
	private static final Logger LOGGER = Logger.getLogger( UC68_HeritageSubclassValidator.class);
	
	
	/**
	* Operation validate for UC68_HeritageSubclassForm
	* @param obj : the current form (UC68_HeritageSubclassForm)
	* @param errors : The spring errors to return for the form UC68_HeritageSubclassForm
	*/
	public void validate(final Object obj,final Errors errors){
		LOGGER.info("Beginning method : validate"); // UC68_HeritageSubclassValidator
		UC68_HeritageSubclassForm cUC68_HeritageSubclassForm = (UC68_HeritageSubclassForm)obj; // UC68_HeritageSubclassValidator
		LOGGER.info("Ending method : validate the form "+ cUC68_HeritageSubclassForm.getClass().getName()); // UC68_HeritageSubclassValidator
	}

	/**
	* Method to implements to use spring validators (UC68_HeritageSubclassForm)
	* @param aClass : Class for the form UC68_HeritageSubclassForm
	* @return boolean
	*/
	public boolean supports(final Class aClass){
		return (new UC68_HeritageSubclassForm()).getClass().equals(aClass); // UC68_HeritageSubclassValidator
	}
}
