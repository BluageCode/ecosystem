/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.web.presentation.standardfeatures.application.navigation.uc29_globalscope.package_one;

// Import declaration.
// Java imports.
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import com.bluage.documentation.service.standardfeatures.application.navigation.uc29_globalscope.ServiceTeam29;
import com.netfective.bluage.core.exception.ApplicationException;
import com.netfective.bluage.core.web.AbstractController;
import com.netfective.bluage.core.web.IForm;

/**
 * Class : UC29_PreControllerFindAllTeamsController
 */
 @Service("com.bluage.documentation.web.presentation.standardfeatures.application.navigation.uc29_globalscope.package_one.UC29_PreControllerFindAllTeamsController")
public class UC29_PreControllerFindAllTeamsController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC29_PreControllerFindAllTeamsController.class);
	
	//CONSTANT PINS: allTeams
	private static final String ALL_TEAMS = "allTeams";
	
	// Declaring all the instances.
		List allTeams;
	/**
	 * Service declaration : serviceTeam29.
	 */
	@Autowired
	private ServiceTeam29 serviceTeam29;
	
	/**
	 * Operation : UC29_PreControllerFindAllTeamsControllerInit
	 * @param request : The current HttpRequest
 	 * @param model :  The current Model
 	 * @param currentIForm : The current IForm
 	 * @return
	 */
	public IForm uC29_PreControllerFindAllTeamsControllerInit(final HttpServletRequest request,final  Model model,final  IForm  currentIForm){

		LOGGER.info("Begin the precontroller method : findTeams");
		LOGGER.info("Form diagnostic : " + currentIForm);
		

	callFindAllTeams(request,currentIForm);
		
			LOGGER.info("Populate the destination screen.");
			// Populate the destination form with all the instances returned from an executed service.
			return populateDestinationForm(request.getSession(), currentIForm); // Populate the destination screen

	}
	/**
	 * method callFindAllTeams
	 * 
	 * @param request HttpServletRequest
	 * @param currentIForm IForm
	 */
	private void callFindAllTeams(HttpServletRequest request, IForm currentIForm) {
		// Initialize all the instances.
		allTeams = (List)get(request.getSession(),currentIForm,ALL_TEAMS);
							try {

				allTeams = 	serviceTeam29.team29FindAll(
	);  

				put(request.getSession(), ALL_TEAMS,allTeams);
							
			} catch (ApplicationException e) {
				LOGGER.error("An error occured during the execution of the operation : team29FindAll",e);
			}
	}
}
