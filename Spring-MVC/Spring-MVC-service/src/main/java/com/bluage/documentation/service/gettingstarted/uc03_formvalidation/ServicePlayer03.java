/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.service.gettingstarted.uc03_formvalidation;


import javax.inject.Inject;
import javax.inject.Named;

import com.bluage.documentation.business.gettingstarted.uc03_formvalidation.bos.Player03BO;
import com.bluage.documentation.business.gettingstarted.uc03_formvalidation.bos.Position03BO;
import com.bluage.documentation.business.gettingstarted.uc03_formvalidation.entities.dao.Player03DAOImpl;
import com.bluage.documentation.service.AbstractService;
import com.netfective.bluage.core.exception.ApplicationException;

/**
 * Defines  application services for ServicePlayer03
  */
@Named("com.bluage.documentation.service.gettingstarted.uc03_formvalidation.ServicePlayer03")
public class ServicePlayer03  extends AbstractService{


								@Inject
		@Named ("com.bluage.documentation.service.gettingstarted.uc03_formvalidation.ServicePosition03")
		private ServicePosition03  instanceServicePosition03;
								@Inject
		@Named ("com.bluage.documentation.business.gettingstarted.uc03_formvalidation.entities.dao.Player03DAOImpl")
		private Player03DAOImpl player03BODao;
	


	/**
	 *  Default constructor 
	 */
	public ServicePlayer03() {
		super(); // Call super for default constructor : ServicePlayer03
	}
	


			
				
	/**    
	 * Operation Crud
	 * @param bo {@link Player03BO}
		 * @return {@link boolean}
	 */
	public boolean player03Create(Player03BO bo )
 throws ApplicationException {

					return player03BODao.create(bo);
	}

		
			
	/**    
	 * Process operation.
	 * @return {@link void}
	 */
	public void back( )
 throws ApplicationException {

			privateBack();
	}
	

	/**    
	 * Process operation.
	 * @return {@link void}
	 */
	private void privateBack( )
 throws ApplicationException {
// Central buffer node declaration for operation back
			
		}

		
			
	/**    
	 * Process operation.
	 * @param playerToCreate {@link Player03BO}
		 * @return {@link void}
	 */
	public void createPlayer03(Player03BO playerToCreate )
 throws ApplicationException {

			privateCreatePlayer03(playerToCreate);
	}
	

	/**    
	 * Process operation.
	 * @param playerToCreate {@link Player03BO}
		 * @return {@link void}
	 */
	private void privateCreatePlayer03(Player03BO playerToCreate )
 throws ApplicationException {
		// Central buffer node declaration for operation createPlayer03
			Position03BO	 posResult =  new Position03BO();
					Position03BO	 position =  new Position03BO();
						// Calling an operation : 
position = playerToCreate.getPosition();
// Calling an operation : 
posResult = instanceServicePosition03.position03FindByID(position);
// Calling an operation : 
playerToCreate.setPosition(posResult);
// Calling an operation : 
player03Create(playerToCreate);

		}

		}
