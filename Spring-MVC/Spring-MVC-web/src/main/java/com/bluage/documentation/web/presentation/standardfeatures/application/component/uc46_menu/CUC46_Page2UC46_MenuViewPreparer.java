/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.web.presentation.standardfeatures.application.component.uc46_menu;

import org.apache.log4j.Logger;
import org.apache.tiles.AttributeContext;
import org.apache.tiles.context.TilesRequestContext;
import org.apache.tiles.preparer.ViewPreparer;

import com.netfective.bluage.core.common.IStringConstants;
/**
 * Class : CUC46_Page2UC46_MenuViewPreparer
 * Prepare the component UC46_Menu for the screen UC46_Page2.
 * 
 */
public class CUC46_Page2UC46_MenuViewPreparer implements ViewPreparer , IStringConstants{

	private static final String UC46_PAGE2UC46_MENU = "UC46_MenuComponentBean";
	
	private static final String COMPONENT_FORM_NAME = "com.bluage.documentation.web.presentation.standardfeatures.application.component.uc46_menu.UC46_MenuForm";
	
	private static final String COMPONENT_SHORT_FORM_NAME = "uC46_MenuForm";

	/**
	 * Property:LOGGER
	 */
	private static final Logger LOGGER = Logger.getLogger( CUC46_Page2UC46_MenuViewPreparer.class);

	/**
	 * Method execute for CUC46_Page2UC46_MenuViewPreparer
	 * @param tilesContext : The tiles context of the component  CUC46_Page2UC46_MenuViewPreparer
	 * @param attributeContext : The attribute context of the component  CUC46_Page2UC46_MenuViewPreparer
	 * @see org.apache.tiles.preparer.ViewPreparer#execute(org.apache.tiles.context.TilesRequestContext, org.apache.tiles.AttributeContext)
	 */
	public void execute(TilesRequestContext tilesContext, AttributeContext attributeContext) {
		
		LOGGER.info("Starting the component initialisation for CUC46_Page2UC46_MenuViewPreparer "+ UC46_PAGE2UC46_MENU +" Component Form name : "+COMPONENT_FORM_NAME);
		

		// Init the component UC46_MenuForm
		final UC46_MenuForm componentScreen = new UC46_MenuForm(); // For the form UC46_MenuForm
		
		tilesContext.getSessionScope().put(COMPONENT_SHORT_FORM_NAME, componentScreen); // For the component CUC46_Page2UC46_MenuViewPreparer
		
		LOGGER.info("The component is initialized (CUC46_Page2UC46_MenuViewPreparer).");
	}
}
