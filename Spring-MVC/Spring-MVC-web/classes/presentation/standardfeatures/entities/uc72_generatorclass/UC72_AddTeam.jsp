<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file="/include.jsp"  %>
<html >
<head >
<title  > 
<fmt:message key="cUC72_AddTeam_BLU_AGE_DOCUMENTATION"/>
</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"  > 
</meta>
<link href="<c:url value= "/presentation/_style/_style.css"/>" rel="stylesheet" type="text/css"  /> 
<link href="<c:url value= "/presentation/_include/jquery/rd_lib.css"/>" rel="stylesheet" type="text/css"  /> 
<script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/jquery.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/rd_setup.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/rd_lib.js"/>" space="preserve"  ></script><link rel="shortcut icon" href="<c:url value= "/presentation/_style/favicon.ico"/>"  /> 

</head>

<body class=" " > 
<div id="rd_ghost"  > 
<small  > 
<fmt:message key="cUC72_AddTeam___CSS_DEACTIVATED"/>
</small>
</div>
<div id="rd_jghost"  > 
<small  > 
<fmt:message key="cUC72_AddTeam___JAVASCRIPT_DEACTIVATED"/>
</small>
</div>
<div id="shape"  > 
<div class="ui-layout-center"  > 
<div class="east-north"  > 
	<form:form id="uC72_AddTeam" commandName="uC72_AddTeamForm"  enctype="application/x-www-form-urlencoded" method="post">
<h2 class="rd_first"  > 
<fmt:message key="cUC72_AddTeam_STANDARD_FEATURES__ENTITIES_"/>
</h2>
<h4 class="rd_first"  > 
<fmt:message key="cUC72_AddTeam_ADD_TEAM"/>
</h4>
<table class="rd_tableform"  >
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC72_AddTeam_CITY_"/>
</label>
<label style="background: none;color: red;"  > 
<fmt:message key="cUC72_AddTeam_"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
<form:input id="txt_city" path="teamToCreate.city"  
		/>
</td> 
<td rowspan="1" colspan="1"  >
<form:errors path="teamToCreate.city"/>
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC72_AddTeam_NAME_"/>
</label>
<label style="background: none;color: red;"  > 
<fmt:message key="cUC72_AddTeam__189"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
<form:input id="txt_name" path="teamToCreate.name"  
		/>
</td> 
<td rowspan="1" colspan="1"  >
<form:errors path="teamToCreate.name"/>
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC72_AddTeam_STATE"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
<form:select id="select_state" path="teamToCreate.state.code"   
		>
		<form:options items="${uC72_AddTeamForm.allStates}" itemValue="code" itemLabel="name" />
</form:select>
</td> 
<td rowspan="1" colspan="1"  >
<fmt:message key="cUC72_AddTeam__190"/>
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<fmt:message key="cUC72_AddTeam__191"/>
</td> 
<td rowspan="1" colspan="1"  >
<a id="lnk_create"onclick="javascript:btn_click_cExecuteUC72_AddTeam(this.href);return false;"				href="<c:url value="/presentation/standardfeatures/entities/uc72_generatorclass/UC72_AddTeam/lnk_create.html"/>"
							 shape="rect"  >									 
<fmt:message key="cUC72_AddTeam_CREATE_A_NEW_TEAM"/>
</a>
</td> 
<td rowspan="1" colspan="1"  >
<fmt:message key="cUC72_AddTeam__192"/>
</td> 
</tr>
</table> 
<label style="background: none;color: red;"  > 
<fmt:message key="cUC72_AddTeam__193"/>
</label>
<fmt:message key="cUC72_AddTeam_REQUIRED_ENTRY_"/>
<br clear="none"  /> 
<br clear="none"  /> 
<display:table uid="table_teams"  sort="list" requestURI="" name="sessionScope.uC72_AddTeamForm.listTeams" class="rd_tabledata"  >
	<c:set   var="rownum">
		<%=(table_teams !=	null) ? new Integer(table_teams_rowNum.intValue() - 1) : new Integer(0) %>
	</c:set>
	<c:set   var="idRow">
		<%=(table_teams != null ) ? table_teams_rowNum.intValue() - 1 : -1 %>
	</c:set>
	<c:set   var="idTab">
		<%=(table_teams != null ) ? "table_teams" : null %>
	</c:set>
	<c:set var="tableHashId">
		<%= new org.displaytag.util.ParamEncoder("table_teams").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE) %>
	</c:set>
	<c:set var="tableHashId2">
		<%=request.getParameter(new org.displaytag.util.ParamEncoder("table_teams").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE)) %>
	</c:set>	
	
<display:column format=""  sortable="true" titleKey="cUC72_AddTeam_CITY">
		<span id="dyn_city">
	<c:out value="${table_teams.city}" />
		</span>
	</display:column>
	
<display:column format=""  sortable="true" titleKey="cUC72_AddTeam_NAME">
		<span id="dyn_name">
	<c:out value="${table_teams.name}" />
		</span>
	</display:column>
	
<display:column format=""  sortable="true" titleKey="cUC72_AddTeam_STATE_194">
		<span id="dyn_statename">
	<c:out value="${table_teams.state.name}" />
		</span>
	<fmt:message key="cUC72_AddTeam__195"/>
		<span id="dyn_statecode">
	<c:out value="${table_teams.state.code}" />
		</span>
	<fmt:message key="cUC72_AddTeam____"/>
</display:column>
	
<display:column format=""  sortable="true" titleKey="cUC72_AddTeam_DELETE">
<a id="lnk_delete[${rownum}]"onclick="javascript:btn_click_cExecuteUC72_AddTeam(this.href);return false;"			href="<c:url value="/presentation/standardfeatures/entities/uc72_generatorclass/UC72_AddTeam/lnk_delete.html?id="/><c:out value="${idRow}" />&tab=<c:out value="table_teams" />&<c:out value="${tableHashId}"/>=<c:out value="${tableHashId2}"/>"
	 shape="rect"  >									 
<fmt:message key="cUC72_AddTeam_DELETE_196"/>
</a>
</display:column>
</display:table>
<br clear="none"  /> 
<a id="lnk_back"				href="<c:url value="/presentation/standardfeatures/entities/uc72_generatorclass/UC72_Home.html"/>"
													 shape="rect"  >									 
<fmt:message key="cUC72_AddTeam_BACK"/>
</a>
	</form:form>
	<script language="javascript">
		function btn_click_cExecuteUC72_AddTeam(url) {
			UC72_AddTeamvar = document.getElementById('uC72_AddTeam');
			UC72_AddTeamvar.setAttribute('action', url);
			UC72_AddTeamvar.submit();
		}
		function btn_click_cSetActionUC72_AddTeam(url) {
			UC72_AddTeamvar = document.getElementById('uC72_AddTeam');
			UC72_AddTeamvar.setAttribute('action', url);
		}
	</script>
</div>
<div class="east-center"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/entities/uc72_generatorclass/UC72_AddTeamcmp_tabs" />
</div>
</div>
<div class="ui-layout-north"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/entities/uc72_generatorclass/UC72_AddTeamcmp_header" />
</div>
<div class="ui-layout-south"  > 
<fmt:message key="cUC72_AddTeam___"/>
<span id="rd_jversion"  > 
<fmt:message key="cUC72_AddTeam_BLU_AGE_CORPORATION"/>
</span>
</div>
<div class="ui-layout-west"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/entities/uc72_generatorclass/UC72_AddTeamcmp_treeview" />
</div>
</div>
</body>
</html>
