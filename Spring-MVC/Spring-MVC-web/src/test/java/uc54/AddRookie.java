package uc54;

import org.junit.Test;
import test.common.AbstractBADocTestCase;

public class AddRookie extends AbstractBADocTestCase {
	
	@Override
	public  String getName(){
		return "UC-54";
	}	

	@Test
	public void AddRookie() throws Exception {
		// On teste l'UC54 - Standard Features/Services/Process - For
		selenium.open("");	
		selenium.click("link=Expand All");
		selenium.waitForPageToLoad("30000");
		selenium.click("link=UC54 - For");
		verifyTrue(selenium.isElementPresent("//h2[contains(text(),\"Standard Features > Services > Process > UC54 > For\")]"));
		selenium.click("//*[contains(@id,'lnk_showPlayers')]");
		selenium.waitForPageToLoad("30000");
		// On indexe la ligne de Roger Waters.
		Number index = selenium.getElementIndex("//table[contains(@id,'tab_PlayersList')]/tbody/tr[td/span[contains(text(),\"Roger\")] and td/span[contains(text(),\"Waters\")]]");
		selenium.click("//table[contains(@id,'tab_Players')]/tbody/tr[" + index + " + 1]/td[5]/input");
		selenium.click("//*[contains(@id,'lnk_selectPlayers')]");
		selenium.waitForPageToLoad("30000");
		// On vérifie que le joueur apparait bien dans le tableau tab_TeamPlayersList.
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_TeamPlayersList')]/tbody/tr[td/span[contains(text(),\"Roger\")] and td/span[contains(text(),\"Waters\")] and td/span[contains(text(),\"32.0\")] and td//input[@checked]]"));
	}


}
