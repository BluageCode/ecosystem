/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.service.standardfeatures.entities.uc64_cascade;


import javax.inject.Inject;
import javax.inject.Named;

import com.bluage.documentation.business.standardfeatures.entities.uc64_cascade.bo.State64BO;
import com.bluage.documentation.business.standardfeatures.entities.uc64_cascade.entities.dao.State64DAOImpl;
import com.bluage.documentation.service.AbstractService;
import com.bluage.documentation.service.standardfeatures.entities.uc64_cascade.daofinder.ServiceState64DAOMethodFinderImpl;
import com.netfective.bluage.core.exception.ApplicationException;

/**
 * Defines  application services for ServiceState64
  */
@Named("com.bluage.documentation.service.standardfeatures.entities.uc64_cascade.ServiceState64")
public class ServiceState64  extends AbstractService{


						@Inject
		@Named ("com.bluage.documentation.service.standardfeatures.entities.uc64_cascade.daofinder.ServiceState64DAOMethodFinder")
		private ServiceState64DAOMethodFinderImpl serviceState64Dao;
		@Inject
		@Named ("com.bluage.documentation.business.standardfeatures.entities.uc64_cascade.entities.dao.State64DAOImpl")
		private State64DAOImpl state64BODao;
	


	/**
	 *  Default constructor 
	 */
	public ServiceState64() {
		super(); // Call super for default constructor : ServiceState64
	}
	


			
				
	/**    
	 * Operation Crud
	 * @param bo {@link State64BO}
		 * @return {@link boolean}
	 */
	public boolean stateDelete(State64BO bo )
 throws ApplicationException {

					return state64BODao.delete(bo);
	}

		
			
	/**    
	 * Process operation.
	 * @param state64ToDelete {@link State64BO}
		 * @return {@link boolean}
	 */
	public boolean stateWithoutTeamsDelete(State64BO state64ToDelete )
 throws ApplicationException {

				return privateStateWithoutTeamsDelete(state64ToDelete);
	}
	

	/**    
	 * Process operation.
	 * @param state64ToDelete {@link State64BO}
		 * @return {@link boolean}
	 */
	private boolean privateStateWithoutTeamsDelete(State64BO state64ToDelete )
 throws ApplicationException {
						// Return Type declaration.
				boolean deleted = false;
					// Central buffer node declaration for operation stateWithoutTeamsDelete
			Long	 nbTeams =  Long.valueOf(0);
					// Calling an operation : get nb teams
nbTeams = getNumbersOfTeams(state64ToDelete);
// Test on exception : 
if (nbTeams>0) {
ApplicationException app = new ApplicationException("You must delete the teams with this state before deleting the state.");
throw app;
}
// Calling an operation : Delete State
deleted = stateDelete(state64ToDelete);

						return deleted;
		}

		
			
	/**    
	 * Operation HQL.
	 * @param state64ToDelete {@link State64BO}
		 * @return {@link Long}
	 */
	public Long getNumbersOfTeams(State64BO state64ToDelete )
 throws ApplicationException {
	
	return serviceState64Dao.getNumbersOfTeams(state64ToDelete);
	}

		}
