/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.entities.uc60_multidomain;


import java.io.Serializable;
import java.util.List;

import com.bluage.documentation.business.standardfeatures.entities.uc60_multidomain.players.bos.Player60BO;
import com.bluage.documentation.business.standardfeatures.entities.uc60_multidomain.teams.bos.Team60BO;
import com.netfective.bluage.core.web.AbstractForm;
import com.netfective.bluage.core.web.IForm;

/**
* Class : UC60_ViewForm
*/
public class UC60_ViewForm extends AbstractForm implements Serializable, IForm {
	
	/** Constant : serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	//CONSTANT : listplayers
	private static final String LISTPLAYERS = "listplayers";
	//CONSTANT : viewPlayers
	private static final String VIEW_PLAYERS = "viewPlayers";
	/**
	 * 	Property: listplayers 
	 */
	private List<Player60BO> listplayers;
	/**
	 * 	Property: viewPlayers 
	 */
	private Team60BO viewPlayers;
/**
	 * Default constructor : UC60_ViewForm
	 */
	public UC60_ViewForm() {
		super();
		// Initialize : listplayers
		this.listplayers = new java.util.ArrayList<com.bluage.documentation.business.standardfeatures.entities.uc60_multidomain.players.bos.Player60BO>();
		// Initialize : viewPlayers
		this.viewPlayers = new Team60BO();
					this.setAlwaysCallPreControllers(true);
	}
	
	/**
	 * 	Getter : listplayers 
	 * 	@return : Return the listplayers instance.
	 */
	public List<Player60BO> getListplayers(){
		return listplayers; // For UC60_ViewForm
	}
	
	/**
	 * 	Setter : listplayers 
	 *  @param listplayersinstance : The instance to set.
	 */
	public void setListplayers(final List<Player60BO> listplayersinstance){
		this.listplayers = listplayersinstance;// For UC60_ViewForm
	}
	/**
	 * 	Getter : viewPlayers 
	 * 	@return : Return the viewPlayers instance.
	 */
	public Team60BO getViewPlayers(){
		return viewPlayers; // For UC60_ViewForm
	}
	
	/**
	 * 	Setter : viewPlayers 
	 *  @param viewPlayersinstance : The instance to set.
	 */
	public void setViewPlayers(final Team60BO viewPlayersinstance){
		this.viewPlayers = viewPlayersinstance;// For UC60_ViewForm
	}
/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UC60_ViewForm [ "+
LISTPLAYERS +" = " + listplayers +VIEW_PLAYERS +" = " + viewPlayers + "]";
	}
	
	/**
	 * Method setInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public void setInstanceByInstanceName(final String instanceName, final Object instance) {
		// Set the instance Listplayers.
		if(LISTPLAYERS.equals(instanceName)){// For UC60_ViewForm
			this.setListplayers((List<Player60BO>)instance); // For UC60_ViewForm
		}
				// Set the instance ViewPlayers.
		if(VIEW_PLAYERS.equals(instanceName)){// For UC60_ViewForm
			this.setViewPlayers((Team60BO)instance); // For UC60_ViewForm
		}
				// Parent instance name for UC60_ViewForm
		if(VIEW_PLAYERS.equals(instanceName) && viewPlayers != null){ // For UC60_ViewForm
			listplayers.clear(); // For UC60_ViewForm
			listplayers.addAll(viewPlayers.getPlayers());// For UC60_ViewForm
		}
		
	}
	
	/**
	 * Method getInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public Object getInstanceByInstanceName(final String instanceName){
		// Temporary object for tmpUC60_ViewForm.
		Object tmpUC60_ViewForm = null;
		
			
		// Get the instance Listplayers for UC60_ViewForm.
		if(LISTPLAYERS.equals(instanceName)){ // For UC60_ViewForm
			tmpUC60_ViewForm = this.getListplayers(); // For UC60_ViewForm
		}
			
		// Get the instance ViewPlayers for UC60_ViewForm.
		if(VIEW_PLAYERS.equals(instanceName)){ // For UC60_ViewForm
			tmpUC60_ViewForm = this.getViewPlayers(); // For UC60_ViewForm
		}
		return tmpUC60_ViewForm;// For UC60_ViewForm
	}
	
			}
