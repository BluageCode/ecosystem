/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.service.qauc.uc1001_tables;


import javax.inject.Inject;
import javax.inject.Named;

import com.bluage.documentation.business.qauc.uc1001_tables.bos.Team1001BO;
import com.bluage.documentation.business.qauc.uc1001_tables.entities.daofinder.Team1001DAOFinderImpl;
import com.bluage.documentation.service.AbstractService;
import com.netfective.bluage.core.exception.ApplicationException;

/**
 * Defines  application services for ServiceTeam1001
  */
@Named("com.bluage.documentation.service.qauc.uc1001_tables.ServiceTeam1001")
public class ServiceTeam1001  extends AbstractService{


			@Inject
		@Named ("com.bluage.documentation.business.qauc.uc1001_tables.entities.daofinder.Team1001DAOFinderImpl")
		private Team1001DAOFinderImpl team1001BOFinder;
	


	/**
	 *  Default constructor 
	 */
	public ServiceTeam1001() {
		super(); // Call super for default constructor : ServiceTeam1001
	}
	


			
				
	/**    
	 * Operation Crud
	 * @param bo {@link Team1001BO}
		 * @return {@link Team1001BO}
	 */
	public Team1001BO teamFindByID(Team1001BO bo )
 throws ApplicationException {

					return (Team1001BO)team1001BOFinder.findByPrimaryKey(getBoId(bo));
	}
	/**
	 *  Return the primary key from business object.
	 */
	private Object getBoId(Team1001BO bo) {
				return bo.getId();
		}

		}
