<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file="/include.jsp"  %>
<html >
<head >
<title  > 
<fmt:message key="cUC02_EntitiesCreation_BLU_AGE_DOCUMENTATION"/>
</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"  > 
</meta>
<link href="<c:url value= "/presentation/_style/_style.css"/>" rel="stylesheet" type="text/css"  /> 
<link href="<c:url value= "/presentation/_include/jquery/rd_lib.css"/>" rel="stylesheet" type="text/css"  /> 
<script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/jquery.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/rd_setup.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/rd_lib.js"/>" space="preserve"  ></script><link rel="shortcut icon" href="<c:url value= "/presentation/_style/favicon.ico"/>"  /> 

</head>

<body class=" " > 
<div id="rd_ghost"  > 
<small  > 
<fmt:message key="cUC02_EntitiesCreation___CSS_DEACTIVATED"/>
</small>
</div>
<div id="rd_jghost"  > 
<small  > 
<fmt:message key="cUC02_EntitiesCreation___JAVASCRIPT_DEACTIVATED"/>
</small>
</div>
<div id="shape"  > 
<div class="ui-layout-center"  > 
<div class="east-north"  > 
	<form:form id="uC02_EntitiesCreation" commandName="uC02_EntitiesCreationForm"  enctype="application/x-www-form-urlencoded" method="post">
<h2 class="rd_first"  > 
<fmt:message key="cUC02_EntitiesCreation_GETTING_STARTED__UC02__ENTIT"/>
</h2>
<table class="rd_tableform"  >
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC02_EntitiesCreation_CITY"/>
</label>
<label style="background: none;color: red;"  > 
<fmt:message key="cUC02_EntitiesCreation_"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
<form:input id="txt_city" path="teamToCreate.city"  
		/>
</td> 
<td rowspan="1" colspan="1"  >
<form:errors path="teamToCreate.city"/>
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC02_EntitiesCreation_NAME"/>
</label>
<label style="background: none;color: red;"  > 
<fmt:message key="cUC02_EntitiesCreation__37"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
<form:input id="txt_name" path="teamToCreate.name"  
		/>
</td> 
<td rowspan="1" colspan="1"  >
<form:errors path="teamToCreate.name"/>
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC02_EntitiesCreation_STATE"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
<form:select id="select_state" path="teamToCreate.state.code"   
		>
		<form:options items="${uC02_EntitiesCreationForm.allStates}" itemValue="code" itemLabel="name" />
</form:select>
</td> 
<td rowspan="1" colspan="1"  >
<fmt:message key="cUC02_EntitiesCreation__38"/>
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<fmt:message key="cUC02_EntitiesCreation__39"/>
</td> 
<td rowspan="1" colspan="1"  >
<a id="lnk_create"onclick="javascript:btn_click_cExecuteUC02_EntitiesCreation(this.href);return false;"				href="<c:url value="/presentation/gettingstarted/uc02_entitiescreation/UC02_EntitiesCreation/lnk_create.html"/>"
							 shape="rect"  >									 
<fmt:message key="cUC02_EntitiesCreation_CREATE_A_NEW_TEAM"/>
</a>
</td> 
<td rowspan="1" colspan="1"  >
<fmt:message key="cUC02_EntitiesCreation__40"/>
</td> 
</tr>
</table> 
<label style="background: none;color: red;"  > 
<fmt:message key="cUC02_EntitiesCreation__41"/>
</label>
<fmt:message key="cUC02_EntitiesCreation_REQUIRED_ENTRY_"/>
<br clear="none"  /> 
<br clear="none"  /> 
<display:table uid="table_teams"  sort="list" requestURI="" name="sessionScope.uC02_EntitiesCreationForm.allTeams" class="rd_tabledata"  >
	<c:set   var="rownum">
		<%=(table_teams !=	null) ? new Integer(table_teams_rowNum.intValue() - 1) : new Integer(0) %>
	</c:set>
	<c:set   var="idRow">
		<%=(table_teams != null ) ? table_teams_rowNum.intValue() - 1 : -1 %>
	</c:set>
	<c:set   var="idTab">
		<%=(table_teams != null ) ? "table_teams" : null %>
	</c:set>
	<c:set var="tableHashId">
		<%= new org.displaytag.util.ParamEncoder("table_teams").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE) %>
	</c:set>
	<c:set var="tableHashId2">
		<%=request.getParameter(new org.displaytag.util.ParamEncoder("table_teams").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE)) %>
	</c:set>	
	
<display:column format=""  titleKey="cUC02_EntitiesCreation_CITY_42">
		<span id="dyn_city">
	<c:out value="${table_teams.city}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="cUC02_EntitiesCreation_NAME_43">
		<span id="dyn_name">
	<c:out value="${table_teams.name}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="cUC02_EntitiesCreation_STATE_44">
		<span id="dyn_statename">
	<c:out value="${table_teams.state.name}" />
		</span>
	<fmt:message key="cUC02_EntitiesCreation__45"/>
		<span id="dyn_statecode">
	<c:out value="${table_teams.state.code}" />
		</span>
	<fmt:message key="cUC02_EntitiesCreation____"/>
</display:column>
	
<display:column format=""  titleKey="cUC02_EntitiesCreation_DELETE">
<a id="lnk_delete[${rownum}]"onclick="javascript:btn_click_cExecuteUC02_EntitiesCreation(this.href);return false;"			href="<c:url value="/presentation/gettingstarted/uc02_entitiescreation/UC02_EntitiesCreation/lnk_delete.html?id="/><c:out value="${idRow}" />&tab=<c:out value="table_teams" />&<c:out value="${tableHashId}"/>=<c:out value="${tableHashId2}"/>"
	 shape="rect"  >									 
<fmt:message key="cUC02_EntitiesCreation_DELETE_46"/>
</a>
</display:column>
</display:table>
	</form:form>
	<script language="javascript">
		function btn_click_cExecuteUC02_EntitiesCreation(url) {
			UC02_EntitiesCreationvar = document.getElementById('uC02_EntitiesCreation');
			UC02_EntitiesCreationvar.setAttribute('action', url);
			UC02_EntitiesCreationvar.submit();
		}
		function btn_click_cSetActionUC02_EntitiesCreation(url) {
			UC02_EntitiesCreationvar = document.getElementById('uC02_EntitiesCreation');
			UC02_EntitiesCreationvar.setAttribute('action', url);
		}
	</script>
</div>
<div class="east-center"  > 
<tiles:insertDefinition	name="presentation/gettingstarted/uc02_entitiescreation/UC02_EntitiesCreationcmp_tabs" />
</div>
</div>
<div class="ui-layout-north"  > 
<tiles:insertDefinition	name="presentation/gettingstarted/uc02_entitiescreation/UC02_EntitiesCreationcmp_header" />
</div>
<div class="ui-layout-south"  > 
<fmt:message key="cUC02_EntitiesCreation___"/>
<span id="rd_jversion"  > 
<fmt:message key="cUC02_EntitiesCreation_BLU_AGE_CORPORATION"/>
</span>
</div>
<div class="ui-layout-west"  > 
<tiles:insertDefinition	name="presentation/gettingstarted/uc02_entitiescreation/UC02_EntitiesCreationcmp_treeview" />
</div>
</div>
</body>
</html>
