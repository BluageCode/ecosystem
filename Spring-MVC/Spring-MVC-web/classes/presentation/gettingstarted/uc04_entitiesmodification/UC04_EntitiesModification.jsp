<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file="/include.jsp"  %>
<html >
<head >
<title  > 
<fmt:message key="cUC04_EntitiesModification_BLU_AGE_DOCUMENTATION"/>
</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"  > 
</meta>
<link href="<c:url value= "/presentation/_style/_style.css"/>" rel="stylesheet" type="text/css"  /> 
<link href="<c:url value= "/presentation/_include/jquery/rd_lib.css"/>" rel="stylesheet" type="text/css"  /> 
<script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/jquery.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/rd_setup.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/rd_lib.js"/>" space="preserve"  ></script><link rel="shortcut icon" href="<c:url value= "/presentation/_style/favicon.ico"/>"  /> 

</head>

<body class=" " > 
<div id="rd_ghost"  > 
<small  > 
<fmt:message key="cUC04_EntitiesModification___CSS_DEACTIVATED"/>
</small>
</div>
<div id="rd_jghost"  > 
<small  > 
<fmt:message key="cUC04_EntitiesModification___JAVASCRIPT_DEACTIVATED"/>
</small>
</div>
<div id="shape"  > 
<div class="ui-layout-center"  > 
<div class="east-north"  > 
	<form:form id="uC04_EntitiesModification" commandName="uC04_EntitiesModificationForm"  enctype="application/x-www-form-urlencoded" method="post">
<h2 class="rd_first"  > 
<fmt:message key="cUC04_EntitiesModification_GETTING_STARTED__UC04__ENTIT"/>
</h2>
<display:table uid="tab_players"  sort="list" requestURI="" name="sessionScope.uC04_EntitiesModificationForm.allPlayers" class="rd_tabledata"  >
	<c:set   var="rownum">
		<%=(tab_players !=	null) ? new Integer(tab_players_rowNum.intValue() - 1) : new Integer(0) %>
	</c:set>
	<c:set   var="idRow">
		<%=(tab_players != null ) ? tab_players_rowNum.intValue() - 1 : -1 %>
	</c:set>
	<c:set   var="idTab">
		<%=(tab_players != null ) ? "tab_players" : null %>
	</c:set>
	<c:set var="tableHashId">
		<%= new org.displaytag.util.ParamEncoder("tab_players").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE) %>
	</c:set>
	<c:set var="tableHashId2">
		<%=request.getParameter(new org.displaytag.util.ParamEncoder("tab_players").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE)) %>
	</c:set>	
	
<display:column format=""  titleKey="cUC04_EntitiesModification_NAME">
<c:if test="${uC04_EntitiesModificationForm.selectedRow!=idRow || uC04_EntitiesModificationForm.selectedTab!=idTab}">
		<span id="dyn_lastname">
	<c:out value="${tab_players.lastName}" />
		</span>
	<fmt:message key="cUC04_EntitiesModification_"/>
		<span id="dyn_firstname">
	<c:out value="${tab_players.firstName}" />
		</span>
	</c:if>
<c:if test="${uC04_EntitiesModificationForm.selectedRow==idRow && uC04_EntitiesModificationForm.selectedTab==idTab}">
<form:input id="txt_lastname[${rownum}]" path="allPlayers[${rownum}].lastName"  
		/>
<form:errors path="allPlayers[${rownum}].lastName"/>
<fmt:message key="cUC04_EntitiesModification__54"/>
<form:input id="txt_firstname[${rownum}]" path="allPlayers[${rownum}].firstName"  
		/>
<form:errors path="allPlayers[${rownum}].firstName"/>
</c:if>
</display:column>
	
<display:column format=""  titleKey="cUC04_EntitiesModification_DATE_OF_BIRTH">
<c:if test="${uC04_EntitiesModificationForm.selectedRow!=idRow || uC04_EntitiesModificationForm.selectedTab!=idTab}">
<span id="dyn_dateofbirth">
	<format:formatDateValidator value="${tab_players.dateOfBirth}" pattern="MM/dd/yyyy" />
</span>
	</c:if>
<c:if test="${uC04_EntitiesModificationForm.selectedRow==idRow && uC04_EntitiesModificationForm.selectedTab==idTab}">
<form:input id="txt_dateofbirth[${rownum}]" path="allPlayers[${rownum}].dateOfBirth"  
		/>
<form:errors path="allPlayers[${rownum}].dateOfBirth"/>
</c:if>
</display:column>
	
<display:column format=""  titleKey="cUC04_EntitiesModification_WEIGHT_KG">
		<span id="dyn_weight">
	<c:out value="${tab_players.weight}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="cUC04_EntitiesModification_HEIGHT_CM">
		<span id="dyn_height">
	<c:out value="${tab_players.height}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="cUC04_EntitiesModification_ESTIMATED_VALUE_M">
		<span id="dyn_value">
	<c:out value="${tab_players.estimatedValue}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="cUC04_EntitiesModification_POSITION">
		<span id="dyn_position">
	<c:out value="${tab_players.position.code}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="cUC04_EntitiesModification__55">
<c:if test="${uC04_EntitiesModificationForm.selectedRow!=idRow || uC04_EntitiesModificationForm.selectedTab!=idTab}">
<a id="lnk_edit[${rownum}]"onclick="javascript:btn_click_cExecuteUC04_EntitiesModification(this.href);return false;"			href="<c:url value="/presentation/gettingstarted/uc04_entitiesmodification/UC04_EntitiesModification/lnk_edit.html?id="/><c:out value="${idRow}" />&tab=<c:out value="tab_players" />&<c:out value="${tableHashId}"/>=<c:out value="${tableHashId2}"/>"
	 shape="rect"  >									 
<fmt:message key="cUC04_EntitiesModification_EDIT"/>
</a>
</c:if>
<c:if test="${uC04_EntitiesModificationForm.selectedRow==idRow && uC04_EntitiesModificationForm.selectedTab==idTab}">
<a id="btn_cancel[${rownum}]"onclick="javascript:btn_click_cExecuteUC04_EntitiesModification(this.href);return false;"			href="<c:url value="/presentation/gettingstarted/uc04_entitiesmodification/UC04_EntitiesModification/btn_cancel.html?id="/><c:out value="${idRow}" />&tab=<c:out value="tab_players" />&<c:out value="${tableHashId}"/>=<c:out value="${tableHashId2}"/>"
	 shape="rect"  >									 
<fmt:message key="cUC04_EntitiesModification_CANCEL"/>
</a>
<fmt:message key="cUC04_EntitiesModification_____"/>
<a id="lnk_update[${rownum}]"onclick="javascript:btn_click_cExecuteUC04_EntitiesModification(this.href);return false;"			href="<c:url value="/presentation/gettingstarted/uc04_entitiesmodification/UC04_EntitiesModification/lnk_update.html?id="/><c:out value="${idRow}" />&tab=<c:out value="tab_players" />&<c:out value="${tableHashId}"/>=<c:out value="${tableHashId2}"/>"
	 shape="rect"  >									 
<fmt:message key="cUC04_EntitiesModification_UPDATE"/>
</a>
</c:if>
</display:column>
	
<display:column format=""  titleKey="cUC04_EntitiesModification__56">
<a id="lnk_details[${rownum}]"onclick="javascript:btn_click_cExecuteUC04_EntitiesModification(this.href);return false;"			href="<c:url value="/presentation/gettingstarted/uc04_entitiesmodification/UC04_EntitiesModification/lnk_details.html?id="/><c:out value="${idRow}" />&tab=<c:out value="tab_players" />&<c:out value="${tableHashId}"/>=<c:out value="${tableHashId2}"/>"
	 shape="rect"  >									 
<fmt:message key="cUC04_EntitiesModification_DETAILS"/>
</a>
</display:column>
</display:table>
	</form:form>
	<script language="javascript">
		function btn_click_cExecuteUC04_EntitiesModification(url) {
			UC04_EntitiesModificationvar = document.getElementById('uC04_EntitiesModification');
			UC04_EntitiesModificationvar.setAttribute('action', url);
			UC04_EntitiesModificationvar.submit();
		}
		function btn_click_cSetActionUC04_EntitiesModification(url) {
			UC04_EntitiesModificationvar = document.getElementById('uC04_EntitiesModification');
			UC04_EntitiesModificationvar.setAttribute('action', url);
		}
	</script>
</div>
<div class="east-center"  > 
<tiles:insertDefinition	name="presentation/gettingstarted/uc04_entitiesmodification/UC04_EntitiesModificationcmp_tabs" />
</div>
</div>
<div class="ui-layout-north"  > 
<tiles:insertDefinition	name="presentation/gettingstarted/uc04_entitiesmodification/UC04_EntitiesModificationcmp_header" />
</div>
<div class="ui-layout-south"  > 
<fmt:message key="cUC04_EntitiesModification___"/>
<span id="rd_jversion"  > 
<fmt:message key="cUC04_EntitiesModification_BLU_AGE_CORPORATION"/>
</span>
</div>
<div class="ui-layout-west"  > 
<tiles:insertDefinition	name="presentation/gettingstarted/uc04_entitiesmodification/UC04_EntitiesModificationcmp_treeview" />
</div>
</div>
</body>
</html>
