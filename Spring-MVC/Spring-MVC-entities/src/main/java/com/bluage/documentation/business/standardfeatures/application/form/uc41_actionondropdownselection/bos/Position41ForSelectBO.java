/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.business.standardfeatures.application.form.uc41_actionondropdownselection.bos;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import com.bluage.documentation.business.AbstractBusinessObject;

/**
 * Position41ForSelectBO
 *
* @see com.netfective.bluage.core.business.IBusinessObject
 */
@XmlRootElement

public class Position41ForSelectBO
 extends AbstractBusinessObject {
	
	/**
	 * Default SUID for Position41ForSelectBO.
	 */
	private static final long serialVersionUID = 1L;
	

						/**
	 * Entity property: allPositions
	 *
	 */
private List allPositions;

					/**
	 * Entity property: toRemove
	 *
	 */
private String toRemove;


	private boolean displayPosition = true;

   /**
	* public default constructor for class Position41ForSelectBO
	*/
	public Position41ForSelectBO(){
		//NOP Position41ForSelectBO
	}


						/**
	 * Getter for allPositions of class Position41ForSelectBO.
	 *
	 * @return {@link java.util.List}
	 * @generated
	 */ 
public  List getAllPositions() {
	return this.allPositions;
	}


	/**
	 * Setter for allPositions of class Position41ForSelectBO.
	 * @param value {@link java.util.List}
	 * @generated
	 */

public  void setAllPositions(final List allPositions) {
	this.allPositions = allPositions;
}

					/**
	 * Getter for toRemove of class Position41ForSelectBO.
	 *
	 * @return {@link java.lang.String}
	 * @generated
	 */ 
public  String getToRemove() {
	return this.toRemove;
	}


	/**
	 * Setter for toRemove of class Position41ForSelectBO.
	 * @param value {@link java.lang.String}
	 * @generated
	 */

public  void setToRemove(final String toRemove) {
	this.toRemove = toRemove;
}


		
	/**
	 * Getter for displayPosition of class Position41ForSelectBO.
	 * @return {@link boolean}
	 */
	public boolean isDisplayPosition()
	{
		return this.displayPosition;
	}
	
	/**
	 * @deprecated Use isDisplayPosition() instead.
	 */
	public boolean getDisplayPosition()
	{
		return this.displayPosition;
	}
	
	/**
	 * Setter for displayPosition of class Position41ForSelectBO.
	 * @param value: {@link boolean}
	 */
	public void setDisplayPosition(boolean displayPosition) 
	{
		this.displayPosition = displayPosition;
	}
}

