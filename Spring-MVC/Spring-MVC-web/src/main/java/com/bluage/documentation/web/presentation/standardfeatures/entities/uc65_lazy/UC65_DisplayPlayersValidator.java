/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.entities.uc65_lazy ;


import org.apache.log4j.Logger;
import org.springframework.validation.Errors;

import com.netfective.bluage.core.validator.AbstractValidator;
/**
* Class :UC65_DisplayPlayersValidator
*/
public class UC65_DisplayPlayersValidator extends AbstractValidator{
	
	// LOGGER for the class UC65_DisplayPlayersValidator
	private static final Logger LOGGER = Logger.getLogger( UC65_DisplayPlayersValidator.class);
	
	
	/**
	* Operation validate for UC65_DisplayPlayersForm
	* @param obj : the current form (UC65_DisplayPlayersForm)
	* @param errors : The spring errors to return for the form UC65_DisplayPlayersForm
	*/
	public void validate(final Object obj,final Errors errors){
		LOGGER.info("Beginning method : validate"); // UC65_DisplayPlayersValidator
		UC65_DisplayPlayersForm cUC65_DisplayPlayersForm = (UC65_DisplayPlayersForm)obj; // UC65_DisplayPlayersValidator
		LOGGER.info("Ending method : validate the form "+ cUC65_DisplayPlayersForm.getClass().getName()); // UC65_DisplayPlayersValidator
	}

	/**
	* Method to implements to use spring validators (UC65_DisplayPlayersForm)
	* @param aClass : Class for the form UC65_DisplayPlayersForm
	* @return boolean
	*/
	public boolean supports(final Class aClass){
		return (new UC65_DisplayPlayersForm()).getClass().equals(aClass); // UC65_DisplayPlayersValidator
	}
}
