/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.common.components ;

// Import declaration.
// Java imports.
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;

/**
 * Class : cmp_tabsController
 */
@Controller
@Scope("prototype")
@SessionAttributes("cmp_tabsForm")
@RequestMapping(value= "/common/components")
public class cmp_tabsController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(cmp_tabsController.class);
	
	//Current form name
	private static final String CMP_TABS_FORM = "cmp_tabsForm";

	
	/**
	 * Property:customDateEditorscmp_tabsController
	 */
	final private Map<String,CustomDateEditor> customDateEditorscmp_tabsController = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:cmp_tabsValidator
	 */
	final private cmp_tabsValidator cmp_tabsValidator = new cmp_tabsValidator();
	
	
	// Initialise all the instances for cmp_tabsController

	/**
	* This method initialise the form : cmp_tabsForm 
	* @return cmp_tabsForm
	*/
	@ModelAttribute("cmp_tabsFormInit")
	public void initcmp_tabsForm(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, CMP_TABS_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method cmp_tabsForm."); //for lnk_home 
		}
		cmp_tabsForm cmp_tabsForm;
	
		if(request.getSession().getAttribute(CMP_TABS_FORM) != null){
			cmp_tabsForm = (cmp_tabsForm)request.getSession().getAttribute(CMP_TABS_FORM);
		} else {
			cmp_tabsForm = new cmp_tabsForm();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form cmp_tabsForm.");
		}
		cmp_tabsForm = (cmp_tabsForm)populateDestinationForm(request.getSession(), cmp_tabsForm);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  cmp_tabsForm.");
		}
		model.addAttribute(CMP_TABS_FORM, cmp_tabsForm);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param cmp_tabsForm : The sceen form.
	 */
	@RequestMapping(value = "/cmp_tabs.html" ,method = RequestMethod.GET)
	public void preparecmp_tabs(final HttpServletRequest request,final  Model model,final  @ModelAttribute("cmp_tabsFormInit") cmp_tabsForm cmp_tabsForm){
		
		cmp_tabsForm currentcmp_tabsForm = cmp_tabsForm;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(CMP_TABS_FORM) == null){
			if(currentcmp_tabsForm!=null){
				request.getSession().setAttribute(CMP_TABS_FORM, currentcmp_tabsForm);
			}else {
				currentcmp_tabsForm = new cmp_tabsForm();
				request.getSession().setAttribute(CMP_TABS_FORM, currentcmp_tabsForm);	
			}
		} else {
			currentcmp_tabsForm = (cmp_tabsForm) request.getSession().getAttribute(CMP_TABS_FORM);
		}

		currentcmp_tabsForm = (cmp_tabsForm)populateDestinationForm(request.getSession(), currentcmp_tabsForm);
		// Call all the Precontroller.
	currentcmp_tabsForm.setAlwaysCallPreControllers(true);
		model.addAttribute(CMP_TABS_FORM, currentcmp_tabsForm);
		request.getSession().setAttribute(CMP_TABS_FORM, currentcmp_tabsForm);
		
	}
	
	
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("cmp_tabsController [ ");
		// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(cmp_tabsValidator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorscmp_tabsController!=null); 
		return strBToS.toString();
	}
}
