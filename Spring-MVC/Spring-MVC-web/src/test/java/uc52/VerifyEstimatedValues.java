package uc52;

import org.junit.Test;
import test.common.AbstractBADocTestCase;


public class VerifyEstimatedValues extends AbstractBADocTestCase {
	
	@Override
	public  String getName(){
		return "UC-52";
	}	

	@Test
	public void verifyEstimatedValues() throws Exception {
				
		selenium.open("");	
		selenium.click("link=Expand All");
		selenium.waitForPageToLoad("30000");
		selenium.click("link=UC52 - Conditional");
		
		// add first player
		selenium.type("//input[contains(@id,'txt_firstName')]", "player1");
		selenium.type("//input[contains(@id,'txt_lastName')]", "player1");
		selenium.type("//input[contains(@id,'txt_value')]", "5.2");
		selenium.click("//*[contains(@id,'lnk_addPlayer')]");
		selenium.waitForPageToLoad("30000");
		// add second player
		selenium.type("//input[contains(@id,'txt_firstName')]", "player2");
		selenium.type("//input[contains(@id,'txt_lastName')]", "player2");
		selenium.type("//input[contains(@id,'txt_value')]", "12.4");
		// selenium.while("!${val}");
		selenium.click("//*[contains(@id,'chx_rookie')]");
		// selenium.endWhile();
		selenium.click("//*[contains(@id,'lnk_addPlayer')]");
		selenium.waitForPageToLoad("30000");
		// show list of players
		selenium.click("//*[contains(@id,'lnk_showPlayers')]");
		selenium.waitForPageToLoad("30000");
		// verify entred values
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_PlayersList')]/thead/tr[th[contains(text(),\"Last Name\")]and th[contains(text(),\"First Name\")] and th[contains(text(),\"Estimated Value (M$)\")] and th[contains(text(),\"Rookie\")]]"));
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_PlayersList')]/tbody/tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_PlayersList')]/tbody/tr[td/span[contains(text(),\"Vega\")]and td/span[contains(text(),\"Manu\")] and td/span[contains(text(),\"18.5\")] and td//input[not(@checked)]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_PlayersList')]/tbody/tr[td/span[contains(text(),\"Glod\")] and td/span[contains(text(),\"Hernest\")] and td/span[contains(text(),\"39.9\")] and td//input[@checked]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_PlayersList')]/tbody/tr[td/span[contains(text(),\"player1\")]and td/span[contains(text(),\"player1\")] and td/span[contains(text(),\"5.2\")] and td//input[not(@checked)]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_PlayersList')]/tbody/tr[td/span[contains(text(),\"player2\")]and td/span[contains(text(),\"player2\")] and td/span[contains(text(),\"11.159999\")] and td//input[@checked]]"));
	}


}
