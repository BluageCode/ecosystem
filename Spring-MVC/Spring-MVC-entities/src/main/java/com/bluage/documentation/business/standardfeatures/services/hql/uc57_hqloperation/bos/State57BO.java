/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.business.standardfeatures.services.hql.uc57_hqloperation.bos;

import javax.inject.Inject;
import javax.inject.Named;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import com.bluage.documentation.business.common.bos.StateBO;
import com.bluage.documentation.business.standardfeatures.services.hql.uc57_hqloperation.entities.daofinder.State57DAOFinderImpl;

/**
 * State57BO
 *
* @see com.netfective.bluage.core.business.IBusinessObject
* * @see StateBO
 */
@XmlRootElement

public class State57BO
 extends StateBO {
	
	/**
	 * Default SUID for State57BO.
	 */
	private static final long serialVersionUID = 1L;
	

	@Inject
	@Named ("com.bluage.documentation.business.standardfeatures.services.hql.uc57_hqloperation.entities.daofinder.State57Finder") 
	private State57DAOFinderImpl finder;

	

   /**
	* public default constructor for class State57BO
	*/
	public State57BO(){
		//NOP State57BO
	}


	
/**
	 * Returns <code>true</code> if the argument is an State57 instance and all identifiers for this entity
     * equal the identifiers of the argument entity. The <code>equals</code> method of the parent entity
     * will also need to return <code>true</code>. Returns <code>false</code> otherwise.
     *
     * @see StateBO#hashCode()
	 * @param object  object to compare with current instance      
     * @return boolean true if equals false if not
     */
    public boolean equals(final Object object )
    {
		if ( this == object )
        {
            return true;
        }
        if ( !( object instanceof State57BO ) )
        {
            return false;
        }
        final State57BO that = (State57BO)object;
    
    	            if (        				        			this.getCode() == null
        	        ) {
        	return false;
        }
		return new EqualsBuilder()
			.append(this.getCode(), that.getCode())
			 	.isEquals();
	}
	/**

	 * Returns a hash code based on this entity's identifiers and the hash code of the parent entity.
     *
     * @see StateBO#hashCode()
     * @return int
     */    
    public int hashCode() {
        return new HashCodeBuilder()
             .append(getCode())
				 	.appendSuper(super.hashCode())
            .toHashCode();
    }
}

