package uc30;

import org.junit.Test;
import test.common.AbstractBADocTestCase;


public class ITuc30 extends AbstractBADocTestCase {
	
	@Override
	public  String getName(){
		return "UC-30";
	}

	@Test
	public void testITuc30() throws Exception {
		// On teste l'UC30 - Standard Features/Application/Navigation - Navigation
		selenium.open("");
		selenium.click("link=Expand All");
		selenium.waitForPageToLoad("30000");
		selenium.click("link=UC30 - Navigation");
		assertTrue(selenium.isElementPresent("//h2[contains(text(),\"Standard Features > Application > Navigation > UC30 > Navigation\")]"));
		assertTrue(selenium.isElementPresent("//h4[contains(text(),\"Navigation page\")]"));
		selenium.select("//*[contains(@id,'select_page')]", "hello world");
		selenium.waitForPageToLoad("30000");
		selenium.click("//*[contains(@id,'btn_execute')]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//h4[contains(text(),\"Hello World !!\")]"));
		selenium.click("//*[contains(@id,'lnk_page1')]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//h4[contains(text(),\"Navigation page\")]"));
		selenium.select("//*[contains(@id,'select_page')]", "goodbye world");
		selenium.waitForPageToLoad("30000");
		selenium.click("//*[contains(@id,'btn_execute')]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//h4[contains(text(),\"Goodbye World !!\")]"));
		selenium.click("//*[contains(@id,'lnk_page1')]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//h4[contains(text(),\"Navigation page\")]"));
	}


}
