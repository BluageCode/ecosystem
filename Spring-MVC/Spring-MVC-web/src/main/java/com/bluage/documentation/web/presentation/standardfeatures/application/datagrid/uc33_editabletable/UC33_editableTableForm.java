/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.application.datagrid.uc33_editabletable;


import java.io.Serializable;
import java.util.List;

import com.bluage.documentation.business.standardfeatures.application.datagrid.uc33_editabletable.bos.Team33BO;
import com.netfective.bluage.core.web.AbstractForm;
import com.netfective.bluage.core.web.IForm;

/**
* Class : UC33_editableTableForm
*/
public class UC33_editableTableForm extends AbstractForm implements Serializable, IForm {
	
	/** Constant : serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	//CONSTANT : allTeams
	private static final String ALL_TEAMS = "allTeams";
	//CONSTANT : selectedTeam
	private static final String SELECTED_TEAM = "selectedTeam";
	/**
	 * 	Property: allTeams 
	 */
	private List<Team33BO> allTeams;
	/**
	 * 	Property: selectedTeam 
	 */
	private Team33BO selectedTeam;
/**
	 * Default constructor : UC33_editableTableForm
	 */
	public UC33_editableTableForm() {
		super();
		// Initialize : allTeams
		this.allTeams = new java.util.ArrayList<com.bluage.documentation.business.standardfeatures.application.datagrid.uc33_editabletable.bos.Team33BO>();
		// Initialize : selectedTeam
		this.selectedTeam = null;
					this.setAlwaysCallPreControllers(true);
	}
	
	/**
	 * 	Getter : allTeams 
	 * 	@return : Return the allTeams instance.
	 */
	public List<Team33BO> getAllTeams(){
		return allTeams; // For UC33_editableTableForm
	}
	
	/**
	 * 	Setter : allTeams 
	 *  @param allTeamsinstance : The instance to set.
	 */
	public void setAllTeams(final List<Team33BO> allTeamsinstance){
		this.allTeams = allTeamsinstance;// For UC33_editableTableForm
	}
	/**
	 * 	Getter : selectedTeam 
	 * 	@return : Return the selectedTeam instance.
	 */
	public Team33BO getSelectedTeam(){
		return selectedTeam; // For UC33_editableTableForm
	}
	
	/**
	 * 	Setter : selectedTeam 
	 *  @param selectedTeaminstance : The instance to set.
	 */
	public void setSelectedTeam(final Team33BO selectedTeaminstance){
		this.selectedTeam = selectedTeaminstance;// For UC33_editableTableForm
	}
/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UC33_editableTableForm [ "+
ALL_TEAMS +" = " + allTeams +SELECTED_TEAM +" = " + selectedTeam + "]";
	}
	
	/**
	 * Method setInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public void setInstanceByInstanceName(final String instanceName, final Object instance) {
		// Set the instance AllTeams.
		if(ALL_TEAMS.equals(instanceName)){// For UC33_editableTableForm
			this.setAllTeams((List<Team33BO>)instance); // For UC33_editableTableForm
		}
				// Set the instance SelectedTeam.
		if(SELECTED_TEAM.equals(instanceName)){// For UC33_editableTableForm
			this.setSelectedTeam((Team33BO)instance); // For UC33_editableTableForm
		}
			}
	
	/**
	 * Method getInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public Object getInstanceByInstanceName(final String instanceName){
		// Temporary object for tmpUC33_editableTableForm.
		Object tmpUC33_editableTableForm = null;
		
			
		// Get the instance AllTeams for UC33_editableTableForm.
		if(ALL_TEAMS.equals(instanceName)){ // For UC33_editableTableForm
			tmpUC33_editableTableForm = this.getAllTeams(); // For UC33_editableTableForm
		}
			
		// Get the instance SelectedTeam for UC33_editableTableForm.
		if(SELECTED_TEAM.equals(instanceName)){ // For UC33_editableTableForm
			tmpUC33_editableTableForm = this.getSelectedTeam(); // For UC33_editableTableForm
		}
		return tmpUC33_editableTableForm;// For UC33_editableTableForm
	}
	
			}
