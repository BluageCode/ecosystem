/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.application.datagrid.uc38_selectedlist ;

// Import declaration.
// Java imports.
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.bluage.documentation.business.standardfeatures.application.datagrid.uc38_selectedlist.bos.Player38BO;
import com.bluage.documentation.business.standardfeatures.application.datagrid.uc38_selectedlist.entities.daofinder.Team38DAOFinderImpl;
import com.bluage.documentation.service.standardfeatures.application.datagrid.uc38_selectedlist.ServicePlayer38;
import com.netfective.bluage.core.exception.ApplicationException;
import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;
import com.netfective.bluage.core.web.IForm;

/**
 * Class : UC38_DisplaySelectedListController
 */
@Controller
@Scope("prototype")
@SessionAttributes("uC38_DisplaySelectedListForm")
@RequestMapping(value= "/presentation/standardfeatures/application/datagrid/uc38_selectedlist")
public class UC38_DisplaySelectedListController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC38_DisplaySelectedListController.class);
	
	//Current form name
	private static final String U_C38__DISPLAY_SELECTED_LIST_FORM = "uC38_DisplaySelectedListForm";

	//CONSTANT: teams
	private static final String TEAMS = "teams";
							//CONSTANT: allPlayers
	private static final String ALL_PLAYERS = "allPlayers";
	//CONSTANT: playerToUpdate
	private static final String PLAYER_TO_UPDATE = "playerToUpdate";
	
	/**
	 * Property:customDateEditorsUC38_DisplaySelectedListController
	 */
	final private Map<String,CustomDateEditor> customDateEditorsUC38_DisplaySelectedListController = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:uC38_DisplaySelectedListValidator
	 */
	final private UC38_DisplaySelectedListValidator uC38_DisplaySelectedListValidator = new UC38_DisplaySelectedListValidator();
	
	/**
	 * Service declaration : servicePlayer38.
	 */
	@Autowired
	private ServicePlayer38 servicePlayer38;
	
	/**
	 * Generic Finder : team38DAOFinderImpl.
	 */
	@Autowired
	private Team38DAOFinderImpl team38DAOFinderImpl;
	
	
	// Initialise all the instances for UC38_DisplaySelectedListController
		// Initialize the instance teams for the state lnk_display
		private List teams; // Initialize the instance teams for UC38_DisplaySelectedListController
										// Declare the instance allPlayers
		private List allPlayers;
		// Declare the instance playerToUpdate
		private Player38BO playerToUpdate;
			/**
	 * Operation : lnk_update
 	 * @param model : 
 	 * @param uC38_DisplaySelectedList : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC38_DisplaySelectedList/lnk_update.html",method = RequestMethod.POST)
	public String lnk_update(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC38_DisplaySelectedListForm") UC38_DisplaySelectedListForm  uC38_DisplaySelectedListForm, final BindingResult bindingResult ,final @RequestParam(ID_REQUEST_PARAM) Integer index){
		
		
		
		LOGGER.info("Begin method : lnk_update"); 
		infoLogger(uC38_DisplaySelectedListForm); 
		uC38_DisplaySelectedListForm.setAlwaysCallPreControllers(false); 
		// initialization for lnk_update
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state lnk_update if the validation fail, you will be forward to the screen : "/presentation/standardfeatures/application/datagrid/uc38_selectedlist/UC38_DisplaySelectedList"
		if(errorsMessages(request,response,uC38_DisplaySelectedListForm,bindingResult,"playerToUpdate", uC38_DisplaySelectedListValidator, customDateEditorsUC38_DisplaySelectedListController)){ 
			return "/presentation/standardfeatures/application/datagrid/uc38_selectedlist/UC38_DisplaySelectedList"; 
		}
		// Updating selected row for the state lnk_update 
		uC38_DisplaySelectedListForm.setSelectedRow(-1); //Set selected row for lnk_update 
		// Updating selected table for the state lnk_update 
		uC38_DisplaySelectedListForm.setSelectedTab(null); //reset selected row for lnk_update 

										 callUpdatePlayer(request,uC38_DisplaySelectedListForm , index);
			
 callLoadPlayers(request,uC38_DisplaySelectedListForm );
						// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.application.datagrid.uc38_selectedlist.UC38_DisplaySelectedListForm or not from lnk_update
			// Populate the destination form with all the instances returned from lnk_update.
			final IForm uC38_DisplaySelectedListForm2 = populateDestinationForm(request.getSession(), uC38_DisplaySelectedListForm); 
					
			// Add the form to the model.
			model.addAttribute("uC38_DisplaySelectedListForm", uC38_DisplaySelectedListForm2); 
			
			request.getSession().setAttribute("uC38_DisplaySelectedListForm", uC38_DisplaySelectedListForm2);
			
			// "OK" CASE => destination screen path from lnk_update
			LOGGER.info("Go to the screen 'UC38_DisplaySelectedList'.");
			// Redirect (PRG) from lnk_update
			destinationPath =  "redirect:/presentation/standardfeatures/application/datagrid/uc38_selectedlist/UC38_DisplaySelectedList.html";
						
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_update 
	}
	
		/**
	 * Operation : lnk_edit
 	 * @param model : The model
 	 * @param uC38_DisplaySelectedList : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC38_DisplaySelectedList/lnk_edit.html")
	public String lnk_edit(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC38_DisplaySelectedListForm") UC38_DisplaySelectedListForm  uC38_DisplaySelectedListForm, BindingResult bindingResult, final @RequestParam(ID_REQUEST_PARAM) Integer index){
		uC38_DisplaySelectedListForm.setSelectedRow(index);	
		String tableId = request.getParameter(TAB_REQUEST_PARAM);
		uC38_DisplaySelectedListForm.setSelectedTab(tableId);
		
		Player38BO playerToUpdate = (Player38BO) uC38_DisplaySelectedListForm.getAllPlayers().get(index);
		uC38_DisplaySelectedListForm.setPlayerToUpdate(playerToUpdate);
		return "/presentation/standardfeatures/application/datagrid/uc38_selectedlist/UC38_DisplaySelectedList";
	}
	/**
	 * Operation : btn_cancel
 	 * @param model : 
 	 * @param uC38_DisplaySelectedList : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC38_DisplaySelectedList/btn_cancel.html")
	public String btn_cancel(final HttpServletRequest request,final HttpServletResponse response, final Model model,final @ModelAttribute("uC38_DisplaySelectedListForm") UC38_DisplaySelectedListForm  uC38_DisplaySelectedListForm, BindingResult bindingResult){
		uC38_DisplaySelectedListForm.setSelectedRow(-1);
		uC38_DisplaySelectedListForm.setSelectedTab(null);
		uC38_DisplaySelectedListForm.setPlayerToUpdate(null);
		return "redirect:/presentation/standardfeatures/application/datagrid/uc38_selectedlist/UC38_DisplaySelectedList.html";
	}

	/**
	* This method initialise the form : UC38_DisplaySelectedListForm 
	* @return UC38_DisplaySelectedListForm
	*/
	@ModelAttribute("UC38_DisplaySelectedListFormInit")
	public void initUC38_DisplaySelectedListForm(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, U_C38__DISPLAY_SELECTED_LIST_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method UC38_DisplaySelectedListForm."); //for btn_cancel 
		}
		UC38_DisplaySelectedListForm uC38_DisplaySelectedListForm;
	
		if(request.getSession().getAttribute(U_C38__DISPLAY_SELECTED_LIST_FORM) != null){
			uC38_DisplaySelectedListForm = (UC38_DisplaySelectedListForm)request.getSession().getAttribute(U_C38__DISPLAY_SELECTED_LIST_FORM);
		} else {
			uC38_DisplaySelectedListForm = new UC38_DisplaySelectedListForm();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form UC38_DisplaySelectedListForm.");
		}
		uC38_DisplaySelectedListForm = (UC38_DisplaySelectedListForm)populateDestinationForm(request.getSession(), uC38_DisplaySelectedListForm);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  UC38_DisplaySelectedListForm.");
		}
		model.addAttribute(U_C38__DISPLAY_SELECTED_LIST_FORM, uC38_DisplaySelectedListForm);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param UC38_DisplaySelectedListForm : The sceen form.
	 */
	@RequestMapping(value = "/UC38_DisplaySelectedList.html" ,method = RequestMethod.GET)
	public void prepareUC38_DisplaySelectedList(final HttpServletRequest request,final  Model model,final  @ModelAttribute("UC38_DisplaySelectedListFormInit") UC38_DisplaySelectedListForm uC38_DisplaySelectedListForm){
		
		UC38_DisplaySelectedListForm currentUC38_DisplaySelectedListForm = uC38_DisplaySelectedListForm;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(U_C38__DISPLAY_SELECTED_LIST_FORM) == null){
			if(currentUC38_DisplaySelectedListForm!=null){
				request.getSession().setAttribute(U_C38__DISPLAY_SELECTED_LIST_FORM, currentUC38_DisplaySelectedListForm);
			}else {
				currentUC38_DisplaySelectedListForm = new UC38_DisplaySelectedListForm();
				request.getSession().setAttribute(U_C38__DISPLAY_SELECTED_LIST_FORM, currentUC38_DisplaySelectedListForm);	
			}
		} else {
			currentUC38_DisplaySelectedListForm = (UC38_DisplaySelectedListForm) request.getSession().getAttribute(U_C38__DISPLAY_SELECTED_LIST_FORM);
		}

		try {
			List teams = team38DAOFinderImpl.findAll();
			put(request.getSession(), TEAMS, teams);
		} catch (ApplicationException e) {
			errorLogger("An error while calling the generic finder for the instance : teams.",e);
		}
				currentUC38_DisplaySelectedListForm = (UC38_DisplaySelectedListForm)populateDestinationForm(request.getSession(), currentUC38_DisplaySelectedListForm);
		// Call all the Precontroller.
	currentUC38_DisplaySelectedListForm.setAlwaysCallPreControllers(true);
		model.addAttribute(U_C38__DISPLAY_SELECTED_LIST_FORM, currentUC38_DisplaySelectedListForm);
		request.getSession().setAttribute(U_C38__DISPLAY_SELECTED_LIST_FORM, currentUC38_DisplaySelectedListForm);
		
	}
	
			/**
	 * method callLoadPlayers
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callLoadPlayers(HttpServletRequest request,IForm form) {
		allPlayers = (List)get(request.getSession(),form, "allPlayers");  
										 
					try {
				// executing LoadPlayers in btn_cancel
				allPlayers = 	servicePlayer38.loadPlayers38(
	);  
 
				put(request.getSession(), ALL_PLAYERS,allPlayers);
								// processing variables LoadPlayers in btn_cancel

			} catch (ApplicationException e) { 
				// error handling for operation loadPlayers38 called LoadPlayers
				errorLogger("An error occured during the execution of the operation : loadPlayers38",e); 
		
			}
	}
									/**
	 * method callUpdatePlayer
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 * @param index Integer the current index 
	 */
	private void callUpdatePlayer(HttpServletRequest request,IForm form,Integer index) {
					if(index!=null){  
			 playerToUpdate = (Player38BO)((List)get(request.getSession(),form, "allPlayers")).get(index);  
		} else {
			 playerToUpdate= null; 
		}
				 
					try {
				// executing UpdatePlayer in btn_cancel
	servicePlayer38.updatePlayer(
			playerToUpdate
			);  

								// processing variables UpdatePlayer in btn_cancel
				put(request.getSession(), PLAYER_TO_UPDATE,playerToUpdate); 
			
			} catch (ApplicationException e) { 
				// error handling for operation updatePlayer called UpdatePlayer
				errorLogger("An error occured during the execution of the operation : updatePlayer",e); 
		
			}
	}
		/**
	* This method Binder.
	* @param binder : 
	*		 WebDataBinder
	*/
	@InitBinder
	protected void initBinder(final WebDataBinder binder){
			customDateEditorsUC38_DisplaySelectedListController.put("allPlayers.dateOfBirth", new CustomDateEditor(new SimpleDateFormat("MM/dd/yyyy"), true, "Invalid format."));
			binder.registerCustomEditor(Date.class, "allPlayers.dateOfBirth", customDateEditorsUC38_DisplaySelectedListController.get("allPlayers.dateOfBirth"));
	}
	
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("UC38_DisplaySelectedListController [ ");
			strBToS.append(TEAMS);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(teams);
			strBToS.append(END_TO_STRING_DELIMITER);
									strBToS.append(ALL_PLAYERS);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(allPlayers);
			strBToS.append(END_TO_STRING_DELIMITER);
			strBToS.append(PLAYER_TO_UPDATE);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(playerToUpdate);
			strBToS.append(END_TO_STRING_DELIMITER);
		// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(uC38_DisplaySelectedListValidator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsUC38_DisplaySelectedListController!=null); 
		return strBToS.toString();
	}
}
