--<ScriptOptions statementTerminator=";"/>

ALTER TABLE player59 DROP CONSTRAINT player59_positionc;

ALTER TABLE player59 DROP CONSTRAINT player59_secondary_position_fk;

ALTER TABLE player59 DROP CONSTRAINT player59_pkey;

ALTER TABLE position59 DROP CONSTRAINT position59_pkey;

DROP TABLE player59;

DROP TABLE position59;

CREATE TABLE player59 (
		id INT8 NOT NULL,
		version INT4 NOT NULL,
		first_name VARCHAR(1024) NOT NULL,
		last_name VARCHAR(30),
		nickname VARCHAR(1024),
		date_of_birth DATE,
		value FLOAT4,
		weight INT4,
		height INT4,
		rookie BOOL,
		position VARCHAR(1024) NOT NULL,
		secondary_position_fk VARCHAR(1024) NOT NULL,
		surname VARCHAR(1024)
	);

CREATE TABLE position59 (
		code VARCHAR(1024) NOT NULL,
		version INT4 NOT NULL,
		name VARCHAR(1024)
	);
	
INSERT INTO position59 (code, version, name) VALUES ('C', 0, 'Center');
INSERT INTO position59 (code, version, name) VALUES ('PF', 0, 'Power Forward');
INSERT INTO position59 (code, version, name) VALUES ('PG', 0, 'Point Guard');
INSERT INTO position59 (code, version, name) VALUES ('SF', 0, 'Small Forward');
INSERT INTO position59 (code, version, name) VALUES ('SG', 0, 'Shooting Guard');

INSERT INTO player59 (id, version, first_name, last_name, nickname, date_of_birth, value, weight, height, rookie, "position", secondary_position_fk, surname) VALUES (202, 0, 'Elbert', 'Cole', 'Elb', '1965-04-07', 3.95, 100, 171, true, 'PF', 'PF', NULL);
INSERT INTO player59 (id, version, first_name, last_name, nickname, date_of_birth, value, weight, height, rookie, "position", secondary_position_fk, surname) VALUES (203, 0, 'Abel', 'Dennis', 'Abel', '1980-04-16', 30.51, 105, 208, true, 'SF', 'PF', NULL);
INSERT INTO player59 (id, version, first_name, last_name, nickname, date_of_birth, value, weight, height, rookie, "position", secondary_position_fk, surname) VALUES (205, 0, 'Earl', 'Russell', 'Earl', '1981-12-15', 44.73, 60, 234, false, 'PF', 'PF', NULL);
INSERT INTO player59 (id, version, first_name, last_name, nickname, date_of_birth, value, weight, height, rookie, "position", secondary_position_fk, surname) VALUES (207, 0, 'Howard', 'Ramos', 'Ho', '1970-08-25', 10.76, 107, 199, false, 'SG', 'PF', NULL);
INSERT INTO player59 (id, version, first_name, last_name, nickname, date_of_birth, value, weight, height, rookie, "position", secondary_position_fk, surname) VALUES (208, 0, 'Allan', 'Meyer', 'Allan', '1969-02-14', 33.669998, 114, 229, false, 'PG', 'PF', NULL);
INSERT INTO player59 (id, version, first_name, last_name, nickname, date_of_birth, value, weight, height, rookie, "position", secondary_position_fk, surname) VALUES (211, 0, 'Keith', 'Barnett', 'Keith', '1976-11-10', 5.8099999, 66, 190, false, 'PF', 'PF', NULL);
INSERT INTO player59 (id, version, first_name, last_name, nickname, date_of_birth, value, weight, height, rookie, "position", secondary_position_fk, surname) VALUES (214, 0, 'Sylvester', 'Nash', 'Sylv', '1965-07-10', 37.73, 68, 184, true, 'PF', 'PF', NULL);
INSERT INTO player59 (id, version, first_name, last_name, nickname, date_of_birth, value, weight, height, rookie, "position", secondary_position_fk, surname) VALUES (215, 0, 'Michael', 'Hodges', 'Mich', '1988-04-01', 25.35, 158, 204, false, 'C', 'PF', NULL);
INSERT INTO player59 (id, version, first_name, last_name, nickname, date_of_birth, value, weight, height, rookie, "position", secondary_position_fk, surname) VALUES (216, 0, 'Jerome', 'Grant', 'Jems', '1982-06-28', 28.940001, 105, 205, true, 'SF', 'PF', NULL);
INSERT INTO player59 (id, version, first_name, last_name, nickname, date_of_birth, value, weight, height, rookie, "position", secondary_position_fk, surname) VALUES (217, 0, 'Woodrow', 'Carpenter', 'Wood', '1987-08-08', 41.759998, 69, 224, true, 'SG', 'PF', NULL);
INSERT INTO player59 (id, version, first_name, last_name, nickname, date_of_birth, value, weight, height, rookie, "position", secondary_position_fk, surname) VALUES (218, 0, 'Roderick', 'Robbins', 'Rod', '1975-07-29', 12.04, 115, 164, true, 'C', 'SG', NULL);
INSERT INTO player59 (id, version, first_name, last_name, nickname, date_of_birth, value, weight, height, rookie, "position", secondary_position_fk, surname) VALUES (220, 0, 'Cecil', 'Conner', 'Cecil', '1978-06-29', 39.709999, 160, 170, false, 'PF', 'SG', NULL);
INSERT INTO player59 (id, version, first_name, last_name, nickname, date_of_birth, value, weight, height, rookie, "position", secondary_position_fk, surname) VALUES (221, 0, 'Stephen', 'Lambert', 'Steph', '1977-03-14', 18.9, 107, 153, false, 'PG', 'SG', NULL);
INSERT INTO player59 (id, version, first_name, last_name, nickname, date_of_birth, value, weight, height, rookie, "position", secondary_position_fk, surname) VALUES (219, 0, 'Christian', 'Chavez', 'Chricri', '1983-05-17', 37.880001, 97, 209, true, 'SG', 'SG', NULL);
INSERT INTO player59 (id, version, first_name, last_name, nickname, date_of_birth, value, weight, height, rookie, "position", secondary_position_fk, surname) VALUES (209, 0, 'Ruben', 'Pope', 'Rub', '1985-05-01', 44.66, 86, 201, true, 'SG', 'PF', NULL);
INSERT INTO player59 (id, version, first_name, last_name, nickname, date_of_birth, value, weight, height, rookie, "position", secondary_position_fk, surname) VALUES (210, 0, 'Lester', 'Cross', 'Les', '1976-02-21', 30.98, 69, 214, true, 'SF', 'PF', NULL);
INSERT INTO player59 (id, version, first_name, last_name, nickname, date_of_birth, value, weight, height, rookie, "position", secondary_position_fk, surname) VALUES (212, 0, 'Ivan', 'Tate', 'Ivan', '1972-02-08', 48.490002, 143, 175, true, 'SG', 'PF', NULL);
INSERT INTO player59 (id, version, first_name, last_name, nickname, date_of_birth, value, weight, height, rookie, "position", secondary_position_fk, surname) VALUES (201, 0, 'Oliver', 'Frank', 'Oliv', '1968-10-31', 0.11, 116, 228, false, 'SF', 'PF', NULL);
INSERT INTO player59 (id, version, first_name, last_name, nickname, date_of_birth, value, weight, height, rookie, "position", secondary_position_fk, surname) VALUES (206, 0, 'Nick', 'Garner', 'Nick', '1992-07-27', 47.84, 87, 221, true, 'PF', 'SF', NULL);
INSERT INTO player59 (id, version, first_name, last_name, nickname, date_of_birth, value, weight, height, rookie, "position", secondary_position_fk, surname) VALUES (213, 0, 'Tommy', 'Wood', 'Tom', '1992-01-20', 13.93, 77, 173, true, 'PF', 'SF', NULL);



ALTER TABLE player59 ADD CONSTRAINT player59_pkey PRIMARY KEY (id);

ALTER TABLE position59 ADD CONSTRAINT position59_pkey PRIMARY KEY (code);

ALTER TABLE player59 ADD CONSTRAINT player59_positionc FOREIGN KEY (position)
	REFERENCES position59 (code);

ALTER TABLE player59 ADD CONSTRAINT player59_secondary_position_fk FOREIGN KEY (secondary_position_fk)
	REFERENCES position59 (code);

