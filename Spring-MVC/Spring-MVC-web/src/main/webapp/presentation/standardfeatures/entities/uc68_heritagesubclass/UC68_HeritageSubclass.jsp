<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file="/include.jsp"  %>
<html >
<head >
<title  > 
<fmt:message key="cUC68_HeritageSubclass_BLU_AGE_DOCUMENTATION"/>
</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"  > 
</meta>
<link href="<c:url value= "/presentation/_style/_style.css"/>" rel="stylesheet" type="text/css"  /> 
<link href="<c:url value= "/presentation/_include/jquery/rd_lib.css"/>" rel="stylesheet" type="text/css"  /> 
<script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/jquery.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/rd_setup.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/rd_lib.js"/>" space="preserve"  ></script><link rel="shortcut icon" href="<c:url value= "/presentation/_style/favicon.ico"/>"  /> 

</head>

<body class=" " > 
<div id="rd_ghost"  > 
<small  > 
<fmt:message key="cUC68_HeritageSubclass___CSS_DEACTIVATED"/>
</small>
</div>
<div id="rd_jghost"  > 
<small  > 
<fmt:message key="cUC68_HeritageSubclass___JAVASCRIPT_DEACTIVATED"/>
</small>
</div>
<div id="shape"  > 
<div class="ui-layout-center"  > 
<div class="east-north"  > 
	<form:form id="uC68_HeritageSubclass" commandName="uC68_HeritageSubclassForm"  enctype="application/x-www-form-urlencoded" method="post">
<h2 class="rd_first"  > 
<fmt:message key="cUC68_HeritageSubclass_STANDARD_FEATURES__ENTITIES_"/>
</h2>
<h4  > 
<fmt:message key="cUC68_HeritageSubclass_PLAYERS"/>
</h4>
<display:table uid="tab_players" pagesize="4"  sort="list" requestURI="" name="sessionScope.uC68_HeritageSubclassForm.allPlayers" class="rd_tabledata"  >
	<c:set   var="rownum">
		<%=(tab_players !=	null) ? new Integer(tab_players_rowNum.intValue() - 1) : new Integer(0) %>
	</c:set>
	<c:set   var="idRow">
		<%=(tab_players != null ) ? tab_players_rowNum.intValue() - 1 : -1 %>
	</c:set>
	<c:set   var="idTab">
		<%=(tab_players != null ) ? "tab_players" : null %>
	</c:set>
	<c:set var="tableHashId">
		<%= new org.displaytag.util.ParamEncoder("tab_players").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE) %>
	</c:set>
	<c:set var="tableHashId2">
		<%=request.getParameter(new org.displaytag.util.ParamEncoder("tab_players").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE)) %>
	</c:set>	
	
<display:column format=""  titleKey="cUC68_HeritageSubclass_NAME">
		<span id="dyn_lastname">
	<c:out value="${tab_players.lastName}" />
		</span>
	<fmt:message key="cUC68_HeritageSubclass______________________"/>
		<span id="dyn_firstname">
	<c:out value="${tab_players.firstName}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="cUC68_HeritageSubclass_DATE_OF_BIRTH">
<span id="dyn_dateofbirth">
	<format:formatDateValidator value="${tab_players.dateOfBirth}" pattern="MM/dd/yyyy" />
</span>
	</display:column>
</display:table>
<h4  > 
<fmt:message key="cUC68_HeritageSubclass_ROOKIE"/>
</h4>
<display:table uid="tab_rookie" pagesize="4"  sort="list" requestURI="" name="sessionScope.uC68_HeritageSubclassForm.allRookies" class="rd_tabledata"  >
	<c:set   var="rownum">
		<%=(tab_rookie !=	null) ? new Integer(tab_rookie_rowNum.intValue() - 1) : new Integer(0) %>
	</c:set>
	<c:set   var="idRow">
		<%=(tab_rookie != null ) ? tab_rookie_rowNum.intValue() - 1 : -1 %>
	</c:set>
	<c:set   var="idTab">
		<%=(tab_rookie != null ) ? "tab_rookie" : null %>
	</c:set>
	<c:set var="tableHashId">
		<%= new org.displaytag.util.ParamEncoder("tab_rookie").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE) %>
	</c:set>
	<c:set var="tableHashId2">
		<%=request.getParameter(new org.displaytag.util.ParamEncoder("tab_rookie").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE)) %>
	</c:set>	
	
<display:column format=""  titleKey="cUC68_HeritageSubclass_NAME_168">
		<span id="dyn_rookie_lastname">
	<c:out value="${tab_rookie.lastName}" />
		</span>
	<fmt:message key="cUC68_HeritageSubclass_______________________169"/>
		<span id="dyn_rookie_firstname">
	<c:out value="${tab_rookie.firstName}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="cUC68_HeritageSubclass_DATE_OF_BIRTH_170">
<span id="dyn_rookie_dateofbirth">
	<format:formatDateValidator value="${tab_rookie.dateOfBirth}" pattern="MM/dd/yyyy" />
</span>
	</display:column>
	
<display:column format=""  titleKey="cUC68_HeritageSubclass_UNIVERSITY">
		<span id="dyn_rookie_university">
	<c:out value="${tab_rookie.university}" />
		</span>
	</display:column>
</display:table>
<h4  > 
<fmt:message key="cUC68_HeritageSubclass_PROFESSIONAL"/>
</h4>
<display:table uid="tab_professionals" pagesize="4"  sort="list" requestURI="" name="sessionScope.uC68_HeritageSubclassForm.allProfessionals" class="rd_tabledata"  >
	<c:set   var="rownum">
		<%=(tab_professionals !=	null) ? new Integer(tab_professionals_rowNum.intValue() - 1) : new Integer(0) %>
	</c:set>
	<c:set   var="idRow">
		<%=(tab_professionals != null ) ? tab_professionals_rowNum.intValue() - 1 : -1 %>
	</c:set>
	<c:set   var="idTab">
		<%=(tab_professionals != null ) ? "tab_professionals" : null %>
	</c:set>
	<c:set var="tableHashId">
		<%= new org.displaytag.util.ParamEncoder("tab_professionals").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE) %>
	</c:set>
	<c:set var="tableHashId2">
		<%=request.getParameter(new org.displaytag.util.ParamEncoder("tab_professionals").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE)) %>
	</c:set>	
	
<display:column format=""  titleKey="cUC68_HeritageSubclass_NAME_171">
		<span id="dyn_pro_lastname">
	<c:out value="${tab_professionals.lastName}" />
		</span>
	<fmt:message key="cUC68_HeritageSubclass_______________________172"/>
		<span id="dyn_pro_firstname">
	<c:out value="${tab_professionals.firstName}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="cUC68_HeritageSubclass_DATE_OF_BIRTH_173">
<span id="dyn_pro_dateofbirth">
	<format:formatDateValidator value="${tab_professionals.dateOfBirth}" pattern="MM/dd/yyyy" />
</span>
	</display:column>
	
<display:column format=""  titleKey="cUC68_HeritageSubclass_SALARY">
		<span id="dyn_pro_salary">
	<c:out value="${tab_professionals.salary}" />
		</span>
	</display:column>
</display:table>
<div style="visibility:hidden;"  > 
<a id="lien_cache"onclick="javascript:btn_click_cExecuteUC68_HeritageSubclass(this.href);return false;"				href="<c:url value="/presentation/standardfeatures/entities/uc68_heritagesubclass/UC68_HeritageSubclass/lien_cache.html"/>"
							 shape="rect"  >									 
<fmt:message key="cUC68_HeritageSubclass_HOME"/>
</a>
</div>
	</form:form>
	<script language="javascript">
		function btn_click_cExecuteUC68_HeritageSubclass(url) {
			UC68_HeritageSubclassvar = document.getElementById('uC68_HeritageSubclass');
			UC68_HeritageSubclassvar.setAttribute('action', url);
			UC68_HeritageSubclassvar.submit();
		}
		function btn_click_cSetActionUC68_HeritageSubclass(url) {
			UC68_HeritageSubclassvar = document.getElementById('uC68_HeritageSubclass');
			UC68_HeritageSubclassvar.setAttribute('action', url);
		}
	</script>
</div>
<div class="east-center"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/entities/uc68_heritagesubclass/UC68_HeritageSubclasscmp_tabs" />
</div>
</div>
<div class="ui-layout-north"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/entities/uc68_heritagesubclass/UC68_HeritageSubclasscmp_header" />
</div>
<div class="ui-layout-south"  > 
<fmt:message key="cUC68_HeritageSubclass_______"/>
<span id="rd_jversion"  > 
<fmt:message key="cUC68_HeritageSubclass_BLU_AGE_DOCUMENTATION_174"/>
</span>
</div>
<div class="ui-layout-west"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/entities/uc68_heritagesubclass/UC68_HeritageSubclasscmp_treeview" />
</div>
</div>
</body>
</html>
