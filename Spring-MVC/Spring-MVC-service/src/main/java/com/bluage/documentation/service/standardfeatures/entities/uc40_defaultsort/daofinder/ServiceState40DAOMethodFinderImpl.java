/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.service.standardfeatures.entities.uc40_defaultsort.daofinder;


import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.Logger;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateTemplate;

import com.bluage.documentation.business.standardfeatures.entities.uc40_defaultsort.bos.State40BO;
import com.netfective.bluage.core.exception.ApplicationException;


/**
 * This class represents query invocation methods implementation
 * for ServiceState40 service.
 *
 * @see com.bluage.documentation.service.standardfeatures.entities.uc40_defaultsort.ServiceState40
 */
 
@Named ("com.bluage.documentation.service.standardfeatures.entities.uc40_defaultsort.daofinder.ServiceState40DAOMethodFinderImpl")
public class ServiceState40DAOMethodFinderImpl {

	// LOGGER for ServiceState40DAOMethodFinderImpl
	private static final Logger LOGGER = Logger.getLogger(ServiceState40DAOMethodFinderImpl.class);
	
	/**
	* Hibernate Template (injected)
	*/
	@Inject
	private HibernateTemplate hibernateTemplate;

	
	/**
	 * setter for HibernateTemplate
	 * @param value : {@link HibernateTemplate}
	 */
	public void setHibernateTemplate( HibernateTemplate value )
	{
		hibernateTemplate = value;
	}
	

	/**    
	 * HQL operation.
	 * @param code {@link String}
		 * @return {@link State40BO}
	 */
	public State40BO searchState40ByCriteria(String code )
 throws ApplicationException {
Session session = hibernateTemplate.getSessionFactory().getCurrentSession();
			return 
				privateSearchState40ByCriteria(code, session);
	}

	
	/**    
	 * private delegation for HQL operation.
	 * @param code {@link String}
		 * @return {@link State40BO}
	 */
	private State40BO privateSearchState40ByCriteria(String code , Session session)
 throws ApplicationException {
			Query query = session.createQuery(" select state from State40BO state where state.code = :code " +
" ");
query.setParameter( "code", code, Hibernate.STRING);
														State40BO returnValue = null;
		try {
			Object result = query.uniqueResult();
			if( result != null ) {
				returnValue = ( com.bluage.documentation.business.standardfeatures.entities.uc40_defaultsort.bos.State40BO )result;
			}				
		} catch(HibernateException ex) {
			LOGGER.error("An error occured while excecuting the HQL operation : com.bluage.documentation.service.standardfeatures.entities.uc40_defaultsort.searchState40ByCriteria");
			throw new ApplicationException(ex);
		}
		return returnValue;
	}

	




}
