/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.business ;
 
//Import declaration 
import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Expression;
import org.springframework.dao.DataAccessException;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.netfective.bluage.core.business.IBusinessObject;
import com.netfective.bluage.core.exception.ApplicationException;
import com.netfective.bluage.core.persistence.IPersistentObjectDAOFinder;


/**
 * Implements all basic dao finder methods 
 * @see HibernateDaoSupport
 * @see IBusinessStringConstants
 * @see IPersistentObjectDAOFinder
 * @param <T>
 */
public abstract class AbstractDaoFinderImpl<T> extends HibernateDaoSupport implements IBusinessStringConstants,
		IPersistentObjectDAOFinder<T> {
			
	private static final Logger LOGGER = Logger.getLogger(AbstractDaoFinderImpl.class);

	private boolean clearSession;
	private boolean refreshSession;
	private boolean flushSession;
	

	/**
	* Returns the refBo. 
	* To be overridden by subclasses
	* @return Class<? extends T>
	*/
	public abstract Class<? extends T> getRefBo();


	/**
	* Returns the default order by instruction
	* @return String
	*/
	public abstract String getDefaultOrder();

	/**
	 * The hibernate session is cleared after a persistence operation, if
	 * clearSession is true.
	 * 
	 * @param value
	 */
	public final void setClearSession(final boolean value) {
		this.clearSession = value;
	}

	/**
	 * The hibernate session is cleared after a persistence operation, if
	 * clearSession is true.
	 * 
	 * @return boolean
	 */
	public final boolean isClearSession() {
		return this.clearSession;
	}

	/**
	 * The object is refreshed after a persistence operation, if refreshSession
	 * is true.
	 * 
	 * @param value
	 */
	public final void setRefreshSession(final boolean value) {
		this.refreshSession = value;
	}

	/**
	 * The object is refreshed after a persistence operation, if refreshSession
	 * is true.
	 * 
	 * @return boolean
	 */
	public final boolean isRefreshSession() {
		return this.refreshSession;
	}
	
		/**
	 * The hibernate session is flushed after a persistence operation, if
	 * flushSession is true.
	 * 
	 * @param value
	 */
	public final void setFlushSession(final boolean value) {
		this.flushSession = value;
	}

	/**
	 * The hibernate session is flushed after a persistence operation, if
	 * flushSession is true.
	 * 
	 * @return boolean
	 */
	public final boolean isFlushSession() {
		return this.flushSession;
	}
	

	/**
	 * <p> This method returns the object whose key is passed as parameter</p>
	 * @param key : primary key of object to search
	 * @return Object
	 * @throws ApplicationException
	 */
	public final Object findByPrimaryKey(final Object key) throws ApplicationException {
		this.logMethod(L_BEGIN_METHOD, L_FIND_BY_PRIMARY_KEY, L_KEY, key);
		Serializable pk = null;
		try {
			if (isClearSession()) {
				this.flushThenClear();
			}
			pk = (Serializable) key;
			Object obj = this
					.getHibernateTemplate()
					.get(this.getRefBo(),pk);
			if (isRefreshSession()) {
				this.getHibernateTemplate().refresh(obj);
			}
			this.logMethod(L_FIND_BY_PRIMARY_KEY, L_END_OF_METHOD, L_RETURNS, obj);
			return obj;
		} catch (DataAccessException ex) {
			this.logError(L_FIND_BY_PRIMARY_KEY, ex);
			throw new com.netfective.bluage.core.exception.UnexpectedException(
					ex);
		}
	}

	/**
	 * This method returns the resulting list of entities of the search by
	 * criteria based on the properties of the Business Object
	 * 
	 * @param properties
	 *            : {@link Hashtable} properties for the search action
	 * @return {@link List} of {@link IBusinessObject}
	 * @throws ApplicationException
	 */
	public final List<T> findByProperties(final Map<String,Object> properties)
			throws ApplicationException {
		this.logMethod(L_BEGIN_METHOD, L_FIND_BY_PROPERTIES, L_PROPERTIES, properties);
		try {
			final Criteria crit = this.getSession().createCriteria(this.getRefBo());
			final Iterator<Entry<String, Object>> it = properties.entrySet().iterator();
			while (it.hasNext()) {
				final Entry<String, Object> tmp = it.next();
				crit.add(Expression.eq(tmp.getKey(), tmp.getValue()));
			}
			final List<T> result = crit.list();
			this.logMethod(L_FIND_BY_PROPERTIES, L_END_OF_METHOD, L_RETURNS, result);
			return result;
		}
		catch (DataAccessException ex) {
			this.logError(L_FIND_BY_PROPERTIES, ex);
			throw new com.netfective.bluage.core.exception.UnexpectedException(
					ex);
		}
	}

	/**
	 * This method returns the resulting list of entities of the search without
	 * criteria
	 * 
	 * @return List : {@link List} of {@link IBusinessObject}
	 * @throws ApplicationException
	 */
	public final List<T> findAll() throws ApplicationException {

		final StringBuilder findAllQueryString = new StringBuilder();
		findAllQueryString.append(L_FROM).append(this.getRefBo().getName()).append(this.getDefaultOrder());
		logMethod(L_FIND_ALL, L_BEGIN_METHOD, "", null);
		try {
			final List<T> list = this
					.getHibernateTemplate()
					.find(findAllQueryString.toString());
			this.logMethod(L_FIND_ALL, L_END_OF_METHOD, L_RETURNS, list);
			return list;
		}
		catch (DataAccessException ex) {
			this.logError(L_FIND_ALL, ex);
			throw new com.netfective.bluage.core.exception.UnexpectedException(
					ex);
		}
	}

	/**
	 * Find Data for paginated query
	 * @param queryName
	 * @param startRecord
	 * @param endRecord
	 * @returns List<T>
	 * @see
	 * com.netfective.bluage.core.persistence.IPersistentObjectDAOFinder#findData( )
	 */
	public final List<T> findData(final String queryName,
			final int startRecord, final int endRecord)
			throws ApplicationException {
		
		this.logMethod(L_FIND_DATA, L_BEGIN_METHOD, "", null);
		Session session = null;
		try {
			session = this.getSession();
			final Query q = session.createQuery(queryName);
			q.setFirstResult(startRecord);
			q.setMaxResults(endRecord);
			final List<T> result =q.list();
			session.flush();
			return result;
		}
		catch (DataAccessException ex) {
			logError(L_FIND_DATA, ex);
			throw new com.netfective.bluage.core.exception.UnexpectedException(
					ex);
		}
	}

	/**
	 * Prepare result for paginated query
	 * @param startRecord
	 * @param endRecord
	 * @returns List<T>
	 * @throws ApplicationException
	 * @see
	 * com.netfective.bluage.core.persistence.IPersistentObjectDAOFinder#findAll( )
	 */
	public final List<T> prepareResult(final int startRecord,
			final int endRecord) throws ApplicationException {
		return findData(
				L_FROM+this.getRefBo().getName(),
				startRecord, endRecord);
	}
	
	/**
	 * Count total number of associated Business Object 
	 * @return int
	 * @see
	 * com.netfective.bluage.core.persistence.IPersistentObjectDAOFinder#findAll( )
	 */
	public final int count() {
		final StringBuilder query = new StringBuilder();
		query.append(L_SELECT_COUNT_STAR).append(L_FROM).append(this.getRefBo().getName());
		final Session session = this.getSession();
		final Long value = (Long) session
				.createQuery(query.toString())
				.uniqueResult();
		return value.intValue();
	}

	/**
	 * This method returns the resulting entity of the search by primary key
	 * 
	 * @param key
	 *            : {@llink Object} primary key used for the search action
	 * @return {@link Object} an {@link IBusinessObject}
	 * @throws ApplicationException
	 */
	public final Object findAndLock(final Object key) throws ApplicationException {
		this.logMethod(L_FIND_AND_LOCK, L_BEGIN_METHOD, L_KEY, key);
		Serializable pk = null;
		try {
			pk = (Serializable) key;
			final Object obj = this
					.getHibernateTemplate()
					.get(this.getRefBo(),pk, LockMode.UPGRADE);
			this.logMethod(L_FIND_AND_LOCK, L_END_OF_METHOD, L_RETURNS, obj);
			return obj;
		}
		catch (DataAccessException ex) {
			this.logError(L_FIND_AND_LOCK, ex);
			throw new com.netfective.bluage.core.exception.UnexpectedException(
					ex);
		}
	}

	
	/**
	 * Log end of method on exception
	 * 
	 * @param methodCallString
	 * @param ex
	 */
	private void logError(final String methodCallString,final Exception ex) {
		if (LOGGER.isDebugEnabled()) {
			final StringBuilder sb = new StringBuilder();
			sb.append(L_END_OF_METHOD).append(this.getClass().getName()).append('.')
					.append(methodCallString).append(L_WITH_ERROR);
			LOGGER.debug(sb.toString(), ex);
		}
	}
	
	/**
	 * Log method call
	 * @param methodCallString
	 * @param prefix
	 * @param paramDesc
	 * @param object
	 */
	private void logMethod(final String methodCallString, final String prefix, final String paramDesc,final Object object) {
		if (LOGGER.isDebugEnabled()) {
			final StringBuilder sb = new StringBuilder();
			sb.append(prefix).append(this.getClass().getName()).append('.')
					.append(methodCallString);
			LOGGER.debug(sb.toString());
			if (object != null) {
				LOGGER.debug(paramDesc + object);
			}
		}
	}
	
	/**
	 * Flush then clear session
	 */
	private void flushThenClear() {
		this.getHibernateTemplate().flush();
		this.getHibernateTemplate().clear();
	}
}

