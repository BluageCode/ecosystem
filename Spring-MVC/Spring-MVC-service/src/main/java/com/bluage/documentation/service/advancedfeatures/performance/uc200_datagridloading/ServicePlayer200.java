/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.service.advancedfeatures.performance.uc200_datagridloading;


import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import com.bluage.documentation.service.AbstractService;
import com.bluage.documentation.service.advancedfeatures.performance.uc200_datagridloading.daofinder.ServicePlayer200DAOMethodFinderImpl;
import com.netfective.bluage.core.exception.ApplicationException;

/**
 * Defines  application services for ServicePlayer200
  */
@Named("com.bluage.documentation.service.advancedfeatures.performance.uc200_datagridloading.ServicePlayer200")
public class ServicePlayer200  extends AbstractService{


			@Inject
		@Named ("com.bluage.documentation.service.advancedfeatures.performance.uc200_datagridloading.daofinder.ServicePlayer200DAOMethodFinder")
		private ServicePlayer200DAOMethodFinderImpl servicePlayer200Dao;
			


	/**
	 *  Default constructor 
	 */
	public ServicePlayer200() {
		super(); // Call super for default constructor : ServicePlayer200
	}
	



				
	/**    
	 * Operation HQL.
	 * @return {@link List}
	 */
	public List findPlayers1( )
 throws ApplicationException {
	
	return servicePlayer200Dao.findPlayers1();
	}

		
				
	/**    
	 * Operation HQL.
	 * @return {@link List}
	 */
	public List findPlayersN1( )
 throws ApplicationException {
	
	return servicePlayer200Dao.findPlayersN1();
	}

		
			
	/**    
	 * Process operation.
	 * @return {@link List}
	 */
	public List clear( )
 throws ApplicationException {

				return privateClear();
	}
	

	/**    
	 * Process operation.
	 * @return {@link List}
	 */
	private List privateClear( )
 throws ApplicationException {
				// Return Type declaration.
				List list = new java.util.ArrayList();
					// Central buffer node declaration for operation clear
			/* Executing : list = new ArrayList ()*/
list = new ArrayList ();

						return list;
		}

		}
