/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.exception.uc73_exception ;

// Import declaration.
// Java imports.
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.bluage.documentation.service.standardfeatures.exception.uc73_exception.ServicePlayer73;
import com.netfective.bluage.core.exception.ApplicationException;
import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;
import com.netfective.bluage.core.web.IForm;

/**
 * Class : UC73_ExceptionController
 */
@Controller
@Scope("prototype")
@SessionAttributes("uC73_ExceptionForm")
@RequestMapping(value= "/presentation/standardfeatures/exception/uc73_exception")
public class UC73_ExceptionController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC73_ExceptionController.class);
	
	//Current form name
	private static final String U_C73__EXCEPTION_FORM = "uC73_ExceptionForm";

	
	/**
	 * Property:customDateEditorsUC73_ExceptionController
	 */
	final private Map<String,CustomDateEditor> customDateEditorsUC73_ExceptionController = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:uC73_ExceptionValidator
	 */
	final private UC73_ExceptionValidator uC73_ExceptionValidator = new UC73_ExceptionValidator();
	
	/**
	 * Service declaration : servicePlayer73.
	 */
	@Autowired
	private ServicePlayer73 servicePlayer73;
	
	
	// Initialise all the instances for UC73_ExceptionController
					/**
	 * Operation : lnk_back
 	 * @param model : 
 	 * @param uC73_Exception : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC73_Exception/lnk_back.html",method = RequestMethod.GET)
	public String lnk_back(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC73_ExceptionForm") UC73_ExceptionForm  uC73_ExceptionForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : lnk_back"); 
		infoLogger(uC73_ExceptionForm); 
		uC73_ExceptionForm.setAlwaysCallPreControllers(false); 
		// initialization for lnk_back
		init();
		
		String destinationPath = null; 
		

	 callGoBack(request,uC73_ExceptionForm );
											// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.exception.uc73_exception.UC73_ManagePlayersForm or not from lnk_back
			final UC73_ManagePlayersForm uC73_ManagePlayersForm =  new UC73_ManagePlayersForm();
			// Populate the destination form with all the instances returned from lnk_back.
			final IForm uC73_ManagePlayersForm2 = populateDestinationForm(request.getSession(), uC73_ManagePlayersForm); 
					
			// Add the form to the model.
			model.addAttribute("uC73_ManagePlayersForm", uC73_ManagePlayersForm2); 
			
			request.getSession().setAttribute("uC73_ManagePlayersForm", uC73_ManagePlayersForm2);
			
			// "OK" CASE => destination screen path from lnk_back
			LOGGER.info("Go to the screen 'UC73_ManagePlayers'.");
			// Redirect (PRG) from lnk_back
			destinationPath =  "redirect:/presentation/standardfeatures/exception/uc73_exception/UC73_ManagePlayers.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_back 
	}
	
	
	/**
	* This method initialise the form : UC73_ExceptionForm 
	* @return UC73_ExceptionForm
	*/
	@ModelAttribute("UC73_ExceptionFormInit")
	public void initUC73_ExceptionForm(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, U_C73__EXCEPTION_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method UC73_ExceptionForm."); //for lnk_back 
		}
		UC73_ExceptionForm uC73_ExceptionForm;
	
		if(request.getSession().getAttribute(U_C73__EXCEPTION_FORM) != null){
			uC73_ExceptionForm = (UC73_ExceptionForm)request.getSession().getAttribute(U_C73__EXCEPTION_FORM);
		} else {
			uC73_ExceptionForm = new UC73_ExceptionForm();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form UC73_ExceptionForm.");
		}
		uC73_ExceptionForm = (UC73_ExceptionForm)populateDestinationForm(request.getSession(), uC73_ExceptionForm);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  UC73_ExceptionForm.");
		}
		model.addAttribute(U_C73__EXCEPTION_FORM, uC73_ExceptionForm);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param UC73_ExceptionForm : The sceen form.
	 */
	@RequestMapping(value = "/UC73_Exception.html" ,method = RequestMethod.GET)
	public void prepareUC73_Exception(final HttpServletRequest request,final  Model model,final  @ModelAttribute("UC73_ExceptionFormInit") UC73_ExceptionForm uC73_ExceptionForm){
		
		UC73_ExceptionForm currentUC73_ExceptionForm = uC73_ExceptionForm;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(U_C73__EXCEPTION_FORM) == null){
			if(currentUC73_ExceptionForm!=null){
				request.getSession().setAttribute(U_C73__EXCEPTION_FORM, currentUC73_ExceptionForm);
			}else {
				currentUC73_ExceptionForm = new UC73_ExceptionForm();
				request.getSession().setAttribute(U_C73__EXCEPTION_FORM, currentUC73_ExceptionForm);	
			}
		} else {
			currentUC73_ExceptionForm = (UC73_ExceptionForm) request.getSession().getAttribute(U_C73__EXCEPTION_FORM);
		}

		currentUC73_ExceptionForm = (UC73_ExceptionForm)populateDestinationForm(request.getSession(), currentUC73_ExceptionForm);
		// Call all the Precontroller.
	currentUC73_ExceptionForm.setAlwaysCallPreControllers(true);
		model.addAttribute(U_C73__EXCEPTION_FORM, currentUC73_ExceptionForm);
		request.getSession().setAttribute(U_C73__EXCEPTION_FORM, currentUC73_ExceptionForm);
		
	}
	
	/**
	 * method callGoBack
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callGoBack(HttpServletRequest request,IForm form) {
					try {
				// executing GoBack in lnk_back
	servicePlayer73.back(
	);  

								// processing variables GoBack in lnk_back

			} catch (ApplicationException e) { 
				// error handling for operation back called GoBack
				errorLogger("An error occured during the execution of the operation : back",e); 
		
			}
	}
		
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("UC73_ExceptionController [ ");
		// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(uC73_ExceptionValidator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsUC73_ExceptionController!=null); 
		return strBToS.toString();
	}
}
