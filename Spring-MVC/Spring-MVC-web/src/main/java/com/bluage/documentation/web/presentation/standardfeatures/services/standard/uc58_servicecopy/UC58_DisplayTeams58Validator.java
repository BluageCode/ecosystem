/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.services.standard.uc58_servicecopy ;


import org.apache.log4j.Logger;
import org.springframework.validation.Errors;

import com.netfective.bluage.core.validator.AbstractValidator;
/**
* Class :UC58_DisplayTeams58Validator
*/
public class UC58_DisplayTeams58Validator extends AbstractValidator{
	
	// LOGGER for the class UC58_DisplayTeams58Validator
	private static final Logger LOGGER = Logger.getLogger( UC58_DisplayTeams58Validator.class);
	
	
	/**
	* Operation validate for UC58_DisplayTeams58Form
	* @param obj : the current form (UC58_DisplayTeams58Form)
	* @param errors : The spring errors to return for the form UC58_DisplayTeams58Form
	*/
	public void validate(final Object obj,final Errors errors){
		LOGGER.info("Beginning method : validate"); // UC58_DisplayTeams58Validator
		UC58_DisplayTeams58Form cUC58_DisplayTeams58Form = (UC58_DisplayTeams58Form)obj; // UC58_DisplayTeams58Validator
		LOGGER.info("Ending method : validate the form "+ cUC58_DisplayTeams58Form.getClass().getName()); // UC58_DisplayTeams58Validator
	}

	/**
	* Method to implements to use spring validators (UC58_DisplayTeams58Form)
	* @param aClass : Class for the form UC58_DisplayTeams58Form
	* @return boolean
	*/
	public boolean supports(final Class aClass){
		return (new UC58_DisplayTeams58Form()).getClass().equals(aClass); // UC58_DisplayTeams58Validator
	}
}
