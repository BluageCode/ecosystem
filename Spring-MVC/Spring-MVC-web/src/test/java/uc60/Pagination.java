package uc60;

import org.junit.Test;
import test.common.AbstractBADocTestCase;

public class Pagination extends AbstractBADocTestCase {

	@Override
	public  String getName(){
		return "UC-60";
	}	


	@Test
	public void pagination() throws Exception {
		// On teste l'UC 60 - Standard Features/Entities - MultiDomain
		selenium.open("");	
		selenium.click("link=Expand All");
		selenium.waitForPageToLoad("30000");
		selenium.click("link=UC60 - MultiDomain");
		verifyTrue(selenium.isElementPresent("//h2[contains(text(),\"Standard Features > Entities > UC60 > MultiDomain\")]"));
		// On effectue les tests de pagination.
		// On teste les liens.
		// On va sur la page 2.
		selenium.click("xpath=(//a)[5]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie qu'on est sur la page 2.
		assertTrue(selenium.isElementPresent("//a/b[contains(text(),\"2\")]|//strong[contains(text(),\"2\")]"));
		// On va tester le lien fast next.
//		selenium.click("//a[img[@alt=\"Fast next\"]]");
//		selenium.waitForPageToLoad("30000");
//		// Les sauts de pages sont de 3 pages. On v�rifie que l'on est bien sur la 5eme page.
//		assertTrue(selenium.isElementPresent("//a/b[contains(text(),\"4\")]|//strong[contains(text(),\"4\")]"));
//		// On teste le lien fast prev.
//		selenium.click("//a[img[@alt=\"Fast prev\"]]");
//		selenium.waitForPageToLoad("30000");
//		// Les sauts de pages sont de 3 pages. On v�rife que l'on est bien sur la 2eme page.
//		assertTrue(selenium.isElementPresent("//a/b[contains(text(),\"1\")]|//strong[contains(text(),\"1\")]"));
		// On teste le lien Prev.
		selenium.click("xpath=(//a)[5]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie que l'on est sur la premi�re page.
		assertTrue(selenium.isElementPresent("//a/b[contains(text(),\"1\")]|//strong[contains(text(),\"1\")]"));
		// On teste le lien Last.
		selenium.click("xpath=(//a)[9]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//strong[contains(text(),\"4\")]|//a/b[contains(text(),\"4\")]"));
		// On teste le lien First
		selenium.click("xpath=(//a)[6]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//strong[contains(text(),\"1\")]|//a/b[contains(text(),\"1\")]"));
	}

	
}
