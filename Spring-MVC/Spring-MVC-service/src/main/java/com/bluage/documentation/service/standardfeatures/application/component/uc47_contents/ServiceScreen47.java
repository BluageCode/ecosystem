/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.service.standardfeatures.application.component.uc47_contents;


import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import com.bluage.documentation.business.standardfeatures.application.component.uc47_contents.bos.Screen47BO;
import com.bluage.documentation.service.AbstractService;
import com.netfective.bluage.core.exception.ApplicationException;

/**
 * Defines  application services for ServiceScreen47
  */
@Named("com.bluage.documentation.service.standardfeatures.application.component.uc47_contents.ServiceScreen47")
public class ServiceScreen47  extends AbstractService{


					@Inject
		@Named ("com.bluage.documentation.service.standardfeatures.application.component.uc47_contents.ServicePlayer47")
		private ServicePlayer47  instanceServicePlayer47;
									


	/**
	 *  Default constructor 
	 */
	public ServiceScreen47() {
		super(); // Call super for default constructor : ServiceScreen47
	}
	



			
	/**    
	 * Process operation.
	 * @return {@link Screen47BO}
	 */
	public Screen47BO InitializeScreen47( )
 throws ApplicationException {

				return privateInitializeScreen47();
	}
	

	/**    
	 * Process operation.
	 * @return {@link Screen47BO}
	 */
	private Screen47BO privateInitializeScreen47( )
 throws ApplicationException {
				// Return Type declaration.
				Screen47BO screen = new Screen47BO();
					// Central buffer node declaration for operation InitializeScreen47
			List	 allPlayers =  new java.util.ArrayList();
						// Calling an operation : Find all players
allPlayers = instanceServicePlayer47.player47FindAll();
// Calling an operation : set All players
screen.setAllPlayers(allPlayers);

						return screen;
		}

		
			
	/**    
	 * Process operation.
	 * @return {@link Screen47BO}
	 */
	public Screen47BO displayTableScreen47( )
 throws ApplicationException {

				return privateDisplayTableScreen47();
	}
	

	/**    
	 * Process operation.
	 * @return {@link Screen47BO}
	 */
	private Screen47BO privateDisplayTableScreen47( )
 throws ApplicationException {
				// Return Type declaration.
				Screen47BO displayTable = new Screen47BO();
					// Central buffer node declaration for operation displayTableScreen47
			// Calling an operation : 
displayTable.setTable(Boolean.TRUE);

						return displayTable;
		}

		}
