/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.application.navigation.uc29_globalscope.package_two ;


import org.apache.log4j.Logger;
import org.springframework.validation.Errors;

import com.netfective.bluage.core.validator.AbstractValidator;
/**
* Class :UC29_StadiumsValidator
*/
public class UC29_StadiumsValidator extends AbstractValidator{
	
	// LOGGER for the class UC29_StadiumsValidator
	private static final Logger LOGGER = Logger.getLogger( UC29_StadiumsValidator.class);
	
	
	/**
	* Operation validate for UC29_StadiumsForm
	* @param obj : the current form (UC29_StadiumsForm)
	* @param errors : The spring errors to return for the form UC29_StadiumsForm
	*/
	public void validate(final Object obj,final Errors errors){
		LOGGER.info("Beginning method : validate"); // UC29_StadiumsValidator
		UC29_StadiumsForm cUC29_StadiumsForm = (UC29_StadiumsForm)obj; // UC29_StadiumsValidator
		LOGGER.info("Ending method : validate the form "+ cUC29_StadiumsForm.getClass().getName()); // UC29_StadiumsValidator
	}

	/**
	* Method to implements to use spring validators (UC29_StadiumsForm)
	* @param aClass : Class for the form UC29_StadiumsForm
	* @return boolean
	*/
	public boolean supports(final Class aClass){
		return (new UC29_StadiumsForm()).getClass().equals(aClass); // UC29_StadiumsValidator
	}
}
