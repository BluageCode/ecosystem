/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.application.datagrid.uc38_selectedlist ;

// Import declaration.
// Java imports.
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.bluage.documentation.service.standardfeatures.application.datagrid.uc38_selectedlist.ServicePlayer38;
import com.netfective.bluage.core.exception.ApplicationException;
import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;
import com.netfective.bluage.core.web.IForm;

/**
 * Class : UC38_SelectedListController
 */
@Controller
@Scope("prototype")
@SessionAttributes("uC38_SelectedListForm")
@RequestMapping(value= "/presentation/standardfeatures/application/datagrid/uc38_selectedlist")
public class UC38_SelectedListController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC38_SelectedListController.class);
	
	//Current form name
	private static final String U_C38__SELECTED_LIST_FORM = "uC38_SelectedListForm";

	//CONSTANT: allPlayers
	private static final String ALL_PLAYERS = "allPlayers";
	
	/**
	 * Property:customDateEditorsUC38_SelectedListController
	 */
	final private Map<String,CustomDateEditor> customDateEditorsUC38_SelectedListController = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:uC38_SelectedListValidator
	 */
	final private UC38_SelectedListValidator uC38_SelectedListValidator = new UC38_SelectedListValidator();
	
	/**
	 * Service declaration : servicePlayer38.
	 */
	@Autowired
	private ServicePlayer38 servicePlayer38;
	
	
	// Initialise all the instances for UC38_SelectedListController
		// Declare the instance allPlayers
		private List allPlayers;
			/**
	 * Operation : lnk_display
 	 * @param model : 
 	 * @param uC38_SelectedList : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC38_SelectedList/lnk_display.html",method = RequestMethod.POST)
	public String lnk_display(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC38_SelectedListForm") UC38_SelectedListForm  uC38_SelectedListForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : lnk_display"); 
		infoLogger(uC38_SelectedListForm); 
		uC38_SelectedListForm.setAlwaysCallPreControllers(false); 
		// initialization for lnk_display
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state lnk_display if the validation fail, you will be forward to the screen : "/presentation/standardfeatures/application/datagrid/uc38_selectedlist/UC38_SelectedList"
		if(errorsMessages(request,response,uC38_SelectedListForm,bindingResult,"", uC38_SelectedListValidator, customDateEditorsUC38_SelectedListController)){ 
			return "/presentation/standardfeatures/application/datagrid/uc38_selectedlist/UC38_SelectedList"; 
		}

	 callLoadPlayers(request,uC38_SelectedListForm );
											// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.application.datagrid.uc38_selectedlist.UC38_DisplaySelectedListForm or not from lnk_display
			final UC38_DisplaySelectedListForm uC38_DisplaySelectedListForm =  new UC38_DisplaySelectedListForm();
			// Populate the destination form with all the instances returned from lnk_display.
			final IForm uC38_DisplaySelectedListForm2 = populateDestinationForm(request.getSession(), uC38_DisplaySelectedListForm); 
					
			// Add the form to the model.
			model.addAttribute("uC38_DisplaySelectedListForm", uC38_DisplaySelectedListForm2); 
			
			request.getSession().setAttribute("uC38_DisplaySelectedListForm", uC38_DisplaySelectedListForm2);
			
			// "OK" CASE => destination screen path from lnk_display
			LOGGER.info("Go to the screen 'UC38_DisplaySelectedList'.");
			// Redirect (PRG) from lnk_display
			destinationPath =  "redirect:/presentation/standardfeatures/application/datagrid/uc38_selectedlist/UC38_DisplaySelectedList.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_display 
	}
	
	
	/**
	* This method initialise the form : UC38_SelectedListForm 
	* @return UC38_SelectedListForm
	*/
	@ModelAttribute("UC38_SelectedListFormInit")
	public void initUC38_SelectedListForm(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, U_C38__SELECTED_LIST_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method UC38_SelectedListForm."); //for lnk_display 
		}
		UC38_SelectedListForm uC38_SelectedListForm;
	
		if(request.getSession().getAttribute(U_C38__SELECTED_LIST_FORM) != null){
			uC38_SelectedListForm = (UC38_SelectedListForm)request.getSession().getAttribute(U_C38__SELECTED_LIST_FORM);
		} else {
			uC38_SelectedListForm = new UC38_SelectedListForm();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form UC38_SelectedListForm.");
		}
		uC38_SelectedListForm = (UC38_SelectedListForm)populateDestinationForm(request.getSession(), uC38_SelectedListForm);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  UC38_SelectedListForm.");
		}
		model.addAttribute(U_C38__SELECTED_LIST_FORM, uC38_SelectedListForm);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param UC38_SelectedListForm : The sceen form.
	 */
	@RequestMapping(value = "/UC38_SelectedList.html" ,method = RequestMethod.GET)
	public void prepareUC38_SelectedList(final HttpServletRequest request,final  Model model,final  @ModelAttribute("UC38_SelectedListFormInit") UC38_SelectedListForm uC38_SelectedListForm){
		
		UC38_SelectedListForm currentUC38_SelectedListForm = uC38_SelectedListForm;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(U_C38__SELECTED_LIST_FORM) == null){
			if(currentUC38_SelectedListForm!=null){
				request.getSession().setAttribute(U_C38__SELECTED_LIST_FORM, currentUC38_SelectedListForm);
			}else {
				currentUC38_SelectedListForm = new UC38_SelectedListForm();
				request.getSession().setAttribute(U_C38__SELECTED_LIST_FORM, currentUC38_SelectedListForm);	
			}
		} else {
			currentUC38_SelectedListForm = (UC38_SelectedListForm) request.getSession().getAttribute(U_C38__SELECTED_LIST_FORM);
		}

		currentUC38_SelectedListForm = (UC38_SelectedListForm)populateDestinationForm(request.getSession(), currentUC38_SelectedListForm);
		// Call all the Precontroller.
	currentUC38_SelectedListForm.setAlwaysCallPreControllers(true);
		model.addAttribute(U_C38__SELECTED_LIST_FORM, currentUC38_SelectedListForm);
		request.getSession().setAttribute(U_C38__SELECTED_LIST_FORM, currentUC38_SelectedListForm);
		
	}
	
	/**
	 * method callLoadPlayers
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callLoadPlayers(HttpServletRequest request,IForm form) {
		allPlayers = (List)get(request.getSession(),form, "allPlayers");  
										 
					try {
				// executing LoadPlayers in lnk_display
				allPlayers = 	servicePlayer38.loadPlayers38(
	);  
 
				put(request.getSession(), ALL_PLAYERS,allPlayers);
								// processing variables LoadPlayers in lnk_display

			} catch (ApplicationException e) { 
				// error handling for operation loadPlayers38 called LoadPlayers
				errorLogger("An error occured during the execution of the operation : loadPlayers38",e); 
		
			}
	}
		
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("UC38_SelectedListController [ ");
			strBToS.append(ALL_PLAYERS);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(allPlayers);
			strBToS.append(END_TO_STRING_DELIMITER);
		// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(uC38_SelectedListValidator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsUC38_SelectedListController!=null); 
		return strBToS.toString();
	}
}
