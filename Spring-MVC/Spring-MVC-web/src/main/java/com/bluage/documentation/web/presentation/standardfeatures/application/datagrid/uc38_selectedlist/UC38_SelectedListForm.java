/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.application.datagrid.uc38_selectedlist;


import java.io.Serializable;
import java.util.List;

import com.netfective.bluage.core.web.AbstractForm;
import com.netfective.bluage.core.web.IForm;

/**
* Class : UC38_SelectedListForm
*/
public class UC38_SelectedListForm extends AbstractForm implements Serializable, IForm {
	
	/** Constant : serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	//CONSTANT : allPlayers
	private static final String ALL_PLAYERS = "allPlayers";
	/**
	 * 	Property: allPlayers 
	 */
	private List allPlayers;
/**
	 * Default constructor : UC38_SelectedListForm
	 */
	public UC38_SelectedListForm() {
		super();
		// Initialize : allPlayers
		this.allPlayers = null;
			this.setAlwaysCallPreControllers(true);
	}
	
	/**
	 * 	Getter : allPlayers 
	 * 	@return : Return the allPlayers instance.
	 */
	public List getAllPlayers(){
		return allPlayers; // For UC38_SelectedListForm
	}
	
	/**
	 * 	Setter : allPlayers 
	 *  @param allPlayersinstance : The instance to set.
	 */
	public void setAllPlayers(final List allPlayersinstance){
		this.allPlayers = allPlayersinstance;// For UC38_SelectedListForm
	}
/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UC38_SelectedListForm [ "+
ALL_PLAYERS +" = " + allPlayers + "]";
	}
	
	/**
	 * Method setInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public void setInstanceByInstanceName(final String instanceName, final Object instance) {
		// Set the instance AllPlayers.
		if(ALL_PLAYERS.equals(instanceName)){// For UC38_SelectedListForm
			this.setAllPlayers((List)instance); // For UC38_SelectedListForm
		}
			}
	
	/**
	 * Method getInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public Object getInstanceByInstanceName(final String instanceName){
		// Temporary object for tmpUC38_SelectedListForm.
		Object tmpUC38_SelectedListForm = null;
		
			
		// Get the instance AllPlayers for UC38_SelectedListForm.
		if(ALL_PLAYERS.equals(instanceName)){ // For UC38_SelectedListForm
			tmpUC38_SelectedListForm = this.getAllPlayers(); // For UC38_SelectedListForm
		}
		return tmpUC38_SelectedListForm;// For UC38_SelectedListForm
	}
	
	}
