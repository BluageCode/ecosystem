/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.netfective.bluage.core.validator ;

//Import declaration
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

/**
* Class : ValidatorsUtil
*/
public final class ValidatorsUtil  {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(ValidatorsUtil.class);

	/**
	 * private constructor for ValidatorsUtil
	 */
	private ValidatorsUtil() {

	}

	/**
	 * This method Validator
	 * 
	 * @param val
	 *            : Object
	 * @param comparToValue
	 *            : String
	 * @param oper
	 *            : String
	 * @return int
	 */
	public static int compareValidator(final Object val, final String comparToValue, final String oper) {
		int intToReturn = -1;
		String valToString = null;
		if (val instanceof Integer) {
			valToString = ((Integer) val).toString();
		} else {
			if (val instanceof Double) {
				valToString = ((Double) val).toString();
			} else {
				if (val instanceof Date) {
					valToString = ((Date) val).toString();
				} else {
					if (val instanceof String) {
						valToString = (String) val;
					}
				}
			}
		}
		if (null == oper || null == comparToValue || null == val) {
			return -1;
		}
		intToReturn = compareEqual(comparToValue, oper, valToString);
		// If the value is NotEqual
		if (intToReturn != 1) {
			intToReturn = compareNotEqual(comparToValue, oper, valToString);
		}
		// If the value is GreaterThan
		if (intToReturn != 1) {
			intToReturn = compareGreaterThan(comparToValue, oper, valToString);
		}
		// If the value is GreaterThanEqual
		if (intToReturn != 1) {
			intToReturn = compareGreaterThanEqual(comparToValue, oper, valToString);
		}
		// If the value is LessThan
		if (intToReturn != 1) {
			intToReturn = compareLessThan(comparToValue, oper, valToString);
		}
		// If the value is LessThanEqual
		if (intToReturn != 1) {
			intToReturn = compareLessThanEqual(comparToValue, oper, valToString);
		}
		return intToReturn;
	}

	/**
	 * compareLessThanEqual method
	 * 
	 * @param comparToValue
	 *            the value to compare
	 * @param oper
	 *            the operator
	 * @param valToString
	 *            the value to be compared to
	 * @return int
	 */
	private static int compareLessThanEqual(final String comparToValue, final String oper, final String valToString) {
		int integerToReturn = -1;
		if (0 == oper.compareTo("lessthanequal")) {
			if (0 <= valToString.compareTo(comparToValue)) {
				integerToReturn = 1;
			} else {
				integerToReturn = 0;
			}
		}
		return integerToReturn;
	}

	/**
	 * compareLessThan method
	 * 
	 * @param comparToValue
	 *            the value to compare
	 * @param oper
	 *            the operator
	 * @param valToString
	 *            the value to be compared to
	 * @return int
	 */
	private static int compareLessThan(final String comparToValue, final String oper, final String valToString) {
		int integerToReturn = -1;
		if (0 == oper.compareTo("lessthan")) {
			if (-1 == valToString.compareTo(comparToValue)) {
				integerToReturn = 1;
			} else {
				integerToReturn = 0;
			}
		}
		return integerToReturn;
	}

	/**
	 * compareGreaterThanEqual method
	 * 
	 * @param comparToValue
	 *            the value to compare
	 * @param oper
	 *            the operator
	 * @param valToString
	 *            the value to be compared to
	 * @return int
	 */
	private static int compareGreaterThanEqual(final String comparToValue, final String oper, final String valToString) {
		int integerToReturn = -1;
		if (0 == oper.compareTo("greaterthanequal")) {
			if (0 >= valToString.compareTo(comparToValue)) {
				integerToReturn = 1;
			} else {
				integerToReturn = 0;
			}
		}
		return integerToReturn;
	}

	/**
	 * compareGreaterThan method
	 * 
	 * @param comparToValue
	 *            the value to compare
	 * @param oper
	 *            the operator
	 * @param valToString
	 *            the value to be compared to
	 * @return int
	 */
	private static int compareGreaterThan(final String comparToValue, final String oper, final String valToString) {
		int integerToReturn = -1;
		if (0 == oper.compareTo("greaterthan")) {
			if (1 == valToString.compareTo(comparToValue)) {
				integerToReturn = 1;
			} else {
				integerToReturn = 0;
			}
		}
		return integerToReturn;
	}

	/**
	 * compareNotEqual method
	 * 
	 * @param comparToValue
	 *            the value to compare
	 * @param oper
	 *            the operator
	 * @param valToString
	 *            the value to be compared to
	 * @return int
	 */
	private static int compareNotEqual(final String comparToValue, final String oper, final String valToString) {
		int integerToReturn = -1;
		if (0 == oper.compareTo("notequal")) {
			if (0 != valToString.compareTo(comparToValue)) {
				integerToReturn = 1;
			} else {
				integerToReturn = 0;
			}
		}
		return integerToReturn;
	}

	/**
	 * private method to compare equal
	 * 
	 * @param comparToValue
	 * @param oper
	 * @param valToString
	 * @return int
	 */
	private static int compareEqual(final String comparToValue, final String oper, final String valToString) {
		int integerToReturn = -1;
		if (0 == oper.compareTo("equal")) {
			if (0 == valToString.compareTo(comparToValue)) {
				integerToReturn = 1;
			} else {
				integerToReturn = 0;
			}
		}
		return integerToReturn;
	}

	/**
	 * This method Validator
	 * 
	 * @param str
	 *            : String
	 * @param min
	 *            : Integer
	 * @param max
	 *            : Integer
	 * @return int
	 */
	public static int lengthValidator(final String str, final Integer min, final Integer max) {
		int intToReturn = 0;
		if (max.intValue() < 0 || min.intValue() < 0 || null == str) {
			intToReturn = -1;
		} else if ((str.length() <= max.intValue()) && (str.length() >= min.intValue())) {
			intToReturn = 1;
		}
		return intToReturn;
	}

	/**
	 * This method Validator
	 * 
	 * @param str
	 *            : Object
	 * @param min
	 *            : String
	 * @param max
	 *            : String
	 * @return int
	 */
	public static int rangeValidator(final Object value, final String min, final String max) {
		if (null == max || null == min || null == value) {
			return -1;
		}
		int intToReturn = 0;
		// default behavior
		intToReturn = compareRangeInteger(value, min, max);
		// If the value is a Double
		if (intToReturn != 1) {
			intToReturn = compareRangeDouble(value, min, max);
		}
		// If the value is a Float
		if (intToReturn != 1) {
			intToReturn = compareRangeFloat(value, min, max);
		}
		// If the value is a Date
		if (intToReturn != 1) {
			intToReturn = compareRangeDate(value, min, max);
		}
		// If the value is a String
		if (intToReturn != 1) {
			intToReturn = compareRangeString(value, min, max);
		}
		return intToReturn;
	}

	/**
	 * @param value
	 * @param min
	 * @param max
	 */
	private static int compareRangeString(final Object value, final String min, final String max) {
		if (value instanceof String) {
			final String stringValue = (String) value;
			if (stringValue != null && (0 <= stringValue.compareTo(max)) && (0 >= stringValue.compareTo(min))) {
				return 1;
			}
		}
		return 0;
	}

	/**
	 * @param value
	 * @param min
	 * @param max
	 */
	private static int compareRangeDate(final Object value, final String min, final String max) {
		if (value instanceof Date) {
			final Date tempValue = (Date) value;
			final DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.SHORT, Locale.getDefault());
			try {
				final Date dateMin = dateFormat.parse(min);
				final Date dateMax = dateFormat.parse(max);
				if (tempValue != null && (tempValue.before(dateMax)) && (tempValue.after(dateMin))) {
					return 1;
				}
			} catch (ParseException e) {
				LOGGER.error("Error while parsing date with pattern: " + dateFormat.toString(), e);
			}
		}
		return 0;
	}

	/**
	 * @param value
	 * @param min
	 * @param max
	 */
	private static int compareRangeFloat(final Object value, final String min, final String max) {
		if (value instanceof Float) {
			final Float minValue = Float.valueOf(min);
			final Float maxValue = Float.valueOf(max);
			final Float tempValue = (Float) value;
			if (tempValue != null && (tempValue <= maxValue) && (tempValue >= minValue)) {
				return 1;
			}
		}
		return 0;
	}

	/**
	 * @param value
	 * @param min
	 * @param max
	 */
	private static int compareRangeDouble(final Object value, final String min, final String max) {
		if (value instanceof Double) {
			final Double minValue = Double.valueOf(min);
			final Double maxValue = Double.valueOf(max);
			final Double tempValue = (Double) value;
			if (tempValue != null && (tempValue <= maxValue) && (tempValue >= minValue)) {
				return 1;
			}
		}
		return 0;
	}

	/**
	 * @param value
	 * @param min
	 * @param max
	 */
	private static int compareRangeInteger(final Object value, final String min, final String max) {
		if (value instanceof Integer) {
			final Integer minValue = Integer.valueOf(min);
			final Integer maxValue = Integer.valueOf(max);
			final Integer tempValue = (Integer) value;
			if (tempValue != null && (tempValue <= maxValue) && (tempValue >= minValue)) {
				return 1;
			}
		}
		return 0;
	}

	/**
	 * This method Expression Validator
	 * 
	 * @param str
	 *            : String
	 * @param pattern
	 *            : String
	 * @return int
	 */
	public static int regularExpressionValidator(final Object str, final String pattern) {
		int intToReturn = 0;
		if (null == pattern || null == str) {
			intToReturn = -1;
		} else if (((Pattern.compile(pattern)).matcher(str.toString())).matches()) {
			intToReturn = 1;
		}
		return intToReturn;
	}

	/**
	 * This method Validator
	 * 
	 * @param val
	 *            : Object
	 * @param pattern
	 *            : String
	 * @param timeZone
	 *            : String
	 * @return int
	 */
	public static int formatValidator(final Object val, final String pattern, final String timeZone) {
		if (null == pattern || null == val) {
			return -1;
		}
		int intToReturn = -1;
		// If the value is a Date
		intToReturn = compareFormatDate(val);
		// If the value is a Double
		if (intToReturn != 1) {
			intToReturn = compareFormatDouble(val, pattern);
		}
		// If the value is a Integer
		if (intToReturn != 1) {
			intToReturn = compareFormatInteger(val, pattern);
		}
		// If the value is a Float
		if (intToReturn != 1) {
			intToReturn = compareFormatFloat(val, pattern);
		}
		// If the value is a String
		if (intToReturn != 1) {
			intToReturn = compareFormatString(val, pattern);
		}
		return intToReturn;
	}

	/**
	 * @param val
	 */
	private static int compareFormatDate(final Object val) {
		if (val instanceof Date) {
			return 1;
		}
		return -1;
	}

	/**
	 * @param val
	 * @param pattern
	 */
	private static int compareFormatString(final Object val, final String pattern) {
		int intToReturn = -1;
		if (val instanceof String) {
			final String string = (String) val;
			final NumberFormat formatter = new DecimalFormat(pattern);
			final String s = String.valueOf(virgule(formatter.format(string)));
			if (0 == s.compareTo(string)) {
				intToReturn = 1;
			} else {
				intToReturn = 0;
			}
		}
		return intToReturn;
	}

	/**
	 * @param val
	 * @param pattern
	 */
	private static int compareFormatFloat(final Object val, final String pattern) {
		int intToReturn = -1;
		if (val instanceof Float) {
			final Float floatNumber = (Float) val;
			final NumberFormat formatter = new DecimalFormat(pattern);
			final Float s = Float.valueOf(virgule(formatter.format(floatNumber)));
			if (0 == s.compareTo(floatNumber)) {
				intToReturn = 1;
			} else {
				intToReturn = 0;
			}
		}
		return intToReturn;
	}

	/**
	 * @param val
	 * @param pattern
	 */
	private static int compareFormatInteger(final Object val, final String pattern) {
		int intToReturn = -1;
		if (val instanceof Integer) {
			final Integer integerNumber = (Integer) val;
			final NumberFormat formatter = new DecimalFormat(pattern);
			final Integer s = Integer.valueOf(virgule(formatter.format(integerNumber)));
			if (0 == s.compareTo(integerNumber)) {
				intToReturn = 1;
			} else {
				intToReturn = 0;
			}
		}
		return intToReturn;
	}

	/**
	 * @param val
	 * @param pattern
	 */
	private static int compareFormatDouble(final Object val, final String pattern) {
		int intToReturn = -1;
		if (val instanceof Double) {
			final Double doubleNumber = (Double) val;
			final NumberFormat formatter = new DecimalFormat(pattern);
			final Double s = Double.valueOf(virgule(formatter.format(doubleNumber)));
			if (0 == s.compareTo(doubleNumber)) {
				intToReturn = 1;
			} else {
				intToReturn = 0;
			}
		}
		return intToReturn;
	}

	/**
	 * This method Validator
	 * 
	 * @param number
	 *            : Double
	 * @param pattern
	 *            : String
	 * @return int
	 */
	public static int formatValidator(final Double number, final String pattern) {
		int intToReturn = 0;
		if (null == pattern || null == number) {
			intToReturn = -1;
		} else {
			final NumberFormat formatter = new DecimalFormat(pattern);
			final Double s = Double.valueOf(virgule(formatter.format(number)));
			if (0 == s.compareTo(number)) {
				intToReturn = 1;
			}
		}
		return intToReturn;
	}

	/**
	 * This method
	 * 
	 * @param str
	 *            : String
	 * @return String
	 */
	private static String virgule(final String str) {

		return str.replace(',', '.');
	}
}

