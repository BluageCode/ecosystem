/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.service.standardfeatures.application.form.uc44_autocompletetable.daofinder;


import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.Logger;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateTemplate;

import com.bluage.documentation.business.standardfeatures.application.form.uc44_autocompletetable.bos.Team44BO;
import com.netfective.bluage.core.exception.ApplicationException;


/**
 * This class represents query invocation methods implementation
 * for ServiceTeam44 service.
 *
 * @see com.bluage.documentation.service.standardfeatures.application.form.uc44_autocompletetable.ServiceTeam44
 */
 
@Named ("com.bluage.documentation.service.standardfeatures.application.form.uc44_autocompletetable.daofinder.ServiceTeam44DAOMethodFinderImpl")
public class ServiceTeam44DAOMethodFinderImpl {

	// LOGGER for ServiceTeam44DAOMethodFinderImpl
	private static final Logger LOGGER = Logger.getLogger(ServiceTeam44DAOMethodFinderImpl.class);
	
	/**
	* Hibernate Template (injected)
	*/
	@Inject
	private HibernateTemplate hibernateTemplate;

	
	/**
	 * setter for HibernateTemplate
	 * @param value : {@link HibernateTemplate}
	 */
	public void setHibernateTemplate( HibernateTemplate value )
	{
		hibernateTemplate = value;
	}
	

	/**    
	 * HQL operation.
	 * @param name {@link String}
		 * @return {@link Team44BO}
	 */
	public Team44BO searchTeam44(String name )
 throws ApplicationException {
Session session = hibernateTemplate.getSessionFactory().getCurrentSession();
			return 
				privateSearchTeam44(name, session);
	}

	
	/**    
	 * private delegation for HQL operation.
	 * @param name {@link String}
		 * @return {@link Team44BO}
	 */
	private Team44BO privateSearchTeam44(String name , Session session)
 throws ApplicationException {
			Query query = session.createQuery(" select t from Team44BO t where t.name = :name ");
query.setParameter( "name", name, Hibernate.STRING);
														Team44BO returnValue = null;
		try {
			Object result = query.uniqueResult();
			if( result != null ) {
				returnValue = ( com.bluage.documentation.business.standardfeatures.application.form.uc44_autocompletetable.bos.Team44BO )result;
			}				
		} catch(HibernateException ex) {
			LOGGER.error("An error occured while excecuting the HQL operation : com.bluage.documentation.service.standardfeatures.application.form.uc44_autocompletetable.searchTeam44");
			throw new ApplicationException(ex);
		}
		return returnValue;
	}

	




}
