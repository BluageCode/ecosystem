/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.service.standardfeatures.services.hql.uc57_hqloperation;


import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import com.bluage.documentation.business.standardfeatures.services.hql.uc57_hqloperation.bos.Screen57BO;
import com.bluage.documentation.business.standardfeatures.services.hql.uc57_hqloperation.bos.Team57BO;
import com.bluage.documentation.service.AbstractService;
import com.bluage.documentation.service.standardfeatures.services.hql.uc57_hqloperation.daofinder.ServiceSearch57DAOMethodFinderImpl;
import com.netfective.bluage.core.exception.ApplicationException;

/**
 * Defines  application services for ServiceSearch57
  */
@Named("com.bluage.documentation.service.standardfeatures.services.hql.uc57_hqloperation.ServiceSearch57")
public class ServiceSearch57  extends AbstractService{


						@Inject
		@Named ("com.bluage.documentation.service.standardfeatures.services.hql.uc57_hqloperation.daofinder.ServiceSearch57DAOMethodFinder")
		private ServiceSearch57DAOMethodFinderImpl serviceSearch57Dao;



	/**
	 *  Default constructor 
	 */
	public ServiceSearch57() {
		super(); // Call super for default constructor : ServiceSearch57
	}
	



				
	/**    
	 * Operation HQL.
	 * @param name {@link String}
		 * @param firstresult {@link Integer}
		 * @param maxresult {@link Integer}
		 * @return {@link List}
	 */
	public List searchByHqlOperation(String name,Integer firstresult,Integer maxresult )
 throws ApplicationException {
	
	return serviceSearch57Dao.searchByHqlOperation(name,firstresult,maxresult);
	}

		
			
	/**    
	 * Process operation.
	 * @param team {@link Team57BO}
		 * @param screen {@link Screen57BO}
		 * @return {@link List}
	 */
	public List executeSearch(Team57BO team,Screen57BO screen )
 throws ApplicationException {

				return privateExecuteSearch(team,screen);
	}
	

	/**    
	 * Process operation.
	 * @param team {@link Team57BO}
		 * @param screen {@link Screen57BO}
		 * @return {@link List}
	 */
	private List privateExecuteSearch(Team57BO team,Screen57BO screen )
 throws ApplicationException {
								// Return Type declaration.
				List searchResult = new java.util.ArrayList();
					// Central buffer node declaration for operation executeSearch
			Integer	 firstresult =  Integer.valueOf(0);
					Integer	 maxresult =  Integer.valueOf(0);
					String	 critName =  "";
					// Calling an operation : 
critName = team.getName();
// Calling an operation : 
maxresult = screen.getMaxresult();
// Calling an operation : 
firstresult = screen.getFirstresult();
/* Executing : */
critName="%"+critName+"%";
// Calling an operation : search with Hql operation
searchResult = searchByHqlOperation(critName, firstresult, maxresult);

						return searchResult;
		}

		}
