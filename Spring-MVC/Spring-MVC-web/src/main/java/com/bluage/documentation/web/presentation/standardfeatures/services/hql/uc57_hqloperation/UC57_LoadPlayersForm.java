/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.services.hql.uc57_hqloperation;


import java.io.Serializable;
import java.util.List;

import com.bluage.documentation.business.standardfeatures.services.hql.uc57_hqloperation.bos.Player57BO;
import com.bluage.documentation.business.standardfeatures.services.hql.uc57_hqloperation.bos.Team57BO;
import com.netfective.bluage.core.web.AbstractForm;
import com.netfective.bluage.core.web.IForm;

/**
* Class : UC57_LoadPlayersForm
*/
public class UC57_LoadPlayersForm extends AbstractForm implements Serializable, IForm {
	
	/** Constant : serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	//CONSTANT : players
	private static final String PLAYERS = "players";
	//CONSTANT : team
	private static final String TEAM = "team";
	/**
	 * 	Property: players 
	 */
	private List<Player57BO> players;
	/**
	 * 	Property: team 
	 */
	private Team57BO team;
/**
	 * Default constructor : UC57_LoadPlayersForm
	 */
	public UC57_LoadPlayersForm() {
		super();
		// Initialize : players
		this.players = new java.util.ArrayList<com.bluage.documentation.business.standardfeatures.services.hql.uc57_hqloperation.bos.Player57BO>();
		// Initialize : team
		this.team = new Team57BO();
					this.setAlwaysCallPreControllers(true);
	}
	
	/**
	 * 	Getter : players 
	 * 	@return : Return the players instance.
	 */
	public List<Player57BO> getPlayers(){
		return players; // For UC57_LoadPlayersForm
	}
	
	/**
	 * 	Setter : players 
	 *  @param playersinstance : The instance to set.
	 */
	public void setPlayers(final List<Player57BO> playersinstance){
		this.players = playersinstance;// For UC57_LoadPlayersForm
	}
	/**
	 * 	Getter : team 
	 * 	@return : Return the team instance.
	 */
	public Team57BO getTeam(){
		return team; // For UC57_LoadPlayersForm
	}
	
	/**
	 * 	Setter : team 
	 *  @param teaminstance : The instance to set.
	 */
	public void setTeam(final Team57BO teaminstance){
		this.team = teaminstance;// For UC57_LoadPlayersForm
	}
/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UC57_LoadPlayersForm [ "+
PLAYERS +" = " + players +TEAM +" = " + team + "]";
	}
	
	/**
	 * Method setInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public void setInstanceByInstanceName(final String instanceName, final Object instance) {
		// Set the instance Players.
		if(PLAYERS.equals(instanceName)){// For UC57_LoadPlayersForm
			this.setPlayers((List<Player57BO>)instance); // For UC57_LoadPlayersForm
		}
				// Set the instance Team.
		if(TEAM.equals(instanceName)){// For UC57_LoadPlayersForm
			this.setTeam((Team57BO)instance); // For UC57_LoadPlayersForm
		}
				// Parent instance name for UC57_LoadPlayersForm
		if(TEAM.equals(instanceName) && team != null){ // For UC57_LoadPlayersForm
			players.clear(); // For UC57_LoadPlayersForm
			players.addAll(team.getPlayers());// For UC57_LoadPlayersForm
		}
		
	}
	
	/**
	 * Method getInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public Object getInstanceByInstanceName(final String instanceName){
		// Temporary object for tmpUC57_LoadPlayersForm.
		Object tmpUC57_LoadPlayersForm = null;
		
			
		// Get the instance Players for UC57_LoadPlayersForm.
		if(PLAYERS.equals(instanceName)){ // For UC57_LoadPlayersForm
			tmpUC57_LoadPlayersForm = this.getPlayers(); // For UC57_LoadPlayersForm
		}
			
		// Get the instance Team for UC57_LoadPlayersForm.
		if(TEAM.equals(instanceName)){ // For UC57_LoadPlayersForm
			tmpUC57_LoadPlayersForm = this.getTeam(); // For UC57_LoadPlayersForm
		}
		return tmpUC57_LoadPlayersForm;// For UC57_LoadPlayersForm
	}
	
			}
