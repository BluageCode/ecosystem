/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.business.advancedfeatures.performance.uc200_datagridloading.bos;

import javax.inject.Inject;
import javax.inject.Named;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import com.bluage.documentation.business.advancedfeatures.performance.uc200_datagridloading.entities.daofinder.Player200DAOFinderImpl;
import com.bluage.documentation.business.common.bos.PersonBO;

/**
 * Player200BO
 *
* @see com.netfective.bluage.core.business.IBusinessObject
* * @see PersonBO
 */
@XmlRootElement

public class Player200BO
 extends PersonBO {
	
	/**
	 * Default SUID for Player200BO.
	 */
	private static final long serialVersionUID = 1L;
	

	@Inject
	@Named ("com.bluage.documentation.business.advancedfeatures.performance.uc200_datagridloading.entities.daofinder.Player200Finder") 
	private Player200DAOFinderImpl finder;

												/**
	 * Entity property: stats
	 *
	 */
private Stats200BO stats;



   /**
	* public default constructor for class Player200BO
	*/
	public Player200BO(){
		//NOP Player200BO
	}


												/**
	 * Getter for stats of class Player200BO.
	 *
	 * @return {@link com.bluage.documentation.business.advancedfeatures.performance.uc200_datagridloading.bos.Stats200BO}
	 * @generated
	 */ 
public  Stats200BO getStats() {
		Stats200BO stats0 = this.stats;
		if(this.stats == null) {
			if(DAO_MODE.equals(this.getObjMode( ) ) ) {
				stats0 = null;
			} else {
				this.stats = new Stats200BO();
				this.stats.setObjMode( this.getObjMode( ) );
				stats0 = this.stats;
			}
		} else {
			this.stats.setObjMode( this.getObjMode( ) );
			if( HELPER_MODE.equals(this.getObjMode( ) ) ) {
				stats0 = this.stats;
			}
			if( DAO_MODE.equals(this.getObjMode()) && this.stats.isEmpty ) {
				stats0 = null;
			}
		}
		return stats0;
	}


	/**
	 * Setter for stats of class Player200BO.
	 * @param value {@link com.bluage.documentation.business.advancedfeatures.performance.uc200_datagridloading.bos.Stats200BO}
	 * @generated
	 */

public  void setStats(final Stats200BO stats) {
	if(stats != null) {
		stats.isEmpty = false;
		stats.setObjMode( this.getObjMode( ) );
	}
	this.isEmpty = false;
	this.stats = stats;
}


/**
	 * Returns <code>true</code> if the argument is an Player200 instance and all identifiers for this entity
     * equal the identifiers of the argument entity. The <code>equals</code> method of the parent entity
     * will also need to return <code>true</code>. Returns <code>false</code> otherwise.
     *
     * @see PersonBO#hashCode()
	 * @param object  object to compare with current instance      
     * @return boolean true if equals false if not
     */
    public boolean equals(final Object object )
    {
		if ( this == object )
        {
            return true;
        }
        if ( !( object instanceof Player200BO ) )
        {
            return false;
        }
        final Player200BO that = (Player200BO)object;
    
    	            if (        				        			this.getId() == null
        	        ) {
        	return false;
        }
		return new EqualsBuilder()
			.append(this.getId(), that.getId())
			 	.isEquals();
	}
	/**

	 * Returns a hash code based on this entity's identifiers and the hash code of the parent entity.
     *
     * @see PersonBO#hashCode()
     * @return int
     */    
    public int hashCode() {
        return new HashCodeBuilder()
             .append(getId())
				 	.appendSuper(super.hashCode())
            .toHashCode();
    }
}

