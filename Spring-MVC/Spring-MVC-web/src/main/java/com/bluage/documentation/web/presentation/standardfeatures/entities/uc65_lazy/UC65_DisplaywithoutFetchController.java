/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.entities.uc65_lazy ;

// Import declaration.
// Java imports.
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.bluage.documentation.business.standardfeatures.entities.uc65_lazy.bos.Team65BO;
import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;

/**
 * Class : UC65_DisplaywithoutFetchController
 */
@Controller
@Scope("prototype")
@SessionAttributes("uC65_DisplaywithoutFetchForm")
@RequestMapping(value= "/presentation/standardfeatures/entities/uc65_lazy")
public class UC65_DisplaywithoutFetchController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC65_DisplaywithoutFetchController.class);
	
	//Current form name
	private static final String U_C65__DISPLAYWITHOUT_FETCH_FORM = "uC65_DisplaywithoutFetchForm";

	//CONSTANT: teamLazyFalse
	private static final String TEAM_LAZY_FALSE = "teamLazyFalse";
		//CONSTANT: players table or repeater.
	private static final String PLAYERS = "players";
				
	/**
	 * Property:customDateEditorsUC65_DisplaywithoutFetchController
	 */
	final private Map<String,CustomDateEditor> customDateEditorsUC65_DisplaywithoutFetchController = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:uC65_DisplaywithoutFetchValidator
	 */
	final private UC65_DisplaywithoutFetchValidator uC65_DisplaywithoutFetchValidator = new UC65_DisplaywithoutFetchValidator();
	
	
	// Initialise all the instances for UC65_DisplaywithoutFetchController
		// Initialize the instance teamLazyFalse of type com.bluage.documentation.business.standardfeatures.entities.uc65_lazy.bos.Team65BO with the value : null for UC65_DisplaywithoutFetchController
		private Team65BO teamLazyFalse;
			// Initialize the instance players for the state lnk_view_nofetch
		private List players; // Initialize the instance players for UC65_DisplaywithoutFetchController
				
	/**
	* This method initialise the form : UC65_DisplaywithoutFetchForm 
	* @return UC65_DisplaywithoutFetchForm
	*/
	@ModelAttribute("UC65_DisplaywithoutFetchFormInit")
	public void initUC65_DisplaywithoutFetchForm(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, U_C65__DISPLAYWITHOUT_FETCH_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method UC65_DisplaywithoutFetchForm."); //for lnk_view_nofetch 
		}
		UC65_DisplaywithoutFetchForm uC65_DisplaywithoutFetchForm;
	
		if(request.getSession().getAttribute(U_C65__DISPLAYWITHOUT_FETCH_FORM) != null){
			uC65_DisplaywithoutFetchForm = (UC65_DisplaywithoutFetchForm)request.getSession().getAttribute(U_C65__DISPLAYWITHOUT_FETCH_FORM);
		} else {
			uC65_DisplaywithoutFetchForm = new UC65_DisplaywithoutFetchForm();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form UC65_DisplaywithoutFetchForm.");
		}
		uC65_DisplaywithoutFetchForm = (UC65_DisplaywithoutFetchForm)populateDestinationForm(request.getSession(), uC65_DisplaywithoutFetchForm);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  UC65_DisplaywithoutFetchForm.");
		}
		model.addAttribute(U_C65__DISPLAYWITHOUT_FETCH_FORM, uC65_DisplaywithoutFetchForm);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param UC65_DisplaywithoutFetchForm : The sceen form.
	 */
	@RequestMapping(value = "/UC65_DisplaywithoutFetch.html" ,method = RequestMethod.GET)
	public void prepareUC65_DisplaywithoutFetch(final HttpServletRequest request,final  Model model,final  @ModelAttribute("UC65_DisplaywithoutFetchFormInit") UC65_DisplaywithoutFetchForm uC65_DisplaywithoutFetchForm){
		
		UC65_DisplaywithoutFetchForm currentUC65_DisplaywithoutFetchForm = uC65_DisplaywithoutFetchForm;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(U_C65__DISPLAYWITHOUT_FETCH_FORM) == null){
			if(currentUC65_DisplaywithoutFetchForm!=null){
				request.getSession().setAttribute(U_C65__DISPLAYWITHOUT_FETCH_FORM, currentUC65_DisplaywithoutFetchForm);
			}else {
				currentUC65_DisplaywithoutFetchForm = new UC65_DisplaywithoutFetchForm();
				request.getSession().setAttribute(U_C65__DISPLAYWITHOUT_FETCH_FORM, currentUC65_DisplaywithoutFetchForm);	
			}
		} else {
			currentUC65_DisplaywithoutFetchForm = (UC65_DisplaywithoutFetchForm) request.getSession().getAttribute(U_C65__DISPLAYWITHOUT_FETCH_FORM);
		}

				currentUC65_DisplaywithoutFetchForm = (UC65_DisplaywithoutFetchForm)populateDestinationForm(request.getSession(), currentUC65_DisplaywithoutFetchForm);
		// Call all the Precontroller.
	currentUC65_DisplaywithoutFetchForm.setAlwaysCallPreControllers(true);
		model.addAttribute(U_C65__DISPLAYWITHOUT_FETCH_FORM, currentUC65_DisplaywithoutFetchForm);
		request.getSession().setAttribute(U_C65__DISPLAYWITHOUT_FETCH_FORM, currentUC65_DisplaywithoutFetchForm);
		
	}
	
			
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("UC65_DisplaywithoutFetchController [ ");
			strBToS.append(TEAM_LAZY_FALSE);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(teamLazyFalse);
			strBToS.append(END_TO_STRING_DELIMITER);
				strBToS.append(PLAYERS);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(players);
			strBToS.append(END_TO_STRING_DELIMITER);
					// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(uC65_DisplaywithoutFetchValidator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsUC65_DisplaywithoutFetchController!=null); 
		return strBToS.toString();
	}
}
