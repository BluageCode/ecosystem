/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.web.presentation.standardfeatures.application.component.uc47_contents;

// Import declaration.
// Java imports.
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import com.bluage.documentation.business.standardfeatures.application.component.uc47_contents.bos.Screen47BO;
import com.bluage.documentation.service.standardfeatures.application.component.uc47_contents.ServicePlayer47;
import com.bluage.documentation.service.standardfeatures.application.component.uc47_contents.ServiceScreen47;
import com.netfective.bluage.core.exception.ApplicationException;
import com.netfective.bluage.core.web.AbstractController;
import com.netfective.bluage.core.web.IForm;

/**
 * Class : UC47_PreControllerFindAllPlayersController
 */
 @Service("com.bluage.documentation.web.presentation.standardfeatures.application.component.uc47_contents.UC47_PreControllerFindAllPlayersController")
public class UC47_PreControllerFindAllPlayersController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC47_PreControllerFindAllPlayersController.class);
	
	//CONSTANT PINS: screen47
	private static final String SCREEN47 = "screen47";
	//CONSTANT PINS: allPlayers
	private static final String ALL_PLAYERS = "allPlayers";
	
	// Declaring all the instances.
		Screen47BO screen47;
		List allPlayers;
	/**
	 * Service declaration : serviceScreen47.
	 */
	@Autowired
	private ServiceScreen47 serviceScreen47;
	
	/**
	 * Service declaration : servicePlayer47.
	 */
	@Autowired
	private ServicePlayer47 servicePlayer47;
	
	/**
	 * Operation : UC47_PreControllerFindAllPlayersControllerInit
	 * @param request : The current HttpRequest
 	 * @param model :  The current Model
 	 * @param currentIForm : The current IForm
 	 * @return
	 */
	public IForm uC47_PreControllerFindAllPlayersControllerInit(final HttpServletRequest request,final  Model model,final  IForm  currentIForm){

		LOGGER.info("Begin the precontroller method : findPlayers");
		LOGGER.info("Form diagnostic : " + currentIForm);
		

	callInitializeScreen47(request,currentIForm);
		

	callFindplayers(request,currentIForm);
		
			LOGGER.info("Populate the destination screen.");
			// Populate the destination form with all the instances returned from an executed service.
			return populateDestinationForm(request.getSession(), currentIForm); // Populate the destination screen

		
	}
	/**
	 * method callInitializeScreen47
	 * 
	 * @param request HttpServletRequest
	 * @param currentIForm IForm
	 */
	private void callInitializeScreen47(HttpServletRequest request, IForm currentIForm) {
		// Initialize all the instances.
		screen47 = (Screen47BO)get(request.getSession(),currentIForm,SCREEN47);
							try {

				screen47 = 	serviceScreen47.InitializeScreen47(
	);  

				put(request.getSession(), SCREEN47,screen47);
							
			} catch (ApplicationException e) {
				LOGGER.error("An error occured during the execution of the operation : InitializeScreen47",e);
			}
	}
	/**
	 * method callFindplayers
	 * 
	 * @param request HttpServletRequest
	 * @param currentIForm IForm
	 */
	private void callFindplayers(HttpServletRequest request, IForm currentIForm) {
		// Initialize all the instances.
		allPlayers = (List)get(request.getSession(),currentIForm,ALL_PLAYERS);
							try {

				allPlayers = 	servicePlayer47.player47FindAll(
	);  

				put(request.getSession(), ALL_PLAYERS,allPlayers);
							
			} catch (ApplicationException e) {
				LOGGER.error("An error occured during the execution of the operation : player47FindAll",e);
			}
	}
}
