/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.application.navigation.uc31_preaction ;

// Import declaration.
// Java imports.
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;

/**
 * Class : UC31_HomePageController
 */
@Controller
@Scope("prototype")
@SessionAttributes("uC31_HomePageForm")
@RequestMapping(value= "/presentation/standardfeatures/application/navigation/uc31_preaction")
public class UC31_HomePageController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC31_HomePageController.class);
	
	//Current form name
	private static final String U_C31__HOME_PAGE_FORM = "uC31_HomePageForm";

	
	/**
	 * Property:customDateEditorsUC31_HomePageController
	 */
	final private Map<String,CustomDateEditor> customDateEditorsUC31_HomePageController = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:uC31_HomePageValidator
	 */
	final private UC31_HomePageValidator uC31_HomePageValidator = new UC31_HomePageValidator();
	
	
	// Initialise all the instances for UC31_HomePageController

	/**
	* This method initialise the form : UC31_HomePageForm 
	* @return UC31_HomePageForm
	*/
	@ModelAttribute("UC31_HomePageFormInit")
	public void initUC31_HomePageForm(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, U_C31__HOME_PAGE_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method UC31_HomePageForm."); //for lnk_stadiums 
		}
		UC31_HomePageForm uC31_HomePageForm;
	
		if(request.getSession().getAttribute(U_C31__HOME_PAGE_FORM) != null){
			uC31_HomePageForm = (UC31_HomePageForm)request.getSession().getAttribute(U_C31__HOME_PAGE_FORM);
		} else {
			uC31_HomePageForm = new UC31_HomePageForm();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form UC31_HomePageForm.");
		}
		uC31_HomePageForm = (UC31_HomePageForm)populateDestinationForm(request.getSession(), uC31_HomePageForm);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  UC31_HomePageForm.");
		}
		model.addAttribute(U_C31__HOME_PAGE_FORM, uC31_HomePageForm);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param UC31_HomePageForm : The sceen form.
	 */
	@RequestMapping(value = "/UC31_HomePage.html" ,method = RequestMethod.GET)
	public void prepareUC31_HomePage(final HttpServletRequest request,final  Model model,final  @ModelAttribute("UC31_HomePageFormInit") UC31_HomePageForm uC31_HomePageForm){
		
		UC31_HomePageForm currentUC31_HomePageForm = uC31_HomePageForm;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(U_C31__HOME_PAGE_FORM) == null){
			if(currentUC31_HomePageForm!=null){
				request.getSession().setAttribute(U_C31__HOME_PAGE_FORM, currentUC31_HomePageForm);
			}else {
				currentUC31_HomePageForm = new UC31_HomePageForm();
				request.getSession().setAttribute(U_C31__HOME_PAGE_FORM, currentUC31_HomePageForm);	
			}
		} else {
			currentUC31_HomePageForm = (UC31_HomePageForm) request.getSession().getAttribute(U_C31__HOME_PAGE_FORM);
		}

		currentUC31_HomePageForm = (UC31_HomePageForm)populateDestinationForm(request.getSession(), currentUC31_HomePageForm);
		// Call all the Precontroller.
	currentUC31_HomePageForm.setAlwaysCallPreControllers(true);
		model.addAttribute(U_C31__HOME_PAGE_FORM, currentUC31_HomePageForm);
		request.getSession().setAttribute(U_C31__HOME_PAGE_FORM, currentUC31_HomePageForm);
		
	}
	
	
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("UC31_HomePageController [ ");
		// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(uC31_HomePageValidator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsUC31_HomePageController!=null); 
		return strBToS.toString();
	}
}
