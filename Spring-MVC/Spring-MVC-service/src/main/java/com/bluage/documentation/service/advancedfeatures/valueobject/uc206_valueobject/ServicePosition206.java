/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.service.advancedfeatures.valueobject.uc206_valueobject;


import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import com.bluage.documentation.business.advancedfeatures.valueobject.uc206_valueobject.bos.Position206BO;
import com.bluage.documentation.business.advancedfeatures.valueobject.uc206_valueobject.entities.daofinder.Position206DAOFinderImpl;
import com.bluage.documentation.service.AbstractService;
import com.netfective.bluage.core.exception.ApplicationException;

/**
 * Defines  application services for ServicePosition206
  */
@Named("com.bluage.documentation.service.advancedfeatures.valueobject.uc206_valueobject.ServicePosition206")
public class ServicePosition206  extends AbstractService{


			@Inject
		@Named ("com.bluage.documentation.business.advancedfeatures.valueobject.uc206_valueobject.entities.daofinder.Position206DAOFinderImpl")
		private Position206DAOFinderImpl position206BOFinder;



	/**
	 *  Default constructor 
	 */
	public ServicePosition206() {
		super(); // Call super for default constructor : ServicePosition206
	}
	


			
				
	/**    
	 * Operation Crud
	 * @param bo {@link Position206BO}
		 * @return {@link Position206BO}
	 */
	public Position206BO position206FindByID(Position206BO bo )
 throws ApplicationException {

					return (Position206BO)position206BOFinder.findByPrimaryKey(getBoId(bo));
	}
	/**
	 *  Return the primary key from business object.
	 */
	private Object getBoId(Position206BO bo) {
				return bo.getCode();
		}

		
								
	/**    
	 * Operation Crud
	 * @return {@link List}
	 */
	public List position206FindAll( )
 throws ApplicationException {

					return position206BOFinder.findAll();
	}

		}
