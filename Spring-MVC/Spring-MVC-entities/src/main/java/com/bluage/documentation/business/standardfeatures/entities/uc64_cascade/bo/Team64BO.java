/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.business.standardfeatures.entities.uc64_cascade.bo;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.inject.Inject;
import javax.inject.Named;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.LazyInitializationException;

import com.bluage.documentation.business.common.bos.TeamBO;
import com.bluage.documentation.business.standardfeatures.entities.uc64_cascade.entities.daofinder.Team64DAOFinderImpl;
import com.netfective.bluage.core.exception.ApplicationException;

/**
 * Team64BO
 *
* @see com.netfective.bluage.core.business.IBusinessObject
* * @see TeamBO
 */
@XmlRootElement

public class Team64BO
 extends TeamBO {
	
	/**
	 * Default SUID for Team64BO.
	 */
	private static final long serialVersionUID = 1L;
	

	@Inject
	@Named ("com.bluage.documentation.business.standardfeatures.entities.uc64_cascade.entities.daofinder.Team64Finder") 
	private Team64DAOFinderImpl finder;

																			/**
	 * Entity property: players
	 *
	 */
private Set<Player64BO> players = new HashSet<Player64BO>();

											/**
	 * Entity property: state
	 *
	 */
private State64BO state;

											/**
	 * Entity property: coach
	 *
	 */
private Coach64BO coach;

																		/**
	 * Entity property: audience
	 *
	 */
private Set<Audience64BO> audience = new HashSet<Audience64BO>();



   /**
	* public default constructor for class Team64BO
	*/
	public Team64BO(){
		//NOP Team64BO
	}


																			/**
	 * Getter for players of class Team64BO.
	 *
	 * @return {@link java.util.Set<com.bluage.documentation.business.standardfeatures.entities.uc64_cascade.bo.Player64BO>}
	 * @generated
	 */ 
public  Set<Player64BO> getPlayers() {
	return this.players;
			}


	/**
	 * Setter for players of class Team64BO.
	 * @param value {@link java.util.Set<com.bluage.documentation.business.standardfeatures.entities.uc64_cascade.bo.Player64BO>}
	 * @generated
	 */

public  void setPlayers(final Set<Player64BO> players) {
	this.players = players;
}

	/**
	 * Remove an entity from the com.bluage.documentation.business.standardfeatures.entities.uc64_cascade.bo.Player64BO collection.
	 *
	 * @param entity
	 *		: {@link Set<Player64BO>} the entity
	 * @throws ApplicationException
     */
public void removeFromPlayers( Player64BO entity ) throws ApplicationException {
	try {
		if( getPlayers() != null && getPlayers().contains(entity) ) {
			getPlayers().remove( entity );
		entity.setTeam( null );
		}
		} catch ( LazyInitializationException e ) {
				Team64BO bo = ( Team64BO ) finder.findByPrimaryKey( getId() );
			finder.getHibernateTemplate().initialize( bo.getPlayers() );
			setPlayers( bo.getPlayers() );
			// evict object from session to avoid NonUniqueObjectException
			finder.getHibernateTemplate().evict( bo );
		
			if( getPlayers() != null && getPlayers().contains( entity ) ) {
				getPlayers().remove( entity );
				entity.setTeam( null );
			}
				}
	}
	
	/**
	 * Add entity to the com.bluage.documentation.business.standardfeatures.entities.uc64_cascade.bo.Player64BO collection.
	 *
	 * @param entity
	 *		{@link Player64BO} the entity
	 * @throws ApplicationException
     */
public void addToPlayers( Player64BO entity ) throws ApplicationException
	{
		try {
			if( getPlayers() != null && !getPlayers().contains( entity ) ) {
				getPlayers().add( entity );
				entity.setTeam( this );
			}
			} catch ( LazyInitializationException e ) {
									Team64BO bo = ( Team64BO ) finder.findByPrimaryKey( getId() );
		finder.getHibernateTemplate().initialize( bo.getPlayers() );
		setPlayers( bo.getPlayers() );
		// evict object from session to avoid NonUniqueObjectException
		finder.getHibernateTemplate().evict( bo );
		if( getPlayers() != null && !getPlayers().contains( entity ) ) {
			getPlayers().add( entity );
			entity.setTeam( this );
		}
			}
	}
	
	/**
	 * Add entitys to the com.bluage.documentation.business.standardfeatures.entities.uc64_cascade.bo.Player64BO collection
	 *
	 * @param entities			
	 *		: {@link java.util.List} A list of entities
	 * @throws ApplicationException 
     */	
public void addAllToPlayers( Collection<Player64BO> entities ) throws ApplicationException {
	for( Player64BO entity : entities) {
		if( getPlayers()!=null ) {
			addToPlayers( entity );
		}
	}
}
	
	/**
	 * Remove entities form the com.bluage.documentation.business.standardfeatures.entities.uc64_cascade.bo.Player64BO collection
	 *
	 * @param entities				
	 *		: {@link java.util.List} a list of entities
	 * @throws ApplicationException
     */	
public void removeAllFromPlayers( Collection<Player64BO> entities ) throws ApplicationException {
	for( Player64BO entity : entities) {
		if( getPlayers()!=null && getPlayers().contains( entity ) )	{		
			removeFromPlayers( entity );
		}
	}
}
	
	/**
	 * Remove all items form the collection com.bluage.documentation.business.standardfeatures.entities.uc64_cascade.bo.Player64BO
	 *
	 * @throws ApplicationException
     */	
public void removeAllFromPlayers() throws ApplicationException {
	Set<Player64BO> entities = this.getPlayers();
	if ( entities == null ) {
		return;
	}
	Iterator<Player64BO> it = entities.iterator();
	while ( it.hasNext() ) {
    	Player64BO entity = it.next(); 
		entity.setTeam( null ); 
	}
	entities.clear();
}
											/**
	 * Getter for state of class Team64BO.
	 *
	 * @return {@link com.bluage.documentation.business.standardfeatures.entities.uc64_cascade.bo.State64BO}
	 * @generated
	 */ 
public  State64BO getState() {
		State64BO state0 = this.state;
		if(this.state == null) {
			if(DAO_MODE.equals(this.getObjMode( ) ) ) {
				state0 = null;
			} else {
				this.state = new State64BO();
				this.state.setObjMode( this.getObjMode( ) );
				state0 = this.state;
			}
		} else {
			this.state.setObjMode( this.getObjMode( ) );
			if( HELPER_MODE.equals(this.getObjMode( ) ) ) {
				state0 = this.state;
			}
			if( DAO_MODE.equals(this.getObjMode()) && this.state.isEmpty ) {
				state0 = null;
			}
		}
		return state0;
	}


	/**
	 * Setter for state of class Team64BO.
	 * @param value {@link com.bluage.documentation.business.standardfeatures.entities.uc64_cascade.bo.State64BO}
	 * @generated
	 */

public  void setState(final State64BO state) {
	if(state != null) {
		state.isEmpty = false;
		state.setObjMode( this.getObjMode( ) );
	}
	this.isEmpty = false;
	this.state = state;
}

											/**
	 * Getter for coach of class Team64BO.
	 *
	 * @return {@link com.bluage.documentation.business.standardfeatures.entities.uc64_cascade.bo.Coach64BO}
	 * @generated
	 */ 
public  Coach64BO getCoach() {
		Coach64BO coach0 = this.coach;
		if(this.coach == null) {
			if(DAO_MODE.equals(this.getObjMode( ) ) ) {
				coach0 = null;
			} else {
				this.coach = new Coach64BO();
				this.coach.setObjMode( this.getObjMode( ) );
				coach0 = this.coach;
			}
		} else {
			this.coach.setObjMode( this.getObjMode( ) );
			if( HELPER_MODE.equals(this.getObjMode( ) ) ) {
				coach0 = this.coach;
			}
			if( DAO_MODE.equals(this.getObjMode()) && this.coach.isEmpty ) {
				coach0 = null;
			}
		}
		return coach0;
	}


	/**
	 * Setter for coach of class Team64BO.
	 * @param value {@link com.bluage.documentation.business.standardfeatures.entities.uc64_cascade.bo.Coach64BO}
	 * @generated
	 */

public  void setCoach(final Coach64BO coach) {
	if(coach != null) {
		coach.isEmpty = false;
		coach.setObjMode( this.getObjMode( ) );
	}
	this.isEmpty = false;
	this.coach = coach;
}

																		/**
	 * Getter for audience of class Team64BO.
	 *
	 * @return {@link java.util.Set<com.bluage.documentation.business.standardfeatures.entities.uc64_cascade.bo.Audience64BO>}
	 * @generated
	 */ 
public  Set<Audience64BO> getAudience() {
	return this.audience;
			}


	/**
	 * Setter for audience of class Team64BO.
	 * @param value {@link java.util.Set<com.bluage.documentation.business.standardfeatures.entities.uc64_cascade.bo.Audience64BO>}
	 * @generated
	 */

public  void setAudience(final Set<Audience64BO> audience) {
	this.audience = audience;
}

	/**
	 * Remove an entity from the com.bluage.documentation.business.standardfeatures.entities.uc64_cascade.bo.Audience64BO collection.
	 *
	 * @param entity
	 *		: {@link Set<Audience64BO>} the entity
	 * @throws ApplicationException
     */
public void removeFromAudience( Audience64BO entity ) throws ApplicationException {
	try {
		if( getAudience() != null && getAudience().contains(entity) ) {
			getAudience().remove( entity );
		}
	} catch ( LazyInitializationException e ) {
				Team64BO bo = ( Team64BO ) finder.findByPrimaryKey( getId() );
			finder.getHibernateTemplate().initialize( bo.getAudience() );
			setAudience( bo.getAudience() );
			// evict object from session to avoid NonUniqueObjectException
			finder.getHibernateTemplate().evict( bo );
		
			if( getAudience() != null && getAudience().contains( entity ) ) {
				getAudience().remove( entity );
			}
		}
	}
	
	/**
	 * Add entity to the com.bluage.documentation.business.standardfeatures.entities.uc64_cascade.bo.Audience64BO collection.
	 *
	 * @param entity
	 *		{@link Audience64BO} the entity
	 * @throws ApplicationException
     */
public void addToAudience( Audience64BO entity ) throws ApplicationException
	{
		try {
			if( getAudience() != null && !getAudience().contains( entity ) ) {
				getAudience().add( entity );
			}
		} catch ( LazyInitializationException e ) {
									Team64BO bo = ( Team64BO ) finder.findByPrimaryKey( getId() );
		finder.getHibernateTemplate().initialize( bo.getAudience() );
		setAudience( bo.getAudience() );
		// evict object from session to avoid NonUniqueObjectException
		finder.getHibernateTemplate().evict( bo );
		if( getAudience() != null && !getAudience().contains( entity ) ) {
			getAudience().add( entity );
		}
	}
	}
	
	/**
	 * Add entitys to the com.bluage.documentation.business.standardfeatures.entities.uc64_cascade.bo.Audience64BO collection
	 *
	 * @param entities			
	 *		: {@link java.util.List} A list of entities
	 * @throws ApplicationException 
     */	
public void addAllToAudience( Collection<Audience64BO> entities ) throws ApplicationException {
	for( Audience64BO entity : entities) {
		if( getAudience()!=null ) {
			addToAudience( entity );
		}
	}
}
	
	/**
	 * Remove entities form the com.bluage.documentation.business.standardfeatures.entities.uc64_cascade.bo.Audience64BO collection
	 *
	 * @param entities				
	 *		: {@link java.util.List} a list of entities
	 * @throws ApplicationException
     */	
public void removeAllFromAudience( Collection<Audience64BO> entities ) throws ApplicationException {
	for( Audience64BO entity : entities) {
		if( getAudience()!=null && getAudience().contains( entity ) )	{		
			removeFromAudience( entity );
		}
	}
}
	
	/**
	 * Remove all items form the collection com.bluage.documentation.business.standardfeatures.entities.uc64_cascade.bo.Audience64BO
	 *
	 * @throws ApplicationException
     */	
public void removeAllFromAudience() throws ApplicationException {
	Set<Audience64BO> entities = this.getAudience();
	if ( entities == null ) {
		return;
	}
	entities.clear();
}

/**
	 * Returns <code>true</code> if the argument is an Team64 instance and all identifiers for this entity
     * equal the identifiers of the argument entity. The <code>equals</code> method of the parent entity
     * will also need to return <code>true</code>. Returns <code>false</code> otherwise.
     *
     * @see TeamBO#hashCode()
	 * @param object  object to compare with current instance      
     * @return boolean true if equals false if not
     */
    public boolean equals(final Object object )
    {
		if ( this == object )
        {
            return true;
        }
        if ( !( object instanceof Team64BO ) )
        {
            return false;
        }
        final Team64BO that = (Team64BO)object;
    
    	            if (        				        			this.getId() == null
        	        ) {
        	return false;
        }
		return new EqualsBuilder()
			.append(this.getId(), that.getId())
			 	.isEquals();
	}
	/**

	 * Returns a hash code based on this entity's identifiers and the hash code of the parent entity.
     *
     * @see TeamBO#hashCode()
     * @return int
     */    
    public int hashCode() {
        return new HashCodeBuilder()
             .append(getId())
				 	.appendSuper(super.hashCode())
            .toHashCode();
    }
}

