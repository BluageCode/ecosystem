/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.application.datagrid.uc34_paginationtable ;

// Import declaration.
// Java imports.
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.bluage.documentation.business.standardfeatures.application.datagrid.uc34_paginationtable.bos.Team34BO;
import com.bluage.documentation.business.standardfeatures.application.datagrid.uc34_paginationtable.entities.daofinder.Team34DAOFinderImpl;
import com.bluage.documentation.service.standardfeatures.application.datagrid.uc34_paginationtable.ServiceTeam34;
import com.netfective.bluage.core.exception.ApplicationException;
import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;
import com.netfective.bluage.core.web.IForm;

/**
 * Class : UC34_PaginationTableController
 */
@Controller
@Scope("prototype")
@SessionAttributes("uC34_PaginationTableForm")
@RequestMapping(value= "/presentation/standardfeatures/application/datagrid/uc34_paginationtable")
public class UC34_PaginationTableController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC34_PaginationTableController.class);
	
	//Current form name
	private static final String U_C34__PAGINATION_TABLE_FORM = "uC34_PaginationTableForm";

	//CONSTANT: allTeams table or repeater.
	private static final String ALL_TEAMS = "allTeams";
				//CONSTANT: teamDetails
	private static final String TEAM_DETAILS = "teamDetails";
	//CONSTANT: selectedTeam
	private static final String SELECTED_TEAM = "selectedTeam";
	
	/**
	 * Property:customDateEditorsUC34_PaginationTableController
	 */
	final private Map<String,CustomDateEditor> customDateEditorsUC34_PaginationTableController = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:uC34_PaginationTableValidator
	 */
	final private UC34_PaginationTableValidator uC34_PaginationTableValidator = new UC34_PaginationTableValidator();
	
	/**
	 * Service declaration : serviceTeam34.
	 */
	@Autowired
	private ServiceTeam34 serviceTeam34;
	
	/**
	 * Generic Finder : team34DAOFinderImpl.
	 */
	@Autowired
	private Team34DAOFinderImpl team34DAOFinderImpl;
	
	
	// Initialise all the instances for UC34_PaginationTableController
		// Initialize the instance allTeams for the state lnk_paginatorbdd
		private List allTeams; // Initialize the instance allTeams for UC34_PaginationTableController
						// Declare the instance teamDetails
		private Team34BO teamDetails;
		// Declare the instance selectedTeam
		private Team34BO selectedTeam;
			/**
	 * Operation : lnk_view
 	 * @param model : 
 	 * @param uC34_PaginationTable : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC34_PaginationTable/lnk_view.html",method = RequestMethod.POST)
	public String lnk_view(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC34_PaginationTableForm") UC34_PaginationTableForm  uC34_PaginationTableForm, final BindingResult bindingResult ,final @RequestParam(ID_REQUEST_PARAM) Integer index){
		
		
		
		LOGGER.info("Begin method : lnk_view"); 
		infoLogger(uC34_PaginationTableForm); 
		uC34_PaginationTableForm.setAlwaysCallPreControllers(false); 
		// initialization for lnk_view
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state lnk_view if the validation fail, you will be forward to the screen : "/presentation/standardfeatures/application/datagrid/uc34_paginationtable/UC34_PaginationTable"
		if(errorsMessages(request,response,uC34_PaginationTableForm,bindingResult,"selectedTeam", uC34_PaginationTableValidator, customDateEditorsUC34_PaginationTableController)){ 
			return "/presentation/standardfeatures/application/datagrid/uc34_paginationtable/UC34_PaginationTable"; 
		}

										 callFindteamByID(request,uC34_PaginationTableForm , index);
											// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.application.datagrid.uc34_paginationtable.UC34_ViewTeamForm or not from lnk_view
			final UC34_ViewTeamForm uC34_ViewTeamForm =  new UC34_ViewTeamForm();
			// Populate the destination form with all the instances returned from lnk_view.
			final IForm uC34_ViewTeamForm2 = populateDestinationForm(request.getSession(), uC34_ViewTeamForm); 
					
			// Add the form to the model.
			model.addAttribute("uC34_ViewTeamForm", uC34_ViewTeamForm2); 
			
			request.getSession().setAttribute("uC34_ViewTeamForm", uC34_ViewTeamForm2);
			
			// "OK" CASE => destination screen path from lnk_view
			LOGGER.info("Go to the screen 'UC34_ViewTeam'.");
			// Redirect (PRG) from lnk_view
			destinationPath =  "redirect:/presentation/standardfeatures/application/datagrid/uc34_paginationtable/UC34_ViewTeam.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_view 
	}
	
	
	/**
	* This method initialise the form : UC34_PaginationTableForm 
	* @return UC34_PaginationTableForm
	*/
	@ModelAttribute("UC34_PaginationTableFormInit")
	public void initUC34_PaginationTableForm(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, U_C34__PAGINATION_TABLE_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method UC34_PaginationTableForm."); //for lnk_view 
		}
		UC34_PaginationTableForm uC34_PaginationTableForm;
	
		if(request.getSession().getAttribute(U_C34__PAGINATION_TABLE_FORM) != null){
			uC34_PaginationTableForm = (UC34_PaginationTableForm)request.getSession().getAttribute(U_C34__PAGINATION_TABLE_FORM);
		} else {
			uC34_PaginationTableForm = new UC34_PaginationTableForm();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form UC34_PaginationTableForm.");
		}
		uC34_PaginationTableForm = (UC34_PaginationTableForm)populateDestinationForm(request.getSession(), uC34_PaginationTableForm);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  UC34_PaginationTableForm.");
		}
		model.addAttribute(U_C34__PAGINATION_TABLE_FORM, uC34_PaginationTableForm);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param UC34_PaginationTableForm : The sceen form.
	 */
	@RequestMapping(value = "/UC34_PaginationTable.html" ,method = RequestMethod.GET)
	public void prepareUC34_PaginationTable(final HttpServletRequest request,final  Model model,final  @ModelAttribute("UC34_PaginationTableFormInit") UC34_PaginationTableForm uC34_PaginationTableForm){
		
		UC34_PaginationTableForm currentUC34_PaginationTableForm = uC34_PaginationTableForm;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(U_C34__PAGINATION_TABLE_FORM) == null){
			if(currentUC34_PaginationTableForm!=null){
				request.getSession().setAttribute(U_C34__PAGINATION_TABLE_FORM, currentUC34_PaginationTableForm);
			}else {
				currentUC34_PaginationTableForm = new UC34_PaginationTableForm();
				request.getSession().setAttribute(U_C34__PAGINATION_TABLE_FORM, currentUC34_PaginationTableForm);	
			}
		} else {
			currentUC34_PaginationTableForm = (UC34_PaginationTableForm) request.getSession().getAttribute(U_C34__PAGINATION_TABLE_FORM);
		}

		try {
			List allTeams = team34DAOFinderImpl.findAll();
			put(request.getSession(), ALL_TEAMS, allTeams);
		} catch (ApplicationException e) {
			errorLogger("An error while calling the generic finder for the instance : allTeams.",e);
		}
				currentUC34_PaginationTableForm = (UC34_PaginationTableForm)populateDestinationForm(request.getSession(), currentUC34_PaginationTableForm);
		// Call all the Precontroller.
	currentUC34_PaginationTableForm.setAlwaysCallPreControllers(true);
		model.addAttribute(U_C34__PAGINATION_TABLE_FORM, currentUC34_PaginationTableForm);
		request.getSession().setAttribute(U_C34__PAGINATION_TABLE_FORM, currentUC34_PaginationTableForm);
		
	}
	
										/**
	 * method callFindteamByID
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 * @param index Integer the current index 
	 */
	private void callFindteamByID(HttpServletRequest request,IForm form,Integer index) {
		teamDetails = (Team34BO)get(request.getSession(),form, "teamDetails");  
										 
					if(index!=null){  
			 selectedTeam = (Team34BO)((List)get(request.getSession(),form, "allTeams")).get(index);  
		} else {
			 selectedTeam= null; 
		}
				 
					try {
				// executing FindteamByID in lnk_view
				teamDetails = 	serviceTeam34.teamFindByID(
			selectedTeam
			);  
 
				put(request.getSession(), TEAM_DETAILS,teamDetails);
								// processing variables FindteamByID in lnk_view
				put(request.getSession(), SELECTED_TEAM,selectedTeam); 
			
			} catch (ApplicationException e) { 
				// error handling for operation teamFindByID called FindteamByID
				errorLogger("An error occured during the execution of the operation : teamFindByID",e); 
		
			}
	}
		
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("UC34_PaginationTableController [ ");
			strBToS.append(ALL_TEAMS);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(allTeams);
			strBToS.append(END_TO_STRING_DELIMITER);
						strBToS.append(TEAM_DETAILS);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(teamDetails);
			strBToS.append(END_TO_STRING_DELIMITER);
			strBToS.append(SELECTED_TEAM);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(selectedTeam);
			strBToS.append(END_TO_STRING_DELIMITER);
		// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(uC34_PaginationTableValidator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsUC34_PaginationTableController!=null); 
		return strBToS.toString();
	}
}
