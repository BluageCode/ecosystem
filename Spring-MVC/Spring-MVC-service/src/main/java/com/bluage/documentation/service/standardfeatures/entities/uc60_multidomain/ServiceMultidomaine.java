/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.service.standardfeatures.entities.uc60_multidomain;


import javax.inject.Inject;
import javax.inject.Named;

import com.bluage.documentation.business.standardfeatures.entities.uc60_multidomain.teams.bos.Team60BO;
import com.bluage.documentation.business.standardfeatures.entities.uc60_multidomain.teams.entities.daofinder.Team60DAOFinderImpl;
import com.bluage.documentation.service.AbstractService;
import com.netfective.bluage.core.exception.ApplicationException;

/**
 * Defines  application services for ServiceMultidomaine
  */
@Named("com.bluage.documentation.service.standardfeatures.entities.uc60_multidomain.ServiceMultidomaine")
public class ServiceMultidomaine  extends AbstractService{


			@Inject
		@Named ("com.bluage.documentation.business.standardfeatures.entities.uc60_multidomain.teams.entities.daofinder.Team60DAOFinderImpl")
		private Team60DAOFinderImpl team60BOFinder;
	


	/**
	 *  Default constructor 
	 */
	public ServiceMultidomaine() {
		super(); // Call super for default constructor : ServiceMultidomaine
	}
	


			
				
	/**    
	 * Operation Crud
	 * @param bo {@link Team60BO}
		 * @return {@link Team60BO}
	 */
	public Team60BO teamFindByID(Team60BO bo )
 throws ApplicationException {

					return (Team60BO)team60BOFinder.findByPrimaryKey(getBoId(bo));
	}
	/**
	 *  Return the primary key from business object.
	 */
	private Object getBoId(Team60BO bo) {
				return bo.getId();
		}

		}
