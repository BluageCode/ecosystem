<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file="/include.jsp"  %>
<html >
<head >
<title  > 
<fmt:message key="cUC37_Teams_BLU_AGE_DOCUMENTATION"/>
</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"  > 
</meta>
<link href="<c:url value= "/presentation/_style/_style.css"/>" rel="stylesheet" type="text/css"  /> 
<link href="<c:url value= "/presentation/_include/jquery/rd_lib.css"/>" rel="stylesheet" type="text/css"  /> 
<script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/jquery.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/rd_setup.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/rd_lib.js"/>" space="preserve"  ></script><link rel="shortcut icon" href="<c:url value= "/presentation/_style/favicon.ico"/>"  /> 

</head>

<body class=" " > 
<div id="rd_ghost"  > 
<small  > 
<fmt:message key="cUC37_Teams___CSS_DEACTIVATED"/>
</small>
</div>
<div id="rd_jghost"  > 
<small  > 
<fmt:message key="cUC37_Teams___JAVASCRIPT_DEACTIVATED"/>
</small>
</div>
<div id="shape"  > 
<div class="ui-layout-center"  > 
<div class="east-north"  > 
	<form:form id="uC37_Teams" commandName="uC37_TeamsForm"  enctype="application/x-www-form-urlencoded" method="post">
<h2 class="rd_first"  > 
<fmt:message key="cUC37_Teams_STANDARD_FEATURES__APPLICATIO"/>
</h2>
<display:table uid="tab_teams"  sort="list" requestURI="" name="sessionScope.uC37_TeamsForm.teams" class="rd_tabledata"  >
	<c:set   var="rownum">
		<%=(tab_teams !=	null) ? new Integer(tab_teams_rowNum.intValue() - 1) : new Integer(0) %>
	</c:set>
	<c:set   var="idRow">
		<%=(tab_teams != null ) ? tab_teams_rowNum.intValue() - 1 : -1 %>
	</c:set>
	<c:set   var="idTab">
		<%=(tab_teams != null ) ? "tab_teams" : null %>
	</c:set>
	<c:set var="tableHashId">
		<%= new org.displaytag.util.ParamEncoder("tab_teams").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE) %>
	</c:set>
	<c:set var="tableHashId2">
		<%=request.getParameter(new org.displaytag.util.ParamEncoder("tab_teams").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE)) %>
	</c:set>	
	
<display:column format=""  titleKey="cUC37_Teams_NAME">
		<span id="txt_name">
	<c:out value="${tab_teams.name}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="cUC37_Teams_CITY">
		<span id="txt_city">
	<c:out value="${tab_teams.city}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="cUC37_Teams_VIEW">
<a id="lnk_view[${rownum}]"onclick="javascript:btn_click_cExecuteUC37_Teams(this.href);return false;"			href="<c:url value="/presentation/standardfeatures/application/datagrid/uc37_collectionorder/UC37_Teams/lnk_view.html?id="/><c:out value="${idRow}" />&tab=<c:out value="tab_teams" />&<c:out value="${tableHashId}"/>=<c:out value="${tableHashId2}"/>"
	 shape="rect"  >									 
<fmt:message key="cUC37_Teams__VIEW_"/>
</a>
</display:column>
</display:table>
	</form:form>
	<script language="javascript">
		function btn_click_cExecuteUC37_Teams(url) {
			UC37_Teamsvar = document.getElementById('uC37_Teams');
			UC37_Teamsvar.setAttribute('action', url);
			UC37_Teamsvar.submit();
		}
		function btn_click_cSetActionUC37_Teams(url) {
			UC37_Teamsvar = document.getElementById('uC37_Teams');
			UC37_Teamsvar.setAttribute('action', url);
		}
	</script>
</div>
<div class="east-center"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/application/datagrid/uc37_collectionorder/UC37_Teamscmp_tabs" />
</div>
</div>
<div class="ui-layout-north"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/application/datagrid/uc37_collectionorder/UC37_Teamscmp_header" />
</div>
<div class="ui-layout-south"  > 
<fmt:message key="cUC37_Teams___"/>
<span id="rd_jversion"  > 
<fmt:message key="cUC37_Teams_BLU_AGE_CORPORATION"/>
</span>
</div>
<div class="ui-layout-west"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/application/datagrid/uc37_collectionorder/UC37_Teamscmp_treeview" />
</div>
</div>
</body>
</html>
