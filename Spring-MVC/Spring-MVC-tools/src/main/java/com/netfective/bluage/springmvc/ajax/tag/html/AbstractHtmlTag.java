/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.netfective.bluage.springmvc.ajax.tag.html;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.BodyTagSupport;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.log4j.Logger;

import com.netfective.bluage.springmvc.ajax.util.Constants;
import com.netfective.bluage.springmvc.ajax.util.Utils;
/**
 * Class HtmlTag
 * @superclass BodyTagSupport
 * @interface Constants
 * @author NETFECTIVE TECHNOLOGY
 */
public abstract class AbstractHtmlTag extends BodyTagSupport implements Constants {
    /**
     * LOGGER.
     */
    private static final  Logger LOGGER = Logger.getLogger(AbstractHtmlTag.class);

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	* Class for loop tag support
	*/
	private Class loopTagSupportClass;
	
	/**
	 * idRow parameter 
	 */
	private String idRow;

	/**
	* Method : getter for status of loop tag support
	*/
	private Method loopTagSupportGetStatus;

	/**
	* Class for loop tag
	*/
	private Class loopTagStatusClass;

	/**
	* Method : getter for status of loop tag
	*/
	private Method loopTagStatusGetIndex;

	/**
	* Flags : true if the step have been already done
	*/
	private boolean triedJstlInit;
	private boolean triedJstlSuccess;

	/**
	* Tag attributes
	*/
	private Map attributes;

	/**
	* Style
	*/
	private String styleClass;
	private String style;

	private boolean indexed;

	/**
	 * Initialisation of "attributes"
	 */
	protected void populateAttributes() {
		attributes = new HashMap();
		attributes.put(HTML_STYLE, style);
		attributes.put(HTML_STYLE_CLASS, styleClass);
	}
	
	/**
	* Get values and set values for following class attributes :
	*	loopTagSupportClass
	*	loopTagSupportGetStatus
	*	loopTagStatusClass
	*	loopTagStatusGetIndex
	*	triedJstlSuccess
	* @return Integer : return value from <code>loopTagStatusGetIndex.invoke(...)</code>
	*/
	private Integer getJstlLoopIndex() {
		Integer indextToReturn = null;
		if (!triedJstlInit) {
			triedJstlInit = true;
			try {
				loopTagSupportClass = Utils.applicationClass("javax.servlet.jsp.jstl.core.LoopTagSupport");
				loopTagSupportGetStatus = loopTagSupportClass.getDeclaredMethod("getLoopStatus", (Class[]) null);
				loopTagStatusClass = Utils.applicationClass("javax.servlet.jsp.jstl.core.LoopTagStatus");
				loopTagStatusGetIndex = loopTagStatusClass.getDeclaredMethod("getIndex", (Class[]) null);
				triedJstlSuccess = true;
			}
			// These just mean that JSTL isn t loaded, so ignore
			catch (ClassNotFoundException e) {
				LOGGER.error(e.getMessage(), e);
			} catch (NoSuchMethodException e) {
				LOGGER.error(e.getMessage(), e);
			}
		}
		if (triedJstlSuccess) {
			try {
				final Object loopTag = findAncestorWithClass(this, loopTagSupportClass);
				if (loopTag == null) {
					indextToReturn = null;
				}
				final Object status = loopTagSupportGetStatus.invoke(loopTag, (Object[]) null);
				indextToReturn = (Integer) loopTagStatusGetIndex.invoke(status, (Object[]) null);
			} catch (IllegalAccessException e) {
				LOGGER.error(e.getMessage(), e);
			} catch (IllegalArgumentException e) {
				LOGGER.error(e.getMessage(), e);
			} catch (InvocationTargetException e) {
				LOGGER.error(e.getMessage(), e);
			}
		}
		return indextToReturn;
	}

	/**
	* Prepare id
	* @param String :
	*			index
	*/
	protected void prepareId(final String index) {
		id += "[" + index + "]";
	}

	/**
	 * Appends id with index in brackets for tags with true value in indexed
	 * attribute.
	 * 
	 * @exception JspException
	 *                if indexed tag used outside of iterate tag.
	 * @throws NoSuchMethodException
	 * @throws InvocationTargetException
	 * @throws IllegalAccessException
	 */
	protected void prepareIndex() throws JspException {
		String index = null;
		// look for outer iterate tag
		index = getClassTagProperty(ITERATE_TAG_CLASS_NAME, "index");
		if (index != null) {
			prepareId(index);
		} else {
			// look for outer jstl tag
			final Integer jstlIndex = Integer.valueOf(getIdRow());
			if (jstlIndex != null) {
				prepareId(jstlIndex.toString());
			} else {
				// look for outer table tag
				index = getClassTagProperty(TABLE_TAG_CLASS_NAME, "index");
				if (index != null) {
					prepareId(index);
				} else {
					// this tag should only be nested in iteratetag, if it s not, throw exception
					throw new JspException("indexed.noEnclosingIterate");
				}
			}
		}
	}
	
	/**
	* Return a property value for the class named <code>className</code>  
	* @param String :
	*			className
	* @param String :
	*			property
	* @return String
	* @throw JspException
	*/
	protected String getClassTagProperty(final String className,final String property) throws JspException{
		
		// load tag class by class name
		final Class tagClass = Utils.tagApplicationClass(className);
		
		Object tag = null;
		if (tagClass != null){
			tag = findAncestorWithClass(this, tagClass);
		}

		if (tag != null) {
			try {
				return PropertyUtils.getProperty(tag, property).toString();
			} catch (IllegalAccessException e) {
				throw new JspException(e);
			}
			catch (InvocationTargetException e) {
				throw new JspException(e);
			}
			catch (NoSuchMethodException e) {
				throw new JspException(e);
			}
		}
		
		return null;
	}

	/**
	* This method prints a start tag on output of <code>pageContext</code>
	* @return int
	*/
	public int doStartTag() throws JspException {
		populateAttributes();
		if (indexed){
			prepareIndex();
		}

		return super.doStartTag();
	}

	// ------------------------------------------------------------------------Getter_Setter

	/**
	* Getter for styleClass
	* @return String
	*/
	public String getStyleClass() {
		return styleClass;
	}

	/**
	* Setter for styleClass
	* @param String :
	*			styleClass
	*/
	public void setStyleClass(final String styleClass) {
		this.styleClass = styleClass;
	}

	/**
	* Getter for style
	* @return String
	*/
	public String getStyle() {
		return style;
	}

	/**
	* Setter for style
	* @param String :
	*			style
	*/
	public void setStyle(final String style) {
		this.style = style;
	}
	
	/**
	* Getter for indexed
	* @return boolean
	*/
	public boolean isIndexed() {
		return indexed;
	}

	/**
	* Setter for indexed
	* @param boolean :
	*			indexed
	*/
	public void setIndexed(final boolean indexed) {
		this.indexed = indexed;
	}

	/**
	 * getter for attributes
	 * @return attributes Map
	 */
	public Map getAttributes() {
		return attributes;
	}

	/**
	 * setter for attributes
	 * @param attributes Map
	 */
	public void setAttributes(final Map attributes) {
		this.attributes = attributes;
	}

	/**
	 * getter for loopTagSupportClass
	 * @return loopTagSupportClass Class
	 */
	public Class getLoopTagSupportClass() {
		return loopTagSupportClass;
	}

	/**
	 * setter for loopTagSupportClass
	 * @param loopTagSupportClass Class
	 */
	public void setLoopTagSupportClass(final Class loopTagSupportClass) {
		this.loopTagSupportClass = loopTagSupportClass;
	}

	/**
	 * getter for loopTagSupportClass
	 * @return loopTagSupportClass Method
	 */
	public Method getLoopTagSupportGetStatus() {
		return loopTagSupportGetStatus;
	}

	/**
	 * setter for loopTagSupportClass	
	 * @param loopTagSupportGetStatus Method
	 */
	public void setLoopTagSupportGetStatus(final Method loopTagSupportGetStatus) {
		this.loopTagSupportGetStatus = loopTagSupportGetStatus;
	}

	/**
	 * getter for loopTagStatusClass 
	 * @return loopTagStatusClass Class
	 */
	public Class getLoopTagStatusClass() {
		return loopTagStatusClass;
	}

	/**
	 * setter for loopTagStatusClass
	 * @param loopTagStatusClass Class
	 */
	public void setLoopTagStatusClass(final Class loopTagStatusClass) {
		this.loopTagStatusClass = loopTagStatusClass;
	}

	/**
	 * getter for loopTagStatusGetIndex
	 * @return loopTagStatusGetIndex Method
	 */
	public Method getLoopTagStatusGetIndex() {
		return loopTagStatusGetIndex;
	}

	/**
	 * setter for loopTagStatusGetIndex
	 * @param loopTagStatusGetIndex Method
	 */
	public void setLoopTagStatusGetIndex(final Method loopTagStatusGetIndex) {
		this.loopTagStatusGetIndex = loopTagStatusGetIndex;
	}

	/**
	 * getter for triedJstlInit
	 * @return triedJstlInit boolean
	 */
	public boolean isTriedJstlInit() {
		return triedJstlInit;
	}

	/**
	 * setter for triedJstlInit
	 * @param triedJstlInit boolean
	 */
	public void setTriedJstlInit(final boolean triedJstlInit) {
		this.triedJstlInit = triedJstlInit;
	}

	/**
	 * getter for triedJstlSuccess
	 * @return triedJstlSuccess boolean
	 */
	public boolean isTriedJstlSuccess() {
		return triedJstlSuccess;
	}

	/**
	 * setter for triedJstlSuccess
	 * @param triedJstlSuccess boolean
	 */
	public void setTriedJstlSuccess(final boolean triedJstlSuccess) {
		this.triedJstlSuccess = triedJstlSuccess;
	}
	
	/**
	 * getter for idRow
	 * @return String
	 */
	public String getIdRow() {
		return idRow;
	}

	/**
	 * setter for idRow
	 * @param String
	 */
	public void setIdRow(String idRow) {
		this.idRow = idRow;
	}
	
}
