ALTER TABLE player44 DROP CONSTRAINT player44_team44_fkc;

ALTER TABLE team44 DROP CONSTRAINT team44_pkey;

ALTER TABLE player44 DROP CONSTRAINT player44_pkey;

DROP TABLE team44;

DROP TABLE player44;

CREATE TABLE team44 (
    id bigint NOT NULL,
    version integer NOT NULL,
    city character varying(1024),
    name character varying(1024)
);



CREATE TABLE player44 (
    id bigint NOT NULL,
    version integer NOT NULL,
    first_name character varying(30) NOT NULL,
    last_name character varying(30) NOT NULL,
    date_of_birth date,
    estimated_value real,
    weight integer,
    height integer,
    rookie boolean,
    team44_fk bigint
);



INSERT INTO player44 VALUES (287, 0, 'Owen', 'Little', '1981-10-27', 45.869999, 135, 183, false, 5);
INSERT INTO player44 VALUES (288, 0, 'Irvin', 'Lee', '1988-02-25', 10.61, 88, 157, false, 1);
INSERT INTO player44 VALUES (289, 0, 'Wayne', 'Rose', '1981-10-28', 20.41, 115, 244, false, 1);
INSERT INTO player44 VALUES (290, 0, 'Lucas', 'Williamson', '1983-06-29', 44.360001, 73, 151, false, 1);
INSERT INTO player44 VALUES (291, 0, 'Walter', 'Reyes', '1984-12-09', 46.150002, 104, 193, false, 1);
INSERT INTO player44 VALUES (292, 0, 'Kirk', 'Cox', '1973-05-10', 26.58, 60, 179, false, 3);
INSERT INTO player44 VALUES (293, 0, 'Arthur', 'Payne', '1981-11-20', 30.860001, 66, 190, false, 5);
INSERT INTO player44 VALUES (294, 0, 'Leroy', 'Moore', '1987-07-26', 45.799999, 89, 183, false, 2);
INSERT INTO player44 VALUES (295, 0, 'Milton', 'Johnson', '1969-07-02', 22.219999, 68, 205, false, 5);
INSERT INTO player44 VALUES (296, 0, 'Sammy', 'Mendez', '1986-07-01', 2.27, 96, 234, false, 2);
INSERT INTO player44 VALUES (297, 0, 'Rafael', 'Steele', '1968-11-25', 15.48, 66, 153, false, 1);
INSERT INTO player44 VALUES (298, 0, 'Bob', 'Jacobs', '1988-05-21', 13.23, 72, 172, false, 2);

INSERT INTO team44 VALUES (1, 0, 'Charlotte', 'Tigers');
INSERT INTO team44 VALUES (2, 0, 'Kansas City', 'Bears');
INSERT INTO team44 VALUES (3, 0, 'Detroit', 'Chargers');
INSERT INTO team44 VALUES (4, 0, 'Detroit', 'Hawks');
INSERT INTO team44 VALUES (5, 0, 'Omaha', 'Braves');

ALTER TABLE ONLY team44
    ADD CONSTRAINT team44_pkey PRIMARY KEY (id);	
	
ALTER TABLE ONLY player44
    ADD CONSTRAINT player44_pkey PRIMARY KEY (id);
	
ALTER TABLE ONLY player44
    ADD CONSTRAINT player44_team44_fkc FOREIGN KEY (team44_fk) REFERENCES team44(id);