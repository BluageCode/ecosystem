package uc68;


import org.junit.Test;
import test.common.AbstractBADocTestCase;

public class ITuc68 extends AbstractBADocTestCase {
	
	@Override
	public  String getName(){
		return "UC-68";
	}


	@Test
	public void testUC68() throws Exception {
		// On teste l'UC68 - Standard Features/Sentites - Inheritance subclass
		
		selenium.open("");	
		selenium.click("link=Expand All");
		selenium.waitForPageToLoad("30000");
		selenium.click("link=UC68 - Inheritance subclass");
		
		verifyTrue(selenium.isElementPresent("//h2[contains(text(),\"Standard Features > Entities > UC68 > Inheritance subclass\")]"));
		// On v�rifie les donn�es du tableau Players.
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Ricky\")] and td/span[contains(text(),\"Dawson\")] and td/span[contains(text(),\"03/03/1990\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Oliver\")] and td/span[contains(text(),\"Frank\")] and td/span[contains(text(),\"10/31/1968\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Elbert\")] and td/span[contains(text(),\"Cole\")] and td/span[contains(text(),\"04/07/1965\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Abel\")] and td/span[contains(text(),\"Dennis\")] and td/span[contains(text(),\"04/16/1980\")]]"));
		selenium.click("xpath=//a[5]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Earl\")] and td/span[contains(text(),\"Russell\")] and td/span[contains(text(),\"12/15/1981\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Howard\")] and td/span[contains(text(),\"Ramos\")] and td/span[contains(text(),\"08/25/1970\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Allan\")] and td/span[contains(text(),\"Meyer\")] and td/span[contains(text(),\"02/14/1969\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Ruben\")] and td/span[contains(text(),\"Pope\")] and td/span[contains(text(),\"05/01/1985\")]]"));
		selenium.click("xpath=//a[7]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Lester\")] and td/span[contains(text(),\"Cross\")] and td/span[contains(text(),\"02/21/1976\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Keith\")] and td/span[contains(text(),\"Barnett\")] and td/span[contains(text(),\"11/10/1976\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Ivan\")] and td/span[contains(text(),\"Tate\")] and td/span[contains(text(),\"02/08/1972\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Sylvester\")] and td/span[contains(text(),\"Nash\")] and td/span[contains(text(),\"07/10/1965\")]]"));
		selenium.click("xpath=//a[7]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Michael\")] and td/span[contains(text(),\"Hodges\")] and td/span[contains(text(),\"04/01/1988\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Jerome\")] and td/span[contains(text(),\"Grant\")] and td/span[contains(text(),\"06/28/1982\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Woodrow\")] and td/span[contains(text(),\"Carpenter\")] and td/span[contains(text(),\"08/08/1987\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Roderick\")] and td/span[contains(text(),\"Robbins\")] and td/span[contains(text(),\"07/29/1975\")]]"));
		selenium.click("xpath=//a[7]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Christian\")] and td/span[contains(text(),\"Chavez\")] and td/span[contains(text(),\"05/17/1983\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Cecil\")] and td/span[contains(text(),\"Conner\")] and td/span[contains(text(),\"06/29/1978\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Nick\")] and td/span[contains(text(),\"Garner\")] and td/span[contains(text(),\"07/27/1992\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Tommy\")] and td/span[contains(text(),\"Wood\")] and td/span[contains(text(),\"01/20/1992\")]]"));
		// On retourne sur la premi�re page du tableau Players.
		selenium.click("xpath=//a[1]");
		selenium.waitForPageToLoad("30000");
		// On teste les donn�es pour le tableau Rookie.
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_rookie')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_rookie')]/tbody/tr[td/span[contains(text(),\"Ricky\")] and td/span[contains(text(),\"Dawson\")] and td/span[contains(text(),\"03/03/1990\")] and td/span[contains(text(),\"Orlando\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_rookie')]/tbody/tr[td/span[contains(text(),\"Elbert\")] and td/span[contains(text(),\"Cole\")] and td/span[contains(text(),\"04/07/1965\")] and td/span[contains(text(),\"Cambridge\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_rookie')]/tbody/tr[td/span[contains(text(),\"Howard\")] and td/span[contains(text(),\"Ramos\")] and td/span[contains(text(),\"08/25/1970\")] and td/span[contains(text(),\"Miami\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_rookie')]/tbody/tr[td/span[contains(text(),\"Ruben\")] and td/span[contains(text(),\"Pope\")] and td/span[contains(text(),\"05/01/1985\")] and td/span[contains(text(),\"Dallas\")]]"));
		selenium.click("xpath=(//a)[9]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_rookie')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_rookie')]/tbody/tr[td/span[contains(text(),\"Lester\")] and td/span[contains(text(),\"Cross\")] and td/span[contains(text(),\"02/21/1976\")] and td/span[contains(text(),\"San Fransisco\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_rookie')]/tbody/tr[td/span[contains(text(),\"Keith\")] and td/span[contains(text(),\"Barnett\")] and td/span[contains(text(),\"11/10/1976\")] and td/span[contains(text(),\"Houston\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_rookie')]/tbody/tr[td/span[contains(text(),\"Ivan\")] and td/span[contains(text(),\"Tate\")] and td/span[contains(text(),\"02/08/1972\")] and td/span[contains(text(),\"Los Angeles\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_rookie')]/tbody/tr[td/span[contains(text(),\"Michael\")] and td/span[contains(text(),\"Hodges\")] and td/span[contains(text(),\"04/01/1988\")] and td/span[contains(text(),\"San Antonio\")]]"));
		selenium.click("xpath=(//a)[11]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("2", selenium.getXpathCount("//table[contains(@id,'tab_rookie')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_rookie')]/tbody/tr[td/span[contains(text(),\"Woodrow\")] and td/span[contains(text(),\"Carpenter\")] and td/span[contains(text(),\"08/08/1987\")] and td/span[contains(text(),\"New York\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_rookie')]/tbody/tr[td/span[contains(text(),\"Roderick\")] and td/span[contains(text(),\"Robbins\")] and td/span[contains(text(),\"07/29/1975\")] and td/span[contains(text(),\"Chicago\")]]"));
		// On retourne sur la premi�re page du tableau Rookie.
		selenium.click("xpath=(//a)[7]");
		selenium.waitForPageToLoad("30000");
		// On teste les donn�es du tableau Professionals.
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_professionals')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_professionals')]/tbody/tr[td/span[contains(text(),\"Oliver\")] and td/span[contains(text(),\"Frank\")] and td/span[contains(text(),\"10/31/1968\")] and td/span[contains(text(),\"125000\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_professionals')]/tbody/tr[td/span[contains(text(),\"Abel\")] and td/span[contains(text(),\"Dennis\")] and td/span[contains(text(),\"04/16/1980\")] and td/span[contains(text(),\"150000\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_professionals')]/tbody/tr[td/span[contains(text(),\"Earl\")] and td/span[contains(text(),\"Russell\")] and td/span[contains(text(),\"12/15/1981\")] and td/span[contains(text(),\"175000\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_professionals')]/tbody/tr[td/span[contains(text(),\"Allan\")] and td/span[contains(text(),\"Meyer\")] and td/span[contains(text(),\"02/14/1969\")] and td/span[contains(text(),\"98000\")]]"));
		selenium.click("xpath=(//a)[13]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_professionals')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_professionals')]/tbody/tr[td/span[contains(text(),\"Sylvester\")] and td/span[contains(text(),\"Nash\")] and td/span[contains(text(),\"07/10/1965\")] and td/span[contains(text(),\"123000\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_professionals')]/tbody/tr[td/span[contains(text(),\"Jerome\")] and td/span[contains(text(),\"Grant\")] and td/span[contains(text(),\"06/28/1982\")] and td/span[contains(text(),\"54000\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_professionals')]/tbody/tr[td/span[contains(text(),\"Christian\")] and td/span[contains(text(),\"Chavez\")] and td/span[contains(text(),\"05/17/1983\")] and td/span[contains(text(),\"32000\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_professionals')]/tbody/tr[td/span[contains(text(),\"Cecil\")] and td/span[contains(text(),\"Conner\")] and td/span[contains(text(),\"06/29/1978\")] and td/span[contains(text(),\"410000\")]]"));
		selenium.click("xpath=(//a)[15]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("2", selenium.getXpathCount("//table[contains(@id,'tab_professionals')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_professionals')]/tbody/tr[td/span[contains(text(),\"Nick\")] and td/span[contains(text(),\"Garner\")] and td/span[contains(text(),\"07/27/1992\")] and td/span[contains(text(),\"80000\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_professionals')]/tbody/tr[td/span[contains(text(),\"Tommy\")] and td/span[contains(text(),\"Wood\")] and td/span[contains(text(),\"01/20/1992\")] and td/span[contains(text(),\"203000\")]]"));
		// On retourne sur la premi�re page du tableau Professionals.
		selenium.click("xpath=(//a)[11]");
		selenium.waitForPageToLoad("30000");
	}


}
