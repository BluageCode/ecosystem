/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.services.sql.uc56_sqloperation ;

// Import declaration.
// Java imports.
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.bluage.documentation.business.standardfeatures.services.sql.uc56_sqloperation.bos.Team56BO;
import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;

/**
 * Class : UC56_LoadPlayersController
 */
@Controller
@Scope("prototype")
@SessionAttributes("uC56_LoadPlayersForm")
@RequestMapping(value= "/presentation/standardfeatures/services/sql/uc56_sqloperation")
public class UC56_LoadPlayersController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC56_LoadPlayersController.class);
	
	//Current form name
	private static final String U_C56__LOAD_PLAYERS_FORM = "uC56_LoadPlayersForm";

	//CONSTANT: team
	private static final String TEAM = "team";
		//CONSTANT: players table or repeater.
	private static final String PLAYERS = "players";
				
	/**
	 * Property:customDateEditorsUC56_LoadPlayersController
	 */
	final private Map<String,CustomDateEditor> customDateEditorsUC56_LoadPlayersController = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:uC56_LoadPlayersValidator
	 */
	final private UC56_LoadPlayersValidator uC56_LoadPlayersValidator = new UC56_LoadPlayersValidator();
	
	
	// Initialise all the instances for UC56_LoadPlayersController
		// Initialize the instance team of type com.bluage.documentation.business.standardfeatures.services.sql.uc56_sqloperation.bos.Team56BO with the value : null for UC56_LoadPlayersController
		private Team56BO team;
			// Initialize the instance players for the state lnk_searchTeamsPostProcess
		private List players; // Initialize the instance players for UC56_LoadPlayersController
				
	/**
	* This method initialise the form : UC56_LoadPlayersForm 
	* @return UC56_LoadPlayersForm
	*/
	@ModelAttribute("UC56_LoadPlayersFormInit")
	public void initUC56_LoadPlayersForm(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, U_C56__LOAD_PLAYERS_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method UC56_LoadPlayersForm."); //for lnk_searchTeamsPostProcess 
		}
		UC56_LoadPlayersForm uC56_LoadPlayersForm;
	
		if(request.getSession().getAttribute(U_C56__LOAD_PLAYERS_FORM) != null){
			uC56_LoadPlayersForm = (UC56_LoadPlayersForm)request.getSession().getAttribute(U_C56__LOAD_PLAYERS_FORM);
		} else {
			uC56_LoadPlayersForm = new UC56_LoadPlayersForm();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form UC56_LoadPlayersForm.");
		}
		uC56_LoadPlayersForm = (UC56_LoadPlayersForm)populateDestinationForm(request.getSession(), uC56_LoadPlayersForm);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  UC56_LoadPlayersForm.");
		}
		model.addAttribute(U_C56__LOAD_PLAYERS_FORM, uC56_LoadPlayersForm);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param UC56_LoadPlayersForm : The sceen form.
	 */
	@RequestMapping(value = "/UC56_LoadPlayers.html" ,method = RequestMethod.GET)
	public void prepareUC56_LoadPlayers(final HttpServletRequest request,final  Model model,final  @ModelAttribute("UC56_LoadPlayersFormInit") UC56_LoadPlayersForm uC56_LoadPlayersForm){
		
		UC56_LoadPlayersForm currentUC56_LoadPlayersForm = uC56_LoadPlayersForm;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(U_C56__LOAD_PLAYERS_FORM) == null){
			if(currentUC56_LoadPlayersForm!=null){
				request.getSession().setAttribute(U_C56__LOAD_PLAYERS_FORM, currentUC56_LoadPlayersForm);
			}else {
				currentUC56_LoadPlayersForm = new UC56_LoadPlayersForm();
				request.getSession().setAttribute(U_C56__LOAD_PLAYERS_FORM, currentUC56_LoadPlayersForm);	
			}
		} else {
			currentUC56_LoadPlayersForm = (UC56_LoadPlayersForm) request.getSession().getAttribute(U_C56__LOAD_PLAYERS_FORM);
		}

				currentUC56_LoadPlayersForm = (UC56_LoadPlayersForm)populateDestinationForm(request.getSession(), currentUC56_LoadPlayersForm);
		// Call all the Precontroller.
	currentUC56_LoadPlayersForm.setAlwaysCallPreControllers(true);
		model.addAttribute(U_C56__LOAD_PLAYERS_FORM, currentUC56_LoadPlayersForm);
		request.getSession().setAttribute(U_C56__LOAD_PLAYERS_FORM, currentUC56_LoadPlayersForm);
		
	}
	
			
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("UC56_LoadPlayersController [ ");
			strBToS.append(TEAM);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(team);
			strBToS.append(END_TO_STRING_DELIMITER);
				strBToS.append(PLAYERS);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(players);
			strBToS.append(END_TO_STRING_DELIMITER);
					// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(uC56_LoadPlayersValidator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsUC56_LoadPlayersController!=null); 
		return strBToS.toString();
	}
}
