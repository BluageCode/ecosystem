--<ScriptOptions statementTerminator=";"/>

ALTER TABLE application_user06 DROP CONSTRAINT application_user06_profile_fkc;

ALTER TABLE application_user06 DROP CONSTRAINT application_user06_pkey;

ALTER TABLE application_profile06 DROP CONSTRAINT application_profile06_pkey;

DROP TABLE application_profile06;

DROP TABLE application_user06;

CREATE TABLE application_profile06 (
		profile_id INT8 NOT NULL,
		version INT4 NOT NULL,
		value VARCHAR(1024)
	);

CREATE TABLE application_user06 (
		login VARCHAR(1024) NOT NULL,
		version INT4 NOT NULL,
		password VARCHAR(1024),
		first_name VARCHAR(1024),
		last_name VARCHAR(1024),
		profile_fk INT8 NOT NULL
	);


INSERT INTO application_profile06 VALUES (1, 0, 'administrator');
INSERT INTO application_profile06 VALUES (2, 0, 'user');


INSERT INTO application_user06 (login, version, password, first_name, last_name, profile_fk) VALUES ('admin', 0, 'admin', 'Christian', 'Robbins', 1);
INSERT INTO application_user06 (login, version, password, first_name, last_name, profile_fk) VALUES ('user', 0, 'user', 'Ricky', 'Dawson', 2);
INSERT INTO application_user06 (login, version, password, first_name, last_name, profile_fk) VALUES ('richard', 0, 'richard', 'Richard', 'Perry', 2);

ALTER TABLE application_user06 ADD CONSTRAINT application_user06_pkey PRIMARY KEY (login);

ALTER TABLE application_profile06 ADD CONSTRAINT application_profile06_pkey PRIMARY KEY (profile_id);

ALTER TABLE application_user06 ADD CONSTRAINT application_user06_profile_fkc FOREIGN KEY (profile_fk)
	REFERENCES application_profile06 (profile_id);
	