/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.application.datagrid.uc34_paginationtable ;


import org.apache.log4j.Logger;
import org.springframework.validation.Errors;

import com.netfective.bluage.core.validator.AbstractValidator;
/**
* Class :UC34_PaginationTableValidator
*/
public class UC34_PaginationTableValidator extends AbstractValidator{
	
	// LOGGER for the class UC34_PaginationTableValidator
	private static final Logger LOGGER = Logger.getLogger( UC34_PaginationTableValidator.class);
	
	
	/**
	* Operation validate for UC34_PaginationTableForm
	* @param obj : the current form (UC34_PaginationTableForm)
	* @param errors : The spring errors to return for the form UC34_PaginationTableForm
	*/
	public void validate(final Object obj,final Errors errors){
		LOGGER.info("Beginning method : validate"); // UC34_PaginationTableValidator
		UC34_PaginationTableForm cUC34_PaginationTableForm = (UC34_PaginationTableForm)obj; // UC34_PaginationTableValidator
		LOGGER.info("Ending method : validate the form "+ cUC34_PaginationTableForm.getClass().getName()); // UC34_PaginationTableValidator
	}

	/**
	* Method to implements to use spring validators (UC34_PaginationTableForm)
	* @param aClass : Class for the form UC34_PaginationTableForm
	* @return boolean
	*/
	public boolean supports(final Class aClass){
		return (new UC34_PaginationTableForm()).getClass().equals(aClass); // UC34_PaginationTableValidator
	}
}
