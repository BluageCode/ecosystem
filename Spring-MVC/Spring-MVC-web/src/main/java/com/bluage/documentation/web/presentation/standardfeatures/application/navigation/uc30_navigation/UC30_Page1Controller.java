/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.application.navigation.uc30_navigation ;

// Import declaration.
// Java imports.
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.bluage.documentation.business.standardfeatures.application.navigation.uc30_navigation.bos.Page30BO;
import com.bluage.documentation.business.standardfeatures.application.navigation.uc30_navigation.entities.daofinder.Page30DAOFinderImpl;
import com.bluage.documentation.service.standardfeatures.application.navigation.uc30_navigation.ServiceNavigation;
import com.netfective.bluage.core.exception.ApplicationException;
import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;
import com.netfective.bluage.core.web.IForm;

/**
 * Class : UC30_Page1Controller
 */
@Controller
@Scope("prototype")
@SessionAttributes("uC30_Page1Form")
@RequestMapping(value= "/presentation/standardfeatures/application/navigation/uc30_navigation")
public class UC30_Page1Controller extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC30_Page1Controller.class);
	
	//Current form name
	private static final String U_C30__PAGE1_FORM = "uC30_Page1Form";

	//CONSTANT: listPage
	private static final String LIST_PAGE = "listPage";
				//CONSTANT: value
	private static final String VALUE = "value";
	//CONSTANT: selectedPage
	private static final String SELECTED_PAGE = "selectedPage";
	
	/**
	 * Property:customDateEditorsUC30_Page1Controller
	 */
	final private Map<String,CustomDateEditor> customDateEditorsUC30_Page1Controller = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:uC30_Page1Validator
	 */
	final private UC30_Page1Validator uC30_Page1Validator = new UC30_Page1Validator();
	
	/**
	 * Service declaration : serviceNavigation.
	 */
	@Autowired
	private ServiceNavigation serviceNavigation;
	
	/**
	 * Generic Finder : page30DAOFinderImpl.
	 */
	@Autowired
	private Page30DAOFinderImpl page30DAOFinderImpl;
	
	
	// Initialise all the instances for UC30_Page1Controller
		// Initialize the instance listPage for the state lnk_loadTeams
		private List listPage; // Initialize the instance listPage for UC30_Page1Controller
						// Declare the instance value
		private Integer value;
		// Declare the instance selectedPage
		private Page30BO selectedPage;
			/**
	 * Operation : btn_execute
 	 * @param model : 
 	 * @param uC30_Page1 : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC30_Page1/btn_execute.html",method = RequestMethod.POST)
	public String btn_execute(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC30_Page1Form") UC30_Page1Form  uC30_Page1Form, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : btn_execute"); 
		infoLogger(uC30_Page1Form); 
		uC30_Page1Form.setAlwaysCallPreControllers(false); 
		// initialization for btn_execute
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state btn_execute if the validation fail, you will be forward to the screen : "/presentation/standardfeatures/application/navigation/uc30_navigation/UC30_Page1"
		if(errorsMessages(request,response,uC30_Page1Form,bindingResult,"", uC30_Page1Validator, customDateEditorsUC30_Page1Controller)){ 
			return "/presentation/standardfeatures/application/navigation/uc30_navigation/UC30_Page1"; 
		}

					 callGetvalueenterredfornavigationdecision(request,uC30_Page1Form );
													if(value == 1){

 callGoonpageHelloWorld(request,uC30_Page1Form );
											// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.application.navigation.uc30_navigation.UC30_Page2Form or not from btn_execute
			final UC30_Page2Form uC30_Page2Form =  new UC30_Page2Form();
			// Populate the destination form with all the instances returned from btn_execute.
			final IForm uC30_Page2Form2 = populateDestinationForm(request.getSession(), uC30_Page2Form); 
					
			// Add the form to the model.
			model.addAttribute("uC30_Page2Form", uC30_Page2Form2); 
			
			request.getSession().setAttribute("uC30_Page2Form", uC30_Page2Form2);
			
			// "OK" CASE => destination screen path from btn_execute
			LOGGER.info("Go to the screen 'UC30_Page2'.");
			// Redirect (PRG) from btn_execute
			destinationPath =  "redirect:/presentation/standardfeatures/application/navigation/uc30_navigation/UC30_Page2.html";
						
																	}
										if(value == 2){

 callGoonpagegoodbyWorld(request,uC30_Page1Form );
											// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.application.navigation.uc30_navigation.UC30_Page3Form or not from btn_execute
			final UC30_Page3Form uC30_Page3Form =  new UC30_Page3Form();
			// Populate the destination form with all the instances returned from btn_execute.
			final IForm uC30_Page3Form2 = populateDestinationForm(request.getSession(), uC30_Page3Form); 
					
			// Add the form to the model.
			model.addAttribute("uC30_Page3Form", uC30_Page3Form2); 
			
			request.getSession().setAttribute("uC30_Page3Form", uC30_Page3Form2);
			
			// "OK" CASE => destination screen path from btn_execute
			LOGGER.info("Go to the screen 'UC30_Page3'.");
			// Redirect (PRG) from btn_execute
			destinationPath =  "redirect:/presentation/standardfeatures/application/navigation/uc30_navigation/UC30_Page3.html";
						
																	}
						
	
	
		return destinationPath; // Returns the destination path for the state : btn_execute 
	}
	
	
	/**
	* This method initialise the form : UC30_Page1Form 
	* @return UC30_Page1Form
	*/
	@ModelAttribute("UC30_Page1FormInit")
	public void initUC30_Page1Form(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, U_C30__PAGE1_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method UC30_Page1Form."); //for btn_execute 
		}
		UC30_Page1Form uC30_Page1Form;
	
		if(request.getSession().getAttribute(U_C30__PAGE1_FORM) != null){
			uC30_Page1Form = (UC30_Page1Form)request.getSession().getAttribute(U_C30__PAGE1_FORM);
		} else {
			uC30_Page1Form = new UC30_Page1Form();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form UC30_Page1Form.");
		}
		uC30_Page1Form = (UC30_Page1Form)populateDestinationForm(request.getSession(), uC30_Page1Form);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  UC30_Page1Form.");
		}
		model.addAttribute(U_C30__PAGE1_FORM, uC30_Page1Form);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param UC30_Page1Form : The sceen form.
	 */
	@RequestMapping(value = "/UC30_Page1.html" ,method = RequestMethod.GET)
	public void prepareUC30_Page1(final HttpServletRequest request,final  Model model,final  @ModelAttribute("UC30_Page1FormInit") UC30_Page1Form uC30_Page1Form){
		
		UC30_Page1Form currentUC30_Page1Form = uC30_Page1Form;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(U_C30__PAGE1_FORM) == null){
			if(currentUC30_Page1Form!=null){
				request.getSession().setAttribute(U_C30__PAGE1_FORM, currentUC30_Page1Form);
			}else {
				currentUC30_Page1Form = new UC30_Page1Form();
				request.getSession().setAttribute(U_C30__PAGE1_FORM, currentUC30_Page1Form);	
			}
		} else {
			currentUC30_Page1Form = (UC30_Page1Form) request.getSession().getAttribute(U_C30__PAGE1_FORM);
		}

		try {
			List listPage = page30DAOFinderImpl.findAll();
			put(request.getSession(), LIST_PAGE, listPage);
		} catch (ApplicationException e) {
			errorLogger("An error while calling the generic finder for the instance : listPage.",e);
		}
		currentUC30_Page1Form = (UC30_Page1Form)populateDestinationForm(request.getSession(), currentUC30_Page1Form);
		// Call all the Precontroller.
	currentUC30_Page1Form.setAlwaysCallPreControllers(true);
		model.addAttribute(U_C30__PAGE1_FORM, currentUC30_Page1Form);
		request.getSession().setAttribute(U_C30__PAGE1_FORM, currentUC30_Page1Form);
		
	}
	
	/**
	 * method callGoonpageHelloWorld
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callGoonpageHelloWorld(HttpServletRequest request,IForm form) {
					try {
				// executing GoonpageHelloWorld in btn_execute
	serviceNavigation.doNothing(
	);  

								// processing variables GoonpageHelloWorld in btn_execute

			} catch (ApplicationException e) { 
				// error handling for operation doNothing called GoonpageHelloWorld
				errorLogger("An error occured during the execution of the operation : doNothing",e); 
		
			}
	}
					/**
	 * method callGetvalueenterredfornavigationdecision
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callGetvalueenterredfornavigationdecision(HttpServletRequest request,IForm form) {
		value = (Integer)get(request.getSession(),form, "value");  
										 
					selectedPage = (Page30BO)get(request.getSession(),form, "selectedPage");  
										 
					try {
				// executing Getvalueenterredfornavigationdecision in btn_execute
				value = 	serviceNavigation.getPage(
			selectedPage
			);  
 
				put(request.getSession(), VALUE,value);
								// processing variables Getvalueenterredfornavigationdecision in btn_execute
				put(request.getSession(), SELECTED_PAGE,selectedPage); 
			
			} catch (ApplicationException e) { 
				// error handling for operation getPage called Getvalueenterredfornavigationdecision
				errorLogger("An error occured during the execution of the operation : getPage",e); 
		
			}
	}
		/**
	 * method callGoonpagegoodbyWorld
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callGoonpagegoodbyWorld(HttpServletRequest request,IForm form) {
					try {
				// executing GoonpagegoodbyWorld in btn_execute
	serviceNavigation.doNothing(
	);  

								// processing variables GoonpagegoodbyWorld in btn_execute

			} catch (ApplicationException e) { 
				// error handling for operation doNothing called GoonpagegoodbyWorld
				errorLogger("An error occured during the execution of the operation : doNothing",e); 
		
			}
	}
		
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("UC30_Page1Controller [ ");
			strBToS.append(LIST_PAGE);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(listPage);
			strBToS.append(END_TO_STRING_DELIMITER);
						strBToS.append(VALUE);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(value);
			strBToS.append(END_TO_STRING_DELIMITER);
			strBToS.append(SELECTED_PAGE);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(selectedPage);
			strBToS.append(END_TO_STRING_DELIMITER);
		// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(uC30_Page1Validator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsUC30_Page1Controller!=null); 
		return strBToS.toString();
	}
}
