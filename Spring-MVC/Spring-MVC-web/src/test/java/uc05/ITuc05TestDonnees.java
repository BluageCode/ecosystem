package uc05;

import org.junit.Test;
import test.common.AbstractBADocTestCase;

public class ITuc05TestDonnees extends AbstractBADocTestCase {
	
	
	@Override
	public  String getName(){
		return "UC-05";
	}
	
	@Test
	public void testITuc05TestDonnees() throws Exception {
		// On teste l'UC05 - Getting started - Search
		// On teste les données de l'UC.
		// On teste les données hql
		selenium.open("");
		selenium.waitForPageToLoad("30000");
		selenium.click("link=Expand All");
		selenium.click("link=UC05 - Search");
		verifyTrue(selenium.isElementPresent("//h2[contains(text(),\"Getting started > UC05 > Search\")]"));
		selenium.click("//*[contains(@id,'lnk_searchCriteria')]");
		selenium.waitForPageToLoad("30000");
		// On vérifie que les informations sont bien affichées.
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_searchResult')]/tbody/tr[td/span[contains(text(),\"Davis\")] and td/span[contains(text(),\"Harry\")] and td/span[contains(text(),\"237\")] and td/span[contains(text(),\"Power Forward\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_searchResult')]/tbody/tr[td/span[contains(text(),\"Colon\")] and td/span[contains(text(),\"Loren\")] and td/span[contains(text(),\"221\")]and td/span[contains(text(),\"Small Forward\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_searchResult')]/tbody/tr[td/span[contains(text(),\"Clark\")] and td/span[contains(text(),\"Bob\")] and td/span[contains(text(),\"224\")] and td/span[contains(text(),\"Shooting Guard\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_searchResult')]/tbody/tr[td/span[contains(text(),\"Stokes\")] and td/span[contains(text(),\"Loren\")] and td/span[contains(text(),\"186\")] and td/span[contains(text(),\"Shooting Guard\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_searchResult')]/tbody/tr[td/span[contains(text(),\"Sandoval\")] and td/span[contains(text(),\"Shane\")] and td/span[contains(text(),\"182\")] and td/span[contains(text(),\"Power Forward\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_searchResult')]/tbody/tr[td/span[contains(text(),\"Davis\")] and td/span[contains(text(),\"Roosevelt\")] and td/span[contains(text(),\"235\")] and td/span[contains(text(),\"Center\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_searchResult')]/tbody/tr[td/span[contains(text(),\"Davis\")] and td/span[contains(text(),\"Alton\")] and td/span[contains(text(),\"196\")] and td/span[contains(text(),\"Center\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_searchResult')]/tbody/tr[td/span[contains(text(),\"Favis\")] and td/span[contains(text(),\"Marion\")] and td/span[contains(text(),\"196\")] and td/span[contains(text(),\"Center\")]]"));
		// On vérifie qu'on a le bon nombre de lignes.
		assertEquals("8", selenium.getXpathCount("//table[contains(@id,'tab_searchResult')]/tbody/tr"));
		// On teste les données :
//		selenium.click("link=UC05 - Search");
//		selenium.waitForPageToLoad("30000");
//		verifyTrue(selenium.isElementPresent("//h2[contains(text(),\"Search\")]"));
		// On selectionne la première position.
		selenium.select("//*[contains(@id,'select_position')]", "label=Point Guard");
		selenium.click("//*[contains(@id,'lnk_searchProperties')]");
		selenium.waitForPageToLoad("30000");
		// On vérifie que les données sont affichées correctement.
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_searchResult')]/tbody/tr[td/span[contains(text(),\"Point Guard\")]]"));
		// On selectionne la seconde position.
		selenium.select("//*[contains(@id,'select_position')]", "label=Shooting Guard");
		selenium.click("//*[contains(@id,'lnk_searchProperties')]");
		selenium.waitForPageToLoad("30000");
		// On vérifie que les données sont affichées correctement.
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_searchResult')]/tbody/tr[td/span[contains(text(),\"Clark\")] and td/span[contains(text(),\"Bob\")] and td/span[contains(text(),\"224\")] and td/span[contains(text(),\"Shooting Guard\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_searchResult')]/tbody/tr[td/span[contains(text(),\"Stokes\")] and td/span[contains(text(),\"Loren\")] and td/span[contains(text(),\"186\")] and td/span[contains(text(),\"Shooting Guard\")]]"));
		// On selectionne la troisième position.
		selenium.select("//*[contains(@id,'select_position')]", "label=Small Forward");
		selenium.click("//*[contains(@id,'lnk_searchProperties')]");
		selenium.waitForPageToLoad("30000");
		// On vérifie que les données sont bien affichées.
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_searchResult')]/tbody/tr[td/span[contains(text(),\"Colon\")] and td/span[contains(text(),\"Loren\")] and td/span[contains(text(),\"221\")]and td/span[contains(text(),\"Small Forward\")]]"));
		// On selectionne la quatrième position.
		selenium.select("//*[contains(@id,'select_position')]", "label=Power Forward");
		selenium.click("//*[contains(@id,'lnk_searchProperties')]");
		selenium.waitForPageToLoad("30000");
		// On vérifie que les données sont bien affichées.
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_searchResult')]/tbody/tr[td/span[contains(text(),\"Sandoval\")] and td/span[contains(text(),\"Shane\")] and td/span[contains(text(),\"182\")] and td/span[contains(text(),\"Power Forward\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_searchResult')]/tbody/tr[td/span[contains(text(),\"Davis\")] and td/span[contains(text(),\"Harry\")] and td/span[contains(text(),\"237\")] and td/span[contains(text(),\"Power Forward\")]]"));
		// On selectionne la dernière position.
		selenium.select("//*[contains(@id,'select_position')]", "label=Center");
		selenium.click("//*[contains(@id,'lnk_searchProperties')]");
		selenium.waitForPageToLoad("30000");
		// On vérifie que les données sont affichées correctement.
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_searchResult')]/tbody/tr[td/span[contains(text(),\"Davis\")] and td/span[contains(text(),\"Roosevelt\")] and td/span[contains(text(),\"235\")] and td/span[contains(text(),\"Center\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_searchResult')]/tbody/tr[td/span[contains(text(),\"Davis\")] and td/span[contains(text(),\"Alton\")] and td/span[contains(text(),\"196\")] and td/span[contains(text(),\"Center\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_searchResult')]/tbody/tr[td/span[contains(text(),\"Favis\")] and td/span[contains(text(),\"Marion\")] and td/span[contains(text(),\"196\")] and td/span[contains(text(),\"Center\")]]"));
	}

}
