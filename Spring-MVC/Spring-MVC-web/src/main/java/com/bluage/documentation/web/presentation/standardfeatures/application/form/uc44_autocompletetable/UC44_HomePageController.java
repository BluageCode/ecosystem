/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.application.form.uc44_autocompletetable ;

// Import declaration.
// Java imports.
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.bluage.documentation.business.standardfeatures.application.form.uc44_autocompletetable.bos.Team44BO;
import com.bluage.documentation.service.standardfeatures.application.form.uc44_autocompletetable.ServicePlayer44;
import com.bluage.documentation.service.standardfeatures.application.form.uc44_autocompletetable.ServiceTeam44;
import com.netfective.bluage.core.exception.ApplicationException;
import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;
import com.netfective.bluage.core.web.IForm;

/**
 * Class : UC44_HomePageController
 */
@Controller
@Scope("prototype")
@SessionAttributes("uC44_HomePageForm")
@RequestMapping(value= "/presentation/standardfeatures/application/form/uc44_autocompletetable")
public class UC44_HomePageController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC44_HomePageController.class);
	
	//Current form name
	private static final String U_C44__HOME_PAGE_FORM = "uC44_HomePageForm";

	//CONSTANT: selectedTeam
	private static final String SELECTED_TEAM = "selectedTeam";
	//CONSTANT: allPlayers
	private static final String ALL_PLAYERS = "allPlayers";
	//CONSTANT: instance_teams
	private static final String INSTANCE_TEAMS = "instance_teams";
	
	/**
	 * Property:customDateEditorsUC44_HomePageController
	 */
	final private Map<String,CustomDateEditor> customDateEditorsUC44_HomePageController = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:uC44_HomePageValidator
	 */
	final private UC44_HomePageValidator uC44_HomePageValidator = new UC44_HomePageValidator();
	
	/**
	 * Service declaration : servicePlayer44.
	 */
	@Autowired
	private ServicePlayer44 servicePlayer44;
	
	/**
	 * Service declaration : serviceTeam44.
	 */
	@Autowired
	private ServiceTeam44 serviceTeam44;
	
	
	// Initialise all the instances for UC44_HomePageController
		// Declare the instance selectedTeam
		private Team44BO selectedTeam;
		// Declare the instance allPlayers
		private List allPlayers;
		// Declare the instance instance_teams
		private List instance_teams;
					/**
	 * Operation : lnk_display
 	 * @param model : 
 	 * @param uC44_HomePage : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC44_HomePage/lnk_display.html",method = RequestMethod.GET)
	public String lnk_display(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC44_HomePageForm") UC44_HomePageForm  uC44_HomePageForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : lnk_display"); 
		infoLogger(uC44_HomePageForm); 
		uC44_HomePageForm.setAlwaysCallPreControllers(false); 
		// initialization for lnk_display
		init();
		
		String destinationPath = null; 
		

	 callLoadPlayers(request,uC44_HomePageForm );
			
 callFindallteams(request,uC44_HomePageForm );
			
 callInitTeam(request,uC44_HomePageForm );
											// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.application.form.uc44_autocompletetable.UC44_AutocompleteTableForm or not from lnk_display
			final UC44_AutocompleteTableForm uC44_AutocompleteTableForm =  new UC44_AutocompleteTableForm();
			// Populate the destination form with all the instances returned from lnk_display.
			final IForm uC44_AutocompleteTableForm2 = populateDestinationForm(request.getSession(), uC44_AutocompleteTableForm); 
					
			// Add the form to the model.
			model.addAttribute("uC44_AutocompleteTableForm", uC44_AutocompleteTableForm2); 
			
			request.getSession().setAttribute("uC44_AutocompleteTableForm", uC44_AutocompleteTableForm2);
			
			// "OK" CASE => destination screen path from lnk_display
			LOGGER.info("Go to the screen 'UC44_AutocompleteTable'.");
			// Redirect (PRG) from lnk_display
			destinationPath =  "redirect:/presentation/standardfeatures/application/form/uc44_autocompletetable/UC44_AutocompleteTable.html";
						
						
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_display 
	}
	
	
	/**
	* This method initialise the form : UC44_HomePageForm 
	* @return UC44_HomePageForm
	*/
	@ModelAttribute("UC44_HomePageFormInit")
	public void initUC44_HomePageForm(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, U_C44__HOME_PAGE_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method UC44_HomePageForm."); //for lnk_display 
		}
		UC44_HomePageForm uC44_HomePageForm;
	
		if(request.getSession().getAttribute(U_C44__HOME_PAGE_FORM) != null){
			uC44_HomePageForm = (UC44_HomePageForm)request.getSession().getAttribute(U_C44__HOME_PAGE_FORM);
		} else {
			uC44_HomePageForm = new UC44_HomePageForm();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form UC44_HomePageForm.");
		}
		uC44_HomePageForm = (UC44_HomePageForm)populateDestinationForm(request.getSession(), uC44_HomePageForm);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  UC44_HomePageForm.");
		}
		model.addAttribute(U_C44__HOME_PAGE_FORM, uC44_HomePageForm);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param UC44_HomePageForm : The sceen form.
	 */
	@RequestMapping(value = "/UC44_HomePage.html" ,method = RequestMethod.GET)
	public void prepareUC44_HomePage(final HttpServletRequest request,final  Model model,final  @ModelAttribute("UC44_HomePageFormInit") UC44_HomePageForm uC44_HomePageForm){
		
		UC44_HomePageForm currentUC44_HomePageForm = uC44_HomePageForm;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(U_C44__HOME_PAGE_FORM) == null){
			if(currentUC44_HomePageForm!=null){
				request.getSession().setAttribute(U_C44__HOME_PAGE_FORM, currentUC44_HomePageForm);
			}else {
				currentUC44_HomePageForm = new UC44_HomePageForm();
				request.getSession().setAttribute(U_C44__HOME_PAGE_FORM, currentUC44_HomePageForm);	
			}
		} else {
			currentUC44_HomePageForm = (UC44_HomePageForm) request.getSession().getAttribute(U_C44__HOME_PAGE_FORM);
		}

		currentUC44_HomePageForm = (UC44_HomePageForm)populateDestinationForm(request.getSession(), currentUC44_HomePageForm);
		// Call all the Precontroller.
	currentUC44_HomePageForm.setAlwaysCallPreControllers(true);
		model.addAttribute(U_C44__HOME_PAGE_FORM, currentUC44_HomePageForm);
		request.getSession().setAttribute(U_C44__HOME_PAGE_FORM, currentUC44_HomePageForm);
		
	}
	
	/**
	 * method callInitTeam
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callInitTeam(HttpServletRequest request,IForm form) {
		selectedTeam = (Team44BO)get(request.getSession(),form, "selectedTeam");  
										 
					try {
				// executing InitTeam in lnk_display
				selectedTeam = 	serviceTeam44.initTeam44(
	);  
 
				put(request.getSession(), SELECTED_TEAM,selectedTeam);
								// processing variables InitTeam in lnk_display

			} catch (ApplicationException e) { 
				// error handling for operation initTeam44 called InitTeam
				errorLogger("An error occured during the execution of the operation : initTeam44",e); 
		
			}
	}
		/**
	 * method callLoadPlayers
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callLoadPlayers(HttpServletRequest request,IForm form) {
		allPlayers = (List)get(request.getSession(),form, "allPlayers");  
										 
					try {
				// executing LoadPlayers in lnk_display
				allPlayers = 	servicePlayer44.loadPlayers44(
	);  
 
				put(request.getSession(), ALL_PLAYERS,allPlayers);
								// processing variables LoadPlayers in lnk_display

			} catch (ApplicationException e) { 
				// error handling for operation loadPlayers44 called LoadPlayers
				errorLogger("An error occured during the execution of the operation : loadPlayers44",e); 
		
			}
	}
		/**
	 * method callFindallteams
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callFindallteams(HttpServletRequest request,IForm form) {
		instance_teams = (List)get(request.getSession(),form, "instance_teams");  
										 
					try {
				// executing Findallteams in lnk_display
				instance_teams = 	serviceTeam44.team44FindAll(
	);  
 
				put(request.getSession(), INSTANCE_TEAMS,instance_teams);
								// processing variables Findallteams in lnk_display

			} catch (ApplicationException e) { 
				// error handling for operation team44FindAll called Findallteams
				errorLogger("An error occured during the execution of the operation : team44FindAll",e); 
		
			}
	}
		
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("UC44_HomePageController [ ");
			strBToS.append(SELECTED_TEAM);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(selectedTeam);
			strBToS.append(END_TO_STRING_DELIMITER);
			strBToS.append(ALL_PLAYERS);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(allPlayers);
			strBToS.append(END_TO_STRING_DELIMITER);
			strBToS.append(INSTANCE_TEAMS);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(instance_teams);
			strBToS.append(END_TO_STRING_DELIMITER);
		// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(uC44_HomePageValidator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsUC44_HomePageController!=null); 
		return strBToS.toString();
	}
}
