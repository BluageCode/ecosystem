/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.advancedfeatures.externaltools.uc209_javamelody ;


import org.apache.log4j.Logger;
import org.springframework.validation.Errors;

import com.netfective.bluage.core.validator.AbstractValidator;
/**
* Class :UC209_HomePageValidator
*/
public class UC209_HomePageValidator extends AbstractValidator{
	
	// LOGGER for the class UC209_HomePageValidator
	private static final Logger LOGGER = Logger.getLogger( UC209_HomePageValidator.class);
	
	
	/**
	* Operation validate for UC209_HomePageForm
	* @param obj : the current form (UC209_HomePageForm)
	* @param errors : The spring errors to return for the form UC209_HomePageForm
	*/
	public void validate(final Object obj,final Errors errors){
		LOGGER.info("Beginning method : validate"); // UC209_HomePageValidator
		UC209_HomePageForm cUC209_HomePageForm = (UC209_HomePageForm)obj; // UC209_HomePageValidator
		LOGGER.info("Ending method : validate the form "+ cUC209_HomePageForm.getClass().getName()); // UC209_HomePageValidator
	}

	/**
	* Method to implements to use spring validators (UC209_HomePageForm)
	* @param aClass : Class for the form UC209_HomePageForm
	* @return boolean
	*/
	public boolean supports(final Class aClass){
		return (new UC209_HomePageForm()).getClass().equals(aClass); // UC209_HomePageValidator
	}
}
