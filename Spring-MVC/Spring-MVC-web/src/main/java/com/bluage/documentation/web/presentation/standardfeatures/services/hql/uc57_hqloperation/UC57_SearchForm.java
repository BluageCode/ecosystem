/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.services.hql.uc57_hqloperation;


import java.io.Serializable;
import java.util.List;

import com.bluage.documentation.business.standardfeatures.services.hql.uc57_hqloperation.bos.Screen57BO;
import com.bluage.documentation.business.standardfeatures.services.hql.uc57_hqloperation.bos.Team57BO;
import com.netfective.bluage.core.web.AbstractForm;
import com.netfective.bluage.core.web.IForm;

/**
* Class : UC57_SearchForm
*/
public class UC57_SearchForm extends AbstractForm implements Serializable, IForm {
	
	/** Constant : serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	//CONSTANT : searchResult
	private static final String SEARCH_RESULT = "searchResult";
	//CONSTANT : hqlCriteria
	private static final String HQL_CRITERIA = "hqlCriteria";
	//CONSTANT : hqlAttribute
	private static final String HQL_ATTRIBUTE = "hqlAttribute";
	//CONSTANT : selectedTeam
	private static final String SELECTED_TEAM = "selectedTeam";
	//CONSTANT : team
	private static final String TEAM = "team";
	/**
	 * 	Property: searchResult 
	 */
	private List<Team57BO> searchResult;
	/**
	 * 	Property: hqlCriteria 
	 */
	private Team57BO hqlCriteria;
	/**
	 * 	Property: hqlAttribute 
	 */
	private Screen57BO hqlAttribute;
	/**
	 * 	Property: selectedTeam 
	 */
	private Team57BO selectedTeam;
	/**
	 * 	Property: team 
	 */
	private Team57BO team;
/**
	 * Default constructor : UC57_SearchForm
	 */
	public UC57_SearchForm() {
		super();
		// Initialize : searchResult
		this.searchResult = new java.util.ArrayList<com.bluage.documentation.business.standardfeatures.services.hql.uc57_hqloperation.bos.Team57BO>();
		// Initialize : hqlCriteria
		this.hqlCriteria = new Team57BO();
		// Initialize : hqlAttribute
		this.hqlAttribute = new Screen57BO();
		// Initialize : selectedTeam
		this.selectedTeam = null;
		// Initialize : team
		this.team = null;
					this.setAlwaysCallPreControllers(true);
	}
	
	/**
	 * 	Getter : searchResult 
	 * 	@return : Return the searchResult instance.
	 */
	public List<Team57BO> getSearchResult(){
		return searchResult; // For UC57_SearchForm
	}
	
	/**
	 * 	Setter : searchResult 
	 *  @param searchResultinstance : The instance to set.
	 */
	public void setSearchResult(final List<Team57BO> searchResultinstance){
		this.searchResult = searchResultinstance;// For UC57_SearchForm
	}
	/**
	 * 	Getter : hqlCriteria 
	 * 	@return : Return the hqlCriteria instance.
	 */
	public Team57BO getHqlCriteria(){
		return hqlCriteria; // For UC57_SearchForm
	}
	
	/**
	 * 	Setter : hqlCriteria 
	 *  @param hqlCriteriainstance : The instance to set.
	 */
	public void setHqlCriteria(final Team57BO hqlCriteriainstance){
		this.hqlCriteria = hqlCriteriainstance;// For UC57_SearchForm
	}
	/**
	 * 	Getter : hqlAttribute 
	 * 	@return : Return the hqlAttribute instance.
	 */
	public Screen57BO getHqlAttribute(){
		return hqlAttribute; // For UC57_SearchForm
	}
	
	/**
	 * 	Setter : hqlAttribute 
	 *  @param hqlAttributeinstance : The instance to set.
	 */
	public void setHqlAttribute(final Screen57BO hqlAttributeinstance){
		this.hqlAttribute = hqlAttributeinstance;// For UC57_SearchForm
	}
	/**
	 * 	Getter : selectedTeam 
	 * 	@return : Return the selectedTeam instance.
	 */
	public Team57BO getSelectedTeam(){
		return selectedTeam; // For UC57_SearchForm
	}
	
	/**
	 * 	Setter : selectedTeam 
	 *  @param selectedTeaminstance : The instance to set.
	 */
	public void setSelectedTeam(final Team57BO selectedTeaminstance){
		this.selectedTeam = selectedTeaminstance;// For UC57_SearchForm
	}
	/**
	 * 	Getter : team 
	 * 	@return : Return the team instance.
	 */
	public Team57BO getTeam(){
		return team; // For UC57_SearchForm
	}
	
	/**
	 * 	Setter : team 
	 *  @param teaminstance : The instance to set.
	 */
	public void setTeam(final Team57BO teaminstance){
		this.team = teaminstance;// For UC57_SearchForm
	}
/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UC57_SearchForm [ "+
SEARCH_RESULT +" = " + searchResult +HQL_CRITERIA +" = " + hqlCriteria +HQL_ATTRIBUTE +" = " + hqlAttribute +SELECTED_TEAM +" = " + selectedTeam +TEAM +" = " + team + "]";
	}
	
	/**
	 * Method setInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public void setInstanceByInstanceName(final String instanceName, final Object instance) {
		// Set the instance SearchResult.
		if(SEARCH_RESULT.equals(instanceName)){// For UC57_SearchForm
			this.setSearchResult((List<Team57BO>)instance); // For UC57_SearchForm
		}
				// Set the instance HqlCriteria.
		if(HQL_CRITERIA.equals(instanceName)){// For UC57_SearchForm
			this.setHqlCriteria((Team57BO)instance); // For UC57_SearchForm
		}
				// Set the instance HqlAttribute.
		if(HQL_ATTRIBUTE.equals(instanceName)){// For UC57_SearchForm
			this.setHqlAttribute((Screen57BO)instance); // For UC57_SearchForm
		}
				// Set the instance SelectedTeam.
		if(SELECTED_TEAM.equals(instanceName)){// For UC57_SearchForm
			this.setSelectedTeam((Team57BO)instance); // For UC57_SearchForm
		}
				// Set the instance Team.
		if(TEAM.equals(instanceName)){// For UC57_SearchForm
			this.setTeam((Team57BO)instance); // For UC57_SearchForm
		}
			}
	
	/**
	 * Method getInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public Object getInstanceByInstanceName(final String instanceName){
		// Temporary object for tmpUC57_SearchForm.
		Object tmpUC57_SearchForm = null;
		
			
		// Get the instance SearchResult for UC57_SearchForm.
		if(SEARCH_RESULT.equals(instanceName)){ // For UC57_SearchForm
			tmpUC57_SearchForm = this.getSearchResult(); // For UC57_SearchForm
		}
			
		// Get the instance HqlCriteria for UC57_SearchForm.
		if(HQL_CRITERIA.equals(instanceName)){ // For UC57_SearchForm
			tmpUC57_SearchForm = this.getHqlCriteria(); // For UC57_SearchForm
		}
			
		// Get the instance HqlAttribute for UC57_SearchForm.
		if(HQL_ATTRIBUTE.equals(instanceName)){ // For UC57_SearchForm
			tmpUC57_SearchForm = this.getHqlAttribute(); // For UC57_SearchForm
		}
			
		// Get the instance SelectedTeam for UC57_SearchForm.
		if(SELECTED_TEAM.equals(instanceName)){ // For UC57_SearchForm
			tmpUC57_SearchForm = this.getSelectedTeam(); // For UC57_SearchForm
		}
			
		// Get the instance Team for UC57_SearchForm.
		if(TEAM.equals(instanceName)){ // For UC57_SearchForm
			tmpUC57_SearchForm = this.getTeam(); // For UC57_SearchForm
		}
		return tmpUC57_SearchForm;// For UC57_SearchForm
	}
	
			}
