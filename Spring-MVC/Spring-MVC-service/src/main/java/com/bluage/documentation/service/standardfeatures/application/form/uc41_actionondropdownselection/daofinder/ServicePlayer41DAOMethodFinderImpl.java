/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.service.standardfeatures.application.form.uc41_actionondropdownselection.daofinder;


import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.Logger;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateTemplate;

import com.netfective.bluage.core.exception.ApplicationException;


/**
 * This class represents query invocation methods implementation
 * for ServicePlayer41 service.
 *
 * @see com.bluage.documentation.service.standardfeatures.application.form.uc41_actionondropdownselection.ServicePlayer41
 */
 
@Named ("com.bluage.documentation.service.standardfeatures.application.form.uc41_actionondropdownselection.daofinder.ServicePlayer41DAOMethodFinderImpl")
public class ServicePlayer41DAOMethodFinderImpl {

	// LOGGER for ServicePlayer41DAOMethodFinderImpl
	private static final Logger LOGGER = Logger.getLogger(ServicePlayer41DAOMethodFinderImpl.class);
	
	/**
	* Hibernate Template (injected)
	*/
	@Inject
	private HibernateTemplate hibernateTemplate;

	
	/**
	 * setter for HibernateTemplate
	 * @param value : {@link HibernateTemplate}
	 */
	public void setHibernateTemplate( HibernateTemplate value )
	{
		hibernateTemplate = value;
	}
	
	
	/**    
	 * HQL operation.
	 * @param code {@link String}
		 * @return {@link List}
	 */
	public List findPlayers41ByPosition41(String code )
 throws ApplicationException {
Session session = hibernateTemplate.getSessionFactory().getCurrentSession();
			return 
				privateFindPlayers41ByPosition41(code, session);
	}

	
	/**    
	 * private delegation for HQL operation.
	 * @param code {@link String}
		 * @return {@link List}
	 */
	private List privateFindPlayers41ByPosition41(String code , Session session)
 throws ApplicationException {
			Query query = session.createQuery(" SELECT p FROM Player41BO p WHERE p.position.code = :code ");
query.setParameter( "code", code, Hibernate.STRING);
														List returnValue = null;
		try {
			returnValue = query.list();
							} catch(HibernateException ex) {
			LOGGER.error("An error occured while excecuting the HQL operation : com.bluage.documentation.service.standardfeatures.application.form.uc41_actionondropdownselection.findPlayers41ByPosition41");
			throw new ApplicationException(ex);
		}
		return returnValue;
	}

	




}
