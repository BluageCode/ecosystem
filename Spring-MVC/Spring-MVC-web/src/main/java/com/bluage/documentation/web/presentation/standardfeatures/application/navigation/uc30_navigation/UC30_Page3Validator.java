/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.application.navigation.uc30_navigation ;


import org.apache.log4j.Logger;
import org.springframework.validation.Errors;

import com.netfective.bluage.core.validator.AbstractValidator;
/**
* Class :UC30_Page3Validator
*/
public class UC30_Page3Validator extends AbstractValidator{
	
	// LOGGER for the class UC30_Page3Validator
	private static final Logger LOGGER = Logger.getLogger( UC30_Page3Validator.class);
	
	
	/**
	* Operation validate for UC30_Page3Form
	* @param obj : the current form (UC30_Page3Form)
	* @param errors : The spring errors to return for the form UC30_Page3Form
	*/
	public void validate(final Object obj,final Errors errors){
		LOGGER.info("Beginning method : validate"); // UC30_Page3Validator
		UC30_Page3Form cUC30_Page3Form = (UC30_Page3Form)obj; // UC30_Page3Validator
		LOGGER.info("Ending method : validate the form "+ cUC30_Page3Form.getClass().getName()); // UC30_Page3Validator
	}

	/**
	* Method to implements to use spring validators (UC30_Page3Form)
	* @param aClass : Class for the form UC30_Page3Form
	* @return boolean
	*/
	public boolean supports(final Class aClass){
		return (new UC30_Page3Form()).getClass().equals(aClass); // UC30_Page3Validator
	}
}
