/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.application.component.uc47_contents;


import java.io.Serializable;
import java.util.List;

import com.bluage.documentation.business.standardfeatures.application.component.uc47_contents.bos.Player47BO;
import com.bluage.documentation.business.standardfeatures.application.component.uc47_contents.bos.Screen47BO;
import com.netfective.bluage.core.web.AbstractForm;
import com.netfective.bluage.core.web.IForm;

/**
* Class : UC47_TableContentsForm
*/
public class UC47_TableContentsForm extends AbstractForm implements Serializable, IForm {
	
	/** Constant : serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	//CONSTANT : allPlayers
	private static final String ALL_PLAYERS = "allPlayers";
	//CONSTANT : displayTable
	private static final String DISPLAY_TABLE = "displayTable";
	//CONSTANT : listTeam
	private static final String LIST_TEAM = "listTeam";
	//CONSTANT : playerToUpdate
	private static final String PLAYER_TO_UPDATE = "playerToUpdate";
	/**
	 * 	Property: allPlayers 
	 */
	private List<Player47BO> allPlayers;
	/**
	 * 	Property: displayTable 
	 */
	private Screen47BO displayTable;
	/**
	 * 	Property: listTeam 
	 */
	private List listTeam;
	/**
	 * 	Property: playerToUpdate 
	 */
	private Player47BO playerToUpdate;
/**
	 * Default constructor : UC47_TableContentsForm
	 */
	public UC47_TableContentsForm() {
		super();
		// Initialize : allPlayers
		this.allPlayers = new java.util.ArrayList<com.bluage.documentation.business.standardfeatures.application.component.uc47_contents.bos.Player47BO>();
		// Initialize : displayTable
		this.displayTable = null;
		// Initialize : listTeam
		this.listTeam = null;
		// Initialize : playerToUpdate
		this.playerToUpdate = null;
					this.setAlwaysCallPreControllers(true);
	}
	
	/**
	 * 	Getter : allPlayers 
	 * 	@return : Return the allPlayers instance.
	 */
	public List<Player47BO> getAllPlayers(){
		return allPlayers; // For UC47_TableContentsForm
	}
	
	/**
	 * 	Setter : allPlayers 
	 *  @param allPlayersinstance : The instance to set.
	 */
	public void setAllPlayers(final List<Player47BO> allPlayersinstance){
		this.allPlayers = allPlayersinstance;// For UC47_TableContentsForm
	}
	/**
	 * 	Getter : displayTable 
	 * 	@return : Return the displayTable instance.
	 */
	public Screen47BO getDisplayTable(){
		return displayTable; // For UC47_TableContentsForm
	}
	
	/**
	 * 	Setter : displayTable 
	 *  @param displayTableinstance : The instance to set.
	 */
	public void setDisplayTable(final Screen47BO displayTableinstance){
		this.displayTable = displayTableinstance;// For UC47_TableContentsForm
	}
	/**
	 * 	Getter : listTeam 
	 * 	@return : Return the listTeam instance.
	 */
	public List getListTeam(){
		return listTeam; // For UC47_TableContentsForm
	}
	
	/**
	 * 	Setter : listTeam 
	 *  @param listTeaminstance : The instance to set.
	 */
	public void setListTeam(final List listTeaminstance){
		this.listTeam = listTeaminstance;// For UC47_TableContentsForm
	}
	/**
	 * 	Getter : playerToUpdate 
	 * 	@return : Return the playerToUpdate instance.
	 */
	public Player47BO getPlayerToUpdate(){
		return playerToUpdate; // For UC47_TableContentsForm
	}
	
	/**
	 * 	Setter : playerToUpdate 
	 *  @param playerToUpdateinstance : The instance to set.
	 */
	public void setPlayerToUpdate(final Player47BO playerToUpdateinstance){
		this.playerToUpdate = playerToUpdateinstance;// For UC47_TableContentsForm
	}
/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UC47_TableContentsForm [ "+
ALL_PLAYERS +" = " + allPlayers +DISPLAY_TABLE +" = " + displayTable +LIST_TEAM +" = " + listTeam +PLAYER_TO_UPDATE +" = " + playerToUpdate + "]";
	}
	
	/**
	 * Method setInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public void setInstanceByInstanceName(final String instanceName, final Object instance) {
		// Set the instance AllPlayers.
		if(ALL_PLAYERS.equals(instanceName)){// For UC47_TableContentsForm
			this.setAllPlayers((List<Player47BO>)instance); // For UC47_TableContentsForm
		}
				// Set the instance DisplayTable.
		if(DISPLAY_TABLE.equals(instanceName)){// For UC47_TableContentsForm
			this.setDisplayTable((Screen47BO)instance); // For UC47_TableContentsForm
		}
				// Set the instance ListTeam.
		if(LIST_TEAM.equals(instanceName)){// For UC47_TableContentsForm
			this.setListTeam((List)instance); // For UC47_TableContentsForm
		}
				// Set the instance PlayerToUpdate.
		if(PLAYER_TO_UPDATE.equals(instanceName)){// For UC47_TableContentsForm
			this.setPlayerToUpdate((Player47BO)instance); // For UC47_TableContentsForm
		}
			}
	
	/**
	 * Method getInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public Object getInstanceByInstanceName(final String instanceName){
		// Temporary object for tmpUC47_TableContentsForm.
		Object tmpUC47_TableContentsForm = null;
		
			
		// Get the instance AllPlayers for UC47_TableContentsForm.
		if(ALL_PLAYERS.equals(instanceName)){ // For UC47_TableContentsForm
			tmpUC47_TableContentsForm = this.getAllPlayers(); // For UC47_TableContentsForm
		}
			
		// Get the instance DisplayTable for UC47_TableContentsForm.
		if(DISPLAY_TABLE.equals(instanceName)){ // For UC47_TableContentsForm
			tmpUC47_TableContentsForm = this.getDisplayTable(); // For UC47_TableContentsForm
		}
			
		// Get the instance ListTeam for UC47_TableContentsForm.
		if(LIST_TEAM.equals(instanceName)){ // For UC47_TableContentsForm
			tmpUC47_TableContentsForm = this.getListTeam(); // For UC47_TableContentsForm
		}
			
		// Get the instance PlayerToUpdate for UC47_TableContentsForm.
		if(PLAYER_TO_UPDATE.equals(instanceName)){ // For UC47_TableContentsForm
			tmpUC47_TableContentsForm = this.getPlayerToUpdate(); // For UC47_TableContentsForm
		}
		return tmpUC47_TableContentsForm;// For UC47_TableContentsForm
	}
	
			}
