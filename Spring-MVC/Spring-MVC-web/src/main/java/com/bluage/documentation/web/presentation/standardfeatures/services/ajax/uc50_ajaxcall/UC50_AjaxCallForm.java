/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.services.ajax.uc50_ajaxcall;


import java.io.Serializable;
import java.util.List;

import com.bluage.documentation.business.standardfeatures.services.ajax.uc50_ajaxcall.bos.Player50BO;
import com.bluage.documentation.business.standardfeatures.services.ajax.uc50_ajaxcall.bos.Team50BO;
import com.netfective.bluage.core.web.AbstractForm;
import com.netfective.bluage.core.web.IForm;

/**
* Class : UC50_AjaxCallForm
*/
public class UC50_AjaxCallForm extends AbstractForm implements Serializable, IForm {
	
	/** Constant : serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	//CONSTANT : isRookie
	private static final String IS_ROOKIE = "isRookie";
	//CONSTANT : minimumValue
	private static final String MINIMUM_VALUE = "minimumValue";
	//CONSTANT : players
	private static final String PLAYERS = "players";
	//CONSTANT : selectedTeam
	private static final String SELECTED_TEAM = "selectedTeam";
	//CONSTANT : teamslist
	private static final String TEAMSLIST = "teamslist";
	/**
	 * 	Property: isRookie 
	 */
	private Player50BO isRookie;
	/**
	 * 	Property: minimumValue 
	 */
	private Player50BO minimumValue;
	/**
	 * 	Property: players 
	 */
	private List<Player50BO> players;
	/**
	 * 	Property: selectedTeam 
	 */
	private Team50BO selectedTeam;
	/**
	 * 	Property: teamslist 
	 */
	private List teamslist;
/**
	 * Default constructor : UC50_AjaxCallForm
	 */
	public UC50_AjaxCallForm() {
		super();
		// Initialize : isRookie
		this.isRookie = new Player50BO();
		// Initialize : minimumValue
		this.minimumValue = new Player50BO();
		// Initialize : players
		this.players = new java.util.ArrayList<com.bluage.documentation.business.standardfeatures.services.ajax.uc50_ajaxcall.bos.Player50BO>();
		// Initialize : selectedTeam
		this.selectedTeam = null;
		// Initialize : teamslist
		this.teamslist = new java.util.ArrayList();
					this.setAlwaysCallPreControllers(true);
	}
	
	/**
	 * 	Getter : isRookie 
	 * 	@return : Return the isRookie instance.
	 */
	public Player50BO getIsRookie(){
		return isRookie; // For UC50_AjaxCallForm
	}
	
	/**
	 * 	Setter : isRookie 
	 *  @param isRookieinstance : The instance to set.
	 */
	public void setIsRookie(final Player50BO isRookieinstance){
		this.isRookie = isRookieinstance;// For UC50_AjaxCallForm
	}
	/**
	 * 	Getter : minimumValue 
	 * 	@return : Return the minimumValue instance.
	 */
	public Player50BO getMinimumValue(){
		return minimumValue; // For UC50_AjaxCallForm
	}
	
	/**
	 * 	Setter : minimumValue 
	 *  @param minimumValueinstance : The instance to set.
	 */
	public void setMinimumValue(final Player50BO minimumValueinstance){
		this.minimumValue = minimumValueinstance;// For UC50_AjaxCallForm
	}
	/**
	 * 	Getter : players 
	 * 	@return : Return the players instance.
	 */
	public List<Player50BO> getPlayers(){
		return players; // For UC50_AjaxCallForm
	}
	
	/**
	 * 	Setter : players 
	 *  @param playersinstance : The instance to set.
	 */
	public void setPlayers(final List<Player50BO> playersinstance){
		this.players = playersinstance;// For UC50_AjaxCallForm
	}
	/**
	 * 	Getter : selectedTeam 
	 * 	@return : Return the selectedTeam instance.
	 */
	public Team50BO getSelectedTeam(){
		return selectedTeam; // For UC50_AjaxCallForm
	}
	
	/**
	 * 	Setter : selectedTeam 
	 *  @param selectedTeaminstance : The instance to set.
	 */
	public void setSelectedTeam(final Team50BO selectedTeaminstance){
		this.selectedTeam = selectedTeaminstance;// For UC50_AjaxCallForm
	}
	/**
	 * 	Getter : teamslist 
	 * 	@return : Return the teamslist instance.
	 */
	public List getTeamslist(){
		return teamslist; // For UC50_AjaxCallForm
	}
	
	/**
	 * 	Setter : teamslist 
	 *  @param teamslistinstance : The instance to set.
	 */
	public void setTeamslist(final List teamslistinstance){
		this.teamslist = teamslistinstance;// For UC50_AjaxCallForm
	}
/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UC50_AjaxCallForm [ "+
IS_ROOKIE +" = " + isRookie +MINIMUM_VALUE +" = " + minimumValue +PLAYERS +" = " + players +SELECTED_TEAM +" = " + selectedTeam +TEAMSLIST +" = " + teamslist + "]";
	}
	
	/**
	 * Method setInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public void setInstanceByInstanceName(final String instanceName, final Object instance) {
		// Set the instance IsRookie.
		if(IS_ROOKIE.equals(instanceName)){// For UC50_AjaxCallForm
			this.setIsRookie((Player50BO)instance); // For UC50_AjaxCallForm
		}
				// Set the instance MinimumValue.
		if(MINIMUM_VALUE.equals(instanceName)){// For UC50_AjaxCallForm
			this.setMinimumValue((Player50BO)instance); // For UC50_AjaxCallForm
		}
				// Set the instance Players.
		if(PLAYERS.equals(instanceName)){// For UC50_AjaxCallForm
			this.setPlayers((List<Player50BO>)instance); // For UC50_AjaxCallForm
		}
				// Set the instance SelectedTeam.
		if(SELECTED_TEAM.equals(instanceName)){// For UC50_AjaxCallForm
			this.setSelectedTeam((Team50BO)instance); // For UC50_AjaxCallForm
		}
				// Set the instance Teamslist.
		if(TEAMSLIST.equals(instanceName)){// For UC50_AjaxCallForm
			this.setTeamslist((List)instance); // For UC50_AjaxCallForm
		}
			}
	
	/**
	 * Method getInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public Object getInstanceByInstanceName(final String instanceName){
		// Temporary object for tmpUC50_AjaxCallForm.
		Object tmpUC50_AjaxCallForm = null;
		
			
		// Get the instance IsRookie for UC50_AjaxCallForm.
		if(IS_ROOKIE.equals(instanceName)){ // For UC50_AjaxCallForm
			tmpUC50_AjaxCallForm = this.getIsRookie(); // For UC50_AjaxCallForm
		}
			
		// Get the instance MinimumValue for UC50_AjaxCallForm.
		if(MINIMUM_VALUE.equals(instanceName)){ // For UC50_AjaxCallForm
			tmpUC50_AjaxCallForm = this.getMinimumValue(); // For UC50_AjaxCallForm
		}
			
		// Get the instance Players for UC50_AjaxCallForm.
		if(PLAYERS.equals(instanceName)){ // For UC50_AjaxCallForm
			tmpUC50_AjaxCallForm = this.getPlayers(); // For UC50_AjaxCallForm
		}
			
		// Get the instance SelectedTeam for UC50_AjaxCallForm.
		if(SELECTED_TEAM.equals(instanceName)){ // For UC50_AjaxCallForm
			tmpUC50_AjaxCallForm = this.getSelectedTeam(); // For UC50_AjaxCallForm
		}
			
		// Get the instance Teamslist for UC50_AjaxCallForm.
		if(TEAMSLIST.equals(instanceName)){ // For UC50_AjaxCallForm
			tmpUC50_AjaxCallForm = this.getTeamslist(); // For UC50_AjaxCallForm
		}
		return tmpUC50_AjaxCallForm;// For UC50_AjaxCallForm
	}
	
			}
