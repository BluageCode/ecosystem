/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.gettingstarted.uc05_search ;


import org.apache.log4j.Logger;
import org.springframework.validation.Errors;

import com.netfective.bluage.core.validator.AbstractValidator;
/**
* Class :UC05_SearchValidator
*/
public class UC05_SearchValidator extends AbstractValidator{
	
	// LOGGER for the class UC05_SearchValidator
	private static final Logger LOGGER = Logger.getLogger( UC05_SearchValidator.class);
	
	
	/**
	* Operation validate for UC05_SearchForm
	* @param obj : the current form (UC05_SearchForm)
	* @param errors : The spring errors to return for the form UC05_SearchForm
	*/
	public void validate(final Object obj,final Errors errors){
		LOGGER.info("Beginning method : validate"); // UC05_SearchValidator
		UC05_SearchForm cUC05_SearchForm = (UC05_SearchForm)obj; // UC05_SearchValidator
		LOGGER.info("Ending method : validate the form "+ cUC05_SearchForm.getClass().getName()); // UC05_SearchValidator
	}

	/**
	* Method to implements to use spring validators (UC05_SearchForm)
	* @param aClass : Class for the form UC05_SearchForm
	* @return boolean
	*/
	public boolean supports(final Class aClass){
		return (new UC05_SearchForm()).getClass().equals(aClass); // UC05_SearchValidator
	}
}
