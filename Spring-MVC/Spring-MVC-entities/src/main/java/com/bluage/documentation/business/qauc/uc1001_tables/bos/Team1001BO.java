/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.business.qauc.uc1001_tables.bos;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.inject.Inject;
import javax.inject.Named;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.LazyInitializationException;

import com.bluage.documentation.business.common.bos.TeamBO;
import com.bluage.documentation.business.qauc.uc1001_tables.entities.daofinder.Team1001DAOFinderImpl;
import com.netfective.bluage.core.exception.ApplicationException;

/**
 * Team1001BO
 *
* @see com.netfective.bluage.core.business.IBusinessObject
* * @see TeamBO
 */
@XmlRootElement

public class Team1001BO
 extends TeamBO {
	
	/**
	 * Default SUID for Team1001BO.
	 */
	private static final long serialVersionUID = 1L;
	

	@Inject
	@Named ("com.bluage.documentation.business.qauc.uc1001_tables.entities.daofinder.Team1001Finder") 
	private Team1001DAOFinderImpl finder;

																			/**
	 * Entity property: players
	 *
	 */
private Set<Player1001BO> players = new HashSet<Player1001BO>();



   /**
	* public default constructor for class Team1001BO
	*/
	public Team1001BO(){
		//NOP Team1001BO
	}


																			/**
	 * Getter for players of class Team1001BO.
	 *
	 * @return {@link java.util.Set<com.bluage.documentation.business.qauc.uc1001_tables.bos.Player1001BO>}
	 * @generated
	 */ 
public  Set<Player1001BO> getPlayers() {
	return this.players;
			}


	/**
	 * Setter for players of class Team1001BO.
	 * @param value {@link java.util.Set<com.bluage.documentation.business.qauc.uc1001_tables.bos.Player1001BO>}
	 * @generated
	 */

public  void setPlayers(final Set<Player1001BO> players) {
	this.players = players;
}

	/**
	 * Remove an entity from the com.bluage.documentation.business.qauc.uc1001_tables.bos.Player1001BO collection.
	 *
	 * @param entity
	 *		: {@link Set<Player1001BO>} the entity
	 * @throws ApplicationException
     */
public void removeFromPlayers( Player1001BO entity ) throws ApplicationException {
	try {
		if( getPlayers() != null && getPlayers().contains(entity) ) {
			getPlayers().remove( entity );
		entity.setTeam( null );
		}
		} catch ( LazyInitializationException e ) {
				Team1001BO bo = ( Team1001BO ) finder.findByPrimaryKey( getId() );
			finder.getHibernateTemplate().initialize( bo.getPlayers() );
			setPlayers( bo.getPlayers() );
			// evict object from session to avoid NonUniqueObjectException
			finder.getHibernateTemplate().evict( bo );
		
			if( getPlayers() != null && getPlayers().contains( entity ) ) {
				getPlayers().remove( entity );
				entity.setTeam( null );
			}
				}
	}
	
	/**
	 * Add entity to the com.bluage.documentation.business.qauc.uc1001_tables.bos.Player1001BO collection.
	 *
	 * @param entity
	 *		{@link Player1001BO} the entity
	 * @throws ApplicationException
     */
public void addToPlayers( Player1001BO entity ) throws ApplicationException
	{
		try {
			if( getPlayers() != null && !getPlayers().contains( entity ) ) {
				getPlayers().add( entity );
				entity.setTeam( this );
			}
			} catch ( LazyInitializationException e ) {
			Team1001BO bo = ( Team1001BO ) finder.findByPrimaryKey( getId() );
		finder.getHibernateTemplate().initialize( bo.getPlayers() );
		setPlayers( bo.getPlayers() );
		// evict object from session to avoid NonUniqueObjectException
		finder.getHibernateTemplate().evict( bo );
		if( getPlayers() != null && !getPlayers().contains( entity ) ) {
			getPlayers().add( entity );
			entity.setTeam( this );
		}
			}
	}
	
	/**
	 * Add entitys to the com.bluage.documentation.business.qauc.uc1001_tables.bos.Player1001BO collection
	 *
	 * @param entities			
	 *		: {@link java.util.List} A list of entities
	 * @throws ApplicationException 
     */	
public void addAllToPlayers( Collection<Player1001BO> entities ) throws ApplicationException {
	for( Player1001BO entity : entities) {
		if( getPlayers()!=null ) {
			addToPlayers( entity );
		}
	}
}
	
	/**
	 * Remove entities form the com.bluage.documentation.business.qauc.uc1001_tables.bos.Player1001BO collection
	 *
	 * @param entities				
	 *		: {@link java.util.List} a list of entities
	 * @throws ApplicationException
     */	
public void removeAllFromPlayers( Collection<Player1001BO> entities ) throws ApplicationException {
	for( Player1001BO entity : entities) {
		if( getPlayers()!=null && getPlayers().contains( entity ) )	{		
			removeFromPlayers( entity );
		}
	}
}
	
	/**
	 * Remove all items form the collection com.bluage.documentation.business.qauc.uc1001_tables.bos.Player1001BO
	 *
	 * @throws ApplicationException
     */	
public void removeAllFromPlayers() throws ApplicationException {
	Set<Player1001BO> entities = this.getPlayers();
	if ( entities == null ) {
		return;
	}
	Iterator<Player1001BO> it = entities.iterator();
	while ( it.hasNext() ) {
    	Player1001BO entity = it.next(); 
		entity.setTeam( null ); 
	}
	entities.clear();
}

/**
	 * Returns <code>true</code> if the argument is an Team1001 instance and all identifiers for this entity
     * equal the identifiers of the argument entity. The <code>equals</code> method of the parent entity
     * will also need to return <code>true</code>. Returns <code>false</code> otherwise.
     *
     * @see TeamBO#hashCode()
	 * @param object  object to compare with current instance      
     * @return boolean true if equals false if not
     */
    public boolean equals(final Object object )
    {
		if ( this == object )
        {
            return true;
        }
        if ( !( object instanceof Team1001BO ) )
        {
            return false;
        }
        final Team1001BO that = (Team1001BO)object;
    
    	            if (        				        			this.getId() == null
        	        ) {
        	return false;
        }
		return new EqualsBuilder()
			.append(this.getId(), that.getId())
			 	.isEquals();
	}
	/**

	 * Returns a hash code based on this entity's identifiers and the hash code of the parent entity.
     *
     * @see TeamBO#hashCode()
     * @return int
     */    
    public int hashCode() {
        return new HashCodeBuilder()
             .append(getId())
				 	.appendSuper(super.hashCode())
            .toHashCode();
    }
}

