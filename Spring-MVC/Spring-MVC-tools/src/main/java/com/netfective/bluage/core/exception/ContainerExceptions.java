/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.netfective.bluage.core.exception;

import java.util.ArrayList;
import java.util.List;

/**
 * La classe represente un container des 2 types d exceptions : system & application
 * 
 * 
 * <DL>
 * <DT><B>Nom du Projet: </B><DD>Bluage</DD>
 * <DT><B>Service: </B>
 * 
 */

public class ContainerExceptions implements IContainerExceptions {
	private List applicationExceptions;
	private boolean emptyApplicationExceptions;
	private boolean existException;
	/**
	 * default constructor for ContainerExceptions
	 */
	public ContainerExceptions() {
		applicationExceptions = new ArrayList();
		emptyApplicationExceptions = true;
		existException = false;
	}

	/**
	 * Adds an errorMessage to applicationExceptions
	 * @param em
	 */
	public void addApplicationException(final ErrorMessage em) {
		applicationExceptions.add(em);
	}
	
	/**
	 * clears the errorMessages in applicationExceptions
	 */
	public void clearExceptionContainers() {
		applicationExceptions = new ArrayList();
	}
	/**
	 * clears the errorMessages in applicationExceptions and sets the emptyApplicationExceptions flag to true
	 */
	public void clearApplicationExceptions() {
		applicationExceptions = new ArrayList();
		emptyApplicationExceptions = true;
	}
	
	/**
	 * getter for applicationExceptions
	 * @return applicationExceptions List
	 */
	public List getApplicationExceptions() {
		return applicationExceptions;
	}
	
	/**
	 * getter for emptyApplicationExceptions
	 * @return emptyApplicationExceptions boolean
	 */
	public boolean isEmptyApplicationExceptions() {
		return applicationExceptions.isEmpty();
	}
	
	/**
	 * setter for applicationExceptions
	 * @param list List
	 */
	public void setApplicationExceptions(final List list) {
		applicationExceptions = list;
	}
	
	/**
	 * setter for applicationExceptions
	 * @param list List
	 */
	public void setEmptyApplicationExceptions(final boolean b) {
		emptyApplicationExceptions = b;
	}
	
	/**
	 * getter for existException
	 * @return existException boolean
	 */
	public boolean isExistException() {
		if (!applicationExceptions.isEmpty()) {
			existException = true;
		}
		return existException;
	}
	
	/**
	 * setter for existException
	 * @param b boolean
	 */
	public void setExistException(final boolean b) {
		existException = b;
	}
}
