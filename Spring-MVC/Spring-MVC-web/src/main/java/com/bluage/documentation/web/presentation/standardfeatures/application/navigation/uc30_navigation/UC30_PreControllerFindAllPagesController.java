/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.web.presentation.standardfeatures.application.navigation.uc30_navigation;

// Import declaration.
// Java imports.
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import com.bluage.documentation.service.standardfeatures.application.navigation.uc30_navigation.ServiceNavigation;
import com.netfective.bluage.core.exception.ApplicationException;
import com.netfective.bluage.core.web.AbstractController;
import com.netfective.bluage.core.web.IForm;

/**
 * Class : UC30_PreControllerFindAllPagesController
 */
 @Service("com.bluage.documentation.web.presentation.standardfeatures.application.navigation.uc30_navigation.UC30_PreControllerFindAllPagesController")
public class UC30_PreControllerFindAllPagesController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC30_PreControllerFindAllPagesController.class);
	
	//CONSTANT PINS: pages
	private static final String PAGES = "pages";
	
	// Declaring all the instances.
		List pages;
	/**
	 * Service declaration : serviceNavigation.
	 */
	@Autowired
	private ServiceNavigation serviceNavigation;
	
	/**
	 * Operation : UC30_PreControllerFindAllPagesControllerInit
	 * @param request : The current HttpRequest
 	 * @param model :  The current Model
 	 * @param currentIForm : The current IForm
 	 * @return
	 */
	public IForm uC30_PreControllerFindAllPagesControllerInit(final HttpServletRequest request,final  Model model,final  IForm  currentIForm){

		LOGGER.info("Begin the precontroller method : findPages");
		LOGGER.info("Form diagnostic : " + currentIForm);
		

	callFindAllPages(request,currentIForm);
		
			LOGGER.info("Populate the destination screen.");
			// Populate the destination form with all the instances returned from an executed service.
			return populateDestinationForm(request.getSession(), currentIForm); // Populate the destination screen

	}
	/**
	 * method callFindAllPages
	 * 
	 * @param request HttpServletRequest
	 * @param currentIForm IForm
	 */
	private void callFindAllPages(HttpServletRequest request, IForm currentIForm) {
		// Initialize all the instances.
		pages = (List)get(request.getSession(),currentIForm,PAGES);
							try {

				pages = 	serviceNavigation.loadPages(
	);  

				put(request.getSession(), PAGES,pages);
							
			} catch (ApplicationException e) {
				LOGGER.error("An error occured during the execution of the operation : loadPages",e);
			}
	}
}
