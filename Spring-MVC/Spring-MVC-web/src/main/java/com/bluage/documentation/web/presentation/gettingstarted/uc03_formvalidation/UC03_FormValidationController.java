/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.gettingstarted.uc03_formvalidation ;

// Import declaration.
// Java imports.
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.bluage.documentation.business.gettingstarted.uc03_formvalidation.bos.Player03BO;
import com.bluage.documentation.business.gettingstarted.uc03_formvalidation.entities.daofinder.Position03DAOFinderImpl;
import com.bluage.documentation.service.gettingstarted.uc03_formvalidation.ServicePlayer03;
import com.netfective.bluage.core.exception.ApplicationException;
import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;
import com.netfective.bluage.core.web.IForm;

/**
 * Class : UC03_FormValidationController
 */
@Controller
@Scope("prototype")
@SessionAttributes("uC03_FormValidationForm")
@RequestMapping(value= "/presentation/gettingstarted/uc03_formvalidation")
public class UC03_FormValidationController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC03_FormValidationController.class);
	
	//Current form name
	private static final String U_C03__FORM_VALIDATION_FORM = "uC03_FormValidationForm";

		//CONSTANT: allPositions
	private static final String ALL_POSITIONS = "allPositions";
				//CONSTANT: playerToCreate
	private static final String PLAYER_TO_CREATE = "playerToCreate";
	
	/**
	 * Property:customDateEditorsUC03_FormValidationController
	 */
	final private Map<String,CustomDateEditor> customDateEditorsUC03_FormValidationController = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:uC03_FormValidationValidator
	 */
	final private UC03_FormValidationValidator uC03_FormValidationValidator = new UC03_FormValidationValidator();
	
	/**
	 * Service declaration : servicePlayer03.
	 */
	@Autowired
	private ServicePlayer03 servicePlayer03;
	
	/**
	 * Generic Finder : position03DAOFinderImpl.
	 */
	@Autowired
	private Position03DAOFinderImpl position03DAOFinderImpl;
	
	
	// Initialise all the instances for UC03_FormValidationController
			// Initialize the instance allPositions for the state lnk_back
		private List allPositions; // Initialize the instance allPositions for UC03_FormValidationController
						// Declare the instance playerToCreate
		private Player03BO playerToCreate;
			/**
	 * Operation : lnk_create
 	 * @param model : 
 	 * @param uC03_FormValidation : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC03_FormValidation/lnk_create.html",method = RequestMethod.POST)
	public String lnk_create(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC03_FormValidationForm") UC03_FormValidationForm  uC03_FormValidationForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : lnk_create"); 
		infoLogger(uC03_FormValidationForm); 
		uC03_FormValidationForm.setAlwaysCallPreControllers(false); 
		// initialization for lnk_create
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state lnk_create if the validation fail, you will be forward to the screen : "/presentation/gettingstarted/uc03_formvalidation/UC03_FormValidation"
		if(errorsMessages(request,response,uC03_FormValidationForm,bindingResult,"", uC03_FormValidationValidator, customDateEditorsUC03_FormValidationController)){ 
			return "/presentation/gettingstarted/uc03_formvalidation/UC03_FormValidation"; 
		}

					 callCreatePlayer(request,uC03_FormValidationForm );
											// Init the destination form com.bluage.documentation.web.presentation.gettingstarted.uc03_formvalidation.UC03_FormValidationMessageForm or not from lnk_create
			final UC03_FormValidationMessageForm uC03_FormValidationMessageForm =  new UC03_FormValidationMessageForm();
			// Populate the destination form with all the instances returned from lnk_create.
			final IForm uC03_FormValidationMessageForm2 = populateDestinationForm(request.getSession(), uC03_FormValidationMessageForm); 
					
			// Add the form to the model.
			model.addAttribute("uC03_FormValidationMessageForm", uC03_FormValidationMessageForm2); 
			
			request.getSession().setAttribute("uC03_FormValidationMessageForm", uC03_FormValidationMessageForm2);
			
			// "OK" CASE => destination screen path from lnk_create
			LOGGER.info("Go to the screen 'UC03_FormValidationMessage'.");
			// Redirect (PRG) from lnk_create
			destinationPath =  "redirect:/presentation/gettingstarted/uc03_formvalidation/UC03_FormValidationMessage.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_create 
	}
	
	
	/**
	* This method initialise the form : UC03_FormValidationForm 
	* @return UC03_FormValidationForm
	*/
	@ModelAttribute("UC03_FormValidationFormInit")
	public void initUC03_FormValidationForm(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, U_C03__FORM_VALIDATION_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method UC03_FormValidationForm."); //for lnk_create 
		}
		UC03_FormValidationForm uC03_FormValidationForm;
	
		if(request.getSession().getAttribute(U_C03__FORM_VALIDATION_FORM) != null){
			uC03_FormValidationForm = (UC03_FormValidationForm)request.getSession().getAttribute(U_C03__FORM_VALIDATION_FORM);
		} else {
			uC03_FormValidationForm = new UC03_FormValidationForm();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form UC03_FormValidationForm.");
		}
		uC03_FormValidationForm = (UC03_FormValidationForm)populateDestinationForm(request.getSession(), uC03_FormValidationForm);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  UC03_FormValidationForm.");
		}
		model.addAttribute(U_C03__FORM_VALIDATION_FORM, uC03_FormValidationForm);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param UC03_FormValidationForm : The sceen form.
	 */
	@RequestMapping(value = "/UC03_FormValidation.html" ,method = RequestMethod.GET)
	public void prepareUC03_FormValidation(final HttpServletRequest request,final  Model model,final  @ModelAttribute("UC03_FormValidationFormInit") UC03_FormValidationForm uC03_FormValidationForm){
		
		UC03_FormValidationForm currentUC03_FormValidationForm = uC03_FormValidationForm;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(U_C03__FORM_VALIDATION_FORM) == null){
			if(currentUC03_FormValidationForm!=null){
				request.getSession().setAttribute(U_C03__FORM_VALIDATION_FORM, currentUC03_FormValidationForm);
			}else {
				currentUC03_FormValidationForm = new UC03_FormValidationForm();
				request.getSession().setAttribute(U_C03__FORM_VALIDATION_FORM, currentUC03_FormValidationForm);	
			}
		} else {
			currentUC03_FormValidationForm = (UC03_FormValidationForm) request.getSession().getAttribute(U_C03__FORM_VALIDATION_FORM);
		}

		try {
			List allPositions = position03DAOFinderImpl.findAll();
			put(request.getSession(), ALL_POSITIONS, allPositions);
		} catch (ApplicationException e) {
			errorLogger("An error while calling the generic finder for the instance : allPositions.",e);
		}
		currentUC03_FormValidationForm = (UC03_FormValidationForm)populateDestinationForm(request.getSession(), currentUC03_FormValidationForm);
		// Call all the Precontroller.
	currentUC03_FormValidationForm.setAlwaysCallPreControllers(true);
		model.addAttribute(U_C03__FORM_VALIDATION_FORM, currentUC03_FormValidationForm);
		request.getSession().setAttribute(U_C03__FORM_VALIDATION_FORM, currentUC03_FormValidationForm);
		
	}
	
				/**
	 * method callCreatePlayer
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callCreatePlayer(HttpServletRequest request,IForm form) {
					playerToCreate = (Player03BO)get(request.getSession(),form, "playerToCreate");  
										 
					try {
				// executing CreatePlayer in lnk_create
	servicePlayer03.createPlayer03(
			playerToCreate
			);  

								// processing variables CreatePlayer in lnk_create
				put(request.getSession(), PLAYER_TO_CREATE,playerToCreate); 
			
			} catch (ApplicationException e) { 
				// error handling for operation createPlayer03 called CreatePlayer
				errorLogger("An error occured during the execution of the operation : createPlayer03",e); 
		
			}
	}
		/**
	* This method Binder.
	* @param binder : 
	*		 WebDataBinder
	*/
	@InitBinder
	protected void initBinder(final WebDataBinder binder){
			customDateEditorsUC03_FormValidationController.put("playerToCreate.dateOfBirth", new CustomDateEditor(new SimpleDateFormat("MM/dd/yyyy"), true, "Invalid format."));
			binder.registerCustomEditor(Date.class, "playerToCreate.dateOfBirth", customDateEditorsUC03_FormValidationController.get("playerToCreate.dateOfBirth"));
	}
	
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("UC03_FormValidationController [ ");
				strBToS.append(ALL_POSITIONS);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(allPositions);
			strBToS.append(END_TO_STRING_DELIMITER);
						strBToS.append(PLAYER_TO_CREATE);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(playerToCreate);
			strBToS.append(END_TO_STRING_DELIMITER);
		// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(uC03_FormValidationValidator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsUC03_FormValidationController!=null); 
		return strBToS.toString();
	}
}
