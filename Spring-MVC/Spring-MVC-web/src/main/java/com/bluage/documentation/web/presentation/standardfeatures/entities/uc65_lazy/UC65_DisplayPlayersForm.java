/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.entities.uc65_lazy;


import java.io.Serializable;
import java.util.List;

import com.bluage.documentation.business.standardfeatures.entities.uc65_lazy.bos.Professional65BO;
import com.bluage.documentation.business.standardfeatures.entities.uc65_lazy.bos.Team65BO;
import com.netfective.bluage.core.web.AbstractForm;
import com.netfective.bluage.core.web.IForm;

/**
* Class : UC65_DisplayPlayersForm
*/
public class UC65_DisplayPlayersForm extends AbstractForm implements Serializable, IForm {
	
	/** Constant : serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	//CONSTANT : teamLazyTrue
	private static final String TEAM_LAZY_TRUE = "teamLazyTrue";
	//CONSTANT : players
	private static final String PLAYERS = "players";
	/**
	 * 	Property: teamLazyTrue 
	 */
	private Team65BO teamLazyTrue;
	/**
	 * 	Property: players 
	 */
	private List<Professional65BO> players;
/**
	 * Default constructor : UC65_DisplayPlayersForm
	 */
	public UC65_DisplayPlayersForm() {
		super();
		// Initialize : teamLazyTrue
		this.teamLazyTrue = new Team65BO();
		// Initialize : players
		this.players = new java.util.ArrayList<com.bluage.documentation.business.standardfeatures.entities.uc65_lazy.bos.Professional65BO>();
					this.setAlwaysCallPreControllers(true);
	}
	
	/**
	 * 	Getter : teamLazyTrue 
	 * 	@return : Return the teamLazyTrue instance.
	 */
	public Team65BO getTeamLazyTrue(){
		return teamLazyTrue; // For UC65_DisplayPlayersForm
	}
	
	/**
	 * 	Setter : teamLazyTrue 
	 *  @param teamLazyTrueinstance : The instance to set.
	 */
	public void setTeamLazyTrue(final Team65BO teamLazyTrueinstance){
		this.teamLazyTrue = teamLazyTrueinstance;// For UC65_DisplayPlayersForm
	}
	/**
	 * 	Getter : players 
	 * 	@return : Return the players instance.
	 */
	public List<Professional65BO> getPlayers(){
		return players; // For UC65_DisplayPlayersForm
	}
	
	/**
	 * 	Setter : players 
	 *  @param playersinstance : The instance to set.
	 */
	public void setPlayers(final List<Professional65BO> playersinstance){
		this.players = playersinstance;// For UC65_DisplayPlayersForm
	}
/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UC65_DisplayPlayersForm [ "+
TEAM_LAZY_TRUE +" = " + teamLazyTrue +PLAYERS +" = " + players + "]";
	}
	
	/**
	 * Method setInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public void setInstanceByInstanceName(final String instanceName, final Object instance) {
		// Set the instance TeamLazyTrue.
		if(TEAM_LAZY_TRUE.equals(instanceName)){// For UC65_DisplayPlayersForm
			this.setTeamLazyTrue((Team65BO)instance); // For UC65_DisplayPlayersForm
		}
				// Set the instance Players.
		if(PLAYERS.equals(instanceName)){// For UC65_DisplayPlayersForm
			this.setPlayers((List<Professional65BO>)instance); // For UC65_DisplayPlayersForm
		}
				// Parent instance name for UC65_DisplayPlayersForm
		if(TEAM_LAZY_TRUE.equals(instanceName) && teamLazyTrue != null){ // For UC65_DisplayPlayersForm
			players.clear(); // For UC65_DisplayPlayersForm
			players.addAll(teamLazyTrue.getProfessionals());// For UC65_DisplayPlayersForm
		}
		
	}
	
	/**
	 * Method getInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public Object getInstanceByInstanceName(final String instanceName){
		// Temporary object for tmpUC65_DisplayPlayersForm.
		Object tmpUC65_DisplayPlayersForm = null;
		
			
		// Get the instance TeamLazyTrue for UC65_DisplayPlayersForm.
		if(TEAM_LAZY_TRUE.equals(instanceName)){ // For UC65_DisplayPlayersForm
			tmpUC65_DisplayPlayersForm = this.getTeamLazyTrue(); // For UC65_DisplayPlayersForm
		}
			
		// Get the instance Players for UC65_DisplayPlayersForm.
		if(PLAYERS.equals(instanceName)){ // For UC65_DisplayPlayersForm
			tmpUC65_DisplayPlayersForm = this.getPlayers(); // For UC65_DisplayPlayersForm
		}
		return tmpUC65_DisplayPlayersForm;// For UC65_DisplayPlayersForm
	}
	
			}
