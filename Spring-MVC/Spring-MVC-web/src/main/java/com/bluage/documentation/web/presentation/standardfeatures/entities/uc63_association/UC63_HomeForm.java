/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.entities.uc63_association;


import java.io.Serializable;
import java.util.List;

import com.bluage.documentation.business.standardfeatures.entities.uc63_association.bos.Coach63BO;
import com.bluage.documentation.business.standardfeatures.entities.uc63_association.bos.Stadium63BO;
import com.bluage.documentation.business.standardfeatures.entities.uc63_association.bos.State63BO;
import com.bluage.documentation.business.standardfeatures.entities.uc63_association.bos.Team63BO;
import com.netfective.bluage.core.web.AbstractForm;
import com.netfective.bluage.core.web.IForm;

/**
* Class : UC63_HomeForm
*/
public class UC63_HomeForm extends AbstractForm implements Serializable, IForm {
	
	/** Constant : serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	//CONSTANT : allStates
	private static final String ALL_STATES = "allStates";
	//CONSTANT : allStadiums
	private static final String ALL_STADIUMS = "allStadiums";
	//CONSTANT : allTeams
	private static final String ALL_TEAMS = "allTeams";
	//CONSTANT : allCoachs
	private static final String ALL_COACHS = "allCoachs";
	/**
	 * 	Property: allStates 
	 */
	private List<State63BO> allStates;
	/**
	 * 	Property: allStadiums 
	 */
	private List<Stadium63BO> allStadiums;
	/**
	 * 	Property: allTeams 
	 */
	private List<Team63BO> allTeams;
	/**
	 * 	Property: allCoachs 
	 */
	private List<Coach63BO> allCoachs;
/**
	 * Default constructor : UC63_HomeForm
	 */
	public UC63_HomeForm() {
		super();
		// Initialize : allStates
		this.allStates = new java.util.ArrayList<com.bluage.documentation.business.standardfeatures.entities.uc63_association.bos.State63BO>();
		// Initialize : allStadiums
		this.allStadiums = new java.util.ArrayList<com.bluage.documentation.business.standardfeatures.entities.uc63_association.bos.Stadium63BO>();
		// Initialize : allTeams
		this.allTeams = new java.util.ArrayList<com.bluage.documentation.business.standardfeatures.entities.uc63_association.bos.Team63BO>();
		// Initialize : allCoachs
		this.allCoachs = new java.util.ArrayList<com.bluage.documentation.business.standardfeatures.entities.uc63_association.bos.Coach63BO>();
											this.setAlwaysCallPreControllers(true);
	}
	
	/**
	 * 	Getter : allStates 
	 * 	@return : Return the allStates instance.
	 */
	public List<State63BO> getAllStates(){
		return allStates; // For UC63_HomeForm
	}
	
	/**
	 * 	Setter : allStates 
	 *  @param allStatesinstance : The instance to set.
	 */
	public void setAllStates(final List<State63BO> allStatesinstance){
		this.allStates = allStatesinstance;// For UC63_HomeForm
	}
	/**
	 * 	Getter : allStadiums 
	 * 	@return : Return the allStadiums instance.
	 */
	public List<Stadium63BO> getAllStadiums(){
		return allStadiums; // For UC63_HomeForm
	}
	
	/**
	 * 	Setter : allStadiums 
	 *  @param allStadiumsinstance : The instance to set.
	 */
	public void setAllStadiums(final List<Stadium63BO> allStadiumsinstance){
		this.allStadiums = allStadiumsinstance;// For UC63_HomeForm
	}
	/**
	 * 	Getter : allTeams 
	 * 	@return : Return the allTeams instance.
	 */
	public List<Team63BO> getAllTeams(){
		return allTeams; // For UC63_HomeForm
	}
	
	/**
	 * 	Setter : allTeams 
	 *  @param allTeamsinstance : The instance to set.
	 */
	public void setAllTeams(final List<Team63BO> allTeamsinstance){
		this.allTeams = allTeamsinstance;// For UC63_HomeForm
	}
	/**
	 * 	Getter : allCoachs 
	 * 	@return : Return the allCoachs instance.
	 */
	public List<Coach63BO> getAllCoachs(){
		return allCoachs; // For UC63_HomeForm
	}
	
	/**
	 * 	Setter : allCoachs 
	 *  @param allCoachsinstance : The instance to set.
	 */
	public void setAllCoachs(final List<Coach63BO> allCoachsinstance){
		this.allCoachs = allCoachsinstance;// For UC63_HomeForm
	}
/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UC63_HomeForm [ "+
ALL_STATES +" = " + allStates +ALL_STADIUMS +" = " + allStadiums +ALL_TEAMS +" = " + allTeams +ALL_COACHS +" = " + allCoachs + "]";
	}
	
	/**
	 * Method setInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public void setInstanceByInstanceName(final String instanceName, final Object instance) {
		// Set the instance AllStates.
		if(ALL_STATES.equals(instanceName)){// For UC63_HomeForm
			this.setAllStates((List<State63BO>)instance); // For UC63_HomeForm
		}
				// Set the instance AllStadiums.
		if(ALL_STADIUMS.equals(instanceName)){// For UC63_HomeForm
			this.setAllStadiums((List<Stadium63BO>)instance); // For UC63_HomeForm
		}
				// Set the instance AllTeams.
		if(ALL_TEAMS.equals(instanceName)){// For UC63_HomeForm
			this.setAllTeams((List<Team63BO>)instance); // For UC63_HomeForm
		}
				// Set the instance AllCoachs.
		if(ALL_COACHS.equals(instanceName)){// For UC63_HomeForm
			this.setAllCoachs((List<Coach63BO>)instance); // For UC63_HomeForm
		}
			}
	
	/**
	 * Method getInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public Object getInstanceByInstanceName(final String instanceName){
		// Temporary object for tmpUC63_HomeForm.
		Object tmpUC63_HomeForm = null;
		
			
		// Get the instance AllStates for UC63_HomeForm.
		if(ALL_STATES.equals(instanceName)){ // For UC63_HomeForm
			tmpUC63_HomeForm = this.getAllStates(); // For UC63_HomeForm
		}
			
		// Get the instance AllStadiums for UC63_HomeForm.
		if(ALL_STADIUMS.equals(instanceName)){ // For UC63_HomeForm
			tmpUC63_HomeForm = this.getAllStadiums(); // For UC63_HomeForm
		}
			
		// Get the instance AllTeams for UC63_HomeForm.
		if(ALL_TEAMS.equals(instanceName)){ // For UC63_HomeForm
			tmpUC63_HomeForm = this.getAllTeams(); // For UC63_HomeForm
		}
			
		// Get the instance AllCoachs for UC63_HomeForm.
		if(ALL_COACHS.equals(instanceName)){ // For UC63_HomeForm
			tmpUC63_HomeForm = this.getAllCoachs(); // For UC63_HomeForm
		}
		return tmpUC63_HomeForm;// For UC63_HomeForm
	}
	
									}
