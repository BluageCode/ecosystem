/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.gettingstarted.uc03_formvalidation ;


import org.apache.log4j.Logger;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;

import com.netfective.bluage.core.validator.AbstractValidator;
import com.netfective.bluage.core.validator.ValidatorsUtil;
/**
* Class :UC03_FormValidationValidator
*/
public class UC03_FormValidationValidator extends AbstractValidator{
	
	// LOGGER for the class UC03_FormValidationValidator
	private static final Logger LOGGER = Logger.getLogger( UC03_FormValidationValidator.class);
	
	
	/**
	* Operation validate for UC03_FormValidationForm
	* @param obj : the current form (UC03_FormValidationForm)
	* @param errors : The spring errors to return for the form UC03_FormValidationForm
	*/
	public void validate(final Object obj,final Errors errors){
		LOGGER.info("Beginning method : validate"); // UC03_FormValidationValidator
		UC03_FormValidationForm cUC03_FormValidationForm = (UC03_FormValidationForm)obj; // UC03_FormValidationValidator
					if(LOGGER.isDebugEnabled()){
				LOGGER.debug("validation of the 'playerToCreate.height' field with the RangeValidator validator");
			}
			
			if(ValidatorsUtil.rangeValidator(cUC03_FormValidationForm.getPlayerToCreate().getHeight(), "150", "240")== 0 ){
				errors.rejectValue("playerToCreate.height", "", "Player height should be between 150 and 240 cms.");
			} 
				
				ValidationUtils.rejectIfEmpty(errors, "playerToCreate.firstName", "", "First name is required.");

		if(errors.getFieldError("playerToCreate.firstName")== null){
						if(LOGGER.isDebugEnabled()){
				LOGGER.debug("validation of the 'playerToCreate.firstName' field with the LengthValidator validator");
			}
			
			if(ValidatorsUtil.lengthValidator(cUC03_FormValidationForm.getPlayerToCreate().getFirstName(), 1, 30) == 0){
				errors.rejectValue("playerToCreate.firstName", "", "First name should be less than 30 characters long.");
			} 
				
		}
				ValidationUtils.rejectIfEmpty(errors, "playerToCreate.lastName", "", "Last name is required.");

		if(errors.getFieldError("playerToCreate.lastName")== null){
						if(LOGGER.isDebugEnabled()){
				LOGGER.debug("validation of the 'playerToCreate.lastName' field with the LengthValidator validator");
			}
			
			if(ValidatorsUtil.lengthValidator(cUC03_FormValidationForm.getPlayerToCreate().getLastName(), 1, 30) == 0){
				errors.rejectValue("playerToCreate.lastName", "", "Last name should be less than 30 characters long.");
			} 
				
		}
				
					if(LOGGER.isDebugEnabled()){
				LOGGER.debug("validation of the 'playerToCreate.weight' field with the RangeValidator validator");
			}
			
			if(ValidatorsUtil.rangeValidator(cUC03_FormValidationForm.getPlayerToCreate().getWeight(), "60", "140")== 0 ){
				errors.rejectValue("playerToCreate.weight", "", "Player weight should be between 60 and 140 kgs.");
			} 
				
		LOGGER.info("Ending method : validate the form "+ cUC03_FormValidationForm.getClass().getName()); // UC03_FormValidationValidator
	}

	/**
	* Method to implements to use spring validators (UC03_FormValidationForm)
	* @param aClass : Class for the form UC03_FormValidationForm
	* @return boolean
	*/
	public boolean supports(final Class aClass){
		return (new UC03_FormValidationForm()).getClass().equals(aClass); // UC03_FormValidationValidator
	}
}
