<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file="/include.jsp"  %>
<html >
<head >
<title  > 
<fmt:message key="cUC61_AddTeam_BLU_AGE_DOCUMENTATION"/>
</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"  > 
</meta>
<link href="<c:url value= "/presentation/_style/_style.css"/>" rel="stylesheet" type="text/css"  /> 
<link href="<c:url value= "/presentation/_include/jquery/rd_lib.css"/>" rel="stylesheet" type="text/css"  /> 
<script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/jquery.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/rd_setup.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/rd_lib.js"/>" space="preserve"  ></script><link rel="shortcut icon" href="<c:url value= "/presentation/_style/favicon.ico"/>"  /> 

</head>

<body class=" " > 
<div id="rd_ghost"  > 
<small  > 
<fmt:message key="cUC61_AddTeam___CSS_DEACTIVATED"/>
</small>
</div>
<div id="rd_jghost"  > 
<small  > 
<fmt:message key="cUC61_AddTeam___JAVASCRIPT_DEACTIVATED"/>
</small>
</div>
<div id="shape"  > 
<div class="ui-layout-center"  > 
<div class="east-north"  > 
	<form:form id="uC61_AddTeam" commandName="uC61_AddTeamForm"  enctype="application/x-www-form-urlencoded" method="post">
<h2 class="rd_first"  > 
<fmt:message key="cUC61_AddTeam_STANDARD_FEATURES__ENTITIES_"/>
</h2>
<table class="rd_tableform"  >
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC61_AddTeam_ID"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
<form:input id="txt_id" path="teamToAdd.id"  
		/>
</td> 
<td rowspan="1" colspan="1"  >
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC61_AddTeam_CITY"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
<form:input id="txt_city" path="teamToAdd.city"  
		/>
</td> 
<td rowspan="1" colspan="1"  >
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC61_AddTeam_NAME"/>
</label>
<label style="background: none;color: red;"  > 
<fmt:message key="cUC61_AddTeam_"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
<form:input id="txt_name" path="teamToAdd.name"  
		/>
<form:errors path="teamToAdd.name"/>
</td> 
<td rowspan="1" colspan="1"  >
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<fmt:message key="cUC61_AddTeam__140"/>
</td> 
<td rowspan="1" colspan="1"  >
<a id="lnk_addteam"onclick="javascript:btn_click_cExecuteUC61_AddTeam(this.href);return false;"				href="<c:url value="/presentation/standardfeatures/entities/uc61_transientobject/UC61_AddTeam/lnk_addteam.html"/>"
							 shape="rect"  >									 
<fmt:message key="cUC61_AddTeam_CREATE_A_NEW_TEAM"/>
</a>
</td> 
</tr>
</table> 
<label style="background: none;color: red;"  > 
<fmt:message key="cUC61_AddTeam__141"/>
</label>
<fmt:message key="cUC61_AddTeam_REQUIRED_ENTRY_"/>
<br clear="none"  /> 
<br clear="none"  /> 
<display:table uid="listTeam"  sort="list" requestURI="" name="sessionScope.uC61_AddTeamForm.teams" class="rd_tabledata"  >
	<c:set   var="rownum">
		<%=(listTeam !=	null) ? new Integer(listTeam_rowNum.intValue() - 1) : new Integer(0) %>
	</c:set>
	<c:set   var="idRow">
		<%=(listTeam != null ) ? listTeam_rowNum.intValue() - 1 : -1 %>
	</c:set>
	<c:set   var="idTab">
		<%=(listTeam != null ) ? "listTeam" : null %>
	</c:set>
	<c:set var="tableHashId">
		<%= new org.displaytag.util.ParamEncoder("listTeam").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE) %>
	</c:set>
	<c:set var="tableHashId2">
		<%=request.getParameter(new org.displaytag.util.ParamEncoder("listTeam").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE)) %>
	</c:set>	
	
<display:column format=""  titleKey="cUC61_AddTeam_ID_142">
		<span id="txt_id">
	<c:out value="${listTeam.id}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="cUC61_AddTeam_NAME_143">
		<span id="txt_name1">
	<c:out value="${listTeam.name}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="cUC61_AddTeam_CITY_144">
		<span id="txt_city2">
	<c:out value="${listTeam.city}" />
		</span>
	</display:column>
</display:table>
	</form:form>
	<script language="javascript">
		function btn_click_cExecuteUC61_AddTeam(url) {
			UC61_AddTeamvar = document.getElementById('uC61_AddTeam');
			UC61_AddTeamvar.setAttribute('action', url);
			UC61_AddTeamvar.submit();
		}
		function btn_click_cSetActionUC61_AddTeam(url) {
			UC61_AddTeamvar = document.getElementById('uC61_AddTeam');
			UC61_AddTeamvar.setAttribute('action', url);
		}
	</script>
</div>
<div class="east-center"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/entities/uc61_transientobject/UC61_AddTeamcmp_tabs" />
</div>
</div>
<div class="ui-layout-north"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/entities/uc61_transientobject/UC61_AddTeamcmp_header" />
</div>
<div class="ui-layout-south"  > 
<fmt:message key="cUC61_AddTeam___"/>
<span id="rd_jversion"  > 
<fmt:message key="cUC61_AddTeam_BLU_AGE_DOCUMENTATION_145"/>
</span>
</div>
<div class="ui-layout-west"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/entities/uc61_transientobject/UC61_AddTeamcmp_treeview" />
</div>
</div>
</body>
</html>
