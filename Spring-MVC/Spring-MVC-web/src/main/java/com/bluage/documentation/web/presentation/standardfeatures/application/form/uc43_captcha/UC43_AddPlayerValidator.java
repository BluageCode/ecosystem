/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.application.form.uc43_captcha ;


import org.apache.log4j.Logger;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;

import com.netfective.bluage.core.validator.AbstractValidator;
import com.netfective.bluage.core.validator.ValidatorsUtil;
/**
* Class :UC43_AddPlayerValidator
*/
public class UC43_AddPlayerValidator extends AbstractValidator{
	
	// LOGGER for the class UC43_AddPlayerValidator
	private static final Logger LOGGER = Logger.getLogger( UC43_AddPlayerValidator.class);
	
	
	/**
	* Operation validate for UC43_AddPlayerForm
	* @param obj : the current form (UC43_AddPlayerForm)
	* @param errors : The spring errors to return for the form UC43_AddPlayerForm
	*/
	public void validate(final Object obj,final Errors errors){
		LOGGER.info("Beginning method : validate"); // UC43_AddPlayerValidator
		UC43_AddPlayerForm cUC43_AddPlayerForm = (UC43_AddPlayerForm)obj; // UC43_AddPlayerValidator
				ValidationUtils.rejectIfEmpty(errors, "playerToAdd.firstName", "", "First name is required.");

		if(errors.getFieldError("playerToAdd.firstName")== null){
						if(LOGGER.isDebugEnabled()){
				LOGGER.debug("validation of the 'playerToAdd.firstName' field with the LengthValidator validator");
			}
			
			if(ValidatorsUtil.lengthValidator(cUC43_AddPlayerForm.getPlayerToAdd().getFirstName(), 1, 30) == 0){
				errors.rejectValue("playerToAdd.firstName", "", "First name should be less than 30 characters long.");
			} 
				
		}
				ValidationUtils.rejectIfEmpty(errors, "playerToAdd.lastName", "", "Last name is required.");

		if(errors.getFieldError("playerToAdd.lastName")== null){
						if(LOGGER.isDebugEnabled()){
				LOGGER.debug("validation of the 'playerToAdd.lastName' field with the LengthValidator validator");
			}
			
			if(ValidatorsUtil.lengthValidator(cUC43_AddPlayerForm.getPlayerToAdd().getLastName(), 1, 30) == 0){
				errors.rejectValue("playerToAdd.lastName", "", "Last name should be less than 30 characters long.");
			} 
				
		}
		LOGGER.info("Ending method : validate the form "+ cUC43_AddPlayerForm.getClass().getName()); // UC43_AddPlayerValidator
	}

	/**
	* Method to implements to use spring validators (UC43_AddPlayerForm)
	* @param aClass : Class for the form UC43_AddPlayerForm
	* @return boolean
	*/
	public boolean supports(final Class aClass){
		return (new UC43_AddPlayerForm()).getClass().equals(aClass); // UC43_AddPlayerValidator
	}
}
