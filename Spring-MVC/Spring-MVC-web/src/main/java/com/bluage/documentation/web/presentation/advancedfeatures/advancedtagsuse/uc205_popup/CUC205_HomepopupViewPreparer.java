/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.web.presentation.advancedfeatures.advancedtagsuse.uc205_popup;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.tiles.Attribute;
import org.apache.tiles.AttributeContext;
import org.apache.tiles.ListAttribute;
import org.apache.tiles.context.TilesRequestContext;
import org.apache.tiles.preparer.ViewPreparer;

import com.netfective.bluage.core.common.IStringConstants;
import com.netfective.bluage.core.web.ComponentsBean;
/**
 * Class : CUC205_HomepopupViewPreparer
 * Prepare the component popup for the screen UC205_Home.
 * 
 */
public class CUC205_HomepopupViewPreparer implements ViewPreparer , IStringConstants{

	private static final String UC205_HOMEPOPUP = "popupComponentBean";
	
	private static final String COMPONENT_FORM_NAME = "com.bluage.documentation.web.presentation.advancedfeatures.advancedtagsuse.uc205_popup.popupForm";
	
	private static final String COMPONENT_SHORT_FORM_NAME = "popupForm";
	private static final String PARENT_SHORT_FORM_NAME = "uC205_HomeForm";

	/**
	 * Property:LOGGER
	 */
	private static final Logger LOGGER = Logger.getLogger( CUC205_HomepopupViewPreparer.class);

	/**
	 * Method execute for CUC205_HomepopupViewPreparer
	 * @param tilesContext : The tiles context of the component  CUC205_HomepopupViewPreparer
	 * @param attributeContext : The attribute context of the component  CUC205_HomepopupViewPreparer
	 * @see org.apache.tiles.preparer.ViewPreparer#execute(org.apache.tiles.context.TilesRequestContext, org.apache.tiles.AttributeContext)
	 */
	public void execute(TilesRequestContext tilesContext, AttributeContext attributeContext) {
		
		LOGGER.info("Starting the component initialisation for CUC205_HomepopupViewPreparer "+ UC205_HOMEPOPUP +" Component Form name : "+COMPONENT_FORM_NAME);
		
		// Retrieve the attributs from the component bean popupComponentBean
		final Attribute attrListInstanceNameCUC205_HomepopupViewPreparer = attributeContext.getAttribute(LIST_INSTANCES); // All the list instancs for popup
		final Attribute attrParentPathScreenNameCUC205_HomepopupViewPreparer = attributeContext.getAttribute(PARENT_PATH_SCREEN_NAME); // The parent screen popup
		final Attribute attrParentHelperNameCUC205_HomepopupViewPreparer = attributeContext.getAttribute(PARENT_HELPER_NAME); // The Parent helper name for popup

		// Init the component bean for popupComponentBean
		final ComponentsBean compB = new ComponentsBean(); // Init the corresponding popupComponentBean
		Map<String, String> tmpInstanceNameMp = new HashMap<String, String>(); // Init values to manage the popupComponentBean

		// Fill the ComponentBean (popupComponentBean)
		// Retrieve from the tiles configuration the parentHelperName popupComponentBean
		compB.setParentHelperName((String) attrParentHelperNameCUC205_HomepopupViewPreparer.getValue()); //Set the parent helper for popupComponentBean

		// Retrieve from the tiles configuration the parentPathScreenName
		compB.setParentPathScreenName((String) attrParentPathScreenNameCUC205_HomepopupViewPreparer.getValue()); // Set the parent screen path popupComponentBean

		final List tmpInstanceNameList = (List) attrListInstanceNameCUC205_HomepopupViewPreparer.getValue(); // Retrieve the attribut list to populate popupComponentBean

		// Transform the List to a Map, more efficient (popupComponentBean)
		for (final Iterator iterator = tmpInstanceNameList.iterator(); iterator.hasNext();) {
			final ListAttribute object = (ListAttribute) iterator.next(); // For all the instances popupComponentBean
			final Object[] strArr = (Object[]) ((List)object.getValue()).toArray(); // To array popupComponentBean
			// The instances always are by pairs, the first is the source, the
			// second the destination.
			final String instanceSource = (String) ((Attribute)strArr[0]).getValue(); // Source screen instance name popupComponentBean
			final String instanceDestination = (String) ((Attribute)strArr[1]).getValue();// Destination screen instance name popupComponentBean

			tmpInstanceNameMp.put(instanceSource, instanceDestination); // Put in the instance popupComponentBean
		}

		// Set.
		compB.setAttributs(tmpInstanceNameMp); // Set to the bean popupComponentBean

		if (LOGGER.isDebugEnabled()) { // Display debug information for popupComponentBean
			LOGGER.debug("The Component Bean  : " + compB + " will be saved in session and identified by : " + UC205_HOMEPOPUP);
		}

		// Get the main sreen : UC205_HomeForm
		final UC205_HomeForm componentScreenMainScreen = (UC205_HomeForm) tilesContext.getSessionScope().get(PARENT_SHORT_FORM_NAME);

		// Init the component popupForm
		final popupForm componentScreen = new popupForm(); // For the form popupForm
		// 
		componentScreen.setInstanceByInstanceName("allPlayers", componentScreenMainScreen.getAllPlayers()); // Set the property allPlayers 
		componentScreen.setInstanceByInstanceName("cp_traitementShowPopUp", componentScreenMainScreen.getCp_traitementShowPopUp()); // Set the property cp_traitementShowPopUp
				
		tilesContext.getSessionScope().put(COMPONENT_SHORT_FORM_NAME, componentScreen); // For the component CUC205_HomepopupViewPreparer
		
		tilesContext.getSessionScope().put(UC205_HOMEPOPUP, compB);
		LOGGER.info("The component is initialized (CUC205_HomepopupViewPreparer).");
	}
}
