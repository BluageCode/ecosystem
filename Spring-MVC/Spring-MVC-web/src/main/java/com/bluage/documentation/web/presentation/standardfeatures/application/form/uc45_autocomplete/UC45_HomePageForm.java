/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.application.form.uc45_autocomplete;


import java.io.Serializable;
import java.util.List;

import com.bluage.documentation.business.standardfeatures.application.form.uc45_autocomplete.bos.Player45BO;
import com.netfective.bluage.core.web.AbstractForm;
import com.netfective.bluage.core.web.IForm;

/**
* Class : UC45_HomePageForm
*/
public class UC45_HomePageForm extends AbstractForm implements Serializable, IForm {
	
	/** Constant : serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	//CONSTANT : instance_players
	private static final String INSTANCE_PLAYERS = "instance_players";
	//CONSTANT : name
	private static final String NAME = "name";
	/**
	 * 	Property: instance_players 
	 */
	private List instance_players;
	/**
	 * 	Property: name 
	 */
	private Player45BO name;
/**
	 * Default constructor : UC45_HomePageForm
	 */
	public UC45_HomePageForm() {
		super();
		// Initialize : instance_players
		this.instance_players = null;
		// Initialize : name
		this.name = null;
			this.setAlwaysCallPreControllers(true);
	}
	
	/**
	 * 	Getter : instance_players 
	 * 	@return : Return the instance_players instance.
	 */
	public List getInstance_players(){
		return instance_players; // For UC45_HomePageForm
	}
	
	/**
	 * 	Setter : instance_players 
	 *  @param instance_playersinstance : The instance to set.
	 */
	public void setInstance_players(final List instance_playersinstance){
		this.instance_players = instance_playersinstance;// For UC45_HomePageForm
	}
	/**
	 * 	Getter : name 
	 * 	@return : Return the name instance.
	 */
	public Player45BO getName(){
		return name; // For UC45_HomePageForm
	}
	
	/**
	 * 	Setter : name 
	 *  @param nameinstance : The instance to set.
	 */
	public void setName(final Player45BO nameinstance){
		this.name = nameinstance;// For UC45_HomePageForm
	}
/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UC45_HomePageForm [ "+
INSTANCE_PLAYERS +" = " + instance_players +NAME +" = " + name + "]";
	}
	
	/**
	 * Method setInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public void setInstanceByInstanceName(final String instanceName, final Object instance) {
		// Set the instance Instance_players.
		if(INSTANCE_PLAYERS.equals(instanceName)){// For UC45_HomePageForm
			this.setInstance_players((List)instance); // For UC45_HomePageForm
		}
				// Set the instance Name.
		if(NAME.equals(instanceName)){// For UC45_HomePageForm
			this.setName((Player45BO)instance); // For UC45_HomePageForm
		}
			}
	
	/**
	 * Method getInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public Object getInstanceByInstanceName(final String instanceName){
		// Temporary object for tmpUC45_HomePageForm.
		Object tmpUC45_HomePageForm = null;
		
			
		// Get the instance Instance_players for UC45_HomePageForm.
		if(INSTANCE_PLAYERS.equals(instanceName)){ // For UC45_HomePageForm
			tmpUC45_HomePageForm = this.getInstance_players(); // For UC45_HomePageForm
		}
			
		// Get the instance Name for UC45_HomePageForm.
		if(NAME.equals(instanceName)){ // For UC45_HomePageForm
			tmpUC45_HomePageForm = this.getName(); // For UC45_HomePageForm
		}
		return tmpUC45_HomePageForm;// For UC45_HomePageForm
	}
	
	}
