/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.business.standardfeatures.application.form.uc41_actionondropdownselection.bos;

import javax.inject.Inject;
import javax.inject.Named;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import com.bluage.documentation.business.common.bos.PositionBO;
import com.bluage.documentation.business.standardfeatures.application.form.uc41_actionondropdownselection.entities.daofinder.Position41DAOFinderImpl;

/**
 * Position41BO
 *
* @see com.netfective.bluage.core.business.IBusinessObject
* * @see PositionBO
 */
@XmlRootElement

public class Position41BO
 extends PositionBO {
	
	/**
	 * Default SUID for Position41BO.
	 */
	private static final long serialVersionUID = 1L;
	

	@Inject
	@Named ("com.bluage.documentation.business.standardfeatures.application.form.uc41_actionondropdownselection.entities.daofinder.Position41Finder") 
	private Position41DAOFinderImpl finder;

	
	private boolean displayPosition = true;

   /**
	* public default constructor for class Position41BO
	*/
	public Position41BO(){
		//NOP Position41BO
	}


	
		
	/**
	 * Getter for displayPosition of class Position41BO.
	 * @return {@link boolean}
	 */
	public boolean isDisplayPosition()
	{
		return this.displayPosition;
	}
	
	/**
	 * @deprecated Use isDisplayPosition() instead.
	 */
	public boolean getDisplayPosition()
	{
		return this.displayPosition;
	}
	
	/**
	 * Setter for displayPosition of class Position41BO.
	 * @param value: {@link boolean}
	 */
	public void setDisplayPosition(boolean displayPosition) 
	{
		this.displayPosition = displayPosition;
	}
/**
	 * Returns <code>true</code> if the argument is an Position41 instance and all identifiers for this entity
     * equal the identifiers of the argument entity. The <code>equals</code> method of the parent entity
     * will also need to return <code>true</code>. Returns <code>false</code> otherwise.
     *
     * @see PositionBO#hashCode()
	 * @param object  object to compare with current instance      
     * @return boolean true if equals false if not
     */
    public boolean equals(final Object object )
    {
		if ( this == object )
        {
            return true;
        }
        if ( !( object instanceof Position41BO ) )
        {
            return false;
        }
        final Position41BO that = (Position41BO)object;
    
    	            if (        				        			this.getCode() == null
        	        ) {
        	return false;
        }
		return new EqualsBuilder()
			.append(this.getCode(), that.getCode())
			 	.isEquals();
	}
	/**

	 * Returns a hash code based on this entity's identifiers and the hash code of the parent entity.
     *
     * @see PositionBO#hashCode()
     * @return int
     */    
    public int hashCode() {
        return new HashCodeBuilder()
             .append(getCode())
				 	.appendSuper(super.hashCode())
            .toHashCode();
    }
}

