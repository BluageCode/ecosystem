/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.business.advancedfeatures.performance.uc200_datagridloading.bos;

import javax.inject.Inject;
import javax.inject.Named;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import com.bluage.documentation.business.AbstractBusinessObject;
import com.bluage.documentation.business.advancedfeatures.performance.uc200_datagridloading.entities.daofinder.Stats200DAOFinderImpl;

/**
 * Stats200BO
 *
* @see com.netfective.bluage.core.business.IBusinessObject
 */
@XmlRootElement

public class Stats200BO
 extends AbstractBusinessObject {
	
	/**
	 * Default SUID for Stats200BO.
	 */
	private static final long serialVersionUID = 1L;
	

	@Inject
	@Named ("com.bluage.documentation.business.advancedfeatures.performance.uc200_datagridloading.entities.daofinder.Stats200Finder") 
	private Stats200DAOFinderImpl finder;
	/**
	 * Hibernate optimistic lock for class Stats200BO.
	 */
	private int version;

						/**
	 * Entity property: id
	 *
	 */
private Long id;

					/**
	 * Entity property: shoots
	 *
	 */
private Integer shoots;

					/**
	 * Entity property: plays
	 *
	 */
private Integer plays;



   /**
	* public default constructor for class Stats200BO
	*/
	public Stats200BO(){
		//NOP Stats200BO
	}

	
	/**
	 * Getter for version for class Stats200BO
	 * @return int
	 */
	public int getVersion() {
		return this.version;
	}
	
	/**
	 * Setter for version for class Stats200BO
	 * @param int version
	 */
	public void setVersion(final int version) {
		this.version = version;
	}

						/**
	 * Getter for id of class Stats200BO.
	 *
	 * @return {@link java.lang.Long}
	 * @generated
	 */ 
public  Long getId() {
	return this.id;
	}


	/**
	 * Setter for id of class Stats200BO.
	 * @param value {@link java.lang.Long}
	 * @generated
	 */

public  void setId(final Long id) {
	this.isEmpty = (id == null);
	this.id = id;
}

					/**
	 * Getter for shoots of class Stats200BO.
	 *
	 * @return {@link java.lang.Integer}
	 * @generated
	 */ 
public  Integer getShoots() {
	return this.shoots;
	}


	/**
	 * Setter for shoots of class Stats200BO.
	 * @param value {@link java.lang.Integer}
	 * @generated
	 */

public  void setShoots(final Integer shoots) {
	this.shoots = shoots;
}

					/**
	 * Getter for plays of class Stats200BO.
	 *
	 * @return {@link java.lang.Integer}
	 * @generated
	 */ 
public  Integer getPlays() {
	return this.plays;
	}


	/**
	 * Setter for plays of class Stats200BO.
	 * @param value {@link java.lang.Integer}
	 * @generated
	 */

public  void setPlays(final Integer plays) {
	this.plays = plays;
}


/**
	 * Returns <code>true</code> if the argument is an Stats200 instance and all identifiers for this entity
     * equal the identifiers of the argument entity. Returns <code>false</code> otherwise.
	 * @param object  object to compare with current instance      
     * @return boolean true if equals false if not
     */
    public boolean equals(final Object object )
    {
		if ( this == object )
        {
            return true;
        }
        if ( !( object instanceof Stats200BO ) )
        {
            return false;
        }
        final Stats200BO that = (Stats200BO)object;
    
    	            if (        				        			this.getId() == null
        	        ) {
        	return false;
        }
		return new EqualsBuilder()
			.append(this.getId(), that.getId())
			 	.appendSuper(super.equals(object))
		 	.isEquals();
	}
	/**
     * Returns a hash code based on this entity's identifiers.     * @return int
     */    
    public int hashCode() {
        return new HashCodeBuilder()
             .append(getId())
		            .toHashCode();
    }
}

