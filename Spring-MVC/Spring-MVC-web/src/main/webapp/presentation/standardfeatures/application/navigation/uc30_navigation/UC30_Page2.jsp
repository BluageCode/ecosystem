<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file="/include.jsp"  %>
<html >
<head >
<title  > 
<fmt:message key="cUC30_Page2_BLU_AGE_DOCUMENTATION"/>
</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"  > 
</meta>
<link href="<c:url value= "/presentation/_style/_style.css"/>" rel="stylesheet" type="text/css"  /> 
<link href="<c:url value= "/presentation/_include/jquery/rd_lib.css"/>" rel="stylesheet" type="text/css"  /> 
<script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/jquery.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/rd_setup.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/rd_lib.js"/>" space="preserve"  ></script><link rel="shortcut icon" href="<c:url value= "/presentation/_style/favicon.ico"/>"  /> 

</head>

<body class=" " > 
<div id="rd_ghost"  > 
<small  > 
<fmt:message key="cUC30_Page2___CSS_DEACTIVATED"/>
</small>
</div>
<div id="rd_jghost"  > 
<small  > 
<fmt:message key="cUC30_Page2___JAVASCRIPT_DEACTIVATED"/>
</small>
</div>
<div id="shape"  > 
<div class="ui-layout-center"  > 
<div class="east-north"  > 
	<form:form id="uC30_Page2" commandName="uC30_Page2Form"  enctype="application/x-www-form-urlencoded" method="post">
<h2 class="rd_first"  > 
<fmt:message key="cUC30_Page2_STANDARD_FEATURES__APPLICATIO"/>
</h2>
<h4  > 
<fmt:message key="cUC30_Page2_HELLO_WORLD_"/>
</h4>
<table class="rd_tableform"  >
<tr  > 
<td rowspan="1" colspan="1"  >
<input type="submit" value="back" id="lnk_page1"  name="lnkpage1" title="go back"  onclick=" btn_click_cSetActionUC30_Page2('<c:url  value="/presentation/standardfeatures/application/navigation/uc30_navigation/UC30_Page2/lnk_page1.html"/>')">
</input>
</td> 
</tr>
</table> 
	</form:form>
	<script language="javascript">
		function btn_click_cExecuteUC30_Page2(url) {
			UC30_Page2var = document.getElementById('uC30_Page2');
			UC30_Page2var.setAttribute('action', url);
			UC30_Page2var.submit();
		}
		function btn_click_cSetActionUC30_Page2(url) {
			UC30_Page2var = document.getElementById('uC30_Page2');
			UC30_Page2var.setAttribute('action', url);
		}
	</script>
</div>
<div class="east-center"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/application/navigation/uc30_navigation/UC30_Page2cmp_tabs" />
</div>
</div>
<div class="ui-layout-north"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/application/navigation/uc30_navigation/UC30_Page2cmp_header" />
</div>
<div class="ui-layout-south"  > 
<fmt:message key="cUC30_Page2______"/>
<span id="rd_jversion"  > 
<fmt:message key="cUC30_Page2_BLU_AGE_DOCUMENTATION_124"/>
</span>
</div>
<div class="ui-layout-west"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/application/navigation/uc30_navigation/UC30_Page2cmp_treeview" />
</div>
</div>
</body>
</html>
