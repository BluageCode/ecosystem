/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.business.standardfeatures.services.standard.uc58_servicecopy.bos;

import javax.inject.Inject;
import javax.inject.Named;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import com.bluage.documentation.business.common.bos.TeamBO;
import com.bluage.documentation.business.standardfeatures.services.standard.uc58_servicecopy.entities.daofinder.Team58DAOFinderImpl;

/**
 * Team58BO
 *
* @see com.netfective.bluage.core.business.IBusinessObject
* * @see TeamBO
 */
@XmlRootElement

public class Team58BO
 extends TeamBO {
	
	/**
	 * Default SUID for Team58BO.
	 */
	private static final long serialVersionUID = 1L;
	

	@Inject
	@Named ("com.bluage.documentation.business.standardfeatures.services.standard.uc58_servicecopy.entities.daofinder.Team58Finder") 
	private Team58DAOFinderImpl finder;

												/**
	 * Entity property: state
	 *
	 */
private State58BO state;



   /**
	* public default constructor for class Team58BO
	*/
	public Team58BO(){
		//NOP Team58BO
	}


												/**
	 * Getter for state of class Team58BO.
	 *
	 * @return {@link com.bluage.documentation.business.standardfeatures.services.standard.uc58_servicecopy.bos.State58BO}
	 * @generated
	 */ 
public  State58BO getState() {
		State58BO state0 = this.state;
		if(this.state == null) {
			if(DAO_MODE.equals(this.getObjMode( ) ) ) {
				state0 = null;
			} else {
				this.state = new State58BO();
				this.state.setObjMode( this.getObjMode( ) );
				state0 = this.state;
			}
		} else {
			this.state.setObjMode( this.getObjMode( ) );
			if( HELPER_MODE.equals(this.getObjMode( ) ) ) {
				state0 = this.state;
			}
			if( DAO_MODE.equals(this.getObjMode()) && this.state.isEmpty ) {
				state0 = null;
			}
		}
		return state0;
	}


	/**
	 * Setter for state of class Team58BO.
	 * @param value {@link com.bluage.documentation.business.standardfeatures.services.standard.uc58_servicecopy.bos.State58BO}
	 * @generated
	 */

public  void setState(final State58BO state) {
	if(state != null) {
		state.isEmpty = false;
		state.setObjMode( this.getObjMode( ) );
	}
	this.isEmpty = false;
	this.state = state;
}


/**
	 * Returns <code>true</code> if the argument is an Team58 instance and all identifiers for this entity
     * equal the identifiers of the argument entity. The <code>equals</code> method of the parent entity
     * will also need to return <code>true</code>. Returns <code>false</code> otherwise.
     *
     * @see TeamBO#hashCode()
	 * @param object  object to compare with current instance      
     * @return boolean true if equals false if not
     */
    public boolean equals(final Object object )
    {
		if ( this == object )
        {
            return true;
        }
        if ( !( object instanceof Team58BO ) )
        {
            return false;
        }
        final Team58BO that = (Team58BO)object;
    
    	            if (        				        			this.getId() == null
        	        ) {
        	return false;
        }
		return new EqualsBuilder()
			.append(this.getId(), that.getId())
			 	.isEquals();
	}
	/**

	 * Returns a hash code based on this entity's identifiers and the hash code of the parent entity.
     *
     * @see TeamBO#hashCode()
     * @return int
     */    
    public int hashCode() {
        return new HashCodeBuilder()
             .append(getId())
				 	.appendSuper(super.hashCode())
            .toHashCode();
    }
}

