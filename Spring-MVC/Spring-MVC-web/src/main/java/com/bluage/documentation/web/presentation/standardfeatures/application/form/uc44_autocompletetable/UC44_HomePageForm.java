/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.application.form.uc44_autocompletetable;


import java.io.Serializable;
import java.util.List;

import com.bluage.documentation.business.standardfeatures.application.form.uc44_autocompletetable.bos.Team44BO;
import com.netfective.bluage.core.web.AbstractForm;
import com.netfective.bluage.core.web.IForm;

/**
* Class : UC44_HomePageForm
*/
public class UC44_HomePageForm extends AbstractForm implements Serializable, IForm {
	
	/** Constant : serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	//CONSTANT : instance_teams
	private static final String INSTANCE_TEAMS = "instance_teams";
	//CONSTANT : allPlayers
	private static final String ALL_PLAYERS = "allPlayers";
	//CONSTANT : selectedTeam
	private static final String SELECTED_TEAM = "selectedTeam";
	/**
	 * 	Property: instance_teams 
	 */
	private List instance_teams;
	/**
	 * 	Property: allPlayers 
	 */
	private List allPlayers;
	/**
	 * 	Property: selectedTeam 
	 */
	private Team44BO selectedTeam;
/**
	 * Default constructor : UC44_HomePageForm
	 */
	public UC44_HomePageForm() {
		super();
		// Initialize : instance_teams
		this.instance_teams = null;
		// Initialize : allPlayers
		this.allPlayers = null;
		// Initialize : selectedTeam
		this.selectedTeam = null;
			this.setAlwaysCallPreControllers(true);
	}
	
	/**
	 * 	Getter : instance_teams 
	 * 	@return : Return the instance_teams instance.
	 */
	public List getInstance_teams(){
		return instance_teams; // For UC44_HomePageForm
	}
	
	/**
	 * 	Setter : instance_teams 
	 *  @param instance_teamsinstance : The instance to set.
	 */
	public void setInstance_teams(final List instance_teamsinstance){
		this.instance_teams = instance_teamsinstance;// For UC44_HomePageForm
	}
	/**
	 * 	Getter : allPlayers 
	 * 	@return : Return the allPlayers instance.
	 */
	public List getAllPlayers(){
		return allPlayers; // For UC44_HomePageForm
	}
	
	/**
	 * 	Setter : allPlayers 
	 *  @param allPlayersinstance : The instance to set.
	 */
	public void setAllPlayers(final List allPlayersinstance){
		this.allPlayers = allPlayersinstance;// For UC44_HomePageForm
	}
	/**
	 * 	Getter : selectedTeam 
	 * 	@return : Return the selectedTeam instance.
	 */
	public Team44BO getSelectedTeam(){
		return selectedTeam; // For UC44_HomePageForm
	}
	
	/**
	 * 	Setter : selectedTeam 
	 *  @param selectedTeaminstance : The instance to set.
	 */
	public void setSelectedTeam(final Team44BO selectedTeaminstance){
		this.selectedTeam = selectedTeaminstance;// For UC44_HomePageForm
	}
/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UC44_HomePageForm [ "+
INSTANCE_TEAMS +" = " + instance_teams +ALL_PLAYERS +" = " + allPlayers +SELECTED_TEAM +" = " + selectedTeam + "]";
	}
	
	/**
	 * Method setInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public void setInstanceByInstanceName(final String instanceName, final Object instance) {
		// Set the instance Instance_teams.
		if(INSTANCE_TEAMS.equals(instanceName)){// For UC44_HomePageForm
			this.setInstance_teams((List)instance); // For UC44_HomePageForm
		}
				// Set the instance AllPlayers.
		if(ALL_PLAYERS.equals(instanceName)){// For UC44_HomePageForm
			this.setAllPlayers((List)instance); // For UC44_HomePageForm
		}
				// Set the instance SelectedTeam.
		if(SELECTED_TEAM.equals(instanceName)){// For UC44_HomePageForm
			this.setSelectedTeam((Team44BO)instance); // For UC44_HomePageForm
		}
			}
	
	/**
	 * Method getInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public Object getInstanceByInstanceName(final String instanceName){
		// Temporary object for tmpUC44_HomePageForm.
		Object tmpUC44_HomePageForm = null;
		
			
		// Get the instance Instance_teams for UC44_HomePageForm.
		if(INSTANCE_TEAMS.equals(instanceName)){ // For UC44_HomePageForm
			tmpUC44_HomePageForm = this.getInstance_teams(); // For UC44_HomePageForm
		}
			
		// Get the instance AllPlayers for UC44_HomePageForm.
		if(ALL_PLAYERS.equals(instanceName)){ // For UC44_HomePageForm
			tmpUC44_HomePageForm = this.getAllPlayers(); // For UC44_HomePageForm
		}
			
		// Get the instance SelectedTeam for UC44_HomePageForm.
		if(SELECTED_TEAM.equals(instanceName)){ // For UC44_HomePageForm
			tmpUC44_HomePageForm = this.getSelectedTeam(); // For UC44_HomePageForm
		}
		return tmpUC44_HomePageForm;// For UC44_HomePageForm
	}
	
	}
