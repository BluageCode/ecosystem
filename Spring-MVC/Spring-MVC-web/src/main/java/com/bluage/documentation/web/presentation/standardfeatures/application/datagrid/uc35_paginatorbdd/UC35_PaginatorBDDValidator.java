/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.application.datagrid.uc35_paginatorbdd ;


import org.apache.log4j.Logger;
import org.springframework.validation.Errors;

import com.netfective.bluage.core.validator.AbstractValidator;
/**
* Class :UC35_PaginatorBDDValidator
*/
public class UC35_PaginatorBDDValidator extends AbstractValidator{
	
	// LOGGER for the class UC35_PaginatorBDDValidator
	private static final Logger LOGGER = Logger.getLogger( UC35_PaginatorBDDValidator.class);
	
	
	/**
	* Operation validate for UC35_PaginatorBDDForm
	* @param obj : the current form (UC35_PaginatorBDDForm)
	* @param errors : The spring errors to return for the form UC35_PaginatorBDDForm
	*/
	public void validate(final Object obj,final Errors errors){
		LOGGER.info("Beginning method : validate"); // UC35_PaginatorBDDValidator
		UC35_PaginatorBDDForm cUC35_PaginatorBDDForm = (UC35_PaginatorBDDForm)obj; // UC35_PaginatorBDDValidator
		LOGGER.info("Ending method : validate the form "+ cUC35_PaginatorBDDForm.getClass().getName()); // UC35_PaginatorBDDValidator
	}

	/**
	* Method to implements to use spring validators (UC35_PaginatorBDDForm)
	* @param aClass : Class for the form UC35_PaginatorBDDForm
	* @return boolean
	*/
	public boolean supports(final Class aClass){
		return (new UC35_PaginatorBDDForm()).getClass().equals(aClass); // UC35_PaginatorBDDValidator
	}
}
