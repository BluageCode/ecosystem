/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.business.standardfeatures.entities.uc61_transientobject.bos;

import javax.xml.bind.annotation.XmlRootElement;

import com.bluage.documentation.business.common.bos.TeamBO;

/**
 * Team61BO
 *
* @see com.netfective.bluage.core.business.IBusinessObject
* * @see TeamBO
 */
@XmlRootElement

public class Team61BO
 extends TeamBO {
	
	/**
	 * Default SUID for Team61BO.
	 */
	private static final long serialVersionUID = 1L;
	

	

   /**
	* public default constructor for class Team61BO
	*/
	public Team61BO(){
		//NOP Team61BO
	}


	
}

