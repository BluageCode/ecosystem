/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.business.standardfeatures.services.hql.uc57_hqloperation.entities.dao;


import javax.inject.Named;

import com.bluage.documentation.business.AbstractDaoImpl;
import com.bluage.documentation.business.standardfeatures.services.hql.uc57_hqloperation.bos.State57BO;

/**
 * This class implements the persistence for the State57 object.
 *
 * @see 	AbstractDaoImpl
 */
 
@Named ("com.bluage.documentation.business.standardfeatures.services.hql.uc57_hqloperation.entities.dao.State57DAOImpl") 
public class State57DAOImpl extends AbstractDaoImpl<State57BO>{}
			

