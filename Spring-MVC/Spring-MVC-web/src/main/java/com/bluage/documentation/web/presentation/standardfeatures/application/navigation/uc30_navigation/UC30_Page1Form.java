/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.application.navigation.uc30_navigation;


import java.io.Serializable;
import java.util.List;

import com.bluage.documentation.business.standardfeatures.application.navigation.uc30_navigation.bos.Page30BO;
import com.netfective.bluage.core.web.AbstractForm;
import com.netfective.bluage.core.web.IForm;

/**
* Class : UC30_Page1Form
*/
public class UC30_Page1Form extends AbstractForm implements Serializable, IForm {
	
	/** Constant : serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	//CONSTANT : selectedPage
	private static final String SELECTED_PAGE = "selectedPage";
	//CONSTANT : listPage
	private static final String LIST_PAGE = "listPage";
	//CONSTANT : value
	private static final String VALUE = "value";
	/**
	 * 	Property: selectedPage 
	 */
	private Page30BO selectedPage;
	/**
	 * 	Property: listPage 
	 */
	private List listPage;
	/**
	 * 	Property: value 
	 */
	private Integer value;
/**
	 * Default constructor : UC30_Page1Form
	 */
	public UC30_Page1Form() {
		super();
		// Initialize : selectedPage
		this.selectedPage = null;
		// Initialize : listPage
		this.listPage = new java.util.ArrayList();
		// Initialize : value
		this.value = null;
			this.setAlwaysCallPreControllers(true);
	}
	
	/**
	 * 	Getter : selectedPage 
	 * 	@return : Return the selectedPage instance.
	 */
	public Page30BO getSelectedPage(){
		return selectedPage; // For UC30_Page1Form
	}
	
	/**
	 * 	Setter : selectedPage 
	 *  @param selectedPageinstance : The instance to set.
	 */
	public void setSelectedPage(final Page30BO selectedPageinstance){
		this.selectedPage = selectedPageinstance;// For UC30_Page1Form
	}
	/**
	 * 	Getter : listPage 
	 * 	@return : Return the listPage instance.
	 */
	public List getListPage(){
		return listPage; // For UC30_Page1Form
	}
	
	/**
	 * 	Setter : listPage 
	 *  @param listPageinstance : The instance to set.
	 */
	public void setListPage(final List listPageinstance){
		this.listPage = listPageinstance;// For UC30_Page1Form
	}
	/**
	 * 	Getter : value 
	 * 	@return : Return the value instance.
	 */
	public Integer getValue(){
		return value; // For UC30_Page1Form
	}
	
	/**
	 * 	Setter : value 
	 *  @param valueinstance : The instance to set.
	 */
	public void setValue(final Integer valueinstance){
		this.value = valueinstance;// For UC30_Page1Form
	}
/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UC30_Page1Form [ "+
SELECTED_PAGE +" = " + selectedPage +LIST_PAGE +" = " + listPage +VALUE +" = " + value + "]";
	}
	
	/**
	 * Method setInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public void setInstanceByInstanceName(final String instanceName, final Object instance) {
		// Set the instance SelectedPage.
		if(SELECTED_PAGE.equals(instanceName)){// For UC30_Page1Form
			this.setSelectedPage((Page30BO)instance); // For UC30_Page1Form
		}
				// Set the instance ListPage.
		if(LIST_PAGE.equals(instanceName)){// For UC30_Page1Form
			this.setListPage((List)instance); // For UC30_Page1Form
		}
				// Set the instance Value.
		if(VALUE.equals(instanceName)){// For UC30_Page1Form
			this.setValue((Integer)instance); // For UC30_Page1Form
		}
			}
	
	/**
	 * Method getInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public Object getInstanceByInstanceName(final String instanceName){
		// Temporary object for tmpUC30_Page1Form.
		Object tmpUC30_Page1Form = null;
		
			
		// Get the instance SelectedPage for UC30_Page1Form.
		if(SELECTED_PAGE.equals(instanceName)){ // For UC30_Page1Form
			tmpUC30_Page1Form = this.getSelectedPage(); // For UC30_Page1Form
		}
			
		// Get the instance ListPage for UC30_Page1Form.
		if(LIST_PAGE.equals(instanceName)){ // For UC30_Page1Form
			tmpUC30_Page1Form = this.getListPage(); // For UC30_Page1Form
		}
			
		// Get the instance Value for UC30_Page1Form.
		if(VALUE.equals(instanceName)){ // For UC30_Page1Form
			tmpUC30_Page1Form = this.getValue(); // For UC30_Page1Form
		}
		return tmpUC30_Page1Form;// For UC30_Page1Form
	}
	
	}
