/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.service.standardfeatures.entities.uc64_cascade;


import javax.inject.Inject;
import javax.inject.Named;

import com.bluage.documentation.business.standardfeatures.entities.uc64_cascade.bo.Team64BO;
import com.bluage.documentation.business.standardfeatures.entities.uc64_cascade.entities.dao.Team64DAOImpl;
import com.bluage.documentation.business.standardfeatures.entities.uc64_cascade.entities.daofinder.Team64DAOFinderImpl;
import com.bluage.documentation.service.AbstractService;
import com.netfective.bluage.core.exception.ApplicationException;

/**
 * Defines  application services for ServiceTeam64
  */
@Named("com.bluage.documentation.service.standardfeatures.entities.uc64_cascade.ServiceTeam64")
public class ServiceTeam64  extends AbstractService{


			@Inject
		@Named ("com.bluage.documentation.business.standardfeatures.entities.uc64_cascade.entities.dao.Team64DAOImpl")
		private Team64DAOImpl team64BODao;
			@Inject
		@Named ("com.bluage.documentation.business.standardfeatures.entities.uc64_cascade.entities.daofinder.Team64DAOFinderImpl")
		private Team64DAOFinderImpl team64BOFinder;
	


	/**
	 *  Default constructor 
	 */
	public ServiceTeam64() {
		super(); // Call super for default constructor : ServiceTeam64
	}
	


			
				
	/**    
	 * Operation Crud
	 * @param bo {@link Team64BO}
		 * @return {@link Team64BO}
	 */
	public Team64BO teamFindByID(Team64BO bo )
 throws ApplicationException {

					return (Team64BO)team64BOFinder.findByPrimaryKey(getBoId(bo));
	}
	/**
	 *  Return the primary key from business object.
	 */
	private Object getBoId(Team64BO bo) {
				return bo.getId();
		}

					
				
	/**    
	 * Operation Crud
	 * @param bo {@link Team64BO}
		 * @return {@link boolean}
	 */
	public boolean teamDelete(Team64BO bo )
 throws ApplicationException {

					return team64BODao.delete(bo);
	}

					
				
	/**    
	 * Operation Crud
	 * @param bo {@link Team64BO}
		 * @return {@link boolean}
	 */
	public boolean teamUpdate(Team64BO bo )
 throws ApplicationException {

			team64BODao.setRefreshSession(true);
				return team64BODao.update(bo);
	}

		}
