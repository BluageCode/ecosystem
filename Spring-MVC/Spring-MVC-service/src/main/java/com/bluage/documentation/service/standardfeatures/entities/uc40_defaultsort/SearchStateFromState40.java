/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.service.standardfeatures.entities.uc40_defaultsort;


import javax.inject.Inject;
import javax.inject.Named;

import com.bluage.documentation.business.standardfeatures.entities.uc40_defaultsort.bos.State40BO;
import com.bluage.documentation.service.AbstractService;
import com.netfective.bluage.core.exception.ApplicationException;

/**
 * Defines  application services for SearchStateFromState40
  */
@Named("com.bluage.documentation.service.standardfeatures.entities.uc40_defaultsort.SearchStateFromState40")
public class SearchStateFromState40  extends AbstractService{


					@Inject
		@Named ("com.bluage.documentation.service.standardfeatures.entities.uc40_defaultsort.ServiceState40")
		private ServiceState40  instanceServiceState40;
						


	/**
	 *  Default constructor 
	 */
	public SearchStateFromState40() {
		super(); // Call super for default constructor : SearchStateFromState40
	}
	



			
	/**    
	 * Process operation.
	 * @param currentState40 {@link State40BO}
		 * @return {@link void}
	 */
	public void searchStateFromState40(State40BO currentState40 )
 throws ApplicationException {

			privateSearchStateFromState40(currentState40);
	}
	

	/**    
	 * Process operation.
	 * @param currentState40 {@link State40BO}
		 * @return {@link void}
	 */
	private void privateSearchStateFromState40(State40BO currentState40 )
 throws ApplicationException {
		// Central buffer node declaration for operation searchStateFromState40
			State40BO	 state40Found =  new State40BO();
					String	 code =  "";
						// Calling an operation : 
code = currentState40.getCode();
// Test on exception : 
if (code=="") {
ApplicationException app = new ApplicationException("Entry code is empty.");
throw app;
}
// Calling an operation : 
state40Found = instanceServiceState40.searchState40ByCriteria(code);
// Test on exception : 
if (state40Found!=null) {
ApplicationException app = new ApplicationException("Can't create duplicate entry.");
throw app;
}

		}

		}
