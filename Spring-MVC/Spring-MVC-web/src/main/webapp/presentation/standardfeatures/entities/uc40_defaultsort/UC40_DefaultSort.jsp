<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file="/include.jsp"  %>
<html >
<head >
<title  > 
<fmt:message key="cUC40_DefaultSort_BLU_AGE_DOCUMENTATION"/>
</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"  > 
</meta>
<link href="<c:url value= "/presentation/_style/_style.css"/>" rel="stylesheet" type="text/css"  /> 
<link href="<c:url value= "/presentation/_include/jquery/rd_lib.css"/>" rel="stylesheet" type="text/css"  /> 
<script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/jquery.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/rd_setup.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/rd_lib.js"/>" space="preserve"  ></script><link rel="shortcut icon" href="<c:url value= "/presentation/_style/favicon.ico"/>"  /> 

</head>

<body class=" " > 
<div id="rd_ghost"  > 
<small  > 
<fmt:message key="cUC40_DefaultSort___CSS_DEACTIVATED"/>
</small>
</div>
<div id="rd_jghost"  > 
<small  > 
<fmt:message key="cUC40_DefaultSort___JAVASCRIPT_DEACTIVATED"/>
</small>
</div>
<div id="shape"  > 
<div class="ui-layout-center"  > 
<div class="east-north"  > 
	<form:form id="uC40_DefaultSort" commandName="uC40_DefaultSortForm"  enctype="application/x-www-form-urlencoded" method="post">
<h2 class="rd_first"  > 
<fmt:message key="cUC40_DefaultSort_STANDARD_FEATURES__ENTITIES_"/>
</h2>
<table class="rd_tableform"  >
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC40_DefaultSort_CODE"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
<form:input id="txt_code" path="currentState40.code"  
		/>
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC40_DefaultSort_NAME"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
<form:input id="txt_name" path="currentState40.name"  
		/>
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<fmt:message key="cUC40_DefaultSort_"/>
</td> 
<td rowspan="1" colspan="1"  >
<a id="lnk_sort"onclick="javascript:btn_click_cExecuteUC40_DefaultSort(this.href);return false;"				href="<c:url value="/presentation/standardfeatures/entities/uc40_defaultsort/UC40_DefaultSort/lnk_sort.html"/>"
							 shape="rect"  >									 
<fmt:message key="cUC40_DefaultSort_CREATE_STATE"/>
</a>
</td> 
</tr>
</table> 
<display:table uid="table_states"  sort="list" requestURI="" name="sessionScope.uC40_DefaultSortForm.stateInstance" class="rd_tabledata"  >
	<c:set   var="rownum">
		<%=(table_states !=	null) ? new Integer(table_states_rowNum.intValue() - 1) : new Integer(0) %>
	</c:set>
	<c:set   var="idRow">
		<%=(table_states != null ) ? table_states_rowNum.intValue() - 1 : -1 %>
	</c:set>
	<c:set   var="idTab">
		<%=(table_states != null ) ? "table_states" : null %>
	</c:set>
	<c:set var="tableHashId">
		<%= new org.displaytag.util.ParamEncoder("table_states").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE) %>
	</c:set>
	<c:set var="tableHashId2">
		<%=request.getParameter(new org.displaytag.util.ParamEncoder("table_states").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE)) %>
	</c:set>	
	
<display:column format=""  titleKey="cUC40_DefaultSort_CODE_130">
		<span id="txt_code1">
	<c:out value="${table_states.code}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="cUC40_DefaultSort_NAME_131">
		<span id="txt_name1">
	<c:out value="${table_states.name}" />
		</span>
	</display:column>
</display:table>
	</form:form>
	<script language="javascript">
		function btn_click_cExecuteUC40_DefaultSort(url) {
			UC40_DefaultSortvar = document.getElementById('uC40_DefaultSort');
			UC40_DefaultSortvar.setAttribute('action', url);
			UC40_DefaultSortvar.submit();
		}
		function btn_click_cSetActionUC40_DefaultSort(url) {
			UC40_DefaultSortvar = document.getElementById('uC40_DefaultSort');
			UC40_DefaultSortvar.setAttribute('action', url);
		}
	</script>
</div>
<div class="east-center"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/entities/uc40_defaultsort/UC40_DefaultSortcmp_tabs" />
</div>
</div>
<div class="ui-layout-north"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/entities/uc40_defaultsort/UC40_DefaultSortcmp_header" />
</div>
<div class="ui-layout-south"  > 
<fmt:message key="cUC40_DefaultSort___"/>
<span id="rd_jversion"  > 
<fmt:message key="cUC40_DefaultSort_BLU_AGE_CORPORATION"/>
</span>
</div>
<div class="ui-layout-west"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/entities/uc40_defaultsort/UC40_DefaultSortcmp_treeview" />
</div>
</div>
</body>
</html>
