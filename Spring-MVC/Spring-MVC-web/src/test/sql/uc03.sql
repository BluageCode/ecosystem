--<ScriptOptions statementTerminator=";"/>

ALTER TABLE player03 DROP CONSTRAINT player03_position_fkc;

ALTER TABLE player03 DROP CONSTRAINT player03_pkey;

ALTER TABLE position03 DROP CONSTRAINT position03_pkey;

DROP TABLE position03;

DROP TABLE player03;

CREATE TABLE position03 (
		code VARCHAR(2) NOT NULL,
		version INT4 NOT NULL,
		name VARCHAR(20) NOT NULL
	);

CREATE TABLE player03 (
		id INT8 NOT NULL,
		version INT4 NOT NULL,
		first_name VARCHAR(30) NOT NULL,
		last_name VARCHAR(30) NOT NULL,
		date_of_birth DATE,
		estimated_value FLOAT4,
		weight INT4,
		height INT4,
		rookie BOOL,
		position_fk VARCHAR(2) NOT NULL
	);


INSERT INTO position03 (code, version, name) VALUES ('PG', 0, 'Point Guard');
INSERT INTO position03 (code, version, name) VALUES ('SG', 0, 'Shooting Guard');
INSERT INTO position03 (code, version, name) VALUES ('SF', 0, 'Small Forward');
INSERT INTO position03 (code, version, name) VALUES ('PF', 0, 'Power Forward');
INSERT INTO position03 (code, version, name) VALUES ('C', 0, 'Center');


INSERT INTO player03 VALUES (284, 0, 'Roger', 'Martin', '1984-10-02', 12, 100, 200, false, 'PG');
INSERT INTO player03 VALUES (298, 0, 'Roger', 'Martin', '1984-10-02', 12, 100, 200, false, 'PG');
INSERT INTO player03 VALUES (317, 0, 'aaaaaaaaa', 'e', NULL, NULL, NULL, NULL, false, 'PG');
INSERT INTO player03 VALUES (321, 0, 'Julian', 'Sabos', NULL, NULL, NULL, NULL, false, 'PG');
INSERT INTO player03 VALUES (326, 0, 'Alphonse', 'Brown', '2011-03-09', 12, NULL, NULL, false, 'PG');
INSERT INTO player03 VALUES (329, 0, 'Toto', 'Tito', '2011-07-11', NULL, NULL, NULL, true, 'SF');

ALTER TABLE player03 ADD CONSTRAINT player03_pkey PRIMARY KEY (id);

ALTER TABLE position03 ADD CONSTRAINT position03_pkey PRIMARY KEY (code);

ALTER TABLE player03 ADD CONSTRAINT player03_position_fkc FOREIGN KEY (position_fk)
	REFERENCES position03 (code);

	