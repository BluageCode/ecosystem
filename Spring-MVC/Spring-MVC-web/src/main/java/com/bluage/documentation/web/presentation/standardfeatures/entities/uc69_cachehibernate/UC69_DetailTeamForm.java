/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.entities.uc69_cachehibernate;


import java.io.Serializable;
import java.util.List;

import com.bluage.documentation.business.standardfeatures.entities.uc69_cachehibernate.bos.Player69BO;
import com.bluage.documentation.business.standardfeatures.entities.uc69_cachehibernate.bos.Team69BO;
import com.netfective.bluage.core.web.AbstractForm;
import com.netfective.bluage.core.web.IForm;

/**
* Class : UC69_DetailTeamForm
*/
public class UC69_DetailTeamForm extends AbstractForm implements Serializable, IForm {
	
	/** Constant : serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	//CONSTANT : allTeams
	private static final String ALL_TEAMS = "allTeams";
	//CONSTANT : players
	private static final String PLAYERS = "players";
	//CONSTANT : team
	private static final String TEAM = "team";
	/**
	 * 	Property: allTeams 
	 */
	private List allTeams;
	/**
	 * 	Property: players 
	 */
	private List<Player69BO> players;
	/**
	 * 	Property: team 
	 */
	private Team69BO team;
/**
	 * Default constructor : UC69_DetailTeamForm
	 */
	public UC69_DetailTeamForm() {
		super();
		// Initialize : allTeams
		this.allTeams = null;
		// Initialize : players
		this.players = new java.util.ArrayList<com.bluage.documentation.business.standardfeatures.entities.uc69_cachehibernate.bos.Player69BO>();
		// Initialize : team
		this.team = new Team69BO();
					this.setAlwaysCallPreControllers(true);
	}
	
	/**
	 * 	Getter : allTeams 
	 * 	@return : Return the allTeams instance.
	 */
	public List getAllTeams(){
		return allTeams; // For UC69_DetailTeamForm
	}
	
	/**
	 * 	Setter : allTeams 
	 *  @param allTeamsinstance : The instance to set.
	 */
	public void setAllTeams(final List allTeamsinstance){
		this.allTeams = allTeamsinstance;// For UC69_DetailTeamForm
	}
	/**
	 * 	Getter : players 
	 * 	@return : Return the players instance.
	 */
	public List<Player69BO> getPlayers(){
		return players; // For UC69_DetailTeamForm
	}
	
	/**
	 * 	Setter : players 
	 *  @param playersinstance : The instance to set.
	 */
	public void setPlayers(final List<Player69BO> playersinstance){
		this.players = playersinstance;// For UC69_DetailTeamForm
	}
	/**
	 * 	Getter : team 
	 * 	@return : Return the team instance.
	 */
	public Team69BO getTeam(){
		return team; // For UC69_DetailTeamForm
	}
	
	/**
	 * 	Setter : team 
	 *  @param teaminstance : The instance to set.
	 */
	public void setTeam(final Team69BO teaminstance){
		this.team = teaminstance;// For UC69_DetailTeamForm
	}
/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UC69_DetailTeamForm [ "+
ALL_TEAMS +" = " + allTeams +PLAYERS +" = " + players +TEAM +" = " + team + "]";
	}
	
	/**
	 * Method setInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public void setInstanceByInstanceName(final String instanceName, final Object instance) {
		// Set the instance AllTeams.
		if(ALL_TEAMS.equals(instanceName)){// For UC69_DetailTeamForm
			this.setAllTeams((List)instance); // For UC69_DetailTeamForm
		}
				// Set the instance Players.
		if(PLAYERS.equals(instanceName)){// For UC69_DetailTeamForm
			this.setPlayers((List<Player69BO>)instance); // For UC69_DetailTeamForm
		}
				// Set the instance Team.
		if(TEAM.equals(instanceName)){// For UC69_DetailTeamForm
			this.setTeam((Team69BO)instance); // For UC69_DetailTeamForm
		}
				// Parent instance name for UC69_DetailTeamForm
		if(TEAM.equals(instanceName) && team != null){ // For UC69_DetailTeamForm
			players.clear(); // For UC69_DetailTeamForm
			players.addAll(team.getPlayers());// For UC69_DetailTeamForm
		}
		
	}
	
	/**
	 * Method getInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public Object getInstanceByInstanceName(final String instanceName){
		// Temporary object for tmpUC69_DetailTeamForm.
		Object tmpUC69_DetailTeamForm = null;
		
			
		// Get the instance AllTeams for UC69_DetailTeamForm.
		if(ALL_TEAMS.equals(instanceName)){ // For UC69_DetailTeamForm
			tmpUC69_DetailTeamForm = this.getAllTeams(); // For UC69_DetailTeamForm
		}
			
		// Get the instance Players for UC69_DetailTeamForm.
		if(PLAYERS.equals(instanceName)){ // For UC69_DetailTeamForm
			tmpUC69_DetailTeamForm = this.getPlayers(); // For UC69_DetailTeamForm
		}
			
		// Get the instance Team for UC69_DetailTeamForm.
		if(TEAM.equals(instanceName)){ // For UC69_DetailTeamForm
			tmpUC69_DetailTeamForm = this.getTeam(); // For UC69_DetailTeamForm
		}
		return tmpUC69_DetailTeamForm;// For UC69_DetailTeamForm
	}
	
			}
