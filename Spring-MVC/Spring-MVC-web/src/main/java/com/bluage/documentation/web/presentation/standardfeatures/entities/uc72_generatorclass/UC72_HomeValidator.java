/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.entities.uc72_generatorclass ;


import org.apache.log4j.Logger;
import org.springframework.validation.Errors;

import com.netfective.bluage.core.validator.AbstractValidator;
/**
* Class :UC72_HomeValidator
*/
public class UC72_HomeValidator extends AbstractValidator{
	
	// LOGGER for the class UC72_HomeValidator
	private static final Logger LOGGER = Logger.getLogger( UC72_HomeValidator.class);
	
	
	/**
	* Operation validate for UC72_HomeForm
	* @param obj : the current form (UC72_HomeForm)
	* @param errors : The spring errors to return for the form UC72_HomeForm
	*/
	public void validate(final Object obj,final Errors errors){
		LOGGER.info("Beginning method : validate"); // UC72_HomeValidator
		UC72_HomeForm cUC72_HomeForm = (UC72_HomeForm)obj; // UC72_HomeValidator
		LOGGER.info("Ending method : validate the form "+ cUC72_HomeForm.getClass().getName()); // UC72_HomeValidator
	}

	/**
	* Method to implements to use spring validators (UC72_HomeForm)
	* @param aClass : Class for the form UC72_HomeForm
	* @return boolean
	*/
	public boolean supports(final Class aClass){
		return (new UC72_HomeForm()).getClass().equals(aClass); // UC72_HomeValidator
	}
}
