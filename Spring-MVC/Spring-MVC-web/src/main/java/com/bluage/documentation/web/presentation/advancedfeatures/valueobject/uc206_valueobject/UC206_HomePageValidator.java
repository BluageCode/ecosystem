/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.advancedfeatures.valueobject.uc206_valueobject ;


import org.apache.log4j.Logger;
import org.springframework.validation.Errors;

import com.netfective.bluage.core.validator.AbstractValidator;
/**
* Class :UC206_HomePageValidator
*/
public class UC206_HomePageValidator extends AbstractValidator{
	
	// LOGGER for the class UC206_HomePageValidator
	private static final Logger LOGGER = Logger.getLogger( UC206_HomePageValidator.class);
	
	
	/**
	* Operation validate for UC206_HomePageForm
	* @param obj : the current form (UC206_HomePageForm)
	* @param errors : The spring errors to return for the form UC206_HomePageForm
	*/
	public void validate(final Object obj,final Errors errors){
		LOGGER.info("Beginning method : validate"); // UC206_HomePageValidator
		UC206_HomePageForm cUC206_HomePageForm = (UC206_HomePageForm)obj; // UC206_HomePageValidator
		LOGGER.info("Ending method : validate the form "+ cUC206_HomePageForm.getClass().getName()); // UC206_HomePageValidator
	}

	/**
	* Method to implements to use spring validators (UC206_HomePageForm)
	* @param aClass : Class for the form UC206_HomePageForm
	* @return boolean
	*/
	public boolean supports(final Class aClass){
		return (new UC206_HomePageForm()).getClass().equals(aClass); // UC206_HomePageValidator
	}
}
