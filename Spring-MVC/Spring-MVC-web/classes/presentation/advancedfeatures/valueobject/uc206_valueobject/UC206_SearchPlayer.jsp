<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file="/include.jsp"  %>
<html >
<head >
<title  > 
<fmt:message key="cUC206_SearchPlayer_BLU_AGE_DOCUMENTATION"/>
</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"  > 
</meta>
<link href="<c:url value= "/presentation/_style/_style.css"/>" rel="stylesheet" type="text/css"  /> 
<link href="<c:url value= "/presentation/_include/jquery/rd_lib.css"/>" rel="stylesheet" type="text/css"  /> 
<script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/jquery.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/rd_setup.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/rd_lib.js"/>" space="preserve"  ></script><link rel="shortcut icon" href="<c:url value= "/presentation/_style/favicon.ico"/>"  /> 

</head>

<body class=" " > 
<div id="rd_ghost"  > 
<small  > 
<fmt:message key="cUC206_SearchPlayer___CSS_DEACTIVATED"/>
</small>
</div>
<div id="rd_jghost"  > 
<small  > 
<fmt:message key="cUC206_SearchPlayer___JAVASCRIPT_DEACTIVATED"/>
</small>
</div>
<div id="shape"  > 
<div class="ui-layout-center"  > 
<div class="east-north"  > 
	<form:form id="uC206_SearchPlayer" commandName="uC206_SearchPlayerForm"  enctype="application/x-www-form-urlencoded" method="post">
<h2 class="rd_first"  > 
<fmt:message key="cUC206_SearchPlayer_ADVANCED_FEATURES__VALUE_OBJE"/>
</h2>
<table class="rd_tableform"  >
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC206_SearchPlayer_FIRST_NAME_"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
<form:input id="txt_city" path="screen206.player206VO.firstName"  
		/>
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC206_SearchPlayer_LAST_NAME_"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
<form:input id="txt_name" path="screen206.player206VO.lastName"  
		/>
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC206_SearchPlayer_POSITION_"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
<form:select id="select_position" path="screen206.player206VO.positionVO.code"   
		>
		<form:options items="${uC206_SearchPlayerForm.listPosition206}" itemValue="code" itemLabel="name" />
</form:select>
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC206_SearchPlayer_ROOKIE_"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
<form:checkbox id="chk_rookie" path="screen206.player206VO.rookie"   
		/>
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<fmt:message key="cUC206_SearchPlayer_"/>
</td> 
<td rowspan="1" colspan="1"  >
<a id="lnk_searchProperties"onclick="javascript:btn_click_cExecuteUC206_SearchPlayer(this.href);return false;"				href="<c:url value="/presentation/advancedfeatures/valueobject/uc206_valueobject/UC206_SearchPlayer/lnk_searchProperties.html"/>"
							 shape="rect"  >									 
<fmt:message key="cUC206_SearchPlayer_SEARCH_WITH_ABOVE_PROPERTIES"/>
</a>
</td> 
</tr>
<tr  > 
<td colspan="2" rowspan="1"  >
<fmt:message key="cUC206_SearchPlayer_OR"/>
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC206_SearchPlayer_LAST_NAME_BEGINS_WITH"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
<form:input id="txt_namebeginswith" path="screen206.player206CriteriaVO.lastName"  
		/>
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<fmt:message key="cUC206_SearchPlayer__18"/>
</td> 
<td rowspan="1" colspan="1"  >
<a id="lnk_searchCriteria"onclick="javascript:btn_click_cExecuteUC206_SearchPlayer(this.href);return false;"				href="<c:url value="/presentation/advancedfeatures/valueobject/uc206_valueobject/UC206_SearchPlayer/lnk_searchCriteria.html"/>"
							 shape="rect"  >									 
<fmt:message key="cUC206_SearchPlayer_SEARCH_WITH_AN_HQL_OPERATION"/>
</a>
</td> 
</tr>
</table> 
<display:table uid="tab_searchResult" pagesize="10"  sort="list" requestURI="" name="sessionScope.uC206_SearchPlayerForm.playerVOs" class="rd_tabledata"  >
	<c:set   var="rownum">
		<%=(tab_searchResult !=	null) ? new Integer(tab_searchResult_rowNum.intValue() - 1) : new Integer(0) %>
	</c:set>
	<c:set   var="idRow">
		<%=(tab_searchResult != null ) ? tab_searchResult_rowNum.intValue() - 1 : -1 %>
	</c:set>
	<c:set   var="idTab">
		<%=(tab_searchResult != null ) ? "tab_searchResult" : null %>
	</c:set>
	<c:set var="tableHashId">
		<%= new org.displaytag.util.ParamEncoder("tab_searchResult").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE) %>
	</c:set>
	<c:set var="tableHashId2">
		<%=request.getParameter(new org.displaytag.util.ParamEncoder("tab_searchResult").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE)) %>
	</c:set>	
	
<display:column format=""  titleKey="cUC206_SearchPlayer_LAST_NAME">
		<span id="txt_lastname1">
	<c:out value="${tab_searchResult.lastName}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="cUC206_SearchPlayer_FIRST_NAME">
		<span id="txt_firstname1">
	<c:out value="${tab_searchResult.firstName}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="cUC206_SearchPlayer_POSITION">
		<span id="txt_position">
	<c:out value="${tab_searchResult.positionVO.name}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="cUC206_SearchPlayer_HEIGHT_CM">
		<span id="txt_height">
	<c:out value="${tab_searchResult.height}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="cUC206_SearchPlayer_ROOKIE">
<form:checkbox id="chk_isRookie[${rownum}]" path="playerVOs[${rownum}].rookie"  name="isRookie" disabled="true"  
		/>
</display:column>
</display:table>
<br clear="none"  /> 
<br clear="none"  /> 
<a id="lnk_homePage"				href="<c:url value="/presentation/advancedfeatures/valueobject/uc206_valueobject/UC206_HomePage.html"/>"
													 shape="rect"  >									 
<fmt:message key="cUC206_SearchPlayer_BACK"/>
</a>
	</form:form>
	<script language="javascript">
		function btn_click_cExecuteUC206_SearchPlayer(url) {
			UC206_SearchPlayervar = document.getElementById('uC206_SearchPlayer');
			UC206_SearchPlayervar.setAttribute('action', url);
			UC206_SearchPlayervar.submit();
		}
		function btn_click_cSetActionUC206_SearchPlayer(url) {
			UC206_SearchPlayervar = document.getElementById('uC206_SearchPlayer');
			UC206_SearchPlayervar.setAttribute('action', url);
		}
	</script>
</div>
<div class="east-center"  > 
<tiles:insertDefinition	name="presentation/advancedfeatures/valueobject/uc206_valueobject/UC206_SearchPlayercmp_tabs" />
</div>
</div>
<div class="ui-layout-north"  > 
<tiles:insertDefinition	name="presentation/advancedfeatures/valueobject/uc206_valueobject/UC206_SearchPlayercmp_header" />
</div>
<div class="ui-layout-south"  > 
<fmt:message key="cUC206_SearchPlayer_______"/>
<span id="rd_jversion"  > 
<fmt:message key="cUC206_SearchPlayer_BLU_AGE_DOCUMENTATION_19"/>
</span>
</div>
<div class="ui-layout-west"  > 
<tiles:insertDefinition	name="presentation/advancedfeatures/valueobject/uc206_valueobject/UC206_SearchPlayercmp_treeview" />
</div>
</div>
</body>
</html>
