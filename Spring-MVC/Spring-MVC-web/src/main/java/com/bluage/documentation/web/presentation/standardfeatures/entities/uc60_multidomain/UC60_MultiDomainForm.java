/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.entities.uc60_multidomain;


import java.io.Serializable;
import java.util.List;

import com.bluage.documentation.business.standardfeatures.entities.uc60_multidomain.teams.bos.Team60BO;
import com.netfective.bluage.core.web.AbstractForm;
import com.netfective.bluage.core.web.IForm;

/**
* Class : UC60_MultiDomainForm
*/
public class UC60_MultiDomainForm extends AbstractForm implements Serializable, IForm {
	
	/** Constant : serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	//CONSTANT : allTeams
	private static final String ALL_TEAMS = "allTeams";
	//CONSTANT : selectedTeam
	private static final String SELECTED_TEAM = "selectedTeam";
	//CONSTANT : viewPlayers
	private static final String VIEW_PLAYERS = "viewPlayers";
	/**
	 * 	Property: allTeams 
	 */
	private List<Team60BO> allTeams;
	/**
	 * 	Property: selectedTeam 
	 */
	private Team60BO selectedTeam;
	/**
	 * 	Property: viewPlayers 
	 */
	private Team60BO viewPlayers;
/**
	 * Default constructor : UC60_MultiDomainForm
	 */
	public UC60_MultiDomainForm() {
		super();
		// Initialize : allTeams
		this.allTeams = new java.util.ArrayList<com.bluage.documentation.business.standardfeatures.entities.uc60_multidomain.teams.bos.Team60BO>();
		// Initialize : selectedTeam
		this.selectedTeam = null;
		// Initialize : viewPlayers
		this.viewPlayers = null;
					this.setAlwaysCallPreControllers(true);
	}
	
	/**
	 * 	Getter : allTeams 
	 * 	@return : Return the allTeams instance.
	 */
	public List<Team60BO> getAllTeams(){
		return allTeams; // For UC60_MultiDomainForm
	}
	
	/**
	 * 	Setter : allTeams 
	 *  @param allTeamsinstance : The instance to set.
	 */
	public void setAllTeams(final List<Team60BO> allTeamsinstance){
		this.allTeams = allTeamsinstance;// For UC60_MultiDomainForm
	}
	/**
	 * 	Getter : selectedTeam 
	 * 	@return : Return the selectedTeam instance.
	 */
	public Team60BO getSelectedTeam(){
		return selectedTeam; // For UC60_MultiDomainForm
	}
	
	/**
	 * 	Setter : selectedTeam 
	 *  @param selectedTeaminstance : The instance to set.
	 */
	public void setSelectedTeam(final Team60BO selectedTeaminstance){
		this.selectedTeam = selectedTeaminstance;// For UC60_MultiDomainForm
	}
	/**
	 * 	Getter : viewPlayers 
	 * 	@return : Return the viewPlayers instance.
	 */
	public Team60BO getViewPlayers(){
		return viewPlayers; // For UC60_MultiDomainForm
	}
	
	/**
	 * 	Setter : viewPlayers 
	 *  @param viewPlayersinstance : The instance to set.
	 */
	public void setViewPlayers(final Team60BO viewPlayersinstance){
		this.viewPlayers = viewPlayersinstance;// For UC60_MultiDomainForm
	}
/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UC60_MultiDomainForm [ "+
ALL_TEAMS +" = " + allTeams +SELECTED_TEAM +" = " + selectedTeam +VIEW_PLAYERS +" = " + viewPlayers + "]";
	}
	
	/**
	 * Method setInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public void setInstanceByInstanceName(final String instanceName, final Object instance) {
		// Set the instance AllTeams.
		if(ALL_TEAMS.equals(instanceName)){// For UC60_MultiDomainForm
			this.setAllTeams((List<Team60BO>)instance); // For UC60_MultiDomainForm
		}
				// Set the instance SelectedTeam.
		if(SELECTED_TEAM.equals(instanceName)){// For UC60_MultiDomainForm
			this.setSelectedTeam((Team60BO)instance); // For UC60_MultiDomainForm
		}
				// Set the instance ViewPlayers.
		if(VIEW_PLAYERS.equals(instanceName)){// For UC60_MultiDomainForm
			this.setViewPlayers((Team60BO)instance); // For UC60_MultiDomainForm
		}
			}
	
	/**
	 * Method getInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public Object getInstanceByInstanceName(final String instanceName){
		// Temporary object for tmpUC60_MultiDomainForm.
		Object tmpUC60_MultiDomainForm = null;
		
			
		// Get the instance AllTeams for UC60_MultiDomainForm.
		if(ALL_TEAMS.equals(instanceName)){ // For UC60_MultiDomainForm
			tmpUC60_MultiDomainForm = this.getAllTeams(); // For UC60_MultiDomainForm
		}
			
		// Get the instance SelectedTeam for UC60_MultiDomainForm.
		if(SELECTED_TEAM.equals(instanceName)){ // For UC60_MultiDomainForm
			tmpUC60_MultiDomainForm = this.getSelectedTeam(); // For UC60_MultiDomainForm
		}
			
		// Get the instance ViewPlayers for UC60_MultiDomainForm.
		if(VIEW_PLAYERS.equals(instanceName)){ // For UC60_MultiDomainForm
			tmpUC60_MultiDomainForm = this.getViewPlayers(); // For UC60_MultiDomainForm
		}
		return tmpUC60_MultiDomainForm;// For UC60_MultiDomainForm
	}
	
			}
