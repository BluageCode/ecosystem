/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.service.standardfeatures.application.navigation.uc29_globalscope;


import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import com.bluage.documentation.business.standardfeatures.application.navigation.uc29_globalscope.entities.daofinder.Stadium29DAOFinderImpl;
import com.bluage.documentation.service.AbstractService;
import com.netfective.bluage.core.exception.ApplicationException;

/**
 * Defines  application services for ServiceStadium29
  */
@Named("com.bluage.documentation.service.standardfeatures.application.navigation.uc29_globalscope.ServiceStadium29")
public class ServiceStadium29  extends AbstractService{


			@Inject
		@Named ("com.bluage.documentation.business.standardfeatures.application.navigation.uc29_globalscope.entities.daofinder.Stadium29DAOFinderImpl")
		private Stadium29DAOFinderImpl stadium29BOFinder;



	/**
	 *  Default constructor 
	 */
	public ServiceStadium29() {
		super(); // Call super for default constructor : ServiceStadium29
	}
	



								
	/**    
	 * Operation Crud
	 * @return {@link List}
	 */
	public List stadium29FindAll( )
 throws ApplicationException {

					return stadium29BOFinder.findAll();
	}

		}
