/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.service.standardfeatures.entities.uc64_cascade.daofinder;


import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateTemplate;

import com.bluage.documentation.business.standardfeatures.entities.uc64_cascade.bo.State64BO;
import com.netfective.bluage.core.exception.ApplicationException;


/**
 * This class represents query invocation methods implementation
 * for ServiceState64 service.
 *
 * @see com.bluage.documentation.service.standardfeatures.entities.uc64_cascade.ServiceState64
 */
 
@Named ("com.bluage.documentation.service.standardfeatures.entities.uc64_cascade.daofinder.ServiceState64DAOMethodFinderImpl")
public class ServiceState64DAOMethodFinderImpl {

	// LOGGER for ServiceState64DAOMethodFinderImpl
	private static final Logger LOGGER = Logger.getLogger(ServiceState64DAOMethodFinderImpl.class);
	
	/**
	* Hibernate Template (injected)
	*/
	@Inject
	private HibernateTemplate hibernateTemplate;

	
	/**
	 * setter for HibernateTemplate
	 * @param value : {@link HibernateTemplate}
	 */
	public void setHibernateTemplate( HibernateTemplate value )
	{
		hibernateTemplate = value;
	}
	

	/**    
	 * HQL operation.
	 * @param state64ToDelete {@link State64BO}
		 * @return {@link Long}
	 */
	public Long getNumbersOfTeams(State64BO state64ToDelete )
 throws ApplicationException {
Session session = hibernateTemplate.getSessionFactory().getCurrentSession();
			return 
				privateGetNumbersOfTeams(state64ToDelete, session);
	}

	
	/**    
	 * private delegation for HQL operation.
	 * @param state64ToDelete {@link State64BO}
		 * @return {@link Long}
	 */
	private Long privateGetNumbersOfTeams(State64BO state64ToDelete , Session session)
 throws ApplicationException {
			Query query = session.createQuery(" SELECT count(t) FROM Team64BO t WHERE t.state= :state64ToDelete ");

			query.setParameter( "state64ToDelete", state64ToDelete);
									Long returnValue = null;
		try {
			Object result = query.uniqueResult();
			if( result != null ) {
				returnValue = ( java.lang.Long )result;
			}				
		} catch(HibernateException ex) {
			LOGGER.error("An error occured while excecuting the HQL operation : com.bluage.documentation.service.standardfeatures.entities.uc64_cascade.getNumbersOfTeams");
			throw new ApplicationException(ex);
		}
		return returnValue;
	}

	




}
