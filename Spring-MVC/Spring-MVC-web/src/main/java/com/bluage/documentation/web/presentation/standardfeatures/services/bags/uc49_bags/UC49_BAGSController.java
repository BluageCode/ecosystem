/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.services.bags.uc49_bags ;

// Import declaration.
// Java imports.
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.bluage.documentation.business.standardfeatures.services.bags.uc49_bags.bos.Player49BO;
import com.bluage.documentation.business.standardfeatures.services.bags.uc49_bags.bos.Team49BO;
import com.bluage.documentation.business.standardfeatures.services.bags.uc49_bags.entities.daofinder.Player49DAOFinderImpl;
import com.bluage.documentation.service.standardfeatures.services.bags.uc49_bags.ServicePlayer49;
import com.netfective.bluage.core.exception.ApplicationException;
import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;
import com.netfective.bluage.core.web.IForm;

/**
 * Class : UC49_BAGSController
 */
@Controller
@Scope("prototype")
@SessionAttributes("uC49_BAGSForm")
@RequestMapping(value= "/presentation/standardfeatures/services/bags/uc49_bags")
public class UC49_BAGSController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC49_BAGSController.class);
	
	//Current form name
	private static final String U_C49__B_A_G_S_FORM = "uC49_BAGSForm";

	//CONSTANT: allPlayers table or repeater.
	private static final String ALL_PLAYERS = "allPlayers";
				//CONSTANT: teamToDisplay
	private static final String TEAM_TO_DISPLAY = "teamToDisplay";
	//CONSTANT: selectedPlayer
	private static final String SELECTED_PLAYER = "selectedPlayer";
	
	/**
	 * Property:customDateEditorsUC49_BAGSController
	 */
	final private Map<String,CustomDateEditor> customDateEditorsUC49_BAGSController = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:uC49_BAGSValidator
	 */
	final private UC49_BAGSValidator uC49_BAGSValidator = new UC49_BAGSValidator();
	
	/**
	 * Service declaration : servicePlayer49.
	 */
	@Autowired
	private ServicePlayer49 servicePlayer49;
	
	/**
	 * Generic Finder : player49DAOFinderImpl.
	 */
	@Autowired
	private Player49DAOFinderImpl player49DAOFinderImpl;
	
	
	// Initialise all the instances for UC49_BAGSController
		// Initialize the instance allPlayers for the state lnk_value
		private List allPlayers; // Initialize the instance allPlayers for UC49_BAGSController
						// Declare the instance teamToDisplay
		private Team49BO teamToDisplay;
		// Declare the instance selectedPlayer
		private Player49BO selectedPlayer;
			/**
	 * Operation : lnk_view
 	 * @param model : 
 	 * @param uC49_BAGS : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC49_BAGS/lnk_view.html",method = RequestMethod.POST)
	public String lnk_view(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC49_BAGSForm") UC49_BAGSForm  uC49_BAGSForm, final BindingResult bindingResult ,final @RequestParam(ID_REQUEST_PARAM) Integer index){
		
		
		
		LOGGER.info("Begin method : lnk_view"); 
		infoLogger(uC49_BAGSForm); 
		uC49_BAGSForm.setAlwaysCallPreControllers(false); 
		// initialization for lnk_view
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state lnk_view if the validation fail, you will be forward to the screen : "/presentation/standardfeatures/services/bags/uc49_bags/UC49_BAGS"
		if(errorsMessages(request,response,uC49_BAGSForm,bindingResult,"selectedPlayer", uC49_BAGSValidator, customDateEditorsUC49_BAGSController)){ 
			return "/presentation/standardfeatures/services/bags/uc49_bags/UC49_BAGS"; 
		}

										 callGetteronteam(request,uC49_BAGSForm , index);
											// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.services.bags.uc49_bags.UC49_ViewTeamForm or not from lnk_view
			final UC49_ViewTeamForm uC49_ViewTeamForm =  new UC49_ViewTeamForm();
			// Populate the destination form with all the instances returned from lnk_view.
			final IForm uC49_ViewTeamForm2 = populateDestinationForm(request.getSession(), uC49_ViewTeamForm); 
					
			// Add the form to the model.
			model.addAttribute("uC49_ViewTeamForm", uC49_ViewTeamForm2); 
			
			request.getSession().setAttribute("uC49_ViewTeamForm", uC49_ViewTeamForm2);
			
			// "OK" CASE => destination screen path from lnk_view
			LOGGER.info("Go to the screen 'UC49_ViewTeam'.");
			// Redirect (PRG) from lnk_view
			destinationPath =  "redirect:/presentation/standardfeatures/services/bags/uc49_bags/UC49_ViewTeam.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_view 
	}
	
	
	/**
	* This method initialise the form : UC49_BAGSForm 
	* @return UC49_BAGSForm
	*/
	@ModelAttribute("UC49_BAGSFormInit")
	public void initUC49_BAGSForm(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, U_C49__B_A_G_S_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method UC49_BAGSForm."); //for lnk_view 
		}
		UC49_BAGSForm uC49_BAGSForm;
	
		if(request.getSession().getAttribute(U_C49__B_A_G_S_FORM) != null){
			uC49_BAGSForm = (UC49_BAGSForm)request.getSession().getAttribute(U_C49__B_A_G_S_FORM);
		} else {
			uC49_BAGSForm = new UC49_BAGSForm();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form UC49_BAGSForm.");
		}
		uC49_BAGSForm = (UC49_BAGSForm)populateDestinationForm(request.getSession(), uC49_BAGSForm);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  UC49_BAGSForm.");
		}
		model.addAttribute(U_C49__B_A_G_S_FORM, uC49_BAGSForm);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param UC49_BAGSForm : The sceen form.
	 */
	@RequestMapping(value = "/UC49_BAGS.html" ,method = RequestMethod.GET)
	public void prepareUC49_BAGS(final HttpServletRequest request,final  Model model,final  @ModelAttribute("UC49_BAGSFormInit") UC49_BAGSForm uC49_BAGSForm){
		
		UC49_BAGSForm currentUC49_BAGSForm = uC49_BAGSForm;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(U_C49__B_A_G_S_FORM) == null){
			if(currentUC49_BAGSForm!=null){
				request.getSession().setAttribute(U_C49__B_A_G_S_FORM, currentUC49_BAGSForm);
			}else {
				currentUC49_BAGSForm = new UC49_BAGSForm();
				request.getSession().setAttribute(U_C49__B_A_G_S_FORM, currentUC49_BAGSForm);	
			}
		} else {
			currentUC49_BAGSForm = (UC49_BAGSForm) request.getSession().getAttribute(U_C49__B_A_G_S_FORM);
		}

		try {
			List allPlayers = player49DAOFinderImpl.findAll();
			put(request.getSession(), ALL_PLAYERS, allPlayers);
		} catch (ApplicationException e) {
			errorLogger("An error while calling the generic finder for the instance : allPlayers.",e);
		}
				currentUC49_BAGSForm = (UC49_BAGSForm)populateDestinationForm(request.getSession(), currentUC49_BAGSForm);
		// Call all the Precontroller.
	currentUC49_BAGSForm.setAlwaysCallPreControllers(true);
		model.addAttribute(U_C49__B_A_G_S_FORM, currentUC49_BAGSForm);
		request.getSession().setAttribute(U_C49__B_A_G_S_FORM, currentUC49_BAGSForm);
		
	}
	
										/**
	 * method callGetteronteam
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 * @param index Integer the current index 
	 */
	private void callGetteronteam(HttpServletRequest request,IForm form,Integer index) {
		teamToDisplay = (Team49BO)get(request.getSession(),form, "teamToDisplay");  
										 
					if(index!=null){  
			 selectedPlayer = (Player49BO)((List)get(request.getSession(),form, "allPlayers")).get(index);  
		} else {
			 selectedPlayer= null; 
		}
				 
					try {
				// executing Getteronteam in lnk_view
				teamToDisplay = 	servicePlayer49.returnTeam49(
			selectedPlayer
			);  
 
				put(request.getSession(), TEAM_TO_DISPLAY,teamToDisplay);
								// processing variables Getteronteam in lnk_view
				put(request.getSession(), SELECTED_PLAYER,selectedPlayer); 
			
			} catch (ApplicationException e) { 
				// error handling for operation returnTeam49 called Getteronteam
				errorLogger("An error occured during the execution of the operation : returnTeam49",e); 
		
			}
	}
		
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("UC49_BAGSController [ ");
			strBToS.append(ALL_PLAYERS);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(allPlayers);
			strBToS.append(END_TO_STRING_DELIMITER);
						strBToS.append(TEAM_TO_DISPLAY);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(teamToDisplay);
			strBToS.append(END_TO_STRING_DELIMITER);
			strBToS.append(SELECTED_PLAYER);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(selectedPlayer);
			strBToS.append(END_TO_STRING_DELIMITER);
		// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(uC49_BAGSValidator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsUC49_BAGSController!=null); 
		return strBToS.toString();
	}
}
