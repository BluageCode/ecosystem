/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.service.standardfeatures.services.standard.uc58_servicecopy;


import java.lang.reflect.InvocationTargetException;

import javax.inject.Named;

import org.apache.commons.beanutils.BeanUtils;

import com.bluage.documentation.business.standardfeatures.services.standard.uc58_servicecopy.bos.Team58BO;
import com.bluage.documentation.service.AbstractService;
import com.netfective.bluage.core.exception.ApplicationException;

/**
 * Defines  application services for ServiceCopyTeam58
  */
@Named("com.bluage.documentation.service.standardfeatures.services.standard.uc58_servicecopy.ServiceCopyTeam58")
public class ServiceCopyTeam58  extends AbstractService{


	


	/**
	 *  Default constructor 
	 */
	public ServiceCopyTeam58() {
		super(); // Call super for default constructor : ServiceCopyTeam58
	}
	

		
	/**    
	 * Operation Copy Properties.
	 * @param team {@link Team58BO}
		 * @return {@link Team58BO}
	 */
	public Team58BO copyBo58(Team58BO team )
 throws ApplicationException {

		Team58BO dstObject = new Team58BO();
		try {
			BeanUtils.copyProperties(dstObject, team);
		} catch (IllegalAccessException e) {
			throw new ApplicationException("Error copying object", e);
		} catch (InvocationTargetException e) {
			throw new ApplicationException("Error copying object", e);
		}
		return dstObject;
	}

	

}
