/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.netfective.bluage.springmvc.ajax.tag;


import java.io.IOException;

import javax.servlet.jsp.JspException;

import com.netfective.bluage.springmvc.ajax.util.Constants;

/**
 * Class SortableTag
 * @superclass AjaxZoneTag
 * @interface Constants
 * @author NETFECTIVE TECHNOLOGY
 */
public class SortableTag extends AjaxZoneTag implements Constants {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6615528570592169128L;

	/**
	* This method prints a end tag on output of <code>pageContext</code>
	* @return int
	* @throw JspException
	*/
	public int doEndTag() throws JspException {
		
		try {
			pageContext.getOut().print(END_OF_DIV);
			pageContext.getOut().println(TagUtils.getSortableScript(id));
			return super.doEndTag();
		} catch (IOException e) {
			throw new JspException(e);
		}
	}
}
