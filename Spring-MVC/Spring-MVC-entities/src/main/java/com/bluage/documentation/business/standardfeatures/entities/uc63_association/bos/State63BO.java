/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.business.standardfeatures.entities.uc63_association.bos;

import javax.inject.Inject;
import javax.inject.Named;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import com.bluage.documentation.business.common.bos.StateBO;
import com.bluage.documentation.business.standardfeatures.entities.uc63_association.entities.daofinder.State63DAOFinderImpl;

/**
 * State63BO
 *
* @see com.netfective.bluage.core.business.IBusinessObject
* * @see StateBO
 */
@XmlRootElement

public class State63BO
 extends StateBO {
	
	/**
	 * Default SUID for State63BO.
	 */
	private static final long serialVersionUID = 1L;
	

	@Inject
	@Named ("com.bluage.documentation.business.standardfeatures.entities.uc63_association.entities.daofinder.State63Finder") 
	private State63DAOFinderImpl finder;

												/**
	 * Entity property: stadium63
	 *
	 */
private Stadium63BO stadium63;



   /**
	* public default constructor for class State63BO
	*/
	public State63BO(){
		//NOP State63BO
	}


												/**
	 * Getter for stadium63 of class State63BO.
	 *
	 * @return {@link com.bluage.documentation.business.standardfeatures.entities.uc63_association.bos.Stadium63BO}
	 * @generated
	 */ 
public  Stadium63BO getStadium63() {
		Stadium63BO stadium630 = this.stadium63;
		if(this.stadium63 == null) {
			if(DAO_MODE.equals(this.getObjMode( ) ) ) {
				stadium630 = null;
			} else {
				this.stadium63 = new Stadium63BO();
				this.stadium63.setObjMode( this.getObjMode( ) );
				stadium630 = this.stadium63;
			}
		} else {
			this.stadium63.setObjMode( this.getObjMode( ) );
			if( HELPER_MODE.equals(this.getObjMode( ) ) ) {
				stadium630 = this.stadium63;
			}
			if( DAO_MODE.equals(this.getObjMode()) && this.stadium63.isEmpty ) {
				stadium630 = null;
			}
		}
		return stadium630;
	}


	/**
	 * Setter for stadium63 of class State63BO.
	 * @param value {@link com.bluage.documentation.business.standardfeatures.entities.uc63_association.bos.Stadium63BO}
	 * @generated
	 */

public  void setStadium63(final Stadium63BO stadium63) {
	if(stadium63 != null) {
		stadium63.isEmpty = false;
		stadium63.setObjMode( this.getObjMode( ) );
	}
	this.isEmpty = false;
	this.stadium63 = stadium63;
}


/**
	 * Returns <code>true</code> if the argument is an State63 instance and all identifiers for this entity
     * equal the identifiers of the argument entity. The <code>equals</code> method of the parent entity
     * will also need to return <code>true</code>. Returns <code>false</code> otherwise.
     *
     * @see StateBO#hashCode()
	 * @param object  object to compare with current instance      
     * @return boolean true if equals false if not
     */
    public boolean equals(final Object object )
    {
		if ( this == object )
        {
            return true;
        }
        if ( !( object instanceof State63BO ) )
        {
            return false;
        }
        final State63BO that = (State63BO)object;
    
    	            if (        				        			this.getCode() == null
        	        ) {
        	return false;
        }
		return new EqualsBuilder()
			.append(this.getCode(), that.getCode())
			 	.isEquals();
	}
	/**

	 * Returns a hash code based on this entity's identifiers and the hash code of the parent entity.
     *
     * @see StateBO#hashCode()
     * @return int
     */    
    public int hashCode() {
        return new HashCodeBuilder()
             .append(getCode())
				 	.appendSuper(super.hashCode())
            .toHashCode();
    }
}

