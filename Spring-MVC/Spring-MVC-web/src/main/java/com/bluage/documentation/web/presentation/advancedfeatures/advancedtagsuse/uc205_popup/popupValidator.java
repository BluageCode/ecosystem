/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.advancedfeatures.advancedtagsuse.uc205_popup ;


import org.apache.log4j.Logger;
import org.springframework.validation.Errors;

import com.netfective.bluage.core.validator.AbstractValidator;
/**
* Class :popupValidator
*/
public class popupValidator extends AbstractValidator{
	
	// LOGGER for the class popupValidator
	private static final Logger LOGGER = Logger.getLogger( popupValidator.class);
	
	
	/**
	* Operation validate for popupForm
	* @param obj : the current form (popupForm)
	* @param errors : The spring errors to return for the form popupForm
	*/
	public void validate(final Object obj,final Errors errors){
		LOGGER.info("Beginning method : validate"); // popupValidator
		popupForm cpopupForm = (popupForm)obj; // popupValidator
		LOGGER.info("Ending method : validate the form "+ cpopupForm.getClass().getName()); // popupValidator
	}

	/**
	* Method to implements to use spring validators (popupForm)
	* @param aClass : Class for the form popupForm
	* @return boolean
	*/
	public boolean supports(final Class aClass){
		return (new popupForm()).getClass().equals(aClass); // popupValidator
	}
}
