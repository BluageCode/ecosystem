/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.service.advancedfeatures.valueobject.uc206_valueobject;


import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import com.bluage.documentation.business.advancedfeatures.valueobject.uc206_valueobject.bos.Player206BO;
import com.bluage.documentation.business.advancedfeatures.valueobject.uc206_valueobject.bos.Position206BO;
import com.bluage.documentation.business.advancedfeatures.valueobject.uc206_valueobject.vo.Player206VO;
import com.bluage.documentation.business.advancedfeatures.valueobject.uc206_valueobject.vo.Position206VO;
import com.bluage.documentation.business.advancedfeatures.valueobject.uc206_valueobject.vo.Screen206VO;
import com.bluage.documentation.service.AbstractService;
import com.bluage.documentation.service.advancedfeatures.valueobject.uc206_valueobject.voservice.ServicePlayer206ConvertBO2VO;
import com.bluage.documentation.service.advancedfeatures.valueobject.uc206_valueobject.voservice.ServicePlayer206Copy;
import com.bluage.documentation.service.advancedfeatures.valueobject.uc206_valueobject.voservice.ServicePlayer206TransferVO2BO;
import com.bluage.documentation.service.advancedfeatures.valueobject.uc206_valueobject.voservice.ServicePosition206ConvertBO2VO;
import com.netfective.bluage.core.exception.ApplicationException;

/**
 * Defines  application services for ServiceScreen206
  */
@Named("com.bluage.documentation.service.advancedfeatures.valueobject.uc206_valueobject.ServiceScreen206")
public class ServiceScreen206  extends AbstractService{


					@Inject
		@Named ("com.bluage.documentation.service.advancedfeatures.valueobject.uc206_valueobject.voservice.ServicePlayer206TransferVO2BO")
		private ServicePlayer206TransferVO2BO  instanceServicePlayer206TransferVO2BO;
							@Inject
		@Named ("com.bluage.documentation.service.advancedfeatures.valueobject.uc206_valueobject.voservice.ServicePlayer206Copy")
		private ServicePlayer206Copy  instanceServicePlayer206Copy;
							@Inject
		@Named ("com.bluage.documentation.service.advancedfeatures.valueobject.uc206_valueobject.voservice.ServicePlayer206ConvertBO2VO")
		private ServicePlayer206ConvertBO2VO  instanceServicePlayer206ConvertBO2VO;
							@Inject
		@Named ("com.bluage.documentation.service.advancedfeatures.valueobject.uc206_valueobject.ServicePlayer206")
		private ServicePlayer206  instanceServicePlayer206;
																									@Inject
		@Named ("com.bluage.documentation.service.advancedfeatures.valueobject.uc206_valueobject.voservice.ServicePosition206ConvertBO2VO")
		private ServicePosition206ConvertBO2VO  instanceServicePosition206ConvertBO2VO;
							@Inject
		@Named ("com.bluage.documentation.service.advancedfeatures.valueobject.uc206_valueobject.ServicePosition206")
		private ServicePosition206  instanceServicePosition206;
									


	/**
	 *  Default constructor 
	 */
	public ServiceScreen206() {
		super(); // Call super for default constructor : ServiceScreen206
	}
	



			
	/**    
	 * Process operation.
	 * @param screen206 {@link Screen206VO}
		 * @return {@link Screen206VO}
	 */
	public Screen206VO initializeListPosition(Screen206VO screen206 )
 throws ApplicationException {

				return privateInitializeListPosition(screen206);
	}
	

	/**    
	 * Process operation.
	 * @param screen206 {@link Screen206VO}
		 * @return {@link Screen206VO}
	 */
	private Screen206VO privateInitializeListPosition(Screen206VO screen206 )
 throws ApplicationException {
				// Central buffer node declaration for operation initializeListPosition
			List	 listPosition206 =  new java.util.ArrayList();
					List	 positionVOs =  new java.util.ArrayList();
					Position206VO	 positionVO =  new Position206VO();
					Position206BO	 positionBO =  new Position206BO();
							// Calling an operation : Find all positions
listPosition206 = instanceServicePosition206.position206FindAll();
/* Executing : */
positionVOs = new ArrayList();
for (java.util.Iterator iter0 = listPosition206.iterator(); iter0.hasNext();) {
positionBO = (com.bluage.documentation.business.advancedfeatures.valueobject.uc206_valueobject.bos.Position206BO)iter0.next();
// Calling an operation : 
positionVO = instanceServicePosition206ConvertBO2VO.position206Convert(positionBO);
/* Executing : */
positionVOs.add(positionVO);
}
// Calling an operation : 
screen206.setListPosition206(positionVOs);

						return screen206;
		}

		
			
	/**    
	 * Process operation.
	 * @return {@link Screen206VO}
	 */
	public Screen206VO initializeScreen206( )
 throws ApplicationException {

				return privateInitializeScreen206();
	}
	

	/**    
	 * Process operation.
	 * @return {@link Screen206VO}
	 */
	private Screen206VO privateInitializeScreen206( )
 throws ApplicationException {
				// Return Type declaration.
				Screen206VO screen206 = new Screen206VO();
					// Central buffer node declaration for operation initializeScreen206
			Position206VO	 position206VO =  new Position206VO();
					Player206VO	 player206VO =  new Player206VO();
					Player206VO	 player206CriteriaVO =  new Player206VO();
					/* Executing : */
player206VO.setPositionVO(position206VO);
// Calling an operation : set PlayerVO
screen206.setPlayer206VO(player206VO);
/* Executing : */
player206CriteriaVO.setPositionVO(position206VO);
// Calling an operation : set PlayerCriteria
screen206.setPlayer206CriteriaVO(player206CriteriaVO);

						return screen206;
		}

		
			
	/**    
	 * Process operation.
	 * @param screen206 {@link Screen206VO}
		 * @return {@link void}
	 */
	public void executeCreate(Screen206VO screen206 )
 throws ApplicationException {

			privateExecuteCreate(screen206);
	}
	

	/**    
	 * Process operation.
	 * @param screen206 {@link Screen206VO}
		 * @return {@link void}
	 */
	private void privateExecuteCreate(Screen206VO screen206 )
 throws ApplicationException {
		// Central buffer node declaration for operation executeCreate
			Player206VO	 player206VO =  new Player206VO();
					Player206BO	 player206BO =  new Player206BO();
								// Calling an operation : Get Player 206
player206VO = screen206.getPlayer206VO();
// Calling an operation : Copy Player VO to BO
player206BO = instanceServicePlayer206Copy.player206CopyVO2BO(player206VO);
// Calling an operation : Transfer Player VO to BO
instanceServicePlayer206TransferVO2BO.player206Transfer(player206VO, player206BO);
// Calling an operation : Create Player
instanceServicePlayer206.player206Create(player206BO);

		}

		
			
	/**    
	 * Process operation.
	 * @param screen206 {@link Screen206VO}
		 * @return {@link List}
	 */
	public List executeListPlayer(Screen206VO screen206 )
 throws ApplicationException {

				return privateExecuteListPlayer(screen206);
	}
	

	/**    
	 * Process operation.
	 * @param screen206 {@link Screen206VO}
		 * @return {@link List}
	 */
	private List privateExecuteListPlayer(Screen206VO screen206 )
 throws ApplicationException {
						// Return Type declaration.
				List playerVOs = new java.util.ArrayList();
					// Central buffer node declaration for operation executeListPlayer
			Player206BO	 player206BO =  new Player206BO();
					Player206VO	 player206VO =  new Player206VO();
					List	 listPlayerBO =  new java.util.ArrayList();
							// Calling an operation : Find all players
listPlayerBO = instanceServicePlayer206.player206FindAll();
/* Executing : */
playerVOs = new ArrayList();
for (java.util.Iterator iter0 = listPlayerBO.iterator(); iter0.hasNext();) {
player206BO = (com.bluage.documentation.business.advancedfeatures.valueobject.uc206_valueobject.bos.Player206BO)iter0.next();
// Calling an operation : Convert with Fks Player BO to VO
player206VO = instanceServicePlayer206ConvertBO2VO.player206ConvertWithFks(player206BO);
/* Executing : */
playerVOs.add(player206VO);
}
// Calling an operation : set Player VOs
screen206.setPlayerVOs(playerVOs);

						return playerVOs;
		}

		
			
	/**    
	 * Process operation.
	 * @param screen206 {@link Screen206VO}
		 * @return {@link List}
	 */
	public List searchPlayer206ByProperties(Screen206VO screen206 )
 throws ApplicationException {

				return privateSearchPlayer206ByProperties(screen206);
	}
	

	/**    
	 * Process operation.
	 * @param screen206 {@link Screen206VO}
		 * @return {@link List}
	 */
	private List privateSearchPlayer206ByProperties(Screen206VO screen206 )
 throws ApplicationException {
						// Return Type declaration.
				List playerVOs = new java.util.ArrayList();
					// Central buffer node declaration for operation searchPlayer206ByProperties
			Player206BO	 player206BO =  new Player206BO();
					List	 listPlayerBO =  new java.util.ArrayList();
					Player206VO	 playerVO =  new Player206VO();
					Player206BO	 playerBO =  new Player206BO();
					Player206VO	 player206VO =  new Player206VO();
									// Calling an operation : Get Player VO
player206VO = screen206.getPlayer206VO();
// Calling an operation : Copy Player VO to BO
player206BO = instanceServicePlayer206Copy.player206CopyVO2BO(player206VO);
// Calling an operation : transfer Player VO to BO
instanceServicePlayer206TransferVO2BO.player206Transfer(player206VO, player206BO);
/* Executing : */
player206BO.setId(null);
/* Executing : */
player206BO.setEstimatedValue(null);
/* Executing : */
player206BO.setWeight(null);
/* Executing : */
player206BO.setHeight(null);
// Calling an operation : Find Player by properties
listPlayerBO = instanceServicePlayer206.player206FindByProperties(player206BO);
/* Executing : */
playerVOs = new ArrayList();
for (java.util.Iterator iter0 = listPlayerBO.iterator(); iter0.hasNext();) {
playerBO = (com.bluage.documentation.business.advancedfeatures.valueobject.uc206_valueobject.bos.Player206BO)iter0.next();
// Calling an operation : Convert with Fks Player BO to VO
playerVO = instanceServicePlayer206ConvertBO2VO.player206ConvertWithFks(playerBO);
/* Executing : */
playerVOs.add(playerVO)
;
}
// Calling an operation : Set Player VOs
screen206.setPlayerVOs(playerVOs);

						return playerVOs;
		}

		
			
	/**    
	 * Process operation.
	 * @param screen206 {@link Screen206VO}
		 * @return {@link List}
	 */
	public List searchPlayer206ByCriteria(Screen206VO screen206 )
 throws ApplicationException {

				return privateSearchPlayer206ByCriteria(screen206);
	}
	

	/**    
	 * Process operation.
	 * @param screen206 {@link Screen206VO}
		 * @return {@link List}
	 */
	private List privateSearchPlayer206ByCriteria(Screen206VO screen206 )
 throws ApplicationException {
						// Return Type declaration.
				List playerVOs = new java.util.ArrayList();
					// Central buffer node declaration for operation searchPlayer206ByCriteria
			Player206BO	 player206BO =  new Player206BO();
					String	 lastName =  "";
					List	 listPlayerBO =  new java.util.ArrayList();
					Player206VO	 player206CritVO =  new Player206VO();
					Player206VO	 playerVO =  new Player206VO();
					Player206BO	 playerBO =  new Player206BO();
									// Calling an operation : Get Player Criteria
player206CritVO = screen206.getPlayer206CriteriaVO();
// Calling an operation : Copy Player VO to BO
player206BO = instanceServicePlayer206Copy.player206CopyVO2BO(player206CritVO);
// Calling an operation : Transfer Player VO to BO
instanceServicePlayer206TransferVO2BO.player206Transfer(player206CritVO, player206BO);
// Calling an operation : Get LastName
lastName = player206BO.getLastName();
/* Executing : */
lastName = (lastName.length()==0) ? "%": lastName.substring(0,1).toUpperCase() + lastName.substring(1,lastName.length()).toLowerCase() + "%";
// Calling an operation : Find Player By Criteria
listPlayerBO = instanceServicePlayer206.player206FindByCriteria(lastName);
/* Executing : */
playerVOs = new ArrayList();
for (java.util.Iterator iter0 = listPlayerBO.iterator(); iter0.hasNext();) {
playerBO = (com.bluage.documentation.business.advancedfeatures.valueobject.uc206_valueobject.bos.Player206BO)iter0.next();
// Calling an operation : 
playerVO = instanceServicePlayer206ConvertBO2VO.player206ConvertWithFks(playerBO);
/* Executing : */
playerVOs.add(playerVO);
}
// Calling an operation : 
screen206.setPlayerVOs(playerVOs);

						return playerVOs;
		}

		
			
	/**    
	 * Process operation.
	 * @param selectedPlayer {@link Player206VO}
		 * @return {@link void}
	 */
	public void executeUpdate(Player206VO selectedPlayer )
 throws ApplicationException {

			privateExecuteUpdate(selectedPlayer);
	}
	

	/**    
	 * Process operation.
	 * @param selectedPlayer {@link Player206VO}
		 * @return {@link void}
	 */
	private void privateExecuteUpdate(Player206VO selectedPlayer )
 throws ApplicationException {
		// Central buffer node declaration for operation executeUpdate
			Player206BO	 player206BO =  new Player206BO();
								// Calling an operation : 
player206BO = instanceServicePlayer206Copy.player206CopyVO2BO(selectedPlayer);
// Calling an operation : 
instanceServicePlayer206TransferVO2BO.player206Transfer(selectedPlayer, player206BO);
// Calling an operation : 
instanceServicePlayer206.player206Update(player206BO);

		}

		
			
	/**    
	 * Process operation.
	 * @param selectedPlayer {@link Player206VO}
		 * @return {@link Player206VO}
	 */
	public Player206VO executeDetails(Player206VO selectedPlayer )
 throws ApplicationException {

				return privateExecuteDetails(selectedPlayer);
	}
	

	/**    
	 * Process operation.
	 * @param selectedPlayer {@link Player206VO}
		 * @return {@link Player206VO}
	 */
	private Player206VO privateExecuteDetails(Player206VO selectedPlayer )
 throws ApplicationException {
				// Central buffer node declaration for operation executeDetails
			Player206BO	 playerResultBO =  new Player206BO();
					Player206BO	 player206BO =  new Player206BO();
									// Calling an operation : 
player206BO = instanceServicePlayer206Copy.player206CopyVO2BO(selectedPlayer);
// Calling an operation : 
instanceServicePlayer206TransferVO2BO.player206Transfer(selectedPlayer, player206BO);
// Calling an operation : 
playerResultBO = instanceServicePlayer206.player206FindByID(player206BO);
// Calling an operation : 
selectedPlayer = instanceServicePlayer206ConvertBO2VO.player206ConvertWithFks(playerResultBO);

						return selectedPlayer;
		}

		}
