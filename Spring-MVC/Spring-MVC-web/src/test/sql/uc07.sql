ALTER TABLE player07 DROP CONSTRAINT player07_position_fkc;

ALTER TABLE position07 DROP CONSTRAINT position07_pkey;

ALTER TABLE player07 DROP CONSTRAINT player07_pkey;

DROP TABLE position07;

DROP TABLE player07;

CREATE TABLE position07 (
		code VARCHAR(2) NOT NULL,
		version INT4 NOT NULL,
		name VARCHAR(20) NOT NULL
	);

CREATE TABLE player07 (
		id INT8 NOT NULL,
		version INT4 NOT NULL,
		first_name VARCHAR(30) NOT NULL,
		last_name VARCHAR(30) NOT NULL,
		date_of_birth DATE,
		estimated_value FLOAT4,
		weight INT4,
		height INT4,
		rookie BOOL,
		position_fk VARCHAR(2) NOT NULL
	);


INSERT INTO position07 VALUES ('PG', 0, 'Point Guard');
INSERT INTO position07 VALUES ('SG', 0, 'Shooting Guard');
INSERT INTO position07 VALUES ('SF', 0, 'Small Forward');
INSERT INTO position07 VALUES ('PF', 0, 'Power Forward');
INSERT INTO position07 VALUES ('C', 0, 'Center');

INSERT INTO player07 VALUES (401, 0, 'Gregg', 'Nash', '1972-01-11', 6.8600001, 147, 215, false, 'PG');
INSERT INTO player07 VALUES (402, 0, 'Floyd', 'Howell', '1979-08-25', 25.709999, 155, 192, false, 'SG');
INSERT INTO player07 VALUES (403, 0, 'Simon', 'Klein', '1987-01-21', 22.24, 63, 203, false, 'SF');
INSERT INTO player07 VALUES (406, 0, 'Troy', 'Rowe', '1973-02-12', 26.82, 94, 211, false, 'PF');
INSERT INTO player07 VALUES (407, 0, 'Jesus', 'Jimenez', '1985-09-01', 17.73, 133, 159, false, 'PF');
INSERT INTO player07 VALUES (409, 0, 'Eddie', 'Boyd', '1967-10-05', 21.719999, 118, 232, false, 'PF');
INSERT INTO player07 VALUES (410, 0, 'Luther', 'Peterson', '1985-06-09', 1.04, 83, 181, false, 'SG');
INSERT INTO player07 VALUES (411, 0, 'Daniel', 'Vargas', '1979-04-21', 30.43, 117, 155, false, 'SG');
INSERT INTO player07 VALUES (412, 0, 'Freddie', 'Kelly', '1967-03-22', 34.400002, 81, 196, false, 'PF');
INSERT INTO player07 VALUES (413, 0, 'Sam', 'Farmer', '1987-11-29', 37.580002, 98, 201, false, 'SF');
INSERT INTO player07 VALUES (414, 0, 'Randal', 'Hampton', '1967-12-01', 31.219999, 121, 202, false, 'SG');
INSERT INTO player07 VALUES (415, 0, 'Felipe', 'Olson', '1976-09-28', 8.0600004, 129, 189, false, 'PF');
INSERT INTO player07 VALUES (416, 0, 'Gary', 'Christensen', '1974-03-13', 21.52, 145, 170, false, 'PG');
INSERT INTO player07 VALUES (417, 0, 'Aaron', 'Mullins', '1972-01-15', 8.54, 86, 239, false, 'SG');
INSERT INTO player07 VALUES (418, 0, 'Miguel', 'Moore', '1967-03-26', 9.4799995, 67, 173, false, 'PG');
INSERT INTO player07 VALUES (419, 0, 'Bruce', 'Adkins', '1986-05-24', 43.720001, 150, 209, false, 'SG');
INSERT INTO player07 VALUES (420, 0, 'Felipe', 'Lawrence', '1981-04-19', 33.66, 150, 219, false, 'PF');
INSERT INTO player07 VALUES (421, 0, 'Nelson', 'Davis', '1978-05-16', 20.84, 119, 171, false, 'PF');
INSERT INTO player07 VALUES (422, 0, 'Jeremiah', 'Johnston', '1983-12-15', 14.57, 149, 200, false, 'SG');
INSERT INTO player07 VALUES (423, 0, 'Enrique', 'Wilkins', '1967-01-21', 47.740002, 75, 240, false, 'SF');
INSERT INTO player07 VALUES (424, 0, 'Gerald', 'Jimenez', '1970-01-22', 29.26, 63, 166, false, 'SF');
INSERT INTO player07 VALUES (425, 0, 'Michael', 'Simon', '1975-12-23', 43.950001, 118, 222, false, 'C');
INSERT INTO player07 VALUES (426, 0, 'Ryan', 'Dennis', '1984-01-19', 48.75, 134, 185, false, 'PF');
INSERT INTO player07 VALUES (427, 0, 'Eddie', 'Miles', '1986-08-25', 3.6800001, 60, 232, false, 'PF');
INSERT INTO player07 VALUES (428, 0, 'Jordan', 'Richardson', '1977-03-16', 47.369999, 69, 172, false, 'PF');
INSERT INTO player07 VALUES (429, 0, 'Brett', 'Santos', '1969-05-12', 38.900002, 139, 214, false, 'C');
INSERT INTO player07 VALUES (430, 0, 'Otis', 'Bowman', '1985-11-23', 10.64, 91, 231, false, 'SF');
INSERT INTO player07 VALUES (431, 0, 'Seth', 'Quinn', '1979-11-06', 19.280001, 137, 220, false, 'C');
INSERT INTO player07 VALUES (432, 0, 'Frank', 'Barrett', '1965-08-10', 46.560001, 121, 178, false, 'PF');
INSERT INTO player07 VALUES (433, 0, 'Ernest', 'Mcgee', '1971-03-20', 6.3299999, 158, 165, false, 'PF');
INSERT INTO player07 VALUES (434, 0, 'Jeremy', 'Blair', '1967-05-24', 24.02, 128, 192, false, 'PF');
INSERT INTO player07 VALUES (435, 0, 'Kenny', 'Kim', '1966-10-03', 43.27, 61, 207, false, 'SG');
INSERT INTO player07 VALUES (436, 0, 'Rolando', 'Lawrence', '1972-03-07', 22.65, 142, 200, false, 'SG');
INSERT INTO player07 VALUES (437, 0, 'Sheldon', 'Vasquez', '1979-06-06', 3.04, 92, 211, false, 'C');
INSERT INTO player07 VALUES (438, 0, 'Arnold', 'Miles', '1970-09-24', 21.389999, 103, 207, false, 'PF');
INSERT INTO player07 VALUES (440, 0, 'Jackie', 'Gomez', '1985-11-01', 16.85, 151, 151, false, 'PF');
INSERT INTO player07 VALUES (441, 0, 'Wayne', 'Wise', '1981-06-15', 7.4200001, 123, 167, false, 'PG');
INSERT INTO player07 VALUES (442, 0, 'Kyle', 'Boyd', '1986-09-13', 47.16, 86, 235, false, 'PF');
INSERT INTO player07 VALUES (443, 0, 'Mitchell', 'Medina', '1966-06-19', 24.07, 158, 235, false, 'SF');
INSERT INTO player07 VALUES (444, 0, 'Kerry', 'Mccarthy', '1986-10-20', 28.08, 125, 190, false, 'C');
INSERT INTO player07 VALUES (445, 0, 'Orlando', 'Mcgee', '1979-12-14', 38.889999, 115, 152, false, 'PG');
INSERT INTO player07 VALUES (446, 0, 'Charlie', 'Clark', '1971-08-31', 45.700001, 120, 187, false, 'SG');
INSERT INTO player07 VALUES (447, 0, 'Marshall', 'Norman', '1965-06-25', 0.69999999, 99, 168, false, 'PF');
INSERT INTO player07 VALUES (448, 0, 'Duane', 'Hart', '1974-02-12', 5.9899998, 99, 217, false, 'SF');
INSERT INTO player07 VALUES (449, 0, 'Julius', 'Byrd', '1968-09-13', 35.459999, 62, 181, false, 'SF');
INSERT INTO player07 VALUES (450, 0, 'Ed', 'Drake', '1972-11-04', 37.450001, 121, 164, false, 'C');
INSERT INTO player07 VALUES (451, 0, 'Clyde', 'Chambers', '1976-08-01', 35.619999, 110, 209, false, 'C');
INSERT INTO player07 VALUES (452, 0, 'Oliver', 'Ballard', '1972-07-18', 25.309999, 63, 237, false, 'C');
INSERT INTO player07 VALUES (453, 0, 'Myron', 'Clarke', '1966-12-31', 14.93, 120, 190, false, 'PF');
INSERT INTO player07 VALUES (454, 0, 'Carroll', 'Shaw', '1987-03-07', 8.7200003, 123, 209, false, 'PF');
INSERT INTO player07 VALUES (455, 0, 'Phil', 'Christensen', '1989-02-23', 25.620001, 116, 214, false, 'SG');
INSERT INTO player07 VALUES (457, 0, 'Bob', 'Fleming', '1972-01-16', 41, 91, 164, false, 'SF');
INSERT INTO player07 VALUES (458, 0, 'Perry', 'Vega', '1975-08-29', 0.28, 153, 224, false, 'SF');
INSERT INTO player07 VALUES (459, 0, 'Alfred', 'Briggs', '1965-09-29', 15.74, 96, 162, false, 'SF');
INSERT INTO player07 VALUES (463, 0, 'Raul', 'Weaver', '1967-02-21', 46.25, 77, 206, false, 'PG');
INSERT INTO player07 VALUES (464, 0, 'Dan', 'Briggs', '1986-05-05', 34.34, 115, 184, false, 'PG');
INSERT INTO player07 VALUES (465, 0, 'Dave', 'Pratt', '1975-09-19', 9.0299997, 148, 164, false, 'C');
INSERT INTO player07 VALUES (466, 0, 'Dale', 'Rhodes', '1966-10-12', 22.65, 64, 197, false, 'PF');
INSERT INTO player07 VALUES (467, 0, 'Marvin', 'Vaughn', '1982-07-08', 13.83, 136, 228, false, 'PF');
INSERT INTO player07 VALUES (468, 0, 'Jay', 'Owens', '1976-06-18', 30.57, 61, 150, false, 'SG');
INSERT INTO player07 VALUES (469, 0, 'Wendell', 'Page', '1988-12-12', 37.77, 116, 188, false, 'PF');
INSERT INTO player07 VALUES (470, 0, 'Leonard', 'Chapman', '1988-11-14', 37.619999, 160, 184, false, 'PF');
INSERT INTO player07 VALUES (471, 0, 'Doyle', 'Clarke', '1982-09-01', 39.630001, 121, 234, false, 'SG');
INSERT INTO player07 VALUES (472, 0, 'Jesus', 'Goodwin', '1975-11-25', 42.810001, 96, 225, false, 'SG');
INSERT INTO player07 VALUES (474, 0, 'Jaime', 'Mccormick', '1969-10-27', 36.639999, 148, 176, false, 'C');
INSERT INTO player07 VALUES (475, 0, 'Rex', 'Lee', '1978-05-11', 31.08, 157, 190, false, 'C');
INSERT INTO player07 VALUES (476, 0, 'Malcolm', 'Chavez', '1973-07-09', 39.950001, 82, 192, false, 'PF');
INSERT INTO player07 VALUES (477, 0, 'Martin', 'Boyd', '1965-02-16', 35.23, 135, 182, false, 'PF');
INSERT INTO player07 VALUES (478, 0, 'Joey', 'Roberson', '1985-12-18', 49.02, 63, 172, false, 'C');
INSERT INTO player07 VALUES (479, 0, 'Enrique', 'Young', '1976-07-26', 18, 150, 223, false, 'SF');
INSERT INTO player07 VALUES (480, 0, 'Jake', 'Lane', '1971-10-13', 20.49, 81, 187, false, 'SG');
INSERT INTO player07 VALUES (481, 0, 'Preston', 'Brown', '1969-04-08', 41.57, 144, 206, false, 'C');
INSERT INTO player07 VALUES (483, 0, 'Rex', 'Daniel', '1983-11-20', 34.369999, 88, 238, false, 'PG');
INSERT INTO player07 VALUES (484, 0, 'Neal', 'Bush', '1986-03-13', 29.469999, 132, 160, false, 'PG');
INSERT INTO player07 VALUES (486, 0, 'Omar', 'Gordon', '1981-12-23', 0.50999999, 76, 238, false, 'C');
INSERT INTO player07 VALUES (487, 0, 'Ronnie', 'Scott', '1965-02-16', 7.2600002, 134, 185, false, 'PF');
INSERT INTO player07 VALUES (488, 0, 'Corey', 'Rice', '1967-03-20', 40.290001, 149, 228, false, 'SG');
INSERT INTO player07 VALUES (489, 0, 'Everett', 'Fields', '1976-03-27', 12.61, 61, 152, false, 'PF');
INSERT INTO player07 VALUES (490, 0, 'Timothy', 'Payne', '1987-02-25', 25.76, 73, 163, false, 'SG');
INSERT INTO player07 VALUES (492, 0, 'Darin', 'Lambert', '1986-12-10', 42.240002, 136, 228, false, 'SG');
INSERT INTO player07 VALUES (494, 0, 'Lance', 'Mitchell', '1985-02-07', 46.099998, 114, 177, false, 'SF');
INSERT INTO player07 VALUES (495, 0, 'Juan', 'Ortiz', '1984-03-15', 17.450001, 127, 195, false, 'SF');
INSERT INTO player07 VALUES (496, 0, 'David', 'Paul', '1970-02-03', 18.9, 110, 172, false, 'PF');
INSERT INTO player07 VALUES (497, 0, 'Carlton', 'Cannon', '1985-02-03', 28.92, 149, 151, false, 'SF');
INSERT INTO player07 VALUES (498, 0, 'Evan', 'James', '1971-09-06', 32.080002, 125, 163, false, 'PG');
INSERT INTO player07 VALUES (499, 0, 'Alfonso', 'Saunders', '1984-09-14', 30.49, 160, 191, false, 'PF');
INSERT INTO player07 VALUES (500, 0, 'Wilbert', 'Porter', '1970-06-13', 7.2600002, 92, 175, false, 'PF');
INSERT INTO player07 VALUES (501, 0, 'Horace', 'Aguilar', '1982-02-13', 16.93, 107, 221, false, 'PG');
INSERT INTO player07 VALUES (502, 0, 'Juan', 'Sparks', '1981-06-07', 7.8699999, 91, 173, false, 'SG');
INSERT INTO player07 VALUES (503, 0, 'Dwight', 'Strickland', '1989-07-29', 33.459999, 73, 157, false, 'SF');
INSERT INTO player07 VALUES (504, 0, 'Bert', 'Carroll', '1977-06-09', 44.060001, 85, 193, false, 'PG');
INSERT INTO player07 VALUES (505, 0, 'Franklin', 'Blake', '1967-05-05', 20.65, 116, 178, false, 'C');
INSERT INTO player07 VALUES (506, 0, 'Santos', 'Ryan', '1980-12-26', 43.459999, 113, 178, false, 'PF');
INSERT INTO player07 VALUES (507, 0, 'Percy', 'Summers', '1985-12-18', 13.65, 72, 223, false, 'PF');
INSERT INTO player07 VALUES (508, 0, 'William', 'Todd', '1985-07-29', 32.43, 73, 153, false, 'C');
INSERT INTO player07 VALUES (509, 0, 'Roger', 'Long', '1984-02-01', 41.73, 68, 194, false, 'SG');
INSERT INTO player07 VALUES (510, 0, 'Rudolph', 'Lloyd', '1974-01-15', 23.43, 79, 169, false, 'PG');
INSERT INTO player07 VALUES (511, 0, 'Oscar', 'Park', '1978-08-06', 16.129999, 70, 219, false, 'SG');
INSERT INTO player07 VALUES (513, 0, 'Neil', 'Henderson', '1971-11-08', 9.7799997, 123, 201, false, 'PG');
INSERT INTO player07 VALUES (514, 0, 'Kenneth', 'Briggs', '1986-01-28', 27.389999, 99, 194, false, 'SF');
INSERT INTO player07 VALUES (515, 0, 'Isaac', 'Walters', '1977-02-24', 29.02, 115, 231, false, 'PF');
INSERT INTO player07 VALUES (516, 0, 'Nathan', 'Clarke', '1969-09-13', 18.459999, 74, 181, false, 'PG');
INSERT INTO player07 VALUES (517, 0, 'Clifton', 'Figueroa', '1988-08-29', 19.610001, 134, 193, false, 'PF');
INSERT INTO player07 VALUES (518, 0, 'Willie', 'Weaver', '1988-06-10', 4.6799998, 102, 235, false, 'SF');
INSERT INTO player07 VALUES (519, 0, 'Rogelio', 'Ingram', '1989-02-14', 16.870001, 133, 231, false, 'C');
INSERT INTO player07 VALUES (520, 0, 'Jared', 'Goodman', '1972-04-02', 14.69, 106, 237, false, 'C');
INSERT INTO player07 VALUES (521, 0, 'Sherman', 'Pearson', '1967-05-25', 5.3699999, 110, 156, false, 'PF');
INSERT INTO player07 VALUES (522, 0, 'Lawrence', 'Nguyen', '1988-03-10', 10.62, 92, 192, false, 'SF');
INSERT INTO player07 VALUES (524, 0, 'Carlos', 'Hicks', '1973-06-15', 23.48, 115, 185, false, 'SF');
INSERT INTO player07 VALUES (525, 0, 'Willis', 'Fuller', '1984-12-05', 29.719999, 110, 240, false, 'SG');
INSERT INTO player07 VALUES (526, 0, 'Jimmy', 'Obrien', '1975-11-25', 41.040001, 97, 226, false, 'C');
INSERT INTO player07 VALUES (527, 0, 'Martin', 'Norton', '1974-10-18', 21.01, 72, 151, false, 'SG');
INSERT INTO player07 VALUES (528, 0, 'Dwayne', 'Gordon', '1978-09-11', 14.78, 122, 227, false, 'C');
INSERT INTO player07 VALUES (529, 0, 'Thomas', 'Pierce', '1980-07-09', 33.48, 130, 174, false, 'SG');
INSERT INTO player07 VALUES (530, 0, 'Jonathan', 'Pittman', '1980-08-20', 8.0500002, 146, 222, false, 'C');
INSERT INTO player07 VALUES (531, 0, 'Norman', 'Dunn', '1978-07-11', 45.32, 64, 228, false, 'SG');
INSERT INTO player07 VALUES (532, 0, 'Earnest', 'Ramirez', '1970-09-12', 19.959999, 108, 162, false, 'SF');
INSERT INTO player07 VALUES (533, 0, 'Marco', 'Ray', '1981-07-28', 31.9, 127, 155, false, 'C');
INSERT INTO player07 VALUES (534, 0, 'Eugene', 'Welch', '1973-05-08', 41.84, 136, 208, false, 'SG');
INSERT INTO player07 VALUES (535, 0, 'Joel', 'Payne', '1982-08-04', 48.110001, 75, 187, false, 'SF');
INSERT INTO player07 VALUES (536, 0, 'Michael', 'Waters', '1972-12-27', 35.759998, 110, 157, false, 'SG');
INSERT INTO player07 VALUES (537, 0, 'Edmund', 'Peterson', '1976-08-24', 7.73, 145, 157, false, 'SG');
INSERT INTO player07 VALUES (538, 0, 'Eugene', 'Morales', '1971-03-08', 30.690001, 148, 173, false, 'PF');
INSERT INTO player07 VALUES (539, 0, 'Howard', 'Harris', '1985-12-20', 41.669998, 115, 172, false, 'SG');
INSERT INTO player07 VALUES (540, 0, 'Delbert', 'Rodriguez', '1967-08-23', 48.77, 139, 235, false, 'SG');
INSERT INTO player07 VALUES (541, 0, 'Luke', 'Rodgers', '1981-08-26', 38.880001, 146, 167, false, 'PF');
INSERT INTO player07 VALUES (542, 0, 'Leland', 'Briggs', '1989-07-06', 25.799999, 88, 164, false, 'SG');
INSERT INTO player07 VALUES (543, 0, 'Dave', 'Berry', '1973-10-05', 49.32, 154, 189, false, 'SF');
INSERT INTO player07 VALUES (544, 0, 'Duane', 'Chapman', '1981-03-01', 48.970001, 76, 151, false, 'SF');
INSERT INTO player07 VALUES (545, 0, 'Norman', 'Bowen', '1975-01-27', 9.3299999, 88, 185, false, 'C');
INSERT INTO player07 VALUES (546, 0, 'Gerardo', 'Ross', '1986-01-23', 36.509998, 120, 189, false, 'PF');
INSERT INTO player07 VALUES (547, 0, 'Brandon', 'Joseph', '1967-09-14', 31.690001, 144, 156, false, 'SG');
INSERT INTO player07 VALUES (548, 0, 'Neal', 'Tran', '1984-01-04', 37.139999, 125, 218, false, 'SF');
INSERT INTO player07 VALUES (549, 0, 'Allan', 'Gonzalez', '1984-04-29', 36.709999, 108, 154, false, 'PF');
INSERT INTO player07 VALUES (550, 0, 'Ryan', 'Newton', '1989-11-12', 27.459999, 69, 239, false, 'PF');
INSERT INTO player07 VALUES (551, 0, 'Ervin', 'Steele', '1980-02-15', 5.0300002, 107, 209, false, 'PF');
INSERT INTO player07 VALUES (552, 0, 'Sherman', 'Love', '1965-10-17', 28.07, 150, 203, false, 'C');
INSERT INTO player07 VALUES (553, 0, 'Norman', 'Sherman', '1971-09-21', 7.25, 113, 166, false, 'SG');
INSERT INTO player07 VALUES (554, 0, 'Antonio', 'Frazier', '1983-10-23', 0.72000003, 66, 196, false, 'C');
INSERT INTO player07 VALUES (555, 0, 'Christian', 'Lopez', '1975-04-06', 2.0599999, 83, 209, false, 'SF');
INSERT INTO player07 VALUES (557, 0, 'Bernard', 'Bowers', '1982-10-09', 40.759998, 70, 202, false, 'C');
INSERT INTO player07 VALUES (558, 0, 'Doug', 'Greene', '1968-02-01', 3.3499999, 100, 169, false, 'SG');
INSERT INTO player07 VALUES (559, 0, 'Mack', 'Burton', '1978-09-16', 38.009998, 107, 240, false, 'SG');
INSERT INTO player07 VALUES (561, 0, 'Andy', 'Crawford', '1978-05-03', 12.41, 72, 171, false, 'SG');
INSERT INTO player07 VALUES (562, 0, 'Doug', 'Fowler', '1978-01-02', 44.16, 125, 190, false, 'SF');
INSERT INTO player07 VALUES (563, 0, 'Jean', 'Stokes', '1987-01-17', 4.0900002, 124, 152, false, 'PG');
INSERT INTO player07 VALUES (564, 0, 'Tommy', 'Collins', '1974-09-19', 37.93, 160, 199, false, 'SG');
INSERT INTO player07 VALUES (566, 0, 'Eric', 'Santos', '1986-08-26', 33.009998, 79, 187, false, 'SG');
INSERT INTO player07 VALUES (567, 0, 'Homer', 'Graham', '1966-10-28', 20.09, 94, 187, false, 'PF');
INSERT INTO player07 VALUES (568, 0, 'Morris', 'Fuller', '1968-05-27', 15.6, 66, 159, false, 'C');
INSERT INTO player07 VALUES (569, 0, 'Nathan', 'Jennings', '1989-11-26', 21.719999, 105, 183, false, 'SF');
INSERT INTO player07 VALUES (570, 0, 'Ray', 'Franklin', '1975-02-06', 21.18, 127, 239, false, 'C');
INSERT INTO player07 VALUES (571, 0, 'Leland', 'Rowe', '1979-07-04', 33.990002, 71, 186, false, 'PF');
INSERT INTO player07 VALUES (572, 0, 'Christian', 'Norris', '1989-09-13', 22.040001, 68, 200, false, 'SG');
INSERT INTO player07 VALUES (573, 0, 'Keith', 'Cannon', '1968-01-14', 20.860001, 131, 216, false, 'C');
INSERT INTO player07 VALUES (575, 0, 'Malcolm', 'Jackson', '1970-06-28', 12.78, 62, 227, false, 'SG');
INSERT INTO player07 VALUES (576, 0, 'Rufus', 'Fox', '1967-03-22', 39.580002, 122, 239, false, 'PF');
INSERT INTO player07 VALUES (577, 0, 'Wade', 'Mclaughlin', '1979-02-16', 20.719999, 117, 196, false, 'PF');
INSERT INTO player07 VALUES (578, 0, 'Jeremy', 'Fernandez', '1984-06-22', 31.08, 147, 178, false, 'SF');
INSERT INTO player07 VALUES (579, 0, 'Dwight', 'Hayes', '1975-11-07', 22.07, 141, 230, false, 'SF');
INSERT INTO player07 VALUES (580, 0, 'Lawrence', 'Keller', '1968-03-14', 39.970001, 65, 184, false, 'SF');
INSERT INTO player07 VALUES (581, 0, 'Jacob', 'Jensen', '1974-01-12', 9.0900002, 83, 220, false, 'SF');
INSERT INTO player07 VALUES (582, 0, 'Drew', 'Riley', '1974-04-09', 29.4, 68, 162, false, 'SG');
INSERT INTO player07 VALUES (583, 0, 'Vincent', 'Barber', '1978-05-02', 44.77, 156, 194, false, 'PF');
INSERT INTO player07 VALUES (584, 0, 'Brian', 'Christensen', '1988-07-10', 18.219999, 141, 178, false, 'PF');
INSERT INTO player07 VALUES (585, 0, 'Tommy', 'Larson', '1967-08-11', 25.690001, 95, 180, false, 'SF');
INSERT INTO player07 VALUES (586, 0, 'Barry', 'Dennis', '1972-11-03', 29.209999, 115, 163, false, 'PF');
INSERT INTO player07 VALUES (587, 0, 'Kenneth', 'Phillips', '1983-11-09', 39.880001, 88, 192, false, 'C');
INSERT INTO player07 VALUES (588, 0, 'Vincent', 'Ray', '1988-04-14', 25.120001, 96, 153, false, 'SF');
INSERT INTO player07 VALUES (589, 0, 'Marlon', 'Howell', '1981-10-10', 10.53, 65, 161, false, 'SF');
INSERT INTO player07 VALUES (590, 0, 'Anthony', 'Cunningham', '1979-03-07', 45.84, 104, 236, false, 'C');
INSERT INTO player07 VALUES (591, 0, 'Eddie', 'Collier', '1967-11-27', 41.66, 107, 189, false, 'PG');
INSERT INTO player07 VALUES (592, 0, 'Gerald', 'Cox', '1967-06-10', 25.379999, 111, 240, false, 'SF');
INSERT INTO player07 VALUES (593, 0, 'Dallas', 'Clayton', '1978-08-11', 8.8800001, 76, 235, false, 'SF');
INSERT INTO player07 VALUES (594, 0, 'Francisco', 'Aguilar', '1978-02-17', 26.799999, 125, 208, false, 'SF');
INSERT INTO player07 VALUES (595, 0, 'Dexter', 'Rivera', '1965-03-16', 47.119999, 153, 232, false, 'C');
INSERT INTO player07 VALUES (596, 0, 'Earl', 'Jacobs', '1981-01-17', 25.74, 124, 203, false, 'SG');
INSERT INTO player07 VALUES (597, 0, 'Jay', 'Oliver', '1977-08-22', 7.0999999, 147, 174, false, 'PF');
INSERT INTO player07 VALUES (598, 0, 'Jimmie', 'West', '1973-06-27', 27.860001, 112, 215, false, 'SG');
INSERT INTO player07 VALUES (599, 0, 'Eugene', 'Terry', '1974-06-06', 36.049999, 98, 197, false, 'C');
INSERT INTO player07 VALUES (600, 0, 'Caleb', 'Byrd', '1971-10-29', 46.169998, 104, 203, false, 'PG');
INSERT INTO player07 VALUES (404, 0, 'Blake', 'Montgomery', '1992-11-16', 37.470001, 113, 205, true, 'SF');
INSERT INTO player07 VALUES (405, 0, 'Doyle', 'Burns', '1990-02-23', 39.130001, 153, 225, true, 'SG');
INSERT INTO player07 VALUES (408, 0, 'Ross', 'Guerrero', '1992-01-26', 13.68, 136, 171, true, 'SF');
INSERT INTO player07 VALUES (439, 0, 'Scott', 'Olson', '1991-12-02', 27.93, 120, 186, true, 'SF');
INSERT INTO player07 VALUES (456, 0, 'Loren', 'Howard', '1991-11-30', 26.68, 84, 184, true, 'PF');
INSERT INTO player07 VALUES (460, 0, 'Ernest', 'Vargas', '1990-07-13', 5.1999998, 109, 178, true, 'SF');
INSERT INTO player07 VALUES (461, 0, 'Tim', 'Kelly', '1992-03-13', 34.07, 101, 221, true, 'SF');
INSERT INTO player07 VALUES (462, 0, 'Mike', 'Day', '1990-11-25', 24.629999, 81, 163, true, 'PF');
INSERT INTO player07 VALUES (473, 0, 'Joshua', 'Malone', '1990-03-23', 23.309999, 151, 173, true, 'C');
INSERT INTO player07 VALUES (482, 0, 'Ed', 'Bass', '1991-02-15', 15.14, 67, 193, true, 'C');
INSERT INTO player07 VALUES (485, 0, 'Gilbert', 'Mcdonald', '1992-08-31', 32.400002, 69, 237, true, 'PF');
INSERT INTO player07 VALUES (491, 0, 'Clark', 'Reese', '1990-01-22', 3.0899999, 135, 211, true, 'PF');
INSERT INTO player07 VALUES (493, 0, 'Randal', 'Barker', '1991-03-19', 48.09, 106, 210, true, 'PF');
INSERT INTO player07 VALUES (512, 0, 'Owen', 'Duncan', '1992-12-22', 49.98, 85, 207, true, 'SF');
INSERT INTO player07 VALUES (523, 0, 'Saul', 'Blake', '1990-07-10', 2.4000001, 70, 169, true, 'PG');
INSERT INTO player07 VALUES (556, 0, 'Rodney', 'Patrick', '1990-02-07', 10.2, 146, 153, true, 'SG');
INSERT INTO player07 VALUES (560, 0, 'Donnie', 'Carter', '1991-09-03', 41, 148, 185, true, 'PG');
INSERT INTO player07 VALUES (565, 0, 'Charles', 'King', '1990-08-09', 36.950001, 70, 214, true, 'PG');
INSERT INTO player07 VALUES (574, 0, 'Marvin', 'Sanchez', '1992-03-11', 22.309999, 147, 208, true, 'SG');

ALTER TABLE position07 ADD CONSTRAINT position07_pkey PRIMARY KEY (code);

ALTER TABLE player07 ADD CONSTRAINT player07_pkey PRIMARY KEY (id);

ALTER TABLE player07 ADD CONSTRAINT player07_position_fkc FOREIGN KEY (position_fk)
	REFERENCES position07 (code);
