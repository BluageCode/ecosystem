package uc72;


import org.junit.Test;
import test.common.AbstractBADocTestCase;

public class IT03uc72ComportementTeams extends AbstractBADocTestCase {
	
	@Override
	public  String getName(){
		return "UC-72";
	}


	@Test
	public void testIT03uc72ComportementTeams() throws Exception {
		// On teste l'UC72 - Standard Features/Entities - Generator Class
		selenium.open("");	
		selenium.click("link=Expand All");
		selenium.waitForPageToLoad("300000");
		selenium.click("link=UC72 - Generator Class");
		// On teste l'affichage de la page de garde :
		assertTrue(selenium.isElementPresent("//h2[contains(text(),'Standard Features > Entities > UC72 > Generator Class')]"));
		assertTrue(selenium.isElementPresent("//h4[contains(text(),'Menu')]"));
		// On teste le comportement teams :
		selenium.click("//a[contains(text(),'[Add a team]')]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//h4[contains(text(),'Add Team')]"));
		// On cree une team vide : 
		selenium.click("//a[contains(text(),'[Create a new team]')]");
		selenium.waitForPageToLoad("30000");
		// On vérifie la présence des messages d'erreur : 
		assertTrue(selenium.isElementPresent("//table/tbody/tr[1]/td/span[contains(text(),'City name is required.')]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[2]/td/span[contains(text(),'Team name is required.')]"));
		// On cree deux teams : 
		verifyEquals("6", selenium.getXpathCount("//table[contains(@id,'table_teams')]/tbody//tr"));
		selenium.type("//input[contains(@id,'txt_city')]", "Miami");
		selenium.type("//input[contains(@id,'txt_name')]", "Heat");
		selenium.select("//*[contains(@id,'select_state')]", "label=Florida");
		selenium.click("//a[contains(text(),'[Create a new team]')]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("7", selenium.getXpathCount("//table[contains(@id,'table_teams')]/tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'table_teams')]/tbody/tr[td/span[contains(text(),'Miami')] and td/span[contains(text(),'Heat')] and td/span[contains(text(),'Florida')] and td/span[contains(text(),'FL')] and td/a[contains(text(),'[delete]')]]"));
		selenium.type("//input[contains(@id,'txt_city')]", "New York");
		selenium.type("//input[contains(@id,'txt_name')]", "Knicks");
		selenium.select("//*[contains(@id,'select_state')]", "label=New York");
		selenium.click("//a[contains(text(),'[Create a new team]')]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//table[contains(@id,'table_teams')]/tbody/tr[td/span[contains(text(),'New York')] and td/span[contains(text(),'Knicks')] and td/span[contains(text(),'New York')] and td/span[contains(text(),'NY')] and td/a[contains(text(),'[delete]')]]"));
		verifyEquals("8", selenium.getXpathCount("//table[contains(@id,'table_teams')]/tbody//tr"));
		// On supprime toutes les teams :
		Number indexBulls = selenium.getElementIndex("//table[contains(@id,'table_teams')]/tbody/tr[td/span[contains(text(),\"Chicago\")] and td/span[contains(text(),\"Bulls\")]]");
		selenium.click("//table[contains(@id,'table_teams')]/tbody/tr[" + indexBulls + " + 1]//td//a[contains(text(),\"[delete]\")]");
		selenium.waitForPageToLoad("30000");
		Number indexGirondins = selenium.getElementIndex("//table[contains(@id,'table_teams')]/tbody/tr[td/span[contains(text(),\"Bordeaux\")] and td/span[contains(text(),\"Girondins\")]]");
		selenium.click("//table[contains(@id,'table_teams')]/tbody/tr[" + indexGirondins + " + 1]//td//a[contains(text(),\"[delete]\")]");
		selenium.waitForPageToLoad("30000");
		Number indexCavaliers = selenium.getElementIndex("//table[contains(@id,'table_teams')]/tbody/tr[td/span[contains(text(),\"Cleveland\")] and td/span[contains(text(),\"Cavaliers\")]]");
		selenium.click("//table[contains(@id,'table_teams')]/tbody/tr[" + indexCavaliers + " + 1]//td//a[contains(text(),\"[delete]\")]");
		selenium.waitForPageToLoad("30000");
		Number indexCeltics = selenium.getElementIndex("//table[contains(@id,'table_teams')]/tbody/tr[td/span[contains(text(),\"Boston\")] and td/span[contains(text(),\"Celtics\")]]");
		selenium.click("//table[contains(@id,'table_teams')]/tbody/tr[" + indexCeltics + " + 1]//td//a[contains(text(),\"[delete]\")]");
		selenium.waitForPageToLoad("30000");
		Number indexFuriousPenguins = selenium.getElementIndex("//table[contains(@id,'table_teams')]/tbody/tr[td/span[contains(text(),\"Anchorage\")] and td/span[contains(text(),\"Furious Penguins\")]]");
		selenium.click("//table[contains(@id,'table_teams')]/tbody/tr[" + indexFuriousPenguins + " + 1]//td//a[contains(text(),\"[delete]\")]");
		selenium.waitForPageToLoad("30000");
		Number indexWildMeese = selenium.getElementIndex("//table[contains(@id,'table_teams')]/tbody/tr[td/span[contains(text(),\"Juneau\")] and td/span[contains(text(),\"Wild Meese\")]]");
		selenium.click("//table[contains(@id,'table_teams')]/tbody/tr[" + indexWildMeese + " + 1]//td//a[contains(text(),\"[delete]\")]");
		selenium.waitForPageToLoad("30000");
		Number indexHeat = selenium.getElementIndex("//table[contains(@id,'table_teams')]/tbody/tr[td/span[contains(text(),\"Miami\")] and td/span[contains(text(),\"Heat\")]]");
		selenium.click("//table[contains(@id,'table_teams')]/tbody/tr[" + indexHeat + " + 1]//td//a[contains(text(),\"[delete]\")]");
		selenium.waitForPageToLoad("30000");
		Number indexKnicks = selenium.getElementIndex("//table[contains(@id,'table_teams')]/tbody/tr[td/span[contains(text(),\"New York\")] and td/span[contains(text(),\"Knicks\")]]");
		selenium.click("//table[contains(@id,'table_teams')]/tbody/tr[" + indexKnicks + " + 1]//td//a[contains(text(),\"[delete]\")]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("0", selenium.getXpathCount("//table[contains(@id,'table_teams')]/tbody//tr"));
	}


}
