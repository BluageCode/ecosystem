/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.entities.uc61_transientobject ;

// Import declaration.
// Java imports.
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.bluage.documentation.business.standardfeatures.entities.uc61_transientobject.bos.Team61BO;
import com.bluage.documentation.service.standardfeatures.entities.uc61_transientobject.ServiceTeam;
import com.bluage.documentation.service.standardfeatures.entities.uc61_transientobject.ServiceUtils;
import com.netfective.bluage.core.exception.ApplicationException;
import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;
import com.netfective.bluage.core.web.IForm;

/**
 * Class : UC61_AddTeamController
 */
@Controller
@Scope("prototype")
@SessionAttributes("uC61_AddTeamForm")
@RequestMapping(value= "/presentation/standardfeatures/entities/uc61_transientobject")
public class UC61_AddTeamController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC61_AddTeamController.class);
	
	//Current form name
	private static final String U_C61__ADD_TEAM_FORM = "uC61_AddTeamForm";

					//CONSTANT: teams
	private static final String TEAMS = "teams";
	//CONSTANT: teamToAdd
	private static final String TEAM_TO_ADD = "teamToAdd";
	
	/**
	 * Property:customDateEditorsUC61_AddTeamController
	 */
	final private Map<String,CustomDateEditor> customDateEditorsUC61_AddTeamController = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:uC61_AddTeamValidator
	 */
	final private UC61_AddTeamValidator uC61_AddTeamValidator = new UC61_AddTeamValidator();
	
	/**
	 * Service declaration : serviceUtils.
	 */
	@Autowired
	private ServiceUtils serviceUtils;
	
	/**
	 * Service declaration : serviceTeam.
	 */
	@Autowired
	private ServiceTeam serviceTeam;
	
	
	// Initialise all the instances for UC61_AddTeamController
							// Declare the instance teams
		private List teams;
		// Declare the instance teamToAdd
		private Team61BO teamToAdd;
			/**
	 * Operation : lnk_addteam
 	 * @param model : 
 	 * @param uC61_AddTeam : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC61_AddTeam/lnk_addteam.html",method = RequestMethod.POST)
	public String lnk_addteam(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC61_AddTeamForm") UC61_AddTeamForm  uC61_AddTeamForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : lnk_addteam"); 
		infoLogger(uC61_AddTeamForm); 
		uC61_AddTeamForm.setAlwaysCallPreControllers(false); 
		// initialization for lnk_addteam
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state lnk_addteam if the validation fail, you will be forward to the screen : "/presentation/standardfeatures/entities/uc61_transientobject/UC61_AddTeam"
		if(errorsMessages(request,response,uC61_AddTeamForm,bindingResult,"", uC61_AddTeamValidator, customDateEditorsUC61_AddTeamController)){ 
			return "/presentation/standardfeatures/entities/uc61_transientobject/UC61_AddTeam"; 
		}

					 callTransferList(request,uC61_AddTeamForm );
			
								destinationPath =  callAddTeam(request,uC61_AddTeamForm , model);
					if(destinationPath != null){
						return destinationPath;
					}
			
 callNewTeam(request,uC61_AddTeamForm );
						// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.entities.uc61_transientobject.UC61_AddTeamForm or not from lnk_addteam
			// Populate the destination form with all the instances returned from lnk_addteam.
			final IForm uC61_AddTeamForm2 = populateDestinationForm(request.getSession(), uC61_AddTeamForm); 
					
			// Add the form to the model.
			model.addAttribute("uC61_AddTeamForm", uC61_AddTeamForm2); 
			
			request.getSession().setAttribute("uC61_AddTeamForm", uC61_AddTeamForm2);
			
			// "OK" CASE => destination screen path from lnk_addteam
			LOGGER.info("Go to the screen 'UC61_AddTeam'.");
			// Redirect (PRG) from lnk_addteam
			destinationPath =  "redirect:/presentation/standardfeatures/entities/uc61_transientobject/UC61_AddTeam.html";
						
						
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_addteam 
	}
	
	
	/**
	* This method initialise the form : UC61_AddTeamForm 
	* @return UC61_AddTeamForm
	*/
	@ModelAttribute("UC61_AddTeamFormInit")
	public void initUC61_AddTeamForm(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, U_C61__ADD_TEAM_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method UC61_AddTeamForm."); //for lnk_addteam 
		}
		UC61_AddTeamForm uC61_AddTeamForm;
	
		if(request.getSession().getAttribute(U_C61__ADD_TEAM_FORM) != null){
			uC61_AddTeamForm = (UC61_AddTeamForm)request.getSession().getAttribute(U_C61__ADD_TEAM_FORM);
		} else {
			uC61_AddTeamForm = new UC61_AddTeamForm();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form UC61_AddTeamForm.");
		}
		uC61_AddTeamForm = (UC61_AddTeamForm)populateDestinationForm(request.getSession(), uC61_AddTeamForm);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  UC61_AddTeamForm.");
		}
		model.addAttribute(U_C61__ADD_TEAM_FORM, uC61_AddTeamForm);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param UC61_AddTeamForm : The sceen form.
	 */
	@RequestMapping(value = "/UC61_AddTeam.html" ,method = RequestMethod.GET)
	public void prepareUC61_AddTeam(final HttpServletRequest request,final  Model model,final  @ModelAttribute("UC61_AddTeamFormInit") UC61_AddTeamForm uC61_AddTeamForm){
		
		UC61_AddTeamForm currentUC61_AddTeamForm = uC61_AddTeamForm;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(U_C61__ADD_TEAM_FORM) == null){
			if(currentUC61_AddTeamForm!=null){
				request.getSession().setAttribute(U_C61__ADD_TEAM_FORM, currentUC61_AddTeamForm);
			}else {
				currentUC61_AddTeamForm = new UC61_AddTeamForm();
				request.getSession().setAttribute(U_C61__ADD_TEAM_FORM, currentUC61_AddTeamForm);	
			}
		} else {
			currentUC61_AddTeamForm = (UC61_AddTeamForm) request.getSession().getAttribute(U_C61__ADD_TEAM_FORM);
		}

					currentUC61_AddTeamForm = (UC61_AddTeamForm)populateDestinationForm(request.getSession(), currentUC61_AddTeamForm);
		// Call all the Precontroller.
	currentUC61_AddTeamForm.setAlwaysCallPreControllers(true);
		model.addAttribute(U_C61__ADD_TEAM_FORM, currentUC61_AddTeamForm);
		request.getSession().setAttribute(U_C61__ADD_TEAM_FORM, currentUC61_AddTeamForm);
		
	}
	
													/**
	 * method callAddTeam
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 * @param model Model the model
	 * @return String result NOK 
	 */
	private String callAddTeam(HttpServletRequest request,IForm form, final Model model) {
		String str = null;
		teams = (List)get(request.getSession(),form, "teams");  
										 
					teamToAdd = (Team61BO)get(request.getSession(),form, "teamToAdd");  
										 
					teams = (List)get(request.getSession(),form, "teams");  
										 
					try {
				// executing AddTeam in lnk_addteam
				teams = 	serviceTeam.addTeam(
			teamToAdd
		,			teams
			);  
 
				put(request.getSession(), TEAMS,teams);
								// processing variables AddTeam in lnk_addteam
				put(request.getSession(), TEAM_TO_ADD,teamToAdd); 
							put(request.getSession(), TEAMS,teams); 
			
			} catch (ApplicationException e) { 
				// error handling for operation addTeam called AddTeam
				errorLogger("An error occured during the execution of the operation : addTeam",e); 
		
				// "NOK" DEFAULT CASE
				UC61_ExceptionForm uC61_ExceptionForm = new UC61_ExceptionForm();
				final IForm uC61_ExceptionForm2 = populateDestinationForm(request.getSession(), uC61_ExceptionForm);
				model.addAttribute("uC61_ExceptionForm", uC61_ExceptionForm2);
				request.getSession().setAttribute("uC61_ExceptionForm", uC61_ExceptionForm2);
				request.getSession().setAttribute("messages", e.getMessage());
				return "redirect:/presentation/standardfeatures/entities/uc61_transientobject/UC61_Exception.html";
						}
		return str;
	}
		/**
	 * method callNewTeam
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callNewTeam(HttpServletRequest request,IForm form) {
		teamToAdd = (Team61BO)get(request.getSession(),form, "teamToAdd");  
										 
					try {
				// executing NewTeam in lnk_addteam
				teamToAdd = 	serviceTeam.newTeam(
	);  
 
				put(request.getSession(), TEAM_TO_ADD,teamToAdd);
								// processing variables NewTeam in lnk_addteam

			} catch (ApplicationException e) { 
				// error handling for operation newTeam called NewTeam
				errorLogger("An error occured during the execution of the operation : newTeam",e); 
		
			}
	}
					/**
	 * method callTransferList
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callTransferList(HttpServletRequest request,IForm form) {
		teams = (List)get(request.getSession(),form, "teams");  
										 
					teams = (List)get(request.getSession(),form, "teams");  
										 
					try {
				// executing TransferList in lnk_addteam
				teams = 	serviceUtils.transfertListe(
			teams
			);  
 
				put(request.getSession(), TEAMS,teams);
								// processing variables TransferList in lnk_addteam
				put(request.getSession(), TEAMS,teams); 
			
			} catch (ApplicationException e) { 
				// error handling for operation transfertListe called TransferList
				errorLogger("An error occured during the execution of the operation : transfertListe",e); 
		
			}
	}
		
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("UC61_AddTeamController [ ");
							strBToS.append(TEAMS);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(teams);
			strBToS.append(END_TO_STRING_DELIMITER);
			strBToS.append(TEAM_TO_ADD);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(teamToAdd);
			strBToS.append(END_TO_STRING_DELIMITER);
		// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(uC61_AddTeamValidator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsUC61_AddTeamController!=null); 
		return strBToS.toString();
	}
}
