<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file="/include.jsp"  %>
<html >
<head >
<title  > 
<fmt:message key="cUC72_Home_BLU_AGE_DOCUMENTATION"/>
</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"  > 
</meta>
<link href="<c:url value= "/presentation/_style/_style.css"/>" rel="stylesheet" type="text/css"  /> 
<link href="<c:url value= "/presentation/_include/jquery/rd_lib.css"/>" rel="stylesheet" type="text/css"  /> 
<script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/jquery.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/rd_setup.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/rd_lib.js"/>" space="preserve"  ></script><link rel="shortcut icon" href="<c:url value= "/presentation/_style/favicon.ico"/>"  /> 

</head>

<body class=" " > 
<div id="rd_ghost"  > 
<small  > 
<fmt:message key="cUC72_Home___CSS_DEACTIVATED"/>
</small>
</div>
<div id="rd_jghost"  > 
<small  > 
<fmt:message key="cUC72_Home___JAVASCRIPT_DEACTIVATED"/>
</small>
</div>
<div id="shape"  > 
<div class="ui-layout-center"  > 
<div class="east-north"  > 
	<form:form id="uC72_Home" commandName="uC72_HomeForm"  enctype="application/x-www-form-urlencoded" method="post">
<h2 class="rd_first"  > 
<fmt:message key="cUC72_Home_STANDARD_FEATURES__ENTITIES_"/>
</h2>
<h4  > 
<fmt:message key="cUC72_Home_MENU"/>
</h4>
<fmt:message key="cUC72_Home_________________"/>
<a id="lnk_addPlayer"				href="<c:url value="/presentation/standardfeatures/entities/uc72_generatorclass/UC72_Home/lnk_addPlayer.html"/>"
													 shape="rect"  >									 
<fmt:message key="cUC72_Home_ADD_A_PLAYER"/>
</a>
<fmt:message key="cUC72_Home__________________198"/>
<a id="lnk_addTeam"				href="<c:url value="/presentation/standardfeatures/entities/uc72_generatorclass/UC72_Home/lnk_addTeam.html"/>"
													 shape="rect"  >									 
<fmt:message key="cUC72_Home_ADD_A_TEAM"/>
</a>
<br clear="none"  /> 
<br clear="none"  /> 
	</form:form>
	<script language="javascript">
		function btn_click_cExecuteUC72_Home(url) {
			UC72_Homevar = document.getElementById('uC72_Home');
			UC72_Homevar.setAttribute('action', url);
			UC72_Homevar.submit();
		}
		function btn_click_cSetActionUC72_Home(url) {
			UC72_Homevar = document.getElementById('uC72_Home');
			UC72_Homevar.setAttribute('action', url);
		}
	</script>
</div>
<div class="east-center"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/entities/uc72_generatorclass/UC72_Homecmp_tabs" />
</div>
</div>
<div class="ui-layout-north"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/entities/uc72_generatorclass/UC72_Homecmp_header" />
</div>
<div class="ui-layout-south"  > 
<fmt:message key="cUC72_Home________"/>
<span id="rd_jversion"  > 
<fmt:message key="cUC72_Home_BLU_AGE_DOCUMENTATION_199"/>
</span>
</div>
<div class="ui-layout-west"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/entities/uc72_generatorclass/UC72_Homecmp_treeview" />
</div>
</div>
</body>
</html>
