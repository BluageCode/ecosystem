/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.service.standardfeatures.services.ajax.uc50_ajaxcall;


import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import com.bluage.documentation.business.standardfeatures.services.ajax.uc50_ajaxcall.bos.Player50BO;
import com.bluage.documentation.business.standardfeatures.services.ajax.uc50_ajaxcall.bos.Team50BO;
import com.bluage.documentation.business.standardfeatures.services.ajax.uc50_ajaxcall.entities.daofinder.Team50DAOFinderImpl;
import com.bluage.documentation.service.AbstractService;
import com.bluage.documentation.service.standardfeatures.services.ajax.uc50_ajaxcall.daofinder.ServiceAjaxCallDAOMethodFinderImpl;
import com.netfective.bluage.core.exception.ApplicationException;

/**
 * Defines  application services for ServiceAjaxCall
  */
@Named("com.bluage.documentation.service.standardfeatures.services.ajax.uc50_ajaxcall.ServiceAjaxCall")
public class ServiceAjaxCall  extends AbstractService{


			@Inject
		@Named ("com.bluage.documentation.business.standardfeatures.services.ajax.uc50_ajaxcall.entities.daofinder.Team50DAOFinderImpl")
		private Team50DAOFinderImpl team50BOFinder;
			@Inject
		@Named ("com.bluage.documentation.service.standardfeatures.services.ajax.uc50_ajaxcall.daofinder.ServiceAjaxCallDAOMethodFinder")
		private ServiceAjaxCallDAOMethodFinderImpl serviceAjaxCallDao;
			


	/**
	 *  Default constructor 
	 */
	public ServiceAjaxCall() {
		super(); // Call super for default constructor : ServiceAjaxCall
	}
	



				
	/**    
	 * Operation HQL.
	 * @param name {@link String}
		 * @param value {@link Float}
		 * @param rook {@link Boolean}
		 * @return {@link List}
	 */
	public List findPlayers50(String name,Float value,Boolean rook )
 throws ApplicationException {
	
	return serviceAjaxCallDao.findPlayers50(name,value,rook);
	}

		
			
	/**    
	 * Process operation.
	 * @param selectedTeam {@link Team50BO}
		 * @param isRookie {@link Player50BO}
		 * @param minimumValue {@link Player50BO}
		 * @return {@link List}
	 */
	public List executeFindPlayers50(Team50BO selectedTeam,Player50BO isRookie,Player50BO minimumValue )
 throws ApplicationException {

				return privateExecuteFindPlayers50(selectedTeam,isRookie,minimumValue);
	}
	

	/**    
	 * Process operation.
	 * @param selectedTeam {@link Team50BO}
		 * @param isRookie {@link Player50BO}
		 * @param minimumValue {@link Player50BO}
		 * @return {@link List}
	 */
	private List privateExecuteFindPlayers50(Team50BO selectedTeam,Player50BO isRookie,Player50BO minimumValue )
 throws ApplicationException {
										// Return Type declaration.
				List players = new java.util.ArrayList();
					// Central buffer node declaration for operation executeFindPlayers50
			String	 teamName =  "";
					Boolean	 rookie =  Boolean.FALSE;
					Float	 minValue =  Float.valueOf(0);
					Team50BO	 selTeam =  new Team50BO();
					// Calling an operation : 
selTeam = Team50FindByID(selectedTeam);
// Calling an operation : 
teamName = selTeam.getName();
// Calling an operation : 
minValue = minimumValue.getEstimatedValue();
if (minValue==null) {
/* Executing : */
minValue = Float.valueOf(0);
// Calling an operation : 
rookie = isRookie.getRookie();
// Calling an operation : 
players = findPlayers50(teamName, minValue, rookie);
} else {
// Calling an operation : 
rookie = isRookie.getRookie();
// Calling an operation : 
players = findPlayers50(teamName, minValue, rookie);
}

						return players;
		}

					
				
	/**    
	 * Operation Crud
	 * @param bo {@link Team50BO}
		 * @return {@link Team50BO}
	 */
	public Team50BO Team50FindByID(Team50BO bo )
 throws ApplicationException {

					return (Team50BO)team50BOFinder.findByPrimaryKey(getBoId(bo));
	}
	/**
	 *  Return the primary key from business object.
	 */
	private Object getBoId(Team50BO bo) {
				return bo.getId();
		}

		}
