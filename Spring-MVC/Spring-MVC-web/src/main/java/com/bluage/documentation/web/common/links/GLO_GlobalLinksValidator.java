/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.common.links ;


import org.apache.log4j.Logger;
import org.springframework.validation.Errors;

import com.netfective.bluage.core.validator.AbstractValidator;
/**
* Class :GLO_GlobalLinksValidator
*/
public class GLO_GlobalLinksValidator extends AbstractValidator{
	
	// LOGGER for the class GLO_GlobalLinksValidator
	private static final Logger LOGGER = Logger.getLogger( GLO_GlobalLinksValidator.class);
	
	
	/**
	* Operation validate for GLO_GlobalLinksForm
	* @param obj : the current form (GLO_GlobalLinksForm)
	* @param errors : The spring errors to return for the form GLO_GlobalLinksForm
	*/
	public void validate(final Object obj,final Errors errors){
		LOGGER.info("Beginning method : validate"); // GLO_GlobalLinksValidator
		GLO_GlobalLinksForm cGLO_GlobalLinksForm = (GLO_GlobalLinksForm)obj; // GLO_GlobalLinksValidator
		LOGGER.info("Ending method : validate the form "+ cGLO_GlobalLinksForm.getClass().getName()); // GLO_GlobalLinksValidator
	}

	/**
	* Method to implements to use spring validators (GLO_GlobalLinksForm)
	* @param aClass : Class for the form GLO_GlobalLinksForm
	* @return boolean
	*/
	public boolean supports(final Class aClass){
		return (new GLO_GlobalLinksForm()).getClass().equals(aClass); // GLO_GlobalLinksValidator
	}
}
