/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.entities.uc59_umltosqltypemapping ;


import org.apache.log4j.Logger;
import org.springframework.validation.Errors;

import com.netfective.bluage.core.validator.AbstractValidator;
/**
* Class :UC59_UMLtoSQLmappingValidator
*/
public class UC59_UMLtoSQLmappingValidator extends AbstractValidator{
	
	// LOGGER for the class UC59_UMLtoSQLmappingValidator
	private static final Logger LOGGER = Logger.getLogger( UC59_UMLtoSQLmappingValidator.class);
	
	
	/**
	* Operation validate for UC59_UMLtoSQLmappingForm
	* @param obj : the current form (UC59_UMLtoSQLmappingForm)
	* @param errors : The spring errors to return for the form UC59_UMLtoSQLmappingForm
	*/
	public void validate(final Object obj,final Errors errors){
		LOGGER.info("Beginning method : validate"); // UC59_UMLtoSQLmappingValidator
		UC59_UMLtoSQLmappingForm cUC59_UMLtoSQLmappingForm = (UC59_UMLtoSQLmappingForm)obj; // UC59_UMLtoSQLmappingValidator
		LOGGER.info("Ending method : validate the form "+ cUC59_UMLtoSQLmappingForm.getClass().getName()); // UC59_UMLtoSQLmappingValidator
	}

	/**
	* Method to implements to use spring validators (UC59_UMLtoSQLmappingForm)
	* @param aClass : Class for the form UC59_UMLtoSQLmappingForm
	* @return boolean
	*/
	public boolean supports(final Class aClass){
		return (new UC59_UMLtoSQLmappingForm()).getClass().equals(aClass); // UC59_UMLtoSQLmappingValidator
	}
}
