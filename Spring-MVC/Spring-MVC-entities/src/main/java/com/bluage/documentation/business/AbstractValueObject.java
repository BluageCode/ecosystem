/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.business ;


import org.apache.log4j.Logger;

import com.netfective.bluage.core.business.IBusinessObject;


/**
*
*/
public abstract class AbstractValueObject implements IBusinessObject
{
	
	public static final String DEFAULT_BYTE_PATTERN ="#";
	public static final String DEFAULT_INT_PATTERN ="#";
	public static final String DEFAULT_INTEGER_PATTERN ="#";
	public static final String DEFAULT_LONG_PATTERN="#";
	public static final String DEFAULT_SHORT_PATTERN="#";
	public static final String DEFAULT_DOUBLE_PATTERN="#.##";
	public static final String DEFAULT_FLOAT_PATTERN="#.##";
	public static final String DEFAULT_BIGDECIMAL_PATTERN="#.##";
	public static final String  DEFAULT_DECIMAL_PATTERN="#.##";
	public static final String DEFAULT_DATE_PATTERN="dd/MM/yyyy";
	public static final String DEFAULT_DATETIME_PATTERN="dd/MM/yyyy";
	public static final String DEFAULT_TIME_PATTERN="dd/MM/yyyy";
	public static final String DEFAULT_TIMESTAMP_PATTERN="dd/MM/yyyy";
	public static final String DEFAULT_BLOB_PATTERN = "";
	public static final String DEFAULT_CLOB_PATTERN = "";
	public static final String DEFAULT_CHAR_PATTERN = "";
	public static final String DEFAULT_CHARACTER_PATTERN = "";
	
	public static String objMode = IBusinessObject.DAO_MODE;

	/**
	 * HFactor
	 */
	protected static final int H_FACTOR=29;
		
	/**
	* Property:LOGGER
	*/
	protected static final Logger LOGGER = Logger.getLogger(AbstractBusinessObject.class);
	
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -4953095525064743339L;
	
	
     /**
     * Get the object mode
     *
     * @return {@link String} : IBusinessObject.DAO_MODE
     */
    public String getObjMode()
    {
        return objMode;
    }
    
     /**
     * Set the object mode
     *
     *  @param mode
     *        : {@link String} IBusinessObject.DAO_MODE
     */
     public void setObjMode(final String strValue)
    {
        this.objMode = strValue;
    }
	
}

