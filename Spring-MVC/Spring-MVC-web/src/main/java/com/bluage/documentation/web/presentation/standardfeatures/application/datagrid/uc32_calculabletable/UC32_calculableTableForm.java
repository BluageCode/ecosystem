/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.application.datagrid.uc32_calculabletable;


import java.io.Serializable;
import java.util.List;

import com.bluage.documentation.business.standardfeatures.application.datagrid.uc32_calculabletable.bos.Player32BO;
import com.netfective.bluage.core.web.AbstractForm;
import com.netfective.bluage.core.web.IForm;

/**
* Class : UC32_calculableTableForm
*/
public class UC32_calculableTableForm extends AbstractForm implements Serializable, IForm {
	
	/** Constant : serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	//CONSTANT : allPlayers
	private static final String ALL_PLAYERS = "allPlayers";
	//CONSTANT : playerToUpdate
	private static final String PLAYER_TO_UPDATE = "playerToUpdate";
	/**
	 * 	Property: allPlayers 
	 */
	private List<Player32BO> allPlayers;
	/**
	 * 	Property: playerToUpdate 
	 */
	private Player32BO playerToUpdate;
/**
	 * Default constructor : UC32_calculableTableForm
	 */
	public UC32_calculableTableForm() {
		super();
		// Initialize : allPlayers
		this.allPlayers = new java.util.ArrayList<com.bluage.documentation.business.standardfeatures.application.datagrid.uc32_calculabletable.bos.Player32BO>();
		// Initialize : playerToUpdate
		this.playerToUpdate = null;
					this.setAlwaysCallPreControllers(true);
	}
	
	/**
	 * 	Getter : allPlayers 
	 * 	@return : Return the allPlayers instance.
	 */
	public List<Player32BO> getAllPlayers(){
		return allPlayers; // For UC32_calculableTableForm
	}
	
	/**
	 * 	Setter : allPlayers 
	 *  @param allPlayersinstance : The instance to set.
	 */
	public void setAllPlayers(final List<Player32BO> allPlayersinstance){
		this.allPlayers = allPlayersinstance;// For UC32_calculableTableForm
	}
	/**
	 * 	Getter : playerToUpdate 
	 * 	@return : Return the playerToUpdate instance.
	 */
	public Player32BO getPlayerToUpdate(){
		return playerToUpdate; // For UC32_calculableTableForm
	}
	
	/**
	 * 	Setter : playerToUpdate 
	 *  @param playerToUpdateinstance : The instance to set.
	 */
	public void setPlayerToUpdate(final Player32BO playerToUpdateinstance){
		this.playerToUpdate = playerToUpdateinstance;// For UC32_calculableTableForm
	}
/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UC32_calculableTableForm [ "+
ALL_PLAYERS +" = " + allPlayers +PLAYER_TO_UPDATE +" = " + playerToUpdate + "]";
	}
	
	/**
	 * Method setInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public void setInstanceByInstanceName(final String instanceName, final Object instance) {
		// Set the instance AllPlayers.
		if(ALL_PLAYERS.equals(instanceName)){// For UC32_calculableTableForm
			this.setAllPlayers((List<Player32BO>)instance); // For UC32_calculableTableForm
		}
				// Set the instance PlayerToUpdate.
		if(PLAYER_TO_UPDATE.equals(instanceName)){// For UC32_calculableTableForm
			this.setPlayerToUpdate((Player32BO)instance); // For UC32_calculableTableForm
		}
			}
	
	/**
	 * Method getInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public Object getInstanceByInstanceName(final String instanceName){
		// Temporary object for tmpUC32_calculableTableForm.
		Object tmpUC32_calculableTableForm = null;
		
			
		// Get the instance AllPlayers for UC32_calculableTableForm.
		if(ALL_PLAYERS.equals(instanceName)){ // For UC32_calculableTableForm
			tmpUC32_calculableTableForm = this.getAllPlayers(); // For UC32_calculableTableForm
		}
			
		// Get the instance PlayerToUpdate for UC32_calculableTableForm.
		if(PLAYER_TO_UPDATE.equals(instanceName)){ // For UC32_calculableTableForm
			tmpUC32_calculableTableForm = this.getPlayerToUpdate(); // For UC32_calculableTableForm
		}
		return tmpUC32_calculableTableForm;// For UC32_calculableTableForm
	}
	
			}
