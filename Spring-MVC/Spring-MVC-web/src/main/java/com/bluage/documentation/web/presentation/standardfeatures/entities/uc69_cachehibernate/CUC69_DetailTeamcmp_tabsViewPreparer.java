/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.web.presentation.standardfeatures.entities.uc69_cachehibernate;

import org.apache.log4j.Logger;
import org.apache.tiles.AttributeContext;
import org.apache.tiles.context.TilesRequestContext;
import org.apache.tiles.preparer.ViewPreparer;

import com.bluage.documentation.web.common.components.cmp_tabsForm;
import com.netfective.bluage.core.common.IStringConstants;
/**
 * Class : CUC69_DetailTeamcmp_tabsViewPreparer
 * Prepare the component cmp_tabs for the screen UC69_DetailTeam.
 * 
 */
public class CUC69_DetailTeamcmp_tabsViewPreparer implements ViewPreparer , IStringConstants{

	private static final String UC69_DETAILTEAMCMP_TABS = "cmp_tabsComponentBean";
	
	private static final String COMPONENT_FORM_NAME = "com.bluage.documentation.web.common.components.cmp_tabsForm";
	
	private static final String COMPONENT_SHORT_FORM_NAME = "cmp_tabsForm";

	/**
	 * Property:LOGGER
	 */
	private static final Logger LOGGER = Logger.getLogger( CUC69_DetailTeamcmp_tabsViewPreparer.class);

	/**
	 * Method execute for CUC69_DetailTeamcmp_tabsViewPreparer
	 * @param tilesContext : The tiles context of the component  CUC69_DetailTeamcmp_tabsViewPreparer
	 * @param attributeContext : The attribute context of the component  CUC69_DetailTeamcmp_tabsViewPreparer
	 * @see org.apache.tiles.preparer.ViewPreparer#execute(org.apache.tiles.context.TilesRequestContext, org.apache.tiles.AttributeContext)
	 */
	public void execute(TilesRequestContext tilesContext, AttributeContext attributeContext) {
		
		LOGGER.info("Starting the component initialisation for CUC69_DetailTeamcmp_tabsViewPreparer "+ UC69_DETAILTEAMCMP_TABS +" Component Form name : "+COMPONENT_FORM_NAME);
		

		// Init the component cmp_tabsForm
		final cmp_tabsForm componentScreen = new cmp_tabsForm(); // For the form cmp_tabsForm
		
		tilesContext.getSessionScope().put(COMPONENT_SHORT_FORM_NAME, componentScreen); // For the component CUC69_DetailTeamcmp_tabsViewPreparer
		
		LOGGER.info("The component is initialized (CUC69_DetailTeamcmp_tabsViewPreparer).");
	}
}
