/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.service.standardfeatures.application.form.uc41_actionondropdownselection;


import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import com.bluage.documentation.business.standardfeatures.application.form.uc41_actionondropdownselection.bos.Position41BO;
import com.bluage.documentation.business.standardfeatures.application.form.uc41_actionondropdownselection.bos.Position41ForSelectBO;
import com.bluage.documentation.service.AbstractService;
import com.netfective.bluage.core.exception.ApplicationException;

/**
 * Defines  application services for ServicePosition41
  */
@Named("com.bluage.documentation.service.standardfeatures.application.form.uc41_actionondropdownselection.ServicePosition41")
public class ServicePosition41  extends AbstractService{


					@Inject
		@Named ("com.bluage.documentation.service.standardfeatures.application.form.uc41_actionondropdownselection.ServicePosition41FindAll")
		private ServicePosition41FindAll  instanceServicePosition41FindAll;
						


	/**
	 *  Default constructor 
	 */
	public ServicePosition41() {
		super(); // Call super for default constructor : ServicePosition41
	}
	



			
	/**    
	 * Process operation.
	 * @return {@link Position41ForSelectBO}
	 */
	public Position41ForSelectBO loadPositions41( )
 throws ApplicationException {

				return privateLoadPositions41();
	}
	

	/**    
	 * Process operation.
	 * @return {@link Position41ForSelectBO}
	 */
	private Position41ForSelectBO privateLoadPositions41( )
 throws ApplicationException {
				// Return Type declaration.
				Position41ForSelectBO positions = new Position41ForSelectBO();
					// Central buffer node declaration for operation loadPositions41
			List	 listP =  new java.util.ArrayList();
					Position41BO	 positionAll =  new Position41BO();
					Position41BO	 currentPosition =  new Position41BO();
					List	 positionsFA =  new java.util.ArrayList();
						// Calling an operation : 
positionsFA = instanceServicePosition41FindAll.position41FindAll();
/* Executing : */
positionAll = new Position41BO();
// Calling an operation : 
positionAll.setCode("ALL");
// Calling an operation : 
positionAll.setName("All positions");
/* Executing : */
listP.add(positionAll);
for (java.util.Iterator iter0 = positionsFA.iterator(); iter0.hasNext();) {
currentPosition = (com.bluage.documentation.business.standardfeatures.application.form.uc41_actionondropdownselection.bos.Position41BO)iter0.next();
/* Executing : */
listP.add(currentPosition);
}
// Calling an operation : 
positions.setAllPositions(listP);

						return positions;
		}

		}
