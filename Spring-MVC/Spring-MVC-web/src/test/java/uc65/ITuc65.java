package uc65;


import org.junit.Test;
import test.common.AbstractBADocTestCase;

public class ITuc65 extends AbstractBADocTestCase {
	
	@Override
	public  String getName(){
		return "UC-65";
	}

	@Test
	public void testUC65() throws Exception {
		// On teste l'UC65 - Standard Features/Entities - Lazy
				
		selenium.open("");	
		selenium.click("link=Expand All");
		selenium.waitForPageToLoad("30000");
		selenium.click("link=UC65 - Lazy");
		
		verifyTrue(selenium.isElementPresent("//h2[contains(text(),\"Standard Features > Entities > UC65 > Lazy\")]"));
		// On teste les donnes Teams.
		verifyEquals("10", selenium.getXpathCount("//table[contains(@id,'tab_teams')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Tigers\")] and td/span[contains(text(),\"Charlotte\")] and td/a[contains(text(),\"[View Professional Players]\")] and td/a[contains(text(),\"[View Rookie Players]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Bears\")] and td/span[contains(text(),\"Kansas City\")] and td/a[contains(text(),\"[View Professional Players]\")] and td/a[contains(text(),\"[View Rookie Players]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Chargers\")] and td/span[contains(text(),\"Detroit\")] and td/a[contains(text(),\"[View Professional Players]\")] and td/a[contains(text(),\"[View Rookie Players]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Hawks\")] and td/span[contains(text(),\"Detroit\")] and td/a[contains(text(),\"[View Professional Players]\")] and td/a[contains(text(),\"[View Rookie Players]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Braves\")] and td/span[contains(text(),\"Omaha\")] and td/a[contains(text(),\"[View Professional Players]\")] and td/a[contains(text(),\"[View Rookie Players]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Eagles\")] and td/span[contains(text(),\"Raleigh\")] and td/a[contains(text(),\"[View Professional Players]\")] and td/a[contains(text(),\"[View Rookie Players]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Cardinals\")] and td/span[contains(text(),\"Oakland\")] and td/a[contains(text(),\"[View Professional Players]\")] and td/a[contains(text(),\"[View Rookie Players]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Red Devils\")] and td/span[contains(text(),\"Kansas City\")] and td/a[contains(text(),\"[View Professional Players]\")] and td/a[contains(text(),\"[View Rookie Players]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Mustangs\")] and td/span[contains(text(),\"Fresno\")] and td/a[contains(text(),\"[View Professional Players]\")] and td/a[contains(text(),\"[View Rookie Players]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Wolverines\")] and td/span[contains(text(),\"Sacramento\")] and td/a[contains(text(),\"[View Professional Players]\")] and td/a[contains(text(),\"[View Rookie Players]\")]]"));
		// On vérifie les données Players, Rookie et Pro pour chaque Team.
		// On indexe la ligne des Tigers - Charlotte.
		Number indexTigers = selenium.getElementIndex("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Tigers\")] and td/span[contains(text(),\"Charlotte\")]]");
		System.out.println("variable index vaut : " + indexTigers);
		// On clique sur [View Professional Players]
		selenium.click("//table[contains(@id,'tab_teams')]/tbody/tr[" + indexTigers + " + 1]/td/a[contains(text(),\"[View Professional Players]\")]");
		selenium.waitForPageToLoad("30000");
		// On vérifie qu'on est sur la bonne page.
		assertTrue(selenium.isElementPresent("//span[contains(text(),\"Tigers\")]"));
		// On vérifie les joueurs pro.
		verifyEquals("3", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Oliver\")] and td/span[contains(text(),\"Frank\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Allan\")] and td/span[contains(text(),\"Meyer\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Lester\")] and td/span[contains(text(),\"Cross\")]]"));
		// On clique sur [back]
		assertEquals("[back]", selenium.getText("//a[contains(@id,'lnk_back')]"));
		selenium.click("//a[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
		// On clique sur [View Rookie Players]
		selenium.click("//table[contains(@id,'tab_teams')]/tbody/tr[" + indexTigers + " + 1]/td/a[contains(text(),\"[View Rookie Players]\")]");
		selenium.waitForPageToLoad("30000");
		// On vérifie qu'on est sur la bonne page.
		assertTrue(selenium.isElementPresent("//span[contains(text(),\"Tigers\")]"));
		// On vérifie les rookies.
		verifyEquals("2", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Darren\")] and td/span[contains(text(),\"Wolfe\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Derrick\")] and td/span[contains(text(),\"Holt\")]]"));
		// On clique sur [back]
		selenium.click("//a[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
		// On indexe la ligne des Bears - Kansas City.
		Number indexBears = selenium.getElementIndex("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Bears\")] and td/span[contains(text(),\"Kansas City\")]]");
		System.out.println("variable index vaut : " + indexBears);
		// On clique sur [View Professional Players]
		selenium.click("//table[contains(@id,'tab_teams')]/tbody/tr[" + indexBears + " + 1]/td/a[contains(text(),\"[View Professional Players]\")]");
		selenium.waitForPageToLoad("30000");
		// On vérifie qu'on est sur la bonne page.
		assertTrue(selenium.isElementPresent("//span[contains(text(),\"Bears\")]"));
		// On vérifie les joueurs pro.
		verifyEquals("3", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Elbert\")] and td/span[contains(text(),\"Cole\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Keith\")] and td/span[contains(text(),\"Barnett\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Roderick\")] and td/span[contains(text(),\"Robbins\")]]"));
		// On clique sur [back]
		selenium.click("//a[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
		// On clique sur [View Rookie Players]
		selenium.click("//table[contains(@id,'tab_teams')]/tbody/tr[" + indexBears + " + 1]/td/a[contains(text(),\"[View Rookie Players]\")]");
		selenium.waitForPageToLoad("30000");
		// On vérifie qu'on est sur la bonne page.
		assertTrue(selenium.isElementPresent("//span[contains(text(),\"Bears\")]"));
		// On vérifie les rookies.
		verifyEquals("2", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Michael\")] and td/span[contains(text(),\"Chambers\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Clayton\")] and td/span[contains(text(),\"Bishop\")]]"));
		// On clique sur [back]
		selenium.click("//a[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
		// On indexe la ligne des Chargers - Detroit.
		Number indexChargers = selenium.getElementIndex("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Chargers\")] and td/span[contains(text(),\"Detroit\")]]");
		System.out.println("variable index vaut : " + indexChargers);
		// On clique sur [View Professional Players]
		selenium.click("//table[contains(@id,'tab_teams')]/tbody/tr[" + indexChargers + " + 1]/td/a[contains(text(),\"[View Professional Players]\")]");
		selenium.waitForPageToLoad("30000");
		// On vérifie qu'on est sur la bonne page.
		assertTrue(selenium.isElementPresent("//span[contains(text(),\"Chargers\")]"));
		// On vérifie les joueurs pro.
		verifyEquals("3", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Abel\")] and td/span[contains(text(),\"Dennis\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Howard\")] and td/span[contains(text(),\"Ramos\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Ivan\")] and td/span[contains(text(),\"Tate\")]]"));
		// On clique sur [back]
		selenium.click("//a[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
		// On clique sur [View Rookie Players]
		selenium.click("//table[contains(@id,'tab_teams')]/tbody/tr[" + indexChargers + " + 1]/td/a[contains(text(),\"[View Rookie Players]\")]");
		selenium.waitForPageToLoad("30000");
		// On vérifie qu'on est sur la bonne page.
		assertTrue(selenium.isElementPresent("//span[contains(text(),\"Chargers\")]"));
		// On vérifie les rookies.
		verifyEquals("2", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Gilbert\")] and td/span[contains(text(),\"Mckinney\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Jean\")] and td/span[contains(text(),\"Walton\")]]"));
		// On clique sur [back]
		selenium.click("//a[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
		// On indexe la ligne des Hawks - Detroit.
		Number indexHawks = selenium.getElementIndex("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Hawks\")] and td/span[contains(text(),\"Detroit\")]]");
		System.out.println("variable index vaut : " + indexHawks);
		// On clique sur [View Professional Players]
		selenium.click("//table[contains(@id,'tab_teams')]/tbody/tr[" + indexHawks + " + 1]/td/a[contains(text(),\"[View Professional Players]\")]");
		selenium.waitForPageToLoad("30000");
		// On vérifie qu'on est sur la bonne page.
		assertTrue(selenium.isElementPresent("//span[contains(text(),\"Hawks\")]"));
		// On vérifie les joueurs pro.
		verifyEquals("2", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Ricky\")] and td/span[contains(text(),\"Dawson\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Tommy\")] and td/span[contains(text(),\"Wood\")]]"));
		// On clique sur [back]
		selenium.click("//a[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
		// On clique sur [View Rookie Players]
		selenium.click("//table[contains(@id,'tab_teams')]/tbody/tr[" + indexHawks + " + 1]/td/a[contains(text(),\"[View Rookie Players]\")]");
		selenium.waitForPageToLoad("30000");
		// On vérifie qu'on est sur la bonne page.
		assertTrue(selenium.isElementPresent("//span[contains(text(),\"Hawks\")]"));
		// On vérifie les rookies.
		verifyEquals("2", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Lamar\")] and td/span[contains(text(),\"Hansen\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Nelson\")] and td/span[contains(text(),\"Hernandez\")]]"));
		// On clique sur [back]
		selenium.click("//a[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
		// On indexe la ligne des Braves - Omaha.
		Number indexBraves = selenium.getElementIndex("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Braves\")] and td/span[contains(text(),\"Omaha\")]]");
		System.out.println("variable index vaut : " + indexBraves);
		// On clique sur [View Professional Players]
		selenium.click("//table[contains(@id,'tab_teams')]/tbody/tr[" + indexBraves + " + 1]/td/a[contains(text(),\"[View Professional Players]\")]");
		selenium.waitForPageToLoad("30000");
		// On vérifie qu'on est sur la bonne page.
		assertTrue(selenium.isElementPresent("//span[contains(text(),\"Braves\")]"));
		// On vérifie les joueurs pro.
		verifyEquals("3", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Earl\")] and td/span[contains(text(),\"Russell\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Ruben\")] and td/span[contains(text(),\"Pope\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Sylvester\")] and td/span[contains(text(),\"Nash\")]]"));
		// On clique sur [back]
		selenium.click("//a[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
		// On clique sur [View Rookie Players]
		selenium.click("//table[contains(@id,'tab_teams')]/tbody/tr[" + indexBraves + " + 1]/td/a[contains(text(),\"[View Rookie Players]\")]");
		selenium.waitForPageToLoad("30000");
		// On vérifie qu'on est sur la bonne page.
		assertTrue(selenium.isElementPresent("//span[contains(text(),\"Braves\")]"));
		// On vérifie les rookies.
		verifyEquals("2", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Michael\")] and td/span[contains(text(),\"Franklin\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Lucas\")] and td/span[contains(text(),\"Obrien\")]]"));
		// On clique sur [back]
		selenium.click("//a[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
		// On indexe la ligne des Eagles - Raleigh.
		Number indexEagles = selenium.getElementIndex("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Eagles\")] and td/span[contains(text(),\"Raleigh\")]]");
		System.out.println("variable index vaut : " + indexEagles);
		// On clique sur [View Professional Players]
		selenium.click("//table[contains(@id,'tab_teams')]/tbody/tr[" + indexEagles + " + 1]/td/a[contains(text(),\"[View Professional Players]\")]");
		selenium.waitForPageToLoad("30000");
		// On vérifie qu'on est sur la bonne page.
		assertTrue(selenium.isElementPresent("//span[contains(text(),\"Eagles\")]"));
		// On vérifie les joueurs pro.
		verifyEquals("1", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Michael\")] and td/span[contains(text(),\"Hodges\")]]"));
		// On clique sur [back]
		selenium.click("//a[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
		// On clique sur [View Rookie Players]
		selenium.click("//table[contains(@id,'tab_teams')]/tbody/tr[" + indexEagles + " + 1]/td/a[contains(text(),\"[View Rookie Players]\")]");
		selenium.waitForPageToLoad("30000");
		// On vérifie qu'on est sur la bonne page.
		assertTrue(selenium.isElementPresent("//span[contains(text(),\"Eagles\")]"));
		// On vérifie les rookies.
		verifyEquals("2", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Norman\")] and td/span[contains(text(),\"Osborne\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Dominick\")] and td/span[contains(text(),\"Burton\")]]"));
		// On clique sur [back]
		selenium.click("//a[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
		// On indexe la ligne des Cardinals - Oakland.
		Number indexCardinals = selenium.getElementIndex("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Cardinals\")] and td/span[contains(text(),\"Oakland\")]]");
		System.out.println("variable index vaut : " + indexCardinals);
		// On clique sur [View Professional Players]
		selenium.click("//table[contains(@id,'tab_teams')]/tbody/tr[" + indexCardinals + " + 1]/td/a[contains(text(),\"[View Professional Players]\")]");
		selenium.waitForPageToLoad("30000");
		// On vérifie qu'on est sur la bonne page.
		assertTrue(selenium.isElementPresent("//span[contains(text(),\"Cardinals\")]"));
		// On vérifie les joueurs pro.
		verifyEquals("1", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Jerome\")] and td/span[contains(text(),\"Grant\")]]"));
		// On clique sur [back]
		selenium.click("//a[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
		// On clique sur [View Rookie Players]
		selenium.click("//table[contains(@id,'tab_teams')]/tbody/tr[" + indexCardinals + " + 1]/td/a[contains(text(),\"[View Rookie Players]\")]");
		selenium.waitForPageToLoad("30000");
		// On vérifie qu'on est sur la bonne page.
		assertTrue(selenium.isElementPresent("//span[contains(text(),\"Cardinals\")]"));
		// On vérifie les rookies.
		verifyEquals("1", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Damon\")] and td/span[contains(text(),\"Richardson\")]]"));
		// On clique sur [back]
		selenium.click("//a[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
		// On indexe la ligne des Red Devils - Kansas City.
		Number indexRedDevils = selenium.getElementIndex("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Red Devils\")] and td/span[contains(text(),\"Kansas City\")]]");
		System.out.println("variable index vaut : " + indexRedDevils);
		// On clique sur [View Professional Players]
		selenium.click("//table[contains(@id,'tab_teams')]/tbody/tr[" + indexRedDevils + " + 1]/td/a[contains(text(),\"[View Professional Players]\")]");
		selenium.waitForPageToLoad("30000");
		// On vérifie qu'on est sur la bonne page.
		assertTrue(selenium.isElementPresent("//span[contains(text(),\"Red Devils\")]"));
		// On vérifie les joueurs pro.
		verifyEquals("1", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Woodrow\")] and td/span[contains(text(),\"Carpenter\")]]"));
		// On clique sur [back]
		selenium.click("//a[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
		// On clique sur [View Rookie Players]
		selenium.click("//table[contains(@id,'tab_teams')]/tbody/tr[" + indexRedDevils + " + 1]/td/a[contains(text(),\"[View Rookie Players]\")]");
		selenium.waitForPageToLoad("30000");
		// On vérifie qu'on est sur la bonne page.
		assertTrue(selenium.isElementPresent("//span[contains(text(),\"Red Devils\")]"));
		// On vérifie les rookies.
		verifyEquals("1", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Grant\")] and td/span[contains(text(),\"Gardner\")]]"));
		// On clique sur [back]
		selenium.click("//a[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
		// On indexe la ligne des Mustangs - Fresno.
		Number indexMustangs = selenium.getElementIndex("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Mustangs\")] and td/span[contains(text(),\"Fresno\")]]");
		System.out.println("variable index vaut : " + indexMustangs);
		// On clique sur [View Professional Players]
		selenium.click("//table[contains(@id,'tab_teams')]/tbody/tr[" + indexMustangs + " + 1]/td/a[contains(text(),\"[View Professional Players]\")]");
		selenium.waitForPageToLoad("30000");
		// On vérifie qu'on est sur la bonne page.
		assertTrue(selenium.isElementPresent("//span[contains(text(),\"Mustangs\")]"));
		// On vérifie les joueurs pro.
		verifyEquals("1", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Christian\")] and td/span[contains(text(),\"Chavez\")]]"));
		// On clique sur [back]
		selenium.click("//a[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
		// On clique sur [View Rookie Players]
		selenium.click("//table[contains(@id,'tab_teams')]/tbody/tr[" + indexMustangs + " + 1]/td/a[contains(text(),\"[View Rookie Players]\")]");
		selenium.waitForPageToLoad("30000");
		// On vérifie qu'on est sur la bonne page.
		assertTrue(selenium.isElementPresent("//span[contains(text(),\"Mustangs\")]"));
		// On vérifie les rookies.
		verifyEquals("1", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Garrett\")] and td/span[contains(text(),\"Townsend\")]]"));
		// On clique sur [back]
		selenium.click("//a[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
		// On indexe la ligne des Wolverines - Sacramento.
		Number indexWolverines = selenium.getElementIndex("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Wolverines\")] and td/span[contains(text(),\"Sacramento\")]]");
		System.out.println("variable index vaut : " + indexWolverines);
		// On clique sur [View Professional Players]
		selenium.click("//table[contains(@id,'tab_teams')]/tbody/tr[" + indexWolverines + " + 1]/td/a[contains(text(),\"[View Professional Players]\")]");
		selenium.waitForPageToLoad("30000");
		// On vérifie qu'on est sur la bonne page.
		assertTrue(selenium.isElementPresent("//span[contains(text(),\"Wolverines\")]"));
		// On vérifie les joueurs pro.
		verifyEquals("1", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Nick\")] and td/span[contains(text(),\"Garner\")]]"));
		// On clique sur [back]
		selenium.click("//a[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
		// On clique sur [View Rookie Players]
		selenium.click("//table[contains(@id,'tab_teams')]/tbody/tr[" + indexWolverines + " + 1]/td/a[contains(text(),\"[View Rookie Players]\")]");
		selenium.waitForPageToLoad("30000");
		// On vérifie qu'on est sur la bonne page.
		assertTrue(selenium.isElementPresent("//span[contains(text(),\"Wolverines\")]"));
		// On vérifie les rookies.
		verifyEquals("1", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Bryant\")] and td/span[contains(text(),\"Hodges\")]]"));
		// On clique sur [back]
		selenium.click("//a[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
		// On vérifie qu'on est sur la page des Teams.
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]"));
	}

	
}
