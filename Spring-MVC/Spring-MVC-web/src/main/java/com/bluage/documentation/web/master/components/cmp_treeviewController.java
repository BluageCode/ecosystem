/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.master.components ;

// Import declaration.
// Java imports.
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.bluage.documentation.business.standardfeatures.application.component.uc47_contents.bos.Screen47BO;
import com.bluage.documentation.business.standardfeatures.application.form.uc41_actionondropdownselection.bos.Position41ForSelectBO;
import com.bluage.documentation.service.advancedfeatures.advancedtagsuse.uc205_popup.ServicePlayer205;
import com.bluage.documentation.service.common.utility.ServiceUtility;
import com.bluage.documentation.service.gettingstarted.uc02_entitiescreation.ServiceTeam02;
import com.bluage.documentation.service.standardfeatures.application.component.uc47_contents.ServicePlayer47;
import com.bluage.documentation.service.standardfeatures.application.component.uc47_contents.ServiceScreen47;
import com.bluage.documentation.service.standardfeatures.application.form.uc41_actionondropdownselection.ServicePosition41;
import com.bluage.documentation.service.standardfeatures.application.navigation.uc28_packagescope.ServiceTeam28;
import com.bluage.documentation.service.standardfeatures.application.navigation.uc29_globalscope.ServiceTeam29;
import com.bluage.documentation.service.standardfeatures.entities.uc69_cachehibernate.ServiceTeam;
import com.bluage.documentation.web.presentation.advancedfeatures.advancedtagsuse.uc205_popup.UC205_HomeForm;
import com.bluage.documentation.web.presentation.advancedfeatures.externaltools.uc209_javamelody.UC209_HomePageForm;
import com.bluage.documentation.web.presentation.advancedfeatures.externaltools.uc210_sonar.UC210_HomePageForm;
import com.bluage.documentation.web.presentation.advancedfeatures.filters.uc220_timeout.UC220_TimeOutForm;
import com.bluage.documentation.web.presentation.advancedfeatures.performance.uc200_datagridloading.UC200_DatagridLoadingForm;
import com.bluage.documentation.web.presentation.advancedfeatures.valueobject.uc206_valueobject.UC206_HomePageForm;
import com.bluage.documentation.web.presentation.common.home.homeForm;
import com.bluage.documentation.web.presentation.gettingstarted.uc01_helloworld.UC01_HelloWorldForm;
import com.bluage.documentation.web.presentation.gettingstarted.uc02_entitiescreation.UC02_EntitiesCreationForm;
import com.bluage.documentation.web.presentation.gettingstarted.uc03_formvalidation.UC03_FormValidationForm;
import com.bluage.documentation.web.presentation.gettingstarted.uc04_entitiesmodification.UC04_EntitiesModificationForm;
import com.bluage.documentation.web.presentation.gettingstarted.uc05_search.UC05_SearchForm;
import com.bluage.documentation.web.presentation.qauc.uc1001_tables.UC1001_TablesForm;
import com.bluage.documentation.web.presentation.qauc.uc1002_validators.UC1002_ValidatorsForm;
import com.bluage.documentation.web.presentation.standardfeatures.application.component.uc46_menu.UC46_Page1Form;
import com.bluage.documentation.web.presentation.standardfeatures.application.component.uc47_contents.UC47_ContentsForm;
import com.bluage.documentation.web.presentation.standardfeatures.application.datagrid.uc32_calculabletable.UC32_calculableTableForm;
import com.bluage.documentation.web.presentation.standardfeatures.application.datagrid.uc33_editabletable.UC33_editableTableForm;
import com.bluage.documentation.web.presentation.standardfeatures.application.datagrid.uc34_paginationtable.UC34_PaginationTableForm;
import com.bluage.documentation.web.presentation.standardfeatures.application.datagrid.uc35_paginatorbdd.UC35_HomePageForm;
import com.bluage.documentation.web.presentation.standardfeatures.application.datagrid.uc37_collectionorder.UC37_TeamsForm;
import com.bluage.documentation.web.presentation.standardfeatures.application.datagrid.uc38_selectedlist.UC38_SelectedListForm;
import com.bluage.documentation.web.presentation.standardfeatures.application.form.uc41_actionondropdownselection.UC41_DropDownActionForm;
import com.bluage.documentation.web.presentation.standardfeatures.application.form.uc43_captcha.UC43_AddPlayerForm;
import com.bluage.documentation.web.presentation.standardfeatures.application.form.uc44_autocompletetable.UC44_HomePageForm;
import com.bluage.documentation.web.presentation.standardfeatures.application.form.uc45_autocomplete.UC45_HomePageForm;
import com.bluage.documentation.web.presentation.standardfeatures.application.navigation.uc28_packagescope.package_one.UC28_TeamsForm;
import com.bluage.documentation.web.presentation.standardfeatures.application.navigation.uc29_globalscope.package_one.UC29_TeamsForm;
import com.bluage.documentation.web.presentation.standardfeatures.application.navigation.uc30_navigation.UC30_Page1Form;
import com.bluage.documentation.web.presentation.standardfeatures.application.navigation.uc31_preaction.UC31_HomePageForm;
import com.bluage.documentation.web.presentation.standardfeatures.application.repeater.uc39_repeater.UC39_HomePageForm;
import com.bluage.documentation.web.presentation.standardfeatures.entities.uc40_defaultsort.UC40_DefaultSortForm;
import com.bluage.documentation.web.presentation.standardfeatures.entities.uc59_umltosqltypemapping.UC59_UMLtoSQLmappingForm;
import com.bluage.documentation.web.presentation.standardfeatures.entities.uc60_multidomain.UC60_MultiDomainForm;
import com.bluage.documentation.web.presentation.standardfeatures.entities.uc61_transientobject.UC61_AddTeamForm;
import com.bluage.documentation.web.presentation.standardfeatures.entities.uc63_association.UC63_HomeForm;
import com.bluage.documentation.web.presentation.standardfeatures.entities.uc64_cascade.UC64_HomePageForm;
import com.bluage.documentation.web.presentation.standardfeatures.entities.uc65_lazy.UC65_HomePageForm;
import com.bluage.documentation.web.presentation.standardfeatures.entities.uc68_heritagesubclass.UC68_HeritageSubclassForm;
import com.bluage.documentation.web.presentation.standardfeatures.entities.uc69_cachehibernate.UC69_HomePageForm;
import com.bluage.documentation.web.presentation.standardfeatures.entities.uc72_generatorclass.UC72_HomeForm;
import com.bluage.documentation.web.presentation.standardfeatures.exception.uc73_exception.UC73_ManagePlayersForm;
import com.bluage.documentation.web.presentation.standardfeatures.services.ajax.uc50_ajaxcall.UC50_AjaxCallForm;
import com.bluage.documentation.web.presentation.standardfeatures.services.bags.uc49_bags.UC49_BAGSForm;
import com.bluage.documentation.web.presentation.standardfeatures.services.hql.uc57_hqloperation.UC57_SearchForm;
import com.bluage.documentation.web.presentation.standardfeatures.services.process.uc52_conditional.UC52_ConditionalForm;
import com.bluage.documentation.web.presentation.standardfeatures.services.process.uc53_while.UC53_WhileForm;
import com.bluage.documentation.web.presentation.standardfeatures.services.process.uc54_for.UC54_ForForm;
import com.bluage.documentation.web.presentation.standardfeatures.services.sql.uc56_sqloperation.UC56_SearchForm;
import com.bluage.documentation.web.presentation.standardfeatures.services.standard.uc58_servicecopy.UC58_DisplayTeams58Form;
import com.netfective.bluage.core.exception.ApplicationException;
import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;
import com.netfective.bluage.core.web.IForm;

/**
 * Class : cmp_treeviewController
 */
@Controller
@Scope("prototype")
@SessionAttributes("cmp_treeviewForm")
@RequestMapping(value= "/master/components")
public class cmp_treeviewController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(cmp_treeviewController.class);
	
	//Current form name
	private static final String CMP_TREEVIEW_FORM = "cmp_treeviewForm";

	//CONSTANT: allTeams
	private static final String ALL_TEAMS = "allTeams";
	//CONSTANT: positions
	private static final String POSITIONS = "positions";
	//CONSTANT: screen47
	private static final String SCREEN47 = "screen47";
	//CONSTANT: allPlayers
	private static final String ALL_PLAYERS = "allPlayers";
	
	/**
	 * Property:customDateEditorscmp_treeviewController
	 */
	final private Map<String,CustomDateEditor> customDateEditorscmp_treeviewController = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:cmp_treeviewValidator
	 */
	final private cmp_treeviewValidator cmp_treeviewValidator = new cmp_treeviewValidator();
	
	/**
	 * Service declaration : servicePosition41.
	 */
	@Autowired
	private ServicePosition41 servicePosition41;
	
	/**
	 * Service declaration : serviceTeam02.
	 */
	@Autowired
	private ServiceTeam02 serviceTeam02;
	
	/**
	 * Service declaration : serviceScreen47.
	 */
	@Autowired
	private ServiceScreen47 serviceScreen47;
	
	/**
	 * Service declaration : servicePlayer47.
	 */
	@Autowired
	private ServicePlayer47 servicePlayer47;
	
	/**
	 * Service declaration : serviceUtility.
	 */
	@Autowired
	private ServiceUtility serviceUtility;
	
	/**
	 * Service declaration : serviceTeam29.
	 */
	@Autowired
	private ServiceTeam29 serviceTeam29;
	
	/**
	 * Service declaration : servicePlayer205.
	 */
	@Autowired
	private ServicePlayer205 servicePlayer205;
	
	/**
	 * Service declaration : serviceTeam28.
	 */
	@Autowired
	private ServiceTeam28 serviceTeam28;
	
	/**
	 * Service declaration : serviceTeam.
	 */
	@Autowired
	private ServiceTeam serviceTeam;
	
	
	// Initialise all the instances for cmp_treeviewController
		// Declare the instance allTeams
		private List allTeams;
		// Declare the instance positions
		private Position41ForSelectBO positions;
		// Declare the instance screen47
		private Screen47BO screen47;
		// Declare the instance allPlayers
		private List allPlayers;
			/**
	 * Operation : howto_helloworld
 	 * @param model : 
 	 * @param cmp_treeview : The form
 	 * @return
	 */
	@RequestMapping(value = "/cmp_treeview/howto_helloworld.html",method = RequestMethod.POST)
	public String howto_helloworld(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("cmp_treeviewForm") cmp_treeviewForm  cmp_treeviewForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : howto_helloworld"); 
		infoLogger(cmp_treeviewForm); 
		cmp_treeviewForm.setAlwaysCallPreControllers(false); 
		// initialization for howto_helloworld
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state howto_helloworld if the validation fail, you will be forward to the screen : "/master/components/cmp_treeview"
		if(errorsMessages(request,response,cmp_treeviewForm,bindingResult,"", cmp_treeviewValidator, customDateEditorscmp_treeviewController)){ 
			return "/master/components/cmp_treeview"; 
		}

	 callGotohelloworld(request,cmp_treeviewForm );
											// Init the destination form com.bluage.documentation.web.presentation.gettingstarted.uc01_helloworld.UC01_HelloWorldForm or not from howto_helloworld
			final UC01_HelloWorldForm uC01_HelloWorldForm =  new UC01_HelloWorldForm();
			// Populate the destination form with all the instances returned from howto_helloworld.
			final IForm uC01_HelloWorldForm2 = populateDestinationForm(request.getSession(), uC01_HelloWorldForm); 
					
			// Add the form to the model.
			model.addAttribute("uC01_HelloWorldForm", uC01_HelloWorldForm2); 
			
			request.getSession().setAttribute("uC01_HelloWorldForm", uC01_HelloWorldForm2);
			
			// "OK" CASE => destination screen path from howto_helloworld
			LOGGER.info("Go to the screen 'UC01_HelloWorld'.");
			// Redirect (PRG) from howto_helloworld
			destinationPath =  "redirect:/presentation/gettingstarted/uc01_helloworld/UC01_HelloWorld.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : howto_helloworld 
	}
	
				/**
	 * Operation : howto_validationform
 	 * @param model : 
 	 * @param cmp_treeview : The form
 	 * @return
	 */
	@RequestMapping(value = "/cmp_treeview/howto_validationform.html",method = RequestMethod.POST)
	public String howto_validationform(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("cmp_treeviewForm") cmp_treeviewForm  cmp_treeviewForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : howto_validationform"); 
		infoLogger(cmp_treeviewForm); 
		cmp_treeviewForm.setAlwaysCallPreControllers(false); 
		// initialization for howto_validationform
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state howto_validationform if the validation fail, you will be forward to the screen : "/master/components/cmp_treeview"
		if(errorsMessages(request,response,cmp_treeviewForm,bindingResult,"", cmp_treeviewValidator, customDateEditorscmp_treeviewController)){ 
			return "/master/components/cmp_treeview"; 
		}

	 callGotoFormValidation(request,cmp_treeviewForm );
											// Init the destination form com.bluage.documentation.web.presentation.gettingstarted.uc03_formvalidation.UC03_FormValidationForm or not from howto_validationform
			final UC03_FormValidationForm uC03_FormValidationForm =  new UC03_FormValidationForm();
			// Populate the destination form with all the instances returned from howto_validationform.
			final IForm uC03_FormValidationForm2 = populateDestinationForm(request.getSession(), uC03_FormValidationForm); 
					
			// Add the form to the model.
			model.addAttribute("uC03_FormValidationForm", uC03_FormValidationForm2); 
			
			request.getSession().setAttribute("uC03_FormValidationForm", uC03_FormValidationForm2);
			
			// "OK" CASE => destination screen path from howto_validationform
			LOGGER.info("Go to the screen 'UC03_FormValidation'.");
			// Redirect (PRG) from howto_validationform
			destinationPath =  "redirect:/presentation/gettingstarted/uc03_formvalidation/UC03_FormValidation.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : howto_validationform 
	}
	
				/**
	 * Operation : howto_entitiesmodification
 	 * @param model : 
 	 * @param cmp_treeview : The form
 	 * @return
	 */
	@RequestMapping(value = "/cmp_treeview/howto_entitiesmodification.html",method = RequestMethod.POST)
	public String howto_entitiesmodification(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("cmp_treeviewForm") cmp_treeviewForm  cmp_treeviewForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : howto_entitiesmodification"); 
		infoLogger(cmp_treeviewForm); 
		cmp_treeviewForm.setAlwaysCallPreControllers(false); 
		// initialization for howto_entitiesmodification
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state howto_entitiesmodification if the validation fail, you will be forward to the screen : "/master/components/cmp_treeview"
		if(errorsMessages(request,response,cmp_treeviewForm,bindingResult,"", cmp_treeviewValidator, customDateEditorscmp_treeviewController)){ 
			return "/master/components/cmp_treeview"; 
		}

	 callGotoEntitiesModification(request,cmp_treeviewForm );
											// Init the destination form com.bluage.documentation.web.presentation.gettingstarted.uc04_entitiesmodification.UC04_EntitiesModificationForm or not from howto_entitiesmodification
			final UC04_EntitiesModificationForm uC04_EntitiesModificationForm =  new UC04_EntitiesModificationForm();
			// Populate the destination form with all the instances returned from howto_entitiesmodification.
			final IForm uC04_EntitiesModificationForm2 = populateDestinationForm(request.getSession(), uC04_EntitiesModificationForm); 
					
			// Add the form to the model.
			model.addAttribute("uC04_EntitiesModificationForm", uC04_EntitiesModificationForm2); 
			
			request.getSession().setAttribute("uC04_EntitiesModificationForm", uC04_EntitiesModificationForm2);
			
			// "OK" CASE => destination screen path from howto_entitiesmodification
			LOGGER.info("Go to the screen 'UC04_EntitiesModification'.");
			// Redirect (PRG) from howto_entitiesmodification
			destinationPath =  "redirect:/presentation/gettingstarted/uc04_entitiesmodification/UC04_EntitiesModification.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : howto_entitiesmodification 
	}
	
				/**
	 * Operation : howto_search
 	 * @param model : 
 	 * @param cmp_treeview : The form
 	 * @return
	 */
	@RequestMapping(value = "/cmp_treeview/howto_search.html",method = RequestMethod.POST)
	public String howto_search(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("cmp_treeviewForm") cmp_treeviewForm  cmp_treeviewForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : howto_search"); 
		infoLogger(cmp_treeviewForm); 
		cmp_treeviewForm.setAlwaysCallPreControllers(false); 
		// initialization for howto_search
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state howto_search if the validation fail, you will be forward to the screen : "/master/components/cmp_treeview"
		if(errorsMessages(request,response,cmp_treeviewForm,bindingResult,"", cmp_treeviewValidator, customDateEditorscmp_treeviewController)){ 
			return "/master/components/cmp_treeview"; 
		}

	 callGotoSearch(request,cmp_treeviewForm );
											// Init the destination form com.bluage.documentation.web.presentation.gettingstarted.uc05_search.UC05_SearchForm or not from howto_search
			final UC05_SearchForm uC05_SearchForm =  new UC05_SearchForm();
			// Populate the destination form with all the instances returned from howto_search.
			final IForm uC05_SearchForm2 = populateDestinationForm(request.getSession(), uC05_SearchForm); 
					
			// Add the form to the model.
			model.addAttribute("uC05_SearchForm", uC05_SearchForm2); 
			
			request.getSession().setAttribute("uC05_SearchForm", uC05_SearchForm2);
			
			// "OK" CASE => destination screen path from howto_search
			LOGGER.info("Go to the screen 'UC05_Search'.");
			// Redirect (PRG) from howto_search
			destinationPath =  "redirect:/presentation/gettingstarted/uc05_search/UC05_Search.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : howto_search 
	}
	
				/**
	 * Operation : howto_navigation
 	 * @param model : 
 	 * @param cmp_treeview : The form
 	 * @return
	 */
	@RequestMapping(value = "/cmp_treeview/howto_navigation.html",method = RequestMethod.POST)
	public String howto_navigation(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("cmp_treeviewForm") cmp_treeviewForm  cmp_treeviewForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : howto_navigation"); 
		infoLogger(cmp_treeviewForm); 
		cmp_treeviewForm.setAlwaysCallPreControllers(false); 
		// initialization for howto_navigation
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state howto_navigation if the validation fail, you will be forward to the screen : "/master/components/cmp_treeview"
		if(errorsMessages(request,response,cmp_treeviewForm,bindingResult,"", cmp_treeviewValidator, customDateEditorscmp_treeviewController)){ 
			return "/master/components/cmp_treeview"; 
		}

	 callGotoNavigation(request,cmp_treeviewForm );
											// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.application.navigation.uc30_navigation.UC30_Page1Form or not from howto_navigation
			final UC30_Page1Form uC30_Page1Form =  new UC30_Page1Form();
			// Populate the destination form with all the instances returned from howto_navigation.
			final IForm uC30_Page1Form2 = populateDestinationForm(request.getSession(), uC30_Page1Form); 
					
			// Add the form to the model.
			model.addAttribute("uC30_Page1Form", uC30_Page1Form2); 
			
			request.getSession().setAttribute("uC30_Page1Form", uC30_Page1Form2);
			
			// "OK" CASE => destination screen path from howto_navigation
			LOGGER.info("Go to the screen 'UC30_Page1'.");
			// Redirect (PRG) from howto_navigation
			destinationPath =  "redirect:/presentation/standardfeatures/application/navigation/uc30_navigation/UC30_Page1.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : howto_navigation 
	}
	
				/**
	 * Operation : howto_edittable
 	 * @param model : 
 	 * @param cmp_treeview : The form
 	 * @return
	 */
	@RequestMapping(value = "/cmp_treeview/howto_edittable.html",method = RequestMethod.POST)
	public String howto_edittable(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("cmp_treeviewForm") cmp_treeviewForm  cmp_treeviewForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : howto_edittable"); 
		infoLogger(cmp_treeviewForm); 
		cmp_treeviewForm.setAlwaysCallPreControllers(false); 
		// initialization for howto_edittable
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state howto_edittable if the validation fail, you will be forward to the screen : "/master/components/cmp_treeview"
		if(errorsMessages(request,response,cmp_treeviewForm,bindingResult,"", cmp_treeviewValidator, customDateEditorscmp_treeviewController)){ 
			return "/master/components/cmp_treeview"; 
		}

	 callGotoEditeTable(request,cmp_treeviewForm );
											// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.application.datagrid.uc33_editabletable.UC33_editableTableForm or not from howto_edittable
			final UC33_editableTableForm uC33_editableTableForm =  new UC33_editableTableForm();
			// Populate the destination form with all the instances returned from howto_edittable.
			final IForm uC33_editableTableForm2 = populateDestinationForm(request.getSession(), uC33_editableTableForm); 
					
			// Add the form to the model.
			model.addAttribute("uC33_editableTableForm", uC33_editableTableForm2); 
			
			request.getSession().setAttribute("uC33_editableTableForm", uC33_editableTableForm2);
			
			// "OK" CASE => destination screen path from howto_edittable
			LOGGER.info("Go to the screen 'UC33_editableTable'.");
			// Redirect (PRG) from howto_edittable
			destinationPath =  "redirect:/presentation/standardfeatures/application/datagrid/uc33_editabletable/UC33_editableTable.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : howto_edittable 
	}
	
				/**
	 * Operation : howto_paginationTable
 	 * @param model : 
 	 * @param cmp_treeview : The form
 	 * @return
	 */
	@RequestMapping(value = "/cmp_treeview/howto_paginationTable.html",method = RequestMethod.POST)
	public String howto_paginationTable(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("cmp_treeviewForm") cmp_treeviewForm  cmp_treeviewForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : howto_paginationTable"); 
		infoLogger(cmp_treeviewForm); 
		cmp_treeviewForm.setAlwaysCallPreControllers(false); 
		// initialization for howto_paginationTable
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state howto_paginationTable if the validation fail, you will be forward to the screen : "/master/components/cmp_treeview"
		if(errorsMessages(request,response,cmp_treeviewForm,bindingResult,"", cmp_treeviewValidator, customDateEditorscmp_treeviewController)){ 
			return "/master/components/cmp_treeview"; 
		}

	 callGotoServerPaginatedTable(request,cmp_treeviewForm );
											// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.application.datagrid.uc34_paginationtable.UC34_PaginationTableForm or not from howto_paginationTable
			final UC34_PaginationTableForm uC34_PaginationTableForm =  new UC34_PaginationTableForm();
			// Populate the destination form with all the instances returned from howto_paginationTable.
			final IForm uC34_PaginationTableForm2 = populateDestinationForm(request.getSession(), uC34_PaginationTableForm); 
					
			// Add the form to the model.
			model.addAttribute("uC34_PaginationTableForm", uC34_PaginationTableForm2); 
			
			request.getSession().setAttribute("uC34_PaginationTableForm", uC34_PaginationTableForm2);
			
			// "OK" CASE => destination screen path from howto_paginationTable
			LOGGER.info("Go to the screen 'UC34_PaginationTable'.");
			// Redirect (PRG) from howto_paginationTable
			destinationPath =  "redirect:/presentation/standardfeatures/application/datagrid/uc34_paginationtable/UC34_PaginationTable.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : howto_paginationTable 
	}
	
				/**
	 * Operation : howto_collectionorder
 	 * @param model : 
 	 * @param cmp_treeview : The form
 	 * @return
	 */
	@RequestMapping(value = "/cmp_treeview/howto_collectionorder.html",method = RequestMethod.POST)
	public String howto_collectionorder(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("cmp_treeviewForm") cmp_treeviewForm  cmp_treeviewForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : howto_collectionorder"); 
		infoLogger(cmp_treeviewForm); 
		cmp_treeviewForm.setAlwaysCallPreControllers(false); 
		// initialization for howto_collectionorder
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state howto_collectionorder if the validation fail, you will be forward to the screen : "/master/components/cmp_treeview"
		if(errorsMessages(request,response,cmp_treeviewForm,bindingResult,"", cmp_treeviewValidator, customDateEditorscmp_treeviewController)){ 
			return "/master/components/cmp_treeview"; 
		}

	 callGotoCollectionOrder(request,cmp_treeviewForm );
											// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.application.datagrid.uc37_collectionorder.UC37_TeamsForm or not from howto_collectionorder
			final UC37_TeamsForm uC37_TeamsForm =  new UC37_TeamsForm();
			// Populate the destination form with all the instances returned from howto_collectionorder.
			final IForm uC37_TeamsForm2 = populateDestinationForm(request.getSession(), uC37_TeamsForm); 
					
			// Add the form to the model.
			model.addAttribute("uC37_TeamsForm", uC37_TeamsForm2); 
			
			request.getSession().setAttribute("uC37_TeamsForm", uC37_TeamsForm2);
			
			// "OK" CASE => destination screen path from howto_collectionorder
			LOGGER.info("Go to the screen 'UC37_Teams'.");
			// Redirect (PRG) from howto_collectionorder
			destinationPath =  "redirect:/presentation/standardfeatures/application/datagrid/uc37_collectionorder/UC37_Teams.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : howto_collectionorder 
	}
	
				/**
	 * Operation : howto_repeater
 	 * @param model : 
 	 * @param cmp_treeview : The form
 	 * @return
	 */
	@RequestMapping(value = "/cmp_treeview/howto_repeater.html",method = RequestMethod.POST)
	public String howto_repeater(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("cmp_treeviewForm") cmp_treeviewForm  cmp_treeviewForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : howto_repeater"); 
		infoLogger(cmp_treeviewForm); 
		cmp_treeviewForm.setAlwaysCallPreControllers(false); 
		// initialization for howto_repeater
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state howto_repeater if the validation fail, you will be forward to the screen : "/master/components/cmp_treeview"
		if(errorsMessages(request,response,cmp_treeviewForm,bindingResult,"", cmp_treeviewValidator, customDateEditorscmp_treeviewController)){ 
			return "/master/components/cmp_treeview"; 
		}

	 callGoToRepeater(request,cmp_treeviewForm );
											// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.application.repeater.uc39_repeater.UC39_HomePageForm or not from howto_repeater
			final UC39_HomePageForm uC39_HomePageForm =  new UC39_HomePageForm();
			// Populate the destination form with all the instances returned from howto_repeater.
			final IForm uC39_HomePageForm2 = populateDestinationForm(request.getSession(), uC39_HomePageForm); 
					
			// Add the form to the model.
			model.addAttribute("uC39_HomePageForm", uC39_HomePageForm2); 
			
			request.getSession().setAttribute("uC39_HomePageForm", uC39_HomePageForm2);
			
			// "OK" CASE => destination screen path from howto_repeater
			LOGGER.info("Go to the screen 'UC39_HomePage'.");
			// Redirect (PRG) from howto_repeater
			destinationPath =  "redirect:/presentation/standardfeatures/application/repeater/uc39_repeater/UC39_HomePage.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : howto_repeater 
	}
	
				/**
	 * Operation : howto_conditional
 	 * @param model : 
 	 * @param cmp_treeview : The form
 	 * @return
	 */
	@RequestMapping(value = "/cmp_treeview/howto_conditional.html",method = RequestMethod.POST)
	public String howto_conditional(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("cmp_treeviewForm") cmp_treeviewForm  cmp_treeviewForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : howto_conditional"); 
		infoLogger(cmp_treeviewForm); 
		cmp_treeviewForm.setAlwaysCallPreControllers(false); 
		// initialization for howto_conditional
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state howto_conditional if the validation fail, you will be forward to the screen : "/master/components/cmp_treeview"
		if(errorsMessages(request,response,cmp_treeviewForm,bindingResult,"", cmp_treeviewValidator, customDateEditorscmp_treeviewController)){ 
			return "/master/components/cmp_treeview"; 
		}

	 callGotoConditionalProcess(request,cmp_treeviewForm );
											// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.services.process.uc52_conditional.UC52_ConditionalForm or not from howto_conditional
			final UC52_ConditionalForm uC52_ConditionalForm =  new UC52_ConditionalForm();
			// Populate the destination form with all the instances returned from howto_conditional.
			final IForm uC52_ConditionalForm2 = populateDestinationForm(request.getSession(), uC52_ConditionalForm); 
					
			// Add the form to the model.
			model.addAttribute("uC52_ConditionalForm", uC52_ConditionalForm2); 
			
			request.getSession().setAttribute("uC52_ConditionalForm", uC52_ConditionalForm2);
			
			// "OK" CASE => destination screen path from howto_conditional
			LOGGER.info("Go to the screen 'UC52_Conditional'.");
			// Redirect (PRG) from howto_conditional
			destinationPath =  "redirect:/presentation/standardfeatures/services/process/uc52_conditional/UC52_Conditional.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : howto_conditional 
	}
	
				/**
	 * Operation : howto_heritagesubclass
 	 * @param model : 
 	 * @param cmp_treeview : The form
 	 * @return
	 */
	@RequestMapping(value = "/cmp_treeview/howto_heritagesubclass.html",method = RequestMethod.POST)
	public String howto_heritagesubclass(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("cmp_treeviewForm") cmp_treeviewForm  cmp_treeviewForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : howto_heritagesubclass"); 
		infoLogger(cmp_treeviewForm); 
		cmp_treeviewForm.setAlwaysCallPreControllers(false); 
		// initialization for howto_heritagesubclass
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state howto_heritagesubclass if the validation fail, you will be forward to the screen : "/master/components/cmp_treeview"
		if(errorsMessages(request,response,cmp_treeviewForm,bindingResult,"", cmp_treeviewValidator, customDateEditorscmp_treeviewController)){ 
			return "/master/components/cmp_treeview"; 
		}

	 callGotoHeritageSubclassUseCase(request,cmp_treeviewForm );
											// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.entities.uc68_heritagesubclass.UC68_HeritageSubclassForm or not from howto_heritagesubclass
			final UC68_HeritageSubclassForm uC68_HeritageSubclassForm =  new UC68_HeritageSubclassForm();
			// Populate the destination form with all the instances returned from howto_heritagesubclass.
			final IForm uC68_HeritageSubclassForm2 = populateDestinationForm(request.getSession(), uC68_HeritageSubclassForm); 
					
			// Add the form to the model.
			model.addAttribute("uC68_HeritageSubclassForm", uC68_HeritageSubclassForm2); 
			
			request.getSession().setAttribute("uC68_HeritageSubclassForm", uC68_HeritageSubclassForm2);
			
			// "OK" CASE => destination screen path from howto_heritagesubclass
			LOGGER.info("Go to the screen 'UC68_HeritageSubclass'.");
			// Redirect (PRG) from howto_heritagesubclass
			destinationPath =  "redirect:/presentation/standardfeatures/entities/uc68_heritagesubclass/UC68_HeritageSubclass.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : howto_heritagesubclass 
	}
	
				/**
	 * Operation : howto_umltosqlmapping
 	 * @param model : 
 	 * @param cmp_treeview : The form
 	 * @return
	 */
	@RequestMapping(value = "/cmp_treeview/howto_umltosqlmapping.html",method = RequestMethod.POST)
	public String howto_umltosqlmapping(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("cmp_treeviewForm") cmp_treeviewForm  cmp_treeviewForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : howto_umltosqlmapping"); 
		infoLogger(cmp_treeviewForm); 
		cmp_treeviewForm.setAlwaysCallPreControllers(false); 
		// initialization for howto_umltosqlmapping
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state howto_umltosqlmapping if the validation fail, you will be forward to the screen : "/master/components/cmp_treeview"
		if(errorsMessages(request,response,cmp_treeviewForm,bindingResult,"", cmp_treeviewValidator, customDateEditorscmp_treeviewController)){ 
			return "/master/components/cmp_treeview"; 
		}

	 callGOtoUMLtoSQL(request,cmp_treeviewForm );
											// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.entities.uc59_umltosqltypemapping.UC59_UMLtoSQLmappingForm or not from howto_umltosqlmapping
			final UC59_UMLtoSQLmappingForm uC59_UMLtoSQLmappingForm =  new UC59_UMLtoSQLmappingForm();
			// Populate the destination form with all the instances returned from howto_umltosqlmapping.
			final IForm uC59_UMLtoSQLmappingForm2 = populateDestinationForm(request.getSession(), uC59_UMLtoSQLmappingForm); 
					
			// Add the form to the model.
			model.addAttribute("uC59_UMLtoSQLmappingForm", uC59_UMLtoSQLmappingForm2); 
			
			request.getSession().setAttribute("uC59_UMLtoSQLmappingForm", uC59_UMLtoSQLmappingForm2);
			
			// "OK" CASE => destination screen path from howto_umltosqlmapping
			LOGGER.info("Go to the screen 'UC59_UMLtoSQLmapping'.");
			// Redirect (PRG) from howto_umltosqlmapping
			destinationPath =  "redirect:/presentation/standardfeatures/entities/uc59_umltosqltypemapping/UC59_UMLtoSQLmapping.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : howto_umltosqlmapping 
	}
	
				/**
	 * Operation : howto_for
 	 * @param model : 
 	 * @param cmp_treeview : The form
 	 * @return
	 */
	@RequestMapping(value = "/cmp_treeview/howto_for.html",method = RequestMethod.POST)
	public String howto_for(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("cmp_treeviewForm") cmp_treeviewForm  cmp_treeviewForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : howto_for"); 
		infoLogger(cmp_treeviewForm); 
		cmp_treeviewForm.setAlwaysCallPreControllers(false); 
		// initialization for howto_for
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state howto_for if the validation fail, you will be forward to the screen : "/master/components/cmp_treeview"
		if(errorsMessages(request,response,cmp_treeviewForm,bindingResult,"", cmp_treeviewValidator, customDateEditorscmp_treeviewController)){ 
			return "/master/components/cmp_treeview"; 
		}

	 callGotoForProcess(request,cmp_treeviewForm );
											// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.services.process.uc54_for.UC54_ForForm or not from howto_for
			final UC54_ForForm uC54_ForForm =  new UC54_ForForm();
			// Populate the destination form with all the instances returned from howto_for.
			final IForm uC54_ForForm2 = populateDestinationForm(request.getSession(), uC54_ForForm); 
					
			// Add the form to the model.
			model.addAttribute("uC54_ForForm", uC54_ForForm2); 
			
			request.getSession().setAttribute("uC54_ForForm", uC54_ForForm2);
			
			// "OK" CASE => destination screen path from howto_for
			LOGGER.info("Go to the screen 'UC54_For'.");
			// Redirect (PRG) from howto_for
			destinationPath =  "redirect:/presentation/standardfeatures/services/process/uc54_for/UC54_For.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : howto_for 
	}
	
				/**
	 * Operation : howto_while
 	 * @param model : 
 	 * @param cmp_treeview : The form
 	 * @return
	 */
	@RequestMapping(value = "/cmp_treeview/howto_while.html",method = RequestMethod.POST)
	public String howto_while(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("cmp_treeviewForm") cmp_treeviewForm  cmp_treeviewForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : howto_while"); 
		infoLogger(cmp_treeviewForm); 
		cmp_treeviewForm.setAlwaysCallPreControllers(false); 
		// initialization for howto_while
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state howto_while if the validation fail, you will be forward to the screen : "/master/components/cmp_treeview"
		if(errorsMessages(request,response,cmp_treeviewForm,bindingResult,"", cmp_treeviewValidator, customDateEditorscmp_treeviewController)){ 
			return "/master/components/cmp_treeview"; 
		}

	 callGotoWhileProcess(request,cmp_treeviewForm );
											// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.services.process.uc53_while.UC53_WhileForm or not from howto_while
			final UC53_WhileForm uC53_WhileForm =  new UC53_WhileForm();
			// Populate the destination form with all the instances returned from howto_while.
			final IForm uC53_WhileForm2 = populateDestinationForm(request.getSession(), uC53_WhileForm); 
					
			// Add the form to the model.
			model.addAttribute("uC53_WhileForm", uC53_WhileForm2); 
			
			request.getSession().setAttribute("uC53_WhileForm", uC53_WhileForm2);
			
			// "OK" CASE => destination screen path from howto_while
			LOGGER.info("Go to the screen 'UC53_While'.");
			// Redirect (PRG) from howto_while
			destinationPath =  "redirect:/presentation/standardfeatures/services/process/uc53_while/UC53_While.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : howto_while 
	}
	
				/**
	 * Operation : howto_menu
 	 * @param model : 
 	 * @param cmp_treeview : The form
 	 * @return
	 */
	@RequestMapping(value = "/cmp_treeview/howto_menu.html",method = RequestMethod.POST)
	public String howto_menu(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("cmp_treeviewForm") cmp_treeviewForm  cmp_treeviewForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : howto_menu"); 
		infoLogger(cmp_treeviewForm); 
		cmp_treeviewForm.setAlwaysCallPreControllers(false); 
		// initialization for howto_menu
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state howto_menu if the validation fail, you will be forward to the screen : "/master/components/cmp_treeview"
		if(errorsMessages(request,response,cmp_treeviewForm,bindingResult,"", cmp_treeviewValidator, customDateEditorscmp_treeviewController)){ 
			return "/master/components/cmp_treeview"; 
		}

	 callGotoComponentPage1(request,cmp_treeviewForm );
											// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.application.component.uc46_menu.UC46_Page1Form or not from howto_menu
			final UC46_Page1Form uC46_Page1Form =  new UC46_Page1Form();
			// Populate the destination form with all the instances returned from howto_menu.
			final IForm uC46_Page1Form2 = populateDestinationForm(request.getSession(), uC46_Page1Form); 
					
			// Add the form to the model.
			model.addAttribute("uC46_Page1Form", uC46_Page1Form2); 
			
			request.getSession().setAttribute("uC46_Page1Form", uC46_Page1Form2);
			
			// "OK" CASE => destination screen path from howto_menu
			LOGGER.info("Go to the screen 'UC46_Page1'.");
			// Redirect (PRG) from howto_menu
			destinationPath =  "redirect:/presentation/standardfeatures/application/component/uc46_menu/UC46_Page1.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : howto_menu 
	}
	
				/**
	 * Operation : howto_exception
 	 * @param model : 
 	 * @param cmp_treeview : The form
 	 * @return
	 */
	@RequestMapping(value = "/cmp_treeview/howto_exception.html",method = RequestMethod.POST)
	public String howto_exception(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("cmp_treeviewForm") cmp_treeviewForm  cmp_treeviewForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : howto_exception"); 
		infoLogger(cmp_treeviewForm); 
		cmp_treeviewForm.setAlwaysCallPreControllers(false); 
		// initialization for howto_exception
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state howto_exception if the validation fail, you will be forward to the screen : "/master/components/cmp_treeview"
		if(errorsMessages(request,response,cmp_treeviewForm,bindingResult,"", cmp_treeviewValidator, customDateEditorscmp_treeviewController)){ 
			return "/master/components/cmp_treeview"; 
		}

	 callGotoexception(request,cmp_treeviewForm );
											// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.exception.uc73_exception.UC73_ManagePlayersForm or not from howto_exception
			final UC73_ManagePlayersForm uC73_ManagePlayersForm =  new UC73_ManagePlayersForm();
			// Populate the destination form with all the instances returned from howto_exception.
			final IForm uC73_ManagePlayersForm2 = populateDestinationForm(request.getSession(), uC73_ManagePlayersForm); 
					
			// Add the form to the model.
			model.addAttribute("uC73_ManagePlayersForm", uC73_ManagePlayersForm2); 
			
			request.getSession().setAttribute("uC73_ManagePlayersForm", uC73_ManagePlayersForm2);
			
			// "OK" CASE => destination screen path from howto_exception
			LOGGER.info("Go to the screen 'UC73_ManagePlayers'.");
			// Redirect (PRG) from howto_exception
			destinationPath =  "redirect:/presentation/standardfeatures/exception/uc73_exception/UC73_ManagePlayers.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : howto_exception 
	}
	
				/**
	 * Operation : howto_datagridloading
 	 * @param model : 
 	 * @param cmp_treeview : The form
 	 * @return
	 */
	@RequestMapping(value = "/cmp_treeview/howto_datagridloading.html",method = RequestMethod.POST)
	public String howto_datagridloading(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("cmp_treeviewForm") cmp_treeviewForm  cmp_treeviewForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : howto_datagridloading"); 
		infoLogger(cmp_treeviewForm); 
		cmp_treeviewForm.setAlwaysCallPreControllers(false); 
		// initialization for howto_datagridloading
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state howto_datagridloading if the validation fail, you will be forward to the screen : "/master/components/cmp_treeview"
		if(errorsMessages(request,response,cmp_treeviewForm,bindingResult,"", cmp_treeviewValidator, customDateEditorscmp_treeviewController)){ 
			return "/master/components/cmp_treeview"; 
		}

	 callGotodatagridloading(request,cmp_treeviewForm );
											// Init the destination form com.bluage.documentation.web.presentation.advancedfeatures.performance.uc200_datagridloading.UC200_DatagridLoadingForm or not from howto_datagridloading
			final UC200_DatagridLoadingForm uC200_DatagridLoadingForm =  new UC200_DatagridLoadingForm();
			// Populate the destination form with all the instances returned from howto_datagridloading.
			final IForm uC200_DatagridLoadingForm2 = populateDestinationForm(request.getSession(), uC200_DatagridLoadingForm); 
					
			// Add the form to the model.
			model.addAttribute("uC200_DatagridLoadingForm", uC200_DatagridLoadingForm2); 
			
			request.getSession().setAttribute("uC200_DatagridLoadingForm", uC200_DatagridLoadingForm2);
			
			// "OK" CASE => destination screen path from howto_datagridloading
			LOGGER.info("Go to the screen 'UC200_DatagridLoading'.");
			// Redirect (PRG) from howto_datagridloading
			destinationPath =  "redirect:/presentation/advancedfeatures/performance/uc200_datagridloading/UC200_DatagridLoading.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : howto_datagridloading 
	}
	
				/**
	 * Operation : howto_preaction
 	 * @param model : 
 	 * @param cmp_treeview : The form
 	 * @return
	 */
	@RequestMapping(value = "/cmp_treeview/howto_preaction.html",method = RequestMethod.POST)
	public String howto_preaction(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("cmp_treeviewForm") cmp_treeviewForm  cmp_treeviewForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : howto_preaction"); 
		infoLogger(cmp_treeviewForm); 
		cmp_treeviewForm.setAlwaysCallPreControllers(false); 
		// initialization for howto_preaction
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state howto_preaction if the validation fail, you will be forward to the screen : "/master/components/cmp_treeview"
		if(errorsMessages(request,response,cmp_treeviewForm,bindingResult,"", cmp_treeviewValidator, customDateEditorscmp_treeviewController)){ 
			return "/master/components/cmp_treeview"; 
		}

	 callGotopreAction(request,cmp_treeviewForm );
											// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.application.navigation.uc31_preaction.UC31_HomePageForm or not from howto_preaction
			final UC31_HomePageForm uC31_HomePageForm =  new UC31_HomePageForm();
			// Populate the destination form with all the instances returned from howto_preaction.
			final IForm uC31_HomePageForm2 = populateDestinationForm(request.getSession(), uC31_HomePageForm); 
					
			// Add the form to the model.
			model.addAttribute("uC31_HomePageForm", uC31_HomePageForm2); 
			
			request.getSession().setAttribute("uC31_HomePageForm", uC31_HomePageForm2);
			
			// "OK" CASE => destination screen path from howto_preaction
			LOGGER.info("Go to the screen 'UC31_HomePage'.");
			// Redirect (PRG) from howto_preaction
			destinationPath =  "redirect:/presentation/standardfeatures/application/navigation/uc31_preaction/UC31_HomePage.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : howto_preaction 
	}
	
				/**
	 * Operation : howto_hqloperation
 	 * @param model : 
 	 * @param cmp_treeview : The form
 	 * @return
	 */
	@RequestMapping(value = "/cmp_treeview/howto_hqloperation.html",method = RequestMethod.POST)
	public String howto_hqloperation(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("cmp_treeviewForm") cmp_treeviewForm  cmp_treeviewForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : howto_hqloperation"); 
		infoLogger(cmp_treeviewForm); 
		cmp_treeviewForm.setAlwaysCallPreControllers(false); 
		// initialization for howto_hqloperation
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state howto_hqloperation if the validation fail, you will be forward to the screen : "/master/components/cmp_treeview"
		if(errorsMessages(request,response,cmp_treeviewForm,bindingResult,"", cmp_treeviewValidator, customDateEditorscmp_treeviewController)){ 
			return "/master/components/cmp_treeview"; 
		}

	 callGotoHQLOperation(request,cmp_treeviewForm );
											// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.services.hql.uc57_hqloperation.UC57_SearchForm or not from howto_hqloperation
			final UC57_SearchForm uC57_SearchForm =  new UC57_SearchForm();
			// Populate the destination form with all the instances returned from howto_hqloperation.
			final IForm uC57_SearchForm2 = populateDestinationForm(request.getSession(), uC57_SearchForm); 
					
			// Add the form to the model.
			model.addAttribute("uC57_SearchForm", uC57_SearchForm2); 
			
			request.getSession().setAttribute("uC57_SearchForm", uC57_SearchForm2);
			
			// "OK" CASE => destination screen path from howto_hqloperation
			LOGGER.info("Go to the screen 'UC57_Search'.");
			// Redirect (PRG) from howto_hqloperation
			destinationPath =  "redirect:/presentation/standardfeatures/services/hql/uc57_hqloperation/UC57_Search.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : howto_hqloperation 
	}
	
				/**
	 * Operation : howto_association
 	 * @param model : 
 	 * @param cmp_treeview : The form
 	 * @return
	 */
	@RequestMapping(value = "/cmp_treeview/howto_association.html",method = RequestMethod.POST)
	public String howto_association(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("cmp_treeviewForm") cmp_treeviewForm  cmp_treeviewForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : howto_association"); 
		infoLogger(cmp_treeviewForm); 
		cmp_treeviewForm.setAlwaysCallPreControllers(false); 
		// initialization for howto_association
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state howto_association if the validation fail, you will be forward to the screen : "/master/components/cmp_treeview"
		if(errorsMessages(request,response,cmp_treeviewForm,bindingResult,"", cmp_treeviewValidator, customDateEditorscmp_treeviewController)){ 
			return "/master/components/cmp_treeview"; 
		}

	 callGotoassociation(request,cmp_treeviewForm );
											// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.entities.uc63_association.UC63_HomeForm or not from howto_association
			final UC63_HomeForm uC63_HomeForm =  new UC63_HomeForm();
			// Populate the destination form with all the instances returned from howto_association.
			final IForm uC63_HomeForm2 = populateDestinationForm(request.getSession(), uC63_HomeForm); 
					
			// Add the form to the model.
			model.addAttribute("uC63_HomeForm", uC63_HomeForm2); 
			
			request.getSession().setAttribute("uC63_HomeForm", uC63_HomeForm2);
			
			// "OK" CASE => destination screen path from howto_association
			LOGGER.info("Go to the screen 'UC63_Home'.");
			// Redirect (PRG) from howto_association
			destinationPath =  "redirect:/presentation/standardfeatures/entities/uc63_association/UC63_Home.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : howto_association 
	}
	
				/**
	 * Operation : howto_cascade
 	 * @param model : 
 	 * @param cmp_treeview : The form
 	 * @return
	 */
	@RequestMapping(value = "/cmp_treeview/howto_cascade.html",method = RequestMethod.POST)
	public String howto_cascade(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("cmp_treeviewForm") cmp_treeviewForm  cmp_treeviewForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : howto_cascade"); 
		infoLogger(cmp_treeviewForm); 
		cmp_treeviewForm.setAlwaysCallPreControllers(false); 
		// initialization for howto_cascade
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state howto_cascade if the validation fail, you will be forward to the screen : "/master/components/cmp_treeview"
		if(errorsMessages(request,response,cmp_treeviewForm,bindingResult,"", cmp_treeviewValidator, customDateEditorscmp_treeviewController)){ 
			return "/master/components/cmp_treeview"; 
		}

	 callGotoCascade(request,cmp_treeviewForm );
											// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.entities.uc64_cascade.UC64_HomePageForm or not from howto_cascade
			final UC64_HomePageForm uC64_HomePageForm =  new UC64_HomePageForm();
			// Populate the destination form with all the instances returned from howto_cascade.
			final IForm uC64_HomePageForm2 = populateDestinationForm(request.getSession(), uC64_HomePageForm); 
					
			// Add the form to the model.
			model.addAttribute("uC64_HomePageForm", uC64_HomePageForm2); 
			
			request.getSession().setAttribute("uC64_HomePageForm", uC64_HomePageForm2);
			
			// "OK" CASE => destination screen path from howto_cascade
			LOGGER.info("Go to the screen 'UC64_HomePage'.");
			// Redirect (PRG) from howto_cascade
			destinationPath =  "redirect:/presentation/standardfeatures/entities/uc64_cascade/UC64_HomePage.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : howto_cascade 
	}
	
				/**
	 * Operation : howto_lazy
 	 * @param model : 
 	 * @param cmp_treeview : The form
 	 * @return
	 */
	@RequestMapping(value = "/cmp_treeview/howto_lazy.html",method = RequestMethod.POST)
	public String howto_lazy(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("cmp_treeviewForm") cmp_treeviewForm  cmp_treeviewForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : howto_lazy"); 
		infoLogger(cmp_treeviewForm); 
		cmp_treeviewForm.setAlwaysCallPreControllers(false); 
		// initialization for howto_lazy
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state howto_lazy if the validation fail, you will be forward to the screen : "/master/components/cmp_treeview"
		if(errorsMessages(request,response,cmp_treeviewForm,bindingResult,"", cmp_treeviewValidator, customDateEditorscmp_treeviewController)){ 
			return "/master/components/cmp_treeview"; 
		}

	 callGotoLazy(request,cmp_treeviewForm );
											// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.entities.uc65_lazy.UC65_HomePageForm or not from howto_lazy
			final UC65_HomePageForm uC65_HomePageForm =  new UC65_HomePageForm();
			// Populate the destination form with all the instances returned from howto_lazy.
			final IForm uC65_HomePageForm2 = populateDestinationForm(request.getSession(), uC65_HomePageForm); 
					
			// Add the form to the model.
			model.addAttribute("uC65_HomePageForm", uC65_HomePageForm2); 
			
			request.getSession().setAttribute("uC65_HomePageForm", uC65_HomePageForm2);
			
			// "OK" CASE => destination screen path from howto_lazy
			LOGGER.info("Go to the screen 'UC65_HomePage'.");
			// Redirect (PRG) from howto_lazy
			destinationPath =  "redirect:/presentation/standardfeatures/entities/uc65_lazy/UC65_HomePage.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : howto_lazy 
	}
	
				/**
	 * Operation : howto_transientobject
 	 * @param model : 
 	 * @param cmp_treeview : The form
 	 * @return
	 */
	@RequestMapping(value = "/cmp_treeview/howto_transientobject.html",method = RequestMethod.POST)
	public String howto_transientobject(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("cmp_treeviewForm") cmp_treeviewForm  cmp_treeviewForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : howto_transientobject"); 
		infoLogger(cmp_treeviewForm); 
		cmp_treeviewForm.setAlwaysCallPreControllers(false); 
		// initialization for howto_transientobject
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state howto_transientobject if the validation fail, you will be forward to the screen : "/master/components/cmp_treeview"
		if(errorsMessages(request,response,cmp_treeviewForm,bindingResult,"", cmp_treeviewValidator, customDateEditorscmp_treeviewController)){ 
			return "/master/components/cmp_treeview"; 
		}

	 callGotoTransientobject(request,cmp_treeviewForm );
											// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.entities.uc61_transientobject.UC61_AddTeamForm or not from howto_transientobject
			final UC61_AddTeamForm uC61_AddTeamForm =  new UC61_AddTeamForm();
			// Populate the destination form with all the instances returned from howto_transientobject.
			final IForm uC61_AddTeamForm2 = populateDestinationForm(request.getSession(), uC61_AddTeamForm); 
					
			// Add the form to the model.
			model.addAttribute("uC61_AddTeamForm", uC61_AddTeamForm2); 
			
			request.getSession().setAttribute("uC61_AddTeamForm", uC61_AddTeamForm2);
			
			// "OK" CASE => destination screen path from howto_transientobject
			LOGGER.info("Go to the screen 'UC61_AddTeam'.");
			// Redirect (PRG) from howto_transientobject
			destinationPath =  "redirect:/presentation/standardfeatures/entities/uc61_transientobject/UC61_AddTeam.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : howto_transientobject 
	}
	
				/**
	 * Operation : howto_multidomaine
 	 * @param model : 
 	 * @param cmp_treeview : The form
 	 * @return
	 */
	@RequestMapping(value = "/cmp_treeview/howto_multidomaine.html",method = RequestMethod.POST)
	public String howto_multidomaine(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("cmp_treeviewForm") cmp_treeviewForm  cmp_treeviewForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : howto_multidomaine"); 
		infoLogger(cmp_treeviewForm); 
		cmp_treeviewForm.setAlwaysCallPreControllers(false); 
		// initialization for howto_multidomaine
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state howto_multidomaine if the validation fail, you will be forward to the screen : "/master/components/cmp_treeview"
		if(errorsMessages(request,response,cmp_treeviewForm,bindingResult,"", cmp_treeviewValidator, customDateEditorscmp_treeviewController)){ 
			return "/master/components/cmp_treeview"; 
		}

	 callGotoMultiDomain(request,cmp_treeviewForm );
											// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.entities.uc60_multidomain.UC60_MultiDomainForm or not from howto_multidomaine
			final UC60_MultiDomainForm uC60_MultiDomainForm =  new UC60_MultiDomainForm();
			// Populate the destination form with all the instances returned from howto_multidomaine.
			final IForm uC60_MultiDomainForm2 = populateDestinationForm(request.getSession(), uC60_MultiDomainForm); 
					
			// Add the form to the model.
			model.addAttribute("uC60_MultiDomainForm", uC60_MultiDomainForm2); 
			
			request.getSession().setAttribute("uC60_MultiDomainForm", uC60_MultiDomainForm2);
			
			// "OK" CASE => destination screen path from howto_multidomaine
			LOGGER.info("Go to the screen 'UC60_MultiDomain'.");
			// Redirect (PRG) from howto_multidomaine
			destinationPath =  "redirect:/presentation/standardfeatures/entities/uc60_multidomain/UC60_MultiDomain.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : howto_multidomaine 
	}
	
				/**
	 * Operation : howto_copy
 	 * @param model : 
 	 * @param cmp_treeview : The form
 	 * @return
	 */
	@RequestMapping(value = "/cmp_treeview/howto_copy.html",method = RequestMethod.POST)
	public String howto_copy(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("cmp_treeviewForm") cmp_treeviewForm  cmp_treeviewForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : howto_copy"); 
		infoLogger(cmp_treeviewForm); 
		cmp_treeviewForm.setAlwaysCallPreControllers(false); 
		// initialization for howto_copy
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state howto_copy if the validation fail, you will be forward to the screen : "/master/components/cmp_treeview"
		if(errorsMessages(request,response,cmp_treeviewForm,bindingResult,"", cmp_treeviewValidator, customDateEditorscmp_treeviewController)){ 
			return "/master/components/cmp_treeview"; 
		}

	 callGotoservicecopy(request,cmp_treeviewForm );
											// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.services.standard.uc58_servicecopy.UC58_DisplayTeams58Form or not from howto_copy
			final UC58_DisplayTeams58Form uC58_DisplayTeams58Form =  new UC58_DisplayTeams58Form();
			// Populate the destination form with all the instances returned from howto_copy.
			final IForm uC58_DisplayTeams58Form2 = populateDestinationForm(request.getSession(), uC58_DisplayTeams58Form); 
					
			// Add the form to the model.
			model.addAttribute("uC58_DisplayTeams58Form", uC58_DisplayTeams58Form2); 
			
			request.getSession().setAttribute("uC58_DisplayTeams58Form", uC58_DisplayTeams58Form2);
			
			// "OK" CASE => destination screen path from howto_copy
			LOGGER.info("Go to the screen 'UC58_DisplayTeams58'.");
			// Redirect (PRG) from howto_copy
			destinationPath =  "redirect:/presentation/standardfeatures/services/standard/uc58_servicecopy/UC58_DisplayTeams58.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : howto_copy 
	}
	
				/**
	 * Operation : howto_paginatorBDD
 	 * @param model : 
 	 * @param cmp_treeview : The form
 	 * @return
	 */
	@RequestMapping(value = "/cmp_treeview/howto_paginatorBDD.html",method = RequestMethod.POST)
	public String howto_paginatorBDD(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("cmp_treeviewForm") cmp_treeviewForm  cmp_treeviewForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : howto_paginatorBDD"); 
		infoLogger(cmp_treeviewForm); 
		cmp_treeviewForm.setAlwaysCallPreControllers(false); 
		// initialization for howto_paginatorBDD
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state howto_paginatorBDD if the validation fail, you will be forward to the screen : "/master/components/cmp_treeview"
		if(errorsMessages(request,response,cmp_treeviewForm,bindingResult,"", cmp_treeviewValidator, customDateEditorscmp_treeviewController)){ 
			return "/master/components/cmp_treeview"; 
		}

	 callGotoPaginatorBDD(request,cmp_treeviewForm );
											// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.application.datagrid.uc35_paginatorbdd.UC35_HomePageForm or not from howto_paginatorBDD
			final UC35_HomePageForm uC35_HomePageForm =  new UC35_HomePageForm();
			// Populate the destination form with all the instances returned from howto_paginatorBDD.
			final IForm uC35_HomePageForm2 = populateDestinationForm(request.getSession(), uC35_HomePageForm); 
					
			// Add the form to the model.
			model.addAttribute("uC35_HomePageForm", uC35_HomePageForm2); 
			
			request.getSession().setAttribute("uC35_HomePageForm", uC35_HomePageForm2);
			
			// "OK" CASE => destination screen path from howto_paginatorBDD
			LOGGER.info("Go to the screen 'UC35_HomePage'.");
			// Redirect (PRG) from howto_paginatorBDD
			destinationPath =  "redirect:/presentation/standardfeatures/application/datagrid/uc35_paginatorbdd/UC35_HomePage.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : howto_paginatorBDD 
	}
	
				/**
	 * Operation : howto_autocomplete
 	 * @param model : 
 	 * @param cmp_treeview : The form
 	 * @return
	 */
	@RequestMapping(value = "/cmp_treeview/howto_autocomplete.html",method = RequestMethod.POST)
	public String howto_autocomplete(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("cmp_treeviewForm") cmp_treeviewForm  cmp_treeviewForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : howto_autocomplete"); 
		infoLogger(cmp_treeviewForm); 
		cmp_treeviewForm.setAlwaysCallPreControllers(false); 
		// initialization for howto_autocomplete
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state howto_autocomplete if the validation fail, you will be forward to the screen : "/master/components/cmp_treeview"
		if(errorsMessages(request,response,cmp_treeviewForm,bindingResult,"", cmp_treeviewValidator, customDateEditorscmp_treeviewController)){ 
			return "/master/components/cmp_treeview"; 
		}

	 callGotoAutocomplete(request,cmp_treeviewForm );
											// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.application.form.uc45_autocomplete.UC45_HomePageForm or not from howto_autocomplete
			final UC45_HomePageForm uC45_HomePageForm =  new UC45_HomePageForm();
			// Populate the destination form with all the instances returned from howto_autocomplete.
			final IForm uC45_HomePageForm2 = populateDestinationForm(request.getSession(), uC45_HomePageForm); 
					
			// Add the form to the model.
			model.addAttribute("uC45_HomePageForm", uC45_HomePageForm2); 
			
			request.getSession().setAttribute("uC45_HomePageForm", uC45_HomePageForm2);
			
			// "OK" CASE => destination screen path from howto_autocomplete
			LOGGER.info("Go to the screen 'UC45_HomePage'.");
			// Redirect (PRG) from howto_autocomplete
			destinationPath =  "redirect:/presentation/standardfeatures/application/form/uc45_autocomplete/UC45_HomePage.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : howto_autocomplete 
	}
	
				/**
	 * Operation : howto_defaultsort
 	 * @param model : 
 	 * @param cmp_treeview : The form
 	 * @return
	 */
	@RequestMapping(value = "/cmp_treeview/howto_defaultsort.html",method = RequestMethod.POST)
	public String howto_defaultsort(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("cmp_treeviewForm") cmp_treeviewForm  cmp_treeviewForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : howto_defaultsort"); 
		infoLogger(cmp_treeviewForm); 
		cmp_treeviewForm.setAlwaysCallPreControllers(false); 
		// initialization for howto_defaultsort
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state howto_defaultsort if the validation fail, you will be forward to the screen : "/master/components/cmp_treeview"
		if(errorsMessages(request,response,cmp_treeviewForm,bindingResult,"", cmp_treeviewValidator, customDateEditorscmp_treeviewController)){ 
			return "/master/components/cmp_treeview"; 
		}

	 callGoToDefaultSort(request,cmp_treeviewForm );
											// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.entities.uc40_defaultsort.UC40_DefaultSortForm or not from howto_defaultsort
			final UC40_DefaultSortForm uC40_DefaultSortForm =  new UC40_DefaultSortForm();
			// Populate the destination form with all the instances returned from howto_defaultsort.
			final IForm uC40_DefaultSortForm2 = populateDestinationForm(request.getSession(), uC40_DefaultSortForm); 
					
			// Add the form to the model.
			model.addAttribute("uC40_DefaultSortForm", uC40_DefaultSortForm2); 
			
			request.getSession().setAttribute("uC40_DefaultSortForm", uC40_DefaultSortForm2);
			
			// "OK" CASE => destination screen path from howto_defaultsort
			LOGGER.info("Go to the screen 'UC40_DefaultSort'.");
			// Redirect (PRG) from howto_defaultsort
			destinationPath =  "redirect:/presentation/standardfeatures/entities/uc40_defaultsort/UC40_DefaultSort.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : howto_defaultsort 
	}
	
				/**
	 * Operation : howto_entitiescreation
 	 * @param model : 
 	 * @param cmp_treeview : The form
 	 * @return
	 */
	@RequestMapping(value = "/cmp_treeview/howto_entitiescreation.html",method = RequestMethod.POST)
	public String howto_entitiescreation(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("cmp_treeviewForm") cmp_treeviewForm  cmp_treeviewForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : howto_entitiescreation"); 
		infoLogger(cmp_treeviewForm); 
		cmp_treeviewForm.setAlwaysCallPreControllers(false); 
		// initialization for howto_entitiescreation
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state howto_entitiescreation if the validation fail, you will be forward to the screen : "/master/components/cmp_treeview"
		if(errorsMessages(request,response,cmp_treeviewForm,bindingResult,"", cmp_treeviewValidator, customDateEditorscmp_treeviewController)){ 
			return "/master/components/cmp_treeview"; 
		}

	 callGoToentitiesCreation(request,cmp_treeviewForm );
											// Init the destination form com.bluage.documentation.web.presentation.gettingstarted.uc02_entitiescreation.UC02_EntitiesCreationForm or not from howto_entitiescreation
			final UC02_EntitiesCreationForm uC02_EntitiesCreationForm =  new UC02_EntitiesCreationForm();
			// Populate the destination form with all the instances returned from howto_entitiescreation.
			final IForm uC02_EntitiesCreationForm2 = populateDestinationForm(request.getSession(), uC02_EntitiesCreationForm); 
					
			// Add the form to the model.
			model.addAttribute("uC02_EntitiesCreationForm", uC02_EntitiesCreationForm2); 
			
			request.getSession().setAttribute("uC02_EntitiesCreationForm", uC02_EntitiesCreationForm2);
			
			// "OK" CASE => destination screen path from howto_entitiescreation
			LOGGER.info("Go to the screen 'UC02_EntitiesCreation'.");
			// Redirect (PRG) from howto_entitiescreation
			destinationPath =  "redirect:/presentation/gettingstarted/uc02_entitiescreation/UC02_EntitiesCreation.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : howto_entitiescreation 
	}
	
				/**
	 * Operation : howto_sqloperation
 	 * @param model : 
 	 * @param cmp_treeview : The form
 	 * @return
	 */
	@RequestMapping(value = "/cmp_treeview/howto_sqloperation.html",method = RequestMethod.POST)
	public String howto_sqloperation(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("cmp_treeviewForm") cmp_treeviewForm  cmp_treeviewForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : howto_sqloperation"); 
		infoLogger(cmp_treeviewForm); 
		cmp_treeviewForm.setAlwaysCallPreControllers(false); 
		// initialization for howto_sqloperation
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state howto_sqloperation if the validation fail, you will be forward to the screen : "/master/components/cmp_treeview"
		if(errorsMessages(request,response,cmp_treeviewForm,bindingResult,"", cmp_treeviewValidator, customDateEditorscmp_treeviewController)){ 
			return "/master/components/cmp_treeview"; 
		}

	 callGoToSQLOperation(request,cmp_treeviewForm );
											// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.services.sql.uc56_sqloperation.UC56_SearchForm or not from howto_sqloperation
			final UC56_SearchForm uC56_SearchForm =  new UC56_SearchForm();
			// Populate the destination form with all the instances returned from howto_sqloperation.
			final IForm uC56_SearchForm2 = populateDestinationForm(request.getSession(), uC56_SearchForm); 
					
			// Add the form to the model.
			model.addAttribute("uC56_SearchForm", uC56_SearchForm2); 
			
			request.getSession().setAttribute("uC56_SearchForm", uC56_SearchForm2);
			
			// "OK" CASE => destination screen path from howto_sqloperation
			LOGGER.info("Go to the screen 'UC56_Search'.");
			// Redirect (PRG) from howto_sqloperation
			destinationPath =  "redirect:/presentation/standardfeatures/services/sql/uc56_sqloperation/UC56_Search.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : howto_sqloperation 
	}
	
				/**
	 * Operation : howto_javamelody
 	 * @param model : 
 	 * @param cmp_treeview : The form
 	 * @return
	 */
	@RequestMapping(value = "/cmp_treeview/howto_javamelody.html",method = RequestMethod.POST)
	public String howto_javamelody(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("cmp_treeviewForm") cmp_treeviewForm  cmp_treeviewForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : howto_javamelody"); 
		infoLogger(cmp_treeviewForm); 
		cmp_treeviewForm.setAlwaysCallPreControllers(false); 
		// initialization for howto_javamelody
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state howto_javamelody if the validation fail, you will be forward to the screen : "/master/components/cmp_treeview"
		if(errorsMessages(request,response,cmp_treeviewForm,bindingResult,"", cmp_treeviewValidator, customDateEditorscmp_treeviewController)){ 
			return "/master/components/cmp_treeview"; 
		}

	 callGotoJavaMelody(request,cmp_treeviewForm );
											// Init the destination form com.bluage.documentation.web.presentation.advancedfeatures.externaltools.uc209_javamelody.UC209_HomePageForm or not from howto_javamelody
			final UC209_HomePageForm uC209_HomePageForm =  new UC209_HomePageForm();
			// Populate the destination form with all the instances returned from howto_javamelody.
			final IForm uC209_HomePageForm2 = populateDestinationForm(request.getSession(), uC209_HomePageForm); 
					
			// Add the form to the model.
			model.addAttribute("uC209_HomePageForm", uC209_HomePageForm2); 
			
			request.getSession().setAttribute("uC209_HomePageForm", uC209_HomePageForm2);
			
			// "OK" CASE => destination screen path from howto_javamelody
			LOGGER.info("Go to the screen 'UC209_HomePage'.");
			// Redirect (PRG) from howto_javamelody
			destinationPath =  "redirect:/presentation/advancedfeatures/externaltools/uc209_javamelody/UC209_HomePage.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : howto_javamelody 
	}
	
				/**
	 * Operation : howto_sonar
 	 * @param model : 
 	 * @param cmp_treeview : The form
 	 * @return
	 */
	@RequestMapping(value = "/cmp_treeview/howto_sonar.html",method = RequestMethod.POST)
	public String howto_sonar(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("cmp_treeviewForm") cmp_treeviewForm  cmp_treeviewForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : howto_sonar"); 
		infoLogger(cmp_treeviewForm); 
		cmp_treeviewForm.setAlwaysCallPreControllers(false); 
		// initialization for howto_sonar
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state howto_sonar if the validation fail, you will be forward to the screen : "/master/components/cmp_treeview"
		if(errorsMessages(request,response,cmp_treeviewForm,bindingResult,"", cmp_treeviewValidator, customDateEditorscmp_treeviewController)){ 
			return "/master/components/cmp_treeview"; 
		}

	 callGotoSonar(request,cmp_treeviewForm );
											// Init the destination form com.bluage.documentation.web.presentation.advancedfeatures.externaltools.uc210_sonar.UC210_HomePageForm or not from howto_sonar
			final UC210_HomePageForm uC210_HomePageForm =  new UC210_HomePageForm();
			// Populate the destination form with all the instances returned from howto_sonar.
			final IForm uC210_HomePageForm2 = populateDestinationForm(request.getSession(), uC210_HomePageForm); 
					
			// Add the form to the model.
			model.addAttribute("uC210_HomePageForm", uC210_HomePageForm2); 
			
			request.getSession().setAttribute("uC210_HomePageForm", uC210_HomePageForm2);
			
			// "OK" CASE => destination screen path from howto_sonar
			LOGGER.info("Go to the screen 'UC210_HomePage'.");
			// Redirect (PRG) from howto_sonar
			destinationPath =  "redirect:/presentation/advancedfeatures/externaltools/uc210_sonar/UC210_HomePage.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : howto_sonar 
	}
	
				/**
	 * Operation : howto_calculabletable
 	 * @param model : 
 	 * @param cmp_treeview : The form
 	 * @return
	 */
	@RequestMapping(value = "/cmp_treeview/howto_calculabletable.html",method = RequestMethod.POST)
	public String howto_calculabletable(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("cmp_treeviewForm") cmp_treeviewForm  cmp_treeviewForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : howto_calculabletable"); 
		infoLogger(cmp_treeviewForm); 
		cmp_treeviewForm.setAlwaysCallPreControllers(false); 
		// initialization for howto_calculabletable
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state howto_calculabletable if the validation fail, you will be forward to the screen : "/master/components/cmp_treeview"
		if(errorsMessages(request,response,cmp_treeviewForm,bindingResult,"", cmp_treeviewValidator, customDateEditorscmp_treeviewController)){ 
			return "/master/components/cmp_treeview"; 
		}

	 callGotoCalculabletable(request,cmp_treeviewForm );
											// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.application.datagrid.uc32_calculabletable.UC32_calculableTableForm or not from howto_calculabletable
			final UC32_calculableTableForm uC32_calculableTableForm =  new UC32_calculableTableForm();
			// Populate the destination form with all the instances returned from howto_calculabletable.
			final IForm uC32_calculableTableForm2 = populateDestinationForm(request.getSession(), uC32_calculableTableForm); 
					
			// Add the form to the model.
			model.addAttribute("uC32_calculableTableForm", uC32_calculableTableForm2); 
			
			request.getSession().setAttribute("uC32_calculableTableForm", uC32_calculableTableForm2);
			
			// "OK" CASE => destination screen path from howto_calculabletable
			LOGGER.info("Go to the screen 'UC32_calculableTable'.");
			// Redirect (PRG) from howto_calculabletable
			destinationPath =  "redirect:/presentation/standardfeatures/application/datagrid/uc32_calculabletable/UC32_calculableTable.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : howto_calculabletable 
	}
	
				/**
	 * Operation : howto_selectedList
 	 * @param model : 
 	 * @param cmp_treeview : The form
 	 * @return
	 */
	@RequestMapping(value = "/cmp_treeview/howto_selectedList.html",method = RequestMethod.POST)
	public String howto_selectedList(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("cmp_treeviewForm") cmp_treeviewForm  cmp_treeviewForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : howto_selectedList"); 
		infoLogger(cmp_treeviewForm); 
		cmp_treeviewForm.setAlwaysCallPreControllers(false); 
		// initialization for howto_selectedList
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state howto_selectedList if the validation fail, you will be forward to the screen : "/master/components/cmp_treeview"
		if(errorsMessages(request,response,cmp_treeviewForm,bindingResult,"", cmp_treeviewValidator, customDateEditorscmp_treeviewController)){ 
			return "/master/components/cmp_treeview"; 
		}

	 callGotoSelectedList(request,cmp_treeviewForm );
											// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.application.datagrid.uc38_selectedlist.UC38_SelectedListForm or not from howto_selectedList
			final UC38_SelectedListForm uC38_SelectedListForm =  new UC38_SelectedListForm();
			// Populate the destination form with all the instances returned from howto_selectedList.
			final IForm uC38_SelectedListForm2 = populateDestinationForm(request.getSession(), uC38_SelectedListForm); 
					
			// Add the form to the model.
			model.addAttribute("uC38_SelectedListForm", uC38_SelectedListForm2); 
			
			request.getSession().setAttribute("uC38_SelectedListForm", uC38_SelectedListForm2);
			
			// "OK" CASE => destination screen path from howto_selectedList
			LOGGER.info("Go to the screen 'UC38_SelectedList'.");
			// Redirect (PRG) from howto_selectedList
			destinationPath =  "redirect:/presentation/standardfeatures/application/datagrid/uc38_selectedlist/UC38_SelectedList.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : howto_selectedList 
	}
	
				/**
	 * Operation : howto_ajaxcall
 	 * @param model : 
 	 * @param cmp_treeview : The form
 	 * @return
	 */
	@RequestMapping(value = "/cmp_treeview/howto_ajaxcall.html",method = RequestMethod.POST)
	public String howto_ajaxcall(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("cmp_treeviewForm") cmp_treeviewForm  cmp_treeviewForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : howto_ajaxcall"); 
		infoLogger(cmp_treeviewForm); 
		cmp_treeviewForm.setAlwaysCallPreControllers(false); 
		// initialization for howto_ajaxcall
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state howto_ajaxcall if the validation fail, you will be forward to the screen : "/master/components/cmp_treeview"
		if(errorsMessages(request,response,cmp_treeviewForm,bindingResult,"", cmp_treeviewValidator, customDateEditorscmp_treeviewController)){ 
			return "/master/components/cmp_treeview"; 
		}

	 callGotoAjaxCall(request,cmp_treeviewForm );
											// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.services.ajax.uc50_ajaxcall.UC50_AjaxCallForm or not from howto_ajaxcall
			final UC50_AjaxCallForm uC50_AjaxCallForm =  new UC50_AjaxCallForm();
			// Populate the destination form with all the instances returned from howto_ajaxcall.
			final IForm uC50_AjaxCallForm2 = populateDestinationForm(request.getSession(), uC50_AjaxCallForm); 
					
			// Add the form to the model.
			model.addAttribute("uC50_AjaxCallForm", uC50_AjaxCallForm2); 
			
			request.getSession().setAttribute("uC50_AjaxCallForm", uC50_AjaxCallForm2);
			
			// "OK" CASE => destination screen path from howto_ajaxcall
			LOGGER.info("Go to the screen 'UC50_AjaxCall'.");
			// Redirect (PRG) from howto_ajaxcall
			destinationPath =  "redirect:/presentation/standardfeatures/services/ajax/uc50_ajaxcall/UC50_AjaxCall.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : howto_ajaxcall 
	}
	
				/**
	 * Operation : howto_dropdown
 	 * @param model : 
 	 * @param cmp_treeview : The form
 	 * @return
	 */
	@RequestMapping(value = "/cmp_treeview/howto_dropdown.html",method = RequestMethod.POST)
	public String howto_dropdown(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("cmp_treeviewForm") cmp_treeviewForm  cmp_treeviewForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : howto_dropdown"); 
		infoLogger(cmp_treeviewForm); 
		cmp_treeviewForm.setAlwaysCallPreControllers(false); 
		// initialization for howto_dropdown
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state howto_dropdown if the validation fail, you will be forward to the screen : "/master/components/cmp_treeview"
		if(errorsMessages(request,response,cmp_treeviewForm,bindingResult,"", cmp_treeviewValidator, customDateEditorscmp_treeviewController)){ 
			return "/master/components/cmp_treeview"; 
		}

	 callGotoActiononDropDownSelection(request,cmp_treeviewForm );
											// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.application.form.uc41_actionondropdownselection.UC41_DropDownActionForm or not from howto_dropdown
			final UC41_DropDownActionForm uC41_DropDownActionForm =  new UC41_DropDownActionForm();
			// Populate the destination form with all the instances returned from howto_dropdown.
			final IForm uC41_DropDownActionForm2 = populateDestinationForm(request.getSession(), uC41_DropDownActionForm); 
					
			// Add the form to the model.
			model.addAttribute("uC41_DropDownActionForm", uC41_DropDownActionForm2); 
			
			request.getSession().setAttribute("uC41_DropDownActionForm", uC41_DropDownActionForm2);
			
			// "OK" CASE => destination screen path from howto_dropdown
			LOGGER.info("Go to the screen 'UC41_DropDownAction'.");
			// Redirect (PRG) from howto_dropdown
			destinationPath =  "redirect:/presentation/standardfeatures/application/form/uc41_actionondropdownselection/UC41_DropDownAction.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : howto_dropdown 
	}
	
				/**
	 * Operation : howto_contents
 	 * @param model : 
 	 * @param cmp_treeview : The form
 	 * @return
	 */
	@RequestMapping(value = "/cmp_treeview/howto_contents.html",method = RequestMethod.POST)
	public String howto_contents(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("cmp_treeviewForm") cmp_treeviewForm  cmp_treeviewForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : howto_contents"); 
		infoLogger(cmp_treeviewForm); 
		cmp_treeviewForm.setAlwaysCallPreControllers(false); 
		// initialization for howto_contents
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state howto_contents if the validation fail, you will be forward to the screen : "/master/components/cmp_treeview"
		if(errorsMessages(request,response,cmp_treeviewForm,bindingResult,"", cmp_treeviewValidator, customDateEditorscmp_treeviewController)){ 
			return "/master/components/cmp_treeview"; 
		}

	 callInitializeScreen47(request,cmp_treeviewForm );
			
 callFindplayers(request,cmp_treeviewForm );
											// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.application.component.uc47_contents.UC47_ContentsForm or not from howto_contents
			final UC47_ContentsForm uC47_ContentsForm =  new UC47_ContentsForm();
			// Populate the destination form with all the instances returned from howto_contents.
			final IForm uC47_ContentsForm2 = populateDestinationForm(request.getSession(), uC47_ContentsForm); 
					
			// Add the form to the model.
			model.addAttribute("uC47_ContentsForm", uC47_ContentsForm2); 
			
			request.getSession().setAttribute("uC47_ContentsForm", uC47_ContentsForm2);
			
			// "OK" CASE => destination screen path from howto_contents
			LOGGER.info("Go to the screen 'UC47_Contents'.");
			// Redirect (PRG) from howto_contents
			destinationPath =  "redirect:/presentation/standardfeatures/application/component/uc47_contents/UC47_Contents.html";
						
						
	
	
		return destinationPath; // Returns the destination path for the state : howto_contents 
	}
	
				/**
	 * Operation : howto_autotable
 	 * @param model : 
 	 * @param cmp_treeview : The form
 	 * @return
	 */
	@RequestMapping(value = "/cmp_treeview/howto_autotable.html",method = RequestMethod.POST)
	public String howto_autotable(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("cmp_treeviewForm") cmp_treeviewForm  cmp_treeviewForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : howto_autotable"); 
		infoLogger(cmp_treeviewForm); 
		cmp_treeviewForm.setAlwaysCallPreControllers(false); 
		// initialization for howto_autotable
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state howto_autotable if the validation fail, you will be forward to the screen : "/master/components/cmp_treeview"
		if(errorsMessages(request,response,cmp_treeviewForm,bindingResult,"", cmp_treeviewValidator, customDateEditorscmp_treeviewController)){ 
			return "/master/components/cmp_treeview"; 
		}

	 callGotoAutocompleteTable(request,cmp_treeviewForm );
											// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.application.form.uc44_autocompletetable.UC44_HomePageForm or not from howto_autotable
			final UC44_HomePageForm uC44_HomePageForm =  new UC44_HomePageForm();
			// Populate the destination form with all the instances returned from howto_autotable.
			final IForm uC44_HomePageForm2 = populateDestinationForm(request.getSession(), uC44_HomePageForm); 
					
			// Add the form to the model.
			model.addAttribute("uC44_HomePageForm", uC44_HomePageForm2); 
			
			request.getSession().setAttribute("uC44_HomePageForm", uC44_HomePageForm2);
			
			// "OK" CASE => destination screen path from howto_autotable
			LOGGER.info("Go to the screen 'UC44_HomePage'.");
			// Redirect (PRG) from howto_autotable
			destinationPath =  "redirect:/presentation/standardfeatures/application/form/uc44_autocompletetable/UC44_HomePage.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : howto_autotable 
	}
	
				/**
	 * Operation : howto_package
 	 * @param model : 
 	 * @param cmp_treeview : The form
 	 * @return
	 */
	@RequestMapping(value = "/cmp_treeview/howto_package.html",method = RequestMethod.POST)
	public String howto_package(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("cmp_treeviewForm") cmp_treeviewForm  cmp_treeviewForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : howto_package"); 
		infoLogger(cmp_treeviewForm); 
		cmp_treeviewForm.setAlwaysCallPreControllers(false); 
		// initialization for howto_package
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state howto_package if the validation fail, you will be forward to the screen : "/master/components/cmp_treeview"
		if(errorsMessages(request,response,cmp_treeviewForm,bindingResult,"", cmp_treeviewValidator, customDateEditorscmp_treeviewController)){ 
			return "/master/components/cmp_treeview"; 
		}

	 callGotoPackageScope(request,cmp_treeviewForm );
											// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.application.navigation.uc28_packagescope.package_one.UC28_TeamsForm or not from howto_package
			final UC28_TeamsForm uC28_TeamsForm =  new UC28_TeamsForm();
			// Populate the destination form with all the instances returned from howto_package.
			final IForm uC28_TeamsForm2 = populateDestinationForm(request.getSession(), uC28_TeamsForm); 
					
			// Add the form to the model.
			model.addAttribute("uC28_TeamsForm", uC28_TeamsForm2); 
			
			request.getSession().setAttribute("uC28_TeamsForm", uC28_TeamsForm2);
			
			// "OK" CASE => destination screen path from howto_package
			LOGGER.info("Go to the screen 'UC28_Teams'.");
			// Redirect (PRG) from howto_package
			destinationPath =  "redirect:/presentation/standardfeatures/application/navigation/uc28_packagescope/package_one/UC28_Teams.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : howto_package 
	}
	
				/**
	 * Operation : howto_global
 	 * @param model : 
 	 * @param cmp_treeview : The form
 	 * @return
	 */
	@RequestMapping(value = "/cmp_treeview/howto_global.html",method = RequestMethod.POST)
	public String howto_global(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("cmp_treeviewForm") cmp_treeviewForm  cmp_treeviewForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : howto_global"); 
		infoLogger(cmp_treeviewForm); 
		cmp_treeviewForm.setAlwaysCallPreControllers(false); 
		// initialization for howto_global
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state howto_global if the validation fail, you will be forward to the screen : "/master/components/cmp_treeview"
		if(errorsMessages(request,response,cmp_treeviewForm,bindingResult,"", cmp_treeviewValidator, customDateEditorscmp_treeviewController)){ 
			return "/master/components/cmp_treeview"; 
		}

	 callGotoGlobalScope(request,cmp_treeviewForm );
											// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.application.navigation.uc29_globalscope.package_one.UC29_TeamsForm or not from howto_global
			final UC29_TeamsForm uC29_TeamsForm =  new UC29_TeamsForm();
			// Populate the destination form with all the instances returned from howto_global.
			final IForm uC29_TeamsForm2 = populateDestinationForm(request.getSession(), uC29_TeamsForm); 
					
			// Add the form to the model.
			model.addAttribute("uC29_TeamsForm", uC29_TeamsForm2); 
			
			request.getSession().setAttribute("uC29_TeamsForm", uC29_TeamsForm2);
			
			// "OK" CASE => destination screen path from howto_global
			LOGGER.info("Go to the screen 'UC29_Teams'.");
			// Redirect (PRG) from howto_global
			destinationPath =  "redirect:/presentation/standardfeatures/application/navigation/uc29_globalscope/package_one/UC29_Teams.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : howto_global 
	}
	
				/**
	 * Operation : howto_cachehibernate
 	 * @param model : 
 	 * @param cmp_treeview : The form
 	 * @return
	 */
	@RequestMapping(value = "/cmp_treeview/howto_cachehibernate.html",method = RequestMethod.POST)
	public String howto_cachehibernate(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("cmp_treeviewForm") cmp_treeviewForm  cmp_treeviewForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : howto_cachehibernate"); 
		infoLogger(cmp_treeviewForm); 
		cmp_treeviewForm.setAlwaysCallPreControllers(false); 
		// initialization for howto_cachehibernate
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state howto_cachehibernate if the validation fail, you will be forward to the screen : "/master/components/cmp_treeview"
		if(errorsMessages(request,response,cmp_treeviewForm,bindingResult,"", cmp_treeviewValidator, customDateEditorscmp_treeviewController)){ 
			return "/master/components/cmp_treeview"; 
		}

	 callSearchTeams(request,cmp_treeviewForm );
											// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.entities.uc69_cachehibernate.UC69_HomePageForm or not from howto_cachehibernate
			final UC69_HomePageForm uC69_HomePageForm =  new UC69_HomePageForm();
			// Populate the destination form with all the instances returned from howto_cachehibernate.
			final IForm uC69_HomePageForm2 = populateDestinationForm(request.getSession(), uC69_HomePageForm); 
					
			// Add the form to the model.
			model.addAttribute("uC69_HomePageForm", uC69_HomePageForm2); 
			
			request.getSession().setAttribute("uC69_HomePageForm", uC69_HomePageForm2);
			
			// "OK" CASE => destination screen path from howto_cachehibernate
			LOGGER.info("Go to the screen 'UC69_HomePage'.");
			// Redirect (PRG) from howto_cachehibernate
			destinationPath =  "redirect:/presentation/standardfeatures/entities/uc69_cachehibernate/UC69_HomePage.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : howto_cachehibernate 
	}
	
				/**
	 * Operation : howto_generator
 	 * @param model : 
 	 * @param cmp_treeview : The form
 	 * @return
	 */
	@RequestMapping(value = "/cmp_treeview/howto_generator.html",method = RequestMethod.POST)
	public String howto_generator(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("cmp_treeviewForm") cmp_treeviewForm  cmp_treeviewForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : howto_generator"); 
		infoLogger(cmp_treeviewForm); 
		cmp_treeviewForm.setAlwaysCallPreControllers(false); 
		// initialization for howto_generator
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state howto_generator if the validation fail, you will be forward to the screen : "/master/components/cmp_treeview"
		if(errorsMessages(request,response,cmp_treeviewForm,bindingResult,"", cmp_treeviewValidator, customDateEditorscmp_treeviewController)){ 
			return "/master/components/cmp_treeview"; 
		}

	 callGotoGeneratorClass(request,cmp_treeviewForm );
											// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.entities.uc72_generatorclass.UC72_HomeForm or not from howto_generator
			final UC72_HomeForm uC72_HomeForm =  new UC72_HomeForm();
			// Populate the destination form with all the instances returned from howto_generator.
			final IForm uC72_HomeForm2 = populateDestinationForm(request.getSession(), uC72_HomeForm); 
					
			// Add the form to the model.
			model.addAttribute("uC72_HomeForm", uC72_HomeForm2); 
			
			request.getSession().setAttribute("uC72_HomeForm", uC72_HomeForm2);
			
			// "OK" CASE => destination screen path from howto_generator
			LOGGER.info("Go to the screen 'UC72_Home'.");
			// Redirect (PRG) from howto_generator
			destinationPath =  "redirect:/presentation/standardfeatures/entities/uc72_generatorclass/UC72_Home.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : howto_generator 
	}
	
				/**
	 * Operation : howto_valueobject
 	 * @param model : 
 	 * @param cmp_treeview : The form
 	 * @return
	 */
	@RequestMapping(value = "/cmp_treeview/howto_valueobject.html",method = RequestMethod.POST)
	public String howto_valueobject(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("cmp_treeviewForm") cmp_treeviewForm  cmp_treeviewForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : howto_valueobject"); 
		infoLogger(cmp_treeviewForm); 
		cmp_treeviewForm.setAlwaysCallPreControllers(false); 
		// initialization for howto_valueobject
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state howto_valueobject if the validation fail, you will be forward to the screen : "/master/components/cmp_treeview"
		if(errorsMessages(request,response,cmp_treeviewForm,bindingResult,"", cmp_treeviewValidator, customDateEditorscmp_treeviewController)){ 
			return "/master/components/cmp_treeview"; 
		}

	 callGotoValueObject(request,cmp_treeviewForm );
											// Init the destination form com.bluage.documentation.web.presentation.advancedfeatures.valueobject.uc206_valueobject.UC206_HomePageForm or not from howto_valueobject
			final UC206_HomePageForm uC206_HomePageForm =  new UC206_HomePageForm();
			// Populate the destination form with all the instances returned from howto_valueobject.
			final IForm uC206_HomePageForm2 = populateDestinationForm(request.getSession(), uC206_HomePageForm); 
					
			// Add the form to the model.
			model.addAttribute("uC206_HomePageForm", uC206_HomePageForm2); 
			
			request.getSession().setAttribute("uC206_HomePageForm", uC206_HomePageForm2);
			
			// "OK" CASE => destination screen path from howto_valueobject
			LOGGER.info("Go to the screen 'UC206_HomePage'.");
			// Redirect (PRG) from howto_valueobject
			destinationPath =  "redirect:/presentation/advancedfeatures/valueobject/uc206_valueobject/UC206_HomePage.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : howto_valueobject 
	}
	
				/**
	 * Operation : howto_popup
 	 * @param model : 
 	 * @param cmp_treeview : The form
 	 * @return
	 */
	@RequestMapping(value = "/cmp_treeview/howto_popup.html",method = RequestMethod.POST)
	public String howto_popup(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("cmp_treeviewForm") cmp_treeviewForm  cmp_treeviewForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : howto_popup"); 
		infoLogger(cmp_treeviewForm); 
		cmp_treeviewForm.setAlwaysCallPreControllers(false); 
		// initialization for howto_popup
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state howto_popup if the validation fail, you will be forward to the screen : "/master/components/cmp_treeview"
		if(errorsMessages(request,response,cmp_treeviewForm,bindingResult,"", cmp_treeviewValidator, customDateEditorscmp_treeviewController)){ 
			return "/master/components/cmp_treeview"; 
		}

	 callGotopopup(request,cmp_treeviewForm );
											// Init the destination form com.bluage.documentation.web.presentation.advancedfeatures.advancedtagsuse.uc205_popup.UC205_HomeForm or not from howto_popup
			final UC205_HomeForm uC205_HomeForm =  new UC205_HomeForm();
			// Populate the destination form with all the instances returned from howto_popup.
			final IForm uC205_HomeForm2 = populateDestinationForm(request.getSession(), uC205_HomeForm); 
					
			// Add the form to the model.
			model.addAttribute("uC205_HomeForm", uC205_HomeForm2); 
			
			request.getSession().setAttribute("uC205_HomeForm", uC205_HomeForm2);
			
			// "OK" CASE => destination screen path from howto_popup
			LOGGER.info("Go to the screen 'UC205_Home'.");
			// Redirect (PRG) from howto_popup
			destinationPath =  "redirect:/presentation/advancedfeatures/advancedtagsuse/uc205_popup/UC205_Home.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : howto_popup 
	}
	
				/**
	 * Operation : howto_bags
 	 * @param model : 
 	 * @param cmp_treeview : The form
 	 * @return
	 */
	@RequestMapping(value = "/cmp_treeview/howto_bags.html",method = RequestMethod.POST)
	public String howto_bags(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("cmp_treeviewForm") cmp_treeviewForm  cmp_treeviewForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : howto_bags"); 
		infoLogger(cmp_treeviewForm); 
		cmp_treeviewForm.setAlwaysCallPreControllers(false); 
		// initialization for howto_bags
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state howto_bags if the validation fail, you will be forward to the screen : "/master/components/cmp_treeview"
		if(errorsMessages(request,response,cmp_treeviewForm,bindingResult,"", cmp_treeviewValidator, customDateEditorscmp_treeviewController)){ 
			return "/master/components/cmp_treeview"; 
		}

	 callGotoBAGS(request,cmp_treeviewForm );
											// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.services.bags.uc49_bags.UC49_BAGSForm or not from howto_bags
			final UC49_BAGSForm uC49_BAGSForm =  new UC49_BAGSForm();
			// Populate the destination form with all the instances returned from howto_bags.
			final IForm uC49_BAGSForm2 = populateDestinationForm(request.getSession(), uC49_BAGSForm); 
					
			// Add the form to the model.
			model.addAttribute("uC49_BAGSForm", uC49_BAGSForm2); 
			
			request.getSession().setAttribute("uC49_BAGSForm", uC49_BAGSForm2);
			
			// "OK" CASE => destination screen path from howto_bags
			LOGGER.info("Go to the screen 'UC49_BAGS'.");
			// Redirect (PRG) from howto_bags
			destinationPath =  "redirect:/presentation/standardfeatures/services/bags/uc49_bags/UC49_BAGS.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : howto_bags 
	}
	
				/**
	 * Operation : lnk_home
 	 * @param model : 
 	 * @param cmp_treeview : The form
 	 * @return
	 */
	@RequestMapping(value = "/cmp_treeview/lnk_home.html",method = RequestMethod.POST)
	public String lnk_home(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("cmp_treeviewForm") cmp_treeviewForm  cmp_treeviewForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : lnk_home"); 
		infoLogger(cmp_treeviewForm); 
		cmp_treeviewForm.setAlwaysCallPreControllers(false); 
		// initialization for lnk_home
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state lnk_home if the validation fail, you will be forward to the screen : "/master/components/cmp_treeview"
		if(errorsMessages(request,response,cmp_treeviewForm,bindingResult,"", cmp_treeviewValidator, customDateEditorscmp_treeviewController)){ 
			return "/master/components/cmp_treeview"; 
		}

	 callGotoHome(request,cmp_treeviewForm );
											// Init the destination form com.bluage.documentation.web.presentation.common.home.homeForm or not from lnk_home
			final homeForm homeForm =  new homeForm();
			// Populate the destination form with all the instances returned from lnk_home.
			final IForm homeForm2 = populateDestinationForm(request.getSession(), homeForm); 
					
			// Add the form to the model.
			model.addAttribute("homeForm", homeForm2); 
			
			request.getSession().setAttribute("homeForm", homeForm2);
			
			// "OK" CASE => destination screen path from lnk_home
			LOGGER.info("Go to the screen 'home'.");
			// Redirect (PRG) from lnk_home
			destinationPath =  "redirect:/presentation/common/home/home.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_home 
	}
	
				/**
	 * Operation : howto_timeout
 	 * @param model : 
 	 * @param cmp_treeview : The form
 	 * @return
	 */
	@RequestMapping(value = "/cmp_treeview/howto_timeout.html",method = RequestMethod.POST)
	public String howto_timeout(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("cmp_treeviewForm") cmp_treeviewForm  cmp_treeviewForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : howto_timeout"); 
		infoLogger(cmp_treeviewForm); 
		cmp_treeviewForm.setAlwaysCallPreControllers(false); 
		// initialization for howto_timeout
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state howto_timeout if the validation fail, you will be forward to the screen : "/master/components/cmp_treeview"
		if(errorsMessages(request,response,cmp_treeviewForm,bindingResult,"", cmp_treeviewValidator, customDateEditorscmp_treeviewController)){ 
			return "/master/components/cmp_treeview"; 
		}

	 callGotoTimeOut(request,cmp_treeviewForm );
											// Init the destination form com.bluage.documentation.web.presentation.advancedfeatures.filters.uc220_timeout.UC220_TimeOutForm or not from howto_timeout
			final UC220_TimeOutForm uC220_TimeOutForm =  new UC220_TimeOutForm();
			// Populate the destination form with all the instances returned from howto_timeout.
			final IForm uC220_TimeOutForm2 = populateDestinationForm(request.getSession(), uC220_TimeOutForm); 
					
			// Add the form to the model.
			model.addAttribute("uC220_TimeOutForm", uC220_TimeOutForm2); 
			
			request.getSession().setAttribute("uC220_TimeOutForm", uC220_TimeOutForm2);
			
			// "OK" CASE => destination screen path from howto_timeout
			LOGGER.info("Go to the screen 'UC220_TimeOut'.");
			// Redirect (PRG) from howto_timeout
			destinationPath =  "redirect:/presentation/advancedfeatures/filters/uc220_timeout/UC220_TimeOut.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : howto_timeout 
	}
	
				/**
	 * Operation : howto_tables
 	 * @param model : 
 	 * @param cmp_treeview : The form
 	 * @return
	 */
	@RequestMapping(value = "/cmp_treeview/howto_tables.html",method = RequestMethod.POST)
	public String howto_tables(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("cmp_treeviewForm") cmp_treeviewForm  cmp_treeviewForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : howto_tables"); 
		infoLogger(cmp_treeviewForm); 
		cmp_treeviewForm.setAlwaysCallPreControllers(false); 
		// initialization for howto_tables
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state howto_tables if the validation fail, you will be forward to the screen : "/master/components/cmp_treeview"
		if(errorsMessages(request,response,cmp_treeviewForm,bindingResult,"", cmp_treeviewValidator, customDateEditorscmp_treeviewController)){ 
			return "/master/components/cmp_treeview"; 
		}

	 callGotoTables(request,cmp_treeviewForm );
											// Init the destination form com.bluage.documentation.web.presentation.qauc.uc1001_tables.UC1001_TablesForm or not from howto_tables
			final UC1001_TablesForm uC1001_TablesForm =  new UC1001_TablesForm();
			// Populate the destination form with all the instances returned from howto_tables.
			final IForm uC1001_TablesForm2 = populateDestinationForm(request.getSession(), uC1001_TablesForm); 
					
			// Add the form to the model.
			model.addAttribute("uC1001_TablesForm", uC1001_TablesForm2); 
			
			request.getSession().setAttribute("uC1001_TablesForm", uC1001_TablesForm2);
			
			// "OK" CASE => destination screen path from howto_tables
			LOGGER.info("Go to the screen 'UC1001_Tables'.");
			// Redirect (PRG) from howto_tables
			destinationPath =  "redirect:/presentation/qauc/uc1001_tables/UC1001_Tables.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : howto_tables 
	}
	
				/**
	 * Operation : howto_validators
 	 * @param model : 
 	 * @param cmp_treeview : The form
 	 * @return
	 */
	@RequestMapping(value = "/cmp_treeview/howto_validators.html",method = RequestMethod.POST)
	public String howto_validators(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("cmp_treeviewForm") cmp_treeviewForm  cmp_treeviewForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : howto_validators"); 
		infoLogger(cmp_treeviewForm); 
		cmp_treeviewForm.setAlwaysCallPreControllers(false); 
		// initialization for howto_validators
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state howto_validators if the validation fail, you will be forward to the screen : "/master/components/cmp_treeview"
		if(errorsMessages(request,response,cmp_treeviewForm,bindingResult,"", cmp_treeviewValidator, customDateEditorscmp_treeviewController)){ 
			return "/master/components/cmp_treeview"; 
		}

	 callGotoValidators(request,cmp_treeviewForm );
											// Init the destination form com.bluage.documentation.web.presentation.qauc.uc1002_validators.UC1002_ValidatorsForm or not from howto_validators
			final UC1002_ValidatorsForm uC1002_ValidatorsForm =  new UC1002_ValidatorsForm();
			// Populate the destination form with all the instances returned from howto_validators.
			final IForm uC1002_ValidatorsForm2 = populateDestinationForm(request.getSession(), uC1002_ValidatorsForm); 
					
			// Add the form to the model.
			model.addAttribute("uC1002_ValidatorsForm", uC1002_ValidatorsForm2); 
			
			request.getSession().setAttribute("uC1002_ValidatorsForm", uC1002_ValidatorsForm2);
			
			// "OK" CASE => destination screen path from howto_validators
			LOGGER.info("Go to the screen 'UC1002_Validators'.");
			// Redirect (PRG) from howto_validators
			destinationPath =  "redirect:/presentation/qauc/uc1002_validators/UC1002_Validators.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : howto_validators 
	}
	
				/**
	 * Operation : howto_captcha
 	 * @param model : 
 	 * @param cmp_treeview : The form
 	 * @return
	 */
	@RequestMapping(value = "/cmp_treeview/howto_captcha.html",method = RequestMethod.POST)
	public String howto_captcha(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("cmp_treeviewForm") cmp_treeviewForm  cmp_treeviewForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : howto_captcha"); 
		infoLogger(cmp_treeviewForm); 
		cmp_treeviewForm.setAlwaysCallPreControllers(false); 
		// initialization for howto_captcha
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state howto_captcha if the validation fail, you will be forward to the screen : "/master/components/cmp_treeview"
		if(errorsMessages(request,response,cmp_treeviewForm,bindingResult,"", cmp_treeviewValidator, customDateEditorscmp_treeviewController)){ 
			return "/master/components/cmp_treeview"; 
		}

	 callGotoCaptcha(request,cmp_treeviewForm );
											// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.application.form.uc43_captcha.UC43_AddPlayerForm or not from howto_captcha
			final UC43_AddPlayerForm uC43_AddPlayerForm =  new UC43_AddPlayerForm();
			// Populate the destination form with all the instances returned from howto_captcha.
			final IForm uC43_AddPlayerForm2 = populateDestinationForm(request.getSession(), uC43_AddPlayerForm); 
					
			// Add the form to the model.
			model.addAttribute("uC43_AddPlayerForm", uC43_AddPlayerForm2); 
			
			request.getSession().setAttribute("uC43_AddPlayerForm", uC43_AddPlayerForm2);
			
			// "OK" CASE => destination screen path from howto_captcha
			LOGGER.info("Go to the screen 'UC43_AddPlayer'.");
			// Redirect (PRG) from howto_captcha
			destinationPath =  "redirect:/presentation/standardfeatures/application/form/uc43_captcha/UC43_AddPlayer.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : howto_captcha 
	}
	
	
	/**
	* This method initialise the form : cmp_treeviewForm 
	* @return cmp_treeviewForm
	*/
	@ModelAttribute("cmp_treeviewFormInit")
	public void initcmp_treeviewForm(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, CMP_TREEVIEW_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method cmp_treeviewForm."); //for howto_captcha 
		}
		cmp_treeviewForm cmp_treeviewForm;
	
		if(request.getSession().getAttribute(CMP_TREEVIEW_FORM) != null){
			cmp_treeviewForm = (cmp_treeviewForm)request.getSession().getAttribute(CMP_TREEVIEW_FORM);
		} else {
			cmp_treeviewForm = new cmp_treeviewForm();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form cmp_treeviewForm.");
		}
		cmp_treeviewForm = (cmp_treeviewForm)populateDestinationForm(request.getSession(), cmp_treeviewForm);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  cmp_treeviewForm.");
		}
		model.addAttribute(CMP_TREEVIEW_FORM, cmp_treeviewForm);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param cmp_treeviewForm : The sceen form.
	 */
	@RequestMapping(value = "/cmp_treeview.html" ,method = RequestMethod.GET)
	public void preparecmp_treeview(final HttpServletRequest request,final  Model model,final  @ModelAttribute("cmp_treeviewFormInit") cmp_treeviewForm cmp_treeviewForm){
		
		cmp_treeviewForm currentcmp_treeviewForm = cmp_treeviewForm;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(CMP_TREEVIEW_FORM) == null){
			if(currentcmp_treeviewForm!=null){
				request.getSession().setAttribute(CMP_TREEVIEW_FORM, currentcmp_treeviewForm);
			}else {
				currentcmp_treeviewForm = new cmp_treeviewForm();
				request.getSession().setAttribute(CMP_TREEVIEW_FORM, currentcmp_treeviewForm);	
			}
		} else {
			currentcmp_treeviewForm = (cmp_treeviewForm) request.getSession().getAttribute(CMP_TREEVIEW_FORM);
		}

		currentcmp_treeviewForm = (cmp_treeviewForm)populateDestinationForm(request.getSession(), currentcmp_treeviewForm);
		// Call all the Precontroller.
	currentcmp_treeviewForm.setAlwaysCallPreControllers(true);
		model.addAttribute(CMP_TREEVIEW_FORM, currentcmp_treeviewForm);
		request.getSession().setAttribute(CMP_TREEVIEW_FORM, currentcmp_treeviewForm);
		
	}
	
	/**
	 * method callGotohelloworld
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callGotohelloworld(HttpServletRequest request,IForm form) {
					try {
				// executing Gotohelloworld in howto_captcha
	serviceUtility.doNothing(
	);  

								// processing variables Gotohelloworld in howto_captcha

			} catch (ApplicationException e) { 
				// error handling for operation doNothing called Gotohelloworld
				errorLogger("An error occured during the execution of the operation : doNothing",e); 
		
			}
	}
		/**
	 * method callGotoFormValidation
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callGotoFormValidation(HttpServletRequest request,IForm form) {
					try {
				// executing GotoFormValidation in howto_captcha
	serviceUtility.doNothing(
	);  

								// processing variables GotoFormValidation in howto_captcha

			} catch (ApplicationException e) { 
				// error handling for operation doNothing called GotoFormValidation
				errorLogger("An error occured during the execution of the operation : doNothing",e); 
		
			}
	}
		/**
	 * method callGotoEntitiesModification
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callGotoEntitiesModification(HttpServletRequest request,IForm form) {
					try {
				// executing GotoEntitiesModification in howto_captcha
	serviceUtility.doNothing(
	);  

								// processing variables GotoEntitiesModification in howto_captcha

			} catch (ApplicationException e) { 
				// error handling for operation doNothing called GotoEntitiesModification
				errorLogger("An error occured during the execution of the operation : doNothing",e); 
		
			}
	}
		/**
	 * method callGotoSearch
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callGotoSearch(HttpServletRequest request,IForm form) {
					try {
				// executing GotoSearch in howto_captcha
	serviceUtility.doNothing(
	);  

								// processing variables GotoSearch in howto_captcha

			} catch (ApplicationException e) { 
				// error handling for operation doNothing called GotoSearch
				errorLogger("An error occured during the execution of the operation : doNothing",e); 
		
			}
	}
		/**
	 * method callGotoNavigation
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callGotoNavigation(HttpServletRequest request,IForm form) {
					try {
				// executing GotoNavigation in howto_captcha
	serviceUtility.doNothing(
	);  

								// processing variables GotoNavigation in howto_captcha

			} catch (ApplicationException e) { 
				// error handling for operation doNothing called GotoNavigation
				errorLogger("An error occured during the execution of the operation : doNothing",e); 
		
			}
	}
		/**
	 * method callGotoEditeTable
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callGotoEditeTable(HttpServletRequest request,IForm form) {
					try {
				// executing GotoEditeTable in howto_captcha
	serviceUtility.doNothing(
	);  

								// processing variables GotoEditeTable in howto_captcha

			} catch (ApplicationException e) { 
				// error handling for operation doNothing called GotoEditeTable
				errorLogger("An error occured during the execution of the operation : doNothing",e); 
		
			}
	}
		/**
	 * method callGotoServerPaginatedTable
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callGotoServerPaginatedTable(HttpServletRequest request,IForm form) {
					try {
				// executing GotoServerPaginatedTable in howto_captcha
	serviceUtility.doNothing(
	);  

								// processing variables GotoServerPaginatedTable in howto_captcha

			} catch (ApplicationException e) { 
				// error handling for operation doNothing called GotoServerPaginatedTable
				errorLogger("An error occured during the execution of the operation : doNothing",e); 
		
			}
	}
		/**
	 * method callGotoCollectionOrder
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callGotoCollectionOrder(HttpServletRequest request,IForm form) {
					try {
				// executing GotoCollectionOrder in howto_captcha
	serviceUtility.doNothing(
	);  

								// processing variables GotoCollectionOrder in howto_captcha

			} catch (ApplicationException e) { 
				// error handling for operation doNothing called GotoCollectionOrder
				errorLogger("An error occured during the execution of the operation : doNothing",e); 
		
			}
	}
		/**
	 * method callGoToRepeater
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callGoToRepeater(HttpServletRequest request,IForm form) {
					try {
				// executing GoToRepeater in howto_captcha
	serviceUtility.doNothing(
	);  

								// processing variables GoToRepeater in howto_captcha

			} catch (ApplicationException e) { 
				// error handling for operation doNothing called GoToRepeater
				errorLogger("An error occured during the execution of the operation : doNothing",e); 
		
			}
	}
		/**
	 * method callGotoConditionalProcess
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callGotoConditionalProcess(HttpServletRequest request,IForm form) {
					try {
				// executing GotoConditionalProcess in howto_captcha
	serviceUtility.doNothing(
	);  

								// processing variables GotoConditionalProcess in howto_captcha

			} catch (ApplicationException e) { 
				// error handling for operation doNothing called GotoConditionalProcess
				errorLogger("An error occured during the execution of the operation : doNothing",e); 
		
			}
	}
		/**
	 * method callGotoHeritageSubclassUseCase
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callGotoHeritageSubclassUseCase(HttpServletRequest request,IForm form) {
					try {
				// executing GotoHeritageSubclassUseCase in howto_captcha
	serviceUtility.doNothing(
	);  

								// processing variables GotoHeritageSubclassUseCase in howto_captcha

			} catch (ApplicationException e) { 
				// error handling for operation doNothing called GotoHeritageSubclassUseCase
				errorLogger("An error occured during the execution of the operation : doNothing",e); 
		
			}
	}
		/**
	 * method callGOtoUMLtoSQL
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callGOtoUMLtoSQL(HttpServletRequest request,IForm form) {
					try {
				// executing GOtoUMLtoSQL in howto_captcha
	serviceUtility.doNothing(
	);  

								// processing variables GOtoUMLtoSQL in howto_captcha

			} catch (ApplicationException e) { 
				// error handling for operation doNothing called GOtoUMLtoSQL
				errorLogger("An error occured during the execution of the operation : doNothing",e); 
		
			}
	}
		/**
	 * method callGotoForProcess
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callGotoForProcess(HttpServletRequest request,IForm form) {
					try {
				// executing GotoForProcess in howto_captcha
	serviceUtility.doNothing(
	);  

								// processing variables GotoForProcess in howto_captcha

			} catch (ApplicationException e) { 
				// error handling for operation doNothing called GotoForProcess
				errorLogger("An error occured during the execution of the operation : doNothing",e); 
		
			}
	}
		/**
	 * method callGotoWhileProcess
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callGotoWhileProcess(HttpServletRequest request,IForm form) {
					try {
				// executing GotoWhileProcess in howto_captcha
	serviceUtility.doNothing(
	);  

								// processing variables GotoWhileProcess in howto_captcha

			} catch (ApplicationException e) { 
				// error handling for operation doNothing called GotoWhileProcess
				errorLogger("An error occured during the execution of the operation : doNothing",e); 
		
			}
	}
		/**
	 * method callGotoComponentPage1
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callGotoComponentPage1(HttpServletRequest request,IForm form) {
					try {
				// executing GotoComponentPage1 in howto_captcha
	serviceUtility.doNothing(
	);  

								// processing variables GotoComponentPage1 in howto_captcha

			} catch (ApplicationException e) { 
				// error handling for operation doNothing called GotoComponentPage1
				errorLogger("An error occured during the execution of the operation : doNothing",e); 
		
			}
	}
		/**
	 * method callGotoexception
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callGotoexception(HttpServletRequest request,IForm form) {
					try {
				// executing Gotoexception in howto_captcha
	serviceUtility.doNothing(
	);  

								// processing variables Gotoexception in howto_captcha

			} catch (ApplicationException e) { 
				// error handling for operation doNothing called Gotoexception
				errorLogger("An error occured during the execution of the operation : doNothing",e); 
		
			}
	}
		/**
	 * method callGotodatagridloading
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callGotodatagridloading(HttpServletRequest request,IForm form) {
					try {
				// executing Gotodatagridloading in howto_captcha
	serviceUtility.doNothing(
	);  

								// processing variables Gotodatagridloading in howto_captcha

			} catch (ApplicationException e) { 
				// error handling for operation doNothing called Gotodatagridloading
				errorLogger("An error occured during the execution of the operation : doNothing",e); 
		
			}
	}
		/**
	 * method callGotopreAction
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callGotopreAction(HttpServletRequest request,IForm form) {
					try {
				// executing GotopreAction in howto_captcha
	serviceUtility.doNothing(
	);  

								// processing variables GotopreAction in howto_captcha

			} catch (ApplicationException e) { 
				// error handling for operation doNothing called GotopreAction
				errorLogger("An error occured during the execution of the operation : doNothing",e); 
		
			}
	}
		/**
	 * method callGotoHQLOperation
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callGotoHQLOperation(HttpServletRequest request,IForm form) {
					try {
				// executing GotoHQLOperation in howto_captcha
	serviceUtility.doNothing(
	);  

								// processing variables GotoHQLOperation in howto_captcha

			} catch (ApplicationException e) { 
				// error handling for operation doNothing called GotoHQLOperation
				errorLogger("An error occured during the execution of the operation : doNothing",e); 
		
			}
	}
		/**
	 * method callGotoassociation
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callGotoassociation(HttpServletRequest request,IForm form) {
					try {
				// executing Gotoassociation in howto_captcha
	serviceUtility.doNothing(
	);  

								// processing variables Gotoassociation in howto_captcha

			} catch (ApplicationException e) { 
				// error handling for operation doNothing called Gotoassociation
				errorLogger("An error occured during the execution of the operation : doNothing",e); 
		
			}
	}
		/**
	 * method callGotoCascade
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callGotoCascade(HttpServletRequest request,IForm form) {
					try {
				// executing GotoCascade in howto_captcha
	serviceUtility.doNothing(
	);  

								// processing variables GotoCascade in howto_captcha

			} catch (ApplicationException e) { 
				// error handling for operation doNothing called GotoCascade
				errorLogger("An error occured during the execution of the operation : doNothing",e); 
		
			}
	}
		/**
	 * method callGotoLazy
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callGotoLazy(HttpServletRequest request,IForm form) {
					try {
				// executing GotoLazy in howto_captcha
	serviceUtility.doNothing(
	);  

								// processing variables GotoLazy in howto_captcha

			} catch (ApplicationException e) { 
				// error handling for operation doNothing called GotoLazy
				errorLogger("An error occured during the execution of the operation : doNothing",e); 
		
			}
	}
		/**
	 * method callGotoTransientobject
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callGotoTransientobject(HttpServletRequest request,IForm form) {
					try {
				// executing GotoTransientobject in howto_captcha
	serviceUtility.doNothing(
	);  

								// processing variables GotoTransientobject in howto_captcha

			} catch (ApplicationException e) { 
				// error handling for operation doNothing called GotoTransientobject
				errorLogger("An error occured during the execution of the operation : doNothing",e); 
		
			}
	}
		/**
	 * method callGotoMultiDomain
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callGotoMultiDomain(HttpServletRequest request,IForm form) {
					try {
				// executing GotoMultiDomain in howto_captcha
	serviceUtility.doNothing(
	);  

								// processing variables GotoMultiDomain in howto_captcha

			} catch (ApplicationException e) { 
				// error handling for operation doNothing called GotoMultiDomain
				errorLogger("An error occured during the execution of the operation : doNothing",e); 
		
			}
	}
		/**
	 * method callGotoservicecopy
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callGotoservicecopy(HttpServletRequest request,IForm form) {
					try {
				// executing Gotoservicecopy in howto_captcha
	serviceUtility.doNothing(
	);  

								// processing variables Gotoservicecopy in howto_captcha

			} catch (ApplicationException e) { 
				// error handling for operation doNothing called Gotoservicecopy
				errorLogger("An error occured during the execution of the operation : doNothing",e); 
		
			}
	}
		/**
	 * method callGotoPaginatorBDD
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callGotoPaginatorBDD(HttpServletRequest request,IForm form) {
					try {
				// executing GotoPaginatorBDD in howto_captcha
	serviceUtility.doNothing(
	);  

								// processing variables GotoPaginatorBDD in howto_captcha

			} catch (ApplicationException e) { 
				// error handling for operation doNothing called GotoPaginatorBDD
				errorLogger("An error occured during the execution of the operation : doNothing",e); 
		
			}
	}
		/**
	 * method callGotoAutocomplete
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callGotoAutocomplete(HttpServletRequest request,IForm form) {
					try {
				// executing GotoAutocomplete in howto_captcha
	serviceUtility.doNothing(
	);  

								// processing variables GotoAutocomplete in howto_captcha

			} catch (ApplicationException e) { 
				// error handling for operation doNothing called GotoAutocomplete
				errorLogger("An error occured during the execution of the operation : doNothing",e); 
		
			}
	}
		/**
	 * method callGoToDefaultSort
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callGoToDefaultSort(HttpServletRequest request,IForm form) {
					try {
				// executing GoToDefaultSort in howto_captcha
	serviceUtility.doNothing(
	);  

								// processing variables GoToDefaultSort in howto_captcha

			} catch (ApplicationException e) { 
				// error handling for operation doNothing called GoToDefaultSort
				errorLogger("An error occured during the execution of the operation : doNothing",e); 
		
			}
	}
		/**
	 * method callGotoHome
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callGotoHome(HttpServletRequest request,IForm form) {
					try {
				// executing GotoHome in howto_captcha
	serviceUtility.doNothing(
	);  

								// processing variables GotoHome in howto_captcha

			} catch (ApplicationException e) { 
				// error handling for operation doNothing called GotoHome
				errorLogger("An error occured during the execution of the operation : doNothing",e); 
		
			}
	}
		/**
	 * method callGoToentitiesCreation
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callGoToentitiesCreation(HttpServletRequest request,IForm form) {
		allTeams = (List)get(request.getSession(),form, "allTeams");  
										 
					try {
				// executing GoToentitiesCreation in howto_captcha
				allTeams = 	serviceTeam02.team02FindAll(
	);  
 
				put(request.getSession(), ALL_TEAMS,allTeams);
								// processing variables GoToentitiesCreation in howto_captcha

			} catch (ApplicationException e) { 
				// error handling for operation team02FindAll called GoToentitiesCreation
				errorLogger("An error occured during the execution of the operation : team02FindAll",e); 
		
			}
	}
		/**
	 * method callGoToSQLOperation
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callGoToSQLOperation(HttpServletRequest request,IForm form) {
					try {
				// executing GoToSQLOperation in howto_captcha
	serviceUtility.doNothing(
	);  

								// processing variables GoToSQLOperation in howto_captcha

			} catch (ApplicationException e) { 
				// error handling for operation doNothing called GoToSQLOperation
				errorLogger("An error occured during the execution of the operation : doNothing",e); 
		
			}
	}
		/**
	 * method callGotoJavaMelody
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callGotoJavaMelody(HttpServletRequest request,IForm form) {
					try {
				// executing GotoJavaMelody in howto_captcha
	serviceUtility.doNothing(
	);  

								// processing variables GotoJavaMelody in howto_captcha

			} catch (ApplicationException e) { 
				// error handling for operation doNothing called GotoJavaMelody
				errorLogger("An error occured during the execution of the operation : doNothing",e); 
		
			}
	}
		/**
	 * method callGotoSonar
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callGotoSonar(HttpServletRequest request,IForm form) {
					try {
				// executing GotoSonar in howto_captcha
	serviceUtility.doNothing(
	);  

								// processing variables GotoSonar in howto_captcha

			} catch (ApplicationException e) { 
				// error handling for operation doNothing called GotoSonar
				errorLogger("An error occured during the execution of the operation : doNothing",e); 
		
			}
	}
		/**
	 * method callGotoCalculabletable
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callGotoCalculabletable(HttpServletRequest request,IForm form) {
					try {
				// executing GotoCalculabletable in howto_captcha
	serviceUtility.doNothing(
	);  

								// processing variables GotoCalculabletable in howto_captcha

			} catch (ApplicationException e) { 
				// error handling for operation doNothing called GotoCalculabletable
				errorLogger("An error occured during the execution of the operation : doNothing",e); 
		
			}
	}
		/**
	 * method callGotoSelectedList
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callGotoSelectedList(HttpServletRequest request,IForm form) {
					try {
				// executing GotoSelectedList in howto_captcha
	serviceUtility.doNothing(
	);  

								// processing variables GotoSelectedList in howto_captcha

			} catch (ApplicationException e) { 
				// error handling for operation doNothing called GotoSelectedList
				errorLogger("An error occured during the execution of the operation : doNothing",e); 
		
			}
	}
		/**
	 * method callGotoAjaxCall
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callGotoAjaxCall(HttpServletRequest request,IForm form) {
					try {
				// executing GotoAjaxCall in howto_captcha
	serviceUtility.doNothing(
	);  

								// processing variables GotoAjaxCall in howto_captcha

			} catch (ApplicationException e) { 
				// error handling for operation doNothing called GotoAjaxCall
				errorLogger("An error occured during the execution of the operation : doNothing",e); 
		
			}
	}
		/**
	 * method callGotoActiononDropDownSelection
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callGotoActiononDropDownSelection(HttpServletRequest request,IForm form) {
		positions = (Position41ForSelectBO)get(request.getSession(),form, "positions");  
										 
					try {
				// executing GotoActiononDropDownSelection in howto_captcha
				positions = 	servicePosition41.loadPositions41(
	);  
 
				put(request.getSession(), POSITIONS,positions);
								// processing variables GotoActiononDropDownSelection in howto_captcha

			} catch (ApplicationException e) { 
				// error handling for operation loadPositions41 called GotoActiononDropDownSelection
				errorLogger("An error occured during the execution of the operation : loadPositions41",e); 
		
			}
	}
		/**
	 * method callInitializeScreen47
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callInitializeScreen47(HttpServletRequest request,IForm form) {
		screen47 = (Screen47BO)get(request.getSession(),form, "screen47");  
										 
					try {
				// executing InitializeScreen47 in howto_captcha
				screen47 = 	serviceScreen47.InitializeScreen47(
	);  
 
				put(request.getSession(), SCREEN47,screen47);
								// processing variables InitializeScreen47 in howto_captcha

			} catch (ApplicationException e) { 
				// error handling for operation InitializeScreen47 called InitializeScreen47
				errorLogger("An error occured during the execution of the operation : InitializeScreen47",e); 
		
			}
	}
		/**
	 * method callFindplayers
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callFindplayers(HttpServletRequest request,IForm form) {
		allPlayers = (List)get(request.getSession(),form, "allPlayers");  
										 
					try {
				// executing Findplayers in howto_captcha
				allPlayers = 	servicePlayer47.player47FindAll(
	);  
 
				put(request.getSession(), ALL_PLAYERS,allPlayers);
								// processing variables Findplayers in howto_captcha

			} catch (ApplicationException e) { 
				// error handling for operation player47FindAll called Findplayers
				errorLogger("An error occured during the execution of the operation : player47FindAll",e); 
		
			}
	}
		/**
	 * method callGotoAutocompleteTable
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callGotoAutocompleteTable(HttpServletRequest request,IForm form) {
					try {
				// executing GotoAutocompleteTable in howto_captcha
	serviceUtility.doNothing(
	);  

								// processing variables GotoAutocompleteTable in howto_captcha

			} catch (ApplicationException e) { 
				// error handling for operation doNothing called GotoAutocompleteTable
				errorLogger("An error occured during the execution of the operation : doNothing",e); 
		
			}
	}
		/**
	 * method callGotoPackageScope
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callGotoPackageScope(HttpServletRequest request,IForm form) {
		allTeams = (List)get(request.getSession(),form, "allTeams");  
										 
					try {
				// executing GotoPackageScope in howto_captcha
				allTeams = 	serviceTeam28.team28FindAll(
	);  
 
				put(request.getSession(), ALL_TEAMS,allTeams);
								// processing variables GotoPackageScope in howto_captcha

			} catch (ApplicationException e) { 
				// error handling for operation team28FindAll called GotoPackageScope
				errorLogger("An error occured during the execution of the operation : team28FindAll",e); 
		
			}
	}
		/**
	 * method callGotoGlobalScope
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callGotoGlobalScope(HttpServletRequest request,IForm form) {
		allTeams = (List)get(request.getSession(),form, "allTeams");  
										 
					try {
				// executing GotoGlobalScope in howto_captcha
				allTeams = 	serviceTeam29.team29FindAll(
	);  
 
				put(request.getSession(), ALL_TEAMS,allTeams);
								// processing variables GotoGlobalScope in howto_captcha

			} catch (ApplicationException e) { 
				// error handling for operation team29FindAll called GotoGlobalScope
				errorLogger("An error occured during the execution of the operation : team29FindAll",e); 
		
			}
	}
		/**
	 * method callSearchTeams
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callSearchTeams(HttpServletRequest request,IForm form) {
		allTeams = (List)get(request.getSession(),form, "allTeams");  
										 
					try {
				// executing SearchTeams in howto_captcha
				allTeams = 	serviceTeam.searchTeams(
	);  
 
				put(request.getSession(), ALL_TEAMS,allTeams);
								// processing variables SearchTeams in howto_captcha

			} catch (ApplicationException e) { 
				// error handling for operation searchTeams called SearchTeams
				errorLogger("An error occured during the execution of the operation : searchTeams",e); 
		
			}
	}
		/**
	 * method callGotoGeneratorClass
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callGotoGeneratorClass(HttpServletRequest request,IForm form) {
					try {
				// executing GotoGeneratorClass in howto_captcha
	serviceUtility.doNothing(
	);  

								// processing variables GotoGeneratorClass in howto_captcha

			} catch (ApplicationException e) { 
				// error handling for operation doNothing called GotoGeneratorClass
				errorLogger("An error occured during the execution of the operation : doNothing",e); 
		
			}
	}
		/**
	 * method callGotoValueObject
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callGotoValueObject(HttpServletRequest request,IForm form) {
					try {
				// executing GotoValueObject in howto_captcha
	serviceUtility.doNothing(
	);  

								// processing variables GotoValueObject in howto_captcha

			} catch (ApplicationException e) { 
				// error handling for operation doNothing called GotoValueObject
				errorLogger("An error occured during the execution of the operation : doNothing",e); 
		
			}
	}
		/**
	 * method callGotopopup
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callGotopopup(HttpServletRequest request,IForm form) {
		allPlayers = (List)get(request.getSession(),form, "allPlayers");  
										 
					try {
				// executing Gotopopup in howto_captcha
				allPlayers = 	servicePlayer205.player205FindAll(
	);  
 
				put(request.getSession(), ALL_PLAYERS,allPlayers);
								// processing variables Gotopopup in howto_captcha

			} catch (ApplicationException e) { 
				// error handling for operation player205FindAll called Gotopopup
				errorLogger("An error occured during the execution of the operation : player205FindAll",e); 
		
			}
	}
		/**
	 * method callGotoBAGS
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callGotoBAGS(HttpServletRequest request,IForm form) {
					try {
				// executing GotoBAGS in howto_captcha
	serviceUtility.doNothing(
	);  

								// processing variables GotoBAGS in howto_captcha

			} catch (ApplicationException e) { 
				// error handling for operation doNothing called GotoBAGS
				errorLogger("An error occured during the execution of the operation : doNothing",e); 
		
			}
	}
		/**
	 * method callGotoTimeOut
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callGotoTimeOut(HttpServletRequest request,IForm form) {
					try {
				// executing GotoTimeOut in howto_captcha
	serviceUtility.doNothing(
	);  

								// processing variables GotoTimeOut in howto_captcha

			} catch (ApplicationException e) { 
				// error handling for operation doNothing called GotoTimeOut
				errorLogger("An error occured during the execution of the operation : doNothing",e); 
		
			}
	}
		/**
	 * method callGotoTables
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callGotoTables(HttpServletRequest request,IForm form) {
					try {
				// executing GotoTables in howto_captcha
	serviceUtility.doNothing(
	);  

								// processing variables GotoTables in howto_captcha

			} catch (ApplicationException e) { 
				// error handling for operation doNothing called GotoTables
				errorLogger("An error occured during the execution of the operation : doNothing",e); 
		
			}
	}
		/**
	 * method callGotoValidators
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callGotoValidators(HttpServletRequest request,IForm form) {
					try {
				// executing GotoValidators in howto_captcha
	serviceUtility.doNothing(
	);  

								// processing variables GotoValidators in howto_captcha

			} catch (ApplicationException e) { 
				// error handling for operation doNothing called GotoValidators
				errorLogger("An error occured during the execution of the operation : doNothing",e); 
		
			}
	}
		/**
	 * method callGotoCaptcha
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callGotoCaptcha(HttpServletRequest request,IForm form) {
					try {
				// executing GotoCaptcha in howto_captcha
	serviceUtility.doNothing(
	);  

								// processing variables GotoCaptcha in howto_captcha

			} catch (ApplicationException e) { 
				// error handling for operation doNothing called GotoCaptcha
				errorLogger("An error occured during the execution of the operation : doNothing",e); 
		
			}
	}
		
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("cmp_treeviewController [ ");
			strBToS.append(ALL_TEAMS);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(allTeams);
			strBToS.append(END_TO_STRING_DELIMITER);
			strBToS.append(POSITIONS);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(positions);
			strBToS.append(END_TO_STRING_DELIMITER);
			strBToS.append(SCREEN47);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(screen47);
			strBToS.append(END_TO_STRING_DELIMITER);
			strBToS.append(ALL_PLAYERS);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(allPlayers);
			strBToS.append(END_TO_STRING_DELIMITER);
		// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(cmp_treeviewValidator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorscmp_treeviewController!=null); 
		return strBToS.toString();
	}
}
