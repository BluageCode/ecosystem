package uc40;

import org.junit.Test;
import test.common.AbstractBADocTestCase;

public class ITuc40 extends AbstractBADocTestCase {

	@Override
	public String getName() {
		return "UC-40";
	}

	@Test
	public void testITuc40() throws Exception {
		// On teste l'UC40 - Standard Features/Entites - Default Sort
		selenium.open("");
		selenium.click("link=Expand All");
		selenium.waitForPageToLoad("30000");
		selenium.click("link=UC40 - Default Sort");
		verifyTrue(selenium
				.isElementPresent("//h2[contains(text(),\"Standard Features > Entities > UC40 > Default Column Sort\")]"));
		// On v�rifie que les donn�es sont bien affich�es.
		verifyEquals(
				"7",
				selenium.getXpathCount("//table[contains(@id,'table_states')]//tbody//tr"));
		assertTrue(selenium
				.isElementPresent("//table[contains(@id,'table_states')]/tbody/tr[td/span[contains(text(),\"Ala\")] and td/span[contains(text(),\"Alaska\")]]"));
		assertTrue(selenium
				.isElementPresent("//table[contains(@id,'table_states')]/tbody/tr[td/span[contains(text(),\"Ark\")] and td/span[contains(text(),\"Arkansas\")]]"));
		assertTrue(selenium
				.isElementPresent("//table[contains(@id,'table_states')]/tbody/tr[td/span[contains(text(),\"Ari\")] and td/span[contains(text(),\"Arizona\")]]"));
		assertTrue(selenium
				.isElementPresent("//table[contains(@id,'table_states')]/tbody/tr[td/span[contains(text(),\"Cal\")] and td/span[contains(text(),\"California\")]]"));
		assertTrue(selenium
				.isElementPresent("//table[contains(@id,'table_states')]/tbody/tr[td/span[contains(text(),\"Col\")] and td/span[contains(text(),\"Colorado\")]]"));
		assertTrue(selenium
				.isElementPresent("//table[contains(@id,'table_states')]/tbody/tr[td/span[contains(text(),\"Con\")] and td/span[contains(text(),\"Connecticut\")]]"));
		assertTrue(selenium
				.isElementPresent("//table[contains(@id,'table_states')]/tbody/tr[td/span[contains(text(),\"Del\")] and td/span[contains(text(),\"Delaware\")]]"));
		// On ajoute 4 nouveaux states.
		selenium.type("//*[contains(@id,'txt_code')]", "FL");
		selenium.type("//*[contains(@id,'txt_name')]", "Florida");
		selenium.click("//*[contains(@id,'lnk_sort')]");
		selenium.waitForPageToLoad("30000");
		selenium.type("//*[contains(@id,'txt_code')]", "HI");
		selenium.type("//*[contains(@id,'txt_name')]", "Hawaii");
		selenium.click("//*[contains(@id,'lnk_sort')]");
		selenium.waitForPageToLoad("30000");
		selenium.type("//*[contains(@id,'txt_code')]", "NV");
		selenium.type("//*[contains(@id,'txt_name')]", "Nevada");
		selenium.click("//*[contains(@id,'lnk_sort')]");
		selenium.waitForPageToLoad("30000");
		selenium.type("//*[contains(@id,'txt_code')]", "ID");
		selenium.type("//*[contains(@id,'txt_name')]", "Idaho");
		selenium.click("//*[contains(@id,'lnk_sort')]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie que les donn�es sont bien affich�es.
		verifyEquals(
				"11",
				selenium.getXpathCount("//table[contains(@id,'table_states')]//tbody//tr"));
		assertTrue(selenium
				.isElementPresent("//table[contains(@id,'table_states')]/tbody//tr[td/span[contains(text(),\"AK\")] and td/span[contains(text(),\"Alaska\")]]"));
		assertTrue(selenium
				.isElementPresent("//table[contains(@id,'table_states')]/tbody/tr[td/span[contains(text(),\"AR\")] and td/ span[contains(text(),\"Arkansas\")]]"));
		assertTrue(selenium
				.isElementPresent("//table[contains(@id,'table_states')]/tbody/tr[td/span[contains(text(),\"AZ\")] and td/ span[contains(text(),\"Arizona\")]]"));
		assertTrue(selenium
				.isElementPresent("//table[contains(@id,'table_states')]/tbody/tr[td/span[contains(text(),\"CA\")] and td/ span[contains(text(),\"California\")]]"));
		assertTrue(selenium
				.isElementPresent("//table[contains(@id,'table_states')]/tbody/tr[td/span[contains(text(),\"CO\")] and td/ span[contains(text(),\"Colorado\")]]"));
		assertTrue(selenium
				.isElementPresent("//table[contains(@id,'table_states')]/tbody/tr[td/span[contains(text(),\"CT\")] and td/ span[contains(text(),\"Connecticut\")]]"));
		assertTrue(selenium
				.isElementPresent("//table[contains(@id,'table_states')]/tbody/tr[td/span[contains(text(),\"DE\")] and td/ span[contains(text(),\"Delaware\")]]"));
		assertTrue(selenium
				.isElementPresent("//table[contains(@id,'table_states')]/tbody/tr[td/span[contains(text(),\"FL\")] and td/ span[contains(text(),\"Florida\")]]"));
		assertTrue(selenium
				.isElementPresent("//table[contains(@id,'table_states')]/tbody/tr[td/span[contains(text(),\"HI\")] and td/ span[contains(text(),\"Hawaii\")]]"));
		assertTrue(selenium
				.isElementPresent("//table[contains(@id,'table_states')]/tbody/tr[td/span[contains(text(),\"ID\")] and td/ span[contains(text(),\"Idaho\")]]"));
		assertTrue(selenium
				.isElementPresent("//table[contains(@id,'table_states')]/tbody/tr[td/span[contains(text(),\"NV\")] and td/ span[contains(text(),\"Nevada\")]]"));
		// On v�rifie que le message d'erreur est le bon.
		// Pour ce faire, on entre un nom en double.
		selenium.type("//*[contains(@id,'txt_code')]", "NV");
		selenium.type("//*[contains(@id,'txt_name')]", "Nevada");
		selenium.click("//*[contains(@id,'lnk_sort')]");
		selenium.waitForPageToLoad("30000");
		verifyTrue(selenium
				.isElementPresent("//table[contains(@id,'applicationErrors')]/tbody/tr/td[contains(text(),\"Can't create duplicate entry.\")]"));
		// assertEquals("Can't create duplicate entry.",
		// selenium.getText("//table[contains(@id,'applicationErrors')]/tbody/tr/td | //ul/li"));
		assertEquals("[back]",
				selenium.getText("//*[contains(@id,'btn_back')]"));
		selenium.click("//*[contains(@id,'btn_back')]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie qu'on est sur la premi�re page.
		assertTrue(selenium
				.isElementPresent("//table[contains(@id,'table_states')]"));
		selenium.type("//*[contains(@id,'txt_code')]", "NA");
		selenium.type("//*[contains(@id,'txt_name')]", "Nevada");
		selenium.click("//*[contains(@id,'lnk_sort')]");
		selenium.waitForPageToLoad("30000");
		selenium.type("//*[contains(@id,'txt_code')]", "NB");
		selenium.type("//*[contains(@id,'txt_name')]", "Nevada");
		selenium.click("//*[contains(@id,'lnk_sort')]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie que les donn�es sont bien affich�es.
		verifyEquals(
				"13",
				selenium.getXpathCount("//table[contains(@id,'table_states')]//tbody//tr"));
		assertTrue(selenium
				.isElementPresent("//table[contains(@id,'table_states')]/tbody//tr[td/span[contains(text(),\"AK\")] and td/span[contains(text(),\"Alaska\")]]"));
		assertTrue(selenium
				.isElementPresent("//table[contains(@id,'table_states')]/tbody/tr[td/span[contains(text(),\"AR\")] and td/ span[contains(text(),\"Arkansas\")]]"));
		assertTrue(selenium
				.isElementPresent("//table[contains(@id,'table_states')]/tbody/tr[td/span[contains(text(),\"AZ\")] and td/ span[contains(text(),\"Arizona\")]]"));
		assertTrue(selenium
				.isElementPresent("//table[contains(@id,'table_states')]/tbody/tr[td/span[contains(text(),\"CA\")] and td/ span[contains(text(),\"California\")]]"));
		assertTrue(selenium
				.isElementPresent("//table[contains(@id,'table_states')]/tbody/tr[td/span[contains(text(),\"CO\")] and td/ span[contains(text(),\"Colorado\")]]"));
		assertTrue(selenium
				.isElementPresent("//table[contains(@id,'table_states')]/tbody/tr[td/span[contains(text(),\"CT\")] and td/ span[contains(text(),\"Connecticut\")]]"));
		assertTrue(selenium
				.isElementPresent("//table[contains(@id,'table_states')]/tbody/tr[td/span[contains(text(),\"DE\")] and td/ span[contains(text(),\"Delaware\")]]"));
		assertTrue(selenium
				.isElementPresent("//table[contains(@id,'table_states')]/tbody/tr[td/span[contains(text(),\"FL\")] and td/ span[contains(text(),\"Florida\")]]"));
		assertTrue(selenium
				.isElementPresent("//table[contains(@id,'table_states')]/tbody/tr[td/span[contains(text(),\"HI\")] and td/ span[contains(text(),\"Hawaii\")]]"));
		assertTrue(selenium
				.isElementPresent("//table[contains(@id,'table_states')]/tbody/tr[td/span[contains(text(),\"ID\")] and td/ span[contains(text(),\"Idaho\")]]"));
		assertTrue(selenium
				.isElementPresent("//table[contains(@id,'table_states')]/tbody/tr[td/span[contains(text(),\"NA\")] and td/ span[contains(text(),\"Nevada\")]]"));
		assertTrue(selenium
				.isElementPresent("//table[contains(@id,'table_states')]/tbody/tr[td/span[contains(text(),\"NB\")] and td/ span[contains(text(),\"Nevada\")]]"));
		assertTrue(selenium
				.isElementPresent("//table[contains(@id,'table_states')]/tbody/tr[td/span[contains(text(),\"NV\")] and td/ span[contains(text(),\"Nevada\")]]"));
	}

}
