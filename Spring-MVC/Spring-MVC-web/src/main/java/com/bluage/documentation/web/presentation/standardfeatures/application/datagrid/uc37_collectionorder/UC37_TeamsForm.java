/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.application.datagrid.uc37_collectionorder;


import java.io.Serializable;
import java.util.List;

import com.bluage.documentation.business.standardfeatures.application.datagrid.uc37_collectionorder.bos.Team37BO;
import com.netfective.bluage.core.web.AbstractForm;
import com.netfective.bluage.core.web.IForm;

/**
* Class : UC37_TeamsForm
*/
public class UC37_TeamsForm extends AbstractForm implements Serializable, IForm {
	
	/** Constant : serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	//CONSTANT : teams
	private static final String TEAMS = "teams";
	//CONSTANT : selectedTeam
	private static final String SELECTED_TEAM = "selectedTeam";
	//CONSTANT : team
	private static final String TEAM = "team";
	/**
	 * 	Property: teams 
	 */
	private List<Team37BO> teams;
	/**
	 * 	Property: selectedTeam 
	 */
	private Team37BO selectedTeam;
	/**
	 * 	Property: team 
	 */
	private Team37BO team;
/**
	 * Default constructor : UC37_TeamsForm
	 */
	public UC37_TeamsForm() {
		super();
		// Initialize : teams
		this.teams = new java.util.ArrayList<com.bluage.documentation.business.standardfeatures.application.datagrid.uc37_collectionorder.bos.Team37BO>();
		// Initialize : selectedTeam
		this.selectedTeam = null;
		// Initialize : team
		this.team = null;
					this.setAlwaysCallPreControllers(true);
	}
	
	/**
	 * 	Getter : teams 
	 * 	@return : Return the teams instance.
	 */
	public List<Team37BO> getTeams(){
		return teams; // For UC37_TeamsForm
	}
	
	/**
	 * 	Setter : teams 
	 *  @param teamsinstance : The instance to set.
	 */
	public void setTeams(final List<Team37BO> teamsinstance){
		this.teams = teamsinstance;// For UC37_TeamsForm
	}
	/**
	 * 	Getter : selectedTeam 
	 * 	@return : Return the selectedTeam instance.
	 */
	public Team37BO getSelectedTeam(){
		return selectedTeam; // For UC37_TeamsForm
	}
	
	/**
	 * 	Setter : selectedTeam 
	 *  @param selectedTeaminstance : The instance to set.
	 */
	public void setSelectedTeam(final Team37BO selectedTeaminstance){
		this.selectedTeam = selectedTeaminstance;// For UC37_TeamsForm
	}
	/**
	 * 	Getter : team 
	 * 	@return : Return the team instance.
	 */
	public Team37BO getTeam(){
		return team; // For UC37_TeamsForm
	}
	
	/**
	 * 	Setter : team 
	 *  @param teaminstance : The instance to set.
	 */
	public void setTeam(final Team37BO teaminstance){
		this.team = teaminstance;// For UC37_TeamsForm
	}
/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UC37_TeamsForm [ "+
TEAMS +" = " + teams +SELECTED_TEAM +" = " + selectedTeam +TEAM +" = " + team + "]";
	}
	
	/**
	 * Method setInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public void setInstanceByInstanceName(final String instanceName, final Object instance) {
		// Set the instance Teams.
		if(TEAMS.equals(instanceName)){// For UC37_TeamsForm
			this.setTeams((List<Team37BO>)instance); // For UC37_TeamsForm
		}
				// Set the instance SelectedTeam.
		if(SELECTED_TEAM.equals(instanceName)){// For UC37_TeamsForm
			this.setSelectedTeam((Team37BO)instance); // For UC37_TeamsForm
		}
				// Set the instance Team.
		if(TEAM.equals(instanceName)){// For UC37_TeamsForm
			this.setTeam((Team37BO)instance); // For UC37_TeamsForm
		}
			}
	
	/**
	 * Method getInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public Object getInstanceByInstanceName(final String instanceName){
		// Temporary object for tmpUC37_TeamsForm.
		Object tmpUC37_TeamsForm = null;
		
			
		// Get the instance Teams for UC37_TeamsForm.
		if(TEAMS.equals(instanceName)){ // For UC37_TeamsForm
			tmpUC37_TeamsForm = this.getTeams(); // For UC37_TeamsForm
		}
			
		// Get the instance SelectedTeam for UC37_TeamsForm.
		if(SELECTED_TEAM.equals(instanceName)){ // For UC37_TeamsForm
			tmpUC37_TeamsForm = this.getSelectedTeam(); // For UC37_TeamsForm
		}
			
		// Get the instance Team for UC37_TeamsForm.
		if(TEAM.equals(instanceName)){ // For UC37_TeamsForm
			tmpUC37_TeamsForm = this.getTeam(); // For UC37_TeamsForm
		}
		return tmpUC37_TeamsForm;// For UC37_TeamsForm
	}
	
			}
