/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.service.standardfeatures.entities.uc72_generatorclass;


import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import com.bluage.documentation.business.standardfeatures.entities.uc72_generatorclass.bos.State72BO;
import com.bluage.documentation.business.standardfeatures.entities.uc72_generatorclass.bos.Team72BO;
import com.bluage.documentation.business.standardfeatures.entities.uc72_generatorclass.entities.dao.Team72DAOImpl;
import com.bluage.documentation.business.standardfeatures.entities.uc72_generatorclass.entities.daofinder.Team72DAOFinderImpl;
import com.bluage.documentation.service.AbstractService;
import com.netfective.bluage.core.exception.ApplicationException;

/**
 * Defines  application services for ServiceTeam72
  */
@Named("com.bluage.documentation.service.standardfeatures.entities.uc72_generatorclass.ServiceTeam72")
public class ServiceTeam72  extends AbstractService{


			@Inject
		@Named ("com.bluage.documentation.business.standardfeatures.entities.uc72_generatorclass.entities.dao.Team72DAOImpl")
		private Team72DAOImpl team72BODao;
					@Inject
		@Named ("com.bluage.documentation.service.standardfeatures.entities.uc72_generatorclass.ServiceState72")
		private ServiceState72  instanceServiceState72;
								@Inject
		@Named ("com.bluage.documentation.business.standardfeatures.entities.uc72_generatorclass.entities.daofinder.Team72DAOFinderImpl")
		private Team72DAOFinderImpl team72BOFinder;



	/**
	 *  Default constructor 
	 */
	public ServiceTeam72() {
		super(); // Call super for default constructor : ServiceTeam72
	}
	


			
				
	/**    
	 * Operation Crud
	 * @param bo {@link Team72BO}
		 * @return {@link boolean}
	 */
	public boolean team72Update(Team72BO bo )
 throws ApplicationException {

					return team72BODao.update(bo);
	}

					
				
	/**    
	 * Operation Crud
	 * @param bo {@link Team72BO}
		 * @return {@link boolean}
	 */
	public boolean team72Create(Team72BO bo )
 throws ApplicationException {

					return team72BODao.create(bo);
	}

					
				
	/**    
	 * Operation Crud
	 * @param bo {@link Team72BO}
		 * @return {@link boolean}
	 */
	public boolean team72Delete(Team72BO bo )
 throws ApplicationException {

					return team72BODao.delete(bo);
	}

		
								
	/**    
	 * Operation Crud
	 * @return {@link List}
	 */
	public List team72FindAll( )
 throws ApplicationException {

					return team72BOFinder.findAll();
	}

		
			
	/**    
	 * Process operation.
	 * @param teamToCreate {@link Team72BO}
		 * @return {@link void}
	 */
	public void createTeam(Team72BO teamToCreate )
 throws ApplicationException {

			privateCreateTeam(teamToCreate);
	}
	

	/**    
	 * Process operation.
	 * @param teamToCreate {@link Team72BO}
		 * @return {@link void}
	 */
	private void privateCreateTeam(Team72BO teamToCreate )
 throws ApplicationException {
		// Central buffer node declaration for operation createTeam
			State72BO	 state =  new State72BO();
					State72BO	 stateFound =  new State72BO();
						// Calling an operation : 
state = teamToCreate.getState();
// Calling an operation : 
stateFound = instanceServiceState72.state72FindByID(state);
// Calling an operation : 
teamToCreate.setState(stateFound);
// Calling an operation : 
team72Create(teamToCreate);

		}

		}
