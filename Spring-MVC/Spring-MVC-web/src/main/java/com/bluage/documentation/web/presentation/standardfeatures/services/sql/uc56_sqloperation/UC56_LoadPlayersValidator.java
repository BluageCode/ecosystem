/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.services.sql.uc56_sqloperation ;


import org.apache.log4j.Logger;
import org.springframework.validation.Errors;

import com.netfective.bluage.core.validator.AbstractValidator;
/**
* Class :UC56_LoadPlayersValidator
*/
public class UC56_LoadPlayersValidator extends AbstractValidator{
	
	// LOGGER for the class UC56_LoadPlayersValidator
	private static final Logger LOGGER = Logger.getLogger( UC56_LoadPlayersValidator.class);
	
	
	/**
	* Operation validate for UC56_LoadPlayersForm
	* @param obj : the current form (UC56_LoadPlayersForm)
	* @param errors : The spring errors to return for the form UC56_LoadPlayersForm
	*/
	public void validate(final Object obj,final Errors errors){
		LOGGER.info("Beginning method : validate"); // UC56_LoadPlayersValidator
		UC56_LoadPlayersForm cUC56_LoadPlayersForm = (UC56_LoadPlayersForm)obj; // UC56_LoadPlayersValidator
		LOGGER.info("Ending method : validate the form "+ cUC56_LoadPlayersForm.getClass().getName()); // UC56_LoadPlayersValidator
	}

	/**
	* Method to implements to use spring validators (UC56_LoadPlayersForm)
	* @param aClass : Class for the form UC56_LoadPlayersForm
	* @return boolean
	*/
	public boolean supports(final Class aClass){
		return (new UC56_LoadPlayersForm()).getClass().equals(aClass); // UC56_LoadPlayersValidator
	}
}
