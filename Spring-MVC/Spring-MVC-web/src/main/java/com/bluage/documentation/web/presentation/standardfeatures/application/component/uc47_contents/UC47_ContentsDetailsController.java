/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.application.component.uc47_contents ;

// Import declaration.
// Java imports.
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.bluage.documentation.business.standardfeatures.application.component.uc47_contents.bos.Player47BO;
import com.bluage.documentation.business.standardfeatures.application.component.uc47_contents.bos.Team47BO;
import com.bluage.documentation.business.standardfeatures.application.component.uc47_contents.entities.daofinder.Position47DAOFinderImpl;
import com.bluage.documentation.service.standardfeatures.application.component.uc47_contents.ServicePlayer47;
import com.bluage.documentation.service.standardfeatures.application.component.uc47_contents.ServicePlayerTeam47;
import com.bluage.documentation.service.standardfeatures.application.component.uc47_contents.ServiceTeam47;
import com.netfective.bluage.core.exception.ApplicationException;
import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;
import com.netfective.bluage.core.web.IForm;

/**
 * Class : UC47_ContentsDetailsController
 */
@Controller
@Scope("prototype")
@SessionAttributes("uC47_ContentsDetailsForm")
@RequestMapping(value= "/presentation/standardfeatures/application/component/uc47_contents")
public class UC47_ContentsDetailsController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC47_ContentsDetailsController.class);
	
	//Current form name
	private static final String U_C47__CONTENTS_DETAILS_FORM = "uC47_ContentsDetailsForm";

		//CONSTANT: allPositions
	private static final String ALL_POSITIONS = "allPositions";
							//CONSTANT: playerToUpdate
	private static final String PLAYER_TO_UPDATE = "playerToUpdate";
	//CONSTANT: teamToAdd
	private static final String TEAM_TO_ADD = "teamToAdd";
	//CONSTANT: allPlayers
	private static final String ALL_PLAYERS = "allPlayers";
	//CONSTANT: listTeam
	private static final String LIST_TEAM = "listTeam";
	
	/**
	 * Property:customDateEditorsUC47_ContentsDetailsController
	 */
	final private Map<String,CustomDateEditor> customDateEditorsUC47_ContentsDetailsController = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:uC47_ContentsDetailsValidator
	 */
	final private UC47_ContentsDetailsValidator uC47_ContentsDetailsValidator = new UC47_ContentsDetailsValidator();
	
	/**
	 * Service declaration : servicePlayerTeam47.
	 */
	@Autowired
	private ServicePlayerTeam47 servicePlayerTeam47;
	
	/**
	 * Service declaration : servicePlayer47.
	 */
	@Autowired
	private ServicePlayer47 servicePlayer47;
	
	/**
	 * Service declaration : serviceTeam47.
	 */
	@Autowired
	private ServiceTeam47 serviceTeam47;
	
	/**
	 * Pre Controller declaration : uC47_PreControllerFindAllPlayers
	 */
	@Autowired
	private UC47_PreControllerFindAllPlayersController uC47_PreControllerFindAllPlayers;

	/**
	 * Generic Finder : position47DAOFinderImpl.
	 */
	@Autowired
	private Position47DAOFinderImpl position47DAOFinderImpl;
	
	
	// Initialise all the instances for UC47_ContentsDetailsController
			// Initialize the instance allPositions for the state lnk_goToPage2
		private List allPositions; // Initialize the instance allPositions for UC47_ContentsDetailsController
										// Declare the instance playerToUpdate
		private Player47BO playerToUpdate;
		// Declare the instance teamToAdd
		private Team47BO teamToAdd;
		// Declare the instance allPlayers
		private List allPlayers;
		// Declare the instance listTeam
		private List listTeam;
			/**
	 * Operation : lnk_update
 	 * @param model : 
 	 * @param uC47_ContentsDetails : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC47_ContentsDetails/lnk_update.html",method = RequestMethod.POST)
	public String lnk_update(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC47_ContentsDetailsForm") UC47_ContentsDetailsForm  uC47_ContentsDetailsForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : lnk_update"); 
		infoLogger(uC47_ContentsDetailsForm); 
		uC47_ContentsDetailsForm.setAlwaysCallPreControllers(false); 
		// initialization for lnk_update
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state lnk_update if the validation fail, you will be forward to the screen : "/presentation/standardfeatures/application/component/uc47_contents/UC47_ContentsDetails"
		if(errorsMessages(request,response,uC47_ContentsDetailsForm,bindingResult,"", uC47_ContentsDetailsValidator, customDateEditorsUC47_ContentsDetailsController)){ 
			return "/presentation/standardfeatures/application/component/uc47_contents/UC47_ContentsDetails"; 
		}

					 callUpdatePlayer(request,uC47_ContentsDetailsForm );
			
 callFindPlayers(request,uC47_ContentsDetailsForm );
						// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.application.component.uc47_contents.UC47_ContentsDetailsForm or not from lnk_update
			// Populate the destination form with all the instances returned from lnk_update.
			final IForm uC47_ContentsDetailsForm2 = populateDestinationForm(request.getSession(), uC47_ContentsDetailsForm); 
					
			// Add the form to the model.
			model.addAttribute("uC47_ContentsDetailsForm", uC47_ContentsDetailsForm2); 
			
			request.getSession().setAttribute("uC47_ContentsDetailsForm", uC47_ContentsDetailsForm2);
			
			// "OK" CASE => destination screen path from lnk_update
			LOGGER.info("Go to the screen 'UC47_ContentsDetails'.");
			// Redirect (PRG) from lnk_update
			destinationPath =  "redirect:/presentation/standardfeatures/application/component/uc47_contents/UC47_ContentsDetails.html";
						
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_update 
	}
	
				/**
	 * Operation : lnk_add
 	 * @param model : 
 	 * @param uC47_ContentsDetails : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC47_ContentsDetails/lnk_add.html",method = RequestMethod.POST)
	public String lnk_add(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC47_ContentsDetailsForm") UC47_ContentsDetailsForm  uC47_ContentsDetailsForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : lnk_add"); 
		infoLogger(uC47_ContentsDetailsForm); 
		uC47_ContentsDetailsForm.setAlwaysCallPreControllers(false); 
		// initialization for lnk_add
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state lnk_add if the validation fail, you will be forward to the screen : "/presentation/standardfeatures/application/component/uc47_contents/UC47_ContentsDetails"
		if(errorsMessages(request,response,uC47_ContentsDetailsForm,bindingResult,"", uC47_ContentsDetailsValidator, customDateEditorsUC47_ContentsDetailsController)){ 
			return "/presentation/standardfeatures/application/component/uc47_contents/UC47_ContentsDetails"; 
		}

									 callCreatePlayerTeam(request,uC47_ContentsDetailsForm );
			
				 callFindPlayer47(request,uC47_ContentsDetailsForm );
			
				 callSearchteamsnotplayed(request,uC47_ContentsDetailsForm );
						// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.application.component.uc47_contents.UC47_ContentsDetailsForm or not from lnk_add
			// Populate the destination form with all the instances returned from lnk_add.
			final IForm uC47_ContentsDetailsForm2 = populateDestinationForm(request.getSession(), uC47_ContentsDetailsForm); 
					
			// Add the form to the model.
			model.addAttribute("uC47_ContentsDetailsForm", uC47_ContentsDetailsForm2); 
			
			request.getSession().setAttribute("uC47_ContentsDetailsForm", uC47_ContentsDetailsForm2);
			
			// "OK" CASE => destination screen path from lnk_add
			LOGGER.info("Go to the screen 'UC47_ContentsDetails'.");
			// Redirect (PRG) from lnk_add
			destinationPath =  "redirect:/presentation/standardfeatures/application/component/uc47_contents/UC47_ContentsDetails.html";
						
						
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_add 
	}
	
						/**
	 * Operation : lnk_back
 	 * @param model : 
 	 * @param uC47_ContentsDetails : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC47_ContentsDetails/lnk_back.html",method = RequestMethod.GET)
	public String lnk_back(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC47_ContentsDetailsForm") UC47_ContentsDetailsForm  uC47_ContentsDetailsForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : lnk_back"); 
		infoLogger(uC47_ContentsDetailsForm); 
		uC47_ContentsDetailsForm.setAlwaysCallPreControllers(false); 
		// initialization for lnk_back
		init();
		
		String destinationPath = null; 
		

	 callFindallPlayers(request,uC47_ContentsDetailsForm );
											// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.application.component.uc47_contents.UC47_ContentsForm or not from lnk_back
			final UC47_ContentsForm uC47_ContentsForm =  new UC47_ContentsForm();
			// Populate the destination form with all the instances returned from lnk_back.
			final IForm uC47_ContentsForm2 = populateDestinationForm(request.getSession(), uC47_ContentsForm); 
					
			// Add the form to the model.
			model.addAttribute("uC47_ContentsForm", uC47_ContentsForm2); 
			
			request.getSession().setAttribute("uC47_ContentsForm", uC47_ContentsForm2);
			
			// "OK" CASE => destination screen path from lnk_back
			LOGGER.info("Go to the screen 'UC47_Contents'.");
			// Redirect (PRG) from lnk_back
			destinationPath =  "redirect:/presentation/standardfeatures/application/component/uc47_contents/UC47_Contents.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_back 
	}
	
	
	/**
	* This method initialise the form : UC47_ContentsDetailsForm 
	* @return UC47_ContentsDetailsForm
	*/
	@ModelAttribute("UC47_ContentsDetailsFormInit")
	public void initUC47_ContentsDetailsForm(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, U_C47__CONTENTS_DETAILS_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method UC47_ContentsDetailsForm."); //for lnk_back 
		}
		UC47_ContentsDetailsForm uC47_ContentsDetailsForm;
	
		if(request.getSession().getAttribute(U_C47__CONTENTS_DETAILS_FORM) != null){
			uC47_ContentsDetailsForm = (UC47_ContentsDetailsForm)request.getSession().getAttribute(U_C47__CONTENTS_DETAILS_FORM);
		} else {
			uC47_ContentsDetailsForm = new UC47_ContentsDetailsForm();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form UC47_ContentsDetailsForm.");
		}
		uC47_ContentsDetailsForm = (UC47_ContentsDetailsForm)populateDestinationForm(request.getSession(), uC47_ContentsDetailsForm);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  UC47_ContentsDetailsForm.");
		}
		model.addAttribute(U_C47__CONTENTS_DETAILS_FORM, uC47_ContentsDetailsForm);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param UC47_ContentsDetailsForm : The sceen form.
	 */
	@RequestMapping(value = "/UC47_ContentsDetails.html" ,method = RequestMethod.GET)
	public void prepareUC47_ContentsDetails(final HttpServletRequest request,final  Model model,final  @ModelAttribute("UC47_ContentsDetailsFormInit") UC47_ContentsDetailsForm uC47_ContentsDetailsForm){
		
		UC47_ContentsDetailsForm currentUC47_ContentsDetailsForm = uC47_ContentsDetailsForm;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(U_C47__CONTENTS_DETAILS_FORM) == null){
			if(currentUC47_ContentsDetailsForm!=null){
				request.getSession().setAttribute(U_C47__CONTENTS_DETAILS_FORM, currentUC47_ContentsDetailsForm);
			}else {
				currentUC47_ContentsDetailsForm = new UC47_ContentsDetailsForm();
				request.getSession().setAttribute(U_C47__CONTENTS_DETAILS_FORM, currentUC47_ContentsDetailsForm);	
			}
		} else {
			currentUC47_ContentsDetailsForm = (UC47_ContentsDetailsForm) request.getSession().getAttribute(U_C47__CONTENTS_DETAILS_FORM);
		}

		try {
			List allPositions = position47DAOFinderImpl.findAll();
			put(request.getSession(), ALL_POSITIONS, allPositions);
		} catch (ApplicationException e) {
			errorLogger("An error while calling the generic finder for the instance : allPositions.",e);
		}
				currentUC47_ContentsDetailsForm = (UC47_ContentsDetailsForm)populateDestinationForm(request.getSession(), currentUC47_ContentsDetailsForm);
		// Call all the Precontroller.
	if ( currentUC47_ContentsDetailsForm.isAlwaysCallPreControllers()) {
	
		// Calling the PreController : UC47_PreControllerFindAllPlayers.
		currentUC47_ContentsDetailsForm = (UC47_ContentsDetailsForm) uC47_PreControllerFindAllPlayers.uC47_PreControllerFindAllPlayersControllerInit(request, model, currentUC47_ContentsDetailsForm);
		
	}
	currentUC47_ContentsDetailsForm.setAlwaysCallPreControllers(true);
		model.addAttribute(U_C47__CONTENTS_DETAILS_FORM, currentUC47_ContentsDetailsForm);
		request.getSession().setAttribute(U_C47__CONTENTS_DETAILS_FORM, currentUC47_ContentsDetailsForm);
		
	}
	
						/**
	 * method callUpdatePlayer
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callUpdatePlayer(HttpServletRequest request,IForm form) {
					playerToUpdate = (Player47BO)get(request.getSession(),form, "playerToUpdate");  
										 
					try {
				// executing UpdatePlayer in lnk_back
	servicePlayer47.player47Update(
			playerToUpdate
			);  

													// processing variables UpdatePlayer in lnk_back
				put(request.getSession(), PLAYER_TO_UPDATE,playerToUpdate); 
			
			} catch (ApplicationException e) { 
				// error handling for operation player47Update called UpdatePlayer
				errorLogger("An error occured during the execution of the operation : player47Update",e); 
		
			}
	}
								/**
	 * method callCreatePlayerTeam
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callCreatePlayerTeam(HttpServletRequest request,IForm form) {
					teamToAdd = (Team47BO)get(request.getSession(),form, "teamToAdd");  
										 
					playerToUpdate = (Player47BO)get(request.getSession(),form, "playerToUpdate");  
										 
					try {
				// executing CreatePlayerTeam in lnk_back
	servicePlayerTeam47.createPlayerTeam47(
			teamToAdd
		,			playerToUpdate
			);  

								// processing variables CreatePlayerTeam in lnk_back
				put(request.getSession(), TEAM_TO_ADD,teamToAdd); 
							put(request.getSession(), PLAYER_TO_UPDATE,playerToUpdate); 
			
			} catch (ApplicationException e) { 
				// error handling for operation createPlayerTeam47 called CreatePlayerTeam
				errorLogger("An error occured during the execution of the operation : createPlayerTeam47",e); 
		
			}
	}
					/**
	 * method callFindPlayer47
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callFindPlayer47(HttpServletRequest request,IForm form) {
		playerToUpdate = (Player47BO)get(request.getSession(),form, "playerToUpdate");  
										 
					playerToUpdate = (Player47BO)get(request.getSession(),form, "playerToUpdate");  
										 
					try {
				// executing FindPlayer47 in lnk_back
				playerToUpdate = 	servicePlayer47.player47FindByID(
			playerToUpdate
			);  
 
				put(request.getSession(), PLAYER_TO_UPDATE,playerToUpdate);
								// processing variables FindPlayer47 in lnk_back
				put(request.getSession(), PLAYER_TO_UPDATE,playerToUpdate); 
			
			} catch (ApplicationException e) { 
				// error handling for operation player47FindByID called FindPlayer47
				errorLogger("An error occured during the execution of the operation : player47FindByID",e); 
		
			}
	}
		/**
	 * method callFindPlayers
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callFindPlayers(HttpServletRequest request,IForm form) {
		allPlayers = (List)get(request.getSession(),form, "allPlayers");  
										 
					try {
				// executing FindPlayers in lnk_back
				allPlayers = 	servicePlayer47.player47FindAll(
	);  
 
				put(request.getSession(), ALL_PLAYERS,allPlayers);
								// processing variables FindPlayers in lnk_back

			} catch (ApplicationException e) { 
				// error handling for operation player47FindAll called FindPlayers
				errorLogger("An error occured during the execution of the operation : player47FindAll",e); 
		
			}
	}
		/**
	 * method callFindallPlayers
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callFindallPlayers(HttpServletRequest request,IForm form) {
		allPlayers = (List)get(request.getSession(),form, "allPlayers");  
										 
					try {
				// executing FindallPlayers in lnk_back
				allPlayers = 	servicePlayer47.player47FindAll(
	);  
 
				put(request.getSession(), ALL_PLAYERS,allPlayers);
								// processing variables FindallPlayers in lnk_back

			} catch (ApplicationException e) { 
				// error handling for operation player47FindAll called FindallPlayers
				errorLogger("An error occured during the execution of the operation : player47FindAll",e); 
		
			}
	}
					/**
	 * method callSearchteamsnotplayed
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callSearchteamsnotplayed(HttpServletRequest request,IForm form) {
		listTeam = (List)get(request.getSession(),form, "listTeam");  
										 
					playerToUpdate = (Player47BO)get(request.getSession(),form, "playerToUpdate");  
										 
					try {
				// executing Searchteamsnotplayed in lnk_back
				listTeam = 	serviceTeam47.searchTeamsNotPlayedByPlayer47(
			playerToUpdate
			);  
 
				put(request.getSession(), LIST_TEAM,listTeam);
								// processing variables Searchteamsnotplayed in lnk_back
				put(request.getSession(), PLAYER_TO_UPDATE,playerToUpdate); 
			
			} catch (ApplicationException e) { 
				// error handling for operation searchTeamsNotPlayedByPlayer47 called Searchteamsnotplayed
				errorLogger("An error occured during the execution of the operation : searchTeamsNotPlayedByPlayer47",e); 
		
			}
	}
		/**
	* This method Binder.
	* @param binder : 
	*		 WebDataBinder
	*/
	@InitBinder
	protected void initBinder(final WebDataBinder binder){
			customDateEditorsUC47_ContentsDetailsController.put("playerToUpdate.dateOfBirth", new CustomDateEditor(new SimpleDateFormat("MM/dd/yyyy"), true, "Invalid format."));
			binder.registerCustomEditor(Date.class, "playerToUpdate.dateOfBirth", customDateEditorsUC47_ContentsDetailsController.get("playerToUpdate.dateOfBirth"));
	}
	
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("UC47_ContentsDetailsController [ ");
				strBToS.append(ALL_POSITIONS);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(allPositions);
			strBToS.append(END_TO_STRING_DELIMITER);
									strBToS.append(PLAYER_TO_UPDATE);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(playerToUpdate);
			strBToS.append(END_TO_STRING_DELIMITER);
			strBToS.append(TEAM_TO_ADD);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(teamToAdd);
			strBToS.append(END_TO_STRING_DELIMITER);
			strBToS.append(ALL_PLAYERS);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(allPlayers);
			strBToS.append(END_TO_STRING_DELIMITER);
			strBToS.append(LIST_TEAM);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(listTeam);
			strBToS.append(END_TO_STRING_DELIMITER);
		// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(uC47_ContentsDetailsValidator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsUC47_ContentsDetailsController!=null); 
		return strBToS.toString();
	}
}
