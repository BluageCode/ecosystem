/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.business;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Expression;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.netfective.bluage.core.business.IBusinessObject;
import com.netfective.bluage.core.exception.ApplicationException;
import com.netfective.bluage.core.persistence.IObjectDAOFinder;
import com.netfective.bluage.core.persistence.IPersistentObjectDAOFinder;
import com.netfective.bluage.core.vo.CopyUtil;


/**
 * Implements all basic dao finder methods
 * 
 * @see HibernateDaoSupport
 * @see IBusinessStringConstants
 * @see IPersistentObjectDAOFinder
 * @param <T>
 */
public abstract class AbstractVODaoFinderImpl<T, U> extends HibernateDaoSupport
		implements IBusinessStringConstants, IObjectDAOFinder<T> {
			
	private static final Logger LOGGER = Logger.getLogger(AbstractVODaoFinderImpl.class);
	
	private boolean clearSession;
	private boolean refreshSession;
	private boolean flushSession;
	
	/**
	 * Returns the refBo. To be overridden by subclasses
	 * 
	 * @return Class<? extends T>
	 */
	public abstract Class<? extends U> getRefBo();
	
	/**
	 * 
	 * @return
	 */
	public abstract Class<? extends T> getRefVo();

	/**
	 * Returns the default order by instruction
	 * 
	 * @return String
	 */
	public abstract String getDefaultOrder();

	/**
	 * The hibernate session is cleared after a persistence operation, if
	 * clearSession is true.
	 * 
	 * @param value
	 */
	public final void setClearSession(final boolean value) {
		this.clearSession = value;
	}

	/**
	 * The hibernate session is cleared after a persistence operation, if
	 * clearSession is true.
	 * 
	 * @return boolean
	 */
	public final boolean isClearSession() {
		return this.clearSession;
	}

	/**
	 * The object is refreshed after a persistence operation, if refreshSession
	 * is true.
	 * 
	 * @param value
	 */
	public final void setRefreshSession(final boolean value) {
		this.refreshSession = value;
	}

	/**
	 * The object is refreshed after a persistence operation, if refreshSession
	 * is true.
	 * 
	 * @return boolean
	 */
	public final boolean isRefreshSession() {
		return this.refreshSession;
	}

	/**
	 * The hibernate session is flushed after a persistence operation, if flushSession
	 * is true.
	 * 
	 * @param value
	 */
	public final void setFlushSession(final boolean value) {
		this.flushSession = value;
	}

	/**
	 * The hibernate session is flushed after a persistence operation, if flushSession
	 * is true.
	 * 
	 * @return boolean
	 */
	public final boolean isFlushSession() {
		return this.flushSession;
	}
	
	/**
	 * <p>
	 * This method returns the object whose key is passed as parameter
	 * </p>
	 * 
	 * @param key
	 *            : primary key of object to search
	 * @return Object
	 * @throws ApplicationException
	 */
	public final Object findByPrimaryKey(final Object key)
			throws ApplicationException {
		this.logMethod(L_BEGIN_METHOD, L_FIND_BY_PRIMARY_KEY, L_KEY, key);
		Serializable pk = null;
		try {
			if (isClearSession()) {
				this.flushThenClear();
			}
			pk = (Serializable) key;
			final Object obj = this.getHibernateTemplate().get(this.getRefBo(), pk);
			if (isRefreshSession()) {
				this.getHibernateTemplate().refresh(obj);
			}
			if(isFlushSession()){
				this.getHibernateTemplate().flush();
			}
			this.logMethod(L_FIND_BY_PRIMARY_KEY, L_END_OF_METHOD, L_RETURNS,
					obj);
			//instantiate vo
			final Object vo = this.getRefVo().newInstance();
			//copy from bo to vo
			CopyUtil.copyBeanToDest(obj, vo);
			//return vo
			return vo;
		} catch (InstantiationException e) {
			this.logError(L_FIND_BY_PRIMARY_KEY, e);
			throw new com.netfective.bluage.core.exception.UnexpectedException(e);
		} catch (IllegalAccessException e) {
			this.logError(L_FIND_BY_PRIMARY_KEY, e);
			throw new com.netfective.bluage.core.exception.UnexpectedException(e);
		}
	}

	/**
	 * This method returns the resulting list of entities of the search by
	 * criteria based on the properties of the Business Object
	 * 
	 * @param properties
	 *            : {@link Hashtable} properties for the search action
	 * @return {@link List} of {@link IBusinessObject}
	 * @throws ApplicationException
	 */
	public final List<T> findByProperties(final Map<String, Object> properties)
			throws ApplicationException {
		this.logMethod(L_BEGIN_METHOD, L_FIND_BY_PROPERTIES, L_PROPERTIES,
				properties);
		try {
			final Criteria crit = this.getSession().createCriteria(this.getRefBo());
			final Iterator<Entry<String, Object>> it = properties.entrySet().iterator();
			while (it.hasNext()) {
				final Entry<String, Object> tmp = it.next();
				crit.add(Expression.eq(tmp.getKey(), tmp.getValue()));
			}
			final List result = crit.list();
			final List<T> resultVo = new ArrayList<T>();
			for (final Iterator iterator = result.iterator(); iterator.hasNext();) {
				final Object bo =  iterator.next();
				final T vo = this.getRefVo().newInstance();
				CopyUtil.copyBeanToDest(bo, vo);
				resultVo.add(vo);
			}
			this.logMethod(L_FIND_BY_PROPERTIES, L_END_OF_METHOD, L_RETURNS,
					resultVo);
			return resultVo;
		} catch (InstantiationException e) {
			this.logError(L_FIND_BY_PROPERTIES, e);
			throw new com.netfective.bluage.core.exception.UnexpectedException(e);
		} catch (IllegalAccessException e) {
			this.logError(L_FIND_BY_PROPERTIES, e);
			throw new com.netfective.bluage.core.exception.UnexpectedException(e);
		}
	}

	/**
	 * This method returns the resulting list of entities of the search without
	 * criteria
	 * 
	 * @return List : {@link List} of {@link IBusinessObject}
	 * @throws ApplicationException
	 */
	public final List<T> findAll() throws ApplicationException {

		final StringBuilder findAllQueryString = new StringBuilder();
		findAllQueryString.append(L_FROM).append(this.getRefBo().getName())
				.append(this.getDefaultOrder());
		logMethod(L_FIND_ALL, L_BEGIN_METHOD, "", null);
		try {
			final List list = this.getHibernateTemplate().find(
					findAllQueryString.toString());
			final List<T> resultVo = new ArrayList<T>();
			for (final Iterator iterator = list.iterator(); iterator.hasNext();) {
				final Object bo =  iterator.next();
				final T vo = this.getRefVo().newInstance();
				CopyUtil.copyBeanToDest(bo, vo);
				resultVo.add(vo);
			}
			this.logMethod(L_FIND_ALL, L_END_OF_METHOD, L_RETURNS, resultVo);
			return resultVo;
		} catch (InstantiationException e) {
			this.logError(L_FIND_ALL, e);
			throw new com.netfective.bluage.core.exception.UnexpectedException(e);
		} catch (IllegalAccessException e) {
			this.logError(L_FIND_ALL, e);
			throw new com.netfective.bluage.core.exception.UnexpectedException(e);
		}
	}

	/**
	 * Find Data for paginated query
	 * 
	 * @param queryName
	 * @param startRecord
	 * @param endRecord
	 * @returns List<T>
	 * @see com.netfective.bluage.core.persistence.IPersistentObjectDAOFinder#findData()
	 */
	public final List<T> findData(final String queryName,
			final int startRecord, final int endRecord)
			throws ApplicationException {

		this.logMethod(L_FIND_DATA, L_BEGIN_METHOD, "", null);
		Session session = null;
		try {
			session = this.getSession();
			final Query q = session.createQuery(queryName);
			q.setFirstResult(startRecord);
			q.setMaxResults(endRecord);
			final List result = q.list();
			final List<T> resultVo = new ArrayList<T>();
			for (final Iterator iterator = result.iterator(); iterator.hasNext();) {
				final Object bo =  iterator.next();
				final T vo = this.getRefVo().newInstance();
				CopyUtil.copyBeanToDest(bo, vo);
				resultVo.add(vo);
			}
			session.flush();
			return resultVo;
		} catch (InstantiationException e) {
			this.logError(L_FIND_DATA, e);
			throw new com.netfective.bluage.core.exception.UnexpectedException(e);
		} catch (IllegalAccessException e) {
			this.logError(L_FIND_DATA, e);
			throw new com.netfective.bluage.core.exception.UnexpectedException(e);
		}
	}

	/**
	 * Prepare result for paginated query
	 * 
	 * @param startRecord
	 * @param endRecord
	 * @returns List<T>
	 * @throws ApplicationException
	 * @see com.netfective.bluage.core.persistence.IPersistentObjectDAOFinder#findAll()
	 */
	public final List<T> prepareResult(final int startRecord,
			final int endRecord) throws ApplicationException {
		return findData(L_FROM + this.getRefBo().getName(), startRecord,
				endRecord);
	}


	/**
	 * Log end of method on exception
	 * 
	 * @param methodCallString
	 * @param ex
	 */
	private void logError(final String methodCallString, final Exception ex) {
		if (LOGGER.isDebugEnabled()) {
			final StringBuilder sb = new StringBuilder();
			sb.append(L_END_OF_METHOD).append(this.getClass().getName())
					.append('.').append(methodCallString).append(L_WITH_ERROR);
			LOGGER.debug(sb.toString(), ex);
		}
	}

	/**
	 * Log method call
	 * 
	 * @param methodCallString
	 * @param prefix
	 * @param paramDesc
	 * @param object
	 */
	private void logMethod(final String methodCallString, final String prefix,
			final String paramDesc, final Object object) {
		if (LOGGER.isDebugEnabled()) {
			final StringBuilder sb = new StringBuilder();
			sb.append(prefix).append(this.getClass().getName()).append('.')
					.append(methodCallString);
			LOGGER.debug(sb.toString());
			if (object != null) {
				LOGGER.debug(paramDesc + object);
			}
		}
	}

	/**
	 * Flush then clear session
	 */
	private void flushThenClear() {
		this.getHibernateTemplate().flush();
		this.getHibernateTemplate().clear();
	}
}
