/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.netfective.bluage.core.persistence;
import java.util.List;
import java.util.Map;

import com.netfective.bluage.core.exception.ApplicationException;

/**
 * Interface for object dao finder
 */
public interface IObjectDAOFinder<T> {

	/**
	 * <p>
	 * find object by primary key
	 * </p>
	 * 
	 * @param key
	 * @return Object
	 */
	Object findByPrimaryKey(Object key) throws ApplicationException;

	/**
	 * <p>
	 * find object by properties
	 * </p>
	 * 
	 * @param properties
	 * @return Hashtable
	 */
	List<T> findByProperties(Map<String, Object> properties)
			throws ApplicationException;

	/**
	 * <p>
	 * find all objects
	 * </p>
	 * 
	 * @return List
	 */
	List<T> findAll() throws ApplicationException;


	/**
	 * If true flush and clear the session.
	 */
	void setClearSession(boolean value);

	/**
	 * If true flush and clear the session.
	 */
	boolean isClearSession();

	/**
	 * <code>true</code> flush and refresh the object in session.
	 * <code>false</code> does nothing.
	 * 
	 * @param value
	 * @return
	 */
	void setRefreshSession(boolean value);

	/**
	 * If true flush and refresh the object in the session.
	 */
	boolean isRefreshSession();
	
	/**
	 * <code>true</code> flush the session.
	 * <code>false</code> does nothing.
	 * @param value
	 * @return
	 */
	void setFlushSession(boolean value);
	
	/**
	 * If true flush the object in the session. 
	 */
	boolean isFlushSession();
}
