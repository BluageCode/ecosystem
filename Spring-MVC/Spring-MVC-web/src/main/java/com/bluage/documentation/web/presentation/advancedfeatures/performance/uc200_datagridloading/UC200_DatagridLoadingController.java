/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.advancedfeatures.performance.uc200_datagridloading ;

// Import declaration.
// Java imports.
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.bluage.documentation.service.advancedfeatures.performance.uc200_datagridloading.ServicePlayer200;
import com.netfective.bluage.core.exception.ApplicationException;
import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;
import com.netfective.bluage.core.web.IForm;

/**
 * Class : UC200_DatagridLoadingController
 */
@Controller
@Scope("prototype")
@SessionAttributes("uC200_DatagridLoadingForm")
@RequestMapping(value= "/presentation/advancedfeatures/performance/uc200_datagridloading")
public class UC200_DatagridLoadingController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC200_DatagridLoadingController.class);
	
	//Current form name
	private static final String U_C200__DATAGRID_LOADING_FORM = "uC200_DatagridLoadingForm";

				//CONSTANT: players
	private static final String PLAYERS = "players";
	
	/**
	 * Property:customDateEditorsUC200_DatagridLoadingController
	 */
	final private Map<String,CustomDateEditor> customDateEditorsUC200_DatagridLoadingController = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:uC200_DatagridLoadingValidator
	 */
	final private UC200_DatagridLoadingValidator uC200_DatagridLoadingValidator = new UC200_DatagridLoadingValidator();
	
	/**
	 * Service declaration : servicePlayer200.
	 */
	@Autowired
	private ServicePlayer200 servicePlayer200;
	
	
	// Initialise all the instances for UC200_DatagridLoadingController
						// Declare the instance players
		private List players;
					/**
	 * Operation : lnk_dataloading_1
 	 * @param model : 
 	 * @param uC200_DatagridLoading : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC200_DatagridLoading/lnk_dataloading_1.html",method = RequestMethod.GET)
	public String lnk_dataloading_1(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC200_DatagridLoadingForm") UC200_DatagridLoadingForm  uC200_DatagridLoadingForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : lnk_dataloading_1"); 
		infoLogger(uC200_DatagridLoadingForm); 
		uC200_DatagridLoadingForm.setAlwaysCallPreControllers(false); 
		// initialization for lnk_dataloading_1
		init();
		
		String destinationPath = null; 
		

	 callFindplayersOnerequestexecuted(request,uC200_DatagridLoadingForm );
						// Init the destination form com.bluage.documentation.web.presentation.advancedfeatures.performance.uc200_datagridloading.UC200_DatagridLoadingForm or not from lnk_dataloading_1
			// Populate the destination form with all the instances returned from lnk_dataloading_1.
			final IForm uC200_DatagridLoadingForm2 = populateDestinationForm(request.getSession(), uC200_DatagridLoadingForm); 
					
			// Add the form to the model.
			model.addAttribute("uC200_DatagridLoadingForm", uC200_DatagridLoadingForm2); 
			
			request.getSession().setAttribute("uC200_DatagridLoadingForm", uC200_DatagridLoadingForm2);
			
			// "OK" CASE => destination screen path from lnk_dataloading_1
			LOGGER.info("Go to the screen 'UC200_DatagridLoading'.");
			// Redirect (PRG) from lnk_dataloading_1
			destinationPath =  "redirect:/presentation/advancedfeatures/performance/uc200_datagridloading/UC200_DatagridLoading.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_dataloading_1 
	}
	
						/**
	 * Operation : lnk_dataloading_n1
 	 * @param model : 
 	 * @param uC200_DatagridLoading : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC200_DatagridLoading/lnk_dataloading_n1.html",method = RequestMethod.GET)
	public String lnk_dataloading_n1(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC200_DatagridLoadingForm") UC200_DatagridLoadingForm  uC200_DatagridLoadingForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : lnk_dataloading_n1"); 
		infoLogger(uC200_DatagridLoadingForm); 
		uC200_DatagridLoadingForm.setAlwaysCallPreControllers(false); 
		// initialization for lnk_dataloading_n1
		init();
		
		String destinationPath = null; 
		

	 callFindplayersNplusOnerequestsexecuted(request,uC200_DatagridLoadingForm );
						// Init the destination form com.bluage.documentation.web.presentation.advancedfeatures.performance.uc200_datagridloading.UC200_DatagridLoadingForm or not from lnk_dataloading_n1
			// Populate the destination form with all the instances returned from lnk_dataloading_n1.
			final IForm uC200_DatagridLoadingForm2 = populateDestinationForm(request.getSession(), uC200_DatagridLoadingForm); 
					
			// Add the form to the model.
			model.addAttribute("uC200_DatagridLoadingForm", uC200_DatagridLoadingForm2); 
			
			request.getSession().setAttribute("uC200_DatagridLoadingForm", uC200_DatagridLoadingForm2);
			
			// "OK" CASE => destination screen path from lnk_dataloading_n1
			LOGGER.info("Go to the screen 'UC200_DatagridLoading'.");
			// Redirect (PRG) from lnk_dataloading_n1
			destinationPath =  "redirect:/presentation/advancedfeatures/performance/uc200_datagridloading/UC200_DatagridLoading.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_dataloading_n1 
	}
	
						/**
	 * Operation : lnk_clear
 	 * @param model : 
 	 * @param uC200_DatagridLoading : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC200_DatagridLoading/lnk_clear.html",method = RequestMethod.GET)
	public String lnk_clear(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC200_DatagridLoadingForm") UC200_DatagridLoadingForm  uC200_DatagridLoadingForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : lnk_clear"); 
		infoLogger(uC200_DatagridLoadingForm); 
		uC200_DatagridLoadingForm.setAlwaysCallPreControllers(false); 
		// initialization for lnk_clear
		init();
		
		String destinationPath = null; 
		

	 callClearthelist(request,uC200_DatagridLoadingForm );
						// Init the destination form com.bluage.documentation.web.presentation.advancedfeatures.performance.uc200_datagridloading.UC200_DatagridLoadingForm or not from lnk_clear
			// Populate the destination form with all the instances returned from lnk_clear.
			final IForm uC200_DatagridLoadingForm2 = populateDestinationForm(request.getSession(), uC200_DatagridLoadingForm); 
					
			// Add the form to the model.
			model.addAttribute("uC200_DatagridLoadingForm", uC200_DatagridLoadingForm2); 
			
			request.getSession().setAttribute("uC200_DatagridLoadingForm", uC200_DatagridLoadingForm2);
			
			// "OK" CASE => destination screen path from lnk_clear
			LOGGER.info("Go to the screen 'UC200_DatagridLoading'.");
			// Redirect (PRG) from lnk_clear
			destinationPath =  "redirect:/presentation/advancedfeatures/performance/uc200_datagridloading/UC200_DatagridLoading.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_clear 
	}
	
	
	/**
	* This method initialise the form : UC200_DatagridLoadingForm 
	* @return UC200_DatagridLoadingForm
	*/
	@ModelAttribute("UC200_DatagridLoadingFormInit")
	public void initUC200_DatagridLoadingForm(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, U_C200__DATAGRID_LOADING_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method UC200_DatagridLoadingForm."); //for lnk_clear 
		}
		UC200_DatagridLoadingForm uC200_DatagridLoadingForm;
	
		if(request.getSession().getAttribute(U_C200__DATAGRID_LOADING_FORM) != null){
			uC200_DatagridLoadingForm = (UC200_DatagridLoadingForm)request.getSession().getAttribute(U_C200__DATAGRID_LOADING_FORM);
		} else {
			uC200_DatagridLoadingForm = new UC200_DatagridLoadingForm();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form UC200_DatagridLoadingForm.");
		}
		uC200_DatagridLoadingForm = (UC200_DatagridLoadingForm)populateDestinationForm(request.getSession(), uC200_DatagridLoadingForm);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  UC200_DatagridLoadingForm.");
		}
		model.addAttribute(U_C200__DATAGRID_LOADING_FORM, uC200_DatagridLoadingForm);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param UC200_DatagridLoadingForm : The sceen form.
	 */
	@RequestMapping(value = "/UC200_DatagridLoading.html" ,method = RequestMethod.GET)
	public void prepareUC200_DatagridLoading(final HttpServletRequest request,final  Model model,final  @ModelAttribute("UC200_DatagridLoadingFormInit") UC200_DatagridLoadingForm uC200_DatagridLoadingForm){
		
		UC200_DatagridLoadingForm currentUC200_DatagridLoadingForm = uC200_DatagridLoadingForm;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(U_C200__DATAGRID_LOADING_FORM) == null){
			if(currentUC200_DatagridLoadingForm!=null){
				request.getSession().setAttribute(U_C200__DATAGRID_LOADING_FORM, currentUC200_DatagridLoadingForm);
			}else {
				currentUC200_DatagridLoadingForm = new UC200_DatagridLoadingForm();
				request.getSession().setAttribute(U_C200__DATAGRID_LOADING_FORM, currentUC200_DatagridLoadingForm);	
			}
		} else {
			currentUC200_DatagridLoadingForm = (UC200_DatagridLoadingForm) request.getSession().getAttribute(U_C200__DATAGRID_LOADING_FORM);
		}

					currentUC200_DatagridLoadingForm = (UC200_DatagridLoadingForm)populateDestinationForm(request.getSession(), currentUC200_DatagridLoadingForm);
		// Call all the Precontroller.
	currentUC200_DatagridLoadingForm.setAlwaysCallPreControllers(true);
		model.addAttribute(U_C200__DATAGRID_LOADING_FORM, currentUC200_DatagridLoadingForm);
		request.getSession().setAttribute(U_C200__DATAGRID_LOADING_FORM, currentUC200_DatagridLoadingForm);
		
	}
	
				/**
	 * method callFindplayersOnerequestexecuted
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callFindplayersOnerequestexecuted(HttpServletRequest request,IForm form) {
		players = (List)get(request.getSession(),form, "players");  
										 
					try {
				// executing FindplayersOnerequestexecuted in lnk_clear
				players = 	servicePlayer200.findPlayers1(
	);  
 
				put(request.getSession(), PLAYERS,players);
								// processing variables FindplayersOnerequestexecuted in lnk_clear

			} catch (ApplicationException e) { 
				// error handling for operation findPlayers1 called FindplayersOnerequestexecuted
				errorLogger("An error occured during the execution of the operation : findPlayers1",e); 
		
			}
	}
		/**
	 * method callFindplayersNplusOnerequestsexecuted
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callFindplayersNplusOnerequestsexecuted(HttpServletRequest request,IForm form) {
		players = (List)get(request.getSession(),form, "players");  
										 
					try {
				// executing FindplayersNplusOnerequestsexecuted in lnk_clear
				players = 	servicePlayer200.findPlayersN1(
	);  
 
				put(request.getSession(), PLAYERS,players);
								// processing variables FindplayersNplusOnerequestsexecuted in lnk_clear

			} catch (ApplicationException e) { 
				// error handling for operation findPlayersN1 called FindplayersNplusOnerequestsexecuted
				errorLogger("An error occured during the execution of the operation : findPlayersN1",e); 
		
			}
	}
		/**
	 * method callClearthelist
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callClearthelist(HttpServletRequest request,IForm form) {
		players = (List)get(request.getSession(),form, "players");  
										 
					try {
				// executing Clearthelist in lnk_clear
				players = 	servicePlayer200.clear(
	);  
 
				put(request.getSession(), PLAYERS,players);
								// processing variables Clearthelist in lnk_clear

			} catch (ApplicationException e) { 
				// error handling for operation clear called Clearthelist
				errorLogger("An error occured during the execution of the operation : clear",e); 
		
			}
	}
		
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("UC200_DatagridLoadingController [ ");
						strBToS.append(PLAYERS);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(players);
			strBToS.append(END_TO_STRING_DELIMITER);
		// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(uC200_DatagridLoadingValidator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsUC200_DatagridLoadingController!=null); 
		return strBToS.toString();
	}
}
