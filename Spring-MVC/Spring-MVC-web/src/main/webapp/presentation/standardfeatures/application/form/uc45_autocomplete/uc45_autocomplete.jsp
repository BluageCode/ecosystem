<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file="/include.jsp"  %>
<html >
<head >
	<script src="<%=request.getContextPath()%>/inc/yui/yahoo-dom-event/yahoo-dom-event.js"></script>
	<script	src="<%=request.getContextPath()%>/inc/yui/datasource/datasource-min.js"></script>
	<script src="<%=request.getContextPath()%>/inc/yui/get/get-min.js"></script>
	<script src="<%=request.getContextPath()%>/inc/yui/connection/connection-min.js"></script>
	<script src="<%=request.getContextPath()%>/inc/yui/animation/animation-min.js"></script>
	<script src="<%=request.getContextPath()%>/inc/yui/json/json-min.js"></script>
	<script src="<%=request.getContextPath()%>/inc/yui/autocomplete/autocomplete-min.js"></script>
	<link type="text/css" rel="stylesheet" href="<%=request.getContextPath()%>/inc/yui/autocomplete/assets/skins/sam/autocomplete.css" ></link>
	<script language="javascript">
	function setAutocomplete(instancenamelist, node_for_id, node_for_id_div, nbMaxResults){
		//<!--
		var auto_req = '<%=request.getContextPath()%>/presentation/standardfeatures/application/form/uc45_autocomplete/'+instancenamelist+'autocomplete.html?';
		var auto_oDS = new YAHOO.util.XHRDataSource(auto_req);   
	    // Set the responseType   
	    auto_oDS.responseType = YAHOO.util.XHRDataSource.TYPE_TEXT;   
	    // Define the schema of the delimited results   
	    auto_oDS.responseSchema = {   
	        recordDelim: ",",   
	        fieldDelim: "\t"  
	     };   
		var node_AutoComp = new YAHOO.widget.AutoComplete(node_for_id,node_for_id_div, auto_oDS);
		node_AutoComp.queryQuestionMark = false
		if (nbMaxResults != null){
			node_AutoComp.maxResultsDisplayed =nbMaxResults;
		}
		// -->
	}		
	</script>
<title  > 
<fmt:message key="cuc45_autocomplete_BLU_AGE_DOCUMENTATION"/>
</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"  > 
</meta>
<link href="<c:url value= "/presentation/_style/_style.css"/>" rel="stylesheet" type="text/css"  /> 
<link href="<c:url value= "/presentation/_include/jquery/rd_lib.css"/>" rel="stylesheet" type="text/css"  /> 
<script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/jquery.js"/>" space="preserve"  ></script><script language="javascript" space="preserve"  > jQuery.noConflict();  
</script><script type="text/javascript" src="<c:url value= "/presentation/_include/rd_setup.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/rd_lib.js"/>" space="preserve"  ></script><link rel="shortcut icon" href="<c:url value= "/presentation/_style/favicon.ico"/>"  /> 

</head>

<body class=" yui-skin-sam" > 
<div id="rd_ghost"  > 
<small  > 
<fmt:message key="cuc45_autocomplete___CSS_DEACTIVATED"/>
</small>
</div>
<div id="rd_jghost"  > 
<small  > 
<fmt:message key="cuc45_autocomplete___JAVASCRIPT_DEACTIVATED"/>
</small>
</div>
<div id="shape"  > 
<div class="ui-layout-center"  > 
<div class="east-north"  > 
	<form:form id="uc45_autocomplete" commandName="uc45_autocompleteForm"  enctype="application/x-www-form-urlencoded" method="post">
<h2 class="rd_first"  > 
<fmt:message key="cuc45_autocomplete_STANDARD_FEATURES__APPLICATIO"/>
</h2>
<table class="rd_tableform"  >
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cuc45_autocomplete_FIRST_NAME"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
<form:input id="txt_firstName" path="name.firstName"  
		/>
	<div id="txt_firstName_div"></div>
	<script>
		setAutocomplete("instance_players", "txt_firstName", "txt_firstName_div",0);
	</script>
</td> 
</tr>
</table> 
	</form:form>
	<script language="javascript">
		function btn_click_cExecuteuc45_autocomplete(url) {
			uc45_autocompletevar = document.getElementById('uc45_autocomplete');
			uc45_autocompletevar.setAttribute('action', url);
			uc45_autocompletevar.submit();
		}
		function btn_click_cSetActionuc45_autocomplete(url) {
			uc45_autocompletevar = document.getElementById('uc45_autocomplete');
			uc45_autocompletevar.setAttribute('action', url);
		}
	</script>
</div>
<div class="east-center"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/application/form/uc45_autocomplete/uc45_autocompletecmp_tabs" />
</div>
</div>
<div class="ui-layout-north"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/application/form/uc45_autocomplete/uc45_autocompletecmp_header" />
</div>
<div class="ui-layout-south"  > 
<fmt:message key="cuc45_autocomplete___"/>
<span id="rd_jversion"  > 
<fmt:message key="cuc45_autocomplete_BLU_AGE_CORPORATION"/>
</span>
</div>
<div class="ui-layout-west"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/application/form/uc45_autocomplete/uc45_autocompletecmp_treeview" />
</div>
</div>
</body>
</html>
