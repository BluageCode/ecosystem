/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.services.hql.uc57_hqloperation ;


import org.apache.log4j.Logger;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;

import com.netfective.bluage.core.validator.AbstractValidator;
/**
* Class :UC57_SearchValidator
*/
public class UC57_SearchValidator extends AbstractValidator{
	
	// LOGGER for the class UC57_SearchValidator
	private static final Logger LOGGER = Logger.getLogger( UC57_SearchValidator.class);
	
	
	/**
	* Operation validate for UC57_SearchForm
	* @param obj : the current form (UC57_SearchForm)
	* @param errors : The spring errors to return for the form UC57_SearchForm
	*/
	public void validate(final Object obj,final Errors errors){
		LOGGER.info("Beginning method : validate"); // UC57_SearchValidator
		UC57_SearchForm cUC57_SearchForm = (UC57_SearchForm)obj; // UC57_SearchValidator
				ValidationUtils.rejectIfEmpty(errors, "hqlAttribute.maxresult", "", "Enter a max number of results.");

			LOGGER.info("Ending method : validate the form "+ cUC57_SearchForm.getClass().getName()); // UC57_SearchValidator
	}

	/**
	* Method to implements to use spring validators (UC57_SearchForm)
	* @param aClass : Class for the form UC57_SearchForm
	* @return boolean
	*/
	public boolean supports(final Class aClass){
		return (new UC57_SearchForm()).getClass().equals(aClass); // UC57_SearchValidator
	}
}
