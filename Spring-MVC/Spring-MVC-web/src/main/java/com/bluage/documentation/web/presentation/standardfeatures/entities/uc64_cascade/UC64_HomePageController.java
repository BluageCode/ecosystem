/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.entities.uc64_cascade ;

// Import declaration.
// Java imports.
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.bluage.documentation.business.standardfeatures.entities.uc64_cascade.bo.State64BO;
import com.bluage.documentation.business.standardfeatures.entities.uc64_cascade.bo.Team64BO;
import com.bluage.documentation.business.standardfeatures.entities.uc64_cascade.entities.daofinder.Audience64DAOFinderImpl;
import com.bluage.documentation.business.standardfeatures.entities.uc64_cascade.entities.daofinder.Coach64DAOFinderImpl;
import com.bluage.documentation.business.standardfeatures.entities.uc64_cascade.entities.daofinder.Player64DAOFinderImpl;
import com.bluage.documentation.business.standardfeatures.entities.uc64_cascade.entities.daofinder.State64DAOFinderImpl;
import com.bluage.documentation.business.standardfeatures.entities.uc64_cascade.entities.daofinder.Team64DAOFinderImpl;
import com.bluage.documentation.service.standardfeatures.entities.uc64_cascade.ServiceState64;
import com.bluage.documentation.service.standardfeatures.entities.uc64_cascade.ServiceTeam64;
import com.netfective.bluage.core.exception.ApplicationException;
import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;
import com.netfective.bluage.core.web.IForm;

/**
 * Class : UC64_HomePageController
 */
@Controller
@Scope("prototype")
@SessionAttributes("uC64_HomePageForm")
@RequestMapping(value= "/presentation/standardfeatures/entities/uc64_cascade")
public class UC64_HomePageController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC64_HomePageController.class);
	
	//Current form name
	private static final String U_C64__HOME_PAGE_FORM = "uC64_HomePageForm";

	//CONSTANT: allcoachs table or repeater.
	private static final String ALLCOACHS = "allcoachs";
				//CONSTANT: allaudiences table or repeater.
	private static final String ALLAUDIENCES = "allaudiences";
				//CONSTANT: allstates table or repeater.
	private static final String ALLSTATES = "allstates";
				//CONSTANT: listTeams table or repeater.
	private static final String LIST_TEAMS = "listTeams";
				//CONSTANT: allPlayers table or repeater.
	private static final String ALL_PLAYERS = "allPlayers";
				//CONSTANT: selectedTeam
	private static final String SELECTED_TEAM = "selectedTeam";
	//CONSTANT: teamToUpdate
	private static final String TEAM_TO_UPDATE = "teamToUpdate";
	//CONSTANT: selectedState
	private static final String SELECTED_STATE = "selectedState";
	
	/**
	 * Property:customDateEditorsUC64_HomePageController
	 */
	final private Map<String,CustomDateEditor> customDateEditorsUC64_HomePageController = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:uC64_HomePageValidator
	 */
	final private UC64_HomePageValidator uC64_HomePageValidator = new UC64_HomePageValidator();
	
	/**
	 * Service declaration : serviceState64.
	 */
	@Autowired
	private ServiceState64 serviceState64;
	
	/**
	 * Service declaration : serviceTeam64.
	 */
	@Autowired
	private ServiceTeam64 serviceTeam64;
	
	/**
	 * Generic Finder : team64DAOFinderImpl.
	 */
	@Autowired
	private Team64DAOFinderImpl team64DAOFinderImpl;
	
	/**
	 * Generic Finder : player64DAOFinderImpl.
	 */
	@Autowired
	private Player64DAOFinderImpl player64DAOFinderImpl;
	
	/**
	 * Generic Finder : coach64DAOFinderImpl.
	 */
	@Autowired
	private Coach64DAOFinderImpl coach64DAOFinderImpl;
	
	/**
	 * Generic Finder : state64DAOFinderImpl.
	 */
	@Autowired
	private State64DAOFinderImpl state64DAOFinderImpl;
	
	/**
	 * Generic Finder : audience64DAOFinderImpl.
	 */
	@Autowired
	private Audience64DAOFinderImpl audience64DAOFinderImpl;
	
	
	// Initialise all the instances for UC64_HomePageController
		// Initialize the instance allcoachs for the state lnk_sort
		private List allcoachs; // Initialize the instance allcoachs for UC64_HomePageController
						// Initialize the instance allaudiences for the state lnk_sort
		private List allaudiences; // Initialize the instance allaudiences for UC64_HomePageController
						// Initialize the instance allstates for the state lnk_sort
		private List allstates; // Initialize the instance allstates for UC64_HomePageController
						// Initialize the instance listTeams for the state lnk_sort
		private List listTeams; // Initialize the instance listTeams for UC64_HomePageController
						// Initialize the instance allPlayers for the state lnk_sort
		private List allPlayers; // Initialize the instance allPlayers for UC64_HomePageController
						// Declare the instance selectedTeam
		private Team64BO selectedTeam;
		// Declare the instance teamToUpdate
		private Team64BO teamToUpdate;
		// Declare the instance selectedState
		private State64BO selectedState;
			/**
	 * Operation : lnk_deleteTeam
 	 * @param model : 
 	 * @param uC64_HomePage : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC64_HomePage/lnk_deleteTeam.html",method = RequestMethod.POST)
	public String lnk_deleteTeam(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC64_HomePageForm") UC64_HomePageForm  uC64_HomePageForm, final BindingResult bindingResult ,final @RequestParam(ID_REQUEST_PARAM) Integer index){
		
		
		
		LOGGER.info("Begin method : lnk_deleteTeam"); 
		infoLogger(uC64_HomePageForm); 
		uC64_HomePageForm.setAlwaysCallPreControllers(false); 
		// initialization for lnk_deleteTeam
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state lnk_deleteTeam if the validation fail, you will be forward to the screen : "/presentation/standardfeatures/entities/uc64_cascade/UC64_HomePage"
		if(errorsMessages(request,response,uC64_HomePageForm,bindingResult,"selectedTeam", uC64_HomePageValidator, customDateEditorsUC64_HomePageController)){ 
			return "/presentation/standardfeatures/entities/uc64_cascade/UC64_HomePage"; 
		}

										 callDeleteselectedteam(request,uC64_HomePageForm , index);
						// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.entities.uc64_cascade.UC64_HomePageForm or not from lnk_deleteTeam
			// Populate the destination form with all the instances returned from lnk_deleteTeam.
			final IForm uC64_HomePageForm2 = populateDestinationForm(request.getSession(), uC64_HomePageForm); 
					
			// Add the form to the model.
			model.addAttribute("uC64_HomePageForm", uC64_HomePageForm2); 
			
			request.getSession().setAttribute("uC64_HomePageForm", uC64_HomePageForm2);
			
			// "OK" CASE => destination screen path from lnk_deleteTeam
			LOGGER.info("Go to the screen 'UC64_HomePage'.");
			// Redirect (PRG) from lnk_deleteTeam
			destinationPath =  "redirect:/presentation/standardfeatures/entities/uc64_cascade/UC64_HomePage.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_deleteTeam 
	}
	
				/**
	 * Operation : lnk_details
 	 * @param model : 
 	 * @param uC64_HomePage : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC64_HomePage/lnk_details.html",method = RequestMethod.POST)
	public String lnk_details(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC64_HomePageForm") UC64_HomePageForm  uC64_HomePageForm, final BindingResult bindingResult ,final @RequestParam(ID_REQUEST_PARAM) Integer index){
		
		
		
		LOGGER.info("Begin method : lnk_details"); 
		infoLogger(uC64_HomePageForm); 
		uC64_HomePageForm.setAlwaysCallPreControllers(false); 
		// initialization for lnk_details
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state lnk_details if the validation fail, you will be forward to the screen : "/presentation/standardfeatures/entities/uc64_cascade/UC64_HomePage"
		if(errorsMessages(request,response,uC64_HomePageForm,bindingResult,"selectedTeam", uC64_HomePageValidator, customDateEditorsUC64_HomePageController)){ 
			return "/presentation/standardfeatures/entities/uc64_cascade/UC64_HomePage"; 
		}

										 callTeamFindByID(request,uC64_HomePageForm , index);
											// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.entities.uc64_cascade.UC64_UpdateTeamForm or not from lnk_details
			final UC64_UpdateTeamForm uC64_UpdateTeamForm =  new UC64_UpdateTeamForm();
			// Populate the destination form with all the instances returned from lnk_details.
			final IForm uC64_UpdateTeamForm2 = populateDestinationForm(request.getSession(), uC64_UpdateTeamForm); 
					
			// Add the form to the model.
			model.addAttribute("uC64_UpdateTeamForm", uC64_UpdateTeamForm2); 
			
			request.getSession().setAttribute("uC64_UpdateTeamForm", uC64_UpdateTeamForm2);
			
			// "OK" CASE => destination screen path from lnk_details
			LOGGER.info("Go to the screen 'UC64_UpdateTeam'.");
			// Redirect (PRG) from lnk_details
			destinationPath =  "redirect:/presentation/standardfeatures/entities/uc64_cascade/UC64_UpdateTeam.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_details 
	}
	
				/**
	 * Operation : lnk_deleteState
 	 * @param model : 
 	 * @param uC64_HomePage : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC64_HomePage/lnk_deleteState.html",method = RequestMethod.POST)
	public String lnk_deleteState(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC64_HomePageForm") UC64_HomePageForm  uC64_HomePageForm, final BindingResult bindingResult ,final @RequestParam(ID_REQUEST_PARAM) Integer index){
		
		
		
		LOGGER.info("Begin method : lnk_deleteState"); 
		infoLogger(uC64_HomePageForm); 
		uC64_HomePageForm.setAlwaysCallPreControllers(false); 
		// initialization for lnk_deleteState
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state lnk_deleteState if the validation fail, you will be forward to the screen : "/presentation/standardfeatures/entities/uc64_cascade/UC64_HomePage"
		if(errorsMessages(request,response,uC64_HomePageForm,bindingResult,"selectedState", uC64_HomePageValidator, customDateEditorsUC64_HomePageController)){ 
			return "/presentation/standardfeatures/entities/uc64_cascade/UC64_HomePage"; 
		}

										destinationPath =  callDeleteselectedstate(request,uC64_HomePageForm , index, model);
					if(destinationPath != null){
						return destinationPath;
					}
						// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.entities.uc64_cascade.UC64_HomePageForm or not from lnk_deleteState
			// Populate the destination form with all the instances returned from lnk_deleteState.
			final IForm uC64_HomePageForm2 = populateDestinationForm(request.getSession(), uC64_HomePageForm); 
					
			// Add the form to the model.
			model.addAttribute("uC64_HomePageForm", uC64_HomePageForm2); 
			
			request.getSession().setAttribute("uC64_HomePageForm", uC64_HomePageForm2);
			
			// "OK" CASE => destination screen path from lnk_deleteState
			LOGGER.info("Go to the screen 'UC64_HomePage'.");
			// Redirect (PRG) from lnk_deleteState
			destinationPath =  "redirect:/presentation/standardfeatures/entities/uc64_cascade/UC64_HomePage.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_deleteState 
	}
	
	
	/**
	* This method initialise the form : UC64_HomePageForm 
	* @return UC64_HomePageForm
	*/
	@ModelAttribute("UC64_HomePageFormInit")
	public void initUC64_HomePageForm(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, U_C64__HOME_PAGE_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method UC64_HomePageForm."); //for lnk_deleteState 
		}
		UC64_HomePageForm uC64_HomePageForm;
	
		if(request.getSession().getAttribute(U_C64__HOME_PAGE_FORM) != null){
			uC64_HomePageForm = (UC64_HomePageForm)request.getSession().getAttribute(U_C64__HOME_PAGE_FORM);
		} else {
			uC64_HomePageForm = new UC64_HomePageForm();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form UC64_HomePageForm.");
		}
		uC64_HomePageForm = (UC64_HomePageForm)populateDestinationForm(request.getSession(), uC64_HomePageForm);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  UC64_HomePageForm.");
		}
		model.addAttribute(U_C64__HOME_PAGE_FORM, uC64_HomePageForm);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param UC64_HomePageForm : The sceen form.
	 */
	@RequestMapping(value = "/UC64_HomePage.html" ,method = RequestMethod.GET)
	public void prepareUC64_HomePage(final HttpServletRequest request,final  Model model,final  @ModelAttribute("UC64_HomePageFormInit") UC64_HomePageForm uC64_HomePageForm){
		
		UC64_HomePageForm currentUC64_HomePageForm = uC64_HomePageForm;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(U_C64__HOME_PAGE_FORM) == null){
			if(currentUC64_HomePageForm!=null){
				request.getSession().setAttribute(U_C64__HOME_PAGE_FORM, currentUC64_HomePageForm);
			}else {
				currentUC64_HomePageForm = new UC64_HomePageForm();
				request.getSession().setAttribute(U_C64__HOME_PAGE_FORM, currentUC64_HomePageForm);	
			}
		} else {
			currentUC64_HomePageForm = (UC64_HomePageForm) request.getSession().getAttribute(U_C64__HOME_PAGE_FORM);
		}

		try {
			List listTeams = team64DAOFinderImpl.findAll();
			put(request.getSession(), LIST_TEAMS, listTeams);
		} catch (ApplicationException e) {
			errorLogger("An error while calling the generic finder for the instance : listTeams.",e);
		}
		try {
			List allPlayers = player64DAOFinderImpl.findAll();
			put(request.getSession(), ALL_PLAYERS, allPlayers);
		} catch (ApplicationException e) {
			errorLogger("An error while calling the generic finder for the instance : allPlayers.",e);
		}
		try {
			List allcoachs = coach64DAOFinderImpl.findAll();
			put(request.getSession(), ALLCOACHS, allcoachs);
		} catch (ApplicationException e) {
			errorLogger("An error while calling the generic finder for the instance : allcoachs.",e);
		}
		try {
			List allstates = state64DAOFinderImpl.findAll();
			put(request.getSession(), ALLSTATES, allstates);
		} catch (ApplicationException e) {
			errorLogger("An error while calling the generic finder for the instance : allstates.",e);
		}
		try {
			List allaudiences = audience64DAOFinderImpl.findAll();
			put(request.getSession(), ALLAUDIENCES, allaudiences);
		} catch (ApplicationException e) {
			errorLogger("An error while calling the generic finder for the instance : allaudiences.",e);
		}
												currentUC64_HomePageForm = (UC64_HomePageForm)populateDestinationForm(request.getSession(), currentUC64_HomePageForm);
		// Call all the Precontroller.
	currentUC64_HomePageForm.setAlwaysCallPreControllers(true);
		model.addAttribute(U_C64__HOME_PAGE_FORM, currentUC64_HomePageForm);
		request.getSession().setAttribute(U_C64__HOME_PAGE_FORM, currentUC64_HomePageForm);
		
	}
	
																		/**
	 * method callDeleteselectedteam
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 * @param index Integer the current index 
	 */
	private void callDeleteselectedteam(HttpServletRequest request,IForm form,Integer index) {
					if(index!=null){  
			 selectedTeam = (Team64BO)((List)get(request.getSession(),form, "listTeams")).get(index);  
		} else {
			 selectedTeam= null; 
		}
				 
					try {
				// executing Deleteselectedteam in lnk_deleteState
	serviceTeam64.teamDelete(
			selectedTeam
			);  

													// processing variables Deleteselectedteam in lnk_deleteState
				put(request.getSession(), SELECTED_TEAM,selectedTeam); 
			
			} catch (ApplicationException e) { 
				// error handling for operation teamDelete called Deleteselectedteam
				errorLogger("An error occured during the execution of the operation : teamDelete",e); 
		
			}
	}
									/**
	 * method callTeamFindByID
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 * @param index Integer the current index 
	 */
	private void callTeamFindByID(HttpServletRequest request,IForm form,Integer index) {
		teamToUpdate = (Team64BO)get(request.getSession(),form, "teamToUpdate");  
										 
					if(index!=null){  
			 selectedTeam = (Team64BO)((List)get(request.getSession(),form, "listTeams")).get(index);  
		} else {
			 selectedTeam= null; 
		}
				 
					try {
				// executing TeamFindByID in lnk_deleteState
				teamToUpdate = 	serviceTeam64.teamFindByID(
			selectedTeam
			);  
 
				put(request.getSession(), TEAM_TO_UPDATE,teamToUpdate);
								// processing variables TeamFindByID in lnk_deleteState
				put(request.getSession(), SELECTED_TEAM,selectedTeam); 
			
			} catch (ApplicationException e) { 
				// error handling for operation teamFindByID called TeamFindByID
				errorLogger("An error occured during the execution of the operation : teamFindByID",e); 
		
			}
	}
												/**
	 * method callDeleteselectedstate
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 * @param index Integer the current index 
	 * @param model Model the model
	 * @return String result NOK 
	 */
	private String callDeleteselectedstate(HttpServletRequest request,IForm form,Integer index, final Model model) {
		String str = null;
					if(index!=null){  
			 selectedState = (State64BO)((List)get(request.getSession(),form, "allstates")).get(index);  
		} else {
			 selectedState= null; 
		}
				 
					try {
				// executing Deleteselectedstate in lnk_deleteState
	serviceState64.stateWithoutTeamsDelete(
			selectedState
			);  

													// processing variables Deleteselectedstate in lnk_deleteState
				put(request.getSession(), SELECTED_STATE,selectedState); 
			
			} catch (ApplicationException e) { 
				// error handling for operation stateWithoutTeamsDelete called Deleteselectedstate
				errorLogger("An error occured during the execution of the operation : stateWithoutTeamsDelete",e); 
		
				// "NOK" DEFAULT CASE
				UC64_exceptionForm uC64_exceptionForm = new UC64_exceptionForm();
				final IForm uC64_exceptionForm2 = populateDestinationForm(request.getSession(), uC64_exceptionForm);
				model.addAttribute("uC64_exceptionForm", uC64_exceptionForm2);
				request.getSession().setAttribute("uC64_exceptionForm", uC64_exceptionForm2);
				request.getSession().setAttribute("messages", e.getMessage());
				return "redirect:/presentation/standardfeatures/entities/uc64_cascade/UC64_exception.html";
						}
		return str;
	}
		
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("UC64_HomePageController [ ");
			strBToS.append(ALLCOACHS);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(allcoachs);
			strBToS.append(END_TO_STRING_DELIMITER);
						strBToS.append(ALLAUDIENCES);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(allaudiences);
			strBToS.append(END_TO_STRING_DELIMITER);
						strBToS.append(ALLSTATES);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(allstates);
			strBToS.append(END_TO_STRING_DELIMITER);
						strBToS.append(LIST_TEAMS);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(listTeams);
			strBToS.append(END_TO_STRING_DELIMITER);
						strBToS.append(ALL_PLAYERS);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(allPlayers);
			strBToS.append(END_TO_STRING_DELIMITER);
						strBToS.append(SELECTED_TEAM);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(selectedTeam);
			strBToS.append(END_TO_STRING_DELIMITER);
			strBToS.append(TEAM_TO_UPDATE);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(teamToUpdate);
			strBToS.append(END_TO_STRING_DELIMITER);
			strBToS.append(SELECTED_STATE);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(selectedState);
			strBToS.append(END_TO_STRING_DELIMITER);
		// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(uC64_HomePageValidator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsUC64_HomePageController!=null); 
		return strBToS.toString();
	}
}
