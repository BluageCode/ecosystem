/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.service.standardfeatures.application.component.uc47_contents.daofinder;


import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateTemplate;

import com.bluage.documentation.business.standardfeatures.application.component.uc47_contents.bos.Player47BO;
import com.bluage.documentation.business.standardfeatures.application.component.uc47_contents.bos.PlayerTeam47BO;
import com.bluage.documentation.business.standardfeatures.application.component.uc47_contents.bos.Team47BO;
import com.netfective.bluage.core.exception.ApplicationException;


/**
 * This class represents query invocation methods implementation
 * for ServicePlayerTeam47 service.
 *
 * @see com.bluage.documentation.service.standardfeatures.application.component.uc47_contents.ServicePlayerTeam47
 */
 
@Named ("com.bluage.documentation.service.standardfeatures.application.component.uc47_contents.daofinder.ServicePlayerTeam47DAOMethodFinderImpl")
public class ServicePlayerTeam47DAOMethodFinderImpl {

	// LOGGER for ServicePlayerTeam47DAOMethodFinderImpl
	private static final Logger LOGGER = Logger.getLogger(ServicePlayerTeam47DAOMethodFinderImpl.class);
	
	/**
	* Hibernate Template (injected)
	*/
	@Inject
	private HibernateTemplate hibernateTemplate;

	
	/**
	 * setter for HibernateTemplate
	 * @param value : {@link HibernateTemplate}
	 */
	public void setHibernateTemplate( HibernateTemplate value )
	{
		hibernateTemplate = value;
	}
	

	/**    
	 * HQL operation.
	 * @param team {@link Team47BO}
		 * @param player {@link Player47BO}
		 * @return {@link PlayerTeam47BO}
	 */
	public PlayerTeam47BO searchPlayerTeam47(Team47BO team,Player47BO player )
 throws ApplicationException {
Session session = hibernateTemplate.getSessionFactory().getCurrentSession();
			return 
				privateSearchPlayerTeam47(team,player, session);
	}

	
	/**    
	 * private delegation for HQL operation.
	 * @param team {@link Team47BO}
		 * @param player {@link Player47BO}
		 * @return {@link PlayerTeam47BO}
	 */
	private PlayerTeam47BO privateSearchPlayerTeam47(Team47BO team,Player47BO player , Session session)
 throws ApplicationException {
			Query query = session.createQuery(" select pt from PlayerTeam47BO pt inner join pt.team t left join pt.player p where p = :player and t = :team ");

			query.setParameter( "team", team);
						
			query.setParameter( "player", player);
									PlayerTeam47BO returnValue = null;
		try {
			Object result = query.uniqueResult();
			if( result != null ) {
				returnValue = ( com.bluage.documentation.business.standardfeatures.application.component.uc47_contents.bos.PlayerTeam47BO )result;
			}				
		} catch(HibernateException ex) {
			LOGGER.error("An error occured while excecuting the HQL operation : com.bluage.documentation.service.standardfeatures.application.component.uc47_contents.searchPlayerTeam47");
			throw new ApplicationException(ex);
		}
		return returnValue;
	}

	




}
