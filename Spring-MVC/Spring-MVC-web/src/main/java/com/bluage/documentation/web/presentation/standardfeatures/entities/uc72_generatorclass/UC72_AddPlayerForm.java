/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.entities.uc72_generatorclass;


import java.io.Serializable;
import java.util.List;

import com.bluage.documentation.business.standardfeatures.entities.uc72_generatorclass.bos.Player72BO;
import com.netfective.bluage.core.web.AbstractForm;
import com.netfective.bluage.core.web.IForm;

/**
* Class : UC72_AddPlayerForm
*/
public class UC72_AddPlayerForm extends AbstractForm implements Serializable, IForm {
	
	/** Constant : serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	//CONSTANT : allPositions
	private static final String ALL_POSITIONS = "allPositions";
	//CONSTANT : listPlayers
	private static final String LIST_PLAYERS = "listPlayers";
	//CONSTANT : selectedPlayer
	private static final String SELECTED_PLAYER = "selectedPlayer";
	//CONSTANT : playerToCreate
	private static final String PLAYER_TO_CREATE = "playerToCreate";
	/**
	 * 	Property: allPositions 
	 */
	private List allPositions;
	/**
	 * 	Property: listPlayers 
	 */
	private List<Player72BO> listPlayers;
	/**
	 * 	Property: selectedPlayer 
	 */
	private Player72BO selectedPlayer;
	/**
	 * 	Property: playerToCreate 
	 */
	private Player72BO playerToCreate;
/**
	 * Default constructor : UC72_AddPlayerForm
	 */
	public UC72_AddPlayerForm() {
		super();
		// Initialize : allPositions
		this.allPositions = new java.util.ArrayList();
		// Initialize : listPlayers
		this.listPlayers = new java.util.ArrayList<com.bluage.documentation.business.standardfeatures.entities.uc72_generatorclass.bos.Player72BO>();
		// Initialize : selectedPlayer
		this.selectedPlayer = null;
		// Initialize : playerToCreate
		this.playerToCreate = new Player72BO();
					this.setAlwaysCallPreControllers(true);
	}
	
	/**
	 * 	Getter : allPositions 
	 * 	@return : Return the allPositions instance.
	 */
	public List getAllPositions(){
		return allPositions; // For UC72_AddPlayerForm
	}
	
	/**
	 * 	Setter : allPositions 
	 *  @param allPositionsinstance : The instance to set.
	 */
	public void setAllPositions(final List allPositionsinstance){
		this.allPositions = allPositionsinstance;// For UC72_AddPlayerForm
	}
	/**
	 * 	Getter : listPlayers 
	 * 	@return : Return the listPlayers instance.
	 */
	public List<Player72BO> getListPlayers(){
		return listPlayers; // For UC72_AddPlayerForm
	}
	
	/**
	 * 	Setter : listPlayers 
	 *  @param listPlayersinstance : The instance to set.
	 */
	public void setListPlayers(final List<Player72BO> listPlayersinstance){
		this.listPlayers = listPlayersinstance;// For UC72_AddPlayerForm
	}
	/**
	 * 	Getter : selectedPlayer 
	 * 	@return : Return the selectedPlayer instance.
	 */
	public Player72BO getSelectedPlayer(){
		return selectedPlayer; // For UC72_AddPlayerForm
	}
	
	/**
	 * 	Setter : selectedPlayer 
	 *  @param selectedPlayerinstance : The instance to set.
	 */
	public void setSelectedPlayer(final Player72BO selectedPlayerinstance){
		this.selectedPlayer = selectedPlayerinstance;// For UC72_AddPlayerForm
	}
	/**
	 * 	Getter : playerToCreate 
	 * 	@return : Return the playerToCreate instance.
	 */
	public Player72BO getPlayerToCreate(){
		return playerToCreate; // For UC72_AddPlayerForm
	}
	
	/**
	 * 	Setter : playerToCreate 
	 *  @param playerToCreateinstance : The instance to set.
	 */
	public void setPlayerToCreate(final Player72BO playerToCreateinstance){
		this.playerToCreate = playerToCreateinstance;// For UC72_AddPlayerForm
	}
/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UC72_AddPlayerForm [ "+
ALL_POSITIONS +" = " + allPositions +LIST_PLAYERS +" = " + listPlayers +SELECTED_PLAYER +" = " + selectedPlayer +PLAYER_TO_CREATE +" = " + playerToCreate + "]";
	}
	
	/**
	 * Method setInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public void setInstanceByInstanceName(final String instanceName, final Object instance) {
		// Set the instance AllPositions.
		if(ALL_POSITIONS.equals(instanceName)){// For UC72_AddPlayerForm
			this.setAllPositions((List)instance); // For UC72_AddPlayerForm
		}
				// Set the instance ListPlayers.
		if(LIST_PLAYERS.equals(instanceName)){// For UC72_AddPlayerForm
			this.setListPlayers((List<Player72BO>)instance); // For UC72_AddPlayerForm
		}
				// Set the instance SelectedPlayer.
		if(SELECTED_PLAYER.equals(instanceName)){// For UC72_AddPlayerForm
			this.setSelectedPlayer((Player72BO)instance); // For UC72_AddPlayerForm
		}
				// Set the instance PlayerToCreate.
		if(PLAYER_TO_CREATE.equals(instanceName)){// For UC72_AddPlayerForm
			this.setPlayerToCreate((Player72BO)instance); // For UC72_AddPlayerForm
		}
			}
	
	/**
	 * Method getInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public Object getInstanceByInstanceName(final String instanceName){
		// Temporary object for tmpUC72_AddPlayerForm.
		Object tmpUC72_AddPlayerForm = null;
		
			
		// Get the instance AllPositions for UC72_AddPlayerForm.
		if(ALL_POSITIONS.equals(instanceName)){ // For UC72_AddPlayerForm
			tmpUC72_AddPlayerForm = this.getAllPositions(); // For UC72_AddPlayerForm
		}
			
		// Get the instance ListPlayers for UC72_AddPlayerForm.
		if(LIST_PLAYERS.equals(instanceName)){ // For UC72_AddPlayerForm
			tmpUC72_AddPlayerForm = this.getListPlayers(); // For UC72_AddPlayerForm
		}
			
		// Get the instance SelectedPlayer for UC72_AddPlayerForm.
		if(SELECTED_PLAYER.equals(instanceName)){ // For UC72_AddPlayerForm
			tmpUC72_AddPlayerForm = this.getSelectedPlayer(); // For UC72_AddPlayerForm
		}
			
		// Get the instance PlayerToCreate for UC72_AddPlayerForm.
		if(PLAYER_TO_CREATE.equals(instanceName)){ // For UC72_AddPlayerForm
			tmpUC72_AddPlayerForm = this.getPlayerToCreate(); // For UC72_AddPlayerForm
		}
		return tmpUC72_AddPlayerForm;// For UC72_AddPlayerForm
	}
	
			}
