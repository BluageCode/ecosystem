/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506

package com.bluage.documentation.business;
 
/**
 *
 * @author NETFECTIVE TECHNOLOGY
 *  
 */
public interface IBusinessStringConstants   {


	/**
	*  select count( * ) 
	*/
	String L_SELECT_COUNT_STAR = "select count( * ) ";


	/**
	*  L_FROM
	*/
	String L_FROM = "from ";


	/**
	*  L_FIND_DATA
	*/
	String L_FIND_DATA = "findData";


	/**
	*  L_KEY
	*/
	String L_KEY = "key =";


	/**
	*  L_PROPERTIES
	*/
	String L_PROPERTIES = "properties =";


	/**
	*  L_HIBERNATE_TEMPLATE
	*/
	String L_HIBERNATE_TEMPLATE = "hibernateTemplate";


	/**
	*  L_APPLICATION_NAME
	*/
	String L_APPLICATION_NAME = "BA-Doc-Struts";


	/**
	*  L_BEGIN_ERROR_WHILE_EXECUTING_HQL
	*/
	String L_BEGIN_ERROR_WHILE_EXECUTING_HQL = "Error while executing the HQL [";


	/**
	*  L_ERROR_WHILE_EXECUTING_HQL
	*/
	String L_END_ERROR_WHILE_EXECUTING_HQL = "]";


	/**
	*  L_SELF
	*/
	String L_SELF = "self";


	/**
	*  L_CREATE
	*/
	String L_CREATE = "create";


	/**
	*  L_UPDATE
	*/
	String L_UPDATE = "update";


	/**
	*  L_DELETE
	*/
	String L_DELETE = "delete";


	/**
	*  L_UPSERT
	*/
	String L_UPSERT = "upsert";


	/**
	*  L_CREATE_ALL
	*/
	String L_CREATE_ALL = "createAll";


	/**
	*  L_UPDATE_ALL
	*/
	String L_UPDATE_ALL = "updateAll";


	/**
	*  L_DELETE_ALL
	*/
	String L_DELETE_ALL = "deleteAll";


	/**
	*  L_FIND_ALL
	*/
	String L_FIND_ALL = "findAll";


	/**
	*  L_FIND_AND_LOCK
	*/
	String L_FIND_AND_LOCK = "findAndLock";


	/**
	*  L_FIND_BY_PRIMARY_KEY
	*/
	String L_FIND_BY_PRIMARY_KEY = "findByPrimaryKey";


	/**
	*  L_FIND_BY_PROPERTIES
	*/
	String L_FIND_BY_PROPERTIES = "findByProperties";


	/**
	*  L_BEG_FIND_ALL_BY_CLASS_NAME
	*/
	String L_BEG_FIND_ALL_BY_CLASS_NAME = "findAllByClassName [";


	/**
	*  L_END_FIND_ALL_BY_CLASS_NAME
	*/
	String L_END_FIND_ALL_BY_CLASS_NAME = "]";


	/**
	*  L_FIND_BY_CRITERIA
	*/
	String L_FIND_BY_CRITERIA = "findByCriteria";


	/**
	*  L_INVOKE_STORED_PROCEDURE
	*/
	String L_INVOKE_STORED_PROCEDURE = "invokeStoredProcedure ";


	/**
	*  L_BEG_INVOKE_STORED_PROCEDURE_PARAMS
	*/
	String L_BEG_INVOKE_STORED_PROCEDURE_PARAMS = " Parameters =[";


	/**
	*  L_INTER_INVOKE_STORED_PROCEDURE_PARAMS
	*/
	String L_INTER_INVOKE_STORED_PROCEDURE_PARAMS = ", ";


	/**
	*  L_BEG_INVOKE_STORED_PROCEDURE_PARAMS
	*/
	String L_END_INVOKE_STORED_PROCEDURE_PARAMS = "]";


	/**
	*  L_RETURNS
	*/
	String L_RETURNS = "returns ";


	/**
	*  L_END_OF_METHOD 
	*/
	String L_END_OF_METHOD = "End of Method : ";


	/**
	*  L_BEGIN_METHOD 
	*/
	String L_BEGIN_METHOD = "Begin Method : ";


	/**
	*  L_WITH_ERROR 
	*/
	String L_WITH_ERROR = " with Error";


	/**
	*  L_CREATE_METHOD 
	*/
	String L_CREATE_METHOD = "create( IBusinessObject bo )";


	/**
	*  L_UPDATE_METHOD 
	*/
	String L_UPDATE_METHOD = "update( IBusinessObject bo )";


	/**
	*  L_MERGE_METHOD
	*/
	String L_MERGE_METHOD = "mergeUpdate( IBusinessObject bo )";


	/**
	*  L_UPSERT_METHOD 
	*/
	String L_UPSERT_METHOD = "upsert( IBusinessObject bo )";


	/**
	*  L_DELETE_METHOD 
	*/
	String L_DELETE_METHOD = "delete( IBusinessObject bo )";


	/**
	*  L_CREATE_ALL_METHOD
	*/
	String L_CREATE_ALL_METHOD = "createAll( IBusinessObject bo )";


	/**
	*  L_DELETE_ALL_METHOD 
	*/
	String L_DELETE_ALL_METHOD = "deleteAll( IBusinessObject bo )";


	/**
	*  L_UPDATE_ALL_METHOD 
	*/
	String L_UPDATE_ALL_METHOD = "updateAll( IBusinessObject bo )";	
}

