package uc58;

import org.junit.Test;
import test.common.AbstractBADocTestCase;

public class ITuc58 extends AbstractBADocTestCase {
	
	
	@Override
	public  String getName(){
		return "UC-58";
	}	


	@Test
	public void testUC58() throws Exception {
		// On teste l'UC58 - Standard Features/Services/Standard - Service Copy
			
		selenium.open("");	
		selenium.click("link=Expand All");
		selenium.waitForPageToLoad("30000");
		selenium.click("link=UC58 - Service Copy");
		
		verifyTrue(selenium.isElementPresent("//h2[contains(text(),\"Standard Features > Services > Standard > UC58 > Service Copy\")]"));
		// On vérifie les données.
		verifyEquals("5", selenium.getXpathCount("//table[contains(@id,'tab_teams')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Detroit\")] and td/span[contains(text(),\"Hawksdee\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody//tr[td/span[contains(text(),\"Kansas City\")] and td/span[contains(text(),\"Bears\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody//tr[td/span[contains(text(),\"Detroit\")] and td/span[contains(text(),\"Chargers\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody//tr[td/span[contains(text(),\"Omahasss\")] and td/span[contains(text(),\"Braves\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody//tr[td/span[contains(text(),\"Charlotte\")] and td/span[contains(text(),\"Tigers\")]]"));
		// On édite une ligne.
		// Pour ce faire, on indexe une ligne.
		// Pour les Braves :
		Number indexBraves = selenium.getElementIndex("//table[contains(@id,'tab_team')]/tbody/tr[td/span[contains(text(),\"Omahasss\")] and td/span[contains(text(),\"Braves\")]]");
		selenium.click("//table[contains(@id,'tab_team')]/tbody/tr[" + indexBraves + " + 1]/td[3]/a[contains(text(),\"[view]\")]");
		selenium.waitForPageToLoad("30000");
		// On vérifie que les données s'affichent correctement.
		assertTrue(selenium.isElementPresent("//tr/td/span[contains(text(),'Omahasss')]"));
		assertTrue(selenium.isElementPresent("//tr/td/span[contains(text(),'Braves')]"));
		assertTrue(selenium.isElementPresent("//tr/td/span[contains(text(),'California')]"));
//		assertEquals("Omahasss", selenium.getText("//span[contains(@id,'txt_city1')]"));
//		assertEquals("Braves", selenium.getText("//span[contains(@id,'txt_name1')]"));
//		assertEquals("California", selenium.getText("//span[contains(@id,'select_state1')]"));
		assertEquals("Omahasss", selenium.getValue("//input[contains(@id,'txt_city')]"));
		assertEquals("Braves", selenium.getValue("//input[contains(@id,'txt_name')]"));
		assertEquals("CA", join(selenium.getSelectedValues("//select[contains(@id,'select_state')]"), ','));
		selenium.type("css=input[id$='txt_city']", "Omaha");
		assertEquals("[Update]", selenium.getText("//*[contains(@id,'lnk_updateTeam')]"));
		selenium.click("//*[contains(@id,'lnk_updateTeam')]");
		selenium.waitForPageToLoad("30000");
		// On vérifie que la modification de la team est faite.
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody//tr[td/span[contains(text(),\"Omaha\")] and td/span[contains(text(),\"Braves\")]]"));
		verifyEquals("5", selenium.getXpathCount("//table[contains(@id,'tab_teams')]//tbody//tr"));
		// On copie une ligne.
		// Pour les Tigers :
		Number indexTigers = selenium.getElementIndex("//table[contains(@id,'tab_team')]/tbody/tr[td/span[contains(text(),\"Charlotte\")] and td/span[contains(text(),\"Tigers\")]]");
		selenium.click("//table[contains(@id,'tab_team')]/tbody/tr[" + indexTigers + " + 1]/td[3]/a[contains(text(),\"[view]\")]");
		selenium.waitForPageToLoad("30000");
		// On vérifie que les données sont affichées correctement.
		assertTrue(selenium.isElementPresent("//tr/td/span[contains(text(),'Charlotte')]"));
		assertTrue(selenium.isElementPresent("//tr/td/span[contains(text(),'Tigers')]"));
		assertTrue(selenium.isElementPresent("//tr/td/span[contains(text(),'Alaska')]"));
//		assertEquals("Charlotte", selenium.getText("//span[contains(@id,'txt_city1')]"));
//		assertEquals("Tigers", selenium.getText("//span[contains(@id,'txt_name1')]"));
//		assertEquals("Alaska", selenium.getText("//span[contains(@id,'select_state1')]"));
		assertEquals("Charlotte", selenium.getValue("//input[contains(@id,'txt_city')]"));
		assertEquals("Tigers", selenium.getValue("//input[contains(@id,'txt_name')]"));
		assertEquals("AK", join(selenium.getSelectedValues("//select[contains(@id,'select_state')]"), ','));
		selenium.click("//*[contains(@id,'lnk_updateTeam')]");
		selenium.waitForPageToLoad("30000");
		// On vérifie que la Team est toujours identique.
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody//tr[td/span[contains(text(),\"Charlotte\")] and td/span[contains(text(),\"Tigers\")]]"));
		verifyEquals("5", selenium.getXpathCount("//table[contains(@id,'tab_teams')]//tbody//tr"));
		// On teste le lien back.
		// Pour les Hawksdee :
		Number indexHawksdee = selenium.getElementIndex("//table[contains(@id,'tab_team')]/tbody/tr[td/span[contains(text(),\"Detroit\")] and td/span[contains(text(),\"Hawksdee\")]]");
		selenium.click("//table[contains(@id,'tab_team')]/tbody/tr[" + indexHawksdee + " + 1]/td[3]/a[contains(text(),\"[view]\")]");
		selenium.waitForPageToLoad("30000");
		// On vérifie que les données sont affichées.
		assertTrue(selenium.isElementPresent("//tr/td/span[contains(text(),'Detroit')]"));
		assertTrue(selenium.isElementPresent("//tr/td/span[contains(text(),'Hawksdee')]"));
		assertTrue(selenium.isElementPresent("//tr/td/span[contains(text(),'Delaware')]"));
//		assertEquals("Detroit", selenium.getText("//span[contains(@id,'txt_city1')]"));
//		assertEquals("Hawksdee", selenium.getText("//span[contains(@id,'txt_name1')]"));
//		assertEquals("Delaware", selenium.getText("//span[contains(@id,'select_state1')]"));
		assertEquals("Detroit", selenium.getValue("//input[contains(@id,'txt_city')]"));
		assertEquals("Hawksdee", selenium.getValue("//input[contains(@id,'txt_name')]"));
		assertEquals("DE", join(selenium.getSelectedValues("//select[contains(@id,'select_state')]"), ','));
		selenium.click("//*[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
		// On vérifie que tout s'affiche correctement.
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody//tr[td/span[contains(text(),\"Charlotte\")] and td/span[contains(text(),\"Tigers\")]]"));
		verifyEquals("5", selenium.getXpathCount("//table[contains(@id,'tab_teams')]//tbody//tr"));
		// On copie une ligne.
		// Pour les Bears :
		Number indexBears = selenium.getElementIndex("//table[contains(@id,'tab_team')]/tbody/tr[td/span[contains(text(),\"Kansas City\")] and td/span[contains(text(),\"Bears\")]]");
		selenium.click("//table[contains(@id,'tab_team')]/tbody/tr[" + indexBears + " + 1]/td[3]/a[contains(text(),\"[view]\")]");
		selenium.waitForPageToLoad("30000");
		// On vérifie que les données s'affichent correctement.
		assertTrue(selenium.isElementPresent("//tr/td/span[contains(text(),'Kansas City')]"));
		assertTrue(selenium.isElementPresent("//tr/td/span[contains(text(),'Bears')]"));
		assertTrue(selenium.isElementPresent("//tr/td/span[contains(text(),'California')]"));
//		assertEquals("Kansas City", selenium.getText("//span[contains(@id,'txt_city1')]"));
//		assertEquals("Bears", selenium.getText("//span[contains(@id,'txt_name1')]"));
//		assertEquals("California", selenium.getText("//span[contains(@id,'select_state1')]"));
		assertEquals("Kansas City", selenium.getValue("//input[contains(@id,'txt_city')]"));
		assertEquals("Bears", selenium.getValue("//input[contains(@id,'txt_name')]"));
		assertEquals("CA", join(selenium.getSelectedValues("//select[contains(@id,'select_state')]"), ','));
		selenium.click("//*[contains(@id,'lnk_updateTeam')]");
		selenium.waitForPageToLoad("30000");
		// On vérifie que tout s'affiche correctement.
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody//tr[td/span[contains(text(),\"Kansas City\")] and td/span[contains(text(),\"Bears\")]]"));
		verifyEquals("5", selenium.getXpathCount("//table[contains(@id,'tab_teams')]//tbody//tr"));
		// On modifie une ligne.
		// Pour les Chargers :
		Number indexChargers = selenium.getElementIndex("//table[contains(@id,'tab_team')]/tbody/tr[td/span[contains(text(),\"Detroit\")] and td/span[contains(text(),\"Chargers\")]]");
		selenium.click("//table[contains(@id,'tab_team')]/tbody/tr[" + indexChargers + " + 1]/td[3]/a[contains(text(),\"[view]\")]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//tr/td/span[contains(text(),'Detroit')]"));
		assertTrue(selenium.isElementPresent("//tr/td/span[contains(text(),'Chargers')]"));
		assertTrue(selenium.isElementPresent("//tr/td/span[contains(text(),'Florida')]"));
//		assertEquals("Detroit", selenium.getText("//span[contains(@id,'txt_city1')]"));
//		assertEquals("Chargers", selenium.getText("//span[contains(@id,'txt_name1')]"));
//		assertEquals("Florida", selenium.getText("//span[contains(@id,'select_state1')]"));
		assertEquals("Detroit", selenium.getValue("//input[contains(@id,'txt_city')]"));
		assertEquals("Chargers", selenium.getValue("//input[contains(@id,'txt_name')]"));
		assertEquals("FL", join(selenium.getSelectedValues("//select[contains(@id,'select_state')]"), ','));
		selenium.type("css=input[id$='txt_city']", "Detroit2");
		selenium.type("css=input[id$='txt_name']", "Chargers2");
		selenium.click("//*[contains(@id,'lnk_updateTeam')]");
		selenium.waitForPageToLoad("30000");
		// On vérifie que tout s'affiche correctement.
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody//tr[td/span[contains(text(),\"Detroit2\")] and td/span[contains(text(),\"Chargers2\")]]"));
		verifyEquals("5", selenium.getXpathCount("//table[contains(@id,'tab_teams')]//tbody//tr"));
	}


}
