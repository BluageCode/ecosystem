<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file="/include.jsp"  %>
<html >
<head >
<title  > 
<fmt:message key="cUC49_ViewTeam_BLU_AGE_DOCUMENTATION"/>
</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"  > 
</meta>
<link href="<c:url value= "/presentation/_style/_style.css"/>" rel="stylesheet" type="text/css"  /> 
<link href="<c:url value= "/presentation/_include/jquery/rd_lib.css"/>" rel="stylesheet" type="text/css"  /> 
<script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/jquery.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/rd_setup.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/rd_lib.js"/>" space="preserve"  ></script><link rel="shortcut icon" href="<c:url value= "/presentation/_style/favicon.ico"/>"  /> 

</head>

<body class=" " > 
<div id="rd_ghost"  > 
<small  > 
<fmt:message key="cUC49_ViewTeam___CSS_DEACTIVATED"/>
</small>
</div>
<div id="rd_jghost"  > 
<small  > 
<fmt:message key="cUC49_ViewTeam___JAVASCRIPT_DEACTIVATED"/>
</small>
</div>
<div id="shape"  > 
<div class="ui-layout-center"  > 
<div class="east-north"  > 
	<form:form id="uC49_ViewTeam" commandName="uC49_ViewTeamForm"  enctype="application/x-www-form-urlencoded" method="post">
<h2 class="rd_first"  > 
<fmt:message key="cUC49_ViewTeam_STANDARD_FEATURES__SERVICES_"/>
</h2>
<h4  > 
<fmt:message key="cUC49_ViewTeam_TEAM_INFORMATION"/>
</h4>
<table class="rd_tableform"  >
<tr  > 
<td rowspan="1" colspan="1"  >
<strong  > 
<fmt:message key="cUC49_ViewTeam_NAME"/>
</strong>
</td> 
<td rowspan="1" colspan="1"  >
		<span id="dyn_name">
	<c:out value="${uC49_ViewTeamForm.teamToDisplay.name}" />
		</span>
	</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<strong  > 
<fmt:message key="cUC49_ViewTeam_CITY"/>
</strong>
</td> 
<td rowspan="1" colspan="1"  >
		<span id="dyn_city">
	<c:out value="${uC49_ViewTeamForm.teamToDisplay.city}" />
		</span>
	</td> 
</tr>
</table> 
<a id="lnk_back"				href="<c:url value="/presentation/standardfeatures/services/bags/uc49_bags/UC49_ViewTeam/lnk_back.html"/>"
													 shape="rect"  >									 
<fmt:message key="cUC49_ViewTeam_BACK"/>
</a>
	</form:form>
	<script language="javascript">
		function btn_click_cExecuteUC49_ViewTeam(url) {
			UC49_ViewTeamvar = document.getElementById('uC49_ViewTeam');
			UC49_ViewTeamvar.setAttribute('action', url);
			UC49_ViewTeamvar.submit();
		}
		function btn_click_cSetActionUC49_ViewTeam(url) {
			UC49_ViewTeamvar = document.getElementById('uC49_ViewTeam');
			UC49_ViewTeamvar.setAttribute('action', url);
		}
	</script>
</div>
<div class="east-center"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/services/bags/uc49_bags/UC49_ViewTeamcmp_tabs" />
</div>
</div>
<div class="ui-layout-north"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/services/bags/uc49_bags/UC49_ViewTeamcmp_header" />
</div>
<div class="ui-layout-south"  > 
<fmt:message key="cUC49_ViewTeam___"/>
<span id="rd_jversion"  > 
<fmt:message key="cUC49_ViewTeam_BLU_AGE_DOCUMENTATION_206"/>
</span>
</div>
<div class="ui-layout-west"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/services/bags/uc49_bags/UC49_ViewTeamcmp_treeview" />
</div>
</div>
</body>
</html>
