/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.business.standardfeatures.services.sql.uc56_sqloperation.bos;

import javax.xml.bind.annotation.XmlRootElement;

import com.bluage.documentation.business.AbstractBusinessObject;

/**
 * Team56ForPostProcessBO
 *
* @see com.netfective.bluage.core.business.IBusinessObject
 */
@XmlRootElement

public class Team56ForPostProcessBO
 extends AbstractBusinessObject {
	
	/**
	 * Default SUID for Team56ForPostProcessBO.
	 */
	private static final long serialVersionUID = 1L;
	

						/**
	 * Entity property: id
	 *
	 */
private Long id;

					/**
	 * Entity property: name
	 *
	 */
private String name;



   /**
	* public default constructor for class Team56ForPostProcessBO
	*/
	public Team56ForPostProcessBO(){
		//NOP Team56ForPostProcessBO
	}


						/**
	 * Getter for id of class Team56ForPostProcessBO.
	 *
	 * @return {@link java.lang.Long}
	 * @generated
	 */ 
public  Long getId() {
	return this.id;
	}


	/**
	 * Setter for id of class Team56ForPostProcessBO.
	 * @param value {@link java.lang.Long}
	 * @generated
	 */

public  void setId(final Long id) {
	this.isEmpty = (id == null);
	this.id = id;
}

					/**
	 * Getter for name of class Team56ForPostProcessBO.
	 *
	 * @return {@link java.lang.String}
	 * @generated
	 */ 
public  String getName() {
	return this.name;
	}


	/**
	 * Setter for name of class Team56ForPostProcessBO.
	 * @param value {@link java.lang.String}
	 * @generated
	 */

public  void setName(final String name) {
	this.name = name;
}


}

