/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.service.standardfeatures.application.datagrid.uc37_collectionorder;


import javax.inject.Inject;
import javax.inject.Named;

import com.bluage.documentation.business.standardfeatures.application.datagrid.uc37_collectionorder.bos.Team37BO;
import com.bluage.documentation.service.AbstractService;
import com.bluage.documentation.service.standardfeatures.application.datagrid.uc37_collectionorder.daofinder.ServiceCollectionOrderDAOMethodFinderImpl;
import com.netfective.bluage.core.exception.ApplicationException;

/**
 * Defines  application services for ServiceCollectionOrder
  */
@Named("com.bluage.documentation.service.standardfeatures.application.datagrid.uc37_collectionorder.ServiceCollectionOrder")
public class ServiceCollectionOrder  extends AbstractService{


			@Inject
		@Named ("com.bluage.documentation.service.standardfeatures.application.datagrid.uc37_collectionorder.daofinder.ServiceCollectionOrderDAOMethodFinder")
		private ServiceCollectionOrderDAOMethodFinderImpl serviceCollectionOrderDao;
			


	/**
	 *  Default constructor 
	 */
	public ServiceCollectionOrder() {
		super(); // Call super for default constructor : ServiceCollectionOrder
	}
	



			
	/**    
	 * Process operation.
	 * @return {@link void}
	 */
	public void back( )
 throws ApplicationException {

			privateBack();
	}
	

	/**    
	 * Process operation.
	 * @return {@link void}
	 */
	private void privateBack( )
 throws ApplicationException {
// Central buffer node declaration for operation back
			
		}

		
			
	/**    
	 * Operation HQL.
	 * @param team {@link Team37BO}
		 * @return {@link Team37BO}
	 */
	public Team37BO loadTeamWithPlayers(Team37BO team )
 throws ApplicationException {
	
	return serviceCollectionOrderDao.loadTeamWithPlayers(team);
	}

		}
