/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.netfective.bluage.springmvc.ajax.util;


/**
 * Bluage Ajax Constants
 * @author NETFECTIVE TECHNOLOGY
 */
public interface Constants {	
	
	//iterable tag class name
	String TABLE_TAG_CLASS_NAME = "org.displaytag.tags.TableTag";
	String ITERATE_TAG_CLASS_NAME = "org.apache.struts.taglib.logic.IterateTag";
	
	//Ajax zone constants
	String START_OF_ZONE = "<!-- @[{0} -->";	
	String END_OF_ZONE = "<!-- {0}]@ -->";
	
    //Html div constants
    String DIV_HTML_ID_PREFIX = "a4sdiv.";    
    String START_OF_DIV = "<div  id=\"{0}\" {1} >";
    String END_OF_DIV = "</div>";
    
    //drag and drop script
    String CREATE_DRAGGABLE_SCRIPT = "new Draggable({0},{1});";
    String CREATE_DROPPABLE_SCRIPT = "Droppables.add({0},{1});";
    String REMOVE_DROPPABLE_SCRIPT = "Droppables.remove({0});";  
    String CREATE_SORTABLE_SCRIPT = "Sortable.create({0}, {1});";
    
    //script constants
    String BEGIN_OF_SCRIPT_TAG = "<script type=\"text/javascript\" >\n";
    String END_OF_SCRIPT_TAG= "\n</script>";
    
    //html attributes
    String HTML_STYLE = "style";
    String HTML_STYLE_CLASS = "class";

    String REFRESH_ZONES_KEY = "BluageAjax.refreshZones";

    // reRendered Constants
    String COMMA_SEPARATOR = ",";
    
    String AJAX_URL_PARAMETER = "a4srequest";
    String ZONES_URL_KEY = "aazones";    

    //constants used in the XML content returned to the client
    String XML_ZONES = "zones";
    String XML_ZONE = "zone";
    String XML_NAME = "name";
    String XML_ID= "id";
    String XML_SCRIPT = "script";
    String XML_IMAGE = "image";
    String XML_EXCEPTION = "exception";
    String XML_TYPE = "type";
    String XML_REDIRECT = "redirect";
	
}
