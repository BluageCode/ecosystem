/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.qauc.uc1001_tables ;

// Import declaration.
// Java imports.
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.bluage.documentation.business.qauc.uc1001_tables.bos.Team1001BO;
import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;

/**
 * Class : UC1001_ViewTeamController
 */
@Controller
@Scope("prototype")
@SessionAttributes("uC1001_ViewTeamForm")
@RequestMapping(value= "/presentation/qauc/uc1001_tables")
public class UC1001_ViewTeamController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC1001_ViewTeamController.class);
	
	//Current form name
	private static final String U_C1001__VIEW_TEAM_FORM = "uC1001_ViewTeamForm";

	//CONSTANT: teamDetails
	private static final String TEAM_DETAILS = "teamDetails";
		//CONSTANT: listplayers table or repeater.
	private static final String LISTPLAYERS = "listplayers";
				
	/**
	 * Property:customDateEditorsUC1001_ViewTeamController
	 */
	final private Map<String,CustomDateEditor> customDateEditorsUC1001_ViewTeamController = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:uC1001_ViewTeamValidator
	 */
	final private UC1001_ViewTeamValidator uC1001_ViewTeamValidator = new UC1001_ViewTeamValidator();
	
	
	// Initialise all the instances for UC1001_ViewTeamController
		// Initialize the instance teamDetails of type com.bluage.documentation.business.qauc.uc1001_tables.bos.Team1001BO with the value : null for UC1001_ViewTeamController
		private Team1001BO teamDetails;
			// Initialize the instance listplayers for the state lnk_view
		private List listplayers; // Initialize the instance listplayers for UC1001_ViewTeamController
				
	/**
	* This method initialise the form : UC1001_ViewTeamForm 
	* @return UC1001_ViewTeamForm
	*/
	@ModelAttribute("UC1001_ViewTeamFormInit")
	public void initUC1001_ViewTeamForm(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, U_C1001__VIEW_TEAM_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method UC1001_ViewTeamForm."); //for lnk_view 
		}
		UC1001_ViewTeamForm uC1001_ViewTeamForm;
	
		if(request.getSession().getAttribute(U_C1001__VIEW_TEAM_FORM) != null){
			uC1001_ViewTeamForm = (UC1001_ViewTeamForm)request.getSession().getAttribute(U_C1001__VIEW_TEAM_FORM);
		} else {
			uC1001_ViewTeamForm = new UC1001_ViewTeamForm();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form UC1001_ViewTeamForm.");
		}
		uC1001_ViewTeamForm = (UC1001_ViewTeamForm)populateDestinationForm(request.getSession(), uC1001_ViewTeamForm);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  UC1001_ViewTeamForm.");
		}
		model.addAttribute(U_C1001__VIEW_TEAM_FORM, uC1001_ViewTeamForm);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param UC1001_ViewTeamForm : The sceen form.
	 */
	@RequestMapping(value = "/UC1001_ViewTeam.html" ,method = RequestMethod.GET)
	public void prepareUC1001_ViewTeam(final HttpServletRequest request,final  Model model,final  @ModelAttribute("UC1001_ViewTeamFormInit") UC1001_ViewTeamForm uC1001_ViewTeamForm){
		
		UC1001_ViewTeamForm currentUC1001_ViewTeamForm = uC1001_ViewTeamForm;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(U_C1001__VIEW_TEAM_FORM) == null){
			if(currentUC1001_ViewTeamForm!=null){
				request.getSession().setAttribute(U_C1001__VIEW_TEAM_FORM, currentUC1001_ViewTeamForm);
			}else {
				currentUC1001_ViewTeamForm = new UC1001_ViewTeamForm();
				request.getSession().setAttribute(U_C1001__VIEW_TEAM_FORM, currentUC1001_ViewTeamForm);	
			}
		} else {
			currentUC1001_ViewTeamForm = (UC1001_ViewTeamForm) request.getSession().getAttribute(U_C1001__VIEW_TEAM_FORM);
		}

				currentUC1001_ViewTeamForm = (UC1001_ViewTeamForm)populateDestinationForm(request.getSession(), currentUC1001_ViewTeamForm);
		// Call all the Precontroller.
	currentUC1001_ViewTeamForm.setAlwaysCallPreControllers(true);
		model.addAttribute(U_C1001__VIEW_TEAM_FORM, currentUC1001_ViewTeamForm);
		request.getSession().setAttribute(U_C1001__VIEW_TEAM_FORM, currentUC1001_ViewTeamForm);
		
	}
	
			
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("UC1001_ViewTeamController [ ");
			strBToS.append(TEAM_DETAILS);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(teamDetails);
			strBToS.append(END_TO_STRING_DELIMITER);
				strBToS.append(LISTPLAYERS);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(listplayers);
			strBToS.append(END_TO_STRING_DELIMITER);
					// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(uC1001_ViewTeamValidator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsUC1001_ViewTeamController!=null); 
		return strBToS.toString();
	}
}
