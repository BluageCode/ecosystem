package uc63;

import org.junit.Test;
import test.common.AbstractBADocTestCase;

public class Donnees extends AbstractBADocTestCase {
	
	@Override
	public  String getName(){
		return "UC-63";
	}	

	@Test
	public void donnees() throws Exception {
		// On teste l'UC63 - Standard Features/Entites - Association
				
		selenium.open("");	
		selenium.click("link=Expand All");
		selenium.waitForPageToLoad("30000");
		selenium.click("link=UC63 - Association");
		
		verifyTrue(selenium.isElementPresent("//h2[contains(text(),\"Standard Features > Entities > UC63 > Association\")]"));
		// On v�rifie les donn�es.
		// On v�rifie les donn�es du tableau Teams.
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_teams')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Tigers\")] and td/span[contains(text(),\"Charlotte\")] and td/span[contains(text(),\"Harry\")] and td/span[contains(text(),\"Davis\")] and td/span[contains(text(),\"Alaska\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Bears\")] and td/span[contains(text(),\"Kansas City\")] and td/span[contains(text(),\"Loren\")] and td/span[contains(text(),\"Colon\")] and td/span[contains(text(),\"Alabama\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Chargers\")] and td/span[contains(text(),\"Detroit\")] and td/span[contains(text(),\"Bob\")] and td/span[contains(text(),\"Clark\")] and td/span[contains(text(),\"Arkansas\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Hawks\")] and td/span[contains(text(),\"Detroit\")] and td/span[contains(text(),\"Gerald\")] and td/span[contains(text(),\"Fox\")] and td/span[contains(text(),\"Arizona\")]]"));
		selenium.click("xpath=(//a)[2]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("3", selenium.getXpathCount("//table[contains(@id,'tab_teams')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Braves\")] and td/span[contains(text(),\"Omaha\")] and td/span[contains(text(),\"Louis\")] and td/span[contains(text(),\"Stokes\")] and td/span[contains(text(),\"California\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Eagles\")] and td/span[contains(text(),\"Raleigh\")] and td/span[contains(text(),\"Cameron\")] and td/span[contains(text(),\"Horton\")] and td/span[contains(text(),\"Colorado\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Cardinals\")] and td/span[contains(text(),\"Oakland\")] and td/span[contains(text(),\"Shane\")] and td/span[contains(text(),\"Sandoval\")] and td/span[contains(text(),\"Connecticut\")]]"));
		// On v�rifie les donn�es du tableau Coach.
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_coach')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coach')]/tbody/tr[td/span[contains(text(),\"Harry\")] and td/span[contains(text(),\"Davis\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coach')]/tbody/tr[td/span[contains(text(),\"Loren\")] and td/span[contains(text(),\"Colon\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coach')]/tbody/tr[td/span[contains(text(),\"Bob\")] and td/span[contains(text(),\"Clark\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coach')]/tbody/tr[td/span[contains(text(),\"Gerald\")] and td/span[contains(text(),\"Fox\")]]"));
		selenium.click("xpath=(//a)[5]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("3", selenium.getXpathCount("//table[contains(@id,'tab_coach')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coach')]/tbody/tr[td/span[contains(text(),\"Louis\")] and td/span[contains(text(),\"Stokes\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coach')]/tbody/tr[td/span[contains(text(),\"Cameron\")] and td/span[contains(text(),\"Horton\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coach')]/tbody/tr[td/span[contains(text(),\"Shane\")] and td/span[contains(text(),\"Sandoval\")]]"));
		// On teste les donn�es du tableau States.
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_states')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_states')]/tbody/tr[td/span[contains(text(),\"Alaska\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_states')]/tbody/tr[td/span[contains(text(),\"Alabama\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_states')]/tbody/tr[td/span[contains(text(),\"Arkansas\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_states')]/tbody/tr[td/span[contains(text(),\"Arizona\")]]"));
		selenium.click("xpath=(//a)[9]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_states')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_states')]/tbody/tr[td/span[contains(text(),\"California\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_states')]/tbody/tr[td/span[contains(text(),\"Colorado\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_states')]/tbody/tr[td/span[contains(text(),\"Connecticut\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_states')]/tbody/tr[td/span[contains(text(),\"Delaware\")]]"));
		selenium.click("xpath=(//a)[11]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_states')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_states')]/tbody/tr[td/span[contains(text(),\"Florida\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_states')]/tbody/tr[td/span[contains(text(),\"Georgia\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_states')]/tbody/tr[td/span[contains(text(),\"Hawaii\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_states')]/tbody/tr[td/span[contains(text(),\"Iowa\")]]"));
		// On v�rifie les donn�es du tableau Stadium.
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_stadium')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_stadium')]/tbody/tr[td/span[contains(text(),\"Heinz Field\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_stadium')]/tbody/tr[td/span[contains(text(),\"Jobing.com Arena\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_stadium')]/tbody/tr[td/span[contains(text(),\"Monster Park\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_stadium')]/tbody/tr[td/span[contains(text(),\"U.S. Cellular Field \")]]"));
		selenium.click("xpath=(//a)[13]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_stadium')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_stadium')]/tbody/tr[td/span[contains(text(),\"Dick s Sporting Goods Park\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_stadium')]/tbody/tr[td/span[contains(text(),\"Hut Park \")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_stadium')]/tbody/tr[td/span[contains(text(),\"Canad Inns Stadium\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_stadium')]/tbody/tr[td/span[contains(text(),\"Gay Meadow\")]]"));
		selenium.click("xpath=(//a)[15]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_stadium')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_stadium')]/tbody/tr[td/span[contains(text(),\"American Airlines Arena\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_stadium')]/tbody/tr[td/span[contains(text(),\"New Louisville Basketball Arena\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_stadium')]/tbody/tr[td/span[contains(text(),\"Sutherland\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_stadium')]/tbody/tr[td/span[contains(text(),\"Spalding Arena\")]]"));
	}


}
