/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.application.component.uc46_menu ;


import org.apache.log4j.Logger;
import org.springframework.validation.Errors;

import com.netfective.bluage.core.validator.AbstractValidator;
/**
* Class :UC46_MenuValidator
*/
public class UC46_MenuValidator extends AbstractValidator{
	
	// LOGGER for the class UC46_MenuValidator
	private static final Logger LOGGER = Logger.getLogger( UC46_MenuValidator.class);
	
	
	/**
	* Operation validate for UC46_MenuForm
	* @param obj : the current form (UC46_MenuForm)
	* @param errors : The spring errors to return for the form UC46_MenuForm
	*/
	public void validate(final Object obj,final Errors errors){
		LOGGER.info("Beginning method : validate"); // UC46_MenuValidator
		UC46_MenuForm cUC46_MenuForm = (UC46_MenuForm)obj; // UC46_MenuValidator
		LOGGER.info("Ending method : validate the form "+ cUC46_MenuForm.getClass().getName()); // UC46_MenuValidator
	}

	/**
	* Method to implements to use spring validators (UC46_MenuForm)
	* @param aClass : Class for the form UC46_MenuForm
	* @return boolean
	*/
	public boolean supports(final Class aClass){
		return (new UC46_MenuForm()).getClass().equals(aClass); // UC46_MenuValidator
	}
}
