<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file="/include.jsp"  %>
<html >
<head >
<title  > 
<fmt:message key="cUC1001_Tables_BLU_AGE_DOCUMENTATION"/>
</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"  > 
</meta>
<link href="<c:url value= "/presentation/_style/_style.css"/>" rel="stylesheet" type="text/css"  /> 
<link href="<c:url value= "/presentation/_include/jquery/rd_lib.css"/>" rel="stylesheet" type="text/css"  /> 
<script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/jquery.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/rd_setup.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/rd_lib.js"/>" space="preserve"  ></script><link rel="shortcut icon" href="<c:url value= "/presentation/_style/favicon.ico"/>"  /> 

</head>

<body class=" " > 
<div id="rd_ghost"  > 
<small  > 
<fmt:message key="cUC1001_Tables___CSS_DEACTIVATED"/>
</small>
</div>
<div id="rd_jghost"  > 
<small  > 
<fmt:message key="cUC1001_Tables___JAVASCRIPT_DEACTIVATED"/>
</small>
</div>
<div id="shape"  > 
<div class="ui-layout-center"  > 
<div class="east-north"  > 
	<form:form id="uC1001_Tables" commandName="uC1001_TablesForm"  enctype="application/x-www-form-urlencoded" method="post">
<h2 class="rd_first"  > 
<fmt:message key="cUC1001_Tables_QA_UC__UC1001__TABLES"/>
</h2>
<p  > 
<display:table uid="tab_teams"  sort="list" requestURI="" name="sessionScope.uC1001_TablesForm.allTeams" class="rd_tabledata"  >
	<c:set   var="rownum">
		<%=(tab_teams !=	null) ? new Integer(tab_teams_rowNum.intValue() - 1) : new Integer(0) %>
	</c:set>
	<c:set   var="idRow">
		<%=(tab_teams != null ) ? tab_teams_rowNum.intValue() - 1 : -1 %>
	</c:set>
	<c:set   var="idTab">
		<%=(tab_teams != null ) ? "tab_teams" : null %>
	</c:set>
	<c:set var="tableHashId">
		<%= new org.displaytag.util.ParamEncoder("tab_teams").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE) %>
	</c:set>
	<c:set var="tableHashId2">
		<%=request.getParameter(new org.displaytag.util.ParamEncoder("tab_teams").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE)) %>
	</c:set>	
	
<display:column format=""  titleKey="cUC1001_Tables_CITY">
		<span id="dyn_city">
	<c:out value="${tab_teams.city}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="cUC1001_Tables_NAME">
		<span id="dyn_name">
	<c:out value="${tab_teams.name}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="cUC1001_Tables_VIEW">
<a id="lnk_view[${rownum}]"onclick="javascript:btn_click_cExecuteUC1001_Tables(this.href);return false;"			href="<c:url value="/presentation/qauc/uc1001_tables/UC1001_Tables/lnk_view.html?id="/><c:out value="${idRow}" />&tab=<c:out value="tab_teams" />&<c:out value="${tableHashId}"/>=<c:out value="${tableHashId2}"/>"
	 shape="rect"  >									 
<fmt:message key="cUC1001_Tables_VIEW_74"/>
</a>
</display:column>
</display:table>
</p>
	</form:form>
	<script language="javascript">
		function btn_click_cExecuteUC1001_Tables(url) {
			UC1001_Tablesvar = document.getElementById('uC1001_Tables');
			UC1001_Tablesvar.setAttribute('action', url);
			UC1001_Tablesvar.submit();
		}
		function btn_click_cSetActionUC1001_Tables(url) {
			UC1001_Tablesvar = document.getElementById('uC1001_Tables');
			UC1001_Tablesvar.setAttribute('action', url);
		}
	</script>
</div>
<div class="east-center"  > 
<tiles:insertDefinition	name="presentation/qauc/uc1001_tables/UC1001_Tablescmp_tabs" />
</div>
</div>
<div class="ui-layout-north"  > 
<tiles:insertDefinition	name="presentation/qauc/uc1001_tables/UC1001_Tablescmp_header" />
</div>
<div class="ui-layout-south"  > 
<fmt:message key="cUC1001_Tables___"/>
<span id="rd_jversion"  > 
<fmt:message key="cUC1001_Tables_BLU_AGE_CORPORATION"/>
</span>
</div>
<div class="ui-layout-west"  > 
<tiles:insertDefinition	name="presentation/qauc/uc1001_tables/UC1001_Tablescmp_treeview" />
</div>
</div>
</body>
</html>
