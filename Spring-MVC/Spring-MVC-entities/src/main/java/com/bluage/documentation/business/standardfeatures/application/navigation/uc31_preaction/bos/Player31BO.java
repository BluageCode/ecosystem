/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.business.standardfeatures.application.navigation.uc31_preaction.bos;

import javax.inject.Inject;
import javax.inject.Named;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import com.bluage.documentation.business.common.bos.PlayerBO;
import com.bluage.documentation.business.standardfeatures.application.navigation.uc31_preaction.entities.daofinder.Player31DAOFinderImpl;

/**
 * Player31BO
 *
* @see com.netfective.bluage.core.business.IBusinessObject
* * @see PlayerBO
 */
@XmlRootElement

public class Player31BO
 extends PlayerBO {
	
	/**
	 * Default SUID for Player31BO.
	 */
	private static final long serialVersionUID = 1L;
	

	@Inject
	@Named ("com.bluage.documentation.business.standardfeatures.application.navigation.uc31_preaction.entities.daofinder.Player31Finder") 
	private Player31DAOFinderImpl finder;

												/**
	 * Entity property: position
	 *
	 */
private Position31BO position;



   /**
	* public default constructor for class Player31BO
	*/
	public Player31BO(){
		//NOP Player31BO
	}


												/**
	 * Getter for position of class Player31BO.
	 *
	 * @return {@link com.bluage.documentation.business.standardfeatures.application.navigation.uc31_preaction.bos.Position31BO}
	 * @generated
	 */ 
public  Position31BO getPosition() {
		Position31BO position0 = this.position;
		if(this.position == null) {
			if(DAO_MODE.equals(this.getObjMode( ) ) ) {
				position0 = null;
			} else {
				this.position = new Position31BO();
				this.position.setObjMode( this.getObjMode( ) );
				position0 = this.position;
			}
		} else {
			this.position.setObjMode( this.getObjMode( ) );
			if( HELPER_MODE.equals(this.getObjMode( ) ) ) {
				position0 = this.position;
			}
			if( DAO_MODE.equals(this.getObjMode()) && this.position.isEmpty ) {
				position0 = null;
			}
		}
		return position0;
	}


	/**
	 * Setter for position of class Player31BO.
	 * @param value {@link com.bluage.documentation.business.standardfeatures.application.navigation.uc31_preaction.bos.Position31BO}
	 * @generated
	 */

public  void setPosition(final Position31BO position) {
	if(position != null) {
		position.isEmpty = false;
		position.setObjMode( this.getObjMode( ) );
	}
	this.isEmpty = false;
	this.position = position;
}


/**
	 * Returns <code>true</code> if the argument is an Player31 instance and all identifiers for this entity
     * equal the identifiers of the argument entity. The <code>equals</code> method of the parent entity
     * will also need to return <code>true</code>. Returns <code>false</code> otherwise.
     *
     * @see PlayerBO#hashCode()
	 * @param object  object to compare with current instance      
     * @return boolean true if equals false if not
     */
    public boolean equals(final Object object )
    {
		if ( this == object )
        {
            return true;
        }
        if ( !( object instanceof Player31BO ) )
        {
            return false;
        }
        final Player31BO that = (Player31BO)object;
    
    	            if (        				        			this.getId() == null
        	        ) {
        	return false;
        }
		return new EqualsBuilder()
			.append(this.getId(), that.getId())
			 	.isEquals();
	}
	/**

	 * Returns a hash code based on this entity's identifiers and the hash code of the parent entity.
     *
     * @see PlayerBO#hashCode()
     * @return int
     */    
    public int hashCode() {
        return new HashCodeBuilder()
             .append(getId())
				 	.appendSuper(super.hashCode())
            .toHashCode();
    }
}

