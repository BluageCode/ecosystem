<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file="/include.jsp"  %>
<html >
<head >
<title  > 
<fmt:message key="cUC57_Search_BLU_AGE_DOCUMENTATION"/>
</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"  > 
</meta>
<link href="<c:url value= "/presentation/_style/_style.css"/>" rel="stylesheet" type="text/css"  /> 
<link href="<c:url value= "/presentation/_include/jquery/rd_lib.css"/>" rel="stylesheet" type="text/css"  /> 
<script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/jquery.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/rd_setup.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/rd_lib.js"/>" space="preserve"  ></script><link rel="shortcut icon" href="<c:url value= "/presentation/_style/favicon.ico"/>"  /> 

</head>

<body class=" " > 
<div id="rd_ghost"  > 
<small  > 
<fmt:message key="cUC57_Search___CSS_DEACTIVATED"/>
</small>
</div>
<div id="rd_jghost"  > 
<small  > 
<fmt:message key="cUC57_Search___JAVASCRIPT_DEACTIVATED"/>
</small>
</div>
<div id="shape"  > 
<div class="ui-layout-center"  > 
<div class="east-north"  > 
	<form:form id="uC57_Search" commandName="uC57_SearchForm"  enctype="application/x-www-form-urlencoded" method="post">
<h2 class="rd_first"  > 
<fmt:message key="cUC57_Search_STANDARD_FEATURES__SERVICES_"/>
</h2>
<table class="rd_tableform"  >
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC57_Search_NAME_"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
<form:input id="txt_name" path="hqlCriteria.name"  
		/>
</td> 
<td rowspan="1" colspan="1"  >
<fmt:message key="cUC57_Search_"/>
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC57_Search_FIRSTRESULT_START_AT_0_"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
<form:input id="txt_firstresult" path="hqlAttribute.firstresult"  
		/>
</td> 
<td rowspan="1" colspan="1"  >
<fmt:message key="cUC57_Search__209"/>
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC57_Search_MAXRESULT_"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
<form:input id="txt_maxresult" path="hqlAttribute.maxresult"  
		/>
</td> 
<td rowspan="1" colspan="1"  >
<form:errors path="hqlAttribute.maxresult"/>
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<fmt:message key="cUC57_Search__210"/>
</td> 
<td rowspan="1" colspan="1"  >
<a id="lnk_searchTeams"onclick="javascript:btn_click_cExecuteUC57_Search(this.href);return false;"				href="<c:url value="/presentation/standardfeatures/services/hql/uc57_hqloperation/UC57_Search/lnk_searchTeams.html"/>"
							 shape="rect"  >									 
<fmt:message key="cUC57_Search_SEARCH_A_TEAM_BY_CRITERIA"/>
</a>
</td> 
<td rowspan="1" colspan="1"  >
<fmt:message key="cUC57_Search__211"/>
</td> 
</tr>
</table> 
<display:table uid="tab_searchResult"  sort="list" requestURI="" name="sessionScope.uC57_SearchForm.searchResult" class="rd_tabledata"  >
	<c:set   var="rownum">
		<%=(tab_searchResult !=	null) ? new Integer(tab_searchResult_rowNum.intValue() - 1) : new Integer(0) %>
	</c:set>
	<c:set   var="idRow">
		<%=(tab_searchResult != null ) ? tab_searchResult_rowNum.intValue() - 1 : -1 %>
	</c:set>
	<c:set   var="idTab">
		<%=(tab_searchResult != null ) ? "tab_searchResult" : null %>
	</c:set>
	<c:set var="tableHashId">
		<%= new org.displaytag.util.ParamEncoder("tab_searchResult").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE) %>
	</c:set>
	<c:set var="tableHashId2">
		<%=request.getParameter(new org.displaytag.util.ParamEncoder("tab_searchResult").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE)) %>
	</c:set>	
	
<display:column format=""  titleKey="cUC57_Search_ID">
		<span id="txt_id">
	<c:out value="${tab_searchResult.id}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="cUC57_Search_NAME">
		<span id="txt_name">
	<c:out value="${tab_searchResult.name}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="cUC57_Search_CITY">
		<span id="txt_city">
	<c:out value="${tab_searchResult.city}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="cUC57_Search_STATE">
		<span id="txt_state">
	<c:out value="${tab_searchResult.state.name}" />
		</span>
	</display:column>
	
<display:column format=""  >
<a id="lnk_loadPlayers[${rownum}]"onclick="javascript:btn_click_cExecuteUC57_Search(this.href);return false;"			href="<c:url value="/presentation/standardfeatures/services/hql/uc57_hqloperation/UC57_Search/lnk_loadPlayers.html?id="/><c:out value="${idRow}" />&tab=<c:out value="tab_searchResult" />&<c:out value="${tableHashId}"/>=<c:out value="${tableHashId2}"/>"
	 shape="rect"  >									 
<fmt:message key="cUC57_Search__SEARCH_PLAYERS_"/>
</a>
</display:column>
</display:table>
	</form:form>
	<script language="javascript">
		function btn_click_cExecuteUC57_Search(url) {
			UC57_Searchvar = document.getElementById('uC57_Search');
			UC57_Searchvar.setAttribute('action', url);
			UC57_Searchvar.submit();
		}
		function btn_click_cSetActionUC57_Search(url) {
			UC57_Searchvar = document.getElementById('uC57_Search');
			UC57_Searchvar.setAttribute('action', url);
		}
	</script>
</div>
<div class="east-center"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/services/hql/uc57_hqloperation/UC57_Searchcmp_tabs" />
</div>
</div>
<div class="ui-layout-north"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/services/hql/uc57_hqloperation/UC57_Searchcmp_header" />
</div>
<div class="ui-layout-south"  > 
<fmt:message key="cUC57_Search___"/>
<span id="rd_jversion"  > 
<fmt:message key="cUC57_Search_BLU_AGE_DOCUMENTATION_212"/>
</span>
</div>
<div class="ui-layout-west"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/services/hql/uc57_hqloperation/UC57_Searchcmp_treeview" />
</div>
</div>
</body>
</html>
