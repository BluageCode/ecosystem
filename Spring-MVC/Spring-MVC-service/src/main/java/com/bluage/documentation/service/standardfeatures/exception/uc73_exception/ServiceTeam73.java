/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.service.standardfeatures.exception.uc73_exception;


import javax.inject.Inject;
import javax.inject.Named;

import com.bluage.documentation.business.standardfeatures.exception.uc73_exception.bos.Team73BO;
import com.bluage.documentation.business.standardfeatures.exception.uc73_exception.entities.daofinder.Team73DAOFinderImpl;
import com.bluage.documentation.service.AbstractService;
import com.netfective.bluage.core.exception.ApplicationException;

/**
 * Defines  application services for ServiceTeam73
  */
@Named("com.bluage.documentation.service.standardfeatures.exception.uc73_exception.ServiceTeam73")
public class ServiceTeam73  extends AbstractService{


			@Inject
		@Named ("com.bluage.documentation.business.standardfeatures.exception.uc73_exception.entities.daofinder.Team73DAOFinderImpl")
		private Team73DAOFinderImpl team73BOFinder;
	


	/**
	 *  Default constructor 
	 */
	public ServiceTeam73() {
		super(); // Call super for default constructor : ServiceTeam73
	}
	


			
				
	/**    
	 * Operation Crud
	 * @param bo {@link Team73BO}
		 * @return {@link Team73BO}
	 */
	public Team73BO Team73FindByID(Team73BO bo )
 throws ApplicationException {

					return (Team73BO)team73BOFinder.findByPrimaryKey(getBoId(bo));
	}
	/**
	 *  Return the primary key from business object.
	 */
	private Object getBoId(Team73BO bo) {
				return bo.getId();
		}

		}
