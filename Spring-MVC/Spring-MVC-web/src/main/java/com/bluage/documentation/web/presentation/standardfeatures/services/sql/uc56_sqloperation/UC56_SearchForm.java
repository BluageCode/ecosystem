/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.services.sql.uc56_sqloperation;


import java.io.Serializable;
import java.util.List;

import com.bluage.documentation.business.standardfeatures.services.sql.uc56_sqloperation.bos.Team56BO;
import com.bluage.documentation.business.standardfeatures.services.sql.uc56_sqloperation.bos.Team56ForPostProcessBO;
import com.netfective.bluage.core.web.AbstractForm;
import com.netfective.bluage.core.web.IForm;

/**
* Class : UC56_SearchForm
*/
public class UC56_SearchForm extends AbstractForm implements Serializable, IForm {
	
	/** Constant : serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	//CONSTANT : searchResult
	private static final String SEARCH_RESULT = "searchResult";
	//CONSTANT : searchResultPostProcess
	private static final String SEARCH_RESULT_POST_PROCESS = "searchResultPostProcess";
	//CONSTANT : selectedTeam
	private static final String SELECTED_TEAM = "selectedTeam";
	//CONSTANT : team
	private static final String TEAM = "team";
	//CONSTANT : sqlCriteria
	private static final String SQL_CRITERIA = "sqlCriteria";
	/**
	 * 	Property: searchResult 
	 */
	private List<Team56BO> searchResult;
	/**
	 * 	Property: searchResultPostProcess 
	 */
	private List<Team56ForPostProcessBO> searchResultPostProcess;
	/**
	 * 	Property: selectedTeam 
	 */
	private Team56BO selectedTeam;
	/**
	 * 	Property: team 
	 */
	private Team56BO team;
	/**
	 * 	Property: sqlCriteria 
	 */
	private Team56BO sqlCriteria;
/**
	 * Default constructor : UC56_SearchForm
	 */
	public UC56_SearchForm() {
		super();
		// Initialize : searchResult
		this.searchResult = new java.util.ArrayList<com.bluage.documentation.business.standardfeatures.services.sql.uc56_sqloperation.bos.Team56BO>();
		// Initialize : searchResultPostProcess
		this.searchResultPostProcess = new java.util.ArrayList<com.bluage.documentation.business.standardfeatures.services.sql.uc56_sqloperation.bos.Team56ForPostProcessBO>();
		// Initialize : selectedTeam
		this.selectedTeam = null;
		// Initialize : team
		this.team = null;
		// Initialize : sqlCriteria
		this.sqlCriteria = new Team56BO();
							this.setAlwaysCallPreControllers(true);
	}
	
	/**
	 * 	Getter : searchResult 
	 * 	@return : Return the searchResult instance.
	 */
	public List<Team56BO> getSearchResult(){
		return searchResult; // For UC56_SearchForm
	}
	
	/**
	 * 	Setter : searchResult 
	 *  @param searchResultinstance : The instance to set.
	 */
	public void setSearchResult(final List<Team56BO> searchResultinstance){
		this.searchResult = searchResultinstance;// For UC56_SearchForm
	}
	/**
	 * 	Getter : searchResultPostProcess 
	 * 	@return : Return the searchResultPostProcess instance.
	 */
	public List<Team56ForPostProcessBO> getSearchResultPostProcess(){
		return searchResultPostProcess; // For UC56_SearchForm
	}
	
	/**
	 * 	Setter : searchResultPostProcess 
	 *  @param searchResultPostProcessinstance : The instance to set.
	 */
	public void setSearchResultPostProcess(final List<Team56ForPostProcessBO> searchResultPostProcessinstance){
		this.searchResultPostProcess = searchResultPostProcessinstance;// For UC56_SearchForm
	}
	/**
	 * 	Getter : selectedTeam 
	 * 	@return : Return the selectedTeam instance.
	 */
	public Team56BO getSelectedTeam(){
		return selectedTeam; // For UC56_SearchForm
	}
	
	/**
	 * 	Setter : selectedTeam 
	 *  @param selectedTeaminstance : The instance to set.
	 */
	public void setSelectedTeam(final Team56BO selectedTeaminstance){
		this.selectedTeam = selectedTeaminstance;// For UC56_SearchForm
	}
	/**
	 * 	Getter : team 
	 * 	@return : Return the team instance.
	 */
	public Team56BO getTeam(){
		return team; // For UC56_SearchForm
	}
	
	/**
	 * 	Setter : team 
	 *  @param teaminstance : The instance to set.
	 */
	public void setTeam(final Team56BO teaminstance){
		this.team = teaminstance;// For UC56_SearchForm
	}
	/**
	 * 	Getter : sqlCriteria 
	 * 	@return : Return the sqlCriteria instance.
	 */
	public Team56BO getSqlCriteria(){
		return sqlCriteria; // For UC56_SearchForm
	}
	
	/**
	 * 	Setter : sqlCriteria 
	 *  @param sqlCriteriainstance : The instance to set.
	 */
	public void setSqlCriteria(final Team56BO sqlCriteriainstance){
		this.sqlCriteria = sqlCriteriainstance;// For UC56_SearchForm
	}
/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UC56_SearchForm [ "+
SEARCH_RESULT +" = " + searchResult +SEARCH_RESULT_POST_PROCESS +" = " + searchResultPostProcess +SELECTED_TEAM +" = " + selectedTeam +TEAM +" = " + team +SQL_CRITERIA +" = " + sqlCriteria + "]";
	}
	
	/**
	 * Method setInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public void setInstanceByInstanceName(final String instanceName, final Object instance) {
		// Set the instance SearchResult.
		if(SEARCH_RESULT.equals(instanceName)){// For UC56_SearchForm
			this.setSearchResult((List<Team56BO>)instance); // For UC56_SearchForm
		}
				// Set the instance SearchResultPostProcess.
		if(SEARCH_RESULT_POST_PROCESS.equals(instanceName)){// For UC56_SearchForm
			this.setSearchResultPostProcess((List<Team56ForPostProcessBO>)instance); // For UC56_SearchForm
		}
				// Set the instance SelectedTeam.
		if(SELECTED_TEAM.equals(instanceName)){// For UC56_SearchForm
			this.setSelectedTeam((Team56BO)instance); // For UC56_SearchForm
		}
				// Set the instance Team.
		if(TEAM.equals(instanceName)){// For UC56_SearchForm
			this.setTeam((Team56BO)instance); // For UC56_SearchForm
		}
				// Set the instance SqlCriteria.
		if(SQL_CRITERIA.equals(instanceName)){// For UC56_SearchForm
			this.setSqlCriteria((Team56BO)instance); // For UC56_SearchForm
		}
			}
	
	/**
	 * Method getInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public Object getInstanceByInstanceName(final String instanceName){
		// Temporary object for tmpUC56_SearchForm.
		Object tmpUC56_SearchForm = null;
		
			
		// Get the instance SearchResult for UC56_SearchForm.
		if(SEARCH_RESULT.equals(instanceName)){ // For UC56_SearchForm
			tmpUC56_SearchForm = this.getSearchResult(); // For UC56_SearchForm
		}
			
		// Get the instance SearchResultPostProcess for UC56_SearchForm.
		if(SEARCH_RESULT_POST_PROCESS.equals(instanceName)){ // For UC56_SearchForm
			tmpUC56_SearchForm = this.getSearchResultPostProcess(); // For UC56_SearchForm
		}
			
		// Get the instance SelectedTeam for UC56_SearchForm.
		if(SELECTED_TEAM.equals(instanceName)){ // For UC56_SearchForm
			tmpUC56_SearchForm = this.getSelectedTeam(); // For UC56_SearchForm
		}
			
		// Get the instance Team for UC56_SearchForm.
		if(TEAM.equals(instanceName)){ // For UC56_SearchForm
			tmpUC56_SearchForm = this.getTeam(); // For UC56_SearchForm
		}
			
		// Get the instance SqlCriteria for UC56_SearchForm.
		if(SQL_CRITERIA.equals(instanceName)){ // For UC56_SearchForm
			tmpUC56_SearchForm = this.getSqlCriteria(); // For UC56_SearchForm
		}
		return tmpUC56_SearchForm;// For UC56_SearchForm
	}
	
					}
