/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.business.standardfeatures.application.component.uc47_contents.entities.daofinder;


import javax.inject.Named;

import com.bluage.documentation.business.AbstractDaoFinderImpl;
import com.bluage.documentation.business.standardfeatures.application.component.uc47_contents.bos.PlayerTeam47BO;

/**
 * This class represents the implementation of 
 * the search methods for the entity {@link PlayerTeam47}.
 *
 * @see		AbstractDaoFinderImpl
 */
 
@Named ("com.bluage.documentation.business.standardfeatures.application.component.uc47_contents.entities.daofinder.PlayerTeam47DAOFinderImpl") 
public class PlayerTeam47DAOFinderImpl extends AbstractDaoFinderImpl<PlayerTeam47BO>{
	
   /**
	* Default order
	*/
	private static final String DEFAULT_ORDER ="";

	/**
	* The associated BO 
	*/
	private final Class<PlayerTeam47BO> refBo = com.bluage.documentation.business.standardfeatures.application.component.uc47_contents.bos.PlayerTeam47BO.class;
	

	/**
	* Returns the associated BO
	* @return Class<PlayerTeam47BO>
	*/
	@Override
	public final Class<PlayerTeam47BO> getRefBo(){
		return this.refBo;
	}
	
	/**
	* Returns the default order by instruction
	* @return String
	*/
	@Override
	public final String getDefaultOrder(){
		return this.DEFAULT_ORDER;
	}
}
