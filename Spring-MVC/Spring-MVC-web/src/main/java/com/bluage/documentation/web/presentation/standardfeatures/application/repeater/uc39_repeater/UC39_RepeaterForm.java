/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.application.repeater.uc39_repeater;


import java.io.Serializable;
import java.util.List;

import com.bluage.documentation.business.standardfeatures.application.repeater.uc39_repeater.bos.Team39BO;
import com.netfective.bluage.core.web.AbstractForm;
import com.netfective.bluage.core.web.IForm;

/**
* Class : UC39_RepeaterForm
*/
public class UC39_RepeaterForm extends AbstractForm implements Serializable, IForm {
	
	/** Constant : serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	//CONSTANT : teams1
	private static final String TEAMS1 = "teams1";
	//CONSTANT : teams3
	private static final String TEAMS3 = "teams3";
	//CONSTANT : teams2
	private static final String TEAMS2 = "teams2";
	/**
	 * 	Property: teams1 
	 */
	private List<Team39BO> teams1;
	/**
	 * 	Property: teams3 
	 */
	private List<Team39BO> teams3;
	/**
	 * 	Property: teams2 
	 */
	private List<Team39BO> teams2;
/**
	 * Default constructor : UC39_RepeaterForm
	 */
	public UC39_RepeaterForm() {
		super();
		// Initialize : teams1
		this.teams1 = new java.util.ArrayList<com.bluage.documentation.business.standardfeatures.application.repeater.uc39_repeater.bos.Team39BO>();
		// Initialize : teams3
		this.teams3 = new java.util.ArrayList<com.bluage.documentation.business.standardfeatures.application.repeater.uc39_repeater.bos.Team39BO>();
		// Initialize : teams2
		this.teams2 = new java.util.ArrayList<com.bluage.documentation.business.standardfeatures.application.repeater.uc39_repeater.bos.Team39BO>();
			this.setAlwaysCallPreControllers(true);
	}
	
	/**
	 * 	Getter : teams1 
	 * 	@return : Return the teams1 instance.
	 */
	public List<Team39BO> getTeams1(){
		return teams1; // For UC39_RepeaterForm
	}
	
	/**
	 * 	Setter : teams1 
	 *  @param teams1instance : The instance to set.
	 */
	public void setTeams1(final List<Team39BO> teams1instance){
		this.teams1 = teams1instance;// For UC39_RepeaterForm
	}
	/**
	 * 	Getter : teams3 
	 * 	@return : Return the teams3 instance.
	 */
	public List<Team39BO> getTeams3(){
		return teams3; // For UC39_RepeaterForm
	}
	
	/**
	 * 	Setter : teams3 
	 *  @param teams3instance : The instance to set.
	 */
	public void setTeams3(final List<Team39BO> teams3instance){
		this.teams3 = teams3instance;// For UC39_RepeaterForm
	}
	/**
	 * 	Getter : teams2 
	 * 	@return : Return the teams2 instance.
	 */
	public List<Team39BO> getTeams2(){
		return teams2; // For UC39_RepeaterForm
	}
	
	/**
	 * 	Setter : teams2 
	 *  @param teams2instance : The instance to set.
	 */
	public void setTeams2(final List<Team39BO> teams2instance){
		this.teams2 = teams2instance;// For UC39_RepeaterForm
	}
/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UC39_RepeaterForm [ "+
TEAMS1 +" = " + teams1 +TEAMS3 +" = " + teams3 +TEAMS2 +" = " + teams2 + "]";
	}
	
	/**
	 * Method setInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public void setInstanceByInstanceName(final String instanceName, final Object instance) {
		// Set the instance Teams1.
		if(TEAMS1.equals(instanceName)){// For UC39_RepeaterForm
			this.setTeams1((List<Team39BO>)instance); // For UC39_RepeaterForm
		}
				// Set the instance Teams3.
		if(TEAMS3.equals(instanceName)){// For UC39_RepeaterForm
			this.setTeams3((List<Team39BO>)instance); // For UC39_RepeaterForm
		}
				// Set the instance Teams2.
		if(TEAMS2.equals(instanceName)){// For UC39_RepeaterForm
			this.setTeams2((List<Team39BO>)instance); // For UC39_RepeaterForm
		}
			}
	
	/**
	 * Method getInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public Object getInstanceByInstanceName(final String instanceName){
		// Temporary object for tmpUC39_RepeaterForm.
		Object tmpUC39_RepeaterForm = null;
		
			
		// Get the instance Teams1 for UC39_RepeaterForm.
		if(TEAMS1.equals(instanceName)){ // For UC39_RepeaterForm
			tmpUC39_RepeaterForm = this.getTeams1(); // For UC39_RepeaterForm
		}
			
		// Get the instance Teams3 for UC39_RepeaterForm.
		if(TEAMS3.equals(instanceName)){ // For UC39_RepeaterForm
			tmpUC39_RepeaterForm = this.getTeams3(); // For UC39_RepeaterForm
		}
			
		// Get the instance Teams2 for UC39_RepeaterForm.
		if(TEAMS2.equals(instanceName)){ // For UC39_RepeaterForm
			tmpUC39_RepeaterForm = this.getTeams2(); // For UC39_RepeaterForm
		}
		return tmpUC39_RepeaterForm;// For UC39_RepeaterForm
	}
	
	}
