package uc220;

import org.junit.Test;
import test.common.AbstractBADocTestCase;

public class ITuc220 extends AbstractBADocTestCase {
	
	@Override
	public  String getName(){
		return "UC-220";
	}
	
	@Test
	public void testUC210() throws Exception {
		// On teste l'UC210 - External Tools - Sonar
				
		selenium.open("");	
		selenium.click("link=Expand All");
		selenium.waitForPageToLoad("300000");
		selenium.click("link=UC220 - Time Out");
		
		verifyTrue(selenium.isElementPresent("//h2[contains(text(),\"Advanced Features > Filters > UC220 > Time Out\")]"));
		assertTrue(selenium.isElementPresent("//div/p[contains(text(),\"The implementation of a specific HTTP user filter is possible in Blu Age as of version 3.6.4. This function allows to add a custom user filter.\")]"));
		assertTrue(selenium.isElementPresent("//div/p[contains(text(),'The example of this use case is a time out filter.')]"));
	}

	
}
