/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.application.navigation.uc29_globalscope.package_one ;

// Import declaration.
// Java imports.
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.bluage.documentation.service.standardfeatures.application.navigation.uc29_globalscope.ServicePlayer29;
import com.bluage.documentation.service.standardfeatures.application.navigation.uc29_globalscope.ServiceTeam29;
import com.netfective.bluage.core.exception.ApplicationException;
import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;
import com.netfective.bluage.core.web.IForm;

/**
 * Class : UC29_Onglets_1Controller
 */
@Controller
@Scope("prototype")
@SessionAttributes("uC29_Onglets_1Form")
@RequestMapping(value= "/presentation/standardfeatures/application/navigation/uc29_globalscope/package_one")
public class UC29_Onglets_1Controller extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC29_Onglets_1Controller.class);
	
	//Current form name
	private static final String U_C29__ONGLETS_1_FORM = "uC29_Onglets_1Form";

	//CONSTANT: allPlayers
	private static final String ALL_PLAYERS = "allPlayers";
	//CONSTANT: allTeams
	private static final String ALL_TEAMS = "allTeams";
	
	/**
	 * Property:customDateEditorsUC29_Onglets_1Controller
	 */
	final private Map<String,CustomDateEditor> customDateEditorsUC29_Onglets_1Controller = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:uC29_Onglets_1Validator
	 */
	final private UC29_Onglets_1Validator uC29_Onglets_1Validator = new UC29_Onglets_1Validator();
	
	/**
	 * Service declaration : servicePlayer29.
	 */
	@Autowired
	private ServicePlayer29 servicePlayer29;
	
	/**
	 * Service declaration : serviceTeam29.
	 */
	@Autowired
	private ServiceTeam29 serviceTeam29;
	
	/**
	 * Pre Controller declaration : uC29_PreControllerFindAllTeams
	 */
	@Autowired
	private UC29_PreControllerFindAllTeamsController uC29_PreControllerFindAllTeams;

	
	// Initialise all the instances for UC29_Onglets_1Controller
		// Declare the instance allPlayers
		private List allPlayers;
		// Declare the instance allTeams
		private List allTeams;
	/**
	 * Operation : lnk_teams_global
 	 * @param model : 
 	 * @param uC29_Onglets_1 : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC29_Onglets_1/lnk_teams_global.html",method = RequestMethod.POST)
	public String lnk_teams_global(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC29_Onglets_1Form") UC29_Onglets_1Form  uC29_Onglets_1Form, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : lnk_teams_global"); 
		infoLogger(uC29_Onglets_1Form); 
		uC29_Onglets_1Form.setAlwaysCallPreControllers(false); 
		// initialization for lnk_teams_global
		init();
		
		String destinationPath = null; 
		

	 callFindallteams(request,uC29_Onglets_1Form );
											// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.application.navigation.uc29_globalscope.package_one.UC29_TeamsForm or not from lnk_teams_global
			final UC29_TeamsForm uC29_TeamsForm =  new UC29_TeamsForm();
			// Populate the destination form with all the instances returned from lnk_teams_global.
			final IForm uC29_TeamsForm2 = populateDestinationForm(request.getSession(), uC29_TeamsForm); 
					
			// Add the form to the model.
			model.addAttribute("uC29_TeamsForm", uC29_TeamsForm2); 
			
			request.getSession().setAttribute("uC29_TeamsForm", uC29_TeamsForm2);
			
			// "OK" CASE => destination screen path from lnk_teams_global
			LOGGER.info("Go to the screen 'UC29_Teams'.");
			// Redirect (PRG) from lnk_teams_global
			destinationPath =  "redirect:/presentation/standardfeatures/application/navigation/uc29_globalscope/package_one/UC29_Teams.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_teams_global 
	}
	
		/**
	 * Operation : lnk_players_global
 	 * @param model : 
 	 * @param uC29_Onglets_1 : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC29_Onglets_1/lnk_players_global.html",method = RequestMethod.POST)
	public String lnk_players_global(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC29_Onglets_1Form") UC29_Onglets_1Form  uC29_Onglets_1Form, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : lnk_players_global"); 
		infoLogger(uC29_Onglets_1Form); 
		uC29_Onglets_1Form.setAlwaysCallPreControllers(false); 
		// initialization for lnk_players_global
		init();
		
		String destinationPath = null; 
		

	 callFindallplayers(request,uC29_Onglets_1Form );
											// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.application.navigation.uc29_globalscope.package_one.UC29_PlayersForm or not from lnk_players_global
			final UC29_PlayersForm uC29_PlayersForm =  new UC29_PlayersForm();
			// Populate the destination form with all the instances returned from lnk_players_global.
			final IForm uC29_PlayersForm2 = populateDestinationForm(request.getSession(), uC29_PlayersForm); 
					
			// Add the form to the model.
			model.addAttribute("uC29_PlayersForm", uC29_PlayersForm2); 
			
			request.getSession().setAttribute("uC29_PlayersForm", uC29_PlayersForm2);
			
			// "OK" CASE => destination screen path from lnk_players_global
			LOGGER.info("Go to the screen 'UC29_Players'.");
			// Redirect (PRG) from lnk_players_global
			destinationPath =  "redirect:/presentation/standardfeatures/application/navigation/uc29_globalscope/package_one/UC29_Players.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_players_global 
	}
	
	
	/**
	* This method initialise the form : UC29_Onglets_1Form 
	* @return UC29_Onglets_1Form
	*/
	@ModelAttribute("UC29_Onglets_1FormInit")
	public void initUC29_Onglets_1Form(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, U_C29__ONGLETS_1_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method UC29_Onglets_1Form."); //for lnk_players_global 
		}
		UC29_Onglets_1Form uC29_Onglets_1Form;
	
		if(request.getSession().getAttribute(U_C29__ONGLETS_1_FORM) != null){
			uC29_Onglets_1Form = (UC29_Onglets_1Form)request.getSession().getAttribute(U_C29__ONGLETS_1_FORM);
		} else {
			uC29_Onglets_1Form = new UC29_Onglets_1Form();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form UC29_Onglets_1Form.");
		}
		uC29_Onglets_1Form = (UC29_Onglets_1Form)populateDestinationForm(request.getSession(), uC29_Onglets_1Form);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  UC29_Onglets_1Form.");
		}
		model.addAttribute(U_C29__ONGLETS_1_FORM, uC29_Onglets_1Form);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param UC29_Onglets_1Form : The sceen form.
	 */
	@RequestMapping(value = "/UC29_Onglets_1.html" ,method = RequestMethod.GET)
	public void prepareUC29_Onglets_1(final HttpServletRequest request,final  Model model,final  @ModelAttribute("UC29_Onglets_1FormInit") UC29_Onglets_1Form uC29_Onglets_1Form){
		
		UC29_Onglets_1Form currentUC29_Onglets_1Form = uC29_Onglets_1Form;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(U_C29__ONGLETS_1_FORM) == null){
			if(currentUC29_Onglets_1Form!=null){
				request.getSession().setAttribute(U_C29__ONGLETS_1_FORM, currentUC29_Onglets_1Form);
			}else {
				currentUC29_Onglets_1Form = new UC29_Onglets_1Form();
				request.getSession().setAttribute(U_C29__ONGLETS_1_FORM, currentUC29_Onglets_1Form);	
			}
		} else {
			currentUC29_Onglets_1Form = (UC29_Onglets_1Form) request.getSession().getAttribute(U_C29__ONGLETS_1_FORM);
		}

					currentUC29_Onglets_1Form = (UC29_Onglets_1Form)populateDestinationForm(request.getSession(), currentUC29_Onglets_1Form);
		// Call all the Precontroller.
	if ( currentUC29_Onglets_1Form.isAlwaysCallPreControllers()) {
	
		// Calling the PreController : UC29_PreControllerFindAllTeams.
		currentUC29_Onglets_1Form = (UC29_Onglets_1Form) uC29_PreControllerFindAllTeams.uC29_PreControllerFindAllTeamsControllerInit(request, model, currentUC29_Onglets_1Form);
		
	}
	currentUC29_Onglets_1Form.setAlwaysCallPreControllers(true);
		model.addAttribute(U_C29__ONGLETS_1_FORM, currentUC29_Onglets_1Form);
		request.getSession().setAttribute(U_C29__ONGLETS_1_FORM, currentUC29_Onglets_1Form);
		
	}
	
				/**
	 * method callFindallplayers
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callFindallplayers(HttpServletRequest request,IForm form) {
		allPlayers = (List)get(request.getSession(),form, "allPlayers");  
										 
					try {
				// executing Findallplayers in lnk_players_global
				allPlayers = 	servicePlayer29.player29FindAll(
	);  
 
				put(request.getSession(), ALL_PLAYERS,allPlayers);
								// processing variables Findallplayers in lnk_players_global

			} catch (ApplicationException e) { 
				// error handling for operation player29FindAll called Findallplayers
				errorLogger("An error occured during the execution of the operation : player29FindAll",e); 
		
			}
	}
		/**
	 * method callFindallteams
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callFindallteams(HttpServletRequest request,IForm form) {
		allTeams = (List)get(request.getSession(),form, "allTeams");  
										 
					try {
				// executing Findallteams in lnk_players_global
				allTeams = 	serviceTeam29.team29FindAll(
	);  
 
				put(request.getSession(), ALL_TEAMS,allTeams);
								// processing variables Findallteams in lnk_players_global

			} catch (ApplicationException e) { 
				// error handling for operation team29FindAll called Findallteams
				errorLogger("An error occured during the execution of the operation : team29FindAll",e); 
		
			}
	}
		
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("UC29_Onglets_1Controller [ ");
			strBToS.append(ALL_PLAYERS);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(allPlayers);
			strBToS.append(END_TO_STRING_DELIMITER);
			strBToS.append(ALL_TEAMS);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(allTeams);
			strBToS.append(END_TO_STRING_DELIMITER);
		// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(uC29_Onglets_1Validator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsUC29_Onglets_1Controller!=null); 
		return strBToS.toString();
	}
}
