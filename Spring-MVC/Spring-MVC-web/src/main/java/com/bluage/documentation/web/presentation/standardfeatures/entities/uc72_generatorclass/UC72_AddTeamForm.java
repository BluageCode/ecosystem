/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.entities.uc72_generatorclass;


import java.io.Serializable;
import java.util.List;

import com.bluage.documentation.business.standardfeatures.entities.uc72_generatorclass.bos.Team72BO;
import com.netfective.bluage.core.web.AbstractForm;
import com.netfective.bluage.core.web.IForm;

/**
* Class : UC72_AddTeamForm
*/
public class UC72_AddTeamForm extends AbstractForm implements Serializable, IForm {
	
	/** Constant : serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	//CONSTANT : allStates
	private static final String ALL_STATES = "allStates";
	//CONSTANT : listTeams
	private static final String LIST_TEAMS = "listTeams";
	//CONSTANT : selectedTeam
	private static final String SELECTED_TEAM = "selectedTeam";
	//CONSTANT : teamToCreate
	private static final String TEAM_TO_CREATE = "teamToCreate";
	/**
	 * 	Property: allStates 
	 */
	private List allStates;
	/**
	 * 	Property: listTeams 
	 */
	private List<Team72BO> listTeams;
	/**
	 * 	Property: selectedTeam 
	 */
	private Team72BO selectedTeam;
	/**
	 * 	Property: teamToCreate 
	 */
	private Team72BO teamToCreate;
/**
	 * Default constructor : UC72_AddTeamForm
	 */
	public UC72_AddTeamForm() {
		super();
		// Initialize : allStates
		this.allStates = new java.util.ArrayList();
		// Initialize : listTeams
		this.listTeams = new java.util.ArrayList<com.bluage.documentation.business.standardfeatures.entities.uc72_generatorclass.bos.Team72BO>();
		// Initialize : selectedTeam
		this.selectedTeam = null;
		// Initialize : teamToCreate
		this.teamToCreate = new Team72BO();
					this.setAlwaysCallPreControllers(true);
	}
	
	/**
	 * 	Getter : allStates 
	 * 	@return : Return the allStates instance.
	 */
	public List getAllStates(){
		return allStates; // For UC72_AddTeamForm
	}
	
	/**
	 * 	Setter : allStates 
	 *  @param allStatesinstance : The instance to set.
	 */
	public void setAllStates(final List allStatesinstance){
		this.allStates = allStatesinstance;// For UC72_AddTeamForm
	}
	/**
	 * 	Getter : listTeams 
	 * 	@return : Return the listTeams instance.
	 */
	public List<Team72BO> getListTeams(){
		return listTeams; // For UC72_AddTeamForm
	}
	
	/**
	 * 	Setter : listTeams 
	 *  @param listTeamsinstance : The instance to set.
	 */
	public void setListTeams(final List<Team72BO> listTeamsinstance){
		this.listTeams = listTeamsinstance;// For UC72_AddTeamForm
	}
	/**
	 * 	Getter : selectedTeam 
	 * 	@return : Return the selectedTeam instance.
	 */
	public Team72BO getSelectedTeam(){
		return selectedTeam; // For UC72_AddTeamForm
	}
	
	/**
	 * 	Setter : selectedTeam 
	 *  @param selectedTeaminstance : The instance to set.
	 */
	public void setSelectedTeam(final Team72BO selectedTeaminstance){
		this.selectedTeam = selectedTeaminstance;// For UC72_AddTeamForm
	}
	/**
	 * 	Getter : teamToCreate 
	 * 	@return : Return the teamToCreate instance.
	 */
	public Team72BO getTeamToCreate(){
		return teamToCreate; // For UC72_AddTeamForm
	}
	
	/**
	 * 	Setter : teamToCreate 
	 *  @param teamToCreateinstance : The instance to set.
	 */
	public void setTeamToCreate(final Team72BO teamToCreateinstance){
		this.teamToCreate = teamToCreateinstance;// For UC72_AddTeamForm
	}
/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UC72_AddTeamForm [ "+
ALL_STATES +" = " + allStates +LIST_TEAMS +" = " + listTeams +SELECTED_TEAM +" = " + selectedTeam +TEAM_TO_CREATE +" = " + teamToCreate + "]";
	}
	
	/**
	 * Method setInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public void setInstanceByInstanceName(final String instanceName, final Object instance) {
		// Set the instance AllStates.
		if(ALL_STATES.equals(instanceName)){// For UC72_AddTeamForm
			this.setAllStates((List)instance); // For UC72_AddTeamForm
		}
				// Set the instance ListTeams.
		if(LIST_TEAMS.equals(instanceName)){// For UC72_AddTeamForm
			this.setListTeams((List<Team72BO>)instance); // For UC72_AddTeamForm
		}
				// Set the instance SelectedTeam.
		if(SELECTED_TEAM.equals(instanceName)){// For UC72_AddTeamForm
			this.setSelectedTeam((Team72BO)instance); // For UC72_AddTeamForm
		}
				// Set the instance TeamToCreate.
		if(TEAM_TO_CREATE.equals(instanceName)){// For UC72_AddTeamForm
			this.setTeamToCreate((Team72BO)instance); // For UC72_AddTeamForm
		}
			}
	
	/**
	 * Method getInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public Object getInstanceByInstanceName(final String instanceName){
		// Temporary object for tmpUC72_AddTeamForm.
		Object tmpUC72_AddTeamForm = null;
		
			
		// Get the instance AllStates for UC72_AddTeamForm.
		if(ALL_STATES.equals(instanceName)){ // For UC72_AddTeamForm
			tmpUC72_AddTeamForm = this.getAllStates(); // For UC72_AddTeamForm
		}
			
		// Get the instance ListTeams for UC72_AddTeamForm.
		if(LIST_TEAMS.equals(instanceName)){ // For UC72_AddTeamForm
			tmpUC72_AddTeamForm = this.getListTeams(); // For UC72_AddTeamForm
		}
			
		// Get the instance SelectedTeam for UC72_AddTeamForm.
		if(SELECTED_TEAM.equals(instanceName)){ // For UC72_AddTeamForm
			tmpUC72_AddTeamForm = this.getSelectedTeam(); // For UC72_AddTeamForm
		}
			
		// Get the instance TeamToCreate for UC72_AddTeamForm.
		if(TEAM_TO_CREATE.equals(instanceName)){ // For UC72_AddTeamForm
			tmpUC72_AddTeamForm = this.getTeamToCreate(); // For UC72_AddTeamForm
		}
		return tmpUC72_AddTeamForm;// For UC72_AddTeamForm
	}
	
			}
