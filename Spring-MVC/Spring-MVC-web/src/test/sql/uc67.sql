--<ScriptOptions statementTerminator=";"/>

ALTER TABLE player67 DROP CONSTRAINT player67_pkey;

ALTER TABLE team67 DROP CONSTRAINT team67_pkey;

DROP TABLE player67;

DROP TABLE team67;

CREATE TABLE player67 (
		id INT8 NOT NULL,
		version INT4 NOT NULL,
		date_of_birth DATE,
		weight INT4,
		first_name VARCHAR(50) NOT NULL,
		height INT4,
		estimated_value FLOAT4,
		last_name VARCHAR(50) NOT NULL,
		rookie BOOL
	);

CREATE TABLE team67 (
		id INT8 NOT NULL,
		version INT4 NOT NULL,
		city VARCHAR(1024),
		name VARCHAR(1024)
	);

INSERT INTO player67 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie) VALUES (201, 0, 'Oliver', 'Frank', '1968-10-31', 0.11, 116, 228, false);
INSERT INTO player67 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie) VALUES (202, 0, 'Elbert', 'Cole', '1965-04-07', 3.95, 100, 171, false);
INSERT INTO player67 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie) VALUES (203, 0, 'Abel', 'Dennis', '1980-04-16', 30.51, 105, 208, false);
INSERT INTO player67 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie) VALUES (204, 0, 'Ricky', 'Dawson', '1990-03-03', 29.299999, 115, 168, true);
INSERT INTO player67 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie) VALUES (205, 0, 'Earl', 'Russell', '1981-12-15', 44.73, 60, 234, false);
INSERT INTO player67 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie) VALUES (206, 0, 'Nick', 'Garner', '1992-07-27', 47.84, 87, 221, true);
INSERT INTO player67 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie) VALUES (207, 0, 'Howard', 'Ramos', '1970-08-25', 10.76, 107, 199, false);
INSERT INTO player67 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie) VALUES (208, 0, 'Allan', 'Meyer', '1969-02-14', 33.669998, 114, 229, false);
INSERT INTO player67 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie) VALUES (209, 0, 'Ruben', 'Pope', '1985-05-01', 44.66, 86, 201, false);
INSERT INTO player67 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie) VALUES (210, 0, 'Lester', 'Cross', '1976-02-21', 30.98, 69, 214, false);
INSERT INTO player67 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie) VALUES (211, 0, 'Keith', 'Barnett', '1976-11-10', 5.8099999, 66, 190, false);
INSERT INTO player67 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie) VALUES (212, 0, 'Ivan', 'Tate', '1972-02-08', 48.490002, 143, 175, false);
INSERT INTO player67 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie) VALUES (213, 0, 'Tommy', 'Wood', '1992-01-20', 13.93, 77, 173, true);
INSERT INTO player67 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie) VALUES (214, 0, 'Sylvester', 'Nash', '1965-07-10', 37.73, 68, 184, false);
INSERT INTO player67 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie) VALUES (215, 0, 'Michael', 'Hodges', '1988-04-01', 25.35, 158, 204, false);
INSERT INTO player67 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie) VALUES (216, 0, 'Jerome', 'Grant', '1982-06-28', 28.940001, 105, 205, false);
INSERT INTO player67 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie) VALUES (217, 0, 'Woodrow', 'Carpenter', '1987-08-08', 41.759998, 69, 224, false);
INSERT INTO player67 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie) VALUES (218, 0, 'Roderick', 'Robbins', '1975-07-29', 12.04, 115, 164, false);

INSERT INTO team67 VALUES (1, 0, 'Charlotte', 'Tigers');
INSERT INTO team67 VALUES (2, 0, 'Kansas City', 'Bears');
INSERT INTO team67 VALUES (3, 0, 'Detroit', 'Chargers');
INSERT INTO team67 VALUES (4, 0, 'Detroit', 'Hawks');
INSERT INTO team67 VALUES (5, 0, 'Omaha', 'Braves');
INSERT INTO team67 VALUES (6, 0, 'Raleigh', 'Eagles');
INSERT INTO team67 VALUES (7, 0, 'Oakland', 'Cardinals');
INSERT INTO team67 VALUES (8, 0, 'Kansas City', 'Red Devils');
INSERT INTO team67 VALUES (9, 0, 'Fresno', 'Mustangs');

	
ALTER TABLE player67 ADD CONSTRAINT player67_pkey PRIMARY KEY (id);

ALTER TABLE team67 ADD CONSTRAINT team67_pkey PRIMARY KEY (id);

