/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.services.sql.uc56_sqloperation ;

// Import declaration.
// Java imports.
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.bluage.documentation.business.standardfeatures.services.sql.uc56_sqloperation.bos.Team56BO;
import com.bluage.documentation.service.standardfeatures.services.sql.uc56_sqloperation.ServiceLoadPlayers56;
import com.bluage.documentation.service.standardfeatures.services.sql.uc56_sqloperation.ServiceSearch56;
import com.netfective.bluage.core.exception.ApplicationException;
import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;
import com.netfective.bluage.core.web.IForm;

/**
 * Class : UC56_SearchController
 */
@Controller
@Scope("prototype")
@SessionAttributes("uC56_SearchForm")
@RequestMapping(value= "/presentation/standardfeatures/services/sql/uc56_sqloperation")
public class UC56_SearchController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC56_SearchController.class);
	
	//Current form name
	private static final String U_C56__SEARCH_FORM = "uC56_SearchForm";

								//CONSTANT: searchResult
	private static final String SEARCH_RESULT = "searchResult";
	//CONSTANT: sqlCriteria
	private static final String SQL_CRITERIA = "sqlCriteria";
	//CONSTANT: team
	private static final String TEAM = "team";
	//CONSTANT: selectedTeam
	private static final String SELECTED_TEAM = "selectedTeam";
	//CONSTANT: searchResultPostProcess
	private static final String SEARCH_RESULT_POST_PROCESS = "searchResultPostProcess";
	
	/**
	 * Property:customDateEditorsUC56_SearchController
	 */
	final private Map<String,CustomDateEditor> customDateEditorsUC56_SearchController = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:uC56_SearchValidator
	 */
	final private UC56_SearchValidator uC56_SearchValidator = new UC56_SearchValidator();
	
	/**
	 * Service declaration : serviceLoadPlayers56.
	 */
	@Autowired
	private ServiceLoadPlayers56 serviceLoadPlayers56;
	
	/**
	 * Service declaration : serviceSearch56.
	 */
	@Autowired
	private ServiceSearch56 serviceSearch56;
	
	
	// Initialise all the instances for UC56_SearchController
											// Declare the instance searchResult
		private List searchResult;
		// Declare the instance sqlCriteria
		private Team56BO sqlCriteria;
		// Declare the instance team
		private Team56BO team;
		// Declare the instance selectedTeam
		private Team56BO selectedTeam;
		// Declare the instance searchResultPostProcess
		private List searchResultPostProcess;
			/**
	 * Operation : lnk_searchTeams
 	 * @param model : 
 	 * @param uC56_Search : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC56_Search/lnk_searchTeams.html",method = RequestMethod.POST)
	public String lnk_searchTeams(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC56_SearchForm") UC56_SearchForm  uC56_SearchForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : lnk_searchTeams"); 
		infoLogger(uC56_SearchForm); 
		uC56_SearchForm.setAlwaysCallPreControllers(false); 
		// initialization for lnk_searchTeams
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state lnk_searchTeams if the validation fail, you will be forward to the screen : "/presentation/standardfeatures/services/sql/uc56_sqloperation/UC56_Search"
		if(errorsMessages(request,response,uC56_SearchForm,bindingResult,"", uC56_SearchValidator, customDateEditorsUC56_SearchController)){ 
			return "/presentation/standardfeatures/services/sql/uc56_sqloperation/UC56_Search"; 
		}

					 callSearchteambycriteria(request,uC56_SearchForm );
						// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.services.sql.uc56_sqloperation.UC56_SearchForm or not from lnk_searchTeams
			// Populate the destination form with all the instances returned from lnk_searchTeams.
			final IForm uC56_SearchForm2 = populateDestinationForm(request.getSession(), uC56_SearchForm); 
					
			// Add the form to the model.
			model.addAttribute("uC56_SearchForm", uC56_SearchForm2); 
			
			request.getSession().setAttribute("uC56_SearchForm", uC56_SearchForm2);
			
			// "OK" CASE => destination screen path from lnk_searchTeams
			LOGGER.info("Go to the screen 'UC56_Search'.");
			// Redirect (PRG) from lnk_searchTeams
			destinationPath =  "redirect:/presentation/standardfeatures/services/sql/uc56_sqloperation/UC56_Search.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_searchTeams 
	}
	
				/**
	 * Operation : lnk_loadPlayers
 	 * @param model : 
 	 * @param uC56_Search : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC56_Search/lnk_loadPlayers.html",method = RequestMethod.POST)
	public String lnk_loadPlayers(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC56_SearchForm") UC56_SearchForm  uC56_SearchForm, final BindingResult bindingResult ,final @RequestParam(ID_REQUEST_PARAM) Integer index){
		
		
		
		LOGGER.info("Begin method : lnk_loadPlayers"); 
		infoLogger(uC56_SearchForm); 
		uC56_SearchForm.setAlwaysCallPreControllers(false); 
		// initialization for lnk_loadPlayers
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state lnk_loadPlayers if the validation fail, you will be forward to the screen : "/presentation/standardfeatures/services/sql/uc56_sqloperation/UC56_Search"
		if(errorsMessages(request,response,uC56_SearchForm,bindingResult,"selectedTeam", uC56_SearchValidator, customDateEditorsUC56_SearchController)){ 
			return "/presentation/standardfeatures/services/sql/uc56_sqloperation/UC56_Search"; 
		}

										 callSearchPlayers(request,uC56_SearchForm , index);
											// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.services.sql.uc56_sqloperation.UC56_LoadPlayersForm or not from lnk_loadPlayers
			final UC56_LoadPlayersForm uC56_LoadPlayersForm =  new UC56_LoadPlayersForm();
			// Populate the destination form with all the instances returned from lnk_loadPlayers.
			final IForm uC56_LoadPlayersForm2 = populateDestinationForm(request.getSession(), uC56_LoadPlayersForm); 
					
			// Add the form to the model.
			model.addAttribute("uC56_LoadPlayersForm", uC56_LoadPlayersForm2); 
			
			request.getSession().setAttribute("uC56_LoadPlayersForm", uC56_LoadPlayersForm2);
			
			// "OK" CASE => destination screen path from lnk_loadPlayers
			LOGGER.info("Go to the screen 'UC56_LoadPlayers'.");
			// Redirect (PRG) from lnk_loadPlayers
			destinationPath =  "redirect:/presentation/standardfeatures/services/sql/uc56_sqloperation/UC56_LoadPlayers.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_loadPlayers 
	}
	
				/**
	 * Operation : lnk_searchTeamsPostProcess
 	 * @param model : 
 	 * @param uC56_Search : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC56_Search/lnk_searchTeamsPostProcess.html",method = RequestMethod.POST)
	public String lnk_searchTeamsPostProcess(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC56_SearchForm") UC56_SearchForm  uC56_SearchForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : lnk_searchTeamsPostProcess"); 
		infoLogger(uC56_SearchForm); 
		uC56_SearchForm.setAlwaysCallPreControllers(false); 
		// initialization for lnk_searchTeamsPostProcess
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state lnk_searchTeamsPostProcess if the validation fail, you will be forward to the screen : "/presentation/standardfeatures/services/sql/uc56_sqloperation/UC56_Search"
		if(errorsMessages(request,response,uC56_SearchForm,bindingResult,"", uC56_SearchValidator, customDateEditorsUC56_SearchController)){ 
			return "/presentation/standardfeatures/services/sql/uc56_sqloperation/UC56_Search"; 
		}

					 callSearchteambycriteriaandpostprocess(request,uC56_SearchForm );
						// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.services.sql.uc56_sqloperation.UC56_SearchForm or not from lnk_searchTeamsPostProcess
			// Populate the destination form with all the instances returned from lnk_searchTeamsPostProcess.
			final IForm uC56_SearchForm2 = populateDestinationForm(request.getSession(), uC56_SearchForm); 
					
			// Add the form to the model.
			model.addAttribute("uC56_SearchForm", uC56_SearchForm2); 
			
			request.getSession().setAttribute("uC56_SearchForm", uC56_SearchForm2);
			
			// "OK" CASE => destination screen path from lnk_searchTeamsPostProcess
			LOGGER.info("Go to the screen 'UC56_Search'.");
			// Redirect (PRG) from lnk_searchTeamsPostProcess
			destinationPath =  "redirect:/presentation/standardfeatures/services/sql/uc56_sqloperation/UC56_Search.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_searchTeamsPostProcess 
	}
	
	
	/**
	* This method initialise the form : UC56_SearchForm 
	* @return UC56_SearchForm
	*/
	@ModelAttribute("UC56_SearchFormInit")
	public void initUC56_SearchForm(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, U_C56__SEARCH_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method UC56_SearchForm."); //for lnk_searchTeamsPostProcess 
		}
		UC56_SearchForm uC56_SearchForm;
	
		if(request.getSession().getAttribute(U_C56__SEARCH_FORM) != null){
			uC56_SearchForm = (UC56_SearchForm)request.getSession().getAttribute(U_C56__SEARCH_FORM);
		} else {
			uC56_SearchForm = new UC56_SearchForm();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form UC56_SearchForm.");
		}
		uC56_SearchForm = (UC56_SearchForm)populateDestinationForm(request.getSession(), uC56_SearchForm);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  UC56_SearchForm.");
		}
		model.addAttribute(U_C56__SEARCH_FORM, uC56_SearchForm);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param UC56_SearchForm : The sceen form.
	 */
	@RequestMapping(value = "/UC56_Search.html" ,method = RequestMethod.GET)
	public void prepareUC56_Search(final HttpServletRequest request,final  Model model,final  @ModelAttribute("UC56_SearchFormInit") UC56_SearchForm uC56_SearchForm){
		
		UC56_SearchForm currentUC56_SearchForm = uC56_SearchForm;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(U_C56__SEARCH_FORM) == null){
			if(currentUC56_SearchForm!=null){
				request.getSession().setAttribute(U_C56__SEARCH_FORM, currentUC56_SearchForm);
			}else {
				currentUC56_SearchForm = new UC56_SearchForm();
				request.getSession().setAttribute(U_C56__SEARCH_FORM, currentUC56_SearchForm);	
			}
		} else {
			currentUC56_SearchForm = (UC56_SearchForm) request.getSession().getAttribute(U_C56__SEARCH_FORM);
		}

							currentUC56_SearchForm = (UC56_SearchForm)populateDestinationForm(request.getSession(), currentUC56_SearchForm);
		// Call all the Precontroller.
	currentUC56_SearchForm.setAlwaysCallPreControllers(true);
		model.addAttribute(U_C56__SEARCH_FORM, currentUC56_SearchForm);
		request.getSession().setAttribute(U_C56__SEARCH_FORM, currentUC56_SearchForm);
		
	}
	
									/**
	 * method callSearchteambycriteria
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callSearchteambycriteria(HttpServletRequest request,IForm form) {
		searchResult = (List)get(request.getSession(),form, "searchResult");  
										 
					sqlCriteria = (Team56BO)get(request.getSession(),form, "sqlCriteria");  
										 
					try {
				// executing Searchteambycriteria in lnk_searchTeamsPostProcess
				searchResult = 	serviceSearch56.executeSearch(
			sqlCriteria
			);  
 
				put(request.getSession(), SEARCH_RESULT,searchResult);
								// processing variables Searchteambycriteria in lnk_searchTeamsPostProcess
				put(request.getSession(), SQL_CRITERIA,sqlCriteria); 
			
			} catch (ApplicationException e) { 
				// error handling for operation executeSearch called Searchteambycriteria
				errorLogger("An error occured during the execution of the operation : executeSearch",e); 
		
			}
	}
									/**
	 * method callSearchPlayers
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 * @param index Integer the current index 
	 */
	private void callSearchPlayers(HttpServletRequest request,IForm form,Integer index) {
		team = (Team56BO)get(request.getSession(),form, "team");  
										 
					if(index!=null){  
			 selectedTeam = (Team56BO)((List)get(request.getSession(),form, "searchResult")).get(index);  
		} else {
			 selectedTeam= null; 
		}
				 
					try {
				// executing SearchPlayers in lnk_searchTeamsPostProcess
				team = 	serviceLoadPlayers56.executeLoad(
			selectedTeam
			);  
 
				put(request.getSession(), TEAM,team);
								// processing variables SearchPlayers in lnk_searchTeamsPostProcess
				put(request.getSession(), SELECTED_TEAM,selectedTeam); 
			
			} catch (ApplicationException e) { 
				// error handling for operation executeLoad called SearchPlayers
				errorLogger("An error occured during the execution of the operation : executeLoad",e); 
		
			}
	}
					/**
	 * method callSearchteambycriteriaandpostprocess
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callSearchteambycriteriaandpostprocess(HttpServletRequest request,IForm form) {
		searchResultPostProcess = (List)get(request.getSession(),form, "searchResultPostProcess");  
										 
					sqlCriteria = (Team56BO)get(request.getSession(),form, "sqlCriteria");  
										 
					try {
				// executing Searchteambycriteriaandpostprocess in lnk_searchTeamsPostProcess
				searchResultPostProcess = 	serviceSearch56.executeSearchPostProcess(
			sqlCriteria
			);  
 
				put(request.getSession(), SEARCH_RESULT_POST_PROCESS,searchResultPostProcess);
								// processing variables Searchteambycriteriaandpostprocess in lnk_searchTeamsPostProcess
				put(request.getSession(), SQL_CRITERIA,sqlCriteria); 
			
			} catch (ApplicationException e) { 
				// error handling for operation executeSearchPostProcess called Searchteambycriteriaandpostprocess
				errorLogger("An error occured during the execution of the operation : executeSearchPostProcess",e); 
		
			}
	}
		
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("UC56_SearchController [ ");
										strBToS.append(SEARCH_RESULT);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(searchResult);
			strBToS.append(END_TO_STRING_DELIMITER);
			strBToS.append(SQL_CRITERIA);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(sqlCriteria);
			strBToS.append(END_TO_STRING_DELIMITER);
			strBToS.append(TEAM);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(team);
			strBToS.append(END_TO_STRING_DELIMITER);
			strBToS.append(SELECTED_TEAM);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(selectedTeam);
			strBToS.append(END_TO_STRING_DELIMITER);
			strBToS.append(SEARCH_RESULT_POST_PROCESS);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(searchResultPostProcess);
			strBToS.append(END_TO_STRING_DELIMITER);
		// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(uC56_SearchValidator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsUC56_SearchController!=null); 
		return strBToS.toString();
	}
}
