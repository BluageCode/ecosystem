/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.qauc.uc1002_validators ;

// Import declaration.
// Java imports.
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.bluage.documentation.business.qauc.uc1002_validators.bos.Player1002BO;
import com.bluage.documentation.service.qauc.uc1002_validators.ServicePlayer1002;
import com.netfective.bluage.core.exception.ApplicationException;
import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;
import com.netfective.bluage.core.web.IForm;

/**
 * Class : UC1002_ValidatorsController
 */
@Controller
@Scope("prototype")
@SessionAttributes("uC1002_ValidatorsForm")
@RequestMapping(value= "/presentation/qauc/uc1002_validators")
public class UC1002_ValidatorsController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC1002_ValidatorsController.class);
	
	//Current form name
	private static final String U_C1002__VALIDATORS_FORM = "uC1002_ValidatorsForm";

		//CONSTANT: playerToCreate
	private static final String PLAYER_TO_CREATE = "playerToCreate";
	
	/**
	 * Property:customDateEditorsUC1002_ValidatorsController
	 */
	final private Map<String,CustomDateEditor> customDateEditorsUC1002_ValidatorsController = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:uC1002_ValidatorsValidator
	 */
	final private UC1002_ValidatorsValidator uC1002_ValidatorsValidator = new UC1002_ValidatorsValidator();
	
	/**
	 * Service declaration : servicePlayer1002.
	 */
	@Autowired
	private ServicePlayer1002 servicePlayer1002;
	
	
	// Initialise all the instances for UC1002_ValidatorsController
			// Declare the instance playerToCreate
		private Player1002BO playerToCreate;
			/**
	 * Operation : lnk_create
 	 * @param model : 
 	 * @param uC1002_Validators : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC1002_Validators/lnk_create.html",method = RequestMethod.POST)
	public String lnk_create(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC1002_ValidatorsForm") UC1002_ValidatorsForm  uC1002_ValidatorsForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : lnk_create"); 
		infoLogger(uC1002_ValidatorsForm); 
		uC1002_ValidatorsForm.setAlwaysCallPreControllers(false); 
		// initialization for lnk_create
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state lnk_create if the validation fail, you will be forward to the screen : "/presentation/qauc/uc1002_validators/UC1002_Validators"
		if(errorsMessages(request,response,uC1002_ValidatorsForm,bindingResult,"", uC1002_ValidatorsValidator, customDateEditorsUC1002_ValidatorsController)){ 
			return "/presentation/qauc/uc1002_validators/UC1002_Validators"; 
		}

					 callCreatePlayer(request,uC1002_ValidatorsForm );
											// Init the destination form com.bluage.documentation.web.presentation.qauc.uc1002_validators.UC1002_ValidatorsMessageForm or not from lnk_create
			final UC1002_ValidatorsMessageForm uC1002_ValidatorsMessageForm =  new UC1002_ValidatorsMessageForm();
			// Populate the destination form with all the instances returned from lnk_create.
			final IForm uC1002_ValidatorsMessageForm2 = populateDestinationForm(request.getSession(), uC1002_ValidatorsMessageForm); 
					
			// Add the form to the model.
			model.addAttribute("uC1002_ValidatorsMessageForm", uC1002_ValidatorsMessageForm2); 
			
			request.getSession().setAttribute("uC1002_ValidatorsMessageForm", uC1002_ValidatorsMessageForm2);
			
			// "OK" CASE => destination screen path from lnk_create
			LOGGER.info("Go to the screen 'UC1002_ValidatorsMessage'.");
			// Redirect (PRG) from lnk_create
			destinationPath =  "redirect:/presentation/qauc/uc1002_validators/UC1002_ValidatorsMessage.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_create 
	}
	
	
	/**
	* This method initialise the form : UC1002_ValidatorsForm 
	* @return UC1002_ValidatorsForm
	*/
	@ModelAttribute("UC1002_ValidatorsFormInit")
	public void initUC1002_ValidatorsForm(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, U_C1002__VALIDATORS_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method UC1002_ValidatorsForm."); //for lnk_create 
		}
		UC1002_ValidatorsForm uC1002_ValidatorsForm;
	
		if(request.getSession().getAttribute(U_C1002__VALIDATORS_FORM) != null){
			uC1002_ValidatorsForm = (UC1002_ValidatorsForm)request.getSession().getAttribute(U_C1002__VALIDATORS_FORM);
		} else {
			uC1002_ValidatorsForm = new UC1002_ValidatorsForm();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form UC1002_ValidatorsForm.");
		}
		uC1002_ValidatorsForm = (UC1002_ValidatorsForm)populateDestinationForm(request.getSession(), uC1002_ValidatorsForm);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  UC1002_ValidatorsForm.");
		}
		model.addAttribute(U_C1002__VALIDATORS_FORM, uC1002_ValidatorsForm);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param UC1002_ValidatorsForm : The sceen form.
	 */
	@RequestMapping(value = "/UC1002_Validators.html" ,method = RequestMethod.GET)
	public void prepareUC1002_Validators(final HttpServletRequest request,final  Model model,final  @ModelAttribute("UC1002_ValidatorsFormInit") UC1002_ValidatorsForm uC1002_ValidatorsForm){
		
		UC1002_ValidatorsForm currentUC1002_ValidatorsForm = uC1002_ValidatorsForm;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(U_C1002__VALIDATORS_FORM) == null){
			if(currentUC1002_ValidatorsForm!=null){
				request.getSession().setAttribute(U_C1002__VALIDATORS_FORM, currentUC1002_ValidatorsForm);
			}else {
				currentUC1002_ValidatorsForm = new UC1002_ValidatorsForm();
				request.getSession().setAttribute(U_C1002__VALIDATORS_FORM, currentUC1002_ValidatorsForm);	
			}
		} else {
			currentUC1002_ValidatorsForm = (UC1002_ValidatorsForm) request.getSession().getAttribute(U_C1002__VALIDATORS_FORM);
		}

		currentUC1002_ValidatorsForm = (UC1002_ValidatorsForm)populateDestinationForm(request.getSession(), currentUC1002_ValidatorsForm);
		// Call all the Precontroller.
	currentUC1002_ValidatorsForm.setAlwaysCallPreControllers(true);
		model.addAttribute(U_C1002__VALIDATORS_FORM, currentUC1002_ValidatorsForm);
		request.getSession().setAttribute(U_C1002__VALIDATORS_FORM, currentUC1002_ValidatorsForm);
		
	}
	
				/**
	 * method callCreatePlayer
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callCreatePlayer(HttpServletRequest request,IForm form) {
					playerToCreate = (Player1002BO)get(request.getSession(),form, "playerToCreate");  
										 
					try {
				// executing CreatePlayer in lnk_create
	servicePlayer1002.createPlayer1002(
			playerToCreate
			);  

								// processing variables CreatePlayer in lnk_create
				put(request.getSession(), PLAYER_TO_CREATE,playerToCreate); 
			
			} catch (ApplicationException e) { 
				// error handling for operation createPlayer1002 called CreatePlayer
				errorLogger("An error occured during the execution of the operation : createPlayer1002",e); 
		
			}
	}
		/**
	* This method Binder.
	* @param binder : 
	*		 WebDataBinder
	*/
	@InitBinder
	protected void initBinder(final WebDataBinder binder){
			customDateEditorsUC1002_ValidatorsController.put("playerToCreate.dateOfBirth", new CustomDateEditor(new SimpleDateFormat("MM/dd/yyyy"), true, "Invalid format."));
			binder.registerCustomEditor(Date.class, "playerToCreate.dateOfBirth", customDateEditorsUC1002_ValidatorsController.get("playerToCreate.dateOfBirth"));
	}
	
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("UC1002_ValidatorsController [ ");
				strBToS.append(PLAYER_TO_CREATE);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(playerToCreate);
			strBToS.append(END_TO_STRING_DELIMITER);
		// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(uC1002_ValidatorsValidator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsUC1002_ValidatorsController!=null); 
		return strBToS.toString();
	}
}
