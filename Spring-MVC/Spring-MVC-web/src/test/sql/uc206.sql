--<ScriptOptions statementTerminator=";"/>

ALTER TABLE player206 DROP CONSTRAINT player206_position_fkc;

ALTER TABLE position206 DROP CONSTRAINT position206_pkey;

ALTER TABLE player206 DROP CONSTRAINT player206_pkey;

DROP TABLE position206;

DROP TABLE player206;

CREATE TABLE position206 (
		code VARCHAR(2) NOT NULL,
		version INT4 NOT NULL,
		name VARCHAR(20) NOT NULL
	);

CREATE TABLE player206 (
		id INT8 NOT NULL,
		version INT4 NOT NULL,
		first_name VARCHAR(30) NOT NULL,
		last_name VARCHAR(30) NOT NULL,
		date_of_birth DATE,
		estimated_value INT4,
		weight INT4,
		height INT4,
		rookie BOOL,
		position_fk VARCHAR(1024) NOT NULL
	);
	
INSERT INTO position206 VALUES ('PG', 0, 'Point Guard');
INSERT INTO position206 VALUES ('SG', 0, 'Shooting Guard');
INSERT INTO position206 VALUES ('SF', 0, 'Small Forward');
INSERT INTO position206 VALUES ('PF', 0, 'Power Forward');
INSERT INTO position206 VALUES ('C', 0, 'Center');


	
INSERT INTO player206 VALUES (401, 0, 'Gregg', 'Nash', '1972-01-11', 6.8600001, 147, 215, false, 'PG');
INSERT INTO player206 VALUES (402, 0, 'Floyd', 'Howell', '1979-08-25', 25.709999, 155, 192, false, 'SG');
INSERT INTO player206 VALUES (403, 0, 'Simon', 'Klein', '1987-01-21', 22.24, 63, 203, false, 'SF');
INSERT INTO player206 VALUES (404, 0, 'Blake', 'Montgomery', '1992-11-16', 37.470001, 113, 205, true, 'SF');
INSERT INTO player206 VALUES (405, 0, 'Doyle', 'Burns', '1990-02-23', 39.130001, 153, 225, true, 'SG');
INSERT INTO player206 VALUES (406, 0, 'Troy', 'Rowe', '1973-02-12', 26.82, 94, 211, false, 'PF');
INSERT INTO player206 VALUES (407, 0, 'Jesus', 'Jimenez', '1985-09-01', 17.73, 133, 159, false, 'PF');
INSERT INTO player206 VALUES (408, 0, 'Ross', 'Guerrero', '1992-01-26', 13.68, 136, 171, true, 'SF');

	
	
ALTER TABLE position206 ADD CONSTRAINT position206_pkey PRIMARY KEY (code);

ALTER TABLE player206 ADD CONSTRAINT player206_pkey PRIMARY KEY (id);

ALTER TABLE player206 ADD CONSTRAINT player206_position_fkc FOREIGN KEY (position_fk)
	REFERENCES position206 (code);

