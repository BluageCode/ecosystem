/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.entities.uc72_generatorclass ;

// Import declaration.
// Java imports.
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.bluage.documentation.business.standardfeatures.entities.uc72_generatorclass.bos.Player72BO;
import com.bluage.documentation.business.standardfeatures.entities.uc72_generatorclass.entities.daofinder.Position72DAOFinderImpl;
import com.bluage.documentation.service.standardfeatures.entities.uc72_generatorclass.ServicePlayer72;
import com.netfective.bluage.core.exception.ApplicationException;
import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;
import com.netfective.bluage.core.web.IForm;

/**
 * Class : UC72_AddPlayerController
 */
@Controller
@Scope("prototype")
@SessionAttributes("uC72_AddPlayerForm")
@RequestMapping(value= "/presentation/standardfeatures/entities/uc72_generatorclass")
public class UC72_AddPlayerController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC72_AddPlayerController.class);
	
	//Current form name
	private static final String U_C72__ADD_PLAYER_FORM = "uC72_AddPlayerForm";

					//CONSTANT: allPositions
	private static final String ALL_POSITIONS = "allPositions";
				//CONSTANT: playerToCreate
	private static final String PLAYER_TO_CREATE = "playerToCreate";
	//CONSTANT: listPlayers
	private static final String LIST_PLAYERS = "listPlayers";
	//CONSTANT: selectedPlayer
	private static final String SELECTED_PLAYER = "selectedPlayer";
	
	/**
	 * Property:customDateEditorsUC72_AddPlayerController
	 */
	final private Map<String,CustomDateEditor> customDateEditorsUC72_AddPlayerController = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:uC72_AddPlayerValidator
	 */
	final private UC72_AddPlayerValidator uC72_AddPlayerValidator = new UC72_AddPlayerValidator();
	
	/**
	 * Service declaration : servicePlayer72.
	 */
	@Autowired
	private ServicePlayer72 servicePlayer72;
	
	/**
	 * Generic Finder : position72DAOFinderImpl.
	 */
	@Autowired
	private Position72DAOFinderImpl position72DAOFinderImpl;
	
	
	// Initialise all the instances for UC72_AddPlayerController
							// Initialize the instance allPositions for the state lnk_addTeam
		private List allPositions; // Initialize the instance allPositions for UC72_AddPlayerController
						// Declare the instance playerToCreate
		private Player72BO playerToCreate;
		// Declare the instance listPlayers
		private List listPlayers;
		// Declare the instance selectedPlayer
		private Player72BO selectedPlayer;
			/**
	 * Operation : lnk_create
 	 * @param model : 
 	 * @param uC72_AddPlayer : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC72_AddPlayer/lnk_create.html",method = RequestMethod.POST)
	public String lnk_create(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC72_AddPlayerForm") UC72_AddPlayerForm  uC72_AddPlayerForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : lnk_create"); 
		infoLogger(uC72_AddPlayerForm); 
		uC72_AddPlayerForm.setAlwaysCallPreControllers(false); 
		// initialization for lnk_create
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state lnk_create if the validation fail, you will be forward to the screen : "/presentation/standardfeatures/entities/uc72_generatorclass/UC72_AddPlayer"
		if(errorsMessages(request,response,uC72_AddPlayerForm,bindingResult,"", uC72_AddPlayerValidator, customDateEditorsUC72_AddPlayerController)){ 
			return "/presentation/standardfeatures/entities/uc72_generatorclass/UC72_AddPlayer"; 
		}

					destinationPath =  callCreateplayer(request,uC72_AddPlayerForm , model);
					if(destinationPath != null){
						return destinationPath;
					}
			
 callFindallplayers(request,uC72_AddPlayerForm );
						// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.entities.uc72_generatorclass.UC72_AddPlayerForm or not from lnk_create
			// Populate the destination form with all the instances returned from lnk_create.
			final IForm uC72_AddPlayerForm2 = populateDestinationForm(request.getSession(), uC72_AddPlayerForm); 
					
			// Add the form to the model.
			model.addAttribute("uC72_AddPlayerForm", uC72_AddPlayerForm2); 
			
			request.getSession().setAttribute("uC72_AddPlayerForm", uC72_AddPlayerForm2);
			
			// "OK" CASE => destination screen path from lnk_create
			LOGGER.info("Go to the screen 'UC72_AddPlayer'.");
			// Redirect (PRG) from lnk_create
			destinationPath =  "redirect:/presentation/standardfeatures/entities/uc72_generatorclass/UC72_AddPlayer.html";
						
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_create 
	}
	
									/**
	 * Operation : lnk_delete
 	 * @param model : 
 	 * @param uC72_AddPlayer : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC72_AddPlayer/lnk_delete.html",method = RequestMethod.POST)
	public String lnk_delete(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC72_AddPlayerForm") UC72_AddPlayerForm  uC72_AddPlayerForm, final BindingResult bindingResult ,final @RequestParam(ID_REQUEST_PARAM) Integer index){
		
		
		
		LOGGER.info("Begin method : lnk_delete"); 
		infoLogger(uC72_AddPlayerForm); 
		uC72_AddPlayerForm.setAlwaysCallPreControllers(false); 
		// initialization for lnk_delete
		init();
		
		String destinationPath = null; 
		

										 callDeleteplayer(request,uC72_AddPlayerForm , index);
			
 callSearchallplayers(request,uC72_AddPlayerForm );
						// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.entities.uc72_generatorclass.UC72_AddPlayerForm or not from lnk_delete
			// Populate the destination form with all the instances returned from lnk_delete.
			final IForm uC72_AddPlayerForm2 = populateDestinationForm(request.getSession(), uC72_AddPlayerForm); 
					
			// Add the form to the model.
			model.addAttribute("uC72_AddPlayerForm", uC72_AddPlayerForm2); 
			
			request.getSession().setAttribute("uC72_AddPlayerForm", uC72_AddPlayerForm2);
			
			// "OK" CASE => destination screen path from lnk_delete
			LOGGER.info("Go to the screen 'UC72_AddPlayer'.");
			// Redirect (PRG) from lnk_delete
			destinationPath =  "redirect:/presentation/standardfeatures/entities/uc72_generatorclass/UC72_AddPlayer.html";
						
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_delete 
	}
	
	
	/**
	* This method initialise the form : UC72_AddPlayerForm 
	* @return UC72_AddPlayerForm
	*/
	@ModelAttribute("UC72_AddPlayerFormInit")
	public void initUC72_AddPlayerForm(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, U_C72__ADD_PLAYER_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method UC72_AddPlayerForm."); //for lnk_delete 
		}
		UC72_AddPlayerForm uC72_AddPlayerForm;
	
		if(request.getSession().getAttribute(U_C72__ADD_PLAYER_FORM) != null){
			uC72_AddPlayerForm = (UC72_AddPlayerForm)request.getSession().getAttribute(U_C72__ADD_PLAYER_FORM);
		} else {
			uC72_AddPlayerForm = new UC72_AddPlayerForm();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form UC72_AddPlayerForm.");
		}
		uC72_AddPlayerForm = (UC72_AddPlayerForm)populateDestinationForm(request.getSession(), uC72_AddPlayerForm);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  UC72_AddPlayerForm.");
		}
		model.addAttribute(U_C72__ADD_PLAYER_FORM, uC72_AddPlayerForm);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param UC72_AddPlayerForm : The sceen form.
	 */
	@RequestMapping(value = "/UC72_AddPlayer.html" ,method = RequestMethod.GET)
	public void prepareUC72_AddPlayer(final HttpServletRequest request,final  Model model,final  @ModelAttribute("UC72_AddPlayerFormInit") UC72_AddPlayerForm uC72_AddPlayerForm){
		
		UC72_AddPlayerForm currentUC72_AddPlayerForm = uC72_AddPlayerForm;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(U_C72__ADD_PLAYER_FORM) == null){
			if(currentUC72_AddPlayerForm!=null){
				request.getSession().setAttribute(U_C72__ADD_PLAYER_FORM, currentUC72_AddPlayerForm);
			}else {
				currentUC72_AddPlayerForm = new UC72_AddPlayerForm();
				request.getSession().setAttribute(U_C72__ADD_PLAYER_FORM, currentUC72_AddPlayerForm);	
			}
		} else {
			currentUC72_AddPlayerForm = (UC72_AddPlayerForm) request.getSession().getAttribute(U_C72__ADD_PLAYER_FORM);
		}

		try {
			List allPositions = position72DAOFinderImpl.findAll();
			put(request.getSession(), ALL_POSITIONS, allPositions);
		} catch (ApplicationException e) {
			errorLogger("An error while calling the generic finder for the instance : allPositions.",e);
		}
					currentUC72_AddPlayerForm = (UC72_AddPlayerForm)populateDestinationForm(request.getSession(), currentUC72_AddPlayerForm);
		// Call all the Precontroller.
	currentUC72_AddPlayerForm.setAlwaysCallPreControllers(true);
		model.addAttribute(U_C72__ADD_PLAYER_FORM, currentUC72_AddPlayerForm);
		request.getSession().setAttribute(U_C72__ADD_PLAYER_FORM, currentUC72_AddPlayerForm);
		
	}
	
										/**
	 * method callCreateplayer
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 * @param model Model the model
	 * @return String result NOK 
	 */
	private String callCreateplayer(HttpServletRequest request,IForm form, final Model model) {
		String str = null;
					playerToCreate = (Player72BO)get(request.getSession(),form, "playerToCreate");  
										 
					try {
				// executing Createplayer in lnk_delete
	servicePlayer72.createPlayer72(
			playerToCreate
			);  

								// processing variables Createplayer in lnk_delete
				put(request.getSession(), PLAYER_TO_CREATE,playerToCreate); 
			
			} catch (ApplicationException e) { 
				// error handling for operation createPlayer72 called Createplayer
				errorLogger("An error occured during the execution of the operation : createPlayer72",e); 
		
				// "NOK" DEFAULT CASE
				UC72_ExceptionForm uC72_ExceptionForm = new UC72_ExceptionForm();
				final IForm uC72_ExceptionForm2 = populateDestinationForm(request.getSession(), uC72_ExceptionForm);
				model.addAttribute("uC72_ExceptionForm", uC72_ExceptionForm2);
				request.getSession().setAttribute("uC72_ExceptionForm", uC72_ExceptionForm2);
				request.getSession().setAttribute("messages", e.getMessage());
				return "redirect:/presentation/standardfeatures/entities/uc72_generatorclass/UC72_Exception.html";
						}
		return str;
	}
		/**
	 * method callFindallplayers
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callFindallplayers(HttpServletRequest request,IForm form) {
		listPlayers = (List)get(request.getSession(),form, "listPlayers");  
										 
					try {
				// executing Findallplayers in lnk_delete
				listPlayers = 	servicePlayer72.player72FindAll(
	);  
 
				put(request.getSession(), LIST_PLAYERS,listPlayers);
								// processing variables Findallplayers in lnk_delete

			} catch (ApplicationException e) { 
				// error handling for operation player72FindAll called Findallplayers
				errorLogger("An error occured during the execution of the operation : player72FindAll",e); 
		
			}
	}
									/**
	 * method callDeleteplayer
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 * @param index Integer the current index 
	 */
	private void callDeleteplayer(HttpServletRequest request,IForm form,Integer index) {
					if(index!=null){  
			 selectedPlayer = (Player72BO)((List)get(request.getSession(),form, "listPlayers")).get(index);  
		} else {
			 selectedPlayer= null; 
		}
				 
					try {
				// executing Deleteplayer in lnk_delete
	servicePlayer72.player72Delete(
			selectedPlayer
			);  

													// processing variables Deleteplayer in lnk_delete
				put(request.getSession(), SELECTED_PLAYER,selectedPlayer); 
			
			} catch (ApplicationException e) { 
				// error handling for operation player72Delete called Deleteplayer
				errorLogger("An error occured during the execution of the operation : player72Delete",e); 
		
			}
	}
		/**
	 * method callSearchallplayers
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callSearchallplayers(HttpServletRequest request,IForm form) {
		listPlayers = (List)get(request.getSession(),form, "listPlayers");  
										 
					try {
				// executing Searchallplayers in lnk_delete
				listPlayers = 	servicePlayer72.player72FindAll(
	);  
 
				put(request.getSession(), LIST_PLAYERS,listPlayers);
								// processing variables Searchallplayers in lnk_delete

			} catch (ApplicationException e) { 
				// error handling for operation player72FindAll called Searchallplayers
				errorLogger("An error occured during the execution of the operation : player72FindAll",e); 
		
			}
	}
		/**
	* This method Binder.
	* @param binder : 
	*		 WebDataBinder
	*/
	@InitBinder
	protected void initBinder(final WebDataBinder binder){
			customDateEditorsUC72_AddPlayerController.put("playerToCreate.dateOfBirth", new CustomDateEditor(new SimpleDateFormat("MM/dd/yyyy"), true, "Invalid format."));
			binder.registerCustomEditor(Date.class, "playerToCreate.dateOfBirth", customDateEditorsUC72_AddPlayerController.get("playerToCreate.dateOfBirth"));
	}
	
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("UC72_AddPlayerController [ ");
							strBToS.append(ALL_POSITIONS);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(allPositions);
			strBToS.append(END_TO_STRING_DELIMITER);
						strBToS.append(PLAYER_TO_CREATE);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(playerToCreate);
			strBToS.append(END_TO_STRING_DELIMITER);
			strBToS.append(LIST_PLAYERS);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(listPlayers);
			strBToS.append(END_TO_STRING_DELIMITER);
			strBToS.append(SELECTED_PLAYER);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(selectedPlayer);
			strBToS.append(END_TO_STRING_DELIMITER);
		// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(uC72_AddPlayerValidator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsUC72_AddPlayerController!=null); 
		return strBToS.toString();
	}
}
