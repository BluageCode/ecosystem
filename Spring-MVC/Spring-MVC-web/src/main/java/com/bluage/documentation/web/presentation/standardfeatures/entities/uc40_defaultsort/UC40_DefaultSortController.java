/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.entities.uc40_defaultsort ;

// Import declaration.
// Java imports.
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.bluage.documentation.business.standardfeatures.entities.uc40_defaultsort.bos.State40BO;
import com.bluage.documentation.business.standardfeatures.entities.uc40_defaultsort.entities.daofinder.State40DAOFinderImpl;
import com.bluage.documentation.service.standardfeatures.entities.uc40_defaultsort.SearchStateFromState40;
import com.bluage.documentation.service.standardfeatures.entities.uc40_defaultsort.ServiceState40;
import com.netfective.bluage.core.exception.ApplicationException;
import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;
import com.netfective.bluage.core.web.IForm;

/**
 * Class : UC40_DefaultSortController
 */
@Controller
@Scope("prototype")
@SessionAttributes("uC40_DefaultSortForm")
@RequestMapping(value= "/presentation/standardfeatures/entities/uc40_defaultsort")
public class UC40_DefaultSortController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC40_DefaultSortController.class);
	
	//Current form name
	private static final String U_C40__DEFAULT_SORT_FORM = "uC40_DefaultSortForm";

		//CONSTANT: stateInstance table or repeater.
	private static final String STATE_INSTANCE = "stateInstance";
				//CONSTANT: currentState40
	private static final String CURRENT_STATE40 = "currentState40";
	
	/**
	 * Property:customDateEditorsUC40_DefaultSortController
	 */
	final private Map<String,CustomDateEditor> customDateEditorsUC40_DefaultSortController = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:uC40_DefaultSortValidator
	 */
	final private UC40_DefaultSortValidator uC40_DefaultSortValidator = new UC40_DefaultSortValidator();
	
	/**
	 * Service declaration : searchStateFromState40.
	 */
	@Autowired
	private SearchStateFromState40 searchStateFromState40;
	
	/**
	 * Service declaration : serviceState40.
	 */
	@Autowired
	private ServiceState40 serviceState40;
	
	/**
	 * Generic Finder : state40DAOFinderImpl.
	 */
	@Autowired
	private State40DAOFinderImpl state40DAOFinderImpl;
	
	
	// Initialise all the instances for UC40_DefaultSortController
			// Initialize the instance stateInstance for the state lnk_view
		private List stateInstance; // Initialize the instance stateInstance for UC40_DefaultSortController
						// Declare the instance currentState40
		private State40BO currentState40;
			/**
	 * Operation : lnk_sort
 	 * @param model : 
 	 * @param uC40_DefaultSort : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC40_DefaultSort/lnk_sort.html",method = RequestMethod.POST)
	public String lnk_sort(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC40_DefaultSortForm") UC40_DefaultSortForm  uC40_DefaultSortForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : lnk_sort"); 
		infoLogger(uC40_DefaultSortForm); 
		uC40_DefaultSortForm.setAlwaysCallPreControllers(false); 
		// initialization for lnk_sort
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state lnk_sort if the validation fail, you will be forward to the screen : "/presentation/standardfeatures/entities/uc40_defaultsort/UC40_DefaultSort"
		if(errorsMessages(request,response,uC40_DefaultSortForm,bindingResult,"", uC40_DefaultSortValidator, customDateEditorsUC40_DefaultSortController)){ 
			return "/presentation/standardfeatures/entities/uc40_defaultsort/UC40_DefaultSort"; 
		}

					destinationPath =  callSeachstate40(request,uC40_DefaultSortForm , model);
					if(destinationPath != null){
						return destinationPath;
					}
			
				 callCreatestate40(request,uC40_DefaultSortForm );
						// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.entities.uc40_defaultsort.UC40_DefaultSortForm or not from lnk_sort
			// Populate the destination form with all the instances returned from lnk_sort.
			final IForm uC40_DefaultSortForm2 = populateDestinationForm(request.getSession(), uC40_DefaultSortForm); 
					
			// Add the form to the model.
			model.addAttribute("uC40_DefaultSortForm", uC40_DefaultSortForm2); 
			
			request.getSession().setAttribute("uC40_DefaultSortForm", uC40_DefaultSortForm2);
			
			// "OK" CASE => destination screen path from lnk_sort
			LOGGER.info("Go to the screen 'UC40_DefaultSort'.");
			// Redirect (PRG) from lnk_sort
			destinationPath =  "redirect:/presentation/standardfeatures/entities/uc40_defaultsort/UC40_DefaultSort.html";
						
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_sort 
	}
	
	
	/**
	* This method initialise the form : UC40_DefaultSortForm 
	* @return UC40_DefaultSortForm
	*/
	@ModelAttribute("UC40_DefaultSortFormInit")
	public void initUC40_DefaultSortForm(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, U_C40__DEFAULT_SORT_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method UC40_DefaultSortForm."); //for lnk_sort 
		}
		UC40_DefaultSortForm uC40_DefaultSortForm;
	
		if(request.getSession().getAttribute(U_C40__DEFAULT_SORT_FORM) != null){
			uC40_DefaultSortForm = (UC40_DefaultSortForm)request.getSession().getAttribute(U_C40__DEFAULT_SORT_FORM);
		} else {
			uC40_DefaultSortForm = new UC40_DefaultSortForm();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form UC40_DefaultSortForm.");
		}
		uC40_DefaultSortForm = (UC40_DefaultSortForm)populateDestinationForm(request.getSession(), uC40_DefaultSortForm);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  UC40_DefaultSortForm.");
		}
		model.addAttribute(U_C40__DEFAULT_SORT_FORM, uC40_DefaultSortForm);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param UC40_DefaultSortForm : The sceen form.
	 */
	@RequestMapping(value = "/UC40_DefaultSort.html" ,method = RequestMethod.GET)
	public void prepareUC40_DefaultSort(final HttpServletRequest request,final  Model model,final  @ModelAttribute("UC40_DefaultSortFormInit") UC40_DefaultSortForm uC40_DefaultSortForm){
		
		UC40_DefaultSortForm currentUC40_DefaultSortForm = uC40_DefaultSortForm;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(U_C40__DEFAULT_SORT_FORM) == null){
			if(currentUC40_DefaultSortForm!=null){
				request.getSession().setAttribute(U_C40__DEFAULT_SORT_FORM, currentUC40_DefaultSortForm);
			}else {
				currentUC40_DefaultSortForm = new UC40_DefaultSortForm();
				request.getSession().setAttribute(U_C40__DEFAULT_SORT_FORM, currentUC40_DefaultSortForm);	
			}
		} else {
			currentUC40_DefaultSortForm = (UC40_DefaultSortForm) request.getSession().getAttribute(U_C40__DEFAULT_SORT_FORM);
		}

		try {
			List stateInstance = state40DAOFinderImpl.findAll();
			put(request.getSession(), STATE_INSTANCE, stateInstance);
		} catch (ApplicationException e) {
			errorLogger("An error while calling the generic finder for the instance : stateInstance.",e);
		}
					currentUC40_DefaultSortForm = (UC40_DefaultSortForm)populateDestinationForm(request.getSession(), currentUC40_DefaultSortForm);
		// Call all the Precontroller.
	currentUC40_DefaultSortForm.setAlwaysCallPreControllers(true);
		model.addAttribute(U_C40__DEFAULT_SORT_FORM, currentUC40_DefaultSortForm);
		request.getSession().setAttribute(U_C40__DEFAULT_SORT_FORM, currentUC40_DefaultSortForm);
		
	}
	
							/**
	 * method callCreatestate40
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callCreatestate40(HttpServletRequest request,IForm form) {
					currentState40 = (State40BO)get(request.getSession(),form, "currentState40");  
										 
					try {
				// executing Createstate40 in lnk_sort
	serviceState40.state40Create(
			currentState40
			);  

													// processing variables Createstate40 in lnk_sort
				put(request.getSession(), CURRENT_STATE40,currentState40); 
			
			} catch (ApplicationException e) { 
				// error handling for operation state40Create called Createstate40
				errorLogger("An error occured during the execution of the operation : state40Create",e); 
		
			}
	}
								/**
	 * method callSeachstate40
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 * @param model Model the model
	 * @return String result NOK 
	 */
	private String callSeachstate40(HttpServletRequest request,IForm form, final Model model) {
		String str = null;
					currentState40 = (State40BO)get(request.getSession(),form, "currentState40");  
										 
					try {
				// executing Seachstate40 in lnk_sort
	searchStateFromState40.searchStateFromState40(
			currentState40
			);  

								// processing variables Seachstate40 in lnk_sort
				put(request.getSession(), CURRENT_STATE40,currentState40); 
			
			} catch (ApplicationException e) { 
				// error handling for operation searchStateFromState40 called Seachstate40
				errorLogger("An error occured during the execution of the operation : searchStateFromState40",e); 
		
				// "NOK" DEFAULT CASE
				UC40_ExceptionForm uC40_ExceptionForm = new UC40_ExceptionForm();
				final IForm uC40_ExceptionForm2 = populateDestinationForm(request.getSession(), uC40_ExceptionForm);
				model.addAttribute("uC40_ExceptionForm", uC40_ExceptionForm2);
				request.getSession().setAttribute("uC40_ExceptionForm", uC40_ExceptionForm2);
				request.getSession().setAttribute("messages", e.getMessage());
				return "redirect:/presentation/standardfeatures/entities/uc40_defaultsort/UC40_Exception.html";
						}
		return str;
	}
		
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("UC40_DefaultSortController [ ");
				strBToS.append(STATE_INSTANCE);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(stateInstance);
			strBToS.append(END_TO_STRING_DELIMITER);
						strBToS.append(CURRENT_STATE40);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(currentState40);
			strBToS.append(END_TO_STRING_DELIMITER);
		// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(uC40_DefaultSortValidator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsUC40_DefaultSortController!=null); 
		return strBToS.toString();
	}
}
