<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file="/include.jsp"  %>
<html >
<head >
<title  > 
<fmt:message key="cUC52_Conditional_BLU_AGE_DOCUMENTATION"/>
</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"  > 
</meta>
<link href="<c:url value= "/presentation/_style/_style.css"/>" rel="stylesheet" type="text/css"  /> 
<link href="<c:url value= "/presentation/_include/jquery/rd_lib.css"/>" rel="stylesheet" type="text/css"  /> 
<script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/jquery.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/rd_setup.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/rd_lib.js"/>" space="preserve"  ></script><link rel="shortcut icon" href="<c:url value= "/presentation/_style/favicon.ico"/>"  /> 

</head>

<body class=" " > 
<div id="rd_ghost"  > 
<small  > 
<fmt:message key="cUC52_Conditional___CSS_DEACTIVATED"/>
</small>
</div>
<div id="rd_jghost"  > 
<small  > 
<fmt:message key="cUC52_Conditional___JAVASCRIPT_DEACTIVATED"/>
</small>
</div>
<div id="shape"  > 
<div class="ui-layout-center"  > 
<div class="east-north"  > 
	<form:form id="uC52_Conditional" commandName="uC52_ConditionalForm"  enctype="application/x-www-form-urlencoded" method="post">
<h2 class="rd_first"  > 
<fmt:message key="cUC52_Conditional_STANDARD_FEATURES__SERVICES_"/>
</h2>
<h4  > 
<fmt:message key="cUC52_Conditional_NEW_PLAYER"/>
</h4>
<table class="rd_tableform"  >
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC52_Conditional_FIRST_NAME_"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
<form:input id="txt_firstName" path="newPlayer.firstName"  
		/>
</td> 
<td rowspan="1" colspan="1"  >
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC52_Conditional_LAST_NAME_"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
<form:input id="txt_lastName" path="newPlayer.lastName"  
		/>
</td> 
<td rowspan="1" colspan="1"  >
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC52_Conditional_ESTIMATED_VALUE_M_"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
<form:input id="txt_value" path="newPlayer.value"  
		/>
</td> 
<td rowspan="1" colspan="1"  >
<label id="hid_err_lbl" style="visibility:hidden; text-align:left"  > 
<fmt:message key="cUC52_Conditional_PLEASE_ENTER_A_CORRECT_VALUE"/>
</label>

       <script type="text/javascript">
       function validateValue() {
        var playerValue = document.getElementById("myForm:txt_value").value;
        reg = new RegExp("^[0-9]+[\.]?[0-9]*$","");
        if( !reg.test(playerValue) && playerValue!="" ) 
         document.getElementById("hid_err_lbl").style.visibility = "visible";
       }
       validateValue();
       </script>
      
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC52_Conditional_ROOKIE_"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
<form:checkbox id="chx_rookie" path="newPlayer.rookie"   
onclick="isRookie(this)"		/>
</td> 
<td rowspan="1" colspan="1"  >
<label id="hid_chx_rookie" style="visibility:hidden; text-align:left"  > 
<fmt:message key="cUC52_Conditional_THE_ESTIMATED_VALUE_OF_A_ROOKI"/>
</label>

       <script type="text/javascript">
       function isRookie(checkbox) {
        if(checkbox.checked == true) 
         document.getElementById("hid_chx_rookie").style.visibility = "visible";
           else
         document.getElementById("hid_chx_rookie").style.visibility = "hidden";
       }      
       </script>
      
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
</td> 
<td rowspan="1" colspan="1"  >
<a id="lnk_addPlayer"onclick="javascript:btn_click_cExecuteUC52_Conditional(this.href);return false;"				href="<c:url value="/presentation/standardfeatures/services/process/uc52_conditional/UC52_Conditional/lnk_addPlayer.html"/>"
							 shape="rect"  >									 
<fmt:message key="cUC52_Conditional_ADD_NEW_PLAYER"/>
</a>
</td> 
<td rowspan="1" colspan="1"  >
</td> 
</tr>
</table> 
<br clear="none"  /> 
<h4  > 
<fmt:message key="cUC52_Conditional_LIST_OF_PLAYERS"/>
</h4>
<a id="lnk_showPlayers"onclick="javascript:btn_click_cExecuteUC52_Conditional(this.href);return false;"				href="<c:url value="/presentation/standardfeatures/services/process/uc52_conditional/UC52_Conditional/lnk_showPlayers.html"/>"
							 shape="rect"  >									 
<fmt:message key="cUC52_Conditional_SHOW_LIST_OF_PLAYERS"/>
</a>
<br clear="none"  /> 
<br clear="none"  /> 
<display:table uid="tab_PlayersList"  sort="list" requestURI="" name="sessionScope.uC52_ConditionalForm.players" class="rd_tabledata"  >
	<c:set   var="rownum">
		<%=(tab_PlayersList !=	null) ? new Integer(tab_PlayersList_rowNum.intValue() - 1) : new Integer(0) %>
	</c:set>
	<c:set   var="idRow">
		<%=(tab_PlayersList != null ) ? tab_PlayersList_rowNum.intValue() - 1 : -1 %>
	</c:set>
	<c:set   var="idTab">
		<%=(tab_PlayersList != null ) ? "tab_PlayersList" : null %>
	</c:set>
	<c:set var="tableHashId">
		<%= new org.displaytag.util.ParamEncoder("tab_PlayersList").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE) %>
	</c:set>
	<c:set var="tableHashId2">
		<%=request.getParameter(new org.displaytag.util.ParamEncoder("tab_PlayersList").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE)) %>
	</c:set>	
	
<display:column format=""  titleKey="cUC52_Conditional_LAST_NAME">
		<span id="txt_lastname1">
	<c:out value="${tab_PlayersList.lastName}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="cUC52_Conditional_FIRST_NAME">
		<span id="txt_firstname1">
	<c:out value="${tab_PlayersList.firstName}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="cUC52_Conditional_ESTIMATED_VALUE_M">
		<span id="txt_value1">
	<c:out value="${tab_PlayersList.value}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="cUC52_Conditional_ROOKIE">
<form:checkbox id="chk_rookie[${rownum}]" path="players[${rownum}].rookie"  name="isRookie" disabled="true"  
		/>
</display:column>
</display:table>
	</form:form>
	<script language="javascript">
		function btn_click_cExecuteUC52_Conditional(url) {
			UC52_Conditionalvar = document.getElementById('uC52_Conditional');
			UC52_Conditionalvar.setAttribute('action', url);
			UC52_Conditionalvar.submit();
		}
		function btn_click_cSetActionUC52_Conditional(url) {
			UC52_Conditionalvar = document.getElementById('uC52_Conditional');
			UC52_Conditionalvar.setAttribute('action', url);
		}
	</script>
</div>
<div class="east-center"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/services/process/uc52_conditional/UC52_Conditionalcmp_tabs" />
</div>
</div>
<div class="ui-layout-north"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/services/process/uc52_conditional/UC52_Conditionalcmp_header" />
</div>
<div class="ui-layout-south"  > 
<fmt:message key="cUC52_Conditional________"/>
<span id="rd_jversion"  > 
<fmt:message key="cUC52_Conditional_BLU_AGE_DOCUMENTATION_213"/>
</span>
</div>
<div class="ui-layout-west"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/services/process/uc52_conditional/UC52_Conditionalcmp_treeview" />
</div>
</div>
</body>
</html>
