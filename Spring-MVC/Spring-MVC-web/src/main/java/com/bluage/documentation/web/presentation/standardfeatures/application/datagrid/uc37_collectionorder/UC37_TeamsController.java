/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.application.datagrid.uc37_collectionorder ;

// Import declaration.
// Java imports.
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.bluage.documentation.business.standardfeatures.application.datagrid.uc37_collectionorder.bos.Team37BO;
import com.bluage.documentation.business.standardfeatures.application.datagrid.uc37_collectionorder.entities.daofinder.Team37DAOFinderImpl;
import com.bluage.documentation.service.standardfeatures.application.datagrid.uc37_collectionorder.ServiceCollectionOrder;
import com.netfective.bluage.core.exception.ApplicationException;
import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;
import com.netfective.bluage.core.web.IForm;

/**
 * Class : UC37_TeamsController
 */
@Controller
@Scope("prototype")
@SessionAttributes("uC37_TeamsForm")
@RequestMapping(value= "/presentation/standardfeatures/application/datagrid/uc37_collectionorder")
public class UC37_TeamsController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC37_TeamsController.class);
	
	//Current form name
	private static final String U_C37__TEAMS_FORM = "uC37_TeamsForm";

	//CONSTANT: teams table or repeater.
	private static final String TEAMS = "teams";
				//CONSTANT: team
	private static final String TEAM = "team";
	//CONSTANT: selectedTeam
	private static final String SELECTED_TEAM = "selectedTeam";
	
	/**
	 * Property:customDateEditorsUC37_TeamsController
	 */
	final private Map<String,CustomDateEditor> customDateEditorsUC37_TeamsController = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:uC37_TeamsValidator
	 */
	final private UC37_TeamsValidator uC37_TeamsValidator = new UC37_TeamsValidator();
	
	/**
	 * Service declaration : serviceCollectionOrder.
	 */
	@Autowired
	private ServiceCollectionOrder serviceCollectionOrder;
	
	/**
	 * Generic Finder : team37DAOFinderImpl.
	 */
	@Autowired
	private Team37DAOFinderImpl team37DAOFinderImpl;
	
	
	// Initialise all the instances for UC37_TeamsController
		// Initialize the instance teams for the state lnk_view
		private List teams; // Initialize the instance teams for UC37_TeamsController
						// Declare the instance team
		private Team37BO team;
		// Declare the instance selectedTeam
		private Team37BO selectedTeam;
			/**
	 * Operation : lnk_view
 	 * @param model : 
 	 * @param uC37_Teams : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC37_Teams/lnk_view.html",method = RequestMethod.POST)
	public String lnk_view(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC37_TeamsForm") UC37_TeamsForm  uC37_TeamsForm, final BindingResult bindingResult ,final @RequestParam(ID_REQUEST_PARAM) Integer index){
		
		
		
		LOGGER.info("Begin method : lnk_view"); 
		infoLogger(uC37_TeamsForm); 
		uC37_TeamsForm.setAlwaysCallPreControllers(false); 
		// initialization for lnk_view
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state lnk_view if the validation fail, you will be forward to the screen : "/presentation/standardfeatures/application/datagrid/uc37_collectionorder/UC37_Teams"
		if(errorsMessages(request,response,uC37_TeamsForm,bindingResult,"selectedTeam", uC37_TeamsValidator, customDateEditorsUC37_TeamsController)){ 
			return "/presentation/standardfeatures/application/datagrid/uc37_collectionorder/UC37_Teams"; 
		}

										 callLoadteamwithplayers(request,uC37_TeamsForm , index);
											// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.application.datagrid.uc37_collectionorder.UC37_TeamsAndPlayersForm or not from lnk_view
			final UC37_TeamsAndPlayersForm uC37_TeamsAndPlayersForm =  new UC37_TeamsAndPlayersForm();
			// Populate the destination form with all the instances returned from lnk_view.
			final IForm uC37_TeamsAndPlayersForm2 = populateDestinationForm(request.getSession(), uC37_TeamsAndPlayersForm); 
					
			// Add the form to the model.
			model.addAttribute("uC37_TeamsAndPlayersForm", uC37_TeamsAndPlayersForm2); 
			
			request.getSession().setAttribute("uC37_TeamsAndPlayersForm", uC37_TeamsAndPlayersForm2);
			
			// "OK" CASE => destination screen path from lnk_view
			LOGGER.info("Go to the screen 'UC37_TeamsAndPlayers'.");
			// Redirect (PRG) from lnk_view
			destinationPath =  "redirect:/presentation/standardfeatures/application/datagrid/uc37_collectionorder/UC37_TeamsAndPlayers.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_view 
	}
	
	
	/**
	* This method initialise the form : UC37_TeamsForm 
	* @return UC37_TeamsForm
	*/
	@ModelAttribute("UC37_TeamsFormInit")
	public void initUC37_TeamsForm(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, U_C37__TEAMS_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method UC37_TeamsForm."); //for lnk_view 
		}
		UC37_TeamsForm uC37_TeamsForm;
	
		if(request.getSession().getAttribute(U_C37__TEAMS_FORM) != null){
			uC37_TeamsForm = (UC37_TeamsForm)request.getSession().getAttribute(U_C37__TEAMS_FORM);
		} else {
			uC37_TeamsForm = new UC37_TeamsForm();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form UC37_TeamsForm.");
		}
		uC37_TeamsForm = (UC37_TeamsForm)populateDestinationForm(request.getSession(), uC37_TeamsForm);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  UC37_TeamsForm.");
		}
		model.addAttribute(U_C37__TEAMS_FORM, uC37_TeamsForm);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param UC37_TeamsForm : The sceen form.
	 */
	@RequestMapping(value = "/UC37_Teams.html" ,method = RequestMethod.GET)
	public void prepareUC37_Teams(final HttpServletRequest request,final  Model model,final  @ModelAttribute("UC37_TeamsFormInit") UC37_TeamsForm uC37_TeamsForm){
		
		UC37_TeamsForm currentUC37_TeamsForm = uC37_TeamsForm;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(U_C37__TEAMS_FORM) == null){
			if(currentUC37_TeamsForm!=null){
				request.getSession().setAttribute(U_C37__TEAMS_FORM, currentUC37_TeamsForm);
			}else {
				currentUC37_TeamsForm = new UC37_TeamsForm();
				request.getSession().setAttribute(U_C37__TEAMS_FORM, currentUC37_TeamsForm);	
			}
		} else {
			currentUC37_TeamsForm = (UC37_TeamsForm) request.getSession().getAttribute(U_C37__TEAMS_FORM);
		}

		try {
			List teams = team37DAOFinderImpl.findAll();
			put(request.getSession(), TEAMS, teams);
		} catch (ApplicationException e) {
			errorLogger("An error while calling the generic finder for the instance : teams.",e);
		}
				currentUC37_TeamsForm = (UC37_TeamsForm)populateDestinationForm(request.getSession(), currentUC37_TeamsForm);
		// Call all the Precontroller.
	currentUC37_TeamsForm.setAlwaysCallPreControllers(true);
		model.addAttribute(U_C37__TEAMS_FORM, currentUC37_TeamsForm);
		request.getSession().setAttribute(U_C37__TEAMS_FORM, currentUC37_TeamsForm);
		
	}
	
										/**
	 * method callLoadteamwithplayers
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 * @param index Integer the current index 
	 */
	private void callLoadteamwithplayers(HttpServletRequest request,IForm form,Integer index) {
		team = (Team37BO)get(request.getSession(),form, "team");  
										 
					if(index!=null){  
			 selectedTeam = (Team37BO)((List)get(request.getSession(),form, "teams")).get(index);  
		} else {
			 selectedTeam= null; 
		}
				 
					try {
				// executing Loadteamwithplayers in lnk_view
				team = 	serviceCollectionOrder.loadTeamWithPlayers(
			selectedTeam
			);  
 
				put(request.getSession(), TEAM,team);
								// processing variables Loadteamwithplayers in lnk_view
				put(request.getSession(), SELECTED_TEAM,selectedTeam); 
			
			} catch (ApplicationException e) { 
				// error handling for operation loadTeamWithPlayers called Loadteamwithplayers
				errorLogger("An error occured during the execution of the operation : loadTeamWithPlayers",e); 
		
			}
	}
		
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("UC37_TeamsController [ ");
			strBToS.append(TEAMS);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(teams);
			strBToS.append(END_TO_STRING_DELIMITER);
						strBToS.append(TEAM);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(team);
			strBToS.append(END_TO_STRING_DELIMITER);
			strBToS.append(SELECTED_TEAM);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(selectedTeam);
			strBToS.append(END_TO_STRING_DELIMITER);
		// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(uC37_TeamsValidator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsUC37_TeamsController!=null); 
		return strBToS.toString();
	}
}
