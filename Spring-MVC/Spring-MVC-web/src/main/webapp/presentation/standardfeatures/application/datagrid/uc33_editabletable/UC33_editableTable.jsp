<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file="/include.jsp"  %>
<html >
<head >
<title  > 
<fmt:message key="cUC33_editableTable_BLU_AGE_DOCUMENTATION"/>
</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"  > 
</meta>
<link href="<c:url value= "/presentation/_style/_style.css"/>" rel="stylesheet" type="text/css"  /> 
<link href="<c:url value= "/presentation/_include/jquery/rd_lib.css"/>" rel="stylesheet" type="text/css"  /> 
<script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/jquery.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/rd_setup.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/rd_lib.js"/>" space="preserve"  ></script><link rel="shortcut icon" href="<c:url value= "/presentation/_style/favicon.ico"/>"  /> 

</head>

<body class=" " > 
<div id="rd_ghost"  > 
<small  > 
<fmt:message key="cUC33_editableTable___CSS_DEACTIVATED"/>
</small>
</div>
<div id="rd_jghost"  > 
<small  > 
<fmt:message key="cUC33_editableTable___JAVASCRIPT_DEACTIVATED"/>
</small>
</div>
<div id="shape"  > 
<div class="ui-layout-center"  > 
<div class="east-north"  > 
	<form:form id="uC33_editableTable" commandName="uC33_editableTableForm"  enctype="application/x-www-form-urlencoded" method="post">
<h2 class="rd_first"  > 
<fmt:message key="cUC33_editableTable_STANDARD_FEATURES__APPLICATIO"/>
</h2>
<display:table uid="tab_teams"  sort="list" requestURI="" name="sessionScope.uC33_editableTableForm.allTeams" class="rd_tabledata"  >
	<c:set   var="rownum">
		<%=(tab_teams !=	null) ? new Integer(tab_teams_rowNum.intValue() - 1) : new Integer(0) %>
	</c:set>
	<c:set   var="idRow">
		<%=(tab_teams != null ) ? tab_teams_rowNum.intValue() - 1 : -1 %>
	</c:set>
	<c:set   var="idTab">
		<%=(tab_teams != null ) ? "tab_teams" : null %>
	</c:set>
	<c:set var="tableHashId">
		<%= new org.displaytag.util.ParamEncoder("tab_teams").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE) %>
	</c:set>
	<c:set var="tableHashId2">
		<%=request.getParameter(new org.displaytag.util.ParamEncoder("tab_teams").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE)) %>
	</c:set>	
	
<display:column format=""  titleKey="cUC33_editableTable_NAME">
<c:if test="${uC33_editableTableForm.selectedRow!=idRow || uC33_editableTableForm.selectedTab!=idTab}">
		<span id="dyn_name">
	<c:out value="${tab_teams.name}" />
		</span>
	</c:if>
<c:if test="${uC33_editableTableForm.selectedRow==idRow && uC33_editableTableForm.selectedTab==idTab}">
<form:input id="txt_name[${rownum}]" path="allTeams[${rownum}].name"  
		/>
<form:errors path="allTeams[${rownum}].name"/>
</c:if>
</display:column>
	
<display:column format=""  titleKey="cUC33_editableTable_CITY">
<c:if test="${uC33_editableTableForm.selectedRow!=idRow || uC33_editableTableForm.selectedTab!=idTab}">
		<span id="dyn_city">
	<c:out value="${tab_teams.city}" />
		</span>
	</c:if>
<c:if test="${uC33_editableTableForm.selectedRow==idRow && uC33_editableTableForm.selectedTab==idTab}">
<form:input id="txt_city[${rownum}]" path="allTeams[${rownum}].city"  
		/>
<form:errors path="allTeams[${rownum}].city"/>
</c:if>
</display:column>
	
<display:column format=""  titleKey="cUC33_editableTable_">
<c:if test="${uC33_editableTableForm.selectedRow!=idRow || uC33_editableTableForm.selectedTab!=idTab}">
<a id="lnk_edit[${rownum}]"onclick="javascript:btn_click_cExecuteUC33_editableTable(this.href);return false;"			href="<c:url value="/presentation/standardfeatures/application/datagrid/uc33_editabletable/UC33_editableTable/lnk_edit.html?id="/><c:out value="${idRow}" />&tab=<c:out value="tab_teams" />&<c:out value="${tableHashId}"/>=<c:out value="${tableHashId2}"/>"
	 shape="rect"  >									 
<fmt:message key="cUC33_editableTable_EDIT"/>
</a>
</c:if>
<c:if test="${uC33_editableTableForm.selectedRow==idRow && uC33_editableTableForm.selectedTab==idTab}">
<a id="btn_cancel[${rownum}]"onclick="javascript:btn_click_cExecuteUC33_editableTable(this.href);return false;"			href="<c:url value="/presentation/standardfeatures/application/datagrid/uc33_editabletable/UC33_editableTable/btn_cancel.html?id="/><c:out value="${idRow}" />&tab=<c:out value="tab_teams" />&<c:out value="${tableHashId}"/>=<c:out value="${tableHashId2}"/>"
	 shape="rect"  >									 
<fmt:message key="cUC33_editableTable_CANCEL"/>
</a>
<fmt:message key="cUC33_editableTable_______________________"/>
<a id="lnk_update[${rownum}]"onclick="javascript:btn_click_cExecuteUC33_editableTable(this.href);return false;"			href="<c:url value="/presentation/standardfeatures/application/datagrid/uc33_editabletable/UC33_editableTable/lnk_update.html?id="/><c:out value="${idRow}" />&tab=<c:out value="tab_teams" />&<c:out value="${tableHashId}"/>=<c:out value="${tableHashId2}"/>"
	 shape="rect"  >									 
<fmt:message key="cUC33_editableTable_UPDATE"/>
</a>
</c:if>
</display:column>
</display:table>
	</form:form>
	<script language="javascript">
		function btn_click_cExecuteUC33_editableTable(url) {
			UC33_editableTablevar = document.getElementById('uC33_editableTable');
			UC33_editableTablevar.setAttribute('action', url);
			UC33_editableTablevar.submit();
		}
		function btn_click_cSetActionUC33_editableTable(url) {
			UC33_editableTablevar = document.getElementById('uC33_editableTable');
			UC33_editableTablevar.setAttribute('action', url);
		}
	</script>
</div>
<div class="east-center"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/application/datagrid/uc33_editabletable/UC33_editableTablecmp_tabs" />
</div>
</div>
<div class="ui-layout-north"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/application/datagrid/uc33_editabletable/UC33_editableTablecmp_header" />
</div>
<div class="ui-layout-south"  > 
<fmt:message key="cUC33_editableTable___"/>
<span id="rd_jversion"  > 
<fmt:message key="cUC33_editableTable_BLU_AGE_CORPORATION"/>
</span>
</div>
<div class="ui-layout-west"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/application/datagrid/uc33_editabletable/UC33_editableTablecmp_treeview" />
</div>
</div>
</body>
</html>
