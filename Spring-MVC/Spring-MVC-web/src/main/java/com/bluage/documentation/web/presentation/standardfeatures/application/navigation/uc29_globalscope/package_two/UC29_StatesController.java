/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.application.navigation.uc29_globalscope.package_two ;

// Import declaration.
// Java imports.
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;

/**
 * Class : UC29_StatesController
 */
@Controller
@Scope("prototype")
@SessionAttributes("uC29_StatesForm")
@RequestMapping(value= "/presentation/standardfeatures/application/navigation/uc29_globalscope/package_two")
public class UC29_StatesController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC29_StatesController.class);
	
	//Current form name
	private static final String U_C29__STATES_FORM = "uC29_StatesForm";

	//CONSTANT: allStates table or repeater.
	private static final String ALL_STATES = "allStates";
				
	/**
	 * Property:customDateEditorsUC29_StatesController
	 */
	final private Map<String,CustomDateEditor> customDateEditorsUC29_StatesController = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:uC29_StatesValidator
	 */
	final private UC29_StatesValidator uC29_StatesValidator = new UC29_StatesValidator();
	
	
	// Initialise all the instances for UC29_StatesController
		// Initialize the instance allStates for the state findTeams
		private List allStates; // Initialize the instance allStates for UC29_StatesController
				
	/**
	* This method initialise the form : UC29_StatesForm 
	* @return UC29_StatesForm
	*/
	@ModelAttribute("UC29_StatesFormInit")
	public void initUC29_StatesForm(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, U_C29__STATES_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method UC29_StatesForm."); //for findTeams 
		}
		UC29_StatesForm uC29_StatesForm;
	
		if(request.getSession().getAttribute(U_C29__STATES_FORM) != null){
			uC29_StatesForm = (UC29_StatesForm)request.getSession().getAttribute(U_C29__STATES_FORM);
		} else {
			uC29_StatesForm = new UC29_StatesForm();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form UC29_StatesForm.");
		}
		uC29_StatesForm = (UC29_StatesForm)populateDestinationForm(request.getSession(), uC29_StatesForm);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  UC29_StatesForm.");
		}
		model.addAttribute(U_C29__STATES_FORM, uC29_StatesForm);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param UC29_StatesForm : The sceen form.
	 */
	@RequestMapping(value = "/UC29_States.html" ,method = RequestMethod.GET)
	public void prepareUC29_States(final HttpServletRequest request,final  Model model,final  @ModelAttribute("UC29_StatesFormInit") UC29_StatesForm uC29_StatesForm){
		
		UC29_StatesForm currentUC29_StatesForm = uC29_StatesForm;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(U_C29__STATES_FORM) == null){
			if(currentUC29_StatesForm!=null){
				request.getSession().setAttribute(U_C29__STATES_FORM, currentUC29_StatesForm);
			}else {
				currentUC29_StatesForm = new UC29_StatesForm();
				request.getSession().setAttribute(U_C29__STATES_FORM, currentUC29_StatesForm);	
			}
		} else {
			currentUC29_StatesForm = (UC29_StatesForm) request.getSession().getAttribute(U_C29__STATES_FORM);
		}

					currentUC29_StatesForm = (UC29_StatesForm)populateDestinationForm(request.getSession(), currentUC29_StatesForm);
		// Call all the Precontroller.
	currentUC29_StatesForm.setAlwaysCallPreControllers(true);
		model.addAttribute(U_C29__STATES_FORM, currentUC29_StatesForm);
		request.getSession().setAttribute(U_C29__STATES_FORM, currentUC29_StatesForm);
		
	}
	
				
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("UC29_StatesController [ ");
			strBToS.append(ALL_STATES);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(allStates);
			strBToS.append(END_TO_STRING_DELIMITER);
					// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(uC29_StatesValidator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsUC29_StatesController!=null); 
		return strBToS.toString();
	}
}
