--<ScriptOptions statementTerminator=";"/>

ALTER TABLE team56 DROP CONSTRAINT team56_state_fkc;

ALTER TABLE player56 DROP CONSTRAINT player56_team56_fkc;

ALTER TABLE team56 DROP CONSTRAINT team56_pkey;

ALTER TABLE state56 DROP CONSTRAINT state56_pkey;

ALTER TABLE player56 DROP CONSTRAINT player56_pkey;

DROP TABLE player56;

DROP TABLE state56;

DROP TABLE team56;

CREATE TABLE player56 (
		id INT8 NOT NULL,
		version INT4 NOT NULL,
		last_name VARCHAR(1024),
		first_name VARCHAR(1024) NOT NULL,
		estimated_value FLOAT4,
		date_of_birth DATE,
		height INT4,
		weight INT4,
		rookie BOOL,
		team56_fk INT8
	);

CREATE TABLE state56 (
		code VARCHAR(1024) NOT NULL,
		version INT4 NOT NULL,
		name VARCHAR(1024) NOT NULL
	);

CREATE TABLE team56 (
		id INT8 NOT NULL,
		version INT4 NOT NULL,
		city VARCHAR(1024),
		state_fk VARCHAR(1024) NOT NULL,
		budget INT4,
		name VARCHAR(1024)
	);
	
INSERT INTO player56 VALUES (201, 0, 'Frank', 'Oliver', 0.11, '1968-10-31', 228, 116, false, 1);
INSERT INTO player56 VALUES (202, 0, 'Cole', 'Elbert', 3.95, '1965-04-07', 171, 100, false, 2);
INSERT INTO player56 VALUES (204, 0, 'Dawson', 'Ricky', 29.299999, '1990-03-03', 168, 115, true, 4);
INSERT INTO player56 VALUES (211, 0, 'Barnett', 'Keith', 5.8099999, '1976-11-10', 190, 66, false, 2);
INSERT INTO player56 VALUES (203, 0, 'Dennis', 'Abel', 30.51, '1980-04-16', 208, 105, false, 3);
INSERT INTO player56 VALUES (205, 0, 'Russell', 'Earl', 44.73, '1981-12-15', 234, 60, false, 5);
INSERT INTO player56 VALUES (206, 0, 'Garner', 'Nick', 47.84, '1992-07-27', 221, 87, true, 6);
INSERT INTO player56 VALUES (207, 0, 'Ramos', 'Howard', 10.76, '1970-08-25', 199, 107, false, 7);
INSERT INTO player56 VALUES (208, 0, 'Meyer', 'Allan', 33.669998, '1969-02-14', 229, 114, false, 8);
INSERT INTO player56 VALUES (209, 0, 'Pope', 'Ruben', 44.66, '1985-05-01', 201, 86, false, 9);
INSERT INTO player56 VALUES (210, 0, 'Cross', 'Lester', 30.98, '1976-02-21', 214, 69, false, 10);
INSERT INTO player56 VALUES (212, 0, 'Tate', 'Ivan', 48.490002, '1972-02-08', 175, 143, false, 1);
INSERT INTO player56 VALUES (213, 0, 'Wood', 'Tommy', 13.93, '1992-01-20', 173, 77, true, 3);
INSERT INTO player56 VALUES (214, 0, 'Nash', 'Sylvester', 37.73, '1965-07-10', 184, 68, false, 4);
INSERT INTO player56 VALUES (215, 0, 'Hodges', 'Michael', 25.35, '1988-04-01', 204, 158, false, 5);
INSERT INTO player56 VALUES (216, 0, 'Grant', 'Jerome', 28.940001, '1982-06-28', 205, 105, false, 6);
INSERT INTO player56 VALUES (217, 0, 'Carpenter', 'Woodrow', 41.759998, '1987-08-08', 224, 69, false, 7);
INSERT INTO player56 VALUES (218, 0, 'Robbins', 'Roderick', 12.04, '1975-07-29', 164, 115, false, 8);
INSERT INTO player56 VALUES (219, 0, 'Chavez', 'Christian', 37.880001, '1983-05-17', 209, 97, false, 9);
INSERT INTO player56 VALUES (220, 0, 'Conner', 'Cecil', 39.709999, '1978-06-29', 170, 160, false, 10);
INSERT INTO player56 VALUES (401, 0, 'Nash', 'Gregg', 6.8600001, '1972-01-11', 215, 147, false, 1);
INSERT INTO player56 VALUES (402, 0, 'Howell', 'Floyd', 25.709999, '1979-08-25', 192, 155, false, 1);
INSERT INTO player56 VALUES (403, 0, 'Klein', 'Simon', 22.24, '1987-01-21', 203, 63, false, 1);
INSERT INTO player56 VALUES (404, 0, 'Montgomery', 'Blake', 37.470001, '1992-11-16', 205, 113, true, 1);
INSERT INTO player56 VALUES (405, 0, 'Burns', 'Doyle', 39.130001, '1990-02-23', 225, 153, true, 1);
INSERT INTO player56 VALUES (406, 0, 'Rowe', 'Troy', 26.82, '1973-02-12', 211, 94, false, 1);
INSERT INTO player56 VALUES (407, 0, 'Jimenez', 'Jesus', 17.73, '1985-09-01', 159, 133, false, 1);
INSERT INTO player56 VALUES (408, 0, 'Guerrero', 'Ross', 13.68, '1992-01-26', 171, 136, true, 1);
INSERT INTO player56 VALUES (409, 0, 'Boyd', 'Eddie', 21.719999, '1967-10-05', 232, 118, false, 1);
INSERT INTO player56 VALUES (410, 0, 'Peterson', 'Luther', 1.04, '1985-06-09', 181, 83, false, 1);
INSERT INTO player56 VALUES (411, 0, 'Vargas', 'Daniel', 30.43, '1979-04-21', 155, 117, false, 2);
INSERT INTO player56 VALUES (412, 0, 'Kelly', 'Freddie', 34.400002, '1967-03-22', 196, 81, false, 2);
INSERT INTO player56 VALUES (413, 0, 'Farmer', 'Sam', 37.580002, '1987-11-29', 201, 98, false, 2);
INSERT INTO player56 VALUES (414, 0, 'Hampton', 'Randal', 31.219999, '1967-12-01', 202, 121, false, 2);
INSERT INTO player56 VALUES (415, 0, 'Olson', 'Felipe', 8.0600004, '1976-09-28', 189, 129, false, 2);
INSERT INTO player56 VALUES (416, 0, 'Christensen', 'Gary', 21.52, '1974-03-13', 170, 145, false, 2);
INSERT INTO player56 VALUES (417, 0, 'Mullins', 'Aaron', 8.54, '1972-01-15', 239, 86, false, 2);
INSERT INTO player56 VALUES (418, 0, 'Moore', 'Miguel', 9.4799995, '1967-03-26', 173, 67, false, 2);
INSERT INTO player56 VALUES (419, 0, 'Adkins', 'Bruce', 43.720001, '1986-05-24', 209, 150, false, 2);
INSERT INTO player56 VALUES (420, 0, 'Lawrence', 'Felipe', 33.66, '1981-04-19', 219, 150, false, 3);
INSERT INTO player56 VALUES (421, 0, 'Davis', 'Nelson', 20.84, '1978-05-16', 171, 119, false, 3);
INSERT INTO player56 VALUES (422, 0, 'Johnston', 'Jeremiah', 14.57, '1983-12-15', 200, 149, false, 3);
INSERT INTO player56 VALUES (423, 0, 'Wilkins', 'Enrique', 47.740002, '1967-01-21', 240, 75, false, 3);
INSERT INTO player56 VALUES (424, 0, 'Jimenez', 'Gerald', 29.26, '1970-01-22', 166, 63, false, 3);
INSERT INTO player56 VALUES (425, 0, 'Simon', 'Michael', 43.950001, '1975-12-23', 222, 118, false, 3);

INSERT INTO state56 VALUES ('AK', 0, 'Alaska');
INSERT INTO state56 VALUES ('AL', 0, 'Alabama');
INSERT INTO state56 VALUES ('AR', 0, 'Arkansas');
INSERT INTO state56 VALUES ('AZ', 0, 'Arizona');
INSERT INTO state56 VALUES ('CA', 0, 'California');
INSERT INTO state56 VALUES ('CO', 0, 'Colorado');
INSERT INTO state56 VALUES ('CT', 0, 'Connecticut');
INSERT INTO state56 VALUES ('DE', 0, 'Delaware');
INSERT INTO state56 VALUES ('FL', 0, 'Florida');
INSERT INTO state56 VALUES ('GA', 0, 'Georgia');
INSERT INTO state56 VALUES ('HI', 0, 'Hawaii');
INSERT INTO state56 VALUES ('IA', 0, 'Iowa');
INSERT INTO state56 VALUES ('ID', 0, 'Idaho');
INSERT INTO state56 VALUES ('IL', 0, 'Illinois');
INSERT INTO state56 VALUES ('IN', 0, 'Indiana');

INSERT INTO team56 VALUES (1, 0, 'Charlotte', 'AK', 1391371, 'Tigers');
INSERT INTO team56 VALUES (2, 0, 'Kansas City', 'AL', 9274552, 'Bears');
INSERT INTO team56 VALUES (3, 0, 'Detroit', 'AR', 4496712, 'Chargers');
INSERT INTO team56 VALUES (4, 0, 'Detroit', 'AK', 5019847, 'Hawks');
INSERT INTO team56 VALUES (5, 0, 'Omaha', 'AR', 2988752, 'Braves');
INSERT INTO team56 VALUES (6, 0, 'Raleigh', 'CA', 4565410, 'Eagles');
INSERT INTO team56 VALUES (7, 0, 'Oakland', 'CO', 5521097, 'Cardinals');
INSERT INTO team56 VALUES (8, 0, 'Kansas City', 'CT', 5321865, 'Red Devils');
INSERT INTO team56 VALUES (9, 0, 'Fresno', 'DE', 6190468, 'Mustangs');
INSERT INTO team56 VALUES (10, 0, 'Sacramento', 'FL', 5890145, 'Wolverines');


ALTER TABLE team56 ADD CONSTRAINT team56_pkey PRIMARY KEY (id);

ALTER TABLE state56 ADD CONSTRAINT state56_pkey PRIMARY KEY (code);

ALTER TABLE player56 ADD CONSTRAINT player56_pkey PRIMARY KEY (id);

ALTER TABLE team56 ADD CONSTRAINT team56_state_fkc FOREIGN KEY (state_fk)
	REFERENCES state56 (code);

ALTER TABLE player56 ADD CONSTRAINT player56_team56_fkc FOREIGN KEY (team56_fk)
	REFERENCES team56 (id);

