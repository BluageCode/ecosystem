--<ScriptOptions statementTerminator=";"/>

ALTER TABLE player45 DROP CONSTRAINT player45_pkey;

DROP TABLE player45;

CREATE TABLE player45 (
		id INT8 NOT NULL,
		version INT4 NOT NULL,
		first_name VARCHAR(30) NOT NULL,
		last_name VARCHAR(30) NOT NULL,
		date_of_birth DATE,
		estimated_value FLOAT4,
		weight INT4,
		rookie BOOL,
		height INT4
	);
	
INSERT INTO player45 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, rookie, height) VALUES (201, 0, 'Oliver', 'Frank', '1968-10-31', 0.11, 116, false, 228);
INSERT INTO player45 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, rookie, height) VALUES (202, 0, 'Elbert', 'Cole', '1965-04-07', 3.95, 100, false, 230);
INSERT INTO player45 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, rookie, height) VALUES (203, 0, 'Abel', 'Dennis', '1980-04-16', 30.51, 131, false, 233);
INSERT INTO player45 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, rookie, height) VALUES (204, 0, 'Ricky', 'Dawson', '1990-03-03', 29.299999, 115, true, 233);
INSERT INTO player45 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, rookie, height) VALUES (205, 0, 'Earl', 'Russell', '1981-12-15', 44.73, 62, false, 228);
INSERT INTO player45 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, rookie, height) VALUES (207, 0, 'Howard', 'Ramos', '1970-08-25', 10.76, 107, false, 228);
INSERT INTO player45 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, rookie, height) VALUES (208, 0, 'Allan', 'Meyer', '1969-02-14', 33.669998, 114, false, 233);
INSERT INTO player45 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, rookie, height) VALUES (209, 0, 'Ruben', 'Pope', '1985-05-01', 44.66, 86, false, 228);
INSERT INTO player45 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, rookie, height) VALUES (210, 0, 'Lester', 'Cross', '1976-02-21', 30.98, 69, false, 200);
INSERT INTO player45 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, rookie, height) VALUES (211, 0, 'Keith', 'Barnett', '1976-11-10', 5.8099999, 66, false, 230);
INSERT INTO player45 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, rookie, height) VALUES (212, 0, 'Ivan', 'Tate', '1972-02-08', 48.490002, 143, false, 228);
INSERT INTO player45 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, rookie, height) VALUES (215, 0, 'Michael', 'Hodges', '1988-04-01', 25.35, 158, false, 200);
INSERT INTO player45 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, rookie, height) VALUES (216, 0, 'Jerome', 'Grant', '1982-06-28', 28.940001, 131, false, 200);
INSERT INTO player45 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, rookie, height) VALUES (217, 0, 'Woodrow', 'Carpenter', '1987-08-08', 41.759998, 69, false, 230);
INSERT INTO player45 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, rookie, height) VALUES (218, 0, 'Roderick', 'Robbins', '1975-07-29', 12.04, 115, false, 233);
INSERT INTO player45 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, rookie, height) VALUES (219, 0, 'Christian', 'Chavez', '1983-05-17', 37.880001, 97, false, 230);
INSERT INTO player45 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, rookie, height) VALUES (220, 0, 'Cecil', 'Conner', '1978-06-29', 39.709999, 162, false, 230);
INSERT INTO player45 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, rookie, height) VALUES (221, 0, 'Stephen', 'Lambert', '1977-03-14', 18.9, 107, false, 233);
INSERT INTO player45 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, rookie, height) VALUES (222, 0, 'Gustavo', 'Romero', '1978-02-10', 5.0300002, 130, false, 230);
INSERT INTO player45 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, rookie, height) VALUES (223, 0, 'Salvatore', 'Robertson', '1979-12-22', 36.169998, 156, false, 228);
INSERT INTO player45 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, rookie, height) VALUES (224, 0, 'Guadalupe', 'Banks', '1986-07-03', 30.209999, 111, false, 228);
INSERT INTO player45 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, rookie, height) VALUES (225, 0, 'Todd', 'Gilbert', '1976-03-13', 0.41999999, 115, false, 200);
INSERT INTO player45 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, rookie, height) VALUES (226, 0, 'Ralph', 'Castillo', '1975-07-29', 36.130001, 115, false, 230);
INSERT INTO player45 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, rookie, height) VALUES (227, 0, 'Gerardo', 'George', '1982-03-17', 11.32, 151, false, 200);
INSERT INTO player45 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, rookie, height) VALUES (228, 0, 'Vernon', 'Lawson', '1974-09-13', 22.48, 146, false, 233);
INSERT INTO player45 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, rookie, height) VALUES (229, 0, 'Marshall', 'Soto', '1977-10-04', 35.629997, 82, false, 230);
INSERT INTO player45 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, rookie, height) VALUES (230, 0, 'Jim', 'Riley', '1971-10-20', 43.98, 104, false, 228);
INSERT INTO player45 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, rookie, height) VALUES (231, 0, 'Matthew', 'Fitzgerald', '1982-08-09', 33.610001, 145, false, 230);
INSERT INTO player45 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, rookie, height) VALUES (232, 0, 'Stuart', 'Ford', '1983-09-22', 2.96, 86, false, 200);
INSERT INTO player45 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, rookie, height) VALUES (233, 0, 'Timmy', 'Miller', '1972-08-07', 35.169998, 89, false, 233);
INSERT INTO player45 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, rookie, height) VALUES (234, 0, 'Antonio', 'Steele', '1984-03-15', 41.650002, 62, false, 200);
INSERT INTO player45 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, rookie, height) VALUES (236, 0, 'Jordan', 'Hayes', '1989-07-11', 44.419998, 113, false, 233);
INSERT INTO player45 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, rookie, height) VALUES (237, 0, 'Milton', 'White', '1970-12-20', 25.4, 132, false, 200);
INSERT INTO player45 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, rookie, height) VALUES (239, 0, 'Ramon', 'Crawford', '1974-10-06', 38.43, 106, false, 228);
INSERT INTO player45 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, rookie, height) VALUES (241, 0, 'Raffy', 'Carson', '1966-09-13', 13.62, 142, false, 230);
INSERT INTO player45 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, rookie, height) VALUES (242, 0, 'Alfonso', 'Harrison', '1981-07-28', 47.43, 147, false, 230);
INSERT INTO player45 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, rookie, height) VALUES (243, 0, 'Salvador', 'Watson', '1984-10-02', 49.900002, 79, false, 228);
INSERT INTO player45 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, rookie, height) VALUES (244, 0, 'Nelson', 'Cook', '1966-03-01', 3.950001, 129, false, 200);
INSERT INTO player45 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, rookie, height) VALUES (245, 0, 'Vernon', 'Davis', '1971-10-19', 2.24, 108, false, 228);
INSERT INTO player45 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, rookie, height) VALUES (246, 0, 'Earnest', 'Maxwell', '1986-01-20', 42.16, 135, false, 228);
INSERT INTO player45 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, rookie, height) VALUES (247, 0, 'Dwayne', 'Stevenson', '1968-10-26', 43.02, 101, false, 230);
INSERT INTO player45 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, rookie, height) VALUES (248, 0, 'Archie', 'Roberson', '1974-08-11', 7.1500001, 101, false, 230);
INSERT INTO player45 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, rookie, height) VALUES (249, 0, 'Darryl', 'Norris', '1965-01-22', 47.700001, 121, false, 230);
INSERT INTO player45 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, rookie, height) VALUES (250, 0, 'Dean', 'Stokes', '1974-10-15', 49.09, 150, false, 230);
INSERT INTO player45 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, rookie, height) VALUES (251, 0, 'Allen', 'Freeman', '1969-09-03', 33.279999, 99, false, 228);
INSERT INTO player45 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, rookie, height) VALUES (252, 0, 'Brett', 'Weaver', '1971-10-04', 14.83, 75, false, 230);
INSERT INTO player45 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, rookie, height) VALUES (253, 0, 'Jerome', 'Griffin', '1987-12-19', 9.1499996, 146, false, 230);
INSERT INTO player45 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, rookie, height) VALUES (254, 0, 'John', 'Lynch', '1980-09-06', 28.35, 123, false, 230);
INSERT INTO player45 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, rookie, height) VALUES (256, 0, 'Jody', 'Elliott', '1972-05-07', 45.290001, 66, false, 153);
INSERT INTO player45 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, rookie, height) VALUES (257, 0, 'Willis', 'Francis', '1977-06-21', 23.91, 77, false, 153);
INSERT INTO player45 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, rookie, height) VALUES (259, 0, 'Stanley', 'Wolfe', '1985-12-14', 0.50999999, 129, false, 211);
INSERT INTO player45 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, rookie, height) VALUES (260, 0, 'Don', 'Griffith', '1975-10-31', 24.25, 101, false, 211);
INSERT INTO player45 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, rookie, height) VALUES (262, 0, 'Edwin', 'Reese', '1989-06-20', 10.7, 103, false, 153);
INSERT INTO player45 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, rookie, height) VALUES (263, 0, 'Salvador', 'Bryant', '1965-08-24', 35.830002, 106, false, 153);
INSERT INTO player45 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, rookie, height) VALUES (265, 0, 'Pablo', 'Rhodes', '1982-06-17', 31.6, 108, false, 153);
INSERT INTO player45 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, rookie, height) VALUES (266, 0, 'Dana', 'Hamilton', '1983-05-30', 33.599998, 97, false, 211);
INSERT INTO player45 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, rookie, height) VALUES (267, 0, 'Cecil', 'Wells', '1989-12-26', 38.610001, 122, false, 211);
INSERT INTO player45 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, rookie, height) VALUES (268, 0, 'Earl', 'Barrett', '1982-09-15', 45.619999, 138, false, 211);
INSERT INTO player45 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, rookie, height) VALUES (269, 0, 'Paul', 'Page', '1969-12-13', 25.809999, 125, false, 153);
INSERT INTO player45 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, rookie, height) VALUES (270, 0, 'Freddie', 'Williamson', '1976-10-10', 3.8, 131, false, 153);
INSERT INTO player45 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, rookie, height) VALUES (271, 0, 'Gilbert', 'Johnston', '1981-12-30', 40.259998, 61, false, 211);
INSERT INTO player45 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, rookie, height) VALUES (272, 0, 'Darnell', 'Williamson', '1978-06-15', 45.080002, 162, false, 153);
INSERT INTO player45 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, rookie, height) VALUES (273, 0, 'Joshua', 'Hardy', '1975-04-09', 9.4499998, 150, false, 211);
INSERT INTO player45 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, rookie, height) VALUES (274, 0, 'Dale', 'Colon', '1984-09-28', 9.25, 68, false, 211);
INSERT INTO player45 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, rookie, height) VALUES (275, 0, 'Clarence', 'Johnson', '1972-05-25', 2.02, 128, false, 153);
INSERT INTO player45 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, rookie, height) VALUES (276, 0, 'Rolando', 'Waters', '1966-02-26', 7.0999999, 67, false, 153);
INSERT INTO player45 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, rookie, height) VALUES (277, 0, 'Dominic', 'Martinez', '1967-03-04', 1.34, 127, false, 211);
INSERT INTO player45 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, rookie, height) VALUES (278, 0, 'Ricardo', 'Oliver', '1968-05-02', 20.5, 147, false, 153);
INSERT INTO player45 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, rookie, height) VALUES (279, 0, 'Edward', 'Hogan', '1986-02-02', 23.24, 75, false, 197);
INSERT INTO player45 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, rookie, height) VALUES (280, 0, 'Gerald', 'Sanders', '1976-03-03', 21.68, 127, false, 153);
INSERT INTO player45 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, rookie, height) VALUES (281, 0, 'Noel', 'Oliver', '1984-11-24', 45.18, 112, false, 211);
INSERT INTO player45 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, rookie, height) VALUES (282, 0, 'Preston', 'Medina', '1974-02-15', 0.33000001, 95, false, 153);
INSERT INTO player45 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, rookie, height) VALUES (283, 0, 'Abraham', 'Drake', '1985-11-27', 21.77, 94, false, 211);
INSERT INTO player45 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, rookie, height) VALUES (284, 0, 'Julian', 'Gibbs', '1985-07-01', 23.34, 80, false, 197);
INSERT INTO player45 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, rookie, height) VALUES (285, 0, 'Juan', 'Dawson', '1987-01-14', 9.9799995, 75, false, 197);
INSERT INTO player45 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, rookie, height) VALUES (286, 0, 'Marvin', 'Mitchell', '1981-05-27', 7.9400001, 91, false, 197);
INSERT INTO player45 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, rookie, height) VALUES (287, 0, 'Owen', 'Little', '1981-10-27', 45.869999, 135, false, 197);
INSERT INTO player45 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, rookie, height) VALUES (288, 0, 'Irvin', 'Lee', '1988-02-25', 10.61, 88, false, 153);
INSERT INTO player45 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, rookie, height) VALUES (289, 0, 'Wayne', 'Rose', '1981-10-28', 20.41, 115, false, 153);
INSERT INTO player45 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, rookie, height) VALUES (290, 0, 'Lucas', 'Williamson', '1983-06-29', 38.362, 73, false, 153);
INSERT INTO player45 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, rookie, height) VALUES (291, 0, 'Walter', 'Reyes', '1984-12-09', 46.150002, 104, false, 197);
INSERT INTO player45 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, rookie, height) VALUES (292, 0, 'Kirk', 'Cox', '1973-05-10', 26.58, 62, false, 153);
INSERT INTO player45 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, rookie, height) VALUES (293, 0, 'Arthur', 'Payne', '1981-11-20', 30.862001, 66, false, 197);
INSERT INTO player45 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, rookie, height) VALUES (294, 0, 'Leroy', 'Moore', '1987-07-26', 45.799999, 89, false, 197);
INSERT INTO player45 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, rookie, height) VALUES (295, 0, 'Milton', 'Johnson', '1969-07-02', 22.219999, 68, false, 153);
INSERT INTO player45 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, rookie, height) VALUES (296, 0, 'Sammy', 'Mendez', '1986-07-01', 2.27, 96, false, 153);
INSERT INTO player45 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, rookie, height) VALUES (297, 0, 'Rafael', 'Steele', '1968-11-25', 15.48, 66, false, 197);
INSERT INTO player45 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, rookie, height) VALUES (298, 0, 'Bob', 'Jacobs', '1988-05-21', 13.23, 72, false, 197);
INSERT INTO player45 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, rookie, height) VALUES (299, 0, 'Jared', 'Stevenson', '1974-05-03', 12.11, 125, false, 153);
INSERT INTO player45 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, rookie, height) VALUES (300, 0, 'Shannon', 'Barton', '1980-03-10', 23.809999, 77, false, 197);



ALTER TABLE player45 ADD CONSTRAINT player45_pkey PRIMARY KEY (id);

