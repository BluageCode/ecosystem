// rd_lib 10407

//Function used by component for click on a hidden button
function clickOn (idLink){
	jQuery("a[id*='"+idLink+"']").click();
}

// DO NOT EDIT PAST THIS POINT

// sync-include version 1.0.1 110419
// plugins.jquery.com/project/sync-include
jQuery.extend({includePath:'',include:function(file){var files=typeof file=="string"?[file]:file;var arlen=files.length;for(var i=0;i<arlen;i++){var name=files[i].replace(/^\s|\s$/g,"");var att=name.split('.');var ext=att[att.length-1].toLowerCase();var isCSS=ext=="css";var tag=isCSS?"link":"script";var attr=isCSS?" type='text/css' rel='stylesheet' ":" language='javascript' type='text/javascript' ";var link=(isCSS?"href":"src")+"='"+jQuery.includePath+name+"'";if(jQuery(tag+"["+link+"]").length==0)document.write("\n<"+tag+attr+link+"></"+tag+">");}}});
// end sync-include
// smart path detector v 1.0.1 110526
// Include in rd_setup.js // rd_inc : name of the script directory, generally "_include" // var rd_inc ="_include";
if(typeof rd_inc=='undefined'){var rd_inc='_include';};var rd_incpath=jQuery('script[src*="'+rd_inc+'/jquery/jquery.js"]').attr('src'); rd_dir=rd_incpath.substr(0,rd_incpath.indexOf(rd_inc));
// end smart path detector
jQuery.includePath  = rd_dir+'_include/jquery/';
jQuery.include(['jquery-ui-1.8.custom.min.js','cupertino/jquery-ui-1.8.custom.css','jquery.layout.min.js', 'jquery.cookie.js','jquery.treeview.min.js','../syntaxhighlighter/shCore.js','jquery.countdown.min.js']);

// doc ready rd_jghost version 1.0.0 101100
var rd_css="not_checked";jQuery(document).ready(function(){if(jQuery('#rd_ghost').css('display')=='block'){rd_css='deactivated';var rd_css_opt_0={'position':'absolute','top':'2px','right':'2px','width':'150px','z-index':'100','filter':'alpha(opacity=71)','background':'#000','-moz-opacity':'0.71','opacity':'0.71'};var rd_css_opt_1={'display':'block','padding':'5px','font':'10px Tahoma, Geneva, sans-serif','color':'#FFF','text-align':'center'};jQuery('#rd_ghost').css(rd_css_opt_0);jQuery('#rd_ghost small').css(rd_css_opt_1);}else{rd_css='activated';}; jQuery('div#rd_jghost small').css({'display':'block'}).html('_ javascript error');});
// Dev : $('div#rd_jghost small').text('_ Javascript error');} Prod $('div#rd_jghost small').html('<img src="../_style/rd_loading.gif" width="19" height="19" alt="" />&nbsp;&nbsp;Chargement...');}
// Place the following at the end of the page
// doc ready rd_jghost version 1.0.0_l1 91200 
// $(document).ready(function(){$('div#rd_jghost').remove();});
// end doc ready rd_jghost

// doc ready
jQuery(document).ready(function(){
	
//set urls to images with rd_dir
document.getElementById("img_bluage").src = rd_dir + "_style/bluage_xs.png";
document.getElementById("img_model2code").src = rd_dir + "_style/model2code_xs.png";
document.getElementById("img_logo_bluage_4wiki").src = rd_dir + "_style/logo_bluage_4wiki.png";

//6 hour countdown
var today = new Date();
if(0 <= today.getHours() && today.getHours() < 6){
	today.setHours(6,0,0,0);
}else if(6 <= today.getHours() && today.getHours() < 12){
	today.setHours(12,0,0,0);
}else if(12 <= today.getHours() && today.getHours() < 18){
	today.setHours(18,0,0,0);
}else if(18 <= today.getHours() && today.getHours() <= 23){
	today.setHours(0,0,0,0);
	today.setDate(today.getDate()+1);
}
jQuery('.countdown').countdown({until: today, format: 'HMS', layout: 'Reset database in <b>{hn} {hl}, {mn} {ml}, {sn} {sl}</b>'});
	
// UI Layout
layout = jQuery('div#shape').layout({
	applyDefaultStyles:true, center__onresize:'innerLayout.resizeAll',
	north__resizable : false, north__closable : false, north__size:'auto', north__spacing_open:1,
	south__resizable : false, south__closable : false, south__size:50, south__spacing_open:1, 
	west__size:'auto', west__minSize:300
});
innerLayout = jQuery('div.ui-layout-center').layout({
	applyDefaultStyles: true, 
	center__paneSelector:'div.east-north', 
	south__paneSelector:'div.east-center', 
	center__size:'auto', south__minSize:200
});


//***************************************************************************************
/*// Layout cookies

//valeurs récupérées dans le cookie
var resizeBarId2Px = jQuery.cookie('centerLayoutCookie');

if(resizeBarId2Px !=null )
	{
		if(resizeBarId2Px.length <= 5)
			var resizeBarId2Value = resizeBarId2Px.substring(0,3);
		else
			var resizeBarId2Value = resizeBarId2Px.substring(0,4);
		
		//var eastNorth = 770 - parseInt(resizeBarId2Value)-118;
		//var eastCenter = 770 - eastNorth -150;
		
		var bar = parseInt(resizeBarId2Value) ;
		
		var eastCenter = bar -3.8;
		var eastNorth = 95.5 - bar;
		
		//Conversion en string
		
		var eastCenterPx = eastCenter + "%";
		var eastNorthPx = eastNorth + "%";
		var resizeBarId2P = resizeBarId2Px + "%"
	}


//si cookie existe ou non
if(jQuery.cookie('centerLayoutCookie') == null)
	{
		jQuery.cookie('centerLayoutCookie', document.getElementById('resizeBarId2').style.bottom,{path: '/' });
	}
else
	{
		//attribution des nouvelles valeurs en fonction de celles calculées précedemment
		if (jQuery.cookie('centerLayoutCookie') != document.getElementById('resizeBarId2').style.bottom){
			jQuery('#resizeBarId2').css('bottom', resizeBarId2P); 
			jQuery('#resizeBarId2').css('width', "98.6%"); 
			jQuery('.east-center').css('height', eastCenterPx );
			jQuery('.east-north').css('height', eastNorthPx );
		}
	}
jQuery.removeCookie('centerLayoutCookie');
// au déchargement de la page
jQuery(window).unload(function(){
	// affectation de la nouvelle valeur dans le cookie
	var positionPx = document.getElementById('resizeBarId2').style.bottom;
	
	if(positionPx.length > 2){
		if(positionPx.length >= 4){
			var positionValue = positionPx.substring(0,3);
			var position = ((parseInt(positionValue)*100)/770)+"%"; 
			var positionFinale = position.substring(0,2);
		}
		else 
			var positionFinale = positionPx.substring(0,2);
		  
	}
	
	jQuery.cookie('centerLayoutCookie', positionFinale,{path: '/' });
});
*/
//***************************************************************************************
// Tabs
jQuery('div.east-center').tabs({
    load:function(event, ui) {
                   //When no html page
                   if(ui.panel.innerHTML == ''){
                                   ui.panel.innerHTML = 'No specific documentation.';
                   }
                   //Highlight synthax for mockup, workflow and generated code
                   if(ui.panel.id == 'ui-tabs-3' || ui.panel.id == 'ui-tabs-4' || ui.panel.id == 'ui-tabs-5'){
                                   SyntaxHighlighter.highlight();
                   }
    },
    remote: true,
    cookie: { expires: 30 }
});


//***************************************************************************************
// Treeview

jQuery("#rd_jtree").treeview({collapsed: true, animated: "medium", control:"#rd_jtree_key", persist:"cookie"});

// http://devkick.com/blog/parsing-strings-with-jquery/
jQuery('.rd_jescapehtml').each(function() { jQuery(this).html( jQuery(this).html().replace(/&/g,'&amp;').replace(/</g,'&lt;').replace(/>/g,'&gt;').replace(/"/g,'&quot;'));});
var regex_striptags = /<("[^"]*"|'[^']*'|[^'">])*>/gi;
jQuery('.rd_jstriptags').each(function() { jQuery(this).html(jQuery(this).html().replace(regex_striptags,""));});
var regex_clickurl = /((ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?)/gi;
jQuery('.rd_jclickurl').each(function() { jQuery(this).html(jQuery(this).html().replace(regex_clickurl,"<a href=\"$1\">$1</a>"));});
});
// end doc ready

// personal
// doc ready personal

jQuery(document).ready(function(){
	
	
	jQuery('#search').keypress(function(event) {
	    if (event.keyCode == 13) {
	        event.preventDefault();
	    }
	});
	
	jQuery("input[id*='search']").live("keyup",function(e){
		var search = jQuery("#search").attr("value");
		
		if(search!= ""){
			jQuery("#valeur", document.body).each(function(){
				jQuery(this).hide("fast");
			});
			jQuery("#rd_jtree").hide("fast");
			jQuery("#collapse").hide("fast");
	    	
			jQuery("#rd_jtree").before("<p id='valeur'><b>Results for '" + (search) + "'</b>:</p>");
	    	
			jQuery("#rd_jtree li a").each(function(){
	    			
    			title = jQuery(this).text();
    	
	    		var reg_casse = new RegExp(""+search+"","gi");
	    		if(title.match(reg_casse)){
	    			if(jQuery(this).attr("onclick")!=null){
		    			var reg = new RegExp("[{}]+","g");
		    			var chaine = "<p id='valeur'><a href='#'>" + jQuery(this).attr("onclick") + "</a></p>";
		    			var tab = chaine.split(reg);
		    			var reg_gui = new RegExp('(")',"g");
		    			var lien = tab[1].replace(reg_gui,"'");
		    			jQuery('#rd_jtree').before('<p id="valeur"><a href="#" onclick="'+lien+'">' + jQuery(this).html() + '</a></p>');
	    			}
	    		} 
			});
	    	
		} else {
			jQuery("#rd_jtree").show("fast");
			jQuery("#collapse").show("fast");
			jQuery("#valeur", document.body).each(function(){
				jQuery(this).hide("fast");
			});
		}
	});

//***************************************************************************************
	
// soft reference 1.0.0 91000
// var rd_soft_location ="div#rd_siteinfo_l2"; var rd_soft_name ="/ XXX"; var rd_soft_version ="0.0"; var rd_soft_startdate ="2009";
if(rd_soft_name.length>0){var rd_date=new Date();var rd_year=rd_date.getFullYear();jQuery(rd_soft_location).append(' ');((rd_soft_startdate==parseInt(rd_soft_startdate))&&(rd_year>rd_soft_startdate))?jQuery(rd_soft_location).append(rd_soft_startdate+'-'+rd_year+' '):jQuery(rd_soft_location).append(rd_year+' ');(rd_soft_version.length>0)?jQuery(rd_soft_location).append(rd_soft_name+' '+rd_soft_version):jQuery(rd_soft_location).append(rd_soft_name);};

});
// end doc ready personal

// doc ready rd_jghost version 1.0.0_l1 91200 
jQuery(document).ready(function(){jQuery('div#rd_jghost').remove();});
// end doc ready rd_jghost