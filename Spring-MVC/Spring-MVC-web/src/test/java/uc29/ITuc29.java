package uc29;

import org.junit.Test;
import test.common.AbstractBADocTestCase;


public class ITuc29 extends AbstractBADocTestCase {
	
	@Override
	public  String getName(){
		return "UC-29";
	}
	
	@Test
	public void testITuc29() throws Exception {
		// On teste l'UC28 - Standard Features/Application/Navigation - Package Scope
		selenium.open("");
		selenium.click("link=Expand All");
		selenium.waitForPageToLoad("30000");
		selenium.click("link=UC29 - Global Scope");
		assertTrue(selenium.isElementPresent("//h2[contains(text(),'Standard Features > Application > Navigation > UC29 > Global Scope')]"));
		// On v�rifie la pr�sence des liens de la page.
		assertTrue(selenium.isElementPresent("//b/a[contains(@id,\"lnk_teams\")]"));
		assertTrue(selenium.isElementPresent("//b/a/u[contains(text(),'Teams')]"));
		assertTrue(selenium.isElementPresent("//b/a[contains(@id,'lnk_players')]"));
		assertTrue(selenium.isElementPresent("//b/a[contains(text(),'Players')]"));
		assertTrue(selenium.isElementPresent("//b/a[contains(@id,\"lnk_states\")]"));
		assertTrue(selenium.isElementPresent("//b/a[contains(text(),'States')]"));
		assertTrue(selenium.isElementPresent("//b/a[contains(@id,\"lnk_stadiums\")]"));
		assertTrue(selenium.isElementPresent("//b/a[contains(text(),'Stadiums')]"));
		// On v�rifie les donn�es du tableau Teams :
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/thead/tr[th[contains(text(),'Name')] and th[contains(text(),'City')]]"));
		verifyEquals("7", selenium.getXpathCount("//table[contains(@id,'tab_teams')]/tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),'Tigers')] and td/span[contains(text(),'Charlotte')]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),'Bears')] and td/span[contains(text(),'Kansas City')]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),'Chargers')] and td/span[contains(text(),'Detroit')]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),'Hawks')] and td/span[contains(text(),'Detroit')]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),'Braves')] and td/span[contains(text(),'Omaha')]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),'Eagles')] and td/span[contains(text(),'Raleigh')]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),'Cardinals')] and td/span[contains(text(),'Oakland')]]"));
		// On clique sur Players :
		selenium.click("//b/a[contains(@id,'lnk_players')]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie l'affichage de la page : 
		assertTrue(selenium.isElementPresent("//h2[contains(text(),'Standard Features > Application > Navigation > UC29 > Global Scope')]"));
		assertTrue(selenium.isElementPresent("//b/a[contains(@id,\"lnk_teams\")]"));
		assertTrue(selenium.isElementPresent("//b/a[contains(text(),'Teams')]"));
		assertTrue(selenium.isElementPresent("//b/a[contains(@id,'lnk_players')]"));
		assertTrue(selenium.isElementPresent("//b/a/u[contains(text(),'Players')]"));
		assertTrue(selenium.isElementPresent("//b/a[contains(@id,\"lnk_states\")]"));
		assertTrue(selenium.isElementPresent("//b/a[contains(text(),'States')]"));
		assertTrue(selenium.isElementPresent("//b/a[contains(@id,\"lnk_stadiums\")]"));
		assertTrue(selenium.isElementPresent("//b/a[contains(text(),'Stadiums')]"));
		// On v�rifie les donn�es du tableau Players :
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_Players')]/thead/tr[th[contains(text(),'Name')] and th[contains(text(),'Date of Birth')] and th[contains(text(),'Weight (kg)')] and th[contains(text(),'Height (cm)')]and th[contains(text(),'Estimated Value (M$)')]and th[contains(text(),'Rookie')]]"));
		verifyEquals("29", selenium.getXpathCount("//table[contains(@id,'tab_Players')]/tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_Players\")]//tbody//tr[td//span[contains(text(),\"Oliver\")] and td//span[contains(text(),\"Frank\")] and td/span[contains(text(),\"10/31/1968\")] and td//span[contains(text(),\"228\")] and td//span[contains(text(),\"116\")] and td//span[contains(text(),\"0.11\")] and td//input[not(@checked)]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_Players\")]//tbody//tr[td//span[contains(text(),\"Elbert\")] and td//span[contains(text(),\"Cole\")] and td/span[contains(text(),\"04/07/1965\")] and td//span[contains(text(),\"171\")] and td//span[contains(text(),\"100\")] and td//span[contains(text(),\"3.95\")] and td//input[not(@checked)]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_Players\")]//tbody//tr[td//span[contains(text(),\"Ricky\")] and td//span[contains(text(),\"Dawson\")] and td/span[contains(text(),\"03/03/1990\")] and td//span[contains(text(),\"168\")] and td//span[contains(text(),\"115\")] and td//span[contains(text(),\"29.3\")] and td//input[@checked]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_Players\")]//tbody//tr[td//span[contains(text(),\"Abel\")] and td//span[contains(text(),\"Dennis\")] and td/span[contains(text(),\"04/16/1980\")] and td//span[contains(text(),\"208\")] and td//span[contains(text(),\"105\")] and td//span[contains(text(),\"30.51\")] and td//input[not(@checked)]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_Players\")]//tbody//tr[td//span[contains(text(),\"Earl\")] and td//span[contains(text(),\"Russell\")] and td/span[contains(text(),\"12/15/1981\")] and td//span[contains(text(),\"234\")] and td//span[contains(text(),\"60\")] and td//span[contains(text(),\"44.73\")] and td//input[not(@checked)]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_Players\")]//tbody//tr[td//span[contains(text(),\"Nick\")] and td//span[contains(text(),\"Garner\")] and td/span[contains(text(),\"07/27/1992\")] and td//span[contains(text(),\"221\")] and td//span[contains(text(),\"87\")] and td//span[contains(text(),\"47.84\")] and td//input[@checked]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_Players\")]//tbody//tr[td//span[contains(text(),\"Howard\")] and td//span[contains(text(),\"Ramos\")] and td/span[contains(text(),\"08/25/1970\")] and td//span[contains(text(),\"199\")] and td//span[contains(text(),\"107\")] and td//span[contains(text(),\"10.76\")] and td//input[not(@checked)]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_Players\")]//tbody//tr[td//span[contains(text(),\"Allan\")] and td//span[contains(text(),\"Meyer\")] and td/span[contains(text(),\"02/14/1969\")] and td//span[contains(text(),\"229\")] and td//span[contains(text(),\"114\")] and td//span[contains(text(),\"33.67\")] and td//input[not(@checked)]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_Players\")]//tbody//tr[td//span[contains(text(),\"Ruben\")] and td//span[contains(text(),\"Pope\")] and td/span[contains(text(),\"05/01/1985\")] and td//span[contains(text(),\"201\")] and td//span[contains(text(),\"86\")] and td//span[contains(text(),\"44.66\")] and td//input[not(@checked)]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_Players\")]//tbody//tr[td//span[contains(text(),\"Lester\")] and td//span[contains(text(),\"Cross\")] and td/span[contains(text(),\"02/21/1976\")] and td//span[contains(text(),\"214\")] and td//span[contains(text(),\"69\")] and td//span[contains(text(),\"30.98\")] and td//input[not(@checked)]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_Players\")]//tbody//tr[td//span[contains(text(),\"Keith\")] and td//span[contains(text(),\"Barnett\")] and td/span[contains(text(),\"11/10/1976\")] and td//span[contains(text(),\"190\")] and td//span[contains(text(),\"66\")] and td//span[contains(text(),\"5.81\")] and td//input[not(@checked)]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_Players\")]//tbody//tr[td//span[contains(text(),\"Tommy\")] and td//span[contains(text(),\"Wood\")] and td/span[contains(text(),\"01/20/1992\")] and td//span[contains(text(),\"173\")] and td//span[contains(text(),\"77\")] and td//span[contains(text(),\"13.93\")] and td//input[@checked]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_Players\")]//tbody//tr[td//span[contains(text(),\"Sylvester\")] and td//span[contains(text(),\"Nash\")] and td/span[contains(text(),\"07/10/1965\")] and td//span[contains(text(),\"184\")] and td//span[contains(text(),\"68\")] and td//span[contains(text(),\"37.73\")] and td//input[not(@checked)]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_Players\")]//tbody//tr[td//span[contains(text(),\"Michael\")] and td//span[contains(text(),\"Hodges\")] and td/span[contains(text(),\"04/01/1988\")] and td//span[contains(text(),\"204\")] and td//span[contains(text(),\"158\")] and td//span[contains(text(),\"25.35\")] and td//input[not(@checked)]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_Players\")]//tbody//tr[td//span[contains(text(),\"Jerome\")] and td//span[contains(text(),\"Grant\")] and td/span[contains(text(),\"06/29/1982\")] and td//span[contains(text(),\"205\")] and td//span[contains(text(),\"105\")] and td//span[contains(text(),\"29.94\")] and td//input[not(@checked)]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_Players\")]//tbody//tr[td//span[contains(text(),\"Woodrow\")] and td//span[contains(text(),\"Carpenter\")] and td/span[contains(text(),\"08/08/1987\")] and td//span[contains(text(),\"224\")] and td//span[contains(text(),\"69\")] and td//span[contains(text(),\"41.76\")] and td//input[not(@checked)]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_Players\")]//tbody//tr[td//span[contains(text(),\"Roderick\")] and td//span[contains(text(),\"Robbins\")] and td/span[contains(text(),\"07/29/1975\")] and td//span[contains(text(),\"164\")] and td//span[contains(text(),\"115\")] and td//span[contains(text(),\"12.04\")] and td//input[not(@checked)]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_Players\")]//tbody//tr[td//span[contains(text(),\"Christian\")] and td//span[contains(text(),\"Chavez\")] and td/span[contains(text(),\"05/17/1983\")] and td//span[contains(text(),\"209\")] and td//span[contains(text(),\"97\")] and td//span[contains(text(),\"37.88\")] and td//input[not(@checked)]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_Players\")]//tbody//tr[td//span[contains(text(),\"Ivan\")] and td//span[contains(text(),\"Tate\")] and td/span[contains(text(),\"02/08/1972\")] and td//span[contains(text(),\"175\")] and td//span[contains(text(),\"143\")] and td//span[contains(text(),\"48.49\")] and td//input[not(@checked)]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_Players\")]//tbody//tr[td//span[contains(text(),\"Gregg\")] and td//span[contains(text(),\"Nash\")] and td/span[contains(text(),\"01/11/1972\")] and td//span[contains(text(),\"215\")] and td//span[contains(text(),\"147\")] and td//span[contains(text(),\"6.86\")] and td//input[not(@checked)]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_Players\")]//tbody//tr[td//span[contains(text(),\"Floyd\")] and td//span[contains(text(),\"Howell\")] and td/span[contains(text(),\"08/25/1979\")] and td//span[contains(text(),\"192\")] and td//span[contains(text(),\"155\")] and td//span[contains(text(),\"25.71\")] and td//input[not(@checked)]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_Players\")]//tbody//tr[td//span[contains(text(),\"Simon\")] and td//span[contains(text(),\"Klein\")] and td/span[contains(text(),\"01/21/1987\")] and td//span[contains(text(),\"203\")] and td//span[contains(text(),\"29\")] and td//span[contains(text(),\"22.24\")] and td//input[not(@checked)]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_Players\")]//tbody//tr[td//span[contains(text(),\"Blake\")] and td//span[contains(text(),\"Montgomery\")] and td/span[contains(text(),\"11/16/1992\")] and td//span[contains(text(),\"205\")] and td//span[contains(text(),\"113\")] and td//span[contains(text(),\"37.47\")] and td//input[@checked]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_Players\")]//tbody//tr[td//span[contains(text(),\"Doyle\")] and td//span[contains(text(),\"Burns\")] and td/span[contains(text(),\"02/23/1990\")] and td//span[contains(text(),\"225\")] and td//span[contains(text(),\"153\")] and td//span[contains(text(),\"39.13\")] and td//input[@checked]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_Players\")]//tbody//tr[td//span[contains(text(),\"Troy\")] and td//span[contains(text(),\"Rowe\")] and td/span[contains(text(),\"02/12/1973\")] and td//span[contains(text(),\"211\")] and td//span[contains(text(),\"94\")] and td//span[contains(text(),\"26.82\")] and td//input[not(@checked)]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_Players\")]//tbody//tr[td//span[contains(text(),\"Jesus\")] and td//span[contains(text(),\"Jimenez\")] and td/span[contains(text(),\"09/01/1985\")] and td//span[contains(text(),\"159\")] and td//span[contains(text(),\"133\")] and td//span[contains(text(),\"17.73\")] and td//input[not(@checked)]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_Players\")]//tbody//tr[td//span[contains(text(),\"Ross\")] and td//span[contains(text(),\"Guerrero\")] and td/span[contains(text(),\"01/26/1992\")] and td//span[contains(text(),\"171\")] and td//span[contains(text(),\"136\")] and td//span[contains(text(),\"13.68\")] and td//input[@checked]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_Players\")]//tbody//tr[td//span[contains(text(),\"Eddie\")] and td//span[contains(text(),\"Boyd\")] and td/span[contains(text(),\"10/05/1967\")] and td//span[contains(text(),\"232\")] and td//span[contains(text(),\"118\")] and td//span[contains(text(),\"21.72\")] and td//input[not(@checked)]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_Players\")]//tbody//tr[td//span[contains(text(),\"Luther\")] and td//span[contains(text(),\"Peterson\")] and td/span[contains(text(),\"06/09/1985\")] and td//span[contains(text(),\"181\")] and td//span[contains(text(),\"83\")] and td//span[contains(text(),\"1.04\")] and td//input[not(@checked)]]"));
		// On clique sur States :
		selenium.click("//b/a[contains(@id,'lnk_states')]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie l'affichage de la page : 
		assertTrue(selenium.isElementPresent("//h2[contains(text(),'Standard Features > Application > Navigation > UC29 > Global Scope')]"));
		assertTrue(selenium.isElementPresent("//b/a[contains(@id,\"lnk_teams\")]"));
		assertTrue(selenium.isElementPresent("//b/a[contains(text(),'Teams')]"));
		assertTrue(selenium.isElementPresent("//b/a[contains(@id,'lnk_players')]"));
		assertTrue(selenium.isElementPresent("//b/a[contains(text(),'Players')]"));
		assertTrue(selenium.isElementPresent("//b/a[contains(@id,\"lnk_states\")]"));
		assertTrue(selenium.isElementPresent("//b/a/u[contains(text(),'States')]"));
		assertTrue(selenium.isElementPresent("//b/a[contains(@id,\"lnk_stadiums\")]"));
		assertTrue(selenium.isElementPresent("//b/a[contains(text(),'Stadiums')]"));
		// On v�rifie les donn�es du tableau States :
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_states')]/thead/tr[th[contains(text(),'Code')] and th[contains(text(),'Name')]]"));
		verifyEquals("12", selenium.getXpathCount("//table[contains(@id,'tab_states')]/tbody//tr"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/span[contains(text(),\"AK\")] and td/span[contains(text(),\"Alaska\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/span[contains(text(),\"AL\")] and td/span[contains(text(),\"Alabama\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/span[contains(text(),\"AR\")] and td/span[contains(text(),\"Arkansas\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/span[contains(text(),\"AZ\")] and td/span[contains(text(),\"Arizona\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/span[contains(text(),\"CA\")] and td/span[contains(text(),\"California\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/span[contains(text(),\"CO\")] and td/span[contains(text(),\"Colorado\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/span[contains(text(),\"CT\")] and td/span[contains(text(),\"Connecticut\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/span[contains(text(),\"DE\")] and td/span[contains(text(),\"Delaware\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/span[contains(text(),\"FL\")] and td/span[contains(text(),\"Florida\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/span[contains(text(),\"GA\")] and td/span[contains(text(),\"Georgia\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/span[contains(text(),\"HI\")] and td/span[contains(text(),\"Hawaii\")]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/span[contains(text(),\"IA\")] and td/span[contains(text(),\"Iowa\")]]"));
		// On clique sur Stadiums :
		selenium.click("//b/a[contains(@id,'lnk_stadium')]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie l'affichage de la page : 
		assertTrue(selenium.isElementPresent("//h2[contains(text(),'Standard Features > Application > Navigation > UC29 > Global Scope')]"));
		assertTrue(selenium.isElementPresent("//b/a[contains(@id,\"lnk_teams\")]"));
		assertTrue(selenium.isElementPresent("//b/a[contains(text(),'Teams')]"));
		assertTrue(selenium.isElementPresent("//b/a[contains(@id,'lnk_players')]"));
		assertTrue(selenium.isElementPresent("//b/a[contains(text(),'Players')]"));
		assertTrue(selenium.isElementPresent("//b/a[contains(@id,\"lnk_states\")]"));
		assertTrue(selenium.isElementPresent("//b/a[contains(text(),'States')]"));
		assertTrue(selenium.isElementPresent("//b/a[contains(@id,\"lnk_stadiums\")]"));
		assertTrue(selenium.isElementPresent("//b/a/u[contains(text(),'Stadiums')]"));
		// On v�rifie les donn�es du tableau Stadium :
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_stadiums')]/thead/tr/th[contains(text(),'Name')]"));
		verifyEquals("12", selenium.getXpathCount("//table[contains(@id,'tab_stadiums')]/tbody//tr"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr/td/span[contains(text(),\"Heinz Field\")]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr/td/span[contains(text(),\"Jobing.com Arena\")]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr/td/span[contains(text(),\"Monster Park\")]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr/td/span[contains(text(),\"U.S. Cellular Field \")]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr/td/span[contains(text(),\"Dick s Sporting Goods Park\")]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr/td/span[contains(text(),\"Hut Park \")]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr/td/span[contains(text(),\"Canad Inns Stadium\")]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr/td/span[contains(text(),\"Gay Meadow\")]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr/td/span[contains(text(),\"American Airlines Arena\")]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr/td/span[contains(text(),\"New Louisville Basketball Arena\")]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr/td/span[contains(text(),\"Sutherland\")]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr/td/span[contains(text(),\"Spalding Arena\")]"));
	}


}
