/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.application.datagrid.uc35_paginatorbdd;


import java.io.Serializable;
import java.util.List;

import com.bluage.documentation.business.standardfeatures.application.datagrid.uc35_paginatorbdd.bos.Player35BO;
import com.bluage.documentation.web.presentation.standardfeatures.application.datagrid.uc35_paginatorbdd.page.command.PageCommandListPlayers;
import com.netfective.bluage.core.web.AbstractForm;
import com.netfective.bluage.core.web.IForm;

/**
* Class : UC35_PaginatorBDDForm
*/
public class UC35_PaginatorBDDForm extends AbstractForm implements Serializable, IForm {
	
	/** Constant : serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	//CONSTANT : listPlayers
	private static final String LIST_PLAYERS = "listPlayers";
	/**
	 * 	Property: listPlayers 
	 */
	private List<Player35BO> listPlayers;
/**
	 * Default constructor : UC35_PaginatorBDDForm
	 */
	public UC35_PaginatorBDDForm() {
		super();
		// Initialize : listPlayers
		this.listPlayers = new java.util.ArrayList<com.bluage.documentation.business.standardfeatures.application.datagrid.uc35_paginatorbdd.bos.Player35BO>();
		// Initialize the paged list.
		initListPlayersPageCommand();
					this.setAlwaysCallPreControllers(true);
	}
	
	/**
	 * 	Getter : listPlayers 
	 * 	@return : Return the listPlayers instance.
	 */
	public List<Player35BO> getListPlayers(){
		return listPlayers; // For UC35_PaginatorBDDForm
	}
	
	/**
	 * 	Setter : listPlayers 
	 *  @param listPlayersinstance : The instance to set.
	 */
	public void setListPlayers(final List<Player35BO> listPlayersinstance){
		this.listPlayers = listPlayersinstance;// For UC35_PaginatorBDDForm
	}
/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UC35_PaginatorBDDForm [ "+
LIST_PLAYERS +" = " + listPlayers + "]";
	}
	
	/**
	 * Method setInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public void setInstanceByInstanceName(final String instanceName, final Object instance) {
		// Set the instance ListPlayers.
		if(LIST_PLAYERS.equals(instanceName)){// For UC35_PaginatorBDDForm
			this.setListPlayers((List<Player35BO>)instance); // For UC35_PaginatorBDDForm
		}
			}
	
	/**
	 * Method getInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public Object getInstanceByInstanceName(final String instanceName){
		// Temporary object for tmpUC35_PaginatorBDDForm.
		Object tmpUC35_PaginatorBDDForm = null;
		
			
		// Get the instance ListPlayers for UC35_PaginatorBDDForm.
		if(LIST_PLAYERS.equals(instanceName)){ // For UC35_PaginatorBDDForm
			tmpUC35_PaginatorBDDForm = this.getListPlayers(); // For UC35_PaginatorBDDForm
		}
		return tmpUC35_PaginatorBDDForm;// For UC35_PaginatorBDDForm
	}
	
		// Table BDD fulle size : listPlayersSizeResult
		private int listPlayersSizeResult;
		
		/**
		 * Getter for : listPlayersSizeResult
		 * @return
		 */
		public int getListPlayersSizeResult() {
			return listPlayersSizeResult;
		}
	
		/**
		 * Setter for : listPlayersSizeResult
		 * @param listPlayersSizeResult
		 */
		public void setListPlayersSizeResult(int listPlayersSizeResult) {
			this.listPlayersSizeResult = listPlayersSizeResult;
		}
		
		// Page command declaration
		PageCommandListPlayers instantePageCommandListPlayers;
		
		// Number of row to display for : listPlayers
		private static final Integer _PAGE_COMMAND_LIST_PLAYERS_R_O_W__D_I_S_P_L_A_Y_E_D = 4;
		
		private void initPageCommandListPlayers(){
			if(instantePageCommandListPlayers== null){
				instantePageCommandListPlayers = new PageCommandListPlayers(_PAGE_COMMAND_LIST_PLAYERS_R_O_W__D_I_S_P_L_A_Y_E_D,0);
			}
		}
		
		/**
		 * Update the page number for the instance : listPlayers
		 * @param currentPage : The current page.
		 * @return : The corresponding PageCommandInstance.
		 */
		public PageCommandListPlayers updateListPlayersPageCommand(final int currentPage){
			// Init if necessary
			initPageCommandListPlayers();
			
			// Set the current page number
			instantePageCommandListPlayers.setCurrentPage(currentPage);

			// Call the paged query
			instantePageCommandListPlayers.updateResults();
			listPlayersSizeResult = instantePageCommandListPlayers.getNumberOfCount();
			listPlayers = instantePageCommandListPlayers.getResults();
			return instantePageCommandListPlayers;
		}
		
		/**
		 * Initialize the PageCommand.
		 * @return
		 */
		public PageCommandListPlayers initListPlayersPageCommand() {
			// Init if necessary
			initPageCommandListPlayers();
			// The default page number is 0.
			return updateListPlayersPageCommand(0);
		}
			}
