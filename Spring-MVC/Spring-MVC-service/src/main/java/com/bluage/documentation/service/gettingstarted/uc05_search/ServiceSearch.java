/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.service.gettingstarted.uc05_search;


import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;

import com.bluage.documentation.business.gettingstarted.uc05_search.bos.Player05BO;
import com.bluage.documentation.business.gettingstarted.uc05_search.bos.Position05BO;
import com.bluage.documentation.business.gettingstarted.uc05_search.entities.daofinder.Player05DAOFinderImpl;
import com.bluage.documentation.business.gettingstarted.uc05_search.entities.daofinder.Position05DAOFinderImpl;
import com.bluage.documentation.service.AbstractService;
import com.bluage.documentation.service.gettingstarted.uc05_search.daofinder.ServiceSearchDAOMethodFinderImpl;
import com.netfective.bluage.core.exception.ApplicationException;

/**
 * Defines  application services for ServiceSearch
  */
@Named("com.bluage.documentation.service.gettingstarted.uc05_search.ServiceSearch")
public class ServiceSearch  extends AbstractService{


						@Inject
		@Named ("com.bluage.documentation.business.gettingstarted.uc05_search.entities.daofinder.Position05DAOFinderImpl")
		private Position05DAOFinderImpl position05BOFinder;
			@Inject
		@Named ("com.bluage.documentation.business.gettingstarted.uc05_search.entities.daofinder.Player05DAOFinderImpl")
		private Player05DAOFinderImpl player05BOFinder;
									@Inject
		@Named ("com.bluage.documentation.service.gettingstarted.uc05_search.daofinder.ServiceSearchDAOMethodFinder")
		private ServiceSearchDAOMethodFinderImpl serviceSearchDao;
			


	/**
	 *  Default constructor 
	 */
	public ServiceSearch() {
		super(); // Call super for default constructor : ServiceSearch
	}
	



			
	/**    
	 * Operation HQL.
	 * @param id {@link Long}
		 * @return {@link Player05BO}
	 */
	public Player05BO player05FindByID(Long id )
 throws ApplicationException {
	
	return serviceSearchDao.player05FindByID(id);
	}

					
								
	/**    
	 * Operation Crud
	 * @param bo {@link Player05BO}
		 * @return {@link List}
	 */
	public List player05FindByProperties(Player05BO bo )
 throws ApplicationException {

					return player05BOFinder.findByProperties(boToCriterias(bo));
	}
	/**
	 * Transform a business object into criteria.
	 * @param boInstance {@link Player05BO} business object instance.
	 * @return a Map containing the search criteria.
	 */
	private Map<String, Object> boToCriterias(Player05BO boInstance) {
		Map<String, Object> criterias = new Hashtable<String, Object>();
    	boInstance.setObjMode("DAO");    	
	    if(boInstance.getPosition() != null && boInstance.getPosition().getCode() != null) {
    		criterias.put("position", boInstance.getPosition());    		
    	}
											    	if(boInstance.getId() != null) {
    		criterias.put("id", boInstance.getId());
    	}
		    	if(boInstance.getFirstName() != null && !"".equals(boInstance.getFirstName())) {
			criterias.put("firstName",boInstance.getFirstName());
		}
		    	if(boInstance.getLastName() != null && !"".equals(boInstance.getLastName())) {
			criterias.put("lastName",boInstance.getLastName());
		}
		    	if(boInstance.getDateOfBirth() != null) {
    		criterias.put("dateOfBirth", boInstance.getDateOfBirth());
    	}
		    	if(boInstance.getEstimatedValue() != null) {
    		criterias.put("estimatedValue", boInstance.getEstimatedValue());
    	}
		    	if(boInstance.getHeight() != null) {
    		criterias.put("height", boInstance.getHeight());
    	}
		    	if(boInstance.getWeight() != null) {
    		criterias.put("weight", boInstance.getWeight());
    	}
		    	if(boInstance.getRookie() != null) {
    		criterias.put("rookie", boInstance.getRookie());
    	}
		 		return criterias;
	}				

		
				
	/**    
	 * Operation HQL.
	 * @param lastName {@link String}
		 * @param minHeight {@link Integer}
		 * @param maxHeight {@link Integer}
		 * @return {@link List}
	 */
	public List player05SearchByCriteria(String lastName,Integer minHeight,Integer maxHeight )
 throws ApplicationException {
	
	return serviceSearchDao.player05SearchByCriteria(lastName,minHeight,maxHeight);
	}

		
			
	/**    
	 * Process operation.
	 * @param player {@link Player05BO}
		 * @return {@link List}
	 */
	public List player05ExecuteSearch(Player05BO player )
 throws ApplicationException {

				return privatePlayer05ExecuteSearch(player);
	}
	

	/**    
	 * Process operation.
	 * @param player {@link Player05BO}
		 * @return {@link List}
	 */
	private List privatePlayer05ExecuteSearch(Player05BO player )
 throws ApplicationException {
						// Return Type declaration.
				List result = new java.util.ArrayList();
					// Central buffer node declaration for operation player05ExecuteSearch
			String	 critLastName =  "";
					Integer	 critMinHeight =  Integer.valueOf(0);
					Integer	 critMaxHeight =  Integer.valueOf(0);
					// Calling an operation : 
critLastName = player.getLastName();
// Calling an operation : 
critMinHeight = player.getMinHeight();
// Calling an operation : 
critMaxHeight = player.getMaxHeight();
/* Executing : Format critLastName for HQL*/
critLastName = (critLastName.length() == 0) ? "%" : critLastName.substring(0,1).toUpperCase() + critLastName.substring(1,critLastName.length()).toLowerCase() + "%";
// Calling an operation : 
result = player05SearchByCriteria(critLastName, critMinHeight, critMaxHeight);

						return result;
		}

		
			
	/**    
	 * Process operation.
	 * @return {@link void}
	 */
	public void back( )
 throws ApplicationException {

			privateBack();
	}
	

	/**    
	 * Process operation.
	 * @return {@link void}
	 */
	private void privateBack( )
 throws ApplicationException {
// Central buffer node declaration for operation back
			
		}

					
				
	/**    
	 * Operation Crud
	 * @param bo {@link Position05BO}
		 * @return {@link Position05BO}
	 */
	public Position05BO position05FindByID(Position05BO bo )
 throws ApplicationException {

					return (Position05BO)position05BOFinder.findByPrimaryKey(getBoId(bo));
	}
	/**
	 *  Return the primary key from business object.
	 */
	private Object getBoId(Position05BO bo) {
				return bo.getCode();
		}

		
			
	/**    
	 * Process operation.
	 * @param playerCriteria {@link Player05BO}
		 * @return {@link List}
	 */
	public List player05ExecuteSearchByProperties(Player05BO playerCriteria )
 throws ApplicationException {

				return privatePlayer05ExecuteSearchByProperties(playerCriteria);
	}
	

	/**    
	 * Process operation.
	 * @param playerCriteria {@link Player05BO}
		 * @return {@link List}
	 */
	private List privatePlayer05ExecuteSearchByProperties(Player05BO playerCriteria )
 throws ApplicationException {
						// Return Type declaration.
				List result = new java.util.ArrayList();
					// Central buffer node declaration for operation player05ExecuteSearchByProperties
			Position05BO	 positionResult =  new Position05BO();
					Position05BO	 position =  new Position05BO();
					// Calling an operation : 
position = playerCriteria.getPosition();
// Calling an operation : 
positionResult = position05FindByID(position);
// Calling an operation : 
playerCriteria.setPosition(positionResult);
// Calling an operation : 
result = player05FindByProperties(playerCriteria);

						return result;
		}

		
			
	/**    
	 * Process operation.
	 * @param player05 {@link Player05BO}
		 * @return {@link Player05BO}
	 */
	public Player05BO playerSearchById(Player05BO player05 )
 throws ApplicationException {

				return privatePlayerSearchById(player05);
	}
	

	/**    
	 * Process operation.
	 * @param player05 {@link Player05BO}
		 * @return {@link Player05BO}
	 */
	private Player05BO privatePlayerSearchById(Player05BO player05 )
 throws ApplicationException {
				// Central buffer node declaration for operation playerSearchById
			Long	 id =  Long.valueOf(0);
					// Calling an operation : getID
id = player05.getId();
// Calling an operation : Player05FindByID
player05 = player05FindByID(id);

						return player05;
		}

		}
