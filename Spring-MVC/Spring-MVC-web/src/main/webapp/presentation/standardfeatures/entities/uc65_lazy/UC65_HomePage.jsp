<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file="/include.jsp"  %>
<html >
<head >
<title  > 
<fmt:message key="cUC65_HomePage_BLU_AGE_DOCUMENTATION"/>
</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"  > 
</meta>
<link href="<c:url value= "/presentation/_style/_style.css"/>" rel="stylesheet" type="text/css"  /> 
<link href="<c:url value= "/presentation/_include/jquery/rd_lib.css"/>" rel="stylesheet" type="text/css"  /> 
<script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/jquery.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/rd_setup.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/rd_lib.js"/>" space="preserve"  ></script><link rel="shortcut icon" href="<c:url value= "/presentation/_style/favicon.ico"/>"  /> 

</head>

<body class=" " > 
<div id="rd_ghost"  > 
<small  > 
<fmt:message key="cUC65_HomePage___CSS_DEACTIVATED"/>
</small>
</div>
<div id="rd_jghost"  > 
<small  > 
<fmt:message key="cUC65_HomePage___JAVASCRIPT_DEACTIVATED"/>
</small>
</div>
<div id="shape"  > 
<div class="ui-layout-center"  > 
<div class="east-north"  > 
	<form:form id="uC65_HomePage" commandName="uC65_HomePageForm"  enctype="application/x-www-form-urlencoded" method="post">
<h2 class="rd_first"  > 
<fmt:message key="cUC65_HomePage_STANDARD_FEATURES__ENTITIES_"/>
</h2>
<display:table uid="tab_teams"  sort="list" requestURI="" name="sessionScope.uC65_HomePageForm.teams" class="rd_tabledata"  >
	<c:set   var="rownum">
		<%=(tab_teams !=	null) ? new Integer(tab_teams_rowNum.intValue() - 1) : new Integer(0) %>
	</c:set>
	<c:set   var="idRow">
		<%=(tab_teams != null ) ? tab_teams_rowNum.intValue() - 1 : -1 %>
	</c:set>
	<c:set   var="idTab">
		<%=(tab_teams != null ) ? "tab_teams" : null %>
	</c:set>
	<c:set var="tableHashId">
		<%= new org.displaytag.util.ParamEncoder("tab_teams").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE) %>
	</c:set>
	<c:set var="tableHashId2">
		<%=request.getParameter(new org.displaytag.util.ParamEncoder("tab_teams").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE)) %>
	</c:set>	
	
<display:column format=""  titleKey="cUC65_HomePage_NAME">
		<span id="txt_name">
	<c:out value="${tab_teams.name}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="cUC65_HomePage_CITY">
		<span id="txt_city">
	<c:out value="${tab_teams.city}" />
		</span>
	</display:column>
	
<display:column format=""  >
<a id="lnk_view_fetch[${rownum}]"onclick="javascript:btn_click_cExecuteUC65_HomePage(this.href);return false;"			href="<c:url value="/presentation/standardfeatures/entities/uc65_lazy/UC65_HomePage/lnk_view_fetch.html?id="/><c:out value="${idRow}" />&tab=<c:out value="tab_teams" />&<c:out value="${tableHashId}"/>=<c:out value="${tableHashId2}"/>"
	 shape="rect"  >									 
<fmt:message key="cUC65_HomePage_____VIEW_PROFESSIONAL_PLAYERS"/>
</a>
</display:column>
	
<display:column format=""  >
<a id="lnk_view_nofetch[${rownum}]"onclick="javascript:btn_click_cExecuteUC65_HomePage(this.href);return false;"			href="<c:url value="/presentation/standardfeatures/entities/uc65_lazy/UC65_HomePage/lnk_view_nofetch.html?id="/><c:out value="${idRow}" />&tab=<c:out value="tab_teams" />&<c:out value="${tableHashId}"/>=<c:out value="${tableHashId2}"/>"
	 shape="rect"  >									 
<fmt:message key="cUC65_HomePage_____VIEW_ROOKIE_PLAYERS_"/>
</a>
</display:column>
</display:table>
	</form:form>
	<script language="javascript">
		function btn_click_cExecuteUC65_HomePage(url) {
			UC65_HomePagevar = document.getElementById('uC65_HomePage');
			UC65_HomePagevar.setAttribute('action', url);
			UC65_HomePagevar.submit();
		}
		function btn_click_cSetActionUC65_HomePage(url) {
			UC65_HomePagevar = document.getElementById('uC65_HomePage');
			UC65_HomePagevar.setAttribute('action', url);
		}
	</script>
</div>
<div class="east-center"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/entities/uc65_lazy/UC65_HomePagecmp_tabs" />
</div>
</div>
<div class="ui-layout-north"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/entities/uc65_lazy/UC65_HomePagecmp_header" />
</div>
<div class="ui-layout-south"  > 
<fmt:message key="cUC65_HomePage___"/>
<span id="rd_jversion"  > 
<fmt:message key="cUC65_HomePage_BLU_AGE_DOCUMENTATION_167"/>
</span>
</div>
<div class="ui-layout-west"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/entities/uc65_lazy/UC65_HomePagecmp_treeview" />
</div>
</div>
</body>
</html>
