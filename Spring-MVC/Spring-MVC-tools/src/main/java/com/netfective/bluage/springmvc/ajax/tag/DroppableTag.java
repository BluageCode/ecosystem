/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.netfective.bluage.springmvc.ajax.tag;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.jsp.JspException;

import com.netfective.bluage.springmvc.ajax.tag.html.HtmlDivTag;
import com.netfective.bluage.springmvc.ajax.util.Constants;
import com.netfective.bluage.springmvc.ajax.util.RequestUtils;

/**
 * Class DroppableTag
 * @superclass HtmlDivTag
 * @interface Constants
 * @author NETFECTIVE TECHNOLOGY
 */
public class DroppableTag extends HtmlDivTag implements Constants {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3253898859258476079L;

	/**
	* Action string
	*/
	private String action;	

	/**
	* DragGroup string id
	*/
	private String dragGroup;

	/**
	* hoverclass
	*/
	private String hoverclass;

	/**
	* Submit option
	*/
	private boolean submit;

	
	/**
	* This method give all script options
	 * @return Object
	 * @throw JspException
	 */
	private Object buildScriptOptions() {
		final StringBuilder strbuff = new StringBuilder();
		strbuff.append("{");
		if (hoverclass != null && hoverclass.trim().length() > 0) {
			strbuff.append("hoverclass:'");
			strbuff.append(hoverclass);
			strbuff.append("'");
			strbuff.append(", ");
		}

		strbuff.append("accept:'");
		strbuff.append(dragGroup);
		strbuff.append("'");
		strbuff.append(", ");
		
		strbuff.append("onLoading:");
		strbuff.append("function(request){alert('aaa');Element.show('indicator');}");
		strbuff.append(", ");
		
		strbuff.append("onDrop:");
		final String url = RequestUtils.buildUrlWithPostData(pageContext,action,getParameters()) ;
		if (submit){
			strbuff.append("function(element,dropon){bluageAjax.submitURL('" + url + "');}");
		} else {
			strbuff.append("function(element,dropon){bluageAjax.getAJAX('" + url + "');}");
		}
	
		strbuff.append("} ");
		
		

		return strbuff.toString();
	}
	
	/**
	* Return an initialisation of parameters
	* @return Map 
	*/
	private Map getParameters() {
		final Map parameters = new HashMap();
		parameters.put(id, "'+element.dragValue+'");
		return parameters;
	}

	/**
	* This method prints a end tag on output of <code>pageContext</code>
	* @return int
	* @throw JspException
	*/
	public int doEndTag() throws JspException {

		final int eval = super.doEndTag();

		final Object[] arguments = { "'" + TagUtils.getRealDivId(id) + "'",
				buildScriptOptions() };
		final String script = MessageFormat
				.format(CREATE_DROPPABLE_SCRIPT, arguments);

		try {
			pageContext.getOut().println(TagUtils.buildScript(script));
		} catch (IOException e) {
			throw new JspException(e);
		}

		return eval;
	}

	// ------------------------------------------------------------------------Getter_Setter

	/**
	* Getter for action
	* @return String
	*/
	public String getAction() {
		return action;
	}

	/**
	* Setter for action
	* @param String
	*/
	public void setAction(final String action) {
		this.action = action;
	}

	/**
	* Getter for dragGroup
	* @return String
	*/
	public String getDragGroup() {
		return dragGroup;
	}

	/**
	* Setter for dragGroup
	* @param String
	*/
	public void setDragGroup(final String dragGroup) {
		this.dragGroup = dragGroup;
	}

	/**
	* Getter for hoverclass
	* @return String
	*/
	public String getHoverclass() {
		return hoverclass;
	}

	/**
	* Setter for hoverclass
	* @param String
	*/
	public void setHoverclass(final String hoverclass) {
		this.hoverclass = hoverclass;
	}

	/**
	* Getter for submit
	* @return boolean
	*/
	public boolean isSubmit() {
		return submit;
	}

	/**
	* Setter for submit
	* @param boolean
	*/
	public void setSubmit(final boolean submit) {
		this.submit = submit;
	}

}
