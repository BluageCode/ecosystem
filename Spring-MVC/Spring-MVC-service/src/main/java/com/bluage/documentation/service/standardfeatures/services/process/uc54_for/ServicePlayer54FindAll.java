/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.service.standardfeatures.services.process.uc54_for;


import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import com.bluage.documentation.business.standardfeatures.services.process.uc54_for.entities.daofinder.Player54DAOFinderImpl;
import com.bluage.documentation.service.AbstractService;
import com.netfective.bluage.core.exception.ApplicationException;
import com.netfective.bluage.core.persistence.IPersistentObjectDAO;

/**
 * Defines  application services for ServicePlayer54FindAll
  */
@Named("com.bluage.documentation.service.standardfeatures.services.process.uc54_for.ServicePlayer54FindAll")
public class ServicePlayer54FindAll  extends AbstractService{


	


	/**
	 *  Default constructor 
	 */
	public ServicePlayer54FindAll() {
		super(); // Call super for default constructor : ServicePlayer54FindAll
	}
	
			@Inject
	@Named ("com.bluage.documentation.business.standardfeatures.services.process.uc54_for.entities.daofinder.Player54DAOFinderImpl")
	private Player54DAOFinderImpl dao;

	/**
	 * constructor with...
	 * 
	 * @param dao
	 *		: {@link IPersistentObjectDAO} the DAO
	 */
	public ServicePlayer54FindAll(Player54DAOFinderImpl dao) {
		this.dao = dao;
	}

							
	/**    
	 * Operation Crud
	 * @return {@link List}
	 */
	public List player54FindAll( )
 throws ApplicationException {

					return dao.findAll();
	}

	

}
