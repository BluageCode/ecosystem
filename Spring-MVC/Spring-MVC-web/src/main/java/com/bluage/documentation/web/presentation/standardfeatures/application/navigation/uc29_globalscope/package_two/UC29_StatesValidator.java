/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.application.navigation.uc29_globalscope.package_two ;


import org.apache.log4j.Logger;
import org.springframework.validation.Errors;

import com.netfective.bluage.core.validator.AbstractValidator;
/**
* Class :UC29_StatesValidator
*/
public class UC29_StatesValidator extends AbstractValidator{
	
	// LOGGER for the class UC29_StatesValidator
	private static final Logger LOGGER = Logger.getLogger( UC29_StatesValidator.class);
	
	
	/**
	* Operation validate for UC29_StatesForm
	* @param obj : the current form (UC29_StatesForm)
	* @param errors : The spring errors to return for the form UC29_StatesForm
	*/
	public void validate(final Object obj,final Errors errors){
		LOGGER.info("Beginning method : validate"); // UC29_StatesValidator
		UC29_StatesForm cUC29_StatesForm = (UC29_StatesForm)obj; // UC29_StatesValidator
		LOGGER.info("Ending method : validate the form "+ cUC29_StatesForm.getClass().getName()); // UC29_StatesValidator
	}

	/**
	* Method to implements to use spring validators (UC29_StatesForm)
	* @param aClass : Class for the form UC29_StatesForm
	* @return boolean
	*/
	public boolean supports(final Class aClass){
		return (new UC29_StatesForm()).getClass().equals(aClass); // UC29_StatesValidator
	}
}
