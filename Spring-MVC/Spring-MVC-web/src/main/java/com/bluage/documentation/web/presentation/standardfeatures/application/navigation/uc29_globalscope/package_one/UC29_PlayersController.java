/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.application.navigation.uc29_globalscope.package_one ;

// Import declaration.
// Java imports.
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;

/**
 * Class : UC29_PlayersController
 */
@Controller
@Scope("prototype")
@SessionAttributes("uC29_PlayersForm")
@RequestMapping(value= "/presentation/standardfeatures/application/navigation/uc29_globalscope/package_one")
public class UC29_PlayersController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC29_PlayersController.class);
	
	//Current form name
	private static final String U_C29__PLAYERS_FORM = "uC29_PlayersForm";

	//CONSTANT: allPlayers table or repeater.
	private static final String ALL_PLAYERS = "allPlayers";
				
	/**
	 * Property:customDateEditorsUC29_PlayersController
	 */
	final private Map<String,CustomDateEditor> customDateEditorsUC29_PlayersController = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:uC29_PlayersValidator
	 */
	final private UC29_PlayersValidator uC29_PlayersValidator = new UC29_PlayersValidator();
	
	/**
	 * Pre Controller declaration : uC29_PreControllerFindAllTeams
	 */
	@Autowired
	private UC29_PreControllerFindAllTeamsController uC29_PreControllerFindAllTeams;

	
	// Initialise all the instances for UC29_PlayersController
		// Initialize the instance allPlayers for the state findPlayers
		private List allPlayers; // Initialize the instance allPlayers for UC29_PlayersController
				
	/**
	* This method initialise the form : UC29_PlayersForm 
	* @return UC29_PlayersForm
	*/
	@ModelAttribute("UC29_PlayersFormInit")
	public void initUC29_PlayersForm(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, U_C29__PLAYERS_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method UC29_PlayersForm."); //for findPlayers 
		}
		UC29_PlayersForm uC29_PlayersForm;
	
		if(request.getSession().getAttribute(U_C29__PLAYERS_FORM) != null){
			uC29_PlayersForm = (UC29_PlayersForm)request.getSession().getAttribute(U_C29__PLAYERS_FORM);
		} else {
			uC29_PlayersForm = new UC29_PlayersForm();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form UC29_PlayersForm.");
		}
		uC29_PlayersForm = (UC29_PlayersForm)populateDestinationForm(request.getSession(), uC29_PlayersForm);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  UC29_PlayersForm.");
		}
		model.addAttribute(U_C29__PLAYERS_FORM, uC29_PlayersForm);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param UC29_PlayersForm : The sceen form.
	 */
	@RequestMapping(value = "/UC29_Players.html" ,method = RequestMethod.GET)
	public void prepareUC29_Players(final HttpServletRequest request,final  Model model,final  @ModelAttribute("UC29_PlayersFormInit") UC29_PlayersForm uC29_PlayersForm){
		
		UC29_PlayersForm currentUC29_PlayersForm = uC29_PlayersForm;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(U_C29__PLAYERS_FORM) == null){
			if(currentUC29_PlayersForm!=null){
				request.getSession().setAttribute(U_C29__PLAYERS_FORM, currentUC29_PlayersForm);
			}else {
				currentUC29_PlayersForm = new UC29_PlayersForm();
				request.getSession().setAttribute(U_C29__PLAYERS_FORM, currentUC29_PlayersForm);	
			}
		} else {
			currentUC29_PlayersForm = (UC29_PlayersForm) request.getSession().getAttribute(U_C29__PLAYERS_FORM);
		}

					currentUC29_PlayersForm = (UC29_PlayersForm)populateDestinationForm(request.getSession(), currentUC29_PlayersForm);
		// Call all the Precontroller.
	if ( currentUC29_PlayersForm.isAlwaysCallPreControllers()) {
	
		// Calling the PreController : UC29_PreControllerFindAllTeams.
		currentUC29_PlayersForm = (UC29_PlayersForm) uC29_PreControllerFindAllTeams.uC29_PreControllerFindAllTeamsControllerInit(request, model, currentUC29_PlayersForm);
		
	}
	currentUC29_PlayersForm.setAlwaysCallPreControllers(true);
		model.addAttribute(U_C29__PLAYERS_FORM, currentUC29_PlayersForm);
		request.getSession().setAttribute(U_C29__PLAYERS_FORM, currentUC29_PlayersForm);
		
	}
	
				
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("UC29_PlayersController [ ");
			strBToS.append(ALL_PLAYERS);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(allPlayers);
			strBToS.append(END_TO_STRING_DELIMITER);
					// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(uC29_PlayersValidator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsUC29_PlayersController!=null); 
		return strBToS.toString();
	}
}
