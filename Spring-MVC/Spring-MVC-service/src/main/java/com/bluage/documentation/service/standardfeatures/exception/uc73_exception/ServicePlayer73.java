/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.service.standardfeatures.exception.uc73_exception;


import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;

import com.bluage.documentation.business.standardfeatures.exception.uc73_exception.bos.Player73BO;
import com.bluage.documentation.business.standardfeatures.exception.uc73_exception.bos.Team73BO;
import com.bluage.documentation.business.standardfeatures.exception.uc73_exception.entities.dao.Player73DAOImpl;
import com.bluage.documentation.business.standardfeatures.exception.uc73_exception.entities.daofinder.Player73DAOFinderImpl;
import com.bluage.documentation.service.AbstractService;
import com.bluage.documentation.service.standardfeatures.exception.uc73_exception.daofinder.ServicePlayer73DAOMethodFinderImpl;
import com.netfective.bluage.core.exception.ApplicationException;

/**
 * Defines  application services for ServicePlayer73
  */
@Named("com.bluage.documentation.service.standardfeatures.exception.uc73_exception.ServicePlayer73")
public class ServicePlayer73  extends AbstractService{


									@Inject
		@Named ("com.bluage.documentation.business.standardfeatures.exception.uc73_exception.entities.daofinder.Player73DAOFinderImpl")
		private Player73DAOFinderImpl player73BOFinder;
						@Inject
		@Named ("com.bluage.documentation.service.standardfeatures.exception.uc73_exception.daofinder.ServicePlayer73DAOMethodFinder")
		private ServicePlayer73DAOMethodFinderImpl servicePlayer73Dao;
					@Inject
		@Named ("com.bluage.documentation.business.standardfeatures.exception.uc73_exception.entities.dao.Player73DAOImpl")
		private Player73DAOImpl player73BODao;
	


	/**
	 *  Default constructor 
	 */
	public ServicePlayer73() {
		super(); // Call super for default constructor : ServicePlayer73
	}
	


			
								
	/**    
	 * Operation Crud
	 * @param bo {@link Player73BO}
		 * @return {@link List}
	 */
	public List player73FindByProperties(Player73BO bo )
 throws ApplicationException {

					return player73BOFinder.findByProperties(boToCriterias(bo));
	}
	/**
	 * Transform a business object into criteria.
	 * @param boInstance {@link Player73BO} business object instance.
	 * @return a Map containing the search criteria.
	 */
	private Map<String, Object> boToCriterias(Player73BO boInstance) {
		Map<String, Object> criterias = new Hashtable<String, Object>();
    	boInstance.setObjMode("DAO");    	
	    if(boInstance.getTeam() != null && boInstance.getTeam().getId() != null) {
    		criterias.put("team", boInstance.getTeam());    		
    	}
											 		return criterias;
	}				

		
			
	/**    
	 * Process operation.
	 * @param playerToDelete {@link Player73BO}
		 * @return {@link boolean}
	 */
	public boolean isReady(Player73BO playerToDelete )
 throws ApplicationException {

				return privateIsReady(playerToDelete);
	}
	

	/**    
	 * Process operation.
	 * @param playerToDelete {@link Player73BO}
		 * @return {@link boolean}
	 */
	private boolean privateIsReady(Player73BO playerToDelete )
 throws ApplicationException {
						// Return Type declaration.
				boolean playerIsReady = false;
					// Central buffer node declaration for operation isReady
			Integer	 height =  Integer.valueOf(0);
					Integer	 weight =  Integer.valueOf(0);
					// Calling an operation : 
weight = playerToDelete.getWeight();
// Calling an operation : 
height = playerToDelete.getHeight();
if (height>180) {
if (weight<150) {
/* Executing : playerIsReady=true*/
playerIsReady=true;
}
}

						return playerIsReady;
		}

		
			
	/**    
	 * Process operation.
	 * @param playerToDelete {@link Player73BO}
		 * @param playersList {@link List}
		 * @return {@link List}
	 */
	public List deletePlayer(Player73BO playerToDelete,List playersList )
 throws ApplicationException {

				return privateDeletePlayer(playerToDelete,playersList);
	}
	

	/**    
	 * Process operation.
	 * @param playerToDelete {@link Player73BO}
		 * @param playersList {@link List}
		 * @return {@link List}
	 */
	private List privateDeletePlayer(Player73BO playerToDelete,List playersList )
 throws ApplicationException {
						// Central buffer node declaration for operation deletePlayer
			boolean	 playerIsReady =  false;
					// Calling an operation : 
playerIsReady = isReady(playerToDelete);
// Test on exception : 
if (playerIsReady==true) {
ApplicationException app = new ApplicationException("It's not authorized to delete a player if his weight is under 150 kg and his height is above 180 cm.");
throw app;
}
/* Executing : */
playerToDelete.setTeam(null);
playersList.remove(playerToDelete);
// Calling an operation : 
update(playerToDelete);

						return playersList;
		}

		
			
	/**    
	 * Process operation.
	 * @return {@link void}
	 */
	public void back( )
 throws ApplicationException {

			privateBack();
	}
	

	/**    
	 * Process operation.
	 * @return {@link void}
	 */
	private void privateBack( )
 throws ApplicationException {
// Central buffer node declaration for operation back
			
		}

		
			
	/**    
	 * Process operation.
	 * @param selectedTeam {@link Team73BO}
		 * @return {@link List}
	 */
	public List player73Find(Team73BO selectedTeam )
 throws ApplicationException {

				return privatePlayer73Find(selectedTeam);
	}
	

	/**    
	 * Process operation.
	 * @param selectedTeam {@link Team73BO}
		 * @return {@link List}
	 */
	private List privatePlayer73Find(Team73BO selectedTeam )
 throws ApplicationException {
						// Return Type declaration.
				List playersFound = new java.util.ArrayList();
					// Central buffer node declaration for operation player73Find
			Long	 id =  Long.valueOf(0);
					// Calling an operation : Get Id
id = selectedTeam.getId();
// Calling an operation : Find all Players
playersFound = findallPlayers(id);
/* Executing : Affiche Msg*/
if (playersFound != null && playersFound.size() > 0) {			selectedTeam.setPlayerExiste
(Boolean.TRUE);
} else {	selectedTeam.setPlayerExiste(Boolean.FALSE);
};

						return playersFound;
		}

		
				
	/**    
	 * Operation HQL.
	 * @param id {@link Long}
		 * @return {@link List}
	 */
	public List findallPlayers(Long id )
 throws ApplicationException {
	
	return servicePlayer73Dao.findallPlayers(id);
	}

					
				
	/**    
	 * Operation Crud
	 * @param playerToUpdate {@link Player73BO}
		 * @return {@link boolean}
	 */
	public boolean update(Player73BO playerToUpdate )
 throws ApplicationException {

					return player73BODao.update(playerToUpdate);
	}

		}
