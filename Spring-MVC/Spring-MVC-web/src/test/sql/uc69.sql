ALTER TABLE team69 DROP CONSTRAINT team69_pkey;

ALTER TABLE player69 DROP CONSTRAINT player69_pkey;

DROP TABLE team69;

DROP TABLE player69;

CREATE TABLE team69 (
		id INT8 NOT NULL,
		version INT4 NOT NULL,
		city VARCHAR(1024),
		name VARCHAR(1024)
	);

CREATE TABLE player69 (
		id INT8 NOT NULL,
		version INT4 NOT NULL,
		first_name VARCHAR(1024),
		team69_fk INT8 NOT NULL,
		date_of_birth DATE,
		last_name VARCHAR(1024)
	);
	
INSERT INTO team69 (id, version, city, name) VALUES (1, 0, 'Charlotte', 'Tigers');
INSERT INTO team69 (id, version, city, name) VALUES (2, 0, 'Kansas City', 'Bears');
INSERT INTO team69 (id, version, city, name) VALUES (3, 0, 'Detroit', 'Chargers');
INSERT INTO team69 (id, version, city, name) VALUES (4, 0, 'Detroit', 'Hawks');
INSERT INTO team69 (id, version, city, name) VALUES (5, 0, 'Omaha', 'Braves');
INSERT INTO team69 (id, version, city, name) VALUES (6, 0, 'Raleigh', 'Eagles');
INSERT INTO team69 (id, version, city, name) VALUES (7, 0, 'Oakland', 'Cardinals');
INSERT INTO team69 (id, version, city, name) VALUES (8, 0, 'Kansas City', 'Red Devils');
INSERT INTO team69 (id, version, city, name) VALUES (9, 0, 'Fresno', 'Mustangs');

INSERT INTO player69 (id, version, first_name, last_name, date_of_birth, team69_fk) VALUES (201, 0, 'Oliver', 'Frank', '1968-10-31',  1);
INSERT INTO player69 (id, version, first_name, last_name, date_of_birth, team69_fk) VALUES (202, 0, 'Elbert', 'Cole', '1965-04-07', 2);
INSERT INTO player69 (id, version, first_name, last_name, date_of_birth, team69_fk) VALUES (203, 0, 'Abel', 'Dennis', '1980-04-16', 3);
INSERT INTO player69 (id, version, first_name, last_name, date_of_birth, team69_fk) VALUES (204, 0, 'Ricky', 'Dawson', '1990-03-03',4);
INSERT INTO player69 (id, version, first_name, last_name, date_of_birth, team69_fk) VALUES (205, 0, 'Earl', 'Russell', '1981-12-15', 5);
INSERT INTO player69 (id, version, first_name, last_name, date_of_birth, team69_fk) VALUES (206, 0, 'Nick', 'Garner', '1992-07-27',  6);
INSERT INTO player69 (id, version, first_name, last_name, date_of_birth, team69_fk) VALUES (207, 0, 'Howard', 'Ramos', '1970-08-25',  7);
INSERT INTO player69 (id, version, first_name, last_name, date_of_birth, team69_fk) VALUES (208, 0, 'Allan', 'Meyer', '1969-02-14', 8);
INSERT INTO player69 (id, version, first_name, last_name, date_of_birth, team69_fk) VALUES (209, 0, 'Ruben', 'Pope', '1985-05-01',  9);
INSERT INTO player69 (id, version, first_name, last_name, date_of_birth, team69_fk) VALUES (210, 0, 'Lester', 'Cross', '1976-02-21', 1);
INSERT INTO player69 (id, version, first_name, last_name, date_of_birth, team69_fk) VALUES (211, 0, 'Keith', 'Barnett', '1976-11-10', 2);
INSERT INTO player69 (id, version, first_name, last_name, date_of_birth, team69_fk) VALUES (212, 0, 'Ivan', 'Tate', '1972-02-08',  1);
INSERT INTO player69 (id, version, first_name, last_name, date_of_birth, team69_fk) VALUES (213, 0, 'Tommy', 'Wood', '1992-01-20', 3);
INSERT INTO player69 (id, version, first_name, last_name, date_of_birth, team69_fk) VALUES (214, 0, 'Sylvester', 'Nash', '1965-07-10',  4);
INSERT INTO player69 (id, version, first_name, last_name, date_of_birth, team69_fk) VALUES (215, 0, 'Michael', 'Hodges', '1988-04-01',  5);
INSERT INTO player69 (id, version, first_name, last_name, date_of_birth, team69_fk) VALUES (216, 0, 'Jerome', 'Grant', '1982-06-28',  6);
INSERT INTO player69 (id, version, first_name, last_name, date_of_birth, team69_fk) VALUES (217, 0, 'Woodrow', 'Carpenter', '1987-08-08', 7);
INSERT INTO player69 (id, version, first_name, last_name, date_of_birth, team69_fk) VALUES (446, 0, 'Charlie', 'Clark', '1971-08-31', 8);
INSERT INTO player69 (id, version, first_name, last_name, date_of_birth, team69_fk) VALUES (447, 0, 'Marshall', 'Norman', '1965-06-25', 9); 
INSERT INTO player69 (id, version, first_name, last_name, date_of_birth, team69_fk) VALUES (448, 0, 'Duane', 'Hart', '1974-02-12', 10);
INSERT INTO player69 (id, version, first_name, last_name, date_of_birth, team69_fk) VALUES (449, 0, 'Julius', 'Byrd', '1968-09-13', 1);
INSERT INTO player69 (id, version, first_name, last_name, date_of_birth, team69_fk) VALUES (450, 0, 'Ed', 'Drake', '1972-11-04', 2);
INSERT INTO player69 (id, version, first_name, last_name, date_of_birth, team69_fk) VALUES (451, 0, 'Clyde', 'Chambers', '1976-08-01', 3);
INSERT INTO player69 (id, version, first_name, last_name, date_of_birth, team69_fk) VALUES (452, 0, 'Oliver', 'Ballard', '1972-07-18', 4);
INSERT INTO player69 (id, version, first_name, last_name, date_of_birth, team69_fk) VALUES (453, 0, 'Myron', 'Clarke', '1966-12-31', 5);
INSERT INTO player69 (id, version, first_name, last_name, date_of_birth, team69_fk) VALUES (454, 0, 'Carroll', 'Shaw', '1987-03-07', 6);
INSERT INTO player69 (id, version, first_name, last_name, date_of_birth, team69_fk) VALUES (455, 0, 'Phil', 'Christensen', '1989-02-23', 7);
INSERT INTO player69 (id, version, first_name, last_name, date_of_birth, team69_fk) VALUES (457, 0, 'Bob', 'Fleming', '1972-01-16', 8);
INSERT INTO player69 (id, version, first_name, last_name, date_of_birth, team69_fk) VALUES (458, 0, 'Perry', 'Vega', '1975-08-29', 9);
INSERT INTO player69 (id, version, first_name, last_name, date_of_birth, team69_fk) VALUES (459, 0, 'Alfred', 'Briggs', '1965-09-29', 10);
INSERT INTO player69 (id, version, first_name, last_name, date_of_birth, team69_fk) VALUES (463, 0, 'Raul', 'Weaver', '1967-02-21', 1);
INSERT INTO player69 (id, version, first_name, last_name, date_of_birth, team69_fk) VALUES (464, 0, 'Dan', 'Briggs', '1986-05-05', 2);
INSERT INTO player69 (id, version, first_name, last_name, date_of_birth, team69_fk) VALUES (465, 0, 'Dave', 'Pratt', '1975-09-19', 3);
INSERT INTO player69 (id, version, first_name, last_name, date_of_birth, team69_fk) VALUES (466, 0, 'Dale', 'Rhodes', '1966-10-12', 4);
INSERT INTO player69 (id, version, first_name, last_name, date_of_birth, team69_fk) VALUES (467, 0, 'Marvin', 'Vaughn', '1982-07-08', 5);
INSERT INTO player69 (id, version, first_name, last_name, date_of_birth, team69_fk) VALUES (468, 0, 'Jay', 'Owens', '1976-06-18', 6);
INSERT INTO player69 (id, version, first_name, last_name, date_of_birth, team69_fk) VALUES (469, 0, 'Wendell', 'Page', '1988-12-12', 7);
INSERT INTO player69 (id, version, first_name, last_name, date_of_birth, team69_fk) VALUES (470, 0, 'Leonard', 'Chapman', '1988-11-14', 8);
INSERT INTO player69 (id, version, first_name, last_name, date_of_birth, team69_fk) VALUES (471, 0, 'Doyle', 'Clarke', '1982-09-01', 9);
INSERT INTO player69 (id, version, first_name, last_name, date_of_birth, team69_fk) VALUES (472, 0, 'Jesus', 'Goodwin', '1975-11-25', 10);
INSERT INTO player69 (id, version, first_name, last_name, date_of_birth, team69_fk) VALUES (474, 0, 'Jaime', 'Mccormick', '1969-10-27', 1);
INSERT INTO player69 (id, version, first_name, last_name, date_of_birth, team69_fk) VALUES (475, 0, 'Rex', 'Lee', '1978-05-11', 2);
INSERT INTO player69 (id, version, first_name, last_name, date_of_birth, team69_fk) VALUES (476, 0, 'Malcolm', 'Chavez', '1973-07-09', 3);
INSERT INTO player69 (id, version, first_name, last_name, date_of_birth, team69_fk) VALUES (477, 0, 'Martin', 'Boyd', '1965-02-16', 4);
INSERT INTO player69 (id, version, first_name, last_name, date_of_birth, team69_fk) VALUES (478, 0, 'Joey', 'Roberson', '1985-12-18', 5);
INSERT INTO player69 (id, version, first_name, last_name, date_of_birth, team69_fk) VALUES (479, 0, 'Enrique', 'Young', '1976-07-26', 6);
INSERT INTO player69 (id, version, first_name, last_name, date_of_birth, team69_fk) VALUES (480, 0, 'Jake', 'Lane', '1971-10-13', 7);
INSERT INTO player69 (id, version, first_name, last_name, date_of_birth, team69_fk) VALUES (481, 0, 'Preston', 'Brown', '1969-04-08', 8);
INSERT INTO player69 (id, version, first_name, last_name, date_of_birth, team69_fk) VALUES (483, 0, 'Rex', 'Daniel', '1983-11-20', 9);
INSERT INTO player69 (id, version, first_name, last_name, date_of_birth, team69_fk) VALUES (484, 0, 'Neal', 'Bush', '1986-03-13', 10);
INSERT INTO player69 (id, version, first_name, last_name, date_of_birth, team69_fk) VALUES (486, 0, 'Omar', 'Gordon', '1981-12-23', 1);
INSERT INTO player69 (id, version, first_name, last_name, date_of_birth, team69_fk) VALUES (487, 0, 'Ronnie', 'Scott', '1965-02-16', 2);
INSERT INTO player69 (id, version, first_name, last_name, date_of_birth, team69_fk) VALUES (488, 0, 'Corey', 'Rice', '1967-03-20', 3);
INSERT INTO player69 (id, version, first_name, last_name, date_of_birth, team69_fk) VALUES (489, 0, 'Everett', 'Fields', '1976-03-27', 4);
INSERT INTO player69 (id, version, first_name, last_name, date_of_birth, team69_fk) VALUES (490, 0, 'Timothy', 'Payne', '1987-02-25', 5);
INSERT INTO player69 (id, version, first_name, last_name, date_of_birth, team69_fk) VALUES (492, 0, 'Darin', 'Lambert', '1986-12-10', 6);
INSERT INTO player69 (id, version, first_name, last_name, date_of_birth, team69_fk) VALUES (494, 0, 'Lance', 'Mitchell', '1985-02-07', 7);


ALTER TABLE team69 ADD CONSTRAINT team69_pkey PRIMARY KEY (id);

ALTER TABLE player69 ADD CONSTRAINT player69_pkey PRIMARY KEY (id);

