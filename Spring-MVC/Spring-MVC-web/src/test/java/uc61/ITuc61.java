package uc61;


import org.junit.Test;
import test.common.AbstractBADocTestCase;

public class ITuc61 extends AbstractBADocTestCase{
	
	@Override
	public  String getName(){
		return "UC-61";
	}	

	@Test
	public void testUC61() throws Exception {
		// On teste l'UC61 - Standard Features/Entities - Transient Object
				
		selenium.open("");	
		selenium.click("link=Expand All");
		selenium.waitForPageToLoad("30000");
		selenium.click("link=UC61 - Transient Object");
		
		verifyTrue(selenium.isElementPresent("//h2[contains(text(),\"Standard Features > Entities > UC61 > Transient Object\")]"));
		// On cr�e des �quipes.
		// On ne donne pas d'ID � la premi�re �quipe.
		selenium.type("//*[contains(@id,'txt_city')]", "Tulsa");
		selenium.type("//*[contains(@id,'txt_name')]", "Vikings");
		selenium.click("//*[contains(@id,'lnk_addteam')]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie que le message d'erreur s'affiche correctement.
		assertTrue(selenium.isElementPresent("//div[contains(@id,'Errors')]"));
		assertEquals("Please enter an ID for the team to create.", selenium.getText("//div[contains(@id,'Errors')]"));
		assertEquals("[back]", selenium.getText("//*[contains(@id,'btn_back')]"));
		selenium.click("//*[contains(@id,'btn_back')]");
		selenium.waitForPageToLoad("30000");
		// On cr�e une seconde �quipe avec ID.
		selenium.type("//*[contains(@id,'txt_id')]", "1");
		selenium.type("//*[contains(@id,'txt_city')]", "Charlotte");
		selenium.type("//*[contains(@id,'txt_name')]", "Blue Devils");
		selenium.click("//*[contains(@id,'lnk_addteam')]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie qu'elle s'affiche correctement.
		assertTrue(selenium.isElementPresent("//table[contains(@id,'listTeam')]/tbody/tr[td/span[contains(text(),\"1\")] and td/span[contains(text(),\"Blue Devils\")] and td/span[contains(text(),\"Charlotte\")]]"));
		// On cr�e une �quipe avec une ID identique.
		selenium.type("//*[contains(@id,'txt_id')]", "1");
		selenium.type("//*[contains(@id,'txt_city')]", "Charlotte");
		selenium.type("//*[contains(@id,'txt_name')]", "Tigers");
		selenium.click("//*[contains(@id,'lnk_addteam')]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie que le message d'erreur d'affiche correctement.
		assertTrue(selenium.isElementPresent("//div[contains(@id,'Errors')]"));
		assertEquals("A team with the same id already exists.", selenium.getText("//div[contains(@id,'Errors')]"));
		selenium.click("//*[contains(@id,'btn_back')]");
		selenium.waitForPageToLoad("30000");
		// On cr�e une nouvelle �quipe.
		selenium.type("//*[contains(@id,'txt_id')]", "2");
		selenium.type("//*[contains(@id,'txt_city')]", "Charlotte");
		selenium.type("//*[contains(@id,'txt_name')]", "Tigers");
		selenium.click("//*[contains(@id,'lnk_addteam')]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie qu'elle s'affiche correctement.
		assertTrue(selenium.isElementPresent("//table[contains(@id,'listTeam')]/tbody/tr[td/span[contains(text(),\"2\")] and td/span[contains(text(),\"Tigers\")] and td/span[contains(text(),\"Charlotte\")]]"));
		// On cr�e une �quipe avec un nom identique � une des p�r�centes. 
		selenium.type("//*[contains(@id,'txt_id')]", "3");
		selenium.type("//*[contains(@id,'txt_city')]", "Cleveland");
		selenium.type("//*[contains(@id,'txt_name')]", "Tigers");
		selenium.click("//*[contains(@id,'lnk_addteam')]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie que le message d'erreur s'affiche bien.
		assertTrue(selenium.isElementPresent("//div[contains(@id,'Errors')]"));
		assertEquals("A team with the same name already exists.", selenium.getText("//div[contains(@id,'Errors')]"));
		assertEquals("[back]", selenium.getText("//*[contains(@id,'btn_back')]"));
		selenium.click("//*[contains(@id,'btn_back')]");
		selenium.waitForPageToLoad("30000");
		// On cr�e une �quipe.
		selenium.type("//*[contains(@id,'txt_id')]", "4");
		selenium.type("//*[contains(@id,'txt_city')]", "Cleveland");
		selenium.type("//*[contains(@id,'txt_name')]", "Mustangs");
		selenium.click("//*[contains(@id,'lnk_addteam')]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie qu'elle s'affiche correctement.
		assertTrue(selenium.isElementPresent("//table[contains(@id,'listTeam')]/tbody/tr[td/span[contains(text(),\"4\")] and td/span[contains(text(),\"Mustangs\")] and td/span[contains(text(),\"Cleveland\")]]"));
		// On compte le nombre de lignes.
		verifyEquals("3", selenium.getXpathCount("//table[contains(@id,'listTeam')]//tbody//tr"));
	}


}
