/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.service.gettingstarted.uc05_search.daofinder;


import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.Logger;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateTemplate;

import com.bluage.documentation.business.gettingstarted.uc05_search.bos.Player05BO;
import com.netfective.bluage.core.exception.ApplicationException;


/**
 * This class represents query invocation methods implementation
 * for ServiceSearch service.
 *
 * @see com.bluage.documentation.service.gettingstarted.uc05_search.ServiceSearch
 */
 
@Named ("com.bluage.documentation.service.gettingstarted.uc05_search.daofinder.ServiceSearchDAOMethodFinderImpl")
public class ServiceSearchDAOMethodFinderImpl {

	// LOGGER for ServiceSearchDAOMethodFinderImpl
	private static final Logger LOGGER = Logger.getLogger(ServiceSearchDAOMethodFinderImpl.class);
	
	/**
	* Hibernate Template (injected)
	*/
	@Inject
	private HibernateTemplate hibernateTemplate;

	
	/**
	 * setter for HibernateTemplate
	 * @param value : {@link HibernateTemplate}
	 */
	public void setHibernateTemplate( HibernateTemplate value )
	{
		hibernateTemplate = value;
	}
	

	/**    
	 * HQL operation.
	 * @param id {@link Long}
		 * @return {@link Player05BO}
	 */
	public Player05BO player05FindByID(Long id )
 throws ApplicationException {
Session session = hibernateTemplate.getSessionFactory().getCurrentSession();
			return 
				privatePlayer05FindByID(id, session);
	}

	
	/**    
	 * private delegation for HQL operation.
	 * @param id {@link Long}
		 * @return {@link Player05BO}
	 */
	private Player05BO privatePlayer05FindByID(Long id , Session session)
 throws ApplicationException {
			Query query = session.createQuery(" SELECT p FROM Player05BO p JOIN FETCH p.position s WHERE p.id =:id ");
query.setParameter( "id", id, Hibernate.LONG);
														Player05BO returnValue = null;
		try {
			Object result = query.uniqueResult();
			if( result != null ) {
				returnValue = ( com.bluage.documentation.business.gettingstarted.uc05_search.bos.Player05BO )result;
			}				
		} catch(HibernateException ex) {
			LOGGER.error("An error occured while excecuting the HQL operation : com.bluage.documentation.service.gettingstarted.uc05_search.player05FindByID");
			throw new ApplicationException(ex);
		}
		return returnValue;
	}
	
	/**    
	 * HQL operation.
	 * @param lastName {@link String}
		 * @param minHeight {@link Integer}
		 * @param maxHeight {@link Integer}
		 * @return {@link List}
	 */
	public List player05SearchByCriteria(String lastName,Integer minHeight,Integer maxHeight )
 throws ApplicationException {
Session session = hibernateTemplate.getSessionFactory().getCurrentSession();
			return 
				privatePlayer05SearchByCriteria(lastName,minHeight,maxHeight, session);
	}

	
	/**    
	 * private delegation for HQL operation.
	 * @param lastName {@link String}
		 * @param minHeight {@link Integer}
		 * @param maxHeight {@link Integer}
		 * @return {@link List}
	 */
	private List privatePlayer05SearchByCriteria(String lastName,Integer minHeight,Integer maxHeight , Session session)
 throws ApplicationException {
			Query query = session.createQuery(" SELECT p FROM Player05BO p LEFT JOIN FETCH p.position s WHERE p.lastName like :lastName and p.height between :minHeight and :maxHeight ");
query.setParameter( "lastName", lastName, Hibernate.STRING);
											query.setParameter( "minHeight", minHeight, Hibernate.INTEGER);
											query.setParameter( "maxHeight", maxHeight, Hibernate.INTEGER);
														List returnValue = null;
		try {
			returnValue = query.list();
							} catch(HibernateException ex) {
			LOGGER.error("An error occured while excecuting the HQL operation : com.bluage.documentation.service.gettingstarted.uc05_search.player05SearchByCriteria");
			throw new ApplicationException(ex);
		}
		return returnValue;
	}

	




}
