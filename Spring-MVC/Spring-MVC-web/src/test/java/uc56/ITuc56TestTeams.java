package uc56;


import org.junit.Test;
import test.common.AbstractBADocTestCase;

public class ITuc56TestTeams extends AbstractBADocTestCase {

	@Override
	public  String getName(){
		return "UC-56";
	}	


	@Test
	public void  iTuc56testTeams() throws Exception {
		// On teste l'UC56 - Standard Features/Services/SQL - SQL Operation.
		selenium.open("");	
		selenium.click("link=Expand All");
		selenium.waitForPageToLoad("30000");
		selenium.click("link=UC56 - SQL Operation");
		verifyTrue(selenium.isElementPresent("//h2[contains(text(),\"Standard Features > Services > SQL > UC56 > SQL Operation\")]"));
		// On teste le champ de recherche.
		selenium.type("//*[contains(@id,'txt_name')]", "Chargers");
		selenium.click("//a[contains(@id,'lnk_searchTeams')]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_searchResult')]/tbody/tr[td/span[contains(text(),\"Chargers\")] and td/span[contains(text(),\"Detroit\")] and td/span[contains(text(),\"Arkansas\")]]"));
		selenium.type("//*[contains(@id,'txt_name')]", "Braves");
		selenium.click("//*[contains(@id,'lnk_searchTeams')]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_searchResult')]/tbody/tr[td/span[contains(text(),\"Braves\")] and td/span[contains(text(),\"Omaha\")] and td/span[contains(text(),\"Arkansas\")]]"));
		// On vérifie que toutes les données Teams s'affichent.
		selenium.type("//*[contains(@id,'txt_name')]", "");
		selenium.click("//a[contains(@id,'lnk_searchTeams')]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_searchResult')]/tbody//tr[td//span[contains(text(),\"Hawks\")] and td//span[contains(text(),\"Detroit\")] and td//span[contains(text(),\"Alaska\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_searchResult')]/tbody//tr[td//span[contains(text(),\"Tigers\")] and td//span[contains(text(),\"Charlotte\")] and td//span[contains(text(),\"Alaska\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_searchResult')]/tbody//tr[td//span[contains(text(),\"Bears\")] and td//span[contains(text(),\"Kansas City\")] and td//span[contains(text(),\"Alabama\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_searchResult')]/tbody//tr[td//span[contains(text(),\"Braves\")] and td//span[contains(text(),\"Omaha\")] and td//span[contains(text(),\"Arkansas\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_searchResult')]/tbody//tr[td//span[contains(text(),\"Chargers\")] and td//span[contains(text(),\"Detroit\")] and td//span[contains(text(),\"Arkansas\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_searchResult')]/tbody//tr[td//span[contains(text(),\"Eagles\")] and td//span[contains(text(),\"Raleigh\")] and td//span[contains(text(),\"California\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_searchResult')]/tbody//tr[td//span[contains(text(),\"Cardinals\")] and td//span[contains(text(),\"Oakland\")] and td//span[contains(text(),\"Colorado\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_searchResult')]/tbody//tr[td//span[contains(text(),\"Red Devils\")] and td//span[contains(text(),\"Kansas City\")] and td//span[contains(text(),\"Connecticut\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_searchResult')]/tbody//tr[td//span[contains(text(),\"Mustangs\")] and td//span[contains(text(),\"Fresno\")] and td//span[contains(text(),\"Delaware\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_searchResult')]/tbody//tr[td//span[contains(text(),\"Wolverines\")] and td//span[contains(text(),\"Sacramento\")] and td//span[contains(text(),\"Florida\")]]"));
	
	
	}


}
