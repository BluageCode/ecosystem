/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.netfective.bluage.core.vo;

import java.lang.reflect.InvocationTargetException;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.beanutils.Converter;
import org.apache.commons.beanutils.converters.ByteConverter;
import org.apache.commons.beanutils.converters.DoubleConverter;
import org.apache.commons.beanutils.converters.FloatConverter;
import org.apache.commons.beanutils.converters.IntegerConverter;
import org.apache.commons.beanutils.converters.LongConverter;
import org.apache.commons.beanutils.converters.ShortConverter;

import com.netfective.bluage.core.exception.ApplicationException;

/**
 * Copy Util
 * 
 * @author Netfective
 */
public final class CopyUtil {
	
	/**
	 * private constructor for CopyUtil
	 */
	private CopyUtil(){}	
	
	/**
	 * copyBeanToDest
	 * 
	 * @param source
	 * @param target
	 * @throws ApplicationException
	 */
	public static void copyBeanToDest(final Object source, final Object target)
			throws ApplicationException {
		final Converter dateConverter = ConvertUtils.lookup(java.util.Date.class);
		// #2929 timestamp management
		final Converter timestampConverter = ConvertUtils.lookup(java.sql.Timestamp.class);
		// #1912 Converter null management
		final Converter integerConverter = new IntegerConverter(null);
		final Converter doubleConverter = new DoubleConverter(null);
		final Converter floatConverter = new FloatConverter(null);
		final Converter byteConverter = new ByteConverter(null);
		final Converter longConverter = new LongConverter(null);
		final Converter shortConverter = new ShortConverter(null);
		
		try {
			ConvertUtils.deregister(java.util.Date.class);
			ConvertUtils.deregister(java.sql.Timestamp.class);
			BeanUtils.copyProperties(target, source);
		} catch (IllegalAccessException e) {
			throw new ApplicationException("Error copying object", e);
		} catch (InvocationTargetException e) {
			throw new ApplicationException("Error copying object", e);
		} finally {
			ConvertUtils.register(dateConverter, java.util.Date.class);
			ConvertUtils.register(timestampConverter, java.sql.Timestamp.class);
			// #1912
			ConvertUtils.register(integerConverter, java.lang.Integer.class);
			ConvertUtils.register(doubleConverter, java.lang.Double.class);
			ConvertUtils.register(floatConverter, java.lang.Float.class);
			ConvertUtils.register(byteConverter, java.lang.Byte.class);
			ConvertUtils.register(longConverter, java.lang.Long.class);
			ConvertUtils.register(shortConverter, java.lang.Short.class);
		}
	}
}
