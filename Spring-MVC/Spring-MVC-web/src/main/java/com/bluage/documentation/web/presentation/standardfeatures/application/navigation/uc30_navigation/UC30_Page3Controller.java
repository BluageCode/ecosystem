/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.application.navigation.uc30_navigation ;

// Import declaration.
// Java imports.
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.bluage.documentation.service.standardfeatures.application.navigation.uc30_navigation.ServiceNavigation;
import com.netfective.bluage.core.exception.ApplicationException;
import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;
import com.netfective.bluage.core.web.IForm;

/**
 * Class : UC30_Page3Controller
 */
@Controller
@Scope("prototype")
@SessionAttributes("uC30_Page3Form")
@RequestMapping(value= "/presentation/standardfeatures/application/navigation/uc30_navigation")
public class UC30_Page3Controller extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC30_Page3Controller.class);
	
	//Current form name
	private static final String U_C30__PAGE3_FORM = "uC30_Page3Form";

	
	/**
	 * Property:customDateEditorsUC30_Page3Controller
	 */
	final private Map<String,CustomDateEditor> customDateEditorsUC30_Page3Controller = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:uC30_Page3Validator
	 */
	final private UC30_Page3Validator uC30_Page3Validator = new UC30_Page3Validator();
	
	/**
	 * Service declaration : serviceNavigation.
	 */
	@Autowired
	private ServiceNavigation serviceNavigation;
	
	
	// Initialise all the instances for UC30_Page3Controller
			/**
	 * Operation : lnk_page1
 	 * @param model : 
 	 * @param uC30_Page3 : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC30_Page3/lnk_page1.html",method = RequestMethod.POST)
	public String lnk_page1(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC30_Page3Form") UC30_Page3Form  uC30_Page3Form, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : lnk_page1"); 
		infoLogger(uC30_Page3Form); 
		uC30_Page3Form.setAlwaysCallPreControllers(false); 
		// initialization for lnk_page1
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state lnk_page1 if the validation fail, you will be forward to the screen : "/presentation/standardfeatures/application/navigation/uc30_navigation/UC30_Page3"
		if(errorsMessages(request,response,uC30_Page3Form,bindingResult,"", uC30_Page3Validator, customDateEditorsUC30_Page3Controller)){ 
			return "/presentation/standardfeatures/application/navigation/uc30_navigation/UC30_Page3"; 
		}

	 callGobacktopage1(request,uC30_Page3Form );
											// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.application.navigation.uc30_navigation.UC30_Page1Form or not from lnk_page1
			final UC30_Page1Form uC30_Page1Form =  new UC30_Page1Form();
			// Populate the destination form with all the instances returned from lnk_page1.
			final IForm uC30_Page1Form2 = populateDestinationForm(request.getSession(), uC30_Page1Form); 
					
			// Add the form to the model.
			model.addAttribute("uC30_Page1Form", uC30_Page1Form2); 
			
			request.getSession().setAttribute("uC30_Page1Form", uC30_Page1Form2);
			
			// "OK" CASE => destination screen path from lnk_page1
			LOGGER.info("Go to the screen 'UC30_Page1'.");
			// Redirect (PRG) from lnk_page1
			destinationPath =  "redirect:/presentation/standardfeatures/application/navigation/uc30_navigation/UC30_Page1.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_page1 
	}
	
	
	/**
	* This method initialise the form : UC30_Page3Form 
	* @return UC30_Page3Form
	*/
	@ModelAttribute("UC30_Page3FormInit")
	public void initUC30_Page3Form(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, U_C30__PAGE3_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method UC30_Page3Form."); //for lnk_page1 
		}
		UC30_Page3Form uC30_Page3Form;
	
		if(request.getSession().getAttribute(U_C30__PAGE3_FORM) != null){
			uC30_Page3Form = (UC30_Page3Form)request.getSession().getAttribute(U_C30__PAGE3_FORM);
		} else {
			uC30_Page3Form = new UC30_Page3Form();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form UC30_Page3Form.");
		}
		uC30_Page3Form = (UC30_Page3Form)populateDestinationForm(request.getSession(), uC30_Page3Form);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  UC30_Page3Form.");
		}
		model.addAttribute(U_C30__PAGE3_FORM, uC30_Page3Form);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param UC30_Page3Form : The sceen form.
	 */
	@RequestMapping(value = "/UC30_Page3.html" ,method = RequestMethod.GET)
	public void prepareUC30_Page3(final HttpServletRequest request,final  Model model,final  @ModelAttribute("UC30_Page3FormInit") UC30_Page3Form uC30_Page3Form){
		
		UC30_Page3Form currentUC30_Page3Form = uC30_Page3Form;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(U_C30__PAGE3_FORM) == null){
			if(currentUC30_Page3Form!=null){
				request.getSession().setAttribute(U_C30__PAGE3_FORM, currentUC30_Page3Form);
			}else {
				currentUC30_Page3Form = new UC30_Page3Form();
				request.getSession().setAttribute(U_C30__PAGE3_FORM, currentUC30_Page3Form);	
			}
		} else {
			currentUC30_Page3Form = (UC30_Page3Form) request.getSession().getAttribute(U_C30__PAGE3_FORM);
		}

		currentUC30_Page3Form = (UC30_Page3Form)populateDestinationForm(request.getSession(), currentUC30_Page3Form);
		// Call all the Precontroller.
	currentUC30_Page3Form.setAlwaysCallPreControllers(true);
		model.addAttribute(U_C30__PAGE3_FORM, currentUC30_Page3Form);
		request.getSession().setAttribute(U_C30__PAGE3_FORM, currentUC30_Page3Form);
		
	}
	
	/**
	 * method callGobacktopage1
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callGobacktopage1(HttpServletRequest request,IForm form) {
					try {
				// executing Gobacktopage1 in lnk_page1
	serviceNavigation.doNothing(
	);  

								// processing variables Gobacktopage1 in lnk_page1

			} catch (ApplicationException e) { 
				// error handling for operation doNothing called Gobacktopage1
				errorLogger("An error occured during the execution of the operation : doNothing",e); 
		
			}
	}
		
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("UC30_Page3Controller [ ");
		// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(uC30_Page3Validator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsUC30_Page3Controller!=null); 
		return strBToS.toString();
	}
}
