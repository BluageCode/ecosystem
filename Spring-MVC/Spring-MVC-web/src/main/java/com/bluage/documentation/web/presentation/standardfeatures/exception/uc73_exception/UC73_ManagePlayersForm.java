/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.exception.uc73_exception;


import java.io.Serializable;
import java.util.List;

import com.bluage.documentation.business.standardfeatures.exception.uc73_exception.bos.Player73BO;
import com.bluage.documentation.business.standardfeatures.exception.uc73_exception.bos.Team73BO;
import com.netfective.bluage.core.web.AbstractForm;
import com.netfective.bluage.core.web.IForm;

/**
* Class : UC73_ManagePlayersForm
*/
public class UC73_ManagePlayersForm extends AbstractForm implements Serializable, IForm {
	
	/** Constant : serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	//CONSTANT : all_teams
	private static final String ALL_TEAMS = "all_teams";
	//CONSTANT : playersList
	private static final String PLAYERS_LIST = "playersList";
	//CONSTANT : selectedTeam
	private static final String SELECTED_TEAM = "selectedTeam";
	//CONSTANT : playerToDelete
	private static final String PLAYER_TO_DELETE = "playerToDelete";
	/**
	 * 	Property: all_teams 
	 */
	private List all_teams;
	/**
	 * 	Property: playersList 
	 */
	private List<Player73BO> playersList;
	/**
	 * 	Property: selectedTeam 
	 */
	private Team73BO selectedTeam;
	/**
	 * 	Property: playerToDelete 
	 */
	private Player73BO playerToDelete;
/**
	 * Default constructor : UC73_ManagePlayersForm
	 */
	public UC73_ManagePlayersForm() {
		super();
		// Initialize : all_teams
		this.all_teams = new java.util.ArrayList();
		// Initialize : playersList
		this.playersList = new java.util.ArrayList<com.bluage.documentation.business.standardfeatures.exception.uc73_exception.bos.Player73BO>();
		// Initialize : selectedTeam
		this.selectedTeam = new Team73BO();
		// Initialize : playerToDelete
		this.playerToDelete = null;
					this.setAlwaysCallPreControllers(true);
	}
	
	/**
	 * 	Getter : all_teams 
	 * 	@return : Return the all_teams instance.
	 */
	public List getAll_teams(){
		return all_teams; // For UC73_ManagePlayersForm
	}
	
	/**
	 * 	Setter : all_teams 
	 *  @param all_teamsinstance : The instance to set.
	 */
	public void setAll_teams(final List all_teamsinstance){
		this.all_teams = all_teamsinstance;// For UC73_ManagePlayersForm
	}
	/**
	 * 	Getter : playersList 
	 * 	@return : Return the playersList instance.
	 */
	public List<Player73BO> getPlayersList(){
		return playersList; // For UC73_ManagePlayersForm
	}
	
	/**
	 * 	Setter : playersList 
	 *  @param playersListinstance : The instance to set.
	 */
	public void setPlayersList(final List<Player73BO> playersListinstance){
		this.playersList = playersListinstance;// For UC73_ManagePlayersForm
	}
	/**
	 * 	Getter : selectedTeam 
	 * 	@return : Return the selectedTeam instance.
	 */
	public Team73BO getSelectedTeam(){
		return selectedTeam; // For UC73_ManagePlayersForm
	}
	
	/**
	 * 	Setter : selectedTeam 
	 *  @param selectedTeaminstance : The instance to set.
	 */
	public void setSelectedTeam(final Team73BO selectedTeaminstance){
		this.selectedTeam = selectedTeaminstance;// For UC73_ManagePlayersForm
	}
	/**
	 * 	Getter : playerToDelete 
	 * 	@return : Return the playerToDelete instance.
	 */
	public Player73BO getPlayerToDelete(){
		return playerToDelete; // For UC73_ManagePlayersForm
	}
	
	/**
	 * 	Setter : playerToDelete 
	 *  @param playerToDeleteinstance : The instance to set.
	 */
	public void setPlayerToDelete(final Player73BO playerToDeleteinstance){
		this.playerToDelete = playerToDeleteinstance;// For UC73_ManagePlayersForm
	}
/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UC73_ManagePlayersForm [ "+
ALL_TEAMS +" = " + all_teams +PLAYERS_LIST +" = " + playersList +SELECTED_TEAM +" = " + selectedTeam +PLAYER_TO_DELETE +" = " + playerToDelete + "]";
	}
	
	/**
	 * Method setInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public void setInstanceByInstanceName(final String instanceName, final Object instance) {
		// Set the instance All_teams.
		if(ALL_TEAMS.equals(instanceName)){// For UC73_ManagePlayersForm
			this.setAll_teams((List)instance); // For UC73_ManagePlayersForm
		}
				// Set the instance PlayersList.
		if(PLAYERS_LIST.equals(instanceName)){// For UC73_ManagePlayersForm
			this.setPlayersList((List<Player73BO>)instance); // For UC73_ManagePlayersForm
		}
				// Set the instance SelectedTeam.
		if(SELECTED_TEAM.equals(instanceName)){// For UC73_ManagePlayersForm
			this.setSelectedTeam((Team73BO)instance); // For UC73_ManagePlayersForm
		}
				// Set the instance PlayerToDelete.
		if(PLAYER_TO_DELETE.equals(instanceName)){// For UC73_ManagePlayersForm
			this.setPlayerToDelete((Player73BO)instance); // For UC73_ManagePlayersForm
		}
			}
	
	/**
	 * Method getInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public Object getInstanceByInstanceName(final String instanceName){
		// Temporary object for tmpUC73_ManagePlayersForm.
		Object tmpUC73_ManagePlayersForm = null;
		
			
		// Get the instance All_teams for UC73_ManagePlayersForm.
		if(ALL_TEAMS.equals(instanceName)){ // For UC73_ManagePlayersForm
			tmpUC73_ManagePlayersForm = this.getAll_teams(); // For UC73_ManagePlayersForm
		}
			
		// Get the instance PlayersList for UC73_ManagePlayersForm.
		if(PLAYERS_LIST.equals(instanceName)){ // For UC73_ManagePlayersForm
			tmpUC73_ManagePlayersForm = this.getPlayersList(); // For UC73_ManagePlayersForm
		}
			
		// Get the instance SelectedTeam for UC73_ManagePlayersForm.
		if(SELECTED_TEAM.equals(instanceName)){ // For UC73_ManagePlayersForm
			tmpUC73_ManagePlayersForm = this.getSelectedTeam(); // For UC73_ManagePlayersForm
		}
			
		// Get the instance PlayerToDelete for UC73_ManagePlayersForm.
		if(PLAYER_TO_DELETE.equals(instanceName)){ // For UC73_ManagePlayersForm
			tmpUC73_ManagePlayersForm = this.getPlayerToDelete(); // For UC73_ManagePlayersForm
		}
		return tmpUC73_ManagePlayersForm;// For UC73_ManagePlayersForm
	}
	
			}
