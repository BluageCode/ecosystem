/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.web.presentation.standardfeatures.application.navigation.uc31_preaction;

// Import declaration.
// Java imports.
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import com.bluage.documentation.service.standardfeatures.application.navigation.uc31_preaction.ServicePlayer31FindAll;
import com.netfective.bluage.core.exception.ApplicationException;
import com.netfective.bluage.core.web.AbstractController;
import com.netfective.bluage.core.web.IForm;

/**
 * Class : UC31_PreControllerDisplayPlayersController
 */
 @Service("com.bluage.documentation.web.presentation.standardfeatures.application.navigation.uc31_preaction.UC31_PreControllerDisplayPlayersController")
public class UC31_PreControllerDisplayPlayersController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC31_PreControllerDisplayPlayersController.class);
	
	//CONSTANT PINS: playersList
	private static final String PLAYERS_LIST = "playersList";
	
	// Declaring all the instances.
		List playersList;
	/**
	 * Service declaration : servicePlayer31FindAll.
	 */
	@Autowired
	private ServicePlayer31FindAll servicePlayer31FindAll;
	
	/**
	 * Operation : UC31_PreControllerDisplayPlayersControllerInit
	 * @param request : The current HttpRequest
 	 * @param model :  The current Model
 	 * @param currentIForm : The current IForm
 	 * @return
	 */
	public IForm uC31_PreControllerDisplayPlayersControllerInit(final HttpServletRequest request,final  Model model,final  IForm  currentIForm){

		LOGGER.info("Begin the precontroller method : findPlayers");
		LOGGER.info("Form diagnostic : " + currentIForm);
		

	callFindAllPlayers(request,currentIForm);
		
			LOGGER.info("Populate the destination screen.");
			// Populate the destination form with all the instances returned from an executed service.
			return populateDestinationForm(request.getSession(), currentIForm); // Populate the destination screen

	}
	/**
	 * method callFindAllPlayers
	 * 
	 * @param request HttpServletRequest
	 * @param currentIForm IForm
	 */
	private void callFindAllPlayers(HttpServletRequest request, IForm currentIForm) {
		// Initialize all the instances.
		playersList = (List)get(request.getSession(),currentIForm,PLAYERS_LIST);
							try {

				playersList = 	servicePlayer31FindAll.player31FindAll(
	);  

				put(request.getSession(), PLAYERS_LIST,playersList);
							
			} catch (ApplicationException e) {
				LOGGER.error("An error occured during the execution of the operation : player31FindAll",e);
			}
	}
}
