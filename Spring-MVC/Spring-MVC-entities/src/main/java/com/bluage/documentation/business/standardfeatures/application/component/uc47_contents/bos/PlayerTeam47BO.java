/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.business.standardfeatures.application.component.uc47_contents.bos;

import javax.inject.Inject;
import javax.inject.Named;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import com.bluage.documentation.business.AbstractBusinessObject;
import com.bluage.documentation.business.standardfeatures.application.component.uc47_contents.entities.daofinder.PlayerTeam47DAOFinderImpl;

/**
 * PlayerTeam47BO
 *
* @see com.netfective.bluage.core.business.IBusinessObject
 */
@XmlRootElement

public class PlayerTeam47BO
 extends AbstractBusinessObject {
	
	/**
	 * Default SUID for PlayerTeam47BO.
	 */
	private static final long serialVersionUID = 1L;
	

	@Inject
	@Named ("com.bluage.documentation.business.standardfeatures.application.component.uc47_contents.entities.daofinder.PlayerTeam47Finder") 
	private PlayerTeam47DAOFinderImpl finder;
	/**
	 * Hibernate optimistic lock for class PlayerTeam47BO.
	 */
	private int version;

						/**
	 * Entity property: id
	 *
	 */
private Long id;

											/**
	 * Entity property: player
	 *
	 */
private Player47BO player;

											/**
	 * Entity property: team
	 *
	 */
private Team47BO team;



   /**
	* public default constructor for class PlayerTeam47BO
	*/
	public PlayerTeam47BO(){
		//NOP PlayerTeam47BO
	}

	
	/**
	 * Getter for version for class PlayerTeam47BO
	 * @return int
	 */
	public int getVersion() {
		return this.version;
	}
	
	/**
	 * Setter for version for class PlayerTeam47BO
	 * @param int version
	 */
	public void setVersion(final int version) {
		this.version = version;
	}

						/**
	 * Getter for id of class PlayerTeam47BO.
	 *
	 * @return {@link java.lang.Long}
	 * @generated
	 */ 
public  Long getId() {
	return this.id;
	}


	/**
	 * Setter for id of class PlayerTeam47BO.
	 * @param value {@link java.lang.Long}
	 * @generated
	 */

public  void setId(final Long id) {
	this.isEmpty = (id == null);
	this.id = id;
}

											/**
	 * Getter for player of class PlayerTeam47BO.
	 *
	 * @return {@link com.bluage.documentation.business.standardfeatures.application.component.uc47_contents.bos.Player47BO}
	 * @generated
	 */ 
public  Player47BO getPlayer() {
		Player47BO player0 = this.player;
		if(this.player == null) {
			if(DAO_MODE.equals(this.getObjMode( ) ) ) {
				player0 = null;
			} else {
				this.player = new Player47BO();
				this.player.setObjMode( this.getObjMode( ) );
				player0 = this.player;
			}
		} else {
			this.player.setObjMode( this.getObjMode( ) );
			if( HELPER_MODE.equals(this.getObjMode( ) ) ) {
				player0 = this.player;
			}
			if( DAO_MODE.equals(this.getObjMode()) && this.player.isEmpty ) {
				player0 = null;
			}
		}
		return player0;
	}


	/**
	 * Setter for player of class PlayerTeam47BO.
	 * @param value {@link com.bluage.documentation.business.standardfeatures.application.component.uc47_contents.bos.Player47BO}
	 * @generated
	 */

public  void setPlayer(final Player47BO player) {
	if(player != null) {
		player.isEmpty = false;
		player.setObjMode( this.getObjMode( ) );
	}
	this.isEmpty = false;
	this.player = player;
}

											/**
	 * Getter for team of class PlayerTeam47BO.
	 *
	 * @return {@link com.bluage.documentation.business.standardfeatures.application.component.uc47_contents.bos.Team47BO}
	 * @generated
	 */ 
public  Team47BO getTeam() {
		Team47BO team0 = this.team;
		if(this.team == null) {
			if(DAO_MODE.equals(this.getObjMode( ) ) ) {
				team0 = null;
			} else {
				this.team = new Team47BO();
				this.team.setObjMode( this.getObjMode( ) );
				team0 = this.team;
			}
		} else {
			this.team.setObjMode( this.getObjMode( ) );
			if( HELPER_MODE.equals(this.getObjMode( ) ) ) {
				team0 = this.team;
			}
			if( DAO_MODE.equals(this.getObjMode()) && this.team.isEmpty ) {
				team0 = null;
			}
		}
		return team0;
	}


	/**
	 * Setter for team of class PlayerTeam47BO.
	 * @param value {@link com.bluage.documentation.business.standardfeatures.application.component.uc47_contents.bos.Team47BO}
	 * @generated
	 */

public  void setTeam(final Team47BO team) {
	if(team != null) {
		team.isEmpty = false;
		team.setObjMode( this.getObjMode( ) );
	}
	this.isEmpty = false;
	this.team = team;
}


/**
	 * Returns <code>true</code> if the argument is an PlayerTeam47 instance and all identifiers for this entity
     * equal the identifiers of the argument entity. Returns <code>false</code> otherwise.
	 * @param object  object to compare with current instance      
     * @return boolean true if equals false if not
     */
    public boolean equals(final Object object )
    {
		if ( this == object )
        {
            return true;
        }
        if ( !( object instanceof PlayerTeam47BO ) )
        {
            return false;
        }
        final PlayerTeam47BO that = (PlayerTeam47BO)object;
    
    	            if (        				        			this.getId() == null
        	        ) {
        	return false;
        }
		return new EqualsBuilder()
			.append(this.getId(), that.getId())
			 	.appendSuper(super.equals(object))
		 	.isEquals();
	}
	/**
     * Returns a hash code based on this entity's identifiers.     * @return int
     */    
    public int hashCode() {
        return new HashCodeBuilder()
             .append(getId())
		            .toHashCode();
    }
}

