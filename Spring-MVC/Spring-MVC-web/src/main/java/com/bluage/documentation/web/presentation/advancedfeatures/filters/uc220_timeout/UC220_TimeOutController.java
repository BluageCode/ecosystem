/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.advancedfeatures.filters.uc220_timeout ;

// Import declaration.
// Java imports.
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;

/**
 * Class : UC220_TimeOutController
 */
@Controller
@Scope("prototype")
@SessionAttributes("uC220_TimeOutForm")
@RequestMapping(value= "/presentation/advancedfeatures/filters/uc220_timeout")
public class UC220_TimeOutController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC220_TimeOutController.class);
	
	//Current form name
	private static final String U_C220__TIME_OUT_FORM = "uC220_TimeOutForm";

	
	/**
	 * Property:customDateEditorsUC220_TimeOutController
	 */
	final private Map<String,CustomDateEditor> customDateEditorsUC220_TimeOutController = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:uC220_TimeOutValidator
	 */
	final private UC220_TimeOutValidator uC220_TimeOutValidator = new UC220_TimeOutValidator();
	
	
	// Initialise all the instances for UC220_TimeOutController

	/**
	* This method initialise the form : UC220_TimeOutForm 
	* @return UC220_TimeOutForm
	*/
	@ModelAttribute("UC220_TimeOutFormInit")
	public void initUC220_TimeOutForm(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, U_C220__TIME_OUT_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method UC220_TimeOutForm."); //for lnk_back 
		}
		UC220_TimeOutForm uC220_TimeOutForm;
	
		if(request.getSession().getAttribute(U_C220__TIME_OUT_FORM) != null){
			uC220_TimeOutForm = (UC220_TimeOutForm)request.getSession().getAttribute(U_C220__TIME_OUT_FORM);
		} else {
			uC220_TimeOutForm = new UC220_TimeOutForm();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form UC220_TimeOutForm.");
		}
		uC220_TimeOutForm = (UC220_TimeOutForm)populateDestinationForm(request.getSession(), uC220_TimeOutForm);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  UC220_TimeOutForm.");
		}
		model.addAttribute(U_C220__TIME_OUT_FORM, uC220_TimeOutForm);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param UC220_TimeOutForm : The sceen form.
	 */
	@RequestMapping(value = "/UC220_TimeOut.html" ,method = RequestMethod.GET)
	public void prepareUC220_TimeOut(final HttpServletRequest request,final  Model model,final  @ModelAttribute("UC220_TimeOutFormInit") UC220_TimeOutForm uC220_TimeOutForm){
		
		UC220_TimeOutForm currentUC220_TimeOutForm = uC220_TimeOutForm;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(U_C220__TIME_OUT_FORM) == null){
			if(currentUC220_TimeOutForm!=null){
				request.getSession().setAttribute(U_C220__TIME_OUT_FORM, currentUC220_TimeOutForm);
			}else {
				currentUC220_TimeOutForm = new UC220_TimeOutForm();
				request.getSession().setAttribute(U_C220__TIME_OUT_FORM, currentUC220_TimeOutForm);	
			}
		} else {
			currentUC220_TimeOutForm = (UC220_TimeOutForm) request.getSession().getAttribute(U_C220__TIME_OUT_FORM);
		}

		currentUC220_TimeOutForm = (UC220_TimeOutForm)populateDestinationForm(request.getSession(), currentUC220_TimeOutForm);
		// Call all the Precontroller.
	currentUC220_TimeOutForm.setAlwaysCallPreControllers(true);
		model.addAttribute(U_C220__TIME_OUT_FORM, currentUC220_TimeOutForm);
		request.getSession().setAttribute(U_C220__TIME_OUT_FORM, currentUC220_TimeOutForm);
		
	}
	
	
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("UC220_TimeOutController [ ");
		// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(uC220_TimeOutValidator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsUC220_TimeOutController!=null); 
		return strBToS.toString();
	}
}
