<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file="/include.jsp"  %>
<html >
<head >
<title  > 
<fmt:message key="cUC73_ManagePlayers_BLU_AGE_DOCUMENTATION"/>
</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"  > 
</meta>
<link href="<c:url value= "/presentation/_style/_style.css"/>" rel="stylesheet" type="text/css"  /> 
<link href="<c:url value= "/presentation/_include/jquery/rd_lib.css"/>" rel="stylesheet" type="text/css"  /> 
<script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/jquery.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/rd_setup.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/rd_lib.js"/>" space="preserve"  ></script><link rel="shortcut icon" href="<c:url value= "/presentation/_style/favicon.ico"/>"  /> 

</head>

<body class=" " > 
<div id="rd_ghost"  > 
<small  > 
<fmt:message key="cUC73_ManagePlayers___CSS_DEACTIVATED"/>
</small>
</div>
<div id="rd_jghost"  > 
<small  > 
<fmt:message key="cUC73_ManagePlayers___JAVASCRIPT_DEACTIVATED"/>
</small>
</div>
<div id="shape"  > 
<div class="ui-layout-center"  > 
<div class="east-north"  > 
	<form:form id="uC73_ManagePlayers" commandName="uC73_ManagePlayersForm"  enctype="application/x-www-form-urlencoded" method="post">
<h2 class="rd_first"  > 
<fmt:message key="cUC73_ManagePlayers_STANDARD_FEATURES__EXCEPTION_"/>
</h2>
<fmt:message key="cUC73_ManagePlayers___TEAM__"/>
<form:select id="select_team" path="selectedTeam.id"   
	onchange="<%=\"btn_click_cExecuteUC73_ManagePlayers('\"+request.getContextPath()+\"/presentation/standardfeatures/exception/uc73_exception/UC73_ManagePlayers/select_team.html');\"%>"
		>
		<form:option value="" label="---"/>
	<form:options items="${uC73_ManagePlayersForm.all_teams}" itemValue="id" itemLabel="name" />
</form:select>
<br clear="none"  /> 
<br clear="none"  /> 
<display:table uid="tab_searchResult"  sort="list" requestURI="" name="sessionScope.uC73_ManagePlayersForm.playersList" class="rd_tabledata"  >
	<c:set   var="rownum">
		<%=(tab_searchResult !=	null) ? new Integer(tab_searchResult_rowNum.intValue() - 1) : new Integer(0) %>
	</c:set>
	<c:set   var="idRow">
		<%=(tab_searchResult != null ) ? tab_searchResult_rowNum.intValue() - 1 : -1 %>
	</c:set>
	<c:set   var="idTab">
		<%=(tab_searchResult != null ) ? "tab_searchResult" : null %>
	</c:set>
	<c:set var="tableHashId">
		<%= new org.displaytag.util.ParamEncoder("tab_searchResult").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE) %>
	</c:set>
	<c:set var="tableHashId2">
		<%=request.getParameter(new org.displaytag.util.ParamEncoder("tab_searchResult").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE)) %>
	</c:set>	
	
<display:column format=""  titleKey="cUC73_ManagePlayers_LAST_NAME">
		<span id="dyn_lastname">
	<c:out value="${tab_searchResult.lastName}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="cUC73_ManagePlayers_FIRST_NAME">
		<span id="dyn_firstname">
	<c:out value="${tab_searchResult.firstName}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="cUC73_ManagePlayers_HEIGHT_CM">
		<span id="dyn_height">
	<c:out value="${tab_searchResult.height}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="cUC73_ManagePlayers_WEIGHT_KG">
		<span id="dyn_weight">
	<c:out value="${tab_searchResult.weight}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="cUC73_ManagePlayers_">
<a id="lnk_delete[${rownum}]"onclick="javascript:btn_click_cExecuteUC73_ManagePlayers(this.href);return false;"			href="<c:url value="/presentation/standardfeatures/exception/uc73_exception/UC73_ManagePlayers/lnk_delete.html?id="/><c:out value="${idRow}" />&tab=<c:out value="tab_searchResult" />&<c:out value="${tableHashId}"/>=<c:out value="${tableHashId2}"/>"
	 shape="rect"  >									 
<fmt:message key="cUC73_ManagePlayers_DELETE"/>
</a>
</display:column>
</display:table>
<c:if test="${uC73_ManagePlayersForm.selectedTeam.playerExiste=='true'}"> 
<h2 class="attention"  > 
<fmt:message key="cUC73_ManagePlayers_A_PLAYER_CANNOT_BE_DELETED_IF_"/>
</h2>
</c:if>
	</form:form>
	<script language="javascript">
		function btn_click_cExecuteUC73_ManagePlayers(url) {
			UC73_ManagePlayersvar = document.getElementById('uC73_ManagePlayers');
			UC73_ManagePlayersvar.setAttribute('action', url);
			UC73_ManagePlayersvar.submit();
		}
		function btn_click_cSetActionUC73_ManagePlayers(url) {
			UC73_ManagePlayersvar = document.getElementById('uC73_ManagePlayers');
			UC73_ManagePlayersvar.setAttribute('action', url);
		}
	</script>
</div>
<div class="east-center"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/exception/uc73_exception/UC73_ManagePlayerscmp_tabs" />
</div>
</div>
<div class="ui-layout-north"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/exception/uc73_exception/UC73_ManagePlayerscmp_header" />
</div>
<div class="ui-layout-south"  > 
<fmt:message key="cUC73_ManagePlayers___"/>
<span id="rd_jversion"  > 
<fmt:message key="cUC73_ManagePlayers_BLU_AGE_CORPORATION"/>
</span>
</div>
<div class="ui-layout-west"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/exception/uc73_exception/UC73_ManagePlayerscmp_treeview" />
</div>
</div>
</body>
</html>
