/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.entities.uc69_cachehibernate;


import java.io.Serializable;
import java.util.List;

import com.bluage.documentation.business.standardfeatures.entities.uc69_cachehibernate.bos.Team69BO;
import com.netfective.bluage.core.web.AbstractForm;
import com.netfective.bluage.core.web.IForm;

/**
* Class : UC69_HomePageForm
*/
public class UC69_HomePageForm extends AbstractForm implements Serializable, IForm {
	
	/** Constant : serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	//CONSTANT : allTeams
	private static final String ALL_TEAMS = "allTeams";
	//CONSTANT : selectedTeam
	private static final String SELECTED_TEAM = "selectedTeam";
	//CONSTANT : team
	private static final String TEAM = "team";
	/**
	 * 	Property: allTeams 
	 */
	private List<Team69BO> allTeams;
	/**
	 * 	Property: selectedTeam 
	 */
	private Team69BO selectedTeam;
	/**
	 * 	Property: team 
	 */
	private Team69BO team;
/**
	 * Default constructor : UC69_HomePageForm
	 */
	public UC69_HomePageForm() {
		super();
		// Initialize : allTeams
		this.allTeams = new java.util.ArrayList<com.bluage.documentation.business.standardfeatures.entities.uc69_cachehibernate.bos.Team69BO>();
		// Initialize : selectedTeam
		this.selectedTeam = null;
		// Initialize : team
		this.team = null;
					this.setAlwaysCallPreControllers(true);
	}
	
	/**
	 * 	Getter : allTeams 
	 * 	@return : Return the allTeams instance.
	 */
	public List<Team69BO> getAllTeams(){
		return allTeams; // For UC69_HomePageForm
	}
	
	/**
	 * 	Setter : allTeams 
	 *  @param allTeamsinstance : The instance to set.
	 */
	public void setAllTeams(final List<Team69BO> allTeamsinstance){
		this.allTeams = allTeamsinstance;// For UC69_HomePageForm
	}
	/**
	 * 	Getter : selectedTeam 
	 * 	@return : Return the selectedTeam instance.
	 */
	public Team69BO getSelectedTeam(){
		return selectedTeam; // For UC69_HomePageForm
	}
	
	/**
	 * 	Setter : selectedTeam 
	 *  @param selectedTeaminstance : The instance to set.
	 */
	public void setSelectedTeam(final Team69BO selectedTeaminstance){
		this.selectedTeam = selectedTeaminstance;// For UC69_HomePageForm
	}
	/**
	 * 	Getter : team 
	 * 	@return : Return the team instance.
	 */
	public Team69BO getTeam(){
		return team; // For UC69_HomePageForm
	}
	
	/**
	 * 	Setter : team 
	 *  @param teaminstance : The instance to set.
	 */
	public void setTeam(final Team69BO teaminstance){
		this.team = teaminstance;// For UC69_HomePageForm
	}
/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UC69_HomePageForm [ "+
ALL_TEAMS +" = " + allTeams +SELECTED_TEAM +" = " + selectedTeam +TEAM +" = " + team + "]";
	}
	
	/**
	 * Method setInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public void setInstanceByInstanceName(final String instanceName, final Object instance) {
		// Set the instance AllTeams.
		if(ALL_TEAMS.equals(instanceName)){// For UC69_HomePageForm
			this.setAllTeams((List<Team69BO>)instance); // For UC69_HomePageForm
		}
				// Set the instance SelectedTeam.
		if(SELECTED_TEAM.equals(instanceName)){// For UC69_HomePageForm
			this.setSelectedTeam((Team69BO)instance); // For UC69_HomePageForm
		}
				// Set the instance Team.
		if(TEAM.equals(instanceName)){// For UC69_HomePageForm
			this.setTeam((Team69BO)instance); // For UC69_HomePageForm
		}
			}
	
	/**
	 * Method getInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public Object getInstanceByInstanceName(final String instanceName){
		// Temporary object for tmpUC69_HomePageForm.
		Object tmpUC69_HomePageForm = null;
		
			
		// Get the instance AllTeams for UC69_HomePageForm.
		if(ALL_TEAMS.equals(instanceName)){ // For UC69_HomePageForm
			tmpUC69_HomePageForm = this.getAllTeams(); // For UC69_HomePageForm
		}
			
		// Get the instance SelectedTeam for UC69_HomePageForm.
		if(SELECTED_TEAM.equals(instanceName)){ // For UC69_HomePageForm
			tmpUC69_HomePageForm = this.getSelectedTeam(); // For UC69_HomePageForm
		}
			
		// Get the instance Team for UC69_HomePageForm.
		if(TEAM.equals(instanceName)){ // For UC69_HomePageForm
			tmpUC69_HomePageForm = this.getTeam(); // For UC69_HomePageForm
		}
		return tmpUC69_HomePageForm;// For UC69_HomePageForm
	}
	
			}
