/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.business.advancedfeatures.valueobject.uc206_valueobject.vo;

import com.bluage.documentation.business.AbstractValueObject;
/**
 * Position206VO
 *
 *
 *
 */
public class Position206VO
 extends AbstractValueObject {

	/**
	 * Default SUID.
	 */
	private static final long serialVersionUID = 8422155768722408555L;
	
	/**
	 * Used to transfer back and forth the Hibernate optimistic lock.
	 */
	private int version;
	
	/**
	 * Getter for version
	 * @return version
	 */
	public int getVersion() {
		return this.version;
	}
	
	/**
	 * Setter for version
	 * @param int version
	 */
	public void setVersion(int value) {
		this.version = value;
	}
						private String code
;
	
	/**
	 * Getter for code.
	 * 
	 *
	 * @return {@link String}
	 */
	public String getCode()
	{
		return this.code;
	}

	/**
	 * Setter for code.
	 * 
	 *
	 * @param value: {@link String}
	 */
	public void setCode(String value) 
	{
		this.code = value;
	}

	
						private String name
;
	
	/**
	 * Getter for name.
	 * 
	 *
	 * @return {@link String}
	 */
	public String getName()
	{
		return this.name;
	}

	/**
	 * Setter for name.
	 * 
	 *
	 * @param value: {@link String}
	 */
	public void setName(String value) 
	{
		this.name = value;
	}

	
/**
	 * Returns <code>true</code> if the argument is an Position206VO instance and all identifiers for this vo
     * equal the identifiers of the argument vo. Returns <code>false</code> otherwise.
	 * @param object  object to compare with current instance      
     * @return boolean true if equals false if not
     */
    public boolean equals( Object object )
    {
		if ( this == object )
        {
            return true;
        }
        if ( !( object instanceof Position206VO ) )
        {
            return false;
        }
        final Position206VO that = (Position206VO)object;
			if ( this.code == null || that.getCode() == null || !that.getCode().equals( this.code ) )
	        {
	            return false;
	        }
		return true;
	}
	/**
     * Returns a hash code based on this entity's identifiers.     * @return int
     */    
    public int hashCode()
    {
			int hashCode = 0;
			hashCode = H_FACTOR * hashCode + ( getCode() == null ? 0 : getCode().hashCode() );
			return hashCode;
	}
}
