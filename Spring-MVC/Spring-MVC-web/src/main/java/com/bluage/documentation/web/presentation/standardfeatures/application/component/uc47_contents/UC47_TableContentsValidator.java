/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.application.component.uc47_contents ;


import org.apache.log4j.Logger;
import org.springframework.validation.Errors;

import com.netfective.bluage.core.validator.AbstractValidator;
/**
* Class :UC47_TableContentsValidator
*/
public class UC47_TableContentsValidator extends AbstractValidator{
	
	// LOGGER for the class UC47_TableContentsValidator
	private static final Logger LOGGER = Logger.getLogger( UC47_TableContentsValidator.class);
	
	
	/**
	* Operation validate for UC47_TableContentsForm
	* @param obj : the current form (UC47_TableContentsForm)
	* @param errors : The spring errors to return for the form UC47_TableContentsForm
	*/
	public void validate(final Object obj,final Errors errors){
		LOGGER.info("Beginning method : validate"); // UC47_TableContentsValidator
		UC47_TableContentsForm cUC47_TableContentsForm = (UC47_TableContentsForm)obj; // UC47_TableContentsValidator
		LOGGER.info("Ending method : validate the form "+ cUC47_TableContentsForm.getClass().getName()); // UC47_TableContentsValidator
	}

	/**
	* Method to implements to use spring validators (UC47_TableContentsForm)
	* @param aClass : Class for the form UC47_TableContentsForm
	* @return boolean
	*/
	public boolean supports(final Class aClass){
		return (new UC47_TableContentsForm()).getClass().equals(aClass); // UC47_TableContentsValidator
	}
}
