/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.netfective.bluage.springmvc.ajax.tag;

import java.text.MessageFormat;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import com.netfective.bluage.springmvc.ajax.util.Constants;
/**
 * Class TagUtils
 * @interface Constants
 * @author NETFECTIVE TECHNOLOGY
 */
public final class TagUtils implements Constants {

    /**
     * LOGGER.
     */
    private static final  Logger LOGGER = Logger.getLogger(TagUtils.class);
    
    /**
    * Default builder
    */
    private TagUtils() {}

	/**
	 * Extract optional html attributes to name = value
	 * @param Map :
	 *			attributes
	 * @return String
	 */
	public static String buildAttributes(final Map attributes) {

		if (attributes == null || attributes.isEmpty()){
			return "";
		}

		final StringBuffer strbuff = new StringBuffer();
		final Set keys = attributes.keySet();
		for (final Iterator iter = keys.iterator(); iter.hasNext();) {
			final String key = (String) iter.next();
			final String value = (String) attributes.get(key);
			if (value != null && value.trim().length() > 0) {
				strbuff.append(key);
				strbuff.append("=\"");
				strbuff.append(value);
				strbuff.append("\"");
				strbuff.append(" ");
			}
		}

		return strbuff.toString();
	}
	
	/**
	* Give starting tag of a zone
	 * @param String :
	 *			zoneId
	 * @return String
	 */
	public static String getZoneStartDelimiter(final String zoneId) {
		final Object[] arguments = { TagUtils.getRealDivId(zoneId) };
		return MessageFormat.format(START_OF_ZONE, arguments);

	}

	
	/**
	* Give ending tag of a zone
	 * @param String :
	 *			zoneId
	 * @return String
	 */
	public static String getZoneEndDelimiter(final String zoneId) {
		final Object[] arguments = { TagUtils.getRealDivId(zoneId) };
		return MessageFormat.format(END_OF_ZONE, arguments);
	}

	/**
	 * Generate code for sortableScript
	 * @param String :
	 *			zone
	 * @return String
	 */
	public static String getSortableScript(final String zone) {
		final String script = "Sortable.create('" + getRealDivId(zone)
				+ "',{tag:'div',dropOnEmpty: true});";
		return buildScript(script);
	}

	/**
	 * Generate a script tag level
	 * @param String :
	 *			script
	 * @return String
	 */
	public static String buildScript(final String script) {
		return BEGIN_OF_SCRIPT_TAG + script + END_OF_SCRIPT_TAG;
	}

	/**
	 * Return HTML code for id parameter of a tag 
	 * @param String :
	 *			id
	 * @return String
	 */
	public static String getRealDivId(final String id) {
		return DIV_HTML_ID_PREFIX + id.replaceAll("\"", "&quot;");
	}

}

