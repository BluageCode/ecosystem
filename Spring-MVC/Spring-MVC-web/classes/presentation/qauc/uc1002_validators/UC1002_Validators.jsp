<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file="/include.jsp"  %>
<html >
<head >
<title  > 
<fmt:message key="cUC1002_Validators_BLU_AGE_DOCUMENTATION"/>
</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"  > 
</meta>
<link href="<c:url value= "/presentation/_style/_style.css"/>" rel="stylesheet" type="text/css"  /> 
<link href="<c:url value= "/presentation/_include/jquery/rd_lib.css"/>" rel="stylesheet" type="text/css"  /> 
<script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/jquery.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/rd_setup.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/rd_lib.js"/>" space="preserve"  ></script><link rel="shortcut icon" href="<c:url value= "/presentation/_style/favicon.ico"/>"  /> 

</head>

<body class=" " > 
<div id="rd_ghost"  > 
<small  > 
<fmt:message key="cUC1002_Validators___CSS_DEACTIVATED"/>
</small>
</div>
<div id="rd_jghost"  > 
<small  > 
<fmt:message key="cUC1002_Validators___JAVASCRIPT_DEACTIVATED"/>
</small>
</div>
<div id="shape"  > 
<div class="ui-layout-center"  > 
<div class="east-north"  > 
	<form:form id="uC1002_Validators" commandName="uC1002_ValidatorsForm"  enctype="application/x-www-form-urlencoded" method="post">
<h2 class="rd_first"  > 
<fmt:message key="cUC1002_Validators_QA_UC__UC1002__VALIDATORS"/>
</h2>
<table class="rd_tableform"  >
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC1002_Validators_FIRST_NAME_REQUIREDFIELDVALID"/>
</label>
<label style="background: none;color: red;"  > 
<fmt:message key="cUC1002_Validators_"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
<form:input id="txt_firstname" path="playerToCreate.firstName"  size="30" 
		/>
</td> 
<td rowspan="1" colspan="1"  >
<form:errors path="playerToCreate.firstName"/>
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC1002_Validators_LAST_NAME_REQUIREDFIELDVALIDA"/>
</label>
<label style="background: none;color: red;"  > 
<fmt:message key="cUC1002_Validators__77"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
<form:input id="txt_lastname" path="playerToCreate.lastName"  size="30" 
		/>
</td> 
<td rowspan="1" colspan="1"  >
<form:errors path="playerToCreate.lastName"/>
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC1002_Validators_DATE_OF_BIRTH_MMDDYYYY"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
<form:input id="txt_dateofbirth" path="playerToCreate.dateOfBirth"  size="12" 
		/>
</td> 
<td rowspan="1" colspan="1"  >
<form:errors path="playerToCreate.dateOfBirth"/>
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC1002_Validators_ESTIMATED_VALUE_CUSTOMVALIDAT"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
<form:input id="txt_estimatedvalue" path="playerToCreate.estimatedValue"  size="12" 
		/>
<fmt:message key="cUC1002_Validators_M"/>
</td> 
<td rowspan="1" colspan="1"  >
	<div id="estimatedvalue_format"  style="display:none;" >
			<c:out value="The Estimated Value should be a Number." />
	</div>

       <script type="text/javascript">
       function validateValue(playerValue) {
        reg = new RegExp("^[0-9]+[\.]?[0-9]*$","");
        return playerValue=="" || reg.test(playerValue);
       }       
       </script>
      
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC1002_Validators_SALARY_RANGEVALIDATOR_08000"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
<form:input id="txt_salary" path="playerToCreate.salary"  size="12" 
		/>
<fmt:message key="cUC1002_Validators_M_78"/>
</td> 
<td rowspan="1" colspan="1"  >
<form:errors path="playerToCreate.salary"/>
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC1002_Validators_WEIGHT_RANGEVALIDATOR_60140K"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
<form:input id="txt_weight" path="playerToCreate.weight"  size="12" 
		/>
<fmt:message key="cUC1002_Validators_KG"/>
</td> 
<td rowspan="1" colspan="1"  >
<form:errors path="playerToCreate.weight"/>
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC1002_Validators_HEIGHT_RANGEVALIDATOR_150240"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
<form:input id="txt_height" path="playerToCreate.height"  size="3" 
		/>
<fmt:message key="cUC1002_Validators_CM"/>
</td> 
<td rowspan="1" colspan="1"  >
<form:errors path="playerToCreate.height"/>
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<fmt:message key="cUC1002_Validators__79"/>
</td> 
<td rowspan="1" colspan="1"  >
<a id="lnk_create"onclick="javascript:btn_click_cExecuteUC1002_Validators(this.href);return false;"				href="<c:url value="/presentation/qauc/uc1002_validators/UC1002_Validators/lnk_create.html"/>"
							 shape="rect"  >									 
<fmt:message key="cUC1002_Validators_CREATE_PLAYER"/>
</a>
</td> 
<td rowspan="1" colspan="1"  >
<fmt:message key="cUC1002_Validators__80"/>
</td> 
</tr>
</table> 
	</form:form>
	<script language="javascript">
		function btn_click_cExecuteUC1002_Validators(url) {
			if(validateValue(document.getElementById("txt_estimatedvalue").value)==false) {
				document.getElementById("estimatedvalue_format").style.display="block";
				return false;
			}
			UC1002_Validatorsvar = document.getElementById('uC1002_Validators');
			UC1002_Validatorsvar.setAttribute('action', url);
			UC1002_Validatorsvar.submit();
		}
		function btn_click_cSetActionUC1002_Validators(url) {
			UC1002_Validatorsvar = document.getElementById('uC1002_Validators');
			UC1002_Validatorsvar.setAttribute('action', url);
		}
	</script>
<label style="background: none;color: red;"  > 
<fmt:message key="cUC1002_Validators__81"/>
</label>
<fmt:message key="cUC1002_Validators_REQUIRED_ENTRY_____"/>
</div>
<div class="east-center"  > 
<tiles:insertDefinition	name="presentation/qauc/uc1002_validators/UC1002_Validatorscmp_tabs" />
</div>
</div>
<div class="ui-layout-north"  > 
<tiles:insertDefinition	name="presentation/qauc/uc1002_validators/UC1002_Validatorscmp_header" />
</div>
<div class="ui-layout-south"  > 
<fmt:message key="cUC1002_Validators_______"/>
<span id="rd_jversion"  > 
<fmt:message key="cUC1002_Validators_BLU_AGE_DOCUMENTATION_82"/>
</span>
</div>
<div class="ui-layout-west"  > 
<tiles:insertDefinition	name="presentation/qauc/uc1002_validators/UC1002_Validatorscmp_treeview" />
</div>
</div>
</body>
</html>
