/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.application.datagrid.uc33_editabletable ;


import org.apache.log4j.Logger;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;

import com.netfective.bluage.core.validator.AbstractValidator;
/**
* Class :UC33_editableTableValidator
*/
public class UC33_editableTableValidator extends AbstractValidator{
	
	// LOGGER for the class UC33_editableTableValidator
	private static final Logger LOGGER = Logger.getLogger( UC33_editableTableValidator.class);
	
	
	/**
	* Operation validate for UC33_editableTableForm
	* @param obj : the current form (UC33_editableTableForm)
	* @param errors : The spring errors to return for the form UC33_editableTableForm
	*/
	public void validate(final Object obj,final Errors errors){
		LOGGER.info("Beginning method : validate"); // UC33_editableTableValidator
		UC33_editableTableForm cUC33_editableTableForm = (UC33_editableTableForm)obj; // UC33_editableTableValidator
							ValidationUtils.rejectIfEmpty(errors, "allTeams["+getRequestIndex()+"].name", "", "Team name is required.");

								ValidationUtils.rejectIfEmpty(errors, "allTeams["+getRequestIndex()+"].city", "", "City is required.");

			LOGGER.info("Ending method : validate the form "+ cUC33_editableTableForm.getClass().getName()); // UC33_editableTableValidator
	}

	/**
	* Method to implements to use spring validators (UC33_editableTableForm)
	* @param aClass : Class for the form UC33_editableTableForm
	* @return boolean
	*/
	public boolean supports(final Class aClass){
		return (new UC33_editableTableForm()).getClass().equals(aClass); // UC33_editableTableValidator
	}
}
