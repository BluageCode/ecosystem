/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.netfective.bluage.tag.format.validator;


import java.io.IOException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Currency;
import java.util.Locale;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
/**
 * 
 * Format the component passed as parameter with the corresponding pattern.
 *
 */
public class FormatCurrencyValidator extends TagSupport {

    /**
     * LOGGER.
     */
    private static final  Logger LOGGER = Logger.getLogger(FormatCurrencyValidator.class);
	
	/**
	 * serial UID
	 */
	private static final long serialVersionUID = -1250000261924529145L;
	
	
	private String pattern;

	private String value;
	

	@Override
	public int doEndTag() throws JspException {
		try {
			final JspWriter out = pageContext.getOut();
			out.print(formatCurrency());
		} catch (IOException ioe) {
			throw new JspException("IOException while writing data to page"
					+ ioe.getMessage(),ioe);
		}
		return SKIP_BODY;

	}
	
	/**
	 * Format Numeric
	 * @return
	 */
	private char[] formatCurrency() {
		final DecimalFormat dcForm = new DecimalFormat();
		// Set the local to US to make the pattern strict
		dcForm.setDecimalFormatSymbols(new DecimalFormatSymbols(Locale.US));
		dcForm.setCurrency(Currency.getInstance(Locale.US));
		// Apply the pattern
		dcForm.applyPattern(pattern);
		
		String  retFormated = StringUtils.EMPTY;
		// Return the value.
		try{
			retFormated = dcForm.format(Double.valueOf(value));
		}catch(IllegalArgumentException ex){
			if(StringUtils.isBlank(value)){
				LOGGER.warn("Can not parse the currency, the setted value is empty or null.");
			} else {
				LOGGER.warn("Can not parse the currency.");
			}
			return StringUtils.EMPTY.toCharArray();
		}
		return retFormated.toCharArray();

	}

	@Override
	public int doStartTag() throws JspException {
		return EVAL_PAGE;
	}

	public String getPattern() {
		return pattern;
	}

	public void setPattern(final String pattern) {
		this.pattern = pattern;
	}

	public String getValue() {
		return value;
	}

	public void setValue(final String value) {
		this.value = value;
	}
}
