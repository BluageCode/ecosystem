/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.service.standardfeatures.services.process.uc54_for;


import java.util.List;

import javax.inject.Named;

import com.bluage.documentation.business.standardfeatures.services.process.uc54_for.bos.Player54BO;
import com.bluage.documentation.service.AbstractService;
import com.netfective.bluage.core.exception.ApplicationException;

/**
 * Defines  application services for ServicePlayer54
  */
@Named("com.bluage.documentation.service.standardfeatures.services.process.uc54_for.ServicePlayer54")
public class ServicePlayer54  extends AbstractService{


				


	/**
	 *  Default constructor 
	 */
	public ServicePlayer54() {
		super(); // Call super for default constructor : ServicePlayer54
	}
	



			
	/**    
	 * Process operation.
	 * @param players {@link List}
		 * @return {@link List}
	 */
	public List buildTeamSelection(List players )
 throws ApplicationException {

				return privateBuildTeamSelection(players);
	}
	

	/**    
	 * Process operation.
	 * @param players {@link List}
		 * @return {@link List}
	 */
	private List privateBuildTeamSelection(List players )
 throws ApplicationException {
						// Return Type declaration.
				List teamPlayers = new java.util.ArrayList();
					// Central buffer node declaration for operation buildTeamSelection
			Player54BO	 currentPlayer =  new Player54BO();
					Boolean	 isInTeam =  Boolean.FALSE;
					for (java.util.Iterator iter0 = players.iterator(); iter0.hasNext();) {
currentPlayer = (com.bluage.documentation.business.standardfeatures.services.process.uc54_for.bos.Player54BO)iter0.next();
// Calling an operation : 
isInTeam = currentPlayer.getIsInTeam();
if (isInTeam.booleanValue()) {
/* Executing : Add the player in the team*/
teamPlayers.add(currentPlayer);
}
}

						return teamPlayers;
		}

		}
