/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.gettingstarted.uc03_formvalidation ;

// Import declaration.
// Java imports.
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.bluage.documentation.service.gettingstarted.uc03_formvalidation.ServicePlayer03;
import com.netfective.bluage.core.exception.ApplicationException;
import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;
import com.netfective.bluage.core.web.IForm;

/**
 * Class : UC03_FormValidationMessageController
 */
@Controller
@Scope("prototype")
@SessionAttributes("uC03_FormValidationMessageForm")
@RequestMapping(value= "/presentation/gettingstarted/uc03_formvalidation")
public class UC03_FormValidationMessageController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC03_FormValidationMessageController.class);
	
	//Current form name
	private static final String U_C03__FORM_VALIDATION_MESSAGE_FORM = "uC03_FormValidationMessageForm";

	
	/**
	 * Property:customDateEditorsUC03_FormValidationMessageController
	 */
	final private Map<String,CustomDateEditor> customDateEditorsUC03_FormValidationMessageController = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:uC03_FormValidationMessageValidator
	 */
	final private UC03_FormValidationMessageValidator uC03_FormValidationMessageValidator = new UC03_FormValidationMessageValidator();
	
	/**
	 * Service declaration : servicePlayer03.
	 */
	@Autowired
	private ServicePlayer03 servicePlayer03;
	
	
	// Initialise all the instances for UC03_FormValidationMessageController
					/**
	 * Operation : lnk_back
 	 * @param model : 
 	 * @param uC03_FormValidationMessage : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC03_FormValidationMessage/lnk_back.html",method = RequestMethod.GET)
	public String lnk_back(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC03_FormValidationMessageForm") UC03_FormValidationMessageForm  uC03_FormValidationMessageForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : lnk_back"); 
		infoLogger(uC03_FormValidationMessageForm); 
		uC03_FormValidationMessageForm.setAlwaysCallPreControllers(false); 
		// initialization for lnk_back
		init();
		
		String destinationPath = null; 
		

	 callGoBack(request,uC03_FormValidationMessageForm );
											// Init the destination form com.bluage.documentation.web.presentation.gettingstarted.uc03_formvalidation.UC03_FormValidationForm or not from lnk_back
			final UC03_FormValidationForm uC03_FormValidationForm =  new UC03_FormValidationForm();
			// Populate the destination form with all the instances returned from lnk_back.
			final IForm uC03_FormValidationForm2 = populateDestinationForm(request.getSession(), uC03_FormValidationForm); 
					
			// Add the form to the model.
			model.addAttribute("uC03_FormValidationForm", uC03_FormValidationForm2); 
			
			request.getSession().setAttribute("uC03_FormValidationForm", uC03_FormValidationForm2);
			
			// "OK" CASE => destination screen path from lnk_back
			LOGGER.info("Go to the screen 'UC03_FormValidation'.");
			// Redirect (PRG) from lnk_back
			destinationPath =  "redirect:/presentation/gettingstarted/uc03_formvalidation/UC03_FormValidation.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_back 
	}
	
	
	/**
	* This method initialise the form : UC03_FormValidationMessageForm 
	* @return UC03_FormValidationMessageForm
	*/
	@ModelAttribute("UC03_FormValidationMessageFormInit")
	public void initUC03_FormValidationMessageForm(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, U_C03__FORM_VALIDATION_MESSAGE_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method UC03_FormValidationMessageForm."); //for lnk_back 
		}
		UC03_FormValidationMessageForm uC03_FormValidationMessageForm;
	
		if(request.getSession().getAttribute(U_C03__FORM_VALIDATION_MESSAGE_FORM) != null){
			uC03_FormValidationMessageForm = (UC03_FormValidationMessageForm)request.getSession().getAttribute(U_C03__FORM_VALIDATION_MESSAGE_FORM);
		} else {
			uC03_FormValidationMessageForm = new UC03_FormValidationMessageForm();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form UC03_FormValidationMessageForm.");
		}
		uC03_FormValidationMessageForm = (UC03_FormValidationMessageForm)populateDestinationForm(request.getSession(), uC03_FormValidationMessageForm);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  UC03_FormValidationMessageForm.");
		}
		model.addAttribute(U_C03__FORM_VALIDATION_MESSAGE_FORM, uC03_FormValidationMessageForm);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param UC03_FormValidationMessageForm : The sceen form.
	 */
	@RequestMapping(value = "/UC03_FormValidationMessage.html" ,method = RequestMethod.GET)
	public void prepareUC03_FormValidationMessage(final HttpServletRequest request,final  Model model,final  @ModelAttribute("UC03_FormValidationMessageFormInit") UC03_FormValidationMessageForm uC03_FormValidationMessageForm){
		
		UC03_FormValidationMessageForm currentUC03_FormValidationMessageForm = uC03_FormValidationMessageForm;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(U_C03__FORM_VALIDATION_MESSAGE_FORM) == null){
			if(currentUC03_FormValidationMessageForm!=null){
				request.getSession().setAttribute(U_C03__FORM_VALIDATION_MESSAGE_FORM, currentUC03_FormValidationMessageForm);
			}else {
				currentUC03_FormValidationMessageForm = new UC03_FormValidationMessageForm();
				request.getSession().setAttribute(U_C03__FORM_VALIDATION_MESSAGE_FORM, currentUC03_FormValidationMessageForm);	
			}
		} else {
			currentUC03_FormValidationMessageForm = (UC03_FormValidationMessageForm) request.getSession().getAttribute(U_C03__FORM_VALIDATION_MESSAGE_FORM);
		}

		currentUC03_FormValidationMessageForm = (UC03_FormValidationMessageForm)populateDestinationForm(request.getSession(), currentUC03_FormValidationMessageForm);
		// Call all the Precontroller.
	currentUC03_FormValidationMessageForm.setAlwaysCallPreControllers(true);
		model.addAttribute(U_C03__FORM_VALIDATION_MESSAGE_FORM, currentUC03_FormValidationMessageForm);
		request.getSession().setAttribute(U_C03__FORM_VALIDATION_MESSAGE_FORM, currentUC03_FormValidationMessageForm);
		
	}
	
	/**
	 * method callGoBack
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callGoBack(HttpServletRequest request,IForm form) {
					try {
				// executing GoBack in lnk_back
	servicePlayer03.back(
	);  

								// processing variables GoBack in lnk_back

			} catch (ApplicationException e) { 
				// error handling for operation back called GoBack
				errorLogger("An error occured during the execution of the operation : back",e); 
		
			}
	}
		
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("UC03_FormValidationMessageController [ ");
		// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(uC03_FormValidationMessageValidator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsUC03_FormValidationMessageController!=null); 
		return strBToS.toString();
	}
}
