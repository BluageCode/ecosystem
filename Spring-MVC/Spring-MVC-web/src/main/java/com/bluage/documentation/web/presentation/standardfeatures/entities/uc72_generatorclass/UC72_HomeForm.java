/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.entities.uc72_generatorclass;


import java.io.Serializable;
import java.util.List;

import com.netfective.bluage.core.web.AbstractForm;
import com.netfective.bluage.core.web.IForm;

/**
* Class : UC72_HomeForm
*/
public class UC72_HomeForm extends AbstractForm implements Serializable, IForm {
	
	/** Constant : serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	//CONSTANT : listTeams
	private static final String LIST_TEAMS = "listTeams";
	//CONSTANT : listPlayers
	private static final String LIST_PLAYERS = "listPlayers";
	/**
	 * 	Property: listTeams 
	 */
	private List listTeams;
	/**
	 * 	Property: listPlayers 
	 */
	private List listPlayers;
/**
	 * Default constructor : UC72_HomeForm
	 */
	public UC72_HomeForm() {
		super();
		// Initialize : listTeams
		this.listTeams = null;
		// Initialize : listPlayers
		this.listPlayers = null;
			this.setAlwaysCallPreControllers(true);
	}
	
	/**
	 * 	Getter : listTeams 
	 * 	@return : Return the listTeams instance.
	 */
	public List getListTeams(){
		return listTeams; // For UC72_HomeForm
	}
	
	/**
	 * 	Setter : listTeams 
	 *  @param listTeamsinstance : The instance to set.
	 */
	public void setListTeams(final List listTeamsinstance){
		this.listTeams = listTeamsinstance;// For UC72_HomeForm
	}
	/**
	 * 	Getter : listPlayers 
	 * 	@return : Return the listPlayers instance.
	 */
	public List getListPlayers(){
		return listPlayers; // For UC72_HomeForm
	}
	
	/**
	 * 	Setter : listPlayers 
	 *  @param listPlayersinstance : The instance to set.
	 */
	public void setListPlayers(final List listPlayersinstance){
		this.listPlayers = listPlayersinstance;// For UC72_HomeForm
	}
/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UC72_HomeForm [ "+
LIST_TEAMS +" = " + listTeams +LIST_PLAYERS +" = " + listPlayers + "]";
	}
	
	/**
	 * Method setInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public void setInstanceByInstanceName(final String instanceName, final Object instance) {
		// Set the instance ListTeams.
		if(LIST_TEAMS.equals(instanceName)){// For UC72_HomeForm
			this.setListTeams((List)instance); // For UC72_HomeForm
		}
				// Set the instance ListPlayers.
		if(LIST_PLAYERS.equals(instanceName)){// For UC72_HomeForm
			this.setListPlayers((List)instance); // For UC72_HomeForm
		}
			}
	
	/**
	 * Method getInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public Object getInstanceByInstanceName(final String instanceName){
		// Temporary object for tmpUC72_HomeForm.
		Object tmpUC72_HomeForm = null;
		
			
		// Get the instance ListTeams for UC72_HomeForm.
		if(LIST_TEAMS.equals(instanceName)){ // For UC72_HomeForm
			tmpUC72_HomeForm = this.getListTeams(); // For UC72_HomeForm
		}
			
		// Get the instance ListPlayers for UC72_HomeForm.
		if(LIST_PLAYERS.equals(instanceName)){ // For UC72_HomeForm
			tmpUC72_HomeForm = this.getListPlayers(); // For UC72_HomeForm
		}
		return tmpUC72_HomeForm;// For UC72_HomeForm
	}
	
	}
