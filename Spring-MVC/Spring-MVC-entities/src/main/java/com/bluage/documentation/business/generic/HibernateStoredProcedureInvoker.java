/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506

package com.bluage.documentation.business.generic;

import java.util.Iterator;

import javax.inject.Named;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.bluage.documentation.business.IBusinessStringConstants;
import com.netfective.bluage.core.common.IStringConstants;
import com.netfective.bluage.core.exception.ApplicationException;

/**
 * This class represents the implementation of the generic search methods class.
 *
 * @date 
 * @see HibernateDaoSupport
 * @see GenericObjectFinder
 */
@Named ("com.bluage.documentation.business.generic.HibernateGenericObjectFinder") 
public class HibernateStoredProcedureInvoker 
		extends HibernateDaoSupport 
		implements IBusinessStringConstants, IStringConstants {

	private static final Logger LOGGER = Logger.getLogger(HibernateStoredProcedureInvoker.class);
	
/**
	 * This method returns the result of a stored procedure invokation
	 * 
	 * @param queryValue 			Query to execute
	 * @param params                List of parameters
	 * @return Object :	            Result of stored procedure invokation
	 */
	public Object invokeStoredProcedure(final String queryValue, final java.util.List<?> params) throws ApplicationException {
		StringBuilder debugInfo;
		Iterator iterator;
		if(LOGGER.isDebugEnabled(  ) ){
			debugInfo = new StringBuilder(L_BEG_METHOD);
			debugInfo.append(L_INVOKE_STORED_PROCEDURE).append(queryValue);
			if (params.size() > 0){
				debugInfo.append(L_BEG_INVOKE_STORED_PROCEDURE_PARAMS);
				for (iterator = params.iterator(); iterator.hasNext();){
					debugInfo.append(iterator.next().toString());
					if (iterator.hasNext()){
						debugInfo.append(L_INTER_INVOKE_STORED_PROCEDURE_PARAMS);
					}
				}
				debugInfo.append(L_END_INVOKE_STORED_PROCEDURE_PARAMS);
			}
			LOGGER.debug(debugInfo.toString());
		}	
		Query query = this.getHibernateTemplate().getSessionFactory()
				.getCurrentSession().createSQLQuery(queryValue);
		Object result = null;
	
		int position = 0;
		// setting parameters
		for (iterator = params.iterator(); iterator.hasNext();) {
			Object object = (Object) iterator.next();
			query.setParameter(position, object);
			position++;
		}
		result = query.uniqueResult();
		
		if(LOGGER.isDebugEnabled(  ) ){
			debugInfo = new StringBuilder(L_END_METHOD);
			debugInfo.append(L_RETURNS).append(result.toString());
			LOGGER.debug(debugInfo.toString());
		}	
		return result;
	}
}
