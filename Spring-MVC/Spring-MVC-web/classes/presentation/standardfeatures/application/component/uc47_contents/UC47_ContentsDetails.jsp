<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file="/include.jsp"  %>
<html >
<head >
<title  > 
<fmt:message key="cUC47_ContentsDetails_BLU_AGE_DOCUMENTATION"/>
</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"  > 
</meta>
<link href="<c:url value= "/presentation/_style/_style.css"/>" rel="stylesheet" type="text/css"  /> 
<link href="<c:url value= "/presentation/_include/jquery/rd_lib.css"/>" rel="stylesheet" type="text/css"  /> 
<script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/jquery.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/rd_setup.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/rd_lib.js"/>" space="preserve"  ></script><link rel="shortcut icon" href="<c:url value= "/presentation/_style/favicon.ico"/>"  /> 

</head>

<body class=" " > 
<div id="rd_ghost"  > 
<small  > 
<fmt:message key="cUC47_ContentsDetails___CSS_DEACTIVATED"/>
</small>
</div>
<div id="rd_jghost"  > 
<small  > 
<fmt:message key="cUC47_ContentsDetails___JAVASCRIPT_DEACTIVATED"/>
</small>
</div>
<div id="shape"  > 
<div class="ui-layout-center"  > 
<div class="east-north"  > 
	<form:form id="uC47_ContentsDetails" commandName="uC47_ContentsDetailsForm"  enctype="application/x-www-form-urlencoded" method="post">
<h2 class="rd_first"  > 
<fmt:message key="cUC47_ContentsDetails_STANDARD_FEATURES__APPLICATIO"/>
</h2>
<h4 class="rd_first"  > 
<fmt:message key="cUC47_ContentsDetails_DETAILS"/>
</h4>
<table class="rd_tableform"  >
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC47_ContentsDetails_FIRST_NAME_"/>
</label>
<label style="background: none;color: red;"  > 
<fmt:message key="cUC47_ContentsDetails_"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
<form:input id="txt_firstname" path="playerToUpdate.firstName"  size="30" 
		/>
</td> 
<td rowspan="1" colspan="1"  >
<form:errors path="playerToUpdate.firstName"/>
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC47_ContentsDetails_LAST_NAME_"/>
</label>
<label style="background: none;color: red;"  > 
<fmt:message key="cUC47_ContentsDetails__86"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
<form:input id="txt_lastname" path="playerToUpdate.lastName"  size="30" 
		/>
</td> 
<td rowspan="1" colspan="1"  >
<form:errors path="playerToUpdate.lastName"/>
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC47_ContentsDetails_DATE_OF_BIRTH_MMDDYYYY"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
<form:input id="txt_dateofbirth" path="playerToUpdate.dateOfBirth"  size="12" 
		/>
</td> 
<td rowspan="1" colspan="1"  >
<form:errors path="playerToUpdate.dateOfBirth"/>
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC47_ContentsDetails_ESTIMATED_VALUE"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
<form:input id="txt_estimatedvalue" path="playerToUpdate.estimatedValue"  size="12" 
		/>
<fmt:message key="cUC47_ContentsDetails_M"/>
</td> 
<td rowspan="1" colspan="1"  >
<fmt:message key="cUC47_ContentsDetails__87"/>
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC47_ContentsDetails_WEIGHT"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
<form:input id="txt_weight" path="playerToUpdate.weight"  size="12" 
		/>
<fmt:message key="cUC47_ContentsDetails_KG"/>
</td> 
<td rowspan="1" colspan="1"  >
<form:errors path="playerToUpdate.weight"/>
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC47_ContentsDetails_HEIGHT"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
<form:input id="txt_height" path="playerToUpdate.height"  size="3" 
		/>
<fmt:message key="cUC47_ContentsDetails_CM"/>
</td> 
<td rowspan="1" colspan="1"  >
<form:errors path="playerToUpdate.height"/>
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC47_ContentsDetails_POSITION"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
<form:select id="slt_position" path="playerToUpdate.position.code"   
		>
		<form:options items="${uC47_ContentsDetailsForm.allPositions}" itemValue="code" itemLabel="name" />
</form:select>
</td> 
<td rowspan="1" colspan="1"  >
<fmt:message key="cUC47_ContentsDetails__88"/>
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC47_ContentsDetails_ROOKIE"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
<form:checkbox id="chk_rookie" path="playerToUpdate.rookie"   
		/>
</td> 
<td rowspan="1" colspan="1"  >
<fmt:message key="cUC47_ContentsDetails__89"/>
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<fmt:message key="cUC47_ContentsDetails__90"/>
</td> 
<td rowspan="1" colspan="1"  >
<a id="lnk_update"onclick="javascript:btn_click_cExecuteUC47_ContentsDetails(this.href);return false;"				href="<c:url value="/presentation/standardfeatures/application/component/uc47_contents/UC47_ContentsDetails/lnk_update.html"/>"
							 shape="rect"  >									 
<fmt:message key="cUC47_ContentsDetails_UPDATE_PLAYER"/>
</a>
</td> 
<td rowspan="1" colspan="1"  >
<fmt:message key="cUC47_ContentsDetails__91"/>
</td> 
</tr>
</table> 
<label style="background: none;color: red;"  > 
<fmt:message key="cUC47_ContentsDetails__92"/>
</label>
<fmt:message key="cUC47_ContentsDetails_REQUIRED_ENTRY_"/>
<br clear="none"  /> 
<br clear="none"  /> 
<h4 class="rd_first"  > 
<fmt:message key="cUC47_ContentsDetails_ADD_A_TEAM"/>
</h4>
<table class="rd_tableform"  >
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC47_ContentsDetails_TEAM"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
<form:select id="slt_team" path="teamToAdd.id"   
		>
		<form:options items="${uC47_ContentsDetailsForm.listTeam}" itemValue="id" itemLabel="name" />
</form:select>
</td> 
<td rowspan="1" colspan="1"  >
<fmt:message key="cUC47_ContentsDetails__93"/>
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<fmt:message key="cUC47_ContentsDetails__94"/>
</td> 
<td rowspan="1" colspan="1"  >
<a id="lnk_add"onclick="javascript:btn_click_cExecuteUC47_ContentsDetails(this.href);return false;"				href="<c:url value="/presentation/standardfeatures/application/component/uc47_contents/UC47_ContentsDetails/lnk_add.html"/>"
							 shape="rect"  >									 
<fmt:message key="cUC47_ContentsDetails_ADD_TEAM"/>
</a>
</td> 
<td rowspan="1" colspan="1"  >
<fmt:message key="cUC47_ContentsDetails__95"/>
</td> 
</tr>
</table> 
<br clear="none"  /> 
<fmt:message key="cUC47_ContentsDetails__"/>
<a id="lnk_back"				href="<c:url value="/presentation/standardfeatures/application/component/uc47_contents/UC47_ContentsDetails/lnk_back.html"/>"
													 shape="rect"  >									 
<fmt:message key="cUC47_ContentsDetails_BACK"/>
</a>
	</form:form>
	<script language="javascript">
		function btn_click_cExecuteUC47_ContentsDetails(url) {
			UC47_ContentsDetailsvar = document.getElementById('uC47_ContentsDetails');
			UC47_ContentsDetailsvar.setAttribute('action', url);
			UC47_ContentsDetailsvar.submit();
		}
		function btn_click_cSetActionUC47_ContentsDetails(url) {
			UC47_ContentsDetailsvar = document.getElementById('uC47_ContentsDetails');
			UC47_ContentsDetailsvar.setAttribute('action', url);
		}
	</script>
<br clear="none"  /> 
<br clear="none"  /> 
</div>
<div class="east-center"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/application/component/uc47_contents/UC47_ContentsDetailscmp_tabs" />
</div>
</div>
<div class="ui-layout-north"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/application/component/uc47_contents/UC47_ContentsDetailscmp_header" />
</div>
<div class="ui-layout-south"  > 
<fmt:message key="cUC47_ContentsDetails___"/>
<span id="rd_jversion"  > 
<fmt:message key="cUC47_ContentsDetails_BLU_AGE_CORPORATION"/>
</span>
</div>
<div class="ui-layout-west"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/application/component/uc47_contents/UC47_ContentsDetailscmp_treeview" />
</div>
</div>
</body>
</html>
