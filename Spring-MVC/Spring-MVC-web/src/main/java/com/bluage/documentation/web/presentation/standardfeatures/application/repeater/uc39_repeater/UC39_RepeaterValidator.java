/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.application.repeater.uc39_repeater ;


import org.apache.log4j.Logger;
import org.springframework.validation.Errors;

import com.netfective.bluage.core.validator.AbstractValidator;
/**
* Class :UC39_RepeaterValidator
*/
public class UC39_RepeaterValidator extends AbstractValidator{
	
	// LOGGER for the class UC39_RepeaterValidator
	private static final Logger LOGGER = Logger.getLogger( UC39_RepeaterValidator.class);
	
	
	/**
	* Operation validate for UC39_RepeaterForm
	* @param obj : the current form (UC39_RepeaterForm)
	* @param errors : The spring errors to return for the form UC39_RepeaterForm
	*/
	public void validate(final Object obj,final Errors errors){
		LOGGER.info("Beginning method : validate"); // UC39_RepeaterValidator
		UC39_RepeaterForm cUC39_RepeaterForm = (UC39_RepeaterForm)obj; // UC39_RepeaterValidator
		LOGGER.info("Ending method : validate the form "+ cUC39_RepeaterForm.getClass().getName()); // UC39_RepeaterValidator
	}

	/**
	* Method to implements to use spring validators (UC39_RepeaterForm)
	* @param aClass : Class for the form UC39_RepeaterForm
	* @return boolean
	*/
	public boolean supports(final Class aClass){
		return (new UC39_RepeaterForm()).getClass().equals(aClass); // UC39_RepeaterValidator
	}
}
