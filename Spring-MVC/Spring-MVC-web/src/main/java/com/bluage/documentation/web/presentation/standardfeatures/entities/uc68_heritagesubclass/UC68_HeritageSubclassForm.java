/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.entities.uc68_heritagesubclass;


import java.io.Serializable;
import java.util.List;

import com.bluage.documentation.business.standardfeatures.entities.uc68_heritagesubclass.bos.Player68BO;
import com.bluage.documentation.business.standardfeatures.entities.uc68_heritagesubclass.bos.Professional68BO;
import com.bluage.documentation.business.standardfeatures.entities.uc68_heritagesubclass.bos.Rookie68BO;
import com.netfective.bluage.core.web.AbstractForm;
import com.netfective.bluage.core.web.IForm;

/**
* Class : UC68_HeritageSubclassForm
*/
public class UC68_HeritageSubclassForm extends AbstractForm implements Serializable, IForm {
	
	/** Constant : serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	//CONSTANT : allProfessionals
	private static final String ALL_PROFESSIONALS = "allProfessionals";
	//CONSTANT : allPlayers
	private static final String ALL_PLAYERS = "allPlayers";
	//CONSTANT : allRookies
	private static final String ALL_ROOKIES = "allRookies";
	/**
	 * 	Property: allProfessionals 
	 */
	private List<Professional68BO> allProfessionals;
	/**
	 * 	Property: allPlayers 
	 */
	private List<Player68BO> allPlayers;
	/**
	 * 	Property: allRookies 
	 */
	private List<Rookie68BO> allRookies;
/**
	 * Default constructor : UC68_HeritageSubclassForm
	 */
	public UC68_HeritageSubclassForm() {
		super();
		// Initialize : allProfessionals
		this.allProfessionals = new java.util.ArrayList<com.bluage.documentation.business.standardfeatures.entities.uc68_heritagesubclass.bos.Professional68BO>();
		// Initialize : allPlayers
		this.allPlayers = new java.util.ArrayList<com.bluage.documentation.business.standardfeatures.entities.uc68_heritagesubclass.bos.Player68BO>();
		// Initialize : allRookies
		this.allRookies = new java.util.ArrayList<com.bluage.documentation.business.standardfeatures.entities.uc68_heritagesubclass.bos.Rookie68BO>();
									this.setAlwaysCallPreControllers(true);
	}
	
	/**
	 * 	Getter : allProfessionals 
	 * 	@return : Return the allProfessionals instance.
	 */
	public List<Professional68BO> getAllProfessionals(){
		return allProfessionals; // For UC68_HeritageSubclassForm
	}
	
	/**
	 * 	Setter : allProfessionals 
	 *  @param allProfessionalsinstance : The instance to set.
	 */
	public void setAllProfessionals(final List<Professional68BO> allProfessionalsinstance){
		this.allProfessionals = allProfessionalsinstance;// For UC68_HeritageSubclassForm
	}
	/**
	 * 	Getter : allPlayers 
	 * 	@return : Return the allPlayers instance.
	 */
	public List<Player68BO> getAllPlayers(){
		return allPlayers; // For UC68_HeritageSubclassForm
	}
	
	/**
	 * 	Setter : allPlayers 
	 *  @param allPlayersinstance : The instance to set.
	 */
	public void setAllPlayers(final List<Player68BO> allPlayersinstance){
		this.allPlayers = allPlayersinstance;// For UC68_HeritageSubclassForm
	}
	/**
	 * 	Getter : allRookies 
	 * 	@return : Return the allRookies instance.
	 */
	public List<Rookie68BO> getAllRookies(){
		return allRookies; // For UC68_HeritageSubclassForm
	}
	
	/**
	 * 	Setter : allRookies 
	 *  @param allRookiesinstance : The instance to set.
	 */
	public void setAllRookies(final List<Rookie68BO> allRookiesinstance){
		this.allRookies = allRookiesinstance;// For UC68_HeritageSubclassForm
	}
/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UC68_HeritageSubclassForm [ "+
ALL_PROFESSIONALS +" = " + allProfessionals +ALL_PLAYERS +" = " + allPlayers +ALL_ROOKIES +" = " + allRookies + "]";
	}
	
	/**
	 * Method setInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public void setInstanceByInstanceName(final String instanceName, final Object instance) {
		// Set the instance AllProfessionals.
		if(ALL_PROFESSIONALS.equals(instanceName)){// For UC68_HeritageSubclassForm
			this.setAllProfessionals((List<Professional68BO>)instance); // For UC68_HeritageSubclassForm
		}
				// Set the instance AllPlayers.
		if(ALL_PLAYERS.equals(instanceName)){// For UC68_HeritageSubclassForm
			this.setAllPlayers((List<Player68BO>)instance); // For UC68_HeritageSubclassForm
		}
				// Set the instance AllRookies.
		if(ALL_ROOKIES.equals(instanceName)){// For UC68_HeritageSubclassForm
			this.setAllRookies((List<Rookie68BO>)instance); // For UC68_HeritageSubclassForm
		}
			}
	
	/**
	 * Method getInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public Object getInstanceByInstanceName(final String instanceName){
		// Temporary object for tmpUC68_HeritageSubclassForm.
		Object tmpUC68_HeritageSubclassForm = null;
		
			
		// Get the instance AllProfessionals for UC68_HeritageSubclassForm.
		if(ALL_PROFESSIONALS.equals(instanceName)){ // For UC68_HeritageSubclassForm
			tmpUC68_HeritageSubclassForm = this.getAllProfessionals(); // For UC68_HeritageSubclassForm
		}
			
		// Get the instance AllPlayers for UC68_HeritageSubclassForm.
		if(ALL_PLAYERS.equals(instanceName)){ // For UC68_HeritageSubclassForm
			tmpUC68_HeritageSubclassForm = this.getAllPlayers(); // For UC68_HeritageSubclassForm
		}
			
		// Get the instance AllRookies for UC68_HeritageSubclassForm.
		if(ALL_ROOKIES.equals(instanceName)){ // For UC68_HeritageSubclassForm
			tmpUC68_HeritageSubclassForm = this.getAllRookies(); // For UC68_HeritageSubclassForm
		}
		return tmpUC68_HeritageSubclassForm;// For UC68_HeritageSubclassForm
	}
	
							}
