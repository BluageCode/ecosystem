/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.advancedfeatures.performance.uc200_datagridloading ;


import org.apache.log4j.Logger;
import org.springframework.validation.Errors;

import com.netfective.bluage.core.validator.AbstractValidator;
/**
* Class :UC200_DatagridLoadingValidator
*/
public class UC200_DatagridLoadingValidator extends AbstractValidator{
	
	// LOGGER for the class UC200_DatagridLoadingValidator
	private static final Logger LOGGER = Logger.getLogger( UC200_DatagridLoadingValidator.class);
	
	
	/**
	* Operation validate for UC200_DatagridLoadingForm
	* @param obj : the current form (UC200_DatagridLoadingForm)
	* @param errors : The spring errors to return for the form UC200_DatagridLoadingForm
	*/
	public void validate(final Object obj,final Errors errors){
		LOGGER.info("Beginning method : validate"); // UC200_DatagridLoadingValidator
		UC200_DatagridLoadingForm cUC200_DatagridLoadingForm = (UC200_DatagridLoadingForm)obj; // UC200_DatagridLoadingValidator
		LOGGER.info("Ending method : validate the form "+ cUC200_DatagridLoadingForm.getClass().getName()); // UC200_DatagridLoadingValidator
	}

	/**
	* Method to implements to use spring validators (UC200_DatagridLoadingForm)
	* @param aClass : Class for the form UC200_DatagridLoadingForm
	* @return boolean
	*/
	public boolean supports(final Class aClass){
		return (new UC200_DatagridLoadingForm()).getClass().equals(aClass); // UC200_DatagridLoadingValidator
	}
}
