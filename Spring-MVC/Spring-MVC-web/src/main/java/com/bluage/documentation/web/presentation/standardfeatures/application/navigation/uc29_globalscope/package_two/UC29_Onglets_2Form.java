/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.application.navigation.uc29_globalscope.package_two;


import java.io.Serializable;
import java.util.List;

import com.netfective.bluage.core.web.AbstractForm;
import com.netfective.bluage.core.web.IForm;

/**
* Class : UC29_Onglets_2Form
*/
public class UC29_Onglets_2Form extends AbstractForm implements Serializable, IForm {
	
	/** Constant : serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	//CONSTANT : allStates
	private static final String ALL_STATES = "allStates";
	//CONSTANT : allStadiums
	private static final String ALL_STADIUMS = "allStadiums";
	/**
	 * 	Property: allStates 
	 */
	private List allStates;
	/**
	 * 	Property: allStadiums 
	 */
	private List allStadiums;
/**
	 * Default constructor : UC29_Onglets_2Form
	 */
	public UC29_Onglets_2Form() {
		super();
		// Initialize : allStates
		this.allStates = null;
		// Initialize : allStadiums
		this.allStadiums = null;
		this.setAlwaysCallPreControllers(true);
	}
	
	/**
	 * 	Getter : allStates 
	 * 	@return : Return the allStates instance.
	 */
	public List getAllStates(){
		return allStates; // For UC29_Onglets_2Form
	}
	
	/**
	 * 	Setter : allStates 
	 *  @param allStatesinstance : The instance to set.
	 */
	public void setAllStates(final List allStatesinstance){
		this.allStates = allStatesinstance;// For UC29_Onglets_2Form
	}
	/**
	 * 	Getter : allStadiums 
	 * 	@return : Return the allStadiums instance.
	 */
	public List getAllStadiums(){
		return allStadiums; // For UC29_Onglets_2Form
	}
	
	/**
	 * 	Setter : allStadiums 
	 *  @param allStadiumsinstance : The instance to set.
	 */
	public void setAllStadiums(final List allStadiumsinstance){
		this.allStadiums = allStadiumsinstance;// For UC29_Onglets_2Form
	}
/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UC29_Onglets_2Form [ "+
ALL_STATES +" = " + allStates +ALL_STADIUMS +" = " + allStadiums + "]";
	}
	
	/**
	 * Method setInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public void setInstanceByInstanceName(final String instanceName, final Object instance) {
		// Set the instance AllStates.
		if(ALL_STATES.equals(instanceName)){// For UC29_Onglets_2Form
			this.setAllStates((List)instance); // For UC29_Onglets_2Form
		}
				// Set the instance AllStadiums.
		if(ALL_STADIUMS.equals(instanceName)){// For UC29_Onglets_2Form
			this.setAllStadiums((List)instance); // For UC29_Onglets_2Form
		}
			}
	
	/**
	 * Method getInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public Object getInstanceByInstanceName(final String instanceName){
		// Temporary object for tmpUC29_Onglets_2Form.
		Object tmpUC29_Onglets_2Form = null;
		
			
		// Get the instance AllStates for UC29_Onglets_2Form.
		if(ALL_STATES.equals(instanceName)){ // For UC29_Onglets_2Form
			tmpUC29_Onglets_2Form = this.getAllStates(); // For UC29_Onglets_2Form
		}
			
		// Get the instance AllStadiums for UC29_Onglets_2Form.
		if(ALL_STADIUMS.equals(instanceName)){ // For UC29_Onglets_2Form
			tmpUC29_Onglets_2Form = this.getAllStadiums(); // For UC29_Onglets_2Form
		}
		return tmpUC29_Onglets_2Form;// For UC29_Onglets_2Form
	}
	
}
