package uc72;


import org.junit.Test;
import test.common.AbstractBADocTestCase;

public class IT02uc72ComportementPlayers extends AbstractBADocTestCase {
	
	@Override
	public  String getName(){
		return "UC-72";
	}


	@Test
	public void testIT02uc72ComportementPlayers() throws Exception {
		// On teste l'UC72 - Standard Features/Entities - Generator Class
		selenium.open("");	
		selenium.click("link=Expand All");
		selenium.waitForPageToLoad("30000");
		selenium.click("link=UC72 - Generator Class");
		// On teste l'affichage de la page de garde :
		assertTrue(selenium.isElementPresent("//h2[contains(text(),'Standard Features > Entities > UC72 > Generator Class')]"));
		assertTrue(selenium.isElementPresent("//h4[contains(text(),'Menu')]"));
		// On teste le comportement players :
		selenium.click("//a[contains(text(),'[Add a player]')]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//h4[contains(text(),'Add Player')]"));
		// On teste les messages d'erreur : 
		// On entre une date de naissance au format erroné : 
		selenium.type("//input[contains(@id,'txt_dateofbirth')]", "19/12/1987");
		selenium.click("//a[contains(text(),'[Create player]')]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//table/tbody/tr[1]/td/span[contains(text(),'Id is required.')]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[2]/td/span[contains(text(),'First name is required.')]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[3]/td/span[contains(text(),'Last name is required.')]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[4]/td/span[contains(text(),'Invalid format.')]"));
		// On teste les messages d'erreur de longueur : 
		selenium.type("//input[contains(@id,'txt_firstname')]", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
		selenium.type("//input[contains(@id,'txt_lastname')]", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
		selenium.click("//a[contains(text(),'[Create player]')]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//table/tbody/tr[2]/td/span[contains(text(),'First name should be less than 30 characters long.')]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[3]/td/span[contains(text(),'Last name should be less than 30 characters long.')]"));
		// On teste la page d'erreur ID : 
		selenium.type("//input[contains(@id,'txt_id')]", "1");
		selenium.type("//input[contains(@id,'txt_firstname')]", "Jean");
		selenium.type("//input[contains(@id,'txt_lastname')]", "Pierre");
		selenium.type("//input[contains(@id,'txt_dateofbirth')]", "12/19/1987");
		selenium.click("//a[contains(text(),'[Create player]')]");
		selenium.waitForPageToLoad("30000");
		// On vérifie l'affichage : 
		assertTrue(selenium.isElementPresent("//h2[contains(text(),'Generator Class Exception')]"));
		assertTrue(selenium.isElementPresent("//h4[contains(text(),'An exception has occurred')]"));
		assertTrue(selenium.isElementPresent("//*[contains(text(),'Error message: entry already exists in database')]"));
		selenium.click("//a[contains(text(),'[back]')]");
		selenium.waitForPageToLoad("30000");
		// On vérifie qu'on est sur la page 'add player' :
		assertTrue(selenium.isElementPresent("//h4[contains(text(),'Add Player')]"));
		// On creer deux joueurs :
		selenium.type("//input[contains(@id,'txt_id')]", "6");
		selenium.type("//input[contains(@id,'txt_firstname')]", "Jean");
		selenium.type("//input[contains(@id,'txt_lastname')]", "Pierre");
		selenium.type("//input[contains(@id,'txt_dateofbirth')]", "12/19/1987");
		selenium.select("//*[contains(@id,'slt_position')]", "label=Center");
		selenium.click("//input[contains(@id,'chk_rookie')]");
		selenium.click("//a[contains(text(),'[Create player]')]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_players')]/tbody/tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),'6')] and td/span[contains(text(),'Jean')] and td/span[contains(text(),'Pierre')] and td/span[contains(text(),'12/19/1987')] and td/span[contains(text(),'C')] and td/input[@checked] and td/a[contains(text(),'[delete]')]]"));
		selenium.type("//input[contains(@id,'txt_id')]", "8");
		selenium.type("//input[contains(@id,'txt_firstname')]", "Michael");
		selenium.type("//input[contains(@id,'txt_lastname')]", "Jordan");
		selenium.type("//input[contains(@id,'txt_dateofbirth')]", "11/24/1978");
		selenium.select("//*[contains(@id,'slt_position')]", "label=Power Forward");
		selenium.click("//input[contains(@id,'chk_rookie')]");
		selenium.click("//a[contains(text(),'[Create player]')]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("5", selenium.getXpathCount("//table[contains(@id,'tab_players')]/tbody/tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),'8')] and td/span[contains(text(),'Michael')] and td/span[contains(text(),'Jordan')] and td/span[contains(text(),'11/24/1978')] and td/span[contains(text(),'PF')] and td/input[not(@checked)] and td/a[contains(text(),'[delete]')]]"));
		// On supprime tous les players : 
		Number indexFO = selenium.getElementIndex("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Frank\")] and td/span[contains(text(),\"Oliver\")]]");
		selenium.click("//table[contains(@id,'tab_players')]/tbody/tr[" + indexFO + " + 1]//td//a[contains(text(),\"[delete]\")]");
		selenium.waitForPageToLoad("30000");
		Number indexCE = selenium.getElementIndex("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Cole\")] and td/span[contains(text(),\"Elbert\")]]");
		selenium.click("//table[contains(@id,'tab_players')]/tbody/tr[" + indexCE + " + 1]//td//a[contains(text(),\"[delete]\")]");
		selenium.waitForPageToLoad("30000");
		Number indexDR = selenium.getElementIndex("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Dawson\")] and td/span[contains(text(),\"Ricky\")]]");
		selenium.click("//table[contains(@id,'tab_players')]/tbody/tr[" + indexDR + " + 1]//td//a[contains(text(),\"[delete]\")]");
		selenium.waitForPageToLoad("30000");
		Number indexPJ = selenium.getElementIndex("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Pierre\")] and td/span[contains(text(),\"Jean\")]]");
		selenium.click("//table[contains(@id,'tab_players')]/tbody/tr[" + indexPJ + " + 1]//td//a[contains(text(),\"[delete]\")]");
		selenium.waitForPageToLoad("30000");
		Number indexJM = selenium.getElementIndex("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Jordan\")] and td/span[contains(text(),\"Michael\")]]");
		selenium.click("//table[contains(@id,'tab_players')]/tbody/tr[" + indexJM + " + 1]//td//a[contains(text(),\"[delete]\")]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("0", selenium.getXpathCount("//table[contains(@id,'tab_players')]/tbody/tr"));
		// On crée un player vide : 
		selenium.type("//input[contains(@id,'txt_id')]", "");
		selenium.type("//input[contains(@id,'txt_firstname')]", "");
		selenium.type("//input[contains(@id,'txt_lastname')]", "");
		selenium.type("//input[contains(@id,'txt_dateofbirth')]", "");
		selenium.click("//a[contains(text(),'[Create player]')]");
		selenium.waitForPageToLoad("30000");
		// On vérifie la présence des messages d'erreur :
		assertTrue(selenium.isElementPresent("//table/tbody/tr[1]/td/span[contains(text(),'Id is required.')]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[2]/td/span[contains(text(),'First name is required.')]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[3]/td/span[contains(text(),'Last name is required.')]"));
		// On crée un player sans date de naissance : 
		selenium.type("//input[contains(@id,'txt_id')]", "1");
		selenium.type("//input[contains(@id,'txt_firstname')]", "Jean");
		selenium.type("//input[contains(@id,'txt_lastname')]", "Pierre");
		selenium.type("//input[contains(@id,'txt_dateofbirth')]", "");
		selenium.select("//*[contains(@id,'slt_position')]", "label=Power Forward");
		selenium.click("//a[contains(text(),'[Create player]')]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("1", selenium.getXpathCount("//table[contains(@id,'tab_players')]/tbody/tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),'1')] and td/span[contains(text(),'Pierre')] and td/span[contains(text(),'Jean')] and td/span[contains(text(),'PF')] and td/input[not(@checked)] and td/a[contains(text(),'[delete]')]]"));
		indexPJ = selenium.getElementIndex("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Pierre\")] and td/span[contains(text(),\"Jean\")]]");
		selenium.click("//table[contains(@id,'tab_players')]/tbody/tr[" + indexPJ + " + 1]//td//a[contains(text(),\"[delete]\")]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("0", selenium.getXpathCount("//table[contains(@id,'tab_players')]/tbody/tr"));
		selenium.click("//a[contains(text(),'[back]')]");
		selenium.waitForPageToLoad("30000");
	}

	
}
