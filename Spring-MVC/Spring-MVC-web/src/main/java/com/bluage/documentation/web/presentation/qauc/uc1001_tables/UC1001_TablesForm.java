/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.qauc.uc1001_tables;


import java.io.Serializable;
import java.util.List;

import com.bluage.documentation.business.qauc.uc1001_tables.bos.Team1001BO;
import com.netfective.bluage.core.web.AbstractForm;
import com.netfective.bluage.core.web.IForm;

/**
* Class : UC1001_TablesForm
*/
public class UC1001_TablesForm extends AbstractForm implements Serializable, IForm {
	
	/** Constant : serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	//CONSTANT : allTeams
	private static final String ALL_TEAMS = "allTeams";
	//CONSTANT : selectedTeam
	private static final String SELECTED_TEAM = "selectedTeam";
	//CONSTANT : teamDetails
	private static final String TEAM_DETAILS = "teamDetails";
	/**
	 * 	Property: allTeams 
	 */
	private List<Team1001BO> allTeams;
	/**
	 * 	Property: selectedTeam 
	 */
	private Team1001BO selectedTeam;
	/**
	 * 	Property: teamDetails 
	 */
	private Team1001BO teamDetails;
/**
	 * Default constructor : UC1001_TablesForm
	 */
	public UC1001_TablesForm() {
		super();
		// Initialize : allTeams
		this.allTeams = new java.util.ArrayList<com.bluage.documentation.business.qauc.uc1001_tables.bos.Team1001BO>();
		// Initialize : selectedTeam
		this.selectedTeam = null;
		// Initialize : teamDetails
		this.teamDetails = null;
					this.setAlwaysCallPreControllers(true);
	}
	
	/**
	 * 	Getter : allTeams 
	 * 	@return : Return the allTeams instance.
	 */
	public List<Team1001BO> getAllTeams(){
		return allTeams; // For UC1001_TablesForm
	}
	
	/**
	 * 	Setter : allTeams 
	 *  @param allTeamsinstance : The instance to set.
	 */
	public void setAllTeams(final List<Team1001BO> allTeamsinstance){
		this.allTeams = allTeamsinstance;// For UC1001_TablesForm
	}
	/**
	 * 	Getter : selectedTeam 
	 * 	@return : Return the selectedTeam instance.
	 */
	public Team1001BO getSelectedTeam(){
		return selectedTeam; // For UC1001_TablesForm
	}
	
	/**
	 * 	Setter : selectedTeam 
	 *  @param selectedTeaminstance : The instance to set.
	 */
	public void setSelectedTeam(final Team1001BO selectedTeaminstance){
		this.selectedTeam = selectedTeaminstance;// For UC1001_TablesForm
	}
	/**
	 * 	Getter : teamDetails 
	 * 	@return : Return the teamDetails instance.
	 */
	public Team1001BO getTeamDetails(){
		return teamDetails; // For UC1001_TablesForm
	}
	
	/**
	 * 	Setter : teamDetails 
	 *  @param teamDetailsinstance : The instance to set.
	 */
	public void setTeamDetails(final Team1001BO teamDetailsinstance){
		this.teamDetails = teamDetailsinstance;// For UC1001_TablesForm
	}
/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UC1001_TablesForm [ "+
ALL_TEAMS +" = " + allTeams +SELECTED_TEAM +" = " + selectedTeam +TEAM_DETAILS +" = " + teamDetails + "]";
	}
	
	/**
	 * Method setInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public void setInstanceByInstanceName(final String instanceName, final Object instance) {
		// Set the instance AllTeams.
		if(ALL_TEAMS.equals(instanceName)){// For UC1001_TablesForm
			this.setAllTeams((List<Team1001BO>)instance); // For UC1001_TablesForm
		}
				// Set the instance SelectedTeam.
		if(SELECTED_TEAM.equals(instanceName)){// For UC1001_TablesForm
			this.setSelectedTeam((Team1001BO)instance); // For UC1001_TablesForm
		}
				// Set the instance TeamDetails.
		if(TEAM_DETAILS.equals(instanceName)){// For UC1001_TablesForm
			this.setTeamDetails((Team1001BO)instance); // For UC1001_TablesForm
		}
			}
	
	/**
	 * Method getInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public Object getInstanceByInstanceName(final String instanceName){
		// Temporary object for tmpUC1001_TablesForm.
		Object tmpUC1001_TablesForm = null;
		
			
		// Get the instance AllTeams for UC1001_TablesForm.
		if(ALL_TEAMS.equals(instanceName)){ // For UC1001_TablesForm
			tmpUC1001_TablesForm = this.getAllTeams(); // For UC1001_TablesForm
		}
			
		// Get the instance SelectedTeam for UC1001_TablesForm.
		if(SELECTED_TEAM.equals(instanceName)){ // For UC1001_TablesForm
			tmpUC1001_TablesForm = this.getSelectedTeam(); // For UC1001_TablesForm
		}
			
		// Get the instance TeamDetails for UC1001_TablesForm.
		if(TEAM_DETAILS.equals(instanceName)){ // For UC1001_TablesForm
			tmpUC1001_TablesForm = this.getTeamDetails(); // For UC1001_TablesForm
		}
		return tmpUC1001_TablesForm;// For UC1001_TablesForm
	}
	
			}
