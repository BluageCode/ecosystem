/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.service.gettingstarted.uc02_entitiescreation;


import javax.inject.Inject;
import javax.inject.Named;

import com.bluage.documentation.business.gettingstarted.uc02_entitiescreation.bos.State02BO;
import com.bluage.documentation.business.gettingstarted.uc02_entitiescreation.entities.daofinder.State02DAOFinderImpl;
import com.bluage.documentation.service.AbstractService;
import com.netfective.bluage.core.exception.ApplicationException;

/**
 * Defines  application services for ServiceState02
  */
@Named("com.bluage.documentation.service.gettingstarted.uc02_entitiescreation.ServiceState02")
public class ServiceState02  extends AbstractService{


			@Inject
		@Named ("com.bluage.documentation.business.gettingstarted.uc02_entitiescreation.entities.daofinder.State02DAOFinderImpl")
		private State02DAOFinderImpl state02BOFinder;
	


	/**
	 *  Default constructor 
	 */
	public ServiceState02() {
		super(); // Call super for default constructor : ServiceState02
	}
	


			
				
	/**    
	 * Operation Crud
	 * @param bo {@link State02BO}
		 * @return {@link State02BO}
	 */
	public State02BO state02FindByID(State02BO bo )
 throws ApplicationException {

					return (State02BO)state02BOFinder.findByPrimaryKey(getBoId(bo));
	}
	/**
	 *  Return the primary key from business object.
	 */
	private Object getBoId(State02BO bo) {
				return bo.getCode();
		}

		}
