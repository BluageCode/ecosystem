/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.application.datagrid.uc37_collectionorder ;


import org.apache.log4j.Logger;
import org.springframework.validation.Errors;

import com.netfective.bluage.core.validator.AbstractValidator;
/**
* Class :UC37_TeamsAndPlayersValidator
*/
public class UC37_TeamsAndPlayersValidator extends AbstractValidator{
	
	// LOGGER for the class UC37_TeamsAndPlayersValidator
	private static final Logger LOGGER = Logger.getLogger( UC37_TeamsAndPlayersValidator.class);
	
	
	/**
	* Operation validate for UC37_TeamsAndPlayersForm
	* @param obj : the current form (UC37_TeamsAndPlayersForm)
	* @param errors : The spring errors to return for the form UC37_TeamsAndPlayersForm
	*/
	public void validate(final Object obj,final Errors errors){
		LOGGER.info("Beginning method : validate"); // UC37_TeamsAndPlayersValidator
		UC37_TeamsAndPlayersForm cUC37_TeamsAndPlayersForm = (UC37_TeamsAndPlayersForm)obj; // UC37_TeamsAndPlayersValidator
		LOGGER.info("Ending method : validate the form "+ cUC37_TeamsAndPlayersForm.getClass().getName()); // UC37_TeamsAndPlayersValidator
	}

	/**
	* Method to implements to use spring validators (UC37_TeamsAndPlayersForm)
	* @param aClass : Class for the form UC37_TeamsAndPlayersForm
	* @return boolean
	*/
	public boolean supports(final Class aClass){
		return (new UC37_TeamsAndPlayersForm()).getClass().equals(aClass); // UC37_TeamsAndPlayersValidator
	}
}
