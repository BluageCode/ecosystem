package uc210;

import org.junit.Test;
import test.common.AbstractBADocTestCase;

public class ITuc210 extends AbstractBADocTestCase {
	
	@Override
	public  String getName(){
		return "UC-210";
	}
	
	@Test
	public void testUC210() throws Exception {
		// On teste l'UC210 - External Tools - Sonar
				
		selenium.open("");	
		selenium.click("link=Expand All");
		selenium.waitForPageToLoad("300000");
		selenium.click("link=UC210 - Sonar");
		
		verifyTrue(selenium.isElementPresent("//h2[contains(text(),\"Advanced Features > External Tools > UC210 > Sonar\")]"));
		assertTrue(selenium.isElementPresent("//p//a[contains(text(),\"http://docs.codehaus.org/display/SONAR/Documentation\")]"));
		assertTrue(selenium.isElementPresent("//p//img[contains(@alt,'Screen Sonar')]"));
	}

	
}
