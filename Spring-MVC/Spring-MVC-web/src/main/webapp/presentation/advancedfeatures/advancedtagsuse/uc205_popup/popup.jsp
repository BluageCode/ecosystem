<%@ include file="/include.jsp"  %>

	<form:form id="popup" commandName="popupForm"  enctype="application/x-www-form-urlencoded" method="post">
	<div id="cp_traitement" style="display: none;">
<table  >
<tr  > 
<td rowspan="1" colspan="1"  >
<a id="btn_back"onclick="javascript:btn_click_cExecutepopup(this.href);return false;"				href="<c:url value="/presentation/advancedfeatures/advancedtagsuse/uc205_popup/popup/btn_back.html"/>"
							 shape="rect"  >									 
		<img id="imgtl1"  src="<c:url value= "/presentation/_style/picto/page_prev.gif"/>" alt="Visualiser" />
</a>
</td> 
</tr>
</table> 
<br  /> 
<display:table uid="tab_players"  sort="list" requestURI="" name="sessionScope.popupForm.allPlayers" class="rd_tabledata"  >
	<c:set   var="rownum">
		<%=(tab_players !=	null) ? new Integer(tab_players_rowNum.intValue() - 1) : new Integer(0) %>
	</c:set>
	<c:set   var="idRow">
		<%=(tab_players != null ) ? tab_players_rowNum.intValue() - 1 : -1 %>
	</c:set>
	<c:set   var="idTab">
		<%=(tab_players != null ) ? "tab_players" : null %>
	</c:set>
	<c:set var="tableHashId">
		<%= new org.displaytag.util.ParamEncoder("tab_players").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE) %>
	</c:set>
	<c:set var="tableHashId2">
		<%=request.getParameter(new org.displaytag.util.ParamEncoder("tab_players").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE)) %>
	</c:set>	
	
<display:column format=""  titleKey="dynamicpopup_FIRST_NAME">
		<span id="txt_firstName">
	<c:out value="${tab_players.firstName}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="dynamicpopup_LAST_NAME">
		<span id="txt_lastName">
	<c:out value="${tab_players.lastName}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="dynamicpopup_DATE_OF_BIRTH">
<span id="txt_dateOfBirth">
	<format:formatDateValidator value="${tab_players.dateOfBirth}" pattern="MM/dd/yyyy" />
</span>
	</display:column>
	
<display:column format=""  titleKey="dynamicpopup_ESTIMATED_VALUE">
		<span id="txt_estimatedValue">
	<c:out value="${tab_players.estimatedValue}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="dynamicpopup_WEIGHT_KG">
		<span id="txt_weight">
	<c:out value="${tab_players.weight}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="dynamicpopup_HEIGHT_CM">
		<span id="txt_height">
	<c:out value="${tab_players.height}" />
		</span>
	</display:column>
</display:table>
	</div>
	<c:if test="${popupForm.cp_traitementShowPopUp==true}"> 
		<script type="text/javascript">
			var modalpopup = dhtmlmodal.open('popup_cp_traitement', 'div','cp_traitement','Bluage Popup', 'display-order=1,width=650px,height=520px,center=1,left=200px,top=110px,resize=1,scrolling=1', 'recal');
			modalpopup.onclose=function(){
				return true
			}
		</script>
	</c:if>
	</form:form>
	<script language="javascript">
		function btn_click_cExecutepopup(url) {
			popupvar = document.getElementById('popup');
			popupvar.setAttribute('action', url);
			popupvar.submit();
		}
		function btn_click_cSetActionpopup(url) {
			popupvar = document.getElementById('popup');
			popupvar.setAttribute('action', url);
		}
	</script>
