/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.exception.uc73_exception ;


import org.apache.log4j.Logger;
import org.springframework.validation.Errors;

import com.netfective.bluage.core.validator.AbstractValidator;
/**
* Class :UC73_ExceptionValidator
*/
public class UC73_ExceptionValidator extends AbstractValidator{
	
	// LOGGER for the class UC73_ExceptionValidator
	private static final Logger LOGGER = Logger.getLogger( UC73_ExceptionValidator.class);
	
	
	/**
	* Operation validate for UC73_ExceptionForm
	* @param obj : the current form (UC73_ExceptionForm)
	* @param errors : The spring errors to return for the form UC73_ExceptionForm
	*/
	public void validate(final Object obj,final Errors errors){
		LOGGER.info("Beginning method : validate"); // UC73_ExceptionValidator
		UC73_ExceptionForm cUC73_ExceptionForm = (UC73_ExceptionForm)obj; // UC73_ExceptionValidator
		LOGGER.info("Ending method : validate the form "+ cUC73_ExceptionForm.getClass().getName()); // UC73_ExceptionValidator
	}

	/**
	* Method to implements to use spring validators (UC73_ExceptionForm)
	* @param aClass : Class for the form UC73_ExceptionForm
	* @return boolean
	*/
	public boolean supports(final Class aClass){
		return (new UC73_ExceptionForm()).getClass().equals(aClass); // UC73_ExceptionValidator
	}
}
