package uc04;

import org.junit.Test;
import test.common.AbstractBADocTestCase;


public class IT03uc04VerifModificationNomJoueur extends AbstractBADocTestCase {
	

	@Override
	public  String getName(){
		return "UC-04";
	}
	
	@Test
	public void testIT03uc04VerifModificationNomJoueur() throws Exception {
		// On teste l'UC04 - Getting started - Entities modification
		selenium.open("");
		selenium.waitForPageToLoad("30000");
		selenium.click("link=Expand All");
		selenium.click("link=UC04 - Entities Modification");
		verifyTrue(selenium.isElementPresent("//h2[contains(text(),\"Getting started > UC04 > Entities Modification\")]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/thead/tr[th[contains(text(),\"Name\")] and th[contains(text(),\"Date of Birth\")] and th[contains(text(),\"Weight (kg)\")] and th[contains(text(),\"Height (cm)\")] and th[contains(text(),\"Estimated Value (M$)\")] and th[contains(text(),\"Position\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Laplace\")] and td/span[contains(text(),\"Bernard\")] and td/span[contains(text(),\"07/14/1980\")] and td/span[contains(text(),\"95\")] and td/span[contains(text(),\"191\")] and td/span[contains(text(),\"37.32\")] and td/span[contains(text(),\"PF\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Sorel\")] and td/span[contains(text(),\"Julien\")] and td/span[contains(text(),\"01/20/1975\")] and td/span[contains(text(),\"87\")] and td/span[contains(text(),\"182\")] and td/span[contains(text(),\"23.55\")] and td/span[contains(text(),\"C\")]]"));
		// On vérifie que le joueur 'Julien Sorel' existe.
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Sorel\")] and td/span[contains(text(),\"Julien\")]]"));
		// On indexe la ligne de 'Julien Sorel'.
		Number index = selenium.getElementIndex("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Sorel\")] and td/span[contains(text(),\"Julien\")]]");
		selenium.click("//table[contains(@id,'tab_players')]/tbody/tr[" + index + " + 1]/td[7]/a[contains(text(),\"[edit]\")]");
		selenium.waitForPageToLoad("30000");
		// On change le nom du joueur.
		selenium.type("//table[contains(@id,'tab_players')]/tbody/tr[" + index + " + 1]/td/input[1]", "");
		selenium.type("//table[contains(@id,'tab_players')]/tbody/tr[" + index + " + 1]/td/input[2]", "");
		selenium.click("link=[update]");
		selenium.waitForPageToLoad("30000");
		// On vérifie si les messages d'erreur sont affichés correctement.
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[" + index + " + 1]/td[1]/span[1]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[" + index + " + 1]/td[1]/span[2]"));
		assertEquals("Last name is required.", selenium.getText("//table[contains(@id,'tab_players')]/tbody/tr[" + index + " + 1]/td[1]/span[1]"));
		assertEquals("First name is required.", selenium.getText("//table[contains(@id,'tab_players')]/tbody/tr[" + index + " + 1]/td[1]/span[2]"));
		// On change le nom du joueur invisible.
		selenium.type("//table[contains(@id,'tab_players')]/tbody/tr[" + index + " + 1]/td/input[1]", "Sorel");
		selenium.type("//table[contains(@id,'tab_players')]/tbody/tr[" + index + " + 1]/td/input[2]", "Julien");
		selenium.click("link=[update]");
		selenium.waitForPageToLoad("30000");
		// On vérifie que le joueur Julien Sorel existe.
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Sorel\")] and td/span[contains(text(),\"Julien\")]]"));
	}

	
}
