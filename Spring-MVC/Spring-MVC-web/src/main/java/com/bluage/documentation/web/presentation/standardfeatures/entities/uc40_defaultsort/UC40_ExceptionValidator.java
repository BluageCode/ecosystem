/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.entities.uc40_defaultsort ;


import org.apache.log4j.Logger;
import org.springframework.validation.Errors;

import com.netfective.bluage.core.validator.AbstractValidator;
/**
* Class :UC40_ExceptionValidator
*/
public class UC40_ExceptionValidator extends AbstractValidator{
	
	// LOGGER for the class UC40_ExceptionValidator
	private static final Logger LOGGER = Logger.getLogger( UC40_ExceptionValidator.class);
	
	
	/**
	* Operation validate for UC40_ExceptionForm
	* @param obj : the current form (UC40_ExceptionForm)
	* @param errors : The spring errors to return for the form UC40_ExceptionForm
	*/
	public void validate(final Object obj,final Errors errors){
		LOGGER.info("Beginning method : validate"); // UC40_ExceptionValidator
		UC40_ExceptionForm cUC40_ExceptionForm = (UC40_ExceptionForm)obj; // UC40_ExceptionValidator
		LOGGER.info("Ending method : validate the form "+ cUC40_ExceptionForm.getClass().getName()); // UC40_ExceptionValidator
	}

	/**
	* Method to implements to use spring validators (UC40_ExceptionForm)
	* @param aClass : Class for the form UC40_ExceptionForm
	* @return boolean
	*/
	public boolean supports(final Class aClass){
		return (new UC40_ExceptionForm()).getClass().equals(aClass); // UC40_ExceptionValidator
	}
}
