/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.gettingstarted.uc01_helloworld ;

// Import declaration.
// Java imports.
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.bluage.documentation.business.gettingstarted.uc01_helloworld.bos.HelloWorld01BO;
import com.bluage.documentation.service.gettingstarted.uc01_helloworld.ServiceHelloWorld;
import com.netfective.bluage.core.exception.ApplicationException;
import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;
import com.netfective.bluage.core.web.IForm;

/**
 * Class : UC01_HelloWorldController
 */
@Controller
@Scope("prototype")
@SessionAttributes("uC01_HelloWorldForm")
@RequestMapping(value= "/presentation/gettingstarted/uc01_helloworld")
public class UC01_HelloWorldController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC01_HelloWorldController.class);
	
	//Current form name
	private static final String U_C01__HELLO_WORLD_FORM = "uC01_HelloWorldForm";

		//CONSTANT: helloWorld
	private static final String HELLO_WORLD = "helloWorld";
	
	/**
	 * Property:customDateEditorsUC01_HelloWorldController
	 */
	final private Map<String,CustomDateEditor> customDateEditorsUC01_HelloWorldController = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:uC01_HelloWorldValidator
	 */
	final private UC01_HelloWorldValidator uC01_HelloWorldValidator = new UC01_HelloWorldValidator();
	
	/**
	 * Service declaration : serviceHelloWorld.
	 */
	@Autowired
	private ServiceHelloWorld serviceHelloWorld;
	
	
	// Initialise all the instances for UC01_HelloWorldController
			// Declare the instance helloWorld
		private HelloWorld01BO helloWorld;
			/**
	 * Operation : lnk_display
 	 * @param model : 
 	 * @param uC01_HelloWorld : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC01_HelloWorld/lnk_display.html",method = RequestMethod.POST)
	public String lnk_display(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC01_HelloWorldForm") UC01_HelloWorldForm  uC01_HelloWorldForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : lnk_display"); 
		infoLogger(uC01_HelloWorldForm); 
		uC01_HelloWorldForm.setAlwaysCallPreControllers(false); 
		// initialization for lnk_display
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state lnk_display if the validation fail, you will be forward to the screen : "/presentation/gettingstarted/uc01_helloworld/UC01_HelloWorld"
		if(errorsMessages(request,response,uC01_HelloWorldForm,bindingResult,"", uC01_HelloWorldValidator, customDateEditorsUC01_HelloWorldController)){ 
			return "/presentation/gettingstarted/uc01_helloworld/UC01_HelloWorld"; 
		}

	 callDisplayHelloWorld(request,uC01_HelloWorldForm );
						// Init the destination form com.bluage.documentation.web.presentation.gettingstarted.uc01_helloworld.UC01_HelloWorldForm or not from lnk_display
			// Populate the destination form with all the instances returned from lnk_display.
			final IForm uC01_HelloWorldForm2 = populateDestinationForm(request.getSession(), uC01_HelloWorldForm); 
					
			// Add the form to the model.
			model.addAttribute("uC01_HelloWorldForm", uC01_HelloWorldForm2); 
			
			request.getSession().setAttribute("uC01_HelloWorldForm", uC01_HelloWorldForm2);
			
			// "OK" CASE => destination screen path from lnk_display
			LOGGER.info("Go to the screen 'UC01_HelloWorld'.");
			// Redirect (PRG) from lnk_display
			destinationPath =  "redirect:/presentation/gettingstarted/uc01_helloworld/UC01_HelloWorld.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_display 
	}
	
				/**
	 * Operation : lnk_reset
 	 * @param model : 
 	 * @param uC01_HelloWorld : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC01_HelloWorld/lnk_reset.html",method = RequestMethod.POST)
	public String lnk_reset(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC01_HelloWorldForm") UC01_HelloWorldForm  uC01_HelloWorldForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : lnk_reset"); 
		infoLogger(uC01_HelloWorldForm); 
		uC01_HelloWorldForm.setAlwaysCallPreControllers(false); 
		// initialization for lnk_reset
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state lnk_reset if the validation fail, you will be forward to the screen : "/presentation/gettingstarted/uc01_helloworld/UC01_HelloWorld"
		if(errorsMessages(request,response,uC01_HelloWorldForm,bindingResult,"", uC01_HelloWorldValidator, customDateEditorsUC01_HelloWorldController)){ 
			return "/presentation/gettingstarted/uc01_helloworld/UC01_HelloWorld"; 
		}

	 callResetHelloWorld(request,uC01_HelloWorldForm );
						// Init the destination form com.bluage.documentation.web.presentation.gettingstarted.uc01_helloworld.UC01_HelloWorldForm or not from lnk_reset
			// Populate the destination form with all the instances returned from lnk_reset.
			final IForm uC01_HelloWorldForm2 = populateDestinationForm(request.getSession(), uC01_HelloWorldForm); 
					
			// Add the form to the model.
			model.addAttribute("uC01_HelloWorldForm", uC01_HelloWorldForm2); 
			
			request.getSession().setAttribute("uC01_HelloWorldForm", uC01_HelloWorldForm2);
			
			// "OK" CASE => destination screen path from lnk_reset
			LOGGER.info("Go to the screen 'UC01_HelloWorld'.");
			// Redirect (PRG) from lnk_reset
			destinationPath =  "redirect:/presentation/gettingstarted/uc01_helloworld/UC01_HelloWorld.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_reset 
	}
	
	
	/**
	* This method initialise the form : UC01_HelloWorldForm 
	* @return UC01_HelloWorldForm
	*/
	@ModelAttribute("UC01_HelloWorldFormInit")
	public void initUC01_HelloWorldForm(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, U_C01__HELLO_WORLD_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method UC01_HelloWorldForm."); //for lnk_reset 
		}
		UC01_HelloWorldForm uC01_HelloWorldForm;
	
		if(request.getSession().getAttribute(U_C01__HELLO_WORLD_FORM) != null){
			uC01_HelloWorldForm = (UC01_HelloWorldForm)request.getSession().getAttribute(U_C01__HELLO_WORLD_FORM);
		} else {
			uC01_HelloWorldForm = new UC01_HelloWorldForm();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form UC01_HelloWorldForm.");
		}
		uC01_HelloWorldForm = (UC01_HelloWorldForm)populateDestinationForm(request.getSession(), uC01_HelloWorldForm);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  UC01_HelloWorldForm.");
		}
		model.addAttribute(U_C01__HELLO_WORLD_FORM, uC01_HelloWorldForm);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param UC01_HelloWorldForm : The sceen form.
	 */
	@RequestMapping(value = "/UC01_HelloWorld.html" ,method = RequestMethod.GET)
	public void prepareUC01_HelloWorld(final HttpServletRequest request,final  Model model,final  @ModelAttribute("UC01_HelloWorldFormInit") UC01_HelloWorldForm uC01_HelloWorldForm){
		
		UC01_HelloWorldForm currentUC01_HelloWorldForm = uC01_HelloWorldForm;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(U_C01__HELLO_WORLD_FORM) == null){
			if(currentUC01_HelloWorldForm!=null){
				request.getSession().setAttribute(U_C01__HELLO_WORLD_FORM, currentUC01_HelloWorldForm);
			}else {
				currentUC01_HelloWorldForm = new UC01_HelloWorldForm();
				request.getSession().setAttribute(U_C01__HELLO_WORLD_FORM, currentUC01_HelloWorldForm);	
			}
		} else {
			currentUC01_HelloWorldForm = (UC01_HelloWorldForm) request.getSession().getAttribute(U_C01__HELLO_WORLD_FORM);
		}

		currentUC01_HelloWorldForm = (UC01_HelloWorldForm)populateDestinationForm(request.getSession(), currentUC01_HelloWorldForm);
		// Call all the Precontroller.
	currentUC01_HelloWorldForm.setAlwaysCallPreControllers(true);
		model.addAttribute(U_C01__HELLO_WORLD_FORM, currentUC01_HelloWorldForm);
		request.getSession().setAttribute(U_C01__HELLO_WORLD_FORM, currentUC01_HelloWorldForm);
		
	}
	
	/**
	 * method callDisplayHelloWorld
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callDisplayHelloWorld(HttpServletRequest request,IForm form) {
		helloWorld = (HelloWorld01BO)get(request.getSession(),form, "helloWorld");  
										 
					try {
				// executing DisplayHelloWorld in lnk_reset
				helloWorld = 	serviceHelloWorld.displayHelloWorld(
	);  
 
				put(request.getSession(), HELLO_WORLD,helloWorld);
								// processing variables DisplayHelloWorld in lnk_reset

			} catch (ApplicationException e) { 
				// error handling for operation displayHelloWorld called DisplayHelloWorld
				errorLogger("An error occured during the execution of the operation : displayHelloWorld",e); 
		
			}
	}
		/**
	 * method callResetHelloWorld
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callResetHelloWorld(HttpServletRequest request,IForm form) {
		helloWorld = (HelloWorld01BO)get(request.getSession(),form, "helloWorld");  
										 
					try {
				// executing ResetHelloWorld in lnk_reset
				helloWorld = 	serviceHelloWorld.resetHelloWorld(
	);  
 
				put(request.getSession(), HELLO_WORLD,helloWorld);
								// processing variables ResetHelloWorld in lnk_reset

			} catch (ApplicationException e) { 
				// error handling for operation resetHelloWorld called ResetHelloWorld
				errorLogger("An error occured during the execution of the operation : resetHelloWorld",e); 
		
			}
	}
		
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("UC01_HelloWorldController [ ");
				strBToS.append(HELLO_WORLD);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(helloWorld);
			strBToS.append(END_TO_STRING_DELIMITER);
		// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(uC01_HelloWorldValidator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsUC01_HelloWorldController!=null); 
		return strBToS.toString();
	}
}
