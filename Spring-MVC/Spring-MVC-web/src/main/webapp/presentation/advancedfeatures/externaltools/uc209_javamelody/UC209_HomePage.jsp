<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file="/include.jsp"  %>
<html >
<head >
<title  > 
<fmt:message key="cUC209_HomePage_BLU_AGE_DOCUMENTATION"/>
</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"  > 
</meta>
<link href="<c:url value= "/presentation/_style/_style.css"/>" rel="stylesheet" type="text/css"  /> 
<link href="<c:url value= "/presentation/_include/jquery/rd_lib.css"/>" rel="stylesheet" type="text/css"  /> 
<script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/jquery.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/rd_setup.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/rd_lib.js"/>" space="preserve"  ></script><link rel="shortcut icon" href="<c:url value= "/presentation/_style/favicon.ico"/>"  /> 

</head>

<body class=" " > 
<div id="rd_ghost"  > 
<small  > 
<fmt:message key="cUC209_HomePage___CSS_DEACTIVATED"/>
</small>
</div>
<div id="rd_jghost"  > 
<small  > 
<fmt:message key="cUC209_HomePage___JAVASCRIPT_DEACTIVATED"/>
</small>
</div>
<div id="shape"  > 
<div class="ui-layout-center"  > 
<div class="east-north"  > 
<h2 class="rd_first"  > 
<fmt:message key="cUC209_HomePage_ADVANCED_FEATURES__EXTERNAL_T"/>
</h2>
<p  > 
<fmt:message key="cUC209_HomePage________THE_GOAL_OF_JAVAMELODY_"/>
<br clear="none"  /> 
<fmt:message key="cUC209_HomePage____________GIVE_FACTS_ABOUT_T"/>
<br clear="none"  /> 
<fmt:message key="cUC209_HomePage____________MAKE_DECISIONS_WHE"/>
<br clear="none"  /> 
<fmt:message key="cUC209_HomePage____________OPTIMIZE_BASED_ON_"/>
<br clear="none"  /> 
<fmt:message key="cUC209_HomePage____________FIND_THE_ROOT_CAUS"/>
<br clear="none"  /> 
<fmt:message key="cUC209_HomePage____________VERIFY_THE_REAL_IM"/>
<br clear="none"  /> 
</p>
<p  > 
<fmt:message key="cUC209_HomePage________LICENSE_LGPL______"/>
</p>
<p  > 
<fmt:message key="cUC209_HomePage________IN_ORDER_TO_USE_AND_TES"/>
<a href="http://tutorial.bluage.com/monitoring" shape="rect"  >
<fmt:message key="cUC209_HomePage_HTTPTUTORIALBLUAGECOMMON"/>
</a> 
<br clear="none"  /> 
<fmt:message key="cUC209_HomePage________SO_AS_TO_CONNECT_USE_T"/>
<br clear="none"  /> 
<fmt:message key="cUC209_HomePage_________USERNAME__BLUAGE"/>
<br clear="none"  /> 
<fmt:message key="cUC209_HomePage_________PASSWORD__BLUAGE"/>
<br clear="none"  /> 
</p>
<p  > 
<fmt:message key="cUC209_HomePage________FOR_MORE_INFORMATION__"/>
<a href="http://javamelody.googlecode.com/" shape="rect"  >
<fmt:message key="cUC209_HomePage_HTTPJAVAMELODYGOOGLECODEC"/>
</a> 
</p>
<p  > 
<img src="<c:url value= "/presentation/advancedfeatures/externaltools/uc209_javamelody/00-screen-javamelody.png"/>" alt="Screen JavaMelody"  />
</p>
</div>
<div class="east-center"  > 
<tiles:insertDefinition	name="presentation/advancedfeatures/externaltools/uc209_javamelody/UC209_HomePagecmp_tabs" />
</div>
</div>
<div class="ui-layout-north"  > 
<tiles:insertDefinition	name="presentation/advancedfeatures/externaltools/uc209_javamelody/UC209_HomePagecmp_header" />
</div>
<div class="ui-layout-south"  > 
<fmt:message key="cUC209_HomePage_______"/>
<span id="rd_jversion"  > 
<fmt:message key="cUC209_HomePage_BLU_AGE_DOCUMENTATION_1"/>
</span>
</div>
<div class="ui-layout-west"  > 
<tiles:insertDefinition	name="presentation/advancedfeatures/externaltools/uc209_javamelody/UC209_HomePagecmp_treeview" />
</div>
</div>
</body>
</html>
