/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.service.standardfeatures.application.datagrid.uc38_selectedlist;


import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import com.bluage.documentation.business.standardfeatures.application.datagrid.uc38_selectedlist.bos.Player38BO;
import com.bluage.documentation.business.standardfeatures.application.datagrid.uc38_selectedlist.bos.Team38BO;
import com.bluage.documentation.business.standardfeatures.application.datagrid.uc38_selectedlist.entities.dao.Player38DAOImpl;
import com.bluage.documentation.service.AbstractService;
import com.bluage.documentation.service.standardfeatures.application.datagrid.uc38_selectedlist.daofinder.ServicePlayer38DAOMethodFinderImpl;
import com.netfective.bluage.core.exception.ApplicationException;

/**
 * Defines  application services for ServicePlayer38
  */
@Named("com.bluage.documentation.service.standardfeatures.application.datagrid.uc38_selectedlist.ServicePlayer38")
public class ServicePlayer38  extends AbstractService{


			@Inject
		@Named ("com.bluage.documentation.business.standardfeatures.application.datagrid.uc38_selectedlist.entities.dao.Player38DAOImpl")
		private Player38DAOImpl player38BODao;
					@Inject
		@Named ("com.bluage.documentation.service.standardfeatures.application.datagrid.uc38_selectedlist.ServiceTeam38")
		private ServiceTeam38  instanceServiceTeam38;
								@Inject
		@Named ("com.bluage.documentation.service.standardfeatures.application.datagrid.uc38_selectedlist.daofinder.ServicePlayer38DAOMethodFinder")
		private ServicePlayer38DAOMethodFinderImpl servicePlayer38Dao;



	/**
	 *  Default constructor 
	 */
	public ServicePlayer38() {
		super(); // Call super for default constructor : ServicePlayer38
	}
	


			
				
	/**    
	 * Operation Crud
	 * @param bo {@link Player38BO}
		 * @return {@link boolean}
	 */
	public boolean player38Update(Player38BO bo )
 throws ApplicationException {

					return player38BODao.update(bo);
	}

		
				
	/**    
	 * Operation HQL.
	 * @return {@link List}
	 */
	public List loadPlayers38( )
 throws ApplicationException {
	
	return servicePlayer38Dao.loadPlayers38();
	}

		
			
	/**    
	 * Process operation.
	 * @param playerToUpdate {@link Player38BO}
		 * @return {@link void}
	 */
	public void updatePlayer(Player38BO playerToUpdate )
 throws ApplicationException {

			privateUpdatePlayer(playerToUpdate);
	}
	

	/**    
	 * Process operation.
	 * @param playerToUpdate {@link Player38BO}
		 * @return {@link void}
	 */
	private void privateUpdatePlayer(Player38BO playerToUpdate )
 throws ApplicationException {
		// Central buffer node declaration for operation updatePlayer
			Team38BO	 teamResult =  new Team38BO();
					Team38BO	 team38 =  new Team38BO();
						// Calling an operation : Update Player
player38Update(playerToUpdate);
// Calling an operation : Get Team
team38 = playerToUpdate.getTeam38();
// Calling an operation : Find Team By Id
teamResult = instanceServiceTeam38.Team38FindByID(team38);
// Calling an operation : Set Team
playerToUpdate.setTeam38(teamResult);

		}

		}
