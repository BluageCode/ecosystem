/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.service.standardfeatures.entities.uc40_defaultsort;


import javax.inject.Inject;
import javax.inject.Named;

import com.bluage.documentation.business.standardfeatures.entities.uc40_defaultsort.bos.State40BO;
import com.bluage.documentation.business.standardfeatures.entities.uc40_defaultsort.entities.dao.State40DAOImpl;
import com.bluage.documentation.service.AbstractService;
import com.bluage.documentation.service.standardfeatures.entities.uc40_defaultsort.daofinder.ServiceState40DAOMethodFinderImpl;
import com.netfective.bluage.core.exception.ApplicationException;

/**
 * Defines  application services for ServiceState40
  */
@Named("com.bluage.documentation.service.standardfeatures.entities.uc40_defaultsort.ServiceState40")
public class ServiceState40  extends AbstractService{


			@Inject
		@Named ("com.bluage.documentation.business.standardfeatures.entities.uc40_defaultsort.entities.dao.State40DAOImpl")
		private State40DAOImpl state40BODao;
			@Inject
		@Named ("com.bluage.documentation.service.standardfeatures.entities.uc40_defaultsort.daofinder.ServiceState40DAOMethodFinder")
		private ServiceState40DAOMethodFinderImpl serviceState40Dao;



	/**
	 *  Default constructor 
	 */
	public ServiceState40() {
		super(); // Call super for default constructor : ServiceState40
	}
	


			
				
	/**    
	 * Operation Crud
	 * @param bo {@link State40BO}
		 * @return {@link boolean}
	 */
	public boolean state40Create(State40BO bo )
 throws ApplicationException {

					return state40BODao.create(bo);
	}

		
			
	/**    
	 * Operation HQL.
	 * @param code {@link String}
		 * @return {@link State40BO}
	 */
	public State40BO searchState40ByCriteria(String code )
 throws ApplicationException {
	
	return serviceState40Dao.searchState40ByCriteria(code);
	}

		}
