/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.services.standard.uc58_servicecopy ;

// Import declaration.
// Java imports.
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.bluage.documentation.business.standardfeatures.services.standard.uc58_servicecopy.bos.Team58BO;
import com.bluage.documentation.business.standardfeatures.services.standard.uc58_servicecopy.entities.daofinder.Team58DAOFinderImpl;
import com.bluage.documentation.service.standardfeatures.services.standard.uc58_servicecopy.ServiceCopyTeam58;
import com.bluage.documentation.service.standardfeatures.services.standard.uc58_servicecopy.Serviceteam58FindByID;
import com.netfective.bluage.core.exception.ApplicationException;
import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;
import com.netfective.bluage.core.web.IForm;

/**
 * Class : UC58_DisplayTeams58Controller
 */
@Controller
@Scope("prototype")
@SessionAttributes("uC58_DisplayTeams58Form")
@RequestMapping(value= "/presentation/standardfeatures/services/standard/uc58_servicecopy")
public class UC58_DisplayTeams58Controller extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC58_DisplayTeams58Controller.class);
	
	//Current form name
	private static final String U_C58__DISPLAY_TEAMS58_FORM = "uC58_DisplayTeams58Form";

	//CONSTANT: allTeams table or repeater.
	private static final String ALL_TEAMS = "allTeams";
				//CONSTANT: teamDetails
	private static final String TEAM_DETAILS = "teamDetails";
	//CONSTANT: selectedTeam
	private static final String SELECTED_TEAM = "selectedTeam";
	//CONSTANT: copyTeam
	private static final String COPY_TEAM = "copyTeam";
	
	/**
	 * Property:customDateEditorsUC58_DisplayTeams58Controller
	 */
	final private Map<String,CustomDateEditor> customDateEditorsUC58_DisplayTeams58Controller = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:uC58_DisplayTeams58Validator
	 */
	final private UC58_DisplayTeams58Validator uC58_DisplayTeams58Validator = new UC58_DisplayTeams58Validator();
	
	/**
	 * Service declaration : serviceteam58FindByID.
	 */
	@Autowired
	private Serviceteam58FindByID serviceteam58FindByID;
	
	/**
	 * Service declaration : serviceCopyTeam58.
	 */
	@Autowired
	private ServiceCopyTeam58 serviceCopyTeam58;
	
	/**
	 * Generic Finder : team58DAOFinderImpl.
	 */
	@Autowired
	private Team58DAOFinderImpl team58DAOFinderImpl;
	
	
	// Initialise all the instances for UC58_DisplayTeams58Controller
		// Initialize the instance allTeams for the state lnk_loadPlayers
		private List allTeams; // Initialize the instance allTeams for UC58_DisplayTeams58Controller
						// Declare the instance teamDetails
		private Team58BO teamDetails;
		// Declare the instance selectedTeam
		private Team58BO selectedTeam;
		// Declare the instance copyTeam
		private Team58BO copyTeam;
			/**
	 * Operation : lnk_view
 	 * @param model : 
 	 * @param uC58_DisplayTeams58 : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC58_DisplayTeams58/lnk_view.html",method = RequestMethod.POST)
	public String lnk_view(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC58_DisplayTeams58Form") UC58_DisplayTeams58Form  uC58_DisplayTeams58Form, final BindingResult bindingResult ,final @RequestParam(ID_REQUEST_PARAM) Integer index){
		
		
		
		LOGGER.info("Begin method : lnk_view"); 
		infoLogger(uC58_DisplayTeams58Form); 
		uC58_DisplayTeams58Form.setAlwaysCallPreControllers(false); 
		// initialization for lnk_view
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state lnk_view if the validation fail, you will be forward to the screen : "/presentation/standardfeatures/services/standard/uc58_servicecopy/UC58_DisplayTeams58"
		if(errorsMessages(request,response,uC58_DisplayTeams58Form,bindingResult,"selectedTeam", uC58_DisplayTeams58Validator, customDateEditorsUC58_DisplayTeams58Controller)){ 
			return "/presentation/standardfeatures/services/standard/uc58_servicecopy/UC58_DisplayTeams58"; 
		}

										 callTeam58FindByID(request,uC58_DisplayTeams58Form , index);
			
				 callCopyBo58(request,uC58_DisplayTeams58Form );
											// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.services.standard.uc58_servicecopy.UC58_CopyDetailsTeam58Form or not from lnk_view
			final UC58_CopyDetailsTeam58Form uC58_CopyDetailsTeam58Form =  new UC58_CopyDetailsTeam58Form();
			// Populate the destination form with all the instances returned from lnk_view.
			final IForm uC58_CopyDetailsTeam58Form2 = populateDestinationForm(request.getSession(), uC58_CopyDetailsTeam58Form); 
					
			// Add the form to the model.
			model.addAttribute("uC58_CopyDetailsTeam58Form", uC58_CopyDetailsTeam58Form2); 
			
			request.getSession().setAttribute("uC58_CopyDetailsTeam58Form", uC58_CopyDetailsTeam58Form2);
			
			// "OK" CASE => destination screen path from lnk_view
			LOGGER.info("Go to the screen 'UC58_CopyDetailsTeam58'.");
			// Redirect (PRG) from lnk_view
			destinationPath =  "redirect:/presentation/standardfeatures/services/standard/uc58_servicecopy/UC58_CopyDetailsTeam58.html";
						
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_view 
	}
	
	
	/**
	* This method initialise the form : UC58_DisplayTeams58Form 
	* @return UC58_DisplayTeams58Form
	*/
	@ModelAttribute("UC58_DisplayTeams58FormInit")
	public void initUC58_DisplayTeams58Form(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, U_C58__DISPLAY_TEAMS58_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method UC58_DisplayTeams58Form."); //for lnk_view 
		}
		UC58_DisplayTeams58Form uC58_DisplayTeams58Form;
	
		if(request.getSession().getAttribute(U_C58__DISPLAY_TEAMS58_FORM) != null){
			uC58_DisplayTeams58Form = (UC58_DisplayTeams58Form)request.getSession().getAttribute(U_C58__DISPLAY_TEAMS58_FORM);
		} else {
			uC58_DisplayTeams58Form = new UC58_DisplayTeams58Form();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form UC58_DisplayTeams58Form.");
		}
		uC58_DisplayTeams58Form = (UC58_DisplayTeams58Form)populateDestinationForm(request.getSession(), uC58_DisplayTeams58Form);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  UC58_DisplayTeams58Form.");
		}
		model.addAttribute(U_C58__DISPLAY_TEAMS58_FORM, uC58_DisplayTeams58Form);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param UC58_DisplayTeams58Form : The sceen form.
	 */
	@RequestMapping(value = "/UC58_DisplayTeams58.html" ,method = RequestMethod.GET)
	public void prepareUC58_DisplayTeams58(final HttpServletRequest request,final  Model model,final  @ModelAttribute("UC58_DisplayTeams58FormInit") UC58_DisplayTeams58Form uC58_DisplayTeams58Form){
		
		UC58_DisplayTeams58Form currentUC58_DisplayTeams58Form = uC58_DisplayTeams58Form;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(U_C58__DISPLAY_TEAMS58_FORM) == null){
			if(currentUC58_DisplayTeams58Form!=null){
				request.getSession().setAttribute(U_C58__DISPLAY_TEAMS58_FORM, currentUC58_DisplayTeams58Form);
			}else {
				currentUC58_DisplayTeams58Form = new UC58_DisplayTeams58Form();
				request.getSession().setAttribute(U_C58__DISPLAY_TEAMS58_FORM, currentUC58_DisplayTeams58Form);	
			}
		} else {
			currentUC58_DisplayTeams58Form = (UC58_DisplayTeams58Form) request.getSession().getAttribute(U_C58__DISPLAY_TEAMS58_FORM);
		}

		try {
			List allTeams = team58DAOFinderImpl.findAll();
			put(request.getSession(), ALL_TEAMS, allTeams);
		} catch (ApplicationException e) {
			errorLogger("An error while calling the generic finder for the instance : allTeams.",e);
		}
				currentUC58_DisplayTeams58Form = (UC58_DisplayTeams58Form)populateDestinationForm(request.getSession(), currentUC58_DisplayTeams58Form);
		// Call all the Precontroller.
	currentUC58_DisplayTeams58Form.setAlwaysCallPreControllers(true);
		model.addAttribute(U_C58__DISPLAY_TEAMS58_FORM, currentUC58_DisplayTeams58Form);
		request.getSession().setAttribute(U_C58__DISPLAY_TEAMS58_FORM, currentUC58_DisplayTeams58Form);
		
	}
	
										/**
	 * method callTeam58FindByID
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 * @param index Integer the current index 
	 */
	private void callTeam58FindByID(HttpServletRequest request,IForm form,Integer index) {
		teamDetails = (Team58BO)get(request.getSession(),form, "teamDetails");  
										 
					if(index!=null){  
			 selectedTeam = (Team58BO)((List)get(request.getSession(),form, "allTeams")).get(index);  
		} else {
			 selectedTeam= null; 
		}
				 
					try {
				// executing Team58FindByID in lnk_view
				teamDetails = 	serviceteam58FindByID.team58FindByID(
			selectedTeam
			);  
 
				put(request.getSession(), TEAM_DETAILS,teamDetails);
								// processing variables Team58FindByID in lnk_view
				put(request.getSession(), SELECTED_TEAM,selectedTeam); 
			
			} catch (ApplicationException e) { 
				// error handling for operation team58FindByID called Team58FindByID
				errorLogger("An error occured during the execution of the operation : team58FindByID",e); 
		
			}
	}
					/**
	 * method callCopyBo58
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callCopyBo58(HttpServletRequest request,IForm form) {
		copyTeam = (Team58BO)get(request.getSession(),form, "copyTeam");  
										 
					teamDetails = (Team58BO)get(request.getSession(),form, "teamDetails");  
										 
					try {
				// executing CopyBo58 in lnk_view
				copyTeam = 	serviceCopyTeam58.copyBo58(
			teamDetails
			);  
 
				put(request.getSession(), COPY_TEAM,copyTeam);
								// processing variables CopyBo58 in lnk_view
				put(request.getSession(), TEAM_DETAILS,teamDetails); 
			
			} catch (ApplicationException e) { 
				// error handling for operation copyBo58 called CopyBo58
				errorLogger("An error occured during the execution of the operation : copyBo58",e); 
		
			}
	}
		
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("UC58_DisplayTeams58Controller [ ");
			strBToS.append(ALL_TEAMS);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(allTeams);
			strBToS.append(END_TO_STRING_DELIMITER);
						strBToS.append(TEAM_DETAILS);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(teamDetails);
			strBToS.append(END_TO_STRING_DELIMITER);
			strBToS.append(SELECTED_TEAM);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(selectedTeam);
			strBToS.append(END_TO_STRING_DELIMITER);
			strBToS.append(COPY_TEAM);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(copyTeam);
			strBToS.append(END_TO_STRING_DELIMITER);
		// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(uC58_DisplayTeams58Validator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsUC58_DisplayTeams58Controller!=null); 
		return strBToS.toString();
	}
}
