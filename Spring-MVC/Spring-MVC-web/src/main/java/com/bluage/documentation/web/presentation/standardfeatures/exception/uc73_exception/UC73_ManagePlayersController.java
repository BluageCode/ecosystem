/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.exception.uc73_exception ;

// Import declaration.
// Java imports.
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.bluage.documentation.business.standardfeatures.exception.uc73_exception.bos.Player73BO;
import com.bluage.documentation.business.standardfeatures.exception.uc73_exception.bos.Team73BO;
import com.bluage.documentation.business.standardfeatures.exception.uc73_exception.entities.daofinder.Team73DAOFinderImpl;
import com.bluage.documentation.service.standardfeatures.exception.uc73_exception.ServicePlayer73;
import com.netfective.bluage.core.exception.ApplicationException;
import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;
import com.netfective.bluage.core.web.IForm;

/**
 * Class : UC73_ManagePlayersController
 */
@Controller
@Scope("prototype")
@SessionAttributes("uC73_ManagePlayersForm")
@RequestMapping(value= "/presentation/standardfeatures/exception/uc73_exception")
public class UC73_ManagePlayersController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC73_ManagePlayersController.class);
	
	//Current form name
	private static final String U_C73__MANAGE_PLAYERS_FORM = "uC73_ManagePlayersForm";

		//CONSTANT: all_teams
	private static final String ALL_TEAMS = "all_teams";
							//CONSTANT: playersList
	private static final String PLAYERS_LIST = "playersList";
	//CONSTANT: playerToDelete
	private static final String PLAYER_TO_DELETE = "playerToDelete";
	//CONSTANT: selectedTeam
	private static final String SELECTED_TEAM = "selectedTeam";
	
	/**
	 * Property:customDateEditorsUC73_ManagePlayersController
	 */
	final private Map<String,CustomDateEditor> customDateEditorsUC73_ManagePlayersController = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:uC73_ManagePlayersValidator
	 */
	final private UC73_ManagePlayersValidator uC73_ManagePlayersValidator = new UC73_ManagePlayersValidator();
	
	/**
	 * Service declaration : servicePlayer73.
	 */
	@Autowired
	private ServicePlayer73 servicePlayer73;
	
	/**
	 * Generic Finder : team73DAOFinderImpl.
	 */
	@Autowired
	private Team73DAOFinderImpl team73DAOFinderImpl;
	
	
	// Initialise all the instances for UC73_ManagePlayersController
			// Initialize the instance all_teams for the state lnk_back
		private List all_teams; // Initialize the instance all_teams for UC73_ManagePlayersController
										// Declare the instance playersList
		private List playersList;
		// Declare the instance playerToDelete
		private Player73BO playerToDelete;
		// Declare the instance selectedTeam
		private Team73BO selectedTeam;
			/**
	 * Operation : lnk_delete
 	 * @param model : 
 	 * @param uC73_ManagePlayers : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC73_ManagePlayers/lnk_delete.html",method = RequestMethod.POST)
	public String lnk_delete(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC73_ManagePlayersForm") UC73_ManagePlayersForm  uC73_ManagePlayersForm, final BindingResult bindingResult ,final @RequestParam(ID_REQUEST_PARAM) Integer index){
		
		
		
		LOGGER.info("Begin method : lnk_delete"); 
		infoLogger(uC73_ManagePlayersForm); 
		uC73_ManagePlayersForm.setAlwaysCallPreControllers(false); 
		// initialization for lnk_delete
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state lnk_delete if the validation fail, you will be forward to the screen : "/presentation/standardfeatures/exception/uc73_exception/UC73_ManagePlayers"
		if(errorsMessages(request,response,uC73_ManagePlayersForm,bindingResult,"playerToDelete", uC73_ManagePlayersValidator, customDateEditorsUC73_ManagePlayersController)){ 
			return "/presentation/standardfeatures/exception/uc73_exception/UC73_ManagePlayers"; 
		}

														destinationPath =  callDeletePlayer(request,uC73_ManagePlayersForm , index, model);
					if(destinationPath != null){
						return destinationPath;
					}
						// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.exception.uc73_exception.UC73_ManagePlayersForm or not from lnk_delete
			// Populate the destination form with all the instances returned from lnk_delete.
			final IForm uC73_ManagePlayersForm2 = populateDestinationForm(request.getSession(), uC73_ManagePlayersForm); 
					
			// Add the form to the model.
			model.addAttribute("uC73_ManagePlayersForm", uC73_ManagePlayersForm2); 
			
			request.getSession().setAttribute("uC73_ManagePlayersForm", uC73_ManagePlayersForm2);
			
			// "OK" CASE => destination screen path from lnk_delete
			LOGGER.info("Go to the screen 'UC73_ManagePlayers'.");
			// Redirect (PRG) from lnk_delete
			destinationPath =  "redirect:/presentation/standardfeatures/exception/uc73_exception/UC73_ManagePlayers.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_delete 
	}
	
		/**
	 * Operation : select_team
 	 * @param model : 
 	 * @param uC73_ManagePlayers : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC73_ManagePlayers/select_team.html",method = RequestMethod.POST)
	public String select_team(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC73_ManagePlayersForm") UC73_ManagePlayersForm  uC73_ManagePlayersForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : select_team"); 
		infoLogger(uC73_ManagePlayersForm); 
		uC73_ManagePlayersForm.setAlwaysCallPreControllers(false); 
		// initialization for select_team
		init();
		
		String destinationPath = null; 
		

					 callFindPlayers(request,uC73_ManagePlayersForm );
						// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.exception.uc73_exception.UC73_ManagePlayersForm or not from select_team
			// Populate the destination form with all the instances returned from select_team.
			final IForm uC73_ManagePlayersForm2 = populateDestinationForm(request.getSession(), uC73_ManagePlayersForm); 
					
			// Add the form to the model.
			model.addAttribute("uC73_ManagePlayersForm", uC73_ManagePlayersForm2); 
			
			request.getSession().setAttribute("uC73_ManagePlayersForm", uC73_ManagePlayersForm2);
			
			// "OK" CASE => destination screen path from select_team
			LOGGER.info("Go to the screen 'UC73_ManagePlayers'.");
			// Redirect (PRG) from select_team
			destinationPath =  "redirect:/presentation/standardfeatures/exception/uc73_exception/UC73_ManagePlayers.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : select_team 
	}
	
	
	/**
	* This method initialise the form : UC73_ManagePlayersForm 
	* @return UC73_ManagePlayersForm
	*/
	@ModelAttribute("UC73_ManagePlayersFormInit")
	public void initUC73_ManagePlayersForm(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, U_C73__MANAGE_PLAYERS_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method UC73_ManagePlayersForm."); //for select_team 
		}
		UC73_ManagePlayersForm uC73_ManagePlayersForm;
	
		if(request.getSession().getAttribute(U_C73__MANAGE_PLAYERS_FORM) != null){
			uC73_ManagePlayersForm = (UC73_ManagePlayersForm)request.getSession().getAttribute(U_C73__MANAGE_PLAYERS_FORM);
		} else {
			uC73_ManagePlayersForm = new UC73_ManagePlayersForm();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form UC73_ManagePlayersForm.");
		}
		uC73_ManagePlayersForm = (UC73_ManagePlayersForm)populateDestinationForm(request.getSession(), uC73_ManagePlayersForm);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  UC73_ManagePlayersForm.");
		}
		model.addAttribute(U_C73__MANAGE_PLAYERS_FORM, uC73_ManagePlayersForm);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param UC73_ManagePlayersForm : The sceen form.
	 */
	@RequestMapping(value = "/UC73_ManagePlayers.html" ,method = RequestMethod.GET)
	public void prepareUC73_ManagePlayers(final HttpServletRequest request,final  Model model,final  @ModelAttribute("UC73_ManagePlayersFormInit") UC73_ManagePlayersForm uC73_ManagePlayersForm){
		
		UC73_ManagePlayersForm currentUC73_ManagePlayersForm = uC73_ManagePlayersForm;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(U_C73__MANAGE_PLAYERS_FORM) == null){
			if(currentUC73_ManagePlayersForm!=null){
				request.getSession().setAttribute(U_C73__MANAGE_PLAYERS_FORM, currentUC73_ManagePlayersForm);
			}else {
				currentUC73_ManagePlayersForm = new UC73_ManagePlayersForm();
				request.getSession().setAttribute(U_C73__MANAGE_PLAYERS_FORM, currentUC73_ManagePlayersForm);	
			}
		} else {
			currentUC73_ManagePlayersForm = (UC73_ManagePlayersForm) request.getSession().getAttribute(U_C73__MANAGE_PLAYERS_FORM);
		}

		try {
			List all_teams = team73DAOFinderImpl.findAll();
			put(request.getSession(), ALL_TEAMS, all_teams);
		} catch (ApplicationException e) {
			errorLogger("An error while calling the generic finder for the instance : all_teams.",e);
		}
				currentUC73_ManagePlayersForm = (UC73_ManagePlayersForm)populateDestinationForm(request.getSession(), currentUC73_ManagePlayersForm);
		// Call all the Precontroller.
	currentUC73_ManagePlayersForm.setAlwaysCallPreControllers(true);
		model.addAttribute(U_C73__MANAGE_PLAYERS_FORM, currentUC73_ManagePlayersForm);
		request.getSession().setAttribute(U_C73__MANAGE_PLAYERS_FORM, currentUC73_ManagePlayersForm);
		
	}
	
																/**
	 * method callDeletePlayer
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 * @param index Integer the current index 
	 * @param model Model the model
	 * @return String result NOK 
	 */
	private String callDeletePlayer(HttpServletRequest request,IForm form,Integer index, final Model model) {
		String str = null;
		playersList = (List)get(request.getSession(),form, "playersList");  
										 
					if(index!=null){  
			 playerToDelete = (Player73BO)((List)get(request.getSession(),form, "playersList")).get(index);  
		} else {
			 playerToDelete= null; 
		}
				 
					playersList = (List)get(request.getSession(),form, "playersList");  
										 
					try {
				// executing DeletePlayer in select_team
				playersList = 	servicePlayer73.deletePlayer(
			playerToDelete
		,			playersList
			);  
 
				put(request.getSession(), PLAYERS_LIST,playersList);
								// processing variables DeletePlayer in select_team
				put(request.getSession(), PLAYER_TO_DELETE,playerToDelete); 
							put(request.getSession(), PLAYERS_LIST,playersList); 
			
			} catch (ApplicationException e) { 
				// error handling for operation deletePlayer called DeletePlayer
				errorLogger("An error occured during the execution of the operation : deletePlayer",e); 
		
				// "NOK" DEFAULT CASE
				UC73_ExceptionForm uC73_ExceptionForm = new UC73_ExceptionForm();
				final IForm uC73_ExceptionForm2 = populateDestinationForm(request.getSession(), uC73_ExceptionForm);
				model.addAttribute("uC73_ExceptionForm", uC73_ExceptionForm2);
				request.getSession().setAttribute("uC73_ExceptionForm", uC73_ExceptionForm2);
				request.getSession().setAttribute("messages", e.getMessage());
				return "redirect:/presentation/standardfeatures/exception/uc73_exception/UC73_Exception.html";
						}
		return str;
	}
					/**
	 * method callFindPlayers
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callFindPlayers(HttpServletRequest request,IForm form) {
		playersList = (List)get(request.getSession(),form, "playersList");  
										 
					selectedTeam = (Team73BO)get(request.getSession(),form, "selectedTeam");  
										 
					try {
				// executing FindPlayers in select_team
				playersList = 	servicePlayer73.player73Find(
			selectedTeam
			);  
 
				put(request.getSession(), PLAYERS_LIST,playersList);
								// processing variables FindPlayers in select_team
				put(request.getSession(), SELECTED_TEAM,selectedTeam); 
			
			} catch (ApplicationException e) { 
				// error handling for operation player73Find called FindPlayers
				errorLogger("An error occured during the execution of the operation : player73Find",e); 
		
			}
	}
		
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("UC73_ManagePlayersController [ ");
				strBToS.append(ALL_TEAMS);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(all_teams);
			strBToS.append(END_TO_STRING_DELIMITER);
									strBToS.append(PLAYERS_LIST);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(playersList);
			strBToS.append(END_TO_STRING_DELIMITER);
			strBToS.append(PLAYER_TO_DELETE);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(playerToDelete);
			strBToS.append(END_TO_STRING_DELIMITER);
			strBToS.append(SELECTED_TEAM);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(selectedTeam);
			strBToS.append(END_TO_STRING_DELIMITER);
		// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(uC73_ManagePlayersValidator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsUC73_ManagePlayersController!=null); 
		return strBToS.toString();
	}
}
