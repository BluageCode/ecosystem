/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.service.common.home;


import java.util.Date;

import javax.inject.Named;

import com.bluage.documentation.service.AbstractService;
import com.netfective.bluage.core.exception.ApplicationException;

/**
 * Defines  application services for ServiceHome
  */
@Named("com.bluage.documentation.service.common.home.ServiceHome")
public class ServiceHome  extends AbstractService{


				


	/**
	 *  Default constructor 
	 */
	public ServiceHome() {
		super(); // Call super for default constructor : ServiceHome
	}
	



			
	/**    
	 * Process operation.
	 * @return {@link void}
	 */
	public void processInstances( )
 throws ApplicationException {

			privateProcessInstances();
	}
	

	/**    
	 * Process operation.
	 * @return {@link void}
	 */
	private void privateProcessInstances( )
 throws ApplicationException {
// Central buffer node declaration for operation processInstances
			Byte	 myByteB =  Byte.valueOf((byte)0);
					int	 myInt =  0;
					Short	 myShortS =  null;
					Long	 myLongL =  Long.valueOf(0);
					boolean	 myBoolean =  false;
					char	 myChar =  Character.MIN_VALUE;
					Date	 myDateD =  new Date();
					double	 myDouble =  0;
					long	 myLong =  0;
					Integer	 myInteger =  Integer.valueOf(0);
					Double	 myDoubleD =  Double.valueOf(0);
					Boolean	 myBooleanB =  Boolean.FALSE;
					float	 myFloat =  0;
					short	 myShort =  0;
					Character	 myCharacter =  Character.valueOf((char)0);
					Float	 myFloatF =  Float.valueOf(0);
					byte	 myByte =  0;
					/* Executing : */
myShort = 0;
/* Executing : */
myChar = 'a';

		}

		}
