ALTER TABLE stadium29 DROP CONSTRAINT stadium29_pkey;

ALTER TABLE state29 DROP CONSTRAINT state29_pkey;

ALTER TABLE player29 DROP CONSTRAINT player29_pkey;

ALTER TABLE team29 DROP CONSTRAINT team29_pkey;



DROP TABLE state29;

DROP TABLE stadium29;

DROP TABLE team29;

DROP TABLE player29;



CREATE TABLE player29 (
    id bigint NOT NULL,
    version integer NOT NULL,
    first_name character varying(1024),
    last_name character varying(1024),
    date_of_birth date,
    estimated_value real,
    weight integer,
    height integer,
    rookie boolean
);

INSERT INTO player29 VALUES (201, 0, 'Oliver', 'Frank', '1968-10-31', 0.11, 116, 228, false);
INSERT INTO player29 VALUES (202, 0, 'Elbert', 'Cole', '1965-04-07', 3.95, 100, 171, false);
INSERT INTO player29 VALUES (204, 0, 'Ricky', 'Dawson', '1990-03-03', 29.299999, 115, 168, true);
INSERT INTO player29 VALUES (203, 0, 'Abel', 'Dennis', '1980-04-16', 30.51, 105, 208, false);
INSERT INTO player29 VALUES (205, 0, 'Earl', 'Russell', '1981-12-15', 44.73, 60, 234, false);
INSERT INTO player29 VALUES (206, 0, 'Nick', 'Garner', '1992-07-27', 47.84, 87, 221, true);
INSERT INTO player29 VALUES (207, 0, 'Howard', 'Ramos', '1970-08-25', 10.76, 107, 199, false);
INSERT INTO player29 VALUES (208, 0, 'Allan', 'Meyer', '1969-02-14', 33.669998, 114, 229, false);
INSERT INTO player29 VALUES (209, 0, 'Ruben', 'Pope', '1985-05-01', 44.66, 86, 201, false);
INSERT INTO player29 VALUES (210, 0, 'Lester', 'Cross', '1976-02-21', 30.98, 69, 214, false);
INSERT INTO player29 VALUES (211, 0, 'Keith', 'Barnett', '1976-11-10', 5.8099999, 66, 190, false);
INSERT INTO player29 VALUES (213, 0, 'Tommy', 'Wood', '1992-01-20', 13.93, 77, 173, true);
INSERT INTO player29 VALUES (214, 0, 'Sylvester', 'Nash', '1965-07-10', 37.73, 68, 184, false);
INSERT INTO player29 VALUES (215, 0, 'Michael', 'Hodges', '1988-04-01', 25.35, 158, 204, false);
INSERT INTO player29 VALUES (216, 0, 'Jerome', 'Grant', '1982-06-29', 29.940001, 105, 205, false);
INSERT INTO player29 VALUES (217, 0, 'Woodrow', 'Carpenter', '1987-08-08', 41.759998, 69, 224, false);
INSERT INTO player29 VALUES (218, 0, 'Roderick', 'Robbins', '1975-07-29', 12.04, 115, 164, false);
INSERT INTO player29 VALUES (219, 0, 'Christian', 'Chavez', '1983-05-17', 37.880001, 97, 209, false);
INSERT INTO player29 VALUES (212, 0, 'Ivan', 'Tate', '1972-02-08', 48.490002, 143, 175, false);
INSERT INTO player29 VALUES (401, 0, 'Gregg', 'Nash', '1972-01-11', 6.8600001, 147, 215, false);
INSERT INTO player29 VALUES (402, 0, 'Floyd', 'Howell', '1979-08-25', 25.709999, 155, 192, false);
INSERT INTO player29 VALUES (403, 0, 'Simon', 'Klein', '1987-01-21', 22.24, 29, 203, false);
INSERT INTO player29 VALUES (404, 0, 'Blake', 'Montgomery', '1992-11-16', 37.470001, 113, 205, true);
INSERT INTO player29 VALUES (405, 0, 'Doyle', 'Burns', '1990-02-23', 39.130001, 153, 225, true);
INSERT INTO player29 VALUES (406, 0, 'Troy', 'Rowe', '1973-02-12', 26.82, 94, 211, false);
INSERT INTO player29 VALUES (407, 0, 'Jesus', 'Jimenez', '1985-09-01', 17.73, 133, 159, false);
INSERT INTO player29 VALUES (408, 0, 'Ross', 'Guerrero', '1992-01-26', 13.68, 136, 171, true);
INSERT INTO player29 VALUES (409, 0, 'Eddie', 'Boyd', '1967-10-05', 21.719999, 118, 232, false);
INSERT INTO player29 VALUES (410, 0, 'Luther', 'Peterson', '1985-06-09', 1.04, 83, 181, false);

ALTER TABLE ONLY player29
    ADD CONSTRAINT player29_pkey PRIMARY KEY (id);

CREATE TABLE team29 (
    id bigint NOT NULL,
    version integer NOT NULL,
    city character varying(1024),
    name character varying(1024)
);

INSERT INTO team29 VALUES (1, 0, 'Charlotte', 'Tigers');
INSERT INTO team29 VALUES (2, 0, 'Kansas City', 'Bears');
INSERT INTO team29 VALUES (3, 0, 'Detroit', 'Chargers');
INSERT INTO team29 VALUES (4, 0, 'Detroit', 'Hawks');
INSERT INTO team29 VALUES (5, 0, 'Omaha', 'Braves');
INSERT INTO team29 VALUES (6, 0, 'Raleigh', 'Eagles');
INSERT INTO team29 VALUES (7, 0, 'Oakland', 'Cardinals');

ALTER TABLE ONLY team29
    ADD CONSTRAINT team29_pkey PRIMARY KEY (id);

CREATE TABLE state29 (
    code character varying(1024) NOT NULL,
    version integer NOT NULL,
    name character varying(1024) NOT NULL
);

INSERT INTO state29 VALUES ('AK', 0, 'Alaska');
INSERT INTO state29 VALUES ('AL', 0, 'Alabama');
INSERT INTO state29 VALUES ('AR', 0, 'Arkansas');
INSERT INTO state29 VALUES ('AZ', 0, 'Arizona');
INSERT INTO state29 VALUES ('CA', 0, 'California');
INSERT INTO state29 VALUES ('CO', 0, 'Colorado');
INSERT INTO state29 VALUES ('CT', 0, 'Connecticut');
INSERT INTO state29 VALUES ('DE', 0, 'Delaware');
INSERT INTO state29 VALUES ('FL', 0, 'Florida');
INSERT INTO state29 VALUES ('GA', 0, 'Georgia');
INSERT INTO state29 VALUES ('HI', 0, 'Hawaii');
INSERT INTO state29 VALUES ('IA', 0, 'Iowa');

ALTER TABLE ONLY state29
    ADD CONSTRAINT state29_pkey PRIMARY KEY (code);
	
CREATE TABLE stadium29 (
    code character varying(1024) NOT NULL,
    version integer NOT NULL,
    name character varying(1024)
);

INSERT INTO stadium29 VALUES ('4', 0, 'Heinz Field');
INSERT INTO stadium29 VALUES ('5', 0, 'Jobing.com Arena');
INSERT INTO stadium29 VALUES ('6', 0, 'Monster Park');
INSERT INTO stadium29 VALUES ('7', 0, 'U.S. Cellular Field ');
INSERT INTO stadium29 VALUES ('8', 0, 'Dick s Sporting Goods Park');
INSERT INTO stadium29 VALUES ('1', 0, 'Hut Park ');
INSERT INTO stadium29 VALUES ('2', 0, 'Canad Inns Stadium');
INSERT INTO stadium29 VALUES ('3', 0, 'Gay Meadow');
INSERT INTO stadium29 VALUES ('9', 0, 'American Airlines Arena');
INSERT INTO stadium29 VALUES ('10', 0, 'New Louisville Basketball Arena');
INSERT INTO stadium29 VALUES ('11', 0, 'Sutherland');
INSERT INTO stadium29 VALUES ('12', 0, 'Spalding Arena');

ALTER TABLE ONLY stadium29
    ADD CONSTRAINT stadium29_pkey PRIMARY KEY (code);	