/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.business.standardfeatures.application.repeater.uc39_repeater.bos;

import javax.inject.Inject;
import javax.inject.Named;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import com.bluage.documentation.business.common.bos.TeamBO;
import com.bluage.documentation.business.standardfeatures.application.repeater.uc39_repeater.entities.daofinder.Team39DAOFinderImpl;

/**
 * Team39BO
 *
* @see com.netfective.bluage.core.business.IBusinessObject
* * @see TeamBO
 */
@XmlRootElement

public class Team39BO
 extends TeamBO {
	
	/**
	 * Default SUID for Team39BO.
	 */
	private static final long serialVersionUID = 1L;
	

	@Inject
	@Named ("com.bluage.documentation.business.standardfeatures.application.repeater.uc39_repeater.entities.daofinder.Team39Finder") 
	private Team39DAOFinderImpl finder;

						/**
	 * Entity property: flag
	 *
	 */
private Boolean flag = Boolean.FALSE;

					/**
	 * Entity property: pictoUrl
	 *
	 */
private byte[] pictoUrl;



   /**
	* public default constructor for class Team39BO
	*/
	public Team39BO(){
		//NOP Team39BO
	}


						/**
	 * Getter for flag of class Team39BO.
	 *
	 * @return {@link java.lang.Boolean}
	 * @generated
	 */ 
public  Boolean getFlag() {
	return this.flag;
	}


	/**
	 * Setter for flag of class Team39BO.
	 * @param value {@link java.lang.Boolean}
	 * @generated
	 */

public  void setFlag(final Boolean flag) {
	this.flag = flag;
}

					/**
	 * Getter for pictoUrl of class Team39BO.
	 *
	 * @return {@link byte[]}
	 * @generated
	 */ 
public  byte[] getPictoUrl() {
	return this.pictoUrl;
	}


	/**
	 * Setter for pictoUrl of class Team39BO.
	 * @param value {@link byte[]}
	 * @generated
	 */

public  void setPictoUrl(final byte[] pictoUrl) {
	this.pictoUrl = pictoUrl;
}


/**
	 * Returns <code>true</code> if the argument is an Team39 instance and all identifiers for this entity
     * equal the identifiers of the argument entity. The <code>equals</code> method of the parent entity
     * will also need to return <code>true</code>. Returns <code>false</code> otherwise.
     *
     * @see TeamBO#hashCode()
	 * @param object  object to compare with current instance      
     * @return boolean true if equals false if not
     */
    public boolean equals(final Object object )
    {
		if ( this == object )
        {
            return true;
        }
        if ( !( object instanceof Team39BO ) )
        {
            return false;
        }
        final Team39BO that = (Team39BO)object;
    
    	            if (        				        			this.getId() == null
        	        ) {
        	return false;
        }
		return new EqualsBuilder()
			.append(this.getId(), that.getId())
			 	.isEquals();
	}
	/**

	 * Returns a hash code based on this entity's identifiers and the hash code of the parent entity.
     *
     * @see TeamBO#hashCode()
     * @return int
     */    
    public int hashCode() {
        return new HashCodeBuilder()
             .append(getId())
				 	.appendSuper(super.hashCode())
            .toHashCode();
    }
}

