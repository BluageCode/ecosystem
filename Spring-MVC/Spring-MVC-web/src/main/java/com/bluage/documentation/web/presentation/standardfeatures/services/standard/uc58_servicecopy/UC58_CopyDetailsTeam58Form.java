/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.services.standard.uc58_servicecopy;


import java.io.Serializable;
import java.util.List;

import com.bluage.documentation.business.standardfeatures.services.standard.uc58_servicecopy.bos.Team58BO;
import com.netfective.bluage.core.web.AbstractForm;
import com.netfective.bluage.core.web.IForm;

/**
* Class : UC58_CopyDetailsTeam58Form
*/
public class UC58_CopyDetailsTeam58Form extends AbstractForm implements Serializable, IForm {
	
	/** Constant : serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	//CONSTANT : copyTeam
	private static final String COPY_TEAM = "copyTeam";
	//CONSTANT : allStates1
	private static final String ALL_STATES1 = "allStates1";
	//CONSTANT : teamDetails
	private static final String TEAM_DETAILS = "teamDetails";
	/**
	 * 	Property: copyTeam 
	 */
	private Team58BO copyTeam;
	/**
	 * 	Property: allStates1 
	 */
	private List allStates1;
	/**
	 * 	Property: teamDetails 
	 */
	private Team58BO teamDetails;
/**
	 * Default constructor : UC58_CopyDetailsTeam58Form
	 */
	public UC58_CopyDetailsTeam58Form() {
		super();
		// Initialize : copyTeam
		this.copyTeam = new Team58BO();
		// Initialize : allStates1
		this.allStates1 = new java.util.ArrayList();
		// Initialize : teamDetails
		this.teamDetails = new Team58BO();
			this.setAlwaysCallPreControllers(true);
	}
	
	/**
	 * 	Getter : copyTeam 
	 * 	@return : Return the copyTeam instance.
	 */
	public Team58BO getCopyTeam(){
		return copyTeam; // For UC58_CopyDetailsTeam58Form
	}
	
	/**
	 * 	Setter : copyTeam 
	 *  @param copyTeaminstance : The instance to set.
	 */
	public void setCopyTeam(final Team58BO copyTeaminstance){
		this.copyTeam = copyTeaminstance;// For UC58_CopyDetailsTeam58Form
	}
	/**
	 * 	Getter : allStates1 
	 * 	@return : Return the allStates1 instance.
	 */
	public List getAllStates1(){
		return allStates1; // For UC58_CopyDetailsTeam58Form
	}
	
	/**
	 * 	Setter : allStates1 
	 *  @param allStates1instance : The instance to set.
	 */
	public void setAllStates1(final List allStates1instance){
		this.allStates1 = allStates1instance;// For UC58_CopyDetailsTeam58Form
	}
	/**
	 * 	Getter : teamDetails 
	 * 	@return : Return the teamDetails instance.
	 */
	public Team58BO getTeamDetails(){
		return teamDetails; // For UC58_CopyDetailsTeam58Form
	}
	
	/**
	 * 	Setter : teamDetails 
	 *  @param teamDetailsinstance : The instance to set.
	 */
	public void setTeamDetails(final Team58BO teamDetailsinstance){
		this.teamDetails = teamDetailsinstance;// For UC58_CopyDetailsTeam58Form
	}
/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UC58_CopyDetailsTeam58Form [ "+
COPY_TEAM +" = " + copyTeam +ALL_STATES1 +" = " + allStates1 +TEAM_DETAILS +" = " + teamDetails + "]";
	}
	
	/**
	 * Method setInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public void setInstanceByInstanceName(final String instanceName, final Object instance) {
		// Set the instance CopyTeam.
		if(COPY_TEAM.equals(instanceName)){// For UC58_CopyDetailsTeam58Form
			this.setCopyTeam((Team58BO)instance); // For UC58_CopyDetailsTeam58Form
		}
				// Set the instance AllStates1.
		if(ALL_STATES1.equals(instanceName)){// For UC58_CopyDetailsTeam58Form
			this.setAllStates1((List)instance); // For UC58_CopyDetailsTeam58Form
		}
				// Set the instance TeamDetails.
		if(TEAM_DETAILS.equals(instanceName)){// For UC58_CopyDetailsTeam58Form
			this.setTeamDetails((Team58BO)instance); // For UC58_CopyDetailsTeam58Form
		}
			}
	
	/**
	 * Method getInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public Object getInstanceByInstanceName(final String instanceName){
		// Temporary object for tmpUC58_CopyDetailsTeam58Form.
		Object tmpUC58_CopyDetailsTeam58Form = null;
		
			
		// Get the instance CopyTeam for UC58_CopyDetailsTeam58Form.
		if(COPY_TEAM.equals(instanceName)){ // For UC58_CopyDetailsTeam58Form
			tmpUC58_CopyDetailsTeam58Form = this.getCopyTeam(); // For UC58_CopyDetailsTeam58Form
		}
			
		// Get the instance AllStates1 for UC58_CopyDetailsTeam58Form.
		if(ALL_STATES1.equals(instanceName)){ // For UC58_CopyDetailsTeam58Form
			tmpUC58_CopyDetailsTeam58Form = this.getAllStates1(); // For UC58_CopyDetailsTeam58Form
		}
			
		// Get the instance TeamDetails for UC58_CopyDetailsTeam58Form.
		if(TEAM_DETAILS.equals(instanceName)){ // For UC58_CopyDetailsTeam58Form
			tmpUC58_CopyDetailsTeam58Form = this.getTeamDetails(); // For UC58_CopyDetailsTeam58Form
		}
		return tmpUC58_CopyDetailsTeam58Form;// For UC58_CopyDetailsTeam58Form
	}
	
	}
