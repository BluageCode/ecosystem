/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.application.navigation.uc29_globalscope.package_one ;


import org.apache.log4j.Logger;
import org.springframework.validation.Errors;

import com.netfective.bluage.core.validator.AbstractValidator;
/**
* Class :UC29_TeamsValidator
*/
public class UC29_TeamsValidator extends AbstractValidator{
	
	// LOGGER for the class UC29_TeamsValidator
	private static final Logger LOGGER = Logger.getLogger( UC29_TeamsValidator.class);
	
	
	/**
	* Operation validate for UC29_TeamsForm
	* @param obj : the current form (UC29_TeamsForm)
	* @param errors : The spring errors to return for the form UC29_TeamsForm
	*/
	public void validate(final Object obj,final Errors errors){
		LOGGER.info("Beginning method : validate"); // UC29_TeamsValidator
		UC29_TeamsForm cUC29_TeamsForm = (UC29_TeamsForm)obj; // UC29_TeamsValidator
		LOGGER.info("Ending method : validate the form "+ cUC29_TeamsForm.getClass().getName()); // UC29_TeamsValidator
	}

	/**
	* Method to implements to use spring validators (UC29_TeamsForm)
	* @param aClass : Class for the form UC29_TeamsForm
	* @return boolean
	*/
	public boolean supports(final Class aClass){
		return (new UC29_TeamsForm()).getClass().equals(aClass); // UC29_TeamsValidator
	}
}
