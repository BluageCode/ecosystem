/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.business.common.bos;

import javax.inject.Inject;
import javax.inject.Named;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import com.bluage.documentation.business.AbstractBusinessObject;
import com.bluage.documentation.business.common.entities.daofinder.TeamDAOFinderImpl;

/**
 * TeamBO
 *
* @see com.netfective.bluage.core.business.IBusinessObject
 */
@XmlRootElement

public class TeamBO
 extends AbstractBusinessObject {
	
	/**
	 * Default SUID for TeamBO.
	 */
	private static final long serialVersionUID = 1L;
	

	@Inject
	@Named ("com.bluage.documentation.business.common.entities.daofinder.TeamFinder") 
	private TeamDAOFinderImpl finder;
	/**
	 * Hibernate optimistic lock for class TeamBO.
	 */
	private int version;

						/**
	 * Entity property: id
	 *
	 */
private Long id;

					/**
	 * Entity property: city
	 *
	 */
private String city;

					/**
	 * Entity property: name
	 *
	 */
private String name;



   /**
	* public default constructor for class TeamBO
	*/
	public TeamBO(){
		//NOP TeamBO
	}

	
	/**
	 * Getter for version for class TeamBO
	 * @return int
	 */
	public int getVersion() {
		return this.version;
	}
	
	/**
	 * Setter for version for class TeamBO
	 * @param int version
	 */
	public void setVersion(final int version) {
		this.version = version;
	}

						/**
	 * Getter for id of class TeamBO.
	 *
	 * @return {@link java.lang.Long}
	 * @generated
	 */ 
public  Long getId() {
	return this.id;
	}


	/**
	 * Setter for id of class TeamBO.
	 * @param value {@link java.lang.Long}
	 * @generated
	 */

public  void setId(final Long id) {
	this.isEmpty = (id == null);
	this.id = id;
}

					/**
	 * Getter for city of class TeamBO.
	 *
	 * @return {@link java.lang.String}
	 * @generated
	 */ 
public  String getCity() {
	return this.city;
	}


	/**
	 * Setter for city of class TeamBO.
	 * @param value {@link java.lang.String}
	 * @generated
	 */

public  void setCity(final String city) {
	this.city = city;
}

					/**
	 * Getter for name of class TeamBO.
	 *
	 * @return {@link java.lang.String}
	 * @generated
	 */ 
public  String getName() {
	return this.name;
	}


	/**
	 * Setter for name of class TeamBO.
	 * @param value {@link java.lang.String}
	 * @generated
	 */

public  void setName(final String name) {
	this.name = name;
}


/**
	 * Returns <code>true</code> if the argument is an Team instance and all identifiers for this entity
     * equal the identifiers of the argument entity. Returns <code>false</code> otherwise.
	 * @param object  object to compare with current instance      
     * @return boolean true if equals false if not
     */
    public boolean equals(final Object object )
    {
		if ( this == object )
        {
            return true;
        }
        if ( !( object instanceof TeamBO ) )
        {
            return false;
        }
        final TeamBO that = (TeamBO)object;
    
    	            if (        				        			this.getId() == null
        	        ) {
        	return false;
        }
		return new EqualsBuilder()
			.append(this.getId(), that.getId())
			 	.appendSuper(super.equals(object))
		 	.isEquals();
	}
	/**
     * Returns a hash code based on this entity's identifiers.     * @return int
     */    
    public int hashCode() {
        return new HashCodeBuilder()
             .append(getId())
		            .toHashCode();
    }
}

