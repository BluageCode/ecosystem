/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.common.components ;


import org.apache.log4j.Logger;
import org.springframework.validation.Errors;

import com.netfective.bluage.core.validator.AbstractValidator;
/**
* Class :cmp_headerValidator
*/
public class cmp_headerValidator extends AbstractValidator{
	
	// LOGGER for the class cmp_headerValidator
	private static final Logger LOGGER = Logger.getLogger( cmp_headerValidator.class);
	
	
	/**
	* Operation validate for cmp_headerForm
	* @param obj : the current form (cmp_headerForm)
	* @param errors : The spring errors to return for the form cmp_headerForm
	*/
	public void validate(final Object obj,final Errors errors){
		LOGGER.info("Beginning method : validate"); // cmp_headerValidator
		cmp_headerForm ccmp_headerForm = (cmp_headerForm)obj; // cmp_headerValidator
		LOGGER.info("Ending method : validate the form "+ ccmp_headerForm.getClass().getName()); // cmp_headerValidator
	}

	/**
	* Method to implements to use spring validators (cmp_headerForm)
	* @param aClass : Class for the form cmp_headerForm
	* @return boolean
	*/
	public boolean supports(final Class aClass){
		return (new cmp_headerForm()).getClass().equals(aClass); // cmp_headerValidator
	}
}
