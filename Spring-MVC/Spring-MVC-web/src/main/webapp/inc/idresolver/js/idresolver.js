
  function checkAttributes(hash,el){
      if( el === undefined || el === null )
        throw("Second argument to checkAttributes should be a DOM node or the ID of a DOM Node : " + el);
  
      if( el.constructor === String )
        el = document.getElementById(el);

      if( el === null || !el.nodeType ) // Make sure el is a Node
        throw("Second argument to checkAttributes should be a DOM node or the ID of a DOM Node : " + el);

      if(! (hash instanceof Object))
        throw("First argument to checkAttributes should be an Object of attribute/test pairs. See the documentation for more information.");

      for(key in hash){  
        var pointer = el;
        var last    = null;        
        var pieces  = key.split('.');        
        for(var i=0; i<pieces.length; i++){
          if(!pointer[pieces[i]]) return false;
          last    = pointer;
          pointer = pointer[pieces[i]];
        }

        if( pointer instanceof Function )
          try {
            pointer = pointer.apply(last);
          }catch(error){
            throw("First agrument to checkAttributes included a Function Refrence which caused an ERROR: " +  error);
          }
    
        if( hash[key] instanceof RegExp ){
          if( !hash[key].test( pointer ) )
             return false;
        }else if( hash[key] instanceof Function ){
          if( !hash[key]( pointer ) )
            return false;
        }else if( hash[key] != pointer ){
          return false;
        }    
        
      }

      return true;
  }

  function getElementsByAttributes( searchAttributes, startAt, resultsLimit, depthLimit ) {   
     if(depthLimit !== undefined && depthLimit <= 0) return [];
      
     if(startAt === undefined){
       startAt = document;
      
     }else if(typeof startAt == 'string'){
       startAt = document.getElementById(startAt);
     }
 
     var results = checkAttributes(searchAttributes, startAt) ? [ startAt ] : [];
   
     if(resultsLimit == 1 && results.length > 0) return results;

     if (startAt.childNodes)
       for( var i = 0; i < startAt.childNodes.length; i++){
         results = results.concat( 
            getElementsByAttributes( searchAttributes, startAt.childNodes[i], (resultsLimit) ? resultsLimit - results.length : undefined, (depthLimit) ? depthLimit -1 : undefined )
         )
         if (resultsLimit !== undefined && results.length >= resultsLimit) break;
       }
      
     return results;
  }
  
  function getElementsByIdMatches( searchIdMatches, startAt, resultsLimit, depthLimit ) {
	var searchAttributes = {'id':(new RegExp(searchIdMatches,'i'))};	 
	var results = getElementsByAttributes( searchAttributes, startAt, resultsLimit, depthLimit );
    return results;
  }
  
  function $(elementId, startAt){  
  	startAt = startAt + ":IncludeTag";
	var searchIdMatches = '^' + startAt + '[\\w:]*' + elementId + '$';
	var results = getElementsByIdMatches( searchIdMatches, startAt, 1, undefined);	
    return (results.length > 0) ? results[0].id : null;
  }