/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.application.navigation.uc29_globalscope.package_two;


import java.io.Serializable;
import java.util.List;

import com.bluage.documentation.business.standardfeatures.application.navigation.uc29_globalscope.bos.Stadium29BO;
import com.netfective.bluage.core.web.AbstractForm;
import com.netfective.bluage.core.web.IForm;

/**
* Class : UC29_StadiumsForm
*/
public class UC29_StadiumsForm extends AbstractForm implements Serializable, IForm {
	
	/** Constant : serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	//CONSTANT : allStadiums
	private static final String ALL_STADIUMS = "allStadiums";
	/**
	 * 	Property: allStadiums 
	 */
	private List<Stadium29BO> allStadiums;
/**
	 * Default constructor : UC29_StadiumsForm
	 */
	public UC29_StadiumsForm() {
		super();
		// Initialize : allStadiums
		this.allStadiums = new java.util.ArrayList<com.bluage.documentation.business.standardfeatures.application.navigation.uc29_globalscope.bos.Stadium29BO>();
					this.setAlwaysCallPreControllers(true);
	}
	
	/**
	 * 	Getter : allStadiums 
	 * 	@return : Return the allStadiums instance.
	 */
	public List<Stadium29BO> getAllStadiums(){
		return allStadiums; // For UC29_StadiumsForm
	}
	
	/**
	 * 	Setter : allStadiums 
	 *  @param allStadiumsinstance : The instance to set.
	 */
	public void setAllStadiums(final List<Stadium29BO> allStadiumsinstance){
		this.allStadiums = allStadiumsinstance;// For UC29_StadiumsForm
	}
/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UC29_StadiumsForm [ "+
ALL_STADIUMS +" = " + allStadiums + "]";
	}
	
	/**
	 * Method setInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public void setInstanceByInstanceName(final String instanceName, final Object instance) {
		// Set the instance AllStadiums.
		if(ALL_STADIUMS.equals(instanceName)){// For UC29_StadiumsForm
			this.setAllStadiums((List<Stadium29BO>)instance); // For UC29_StadiumsForm
		}
			}
	
	/**
	 * Method getInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public Object getInstanceByInstanceName(final String instanceName){
		// Temporary object for tmpUC29_StadiumsForm.
		Object tmpUC29_StadiumsForm = null;
		
			
		// Get the instance AllStadiums for UC29_StadiumsForm.
		if(ALL_STADIUMS.equals(instanceName)){ // For UC29_StadiumsForm
			tmpUC29_StadiumsForm = this.getAllStadiums(); // For UC29_StadiumsForm
		}
		return tmpUC29_StadiumsForm;// For UC29_StadiumsForm
	}
	
			}
