/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.application.component.uc46_menu ;

// Import declaration.
// Java imports.
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;

/**
 * Class : UC46_Page1Controller
 */
@Controller
@Scope("prototype")
@SessionAttributes("uC46_Page1Form")
@RequestMapping(value= "/presentation/standardfeatures/application/component/uc46_menu")
public class UC46_Page1Controller extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC46_Page1Controller.class);
	
	//Current form name
	private static final String U_C46__PAGE1_FORM = "uC46_Page1Form";

	
	/**
	 * Property:customDateEditorsUC46_Page1Controller
	 */
	final private Map<String,CustomDateEditor> customDateEditorsUC46_Page1Controller = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:uC46_Page1Validator
	 */
	final private UC46_Page1Validator uC46_Page1Validator = new UC46_Page1Validator();
	
	
	// Initialise all the instances for UC46_Page1Controller

	/**
	* This method initialise the form : UC46_Page1Form 
	* @return UC46_Page1Form
	*/
	@ModelAttribute("UC46_Page1FormInit")
	public void initUC46_Page1Form(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, U_C46__PAGE1_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method UC46_Page1Form."); //for findPositions 
		}
		UC46_Page1Form uC46_Page1Form;
	
		if(request.getSession().getAttribute(U_C46__PAGE1_FORM) != null){
			uC46_Page1Form = (UC46_Page1Form)request.getSession().getAttribute(U_C46__PAGE1_FORM);
		} else {
			uC46_Page1Form = new UC46_Page1Form();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form UC46_Page1Form.");
		}
		uC46_Page1Form = (UC46_Page1Form)populateDestinationForm(request.getSession(), uC46_Page1Form);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  UC46_Page1Form.");
		}
		model.addAttribute(U_C46__PAGE1_FORM, uC46_Page1Form);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param UC46_Page1Form : The sceen form.
	 */
	@RequestMapping(value = "/UC46_Page1.html" ,method = RequestMethod.GET)
	public void prepareUC46_Page1(final HttpServletRequest request,final  Model model,final  @ModelAttribute("UC46_Page1FormInit") UC46_Page1Form uC46_Page1Form){
		
		UC46_Page1Form currentUC46_Page1Form = uC46_Page1Form;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(U_C46__PAGE1_FORM) == null){
			if(currentUC46_Page1Form!=null){
				request.getSession().setAttribute(U_C46__PAGE1_FORM, currentUC46_Page1Form);
			}else {
				currentUC46_Page1Form = new UC46_Page1Form();
				request.getSession().setAttribute(U_C46__PAGE1_FORM, currentUC46_Page1Form);	
			}
		} else {
			currentUC46_Page1Form = (UC46_Page1Form) request.getSession().getAttribute(U_C46__PAGE1_FORM);
		}

		currentUC46_Page1Form = (UC46_Page1Form)populateDestinationForm(request.getSession(), currentUC46_Page1Form);
		// Call all the Precontroller.
	currentUC46_Page1Form.setAlwaysCallPreControllers(true);
		model.addAttribute(U_C46__PAGE1_FORM, currentUC46_Page1Form);
		request.getSession().setAttribute(U_C46__PAGE1_FORM, currentUC46_Page1Form);
		
	}
	
	
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("UC46_Page1Controller [ ");
		// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(uC46_Page1Validator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsUC46_Page1Controller!=null); 
		return strBToS.toString();
	}
}
