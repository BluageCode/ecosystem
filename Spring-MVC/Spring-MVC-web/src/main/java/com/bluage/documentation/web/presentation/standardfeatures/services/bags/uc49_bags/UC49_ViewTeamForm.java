/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.services.bags.uc49_bags;


import java.io.Serializable;

import com.bluage.documentation.business.standardfeatures.services.bags.uc49_bags.bos.Team49BO;
import com.netfective.bluage.core.web.AbstractForm;
import com.netfective.bluage.core.web.IForm;

/**
* Class : UC49_ViewTeamForm
*/
public class UC49_ViewTeamForm extends AbstractForm implements Serializable, IForm {
	
	/** Constant : serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	//CONSTANT : teamToDisplay
	private static final String TEAM_TO_DISPLAY = "teamToDisplay";
	/**
	 * 	Property: teamToDisplay 
	 */
	private Team49BO teamToDisplay;
/**
	 * Default constructor : UC49_ViewTeamForm
	 */
	public UC49_ViewTeamForm() {
		super();
		// Initialize : teamToDisplay
		this.teamToDisplay = new Team49BO();
			this.setAlwaysCallPreControllers(true);
	}
	
	/**
	 * 	Getter : teamToDisplay 
	 * 	@return : Return the teamToDisplay instance.
	 */
	public Team49BO getTeamToDisplay(){
		return teamToDisplay; // For UC49_ViewTeamForm
	}
	
	/**
	 * 	Setter : teamToDisplay 
	 *  @param teamToDisplayinstance : The instance to set.
	 */
	public void setTeamToDisplay(final Team49BO teamToDisplayinstance){
		this.teamToDisplay = teamToDisplayinstance;// For UC49_ViewTeamForm
	}
/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UC49_ViewTeamForm [ "+
TEAM_TO_DISPLAY +" = " + teamToDisplay + "]";
	}
	
	/**
	 * Method setInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public void setInstanceByInstanceName(final String instanceName, final Object instance) {
		// Set the instance TeamToDisplay.
		if(TEAM_TO_DISPLAY.equals(instanceName)){// For UC49_ViewTeamForm
			this.setTeamToDisplay((Team49BO)instance); // For UC49_ViewTeamForm
		}
			}
	
	/**
	 * Method getInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public Object getInstanceByInstanceName(final String instanceName){
		// Temporary object for tmpUC49_ViewTeamForm.
		Object tmpUC49_ViewTeamForm = null;
		
			
		// Get the instance TeamToDisplay for UC49_ViewTeamForm.
		if(TEAM_TO_DISPLAY.equals(instanceName)){ // For UC49_ViewTeamForm
			tmpUC49_ViewTeamForm = this.getTeamToDisplay(); // For UC49_ViewTeamForm
		}
		return tmpUC49_ViewTeamForm;// For UC49_ViewTeamForm
	}
	
	}
