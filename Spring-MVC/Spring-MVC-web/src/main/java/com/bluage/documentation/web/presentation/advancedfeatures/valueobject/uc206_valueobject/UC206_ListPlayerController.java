/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.advancedfeatures.valueobject.uc206_valueobject ;

// Import declaration.
// Java imports.
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.bluage.documentation.business.advancedfeatures.valueobject.uc206_valueobject.vo.Player206VO;
import com.bluage.documentation.business.advancedfeatures.valueobject.uc206_valueobject.vo.Screen206VO;
import com.bluage.documentation.service.advancedfeatures.valueobject.uc206_valueobject.ServiceScreen206;
import com.netfective.bluage.core.exception.ApplicationException;
import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;
import com.netfective.bluage.core.web.IForm;

/**
 * Class : UC206_ListPlayerController
 */
@Controller
@Scope("prototype")
@SessionAttributes("uC206_ListPlayerForm")
@RequestMapping(value= "/presentation/advancedfeatures/valueobject/uc206_valueobject")
public class UC206_ListPlayerController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC206_ListPlayerController.class);
	
	//Current form name
	private static final String U_C206__LIST_PLAYER_FORM = "uC206_ListPlayerForm";

				//CONSTANT: selectedPlayer
	private static final String SELECTED_PLAYER = "selectedPlayer";
	//CONSTANT: screen206
	private static final String SCREEN206 = "screen206";
	//CONSTANT: playerVOs
	private static final String PLAYER_V_OS = "playerVOs";
	
	/**
	 * Property:customDateEditorsUC206_ListPlayerController
	 */
	final private Map<String,CustomDateEditor> customDateEditorsUC206_ListPlayerController = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:uC206_ListPlayerValidator
	 */
	final private UC206_ListPlayerValidator uC206_ListPlayerValidator = new UC206_ListPlayerValidator();
	
	/**
	 * Service declaration : serviceScreen206.
	 */
	@Autowired
	private ServiceScreen206 serviceScreen206;
	
	
	// Initialise all the instances for UC206_ListPlayerController
						// Declare the instance selectedPlayer
		private Player206VO selectedPlayer;
		// Declare the instance screen206
		private Screen206VO screen206;
		// Declare the instance playerVOs
		private List playerVOs;
			/**
	 * Operation : lnk_update
 	 * @param model : 
 	 * @param uC206_ListPlayer : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC206_ListPlayer/lnk_update.html",method = RequestMethod.POST)
	public String lnk_update(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC206_ListPlayerForm") UC206_ListPlayerForm  uC206_ListPlayerForm, final BindingResult bindingResult ,final @RequestParam(ID_REQUEST_PARAM) Integer index){
		
		
		
		LOGGER.info("Begin method : lnk_update"); 
		infoLogger(uC206_ListPlayerForm); 
		uC206_ListPlayerForm.setAlwaysCallPreControllers(false); 
		// initialization for lnk_update
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state lnk_update if the validation fail, you will be forward to the screen : "/presentation/advancedfeatures/valueobject/uc206_valueobject/UC206_ListPlayer"
		if(errorsMessages(request,response,uC206_ListPlayerForm,bindingResult,"selectedPlayer", uC206_ListPlayerValidator, customDateEditorsUC206_ListPlayerController)){ 
			return "/presentation/advancedfeatures/valueobject/uc206_valueobject/UC206_ListPlayer"; 
		}
		// Updating selected row for the state lnk_update 
		uC206_ListPlayerForm.setSelectedRow(-1); //Set selected row for lnk_update 
		// Updating selected table for the state lnk_update 
		uC206_ListPlayerForm.setSelectedTab(null); //reset selected row for lnk_update 

										 callExecuteUpdate(request,uC206_ListPlayerForm , index);
			
 callInitializeListPlayer(request,uC206_ListPlayerForm );
			
				 callExecuteListPlayer(request,uC206_ListPlayerForm );
						// Init the destination form com.bluage.documentation.web.presentation.advancedfeatures.valueobject.uc206_valueobject.UC206_ListPlayerForm or not from lnk_update
			// Populate the destination form with all the instances returned from lnk_update.
			final IForm uC206_ListPlayerForm2 = populateDestinationForm(request.getSession(), uC206_ListPlayerForm); 
					
			// Add the form to the model.
			model.addAttribute("uC206_ListPlayerForm", uC206_ListPlayerForm2); 
			
			request.getSession().setAttribute("uC206_ListPlayerForm", uC206_ListPlayerForm2);
			
			// "OK" CASE => destination screen path from lnk_update
			LOGGER.info("Go to the screen 'UC206_ListPlayer'.");
			// Redirect (PRG) from lnk_update
			destinationPath =  "redirect:/presentation/advancedfeatures/valueobject/uc206_valueobject/UC206_ListPlayer.html";
						
						
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_update 
	}
	
									/**
	 * Operation : lnk_details
 	 * @param model : 
 	 * @param uC206_ListPlayer : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC206_ListPlayer/lnk_details.html",method = RequestMethod.POST)
	public String lnk_details(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC206_ListPlayerForm") UC206_ListPlayerForm  uC206_ListPlayerForm, final BindingResult bindingResult ,final @RequestParam(ID_REQUEST_PARAM) Integer index){
		
		
		
		LOGGER.info("Begin method : lnk_details"); 
		infoLogger(uC206_ListPlayerForm); 
		uC206_ListPlayerForm.setAlwaysCallPreControllers(false); 
		// initialization for lnk_details
		init();
		
		String destinationPath = null; 
		

										 callExecuteDetails(request,uC206_ListPlayerForm , index);
											// Init the destination form com.bluage.documentation.web.presentation.advancedfeatures.valueobject.uc206_valueobject.UC206_DetailsPlayerForm or not from lnk_details
			final UC206_DetailsPlayerForm uC206_DetailsPlayerForm =  new UC206_DetailsPlayerForm();
			// Populate the destination form with all the instances returned from lnk_details.
			final IForm uC206_DetailsPlayerForm2 = populateDestinationForm(request.getSession(), uC206_DetailsPlayerForm); 
					
			// Add the form to the model.
			model.addAttribute("uC206_DetailsPlayerForm", uC206_DetailsPlayerForm2); 
			
			request.getSession().setAttribute("uC206_DetailsPlayerForm", uC206_DetailsPlayerForm2);
			
			// "OK" CASE => destination screen path from lnk_details
			LOGGER.info("Go to the screen 'UC206_DetailsPlayer'.");
			// Redirect (PRG) from lnk_details
			destinationPath =  "redirect:/presentation/advancedfeatures/valueobject/uc206_valueobject/UC206_DetailsPlayer.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_details 
	}
	
				/**
	 * Operation : lnk_edit
 	 * @param model : The model
 	 * @param uC206_ListPlayer : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC206_ListPlayer/lnk_edit.html")
	public String lnk_edit(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC206_ListPlayerForm") UC206_ListPlayerForm  uC206_ListPlayerForm, BindingResult bindingResult, final @RequestParam(ID_REQUEST_PARAM) Integer index){
		uC206_ListPlayerForm.setSelectedRow(index);	
		String tableId = request.getParameter(TAB_REQUEST_PARAM);
		uC206_ListPlayerForm.setSelectedTab(tableId);
		
		Player206VO selectedPlayer = (Player206VO) uC206_ListPlayerForm.getPlayerVOs().get(index);
		uC206_ListPlayerForm.setSelectedPlayer(selectedPlayer);
		return "/presentation/advancedfeatures/valueobject/uc206_valueobject/UC206_ListPlayer";
	}

	/**
	* This method initialise the form : UC206_ListPlayerForm 
	* @return UC206_ListPlayerForm
	*/
	@ModelAttribute("UC206_ListPlayerFormInit")
	public void initUC206_ListPlayerForm(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, U_C206__LIST_PLAYER_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method UC206_ListPlayerForm."); //for lnk_edit 
		}
		UC206_ListPlayerForm uC206_ListPlayerForm;
	
		if(request.getSession().getAttribute(U_C206__LIST_PLAYER_FORM) != null){
			uC206_ListPlayerForm = (UC206_ListPlayerForm)request.getSession().getAttribute(U_C206__LIST_PLAYER_FORM);
		} else {
			uC206_ListPlayerForm = new UC206_ListPlayerForm();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form UC206_ListPlayerForm.");
		}
		uC206_ListPlayerForm = (UC206_ListPlayerForm)populateDestinationForm(request.getSession(), uC206_ListPlayerForm);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  UC206_ListPlayerForm.");
		}
		model.addAttribute(U_C206__LIST_PLAYER_FORM, uC206_ListPlayerForm);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param UC206_ListPlayerForm : The sceen form.
	 */
	@RequestMapping(value = "/UC206_ListPlayer.html" ,method = RequestMethod.GET)
	public void prepareUC206_ListPlayer(final HttpServletRequest request,final  Model model,final  @ModelAttribute("UC206_ListPlayerFormInit") UC206_ListPlayerForm uC206_ListPlayerForm){
		
		UC206_ListPlayerForm currentUC206_ListPlayerForm = uC206_ListPlayerForm;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(U_C206__LIST_PLAYER_FORM) == null){
			if(currentUC206_ListPlayerForm!=null){
				request.getSession().setAttribute(U_C206__LIST_PLAYER_FORM, currentUC206_ListPlayerForm);
			}else {
				currentUC206_ListPlayerForm = new UC206_ListPlayerForm();
				request.getSession().setAttribute(U_C206__LIST_PLAYER_FORM, currentUC206_ListPlayerForm);	
			}
		} else {
			currentUC206_ListPlayerForm = (UC206_ListPlayerForm) request.getSession().getAttribute(U_C206__LIST_PLAYER_FORM);
		}

				currentUC206_ListPlayerForm = (UC206_ListPlayerForm)populateDestinationForm(request.getSession(), currentUC206_ListPlayerForm);
		// Call all the Precontroller.
	currentUC206_ListPlayerForm.setAlwaysCallPreControllers(true);
		model.addAttribute(U_C206__LIST_PLAYER_FORM, currentUC206_ListPlayerForm);
		request.getSession().setAttribute(U_C206__LIST_PLAYER_FORM, currentUC206_ListPlayerForm);
		
	}
	
										/**
	 * method callExecuteUpdate
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 * @param index Integer the current index 
	 */
	private void callExecuteUpdate(HttpServletRequest request,IForm form,Integer index) {
					if(index!=null){  
			 selectedPlayer = (Player206VO)((List)get(request.getSession(),form, "playerVOs")).get(index);  
		} else {
			 selectedPlayer= null; 
		}
				 
					try {
				// executing ExecuteUpdate in lnk_edit
	serviceScreen206.executeUpdate(
			selectedPlayer
			);  

								// processing variables ExecuteUpdate in lnk_edit
				put(request.getSession(), SELECTED_PLAYER,selectedPlayer); 
			
			} catch (ApplicationException e) { 
				// error handling for operation executeUpdate called ExecuteUpdate
				errorLogger("An error occured during the execution of the operation : executeUpdate",e); 
		
			}
	}
		/**
	 * method callInitializeListPlayer
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callInitializeListPlayer(HttpServletRequest request,IForm form) {
		screen206 = (Screen206VO)get(request.getSession(),form, "screen206");  
										 
					try {
				// executing InitializeListPlayer in lnk_edit
				screen206 = 	serviceScreen206.initializeScreen206(
	);  
 
				put(request.getSession(), SCREEN206,screen206);
								// processing variables InitializeListPlayer in lnk_edit

			} catch (ApplicationException e) { 
				// error handling for operation initializeScreen206 called InitializeListPlayer
				errorLogger("An error occured during the execution of the operation : initializeScreen206",e); 
		
			}
	}
					/**
	 * method callExecuteListPlayer
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callExecuteListPlayer(HttpServletRequest request,IForm form) {
		playerVOs = (List)get(request.getSession(),form, "playerVOs");  
										 
					screen206 = (Screen206VO)get(request.getSession(),form, "screen206");  
										 
					try {
				// executing ExecuteListPlayer in lnk_edit
				playerVOs = 	serviceScreen206.executeListPlayer(
			screen206
			);  
 
				put(request.getSession(), PLAYER_V_OS,playerVOs);
								// processing variables ExecuteListPlayer in lnk_edit
				put(request.getSession(), SCREEN206,screen206); 
			
			} catch (ApplicationException e) { 
				// error handling for operation executeListPlayer called ExecuteListPlayer
				errorLogger("An error occured during the execution of the operation : executeListPlayer",e); 
		
			}
	}
									/**
	 * method callExecuteDetails
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 * @param index Integer the current index 
	 */
	private void callExecuteDetails(HttpServletRequest request,IForm form,Integer index) {
		if(index!=null){  
			 selectedPlayer = (Player206VO)((List)get(request.getSession(),form, "playerVOs")).get(index);  
		} else {
			 selectedPlayer= null; 
		}
				 
					if(index!=null){  
			 selectedPlayer = (Player206VO)((List)get(request.getSession(),form, "playerVOs")).get(index);  
		} else {
			 selectedPlayer= null; 
		}
				 
					try {
				// executing ExecuteDetails in lnk_edit
				selectedPlayer = 	serviceScreen206.executeDetails(
			selectedPlayer
			);  
 
				put(request.getSession(), SELECTED_PLAYER,selectedPlayer);
								// processing variables ExecuteDetails in lnk_edit
				put(request.getSession(), SELECTED_PLAYER,selectedPlayer); 
			
			} catch (ApplicationException e) { 
				// error handling for operation executeDetails called ExecuteDetails
				errorLogger("An error occured during the execution of the operation : executeDetails",e); 
		
			}
	}
		
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("UC206_ListPlayerController [ ");
						strBToS.append(SELECTED_PLAYER);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(selectedPlayer);
			strBToS.append(END_TO_STRING_DELIMITER);
			strBToS.append(SCREEN206);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(screen206);
			strBToS.append(END_TO_STRING_DELIMITER);
			strBToS.append(PLAYER_V_OS);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(playerVOs);
			strBToS.append(END_TO_STRING_DELIMITER);
		// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(uC206_ListPlayerValidator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsUC206_ListPlayerController!=null); 
		return strBToS.toString();
	}
}
