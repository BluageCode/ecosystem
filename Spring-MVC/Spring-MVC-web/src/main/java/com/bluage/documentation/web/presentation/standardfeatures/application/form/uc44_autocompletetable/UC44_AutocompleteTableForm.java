/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.application.form.uc44_autocompletetable;


import java.io.Serializable;
import java.util.List;

import com.bluage.documentation.business.standardfeatures.application.form.uc44_autocompletetable.bos.Player44BO;
import com.bluage.documentation.business.standardfeatures.application.form.uc44_autocompletetable.bos.Team44BO;
import com.netfective.bluage.core.web.AbstractForm;
import com.netfective.bluage.core.web.IForm;

/**
* Class : UC44_AutocompleteTableForm
*/
public class UC44_AutocompleteTableForm extends AbstractForm implements Serializable, IForm {
	
	/** Constant : serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	//CONSTANT : instance_teams
	private static final String INSTANCE_TEAMS = "instance_teams";
	//CONSTANT : allPlayers
	private static final String ALL_PLAYERS = "allPlayers";
	//CONSTANT : selectedTeam
	private static final String SELECTED_TEAM = "selectedTeam";
	//CONSTANT : selectedPlayer
	private static final String SELECTED_PLAYER = "selectedPlayer";
	/**
	 * 	Property: instance_teams 
	 */
	private List instance_teams;
	/**
	 * 	Property: allPlayers 
	 */
	private List<Player44BO> allPlayers;
	/**
	 * 	Property: selectedTeam 
	 */
	private Team44BO selectedTeam;
	/**
	 * 	Property: selectedPlayer 
	 */
	private Player44BO selectedPlayer;
/**
	 * Default constructor : UC44_AutocompleteTableForm
	 */
	public UC44_AutocompleteTableForm() {
		super();
		// Initialize : instance_teams
		this.instance_teams = new java.util.ArrayList();
		// Initialize : allPlayers
		this.allPlayers = new java.util.ArrayList<com.bluage.documentation.business.standardfeatures.application.form.uc44_autocompletetable.bos.Player44BO>();
		// Initialize : selectedTeam
		this.selectedTeam = new Team44BO();
		// Initialize : selectedPlayer
		this.selectedPlayer = null;
					this.setAlwaysCallPreControllers(true);
	}
	
	/**
	 * 	Getter : instance_teams 
	 * 	@return : Return the instance_teams instance.
	 */
	public List getInstance_teams(){
		return instance_teams; // For UC44_AutocompleteTableForm
	}
	
	/**
	 * 	Setter : instance_teams 
	 *  @param instance_teamsinstance : The instance to set.
	 */
	public void setInstance_teams(final List instance_teamsinstance){
		this.instance_teams = instance_teamsinstance;// For UC44_AutocompleteTableForm
	}
	/**
	 * 	Getter : allPlayers 
	 * 	@return : Return the allPlayers instance.
	 */
	public List<Player44BO> getAllPlayers(){
		return allPlayers; // For UC44_AutocompleteTableForm
	}
	
	/**
	 * 	Setter : allPlayers 
	 *  @param allPlayersinstance : The instance to set.
	 */
	public void setAllPlayers(final List<Player44BO> allPlayersinstance){
		this.allPlayers = allPlayersinstance;// For UC44_AutocompleteTableForm
	}
	/**
	 * 	Getter : selectedTeam 
	 * 	@return : Return the selectedTeam instance.
	 */
	public Team44BO getSelectedTeam(){
		return selectedTeam; // For UC44_AutocompleteTableForm
	}
	
	/**
	 * 	Setter : selectedTeam 
	 *  @param selectedTeaminstance : The instance to set.
	 */
	public void setSelectedTeam(final Team44BO selectedTeaminstance){
		this.selectedTeam = selectedTeaminstance;// For UC44_AutocompleteTableForm
	}
	/**
	 * 	Getter : selectedPlayer 
	 * 	@return : Return the selectedPlayer instance.
	 */
	public Player44BO getSelectedPlayer(){
		return selectedPlayer; // For UC44_AutocompleteTableForm
	}
	
	/**
	 * 	Setter : selectedPlayer 
	 *  @param selectedPlayerinstance : The instance to set.
	 */
	public void setSelectedPlayer(final Player44BO selectedPlayerinstance){
		this.selectedPlayer = selectedPlayerinstance;// For UC44_AutocompleteTableForm
	}
/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UC44_AutocompleteTableForm [ "+
INSTANCE_TEAMS +" = " + instance_teams +ALL_PLAYERS +" = " + allPlayers +SELECTED_TEAM +" = " + selectedTeam +SELECTED_PLAYER +" = " + selectedPlayer + "]";
	}
	
	/**
	 * Method setInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public void setInstanceByInstanceName(final String instanceName, final Object instance) {
		// Set the instance Instance_teams.
		if(INSTANCE_TEAMS.equals(instanceName)){// For UC44_AutocompleteTableForm
			this.setInstance_teams((List)instance); // For UC44_AutocompleteTableForm
		}
				// Set the instance AllPlayers.
		if(ALL_PLAYERS.equals(instanceName)){// For UC44_AutocompleteTableForm
			this.setAllPlayers((List<Player44BO>)instance); // For UC44_AutocompleteTableForm
		}
				// Set the instance SelectedTeam.
		if(SELECTED_TEAM.equals(instanceName)){// For UC44_AutocompleteTableForm
			this.setSelectedTeam((Team44BO)instance); // For UC44_AutocompleteTableForm
		}
				// Set the instance SelectedPlayer.
		if(SELECTED_PLAYER.equals(instanceName)){// For UC44_AutocompleteTableForm
			this.setSelectedPlayer((Player44BO)instance); // For UC44_AutocompleteTableForm
		}
			}
	
	/**
	 * Method getInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public Object getInstanceByInstanceName(final String instanceName){
		// Temporary object for tmpUC44_AutocompleteTableForm.
		Object tmpUC44_AutocompleteTableForm = null;
		
			
		// Get the instance Instance_teams for UC44_AutocompleteTableForm.
		if(INSTANCE_TEAMS.equals(instanceName)){ // For UC44_AutocompleteTableForm
			tmpUC44_AutocompleteTableForm = this.getInstance_teams(); // For UC44_AutocompleteTableForm
		}
			
		// Get the instance AllPlayers for UC44_AutocompleteTableForm.
		if(ALL_PLAYERS.equals(instanceName)){ // For UC44_AutocompleteTableForm
			tmpUC44_AutocompleteTableForm = this.getAllPlayers(); // For UC44_AutocompleteTableForm
		}
			
		// Get the instance SelectedTeam for UC44_AutocompleteTableForm.
		if(SELECTED_TEAM.equals(instanceName)){ // For UC44_AutocompleteTableForm
			tmpUC44_AutocompleteTableForm = this.getSelectedTeam(); // For UC44_AutocompleteTableForm
		}
			
		// Get the instance SelectedPlayer for UC44_AutocompleteTableForm.
		if(SELECTED_PLAYER.equals(instanceName)){ // For UC44_AutocompleteTableForm
			tmpUC44_AutocompleteTableForm = this.getSelectedPlayer(); // For UC44_AutocompleteTableForm
		}
		return tmpUC44_AutocompleteTableForm;// For UC44_AutocompleteTableForm
	}
	
			}
