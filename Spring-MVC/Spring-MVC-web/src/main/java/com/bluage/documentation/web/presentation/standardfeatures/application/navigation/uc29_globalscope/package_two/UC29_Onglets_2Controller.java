/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.application.navigation.uc29_globalscope.package_two ;

// Import declaration.
// Java imports.
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.bluage.documentation.service.standardfeatures.application.navigation.uc29_globalscope.ServiceStadium29;
import com.bluage.documentation.service.standardfeatures.application.navigation.uc29_globalscope.ServiceState29;
import com.netfective.bluage.core.exception.ApplicationException;
import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;
import com.netfective.bluage.core.web.IForm;

/**
 * Class : UC29_Onglets_2Controller
 */
@Controller
@Scope("prototype")
@SessionAttributes("uC29_Onglets_2Form")
@RequestMapping(value= "/presentation/standardfeatures/application/navigation/uc29_globalscope/package_two")
public class UC29_Onglets_2Controller extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC29_Onglets_2Controller.class);
	
	//Current form name
	private static final String U_C29__ONGLETS_2_FORM = "uC29_Onglets_2Form";

	//CONSTANT: allStates
	private static final String ALL_STATES = "allStates";
	//CONSTANT: allStadiums
	private static final String ALL_STADIUMS = "allStadiums";
	
	/**
	 * Property:customDateEditorsUC29_Onglets_2Controller
	 */
	final private Map<String,CustomDateEditor> customDateEditorsUC29_Onglets_2Controller = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:uC29_Onglets_2Validator
	 */
	final private UC29_Onglets_2Validator uC29_Onglets_2Validator = new UC29_Onglets_2Validator();
	
	/**
	 * Service declaration : serviceState29.
	 */
	@Autowired
	private ServiceState29 serviceState29;
	
	/**
	 * Service declaration : serviceStadium29.
	 */
	@Autowired
	private ServiceStadium29 serviceStadium29;
	
	
	// Initialise all the instances for UC29_Onglets_2Controller
		// Declare the instance allStates
		private List allStates;
		// Declare the instance allStadiums
		private List allStadiums;
	/**
	 * Operation : lnk_states_global
 	 * @param model : 
 	 * @param uC29_Onglets_2 : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC29_Onglets_2/lnk_states_global.html",method = RequestMethod.POST)
	public String lnk_states_global(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC29_Onglets_2Form") UC29_Onglets_2Form  uC29_Onglets_2Form, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : lnk_states_global"); 
		infoLogger(uC29_Onglets_2Form); 
		uC29_Onglets_2Form.setAlwaysCallPreControllers(false); 
		// initialization for lnk_states_global
		init();
		
		String destinationPath = null; 
		

	 callFindallstates(request,uC29_Onglets_2Form );
											// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.application.navigation.uc29_globalscope.package_two.UC29_StatesForm or not from lnk_states_global
			final UC29_StatesForm uC29_StatesForm =  new UC29_StatesForm();
			// Populate the destination form with all the instances returned from lnk_states_global.
			final IForm uC29_StatesForm2 = populateDestinationForm(request.getSession(), uC29_StatesForm); 
					
			// Add the form to the model.
			model.addAttribute("uC29_StatesForm", uC29_StatesForm2); 
			
			request.getSession().setAttribute("uC29_StatesForm", uC29_StatesForm2);
			
			// "OK" CASE => destination screen path from lnk_states_global
			LOGGER.info("Go to the screen 'UC29_States'.");
			// Redirect (PRG) from lnk_states_global
			destinationPath =  "redirect:/presentation/standardfeatures/application/navigation/uc29_globalscope/package_two/UC29_States.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_states_global 
	}
	
		/**
	 * Operation : lnk_stadiums_global
 	 * @param model : 
 	 * @param uC29_Onglets_2 : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC29_Onglets_2/lnk_stadiums_global.html",method = RequestMethod.POST)
	public String lnk_stadiums_global(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC29_Onglets_2Form") UC29_Onglets_2Form  uC29_Onglets_2Form, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : lnk_stadiums_global"); 
		infoLogger(uC29_Onglets_2Form); 
		uC29_Onglets_2Form.setAlwaysCallPreControllers(false); 
		// initialization for lnk_stadiums_global
		init();
		
		String destinationPath = null; 
		

	 callFindallstadiums(request,uC29_Onglets_2Form );
											// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.application.navigation.uc29_globalscope.package_two.UC29_StadiumsForm or not from lnk_stadiums_global
			final UC29_StadiumsForm uC29_StadiumsForm =  new UC29_StadiumsForm();
			// Populate the destination form with all the instances returned from lnk_stadiums_global.
			final IForm uC29_StadiumsForm2 = populateDestinationForm(request.getSession(), uC29_StadiumsForm); 
					
			// Add the form to the model.
			model.addAttribute("uC29_StadiumsForm", uC29_StadiumsForm2); 
			
			request.getSession().setAttribute("uC29_StadiumsForm", uC29_StadiumsForm2);
			
			// "OK" CASE => destination screen path from lnk_stadiums_global
			LOGGER.info("Go to the screen 'UC29_Stadiums'.");
			// Redirect (PRG) from lnk_stadiums_global
			destinationPath =  "redirect:/presentation/standardfeatures/application/navigation/uc29_globalscope/package_two/UC29_Stadiums.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_stadiums_global 
	}
	
	
	/**
	* This method initialise the form : UC29_Onglets_2Form 
	* @return UC29_Onglets_2Form
	*/
	@ModelAttribute("UC29_Onglets_2FormInit")
	public void initUC29_Onglets_2Form(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, U_C29__ONGLETS_2_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method UC29_Onglets_2Form."); //for lnk_stadiums_global 
		}
		UC29_Onglets_2Form uC29_Onglets_2Form;
	
		if(request.getSession().getAttribute(U_C29__ONGLETS_2_FORM) != null){
			uC29_Onglets_2Form = (UC29_Onglets_2Form)request.getSession().getAttribute(U_C29__ONGLETS_2_FORM);
		} else {
			uC29_Onglets_2Form = new UC29_Onglets_2Form();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form UC29_Onglets_2Form.");
		}
		uC29_Onglets_2Form = (UC29_Onglets_2Form)populateDestinationForm(request.getSession(), uC29_Onglets_2Form);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  UC29_Onglets_2Form.");
		}
		model.addAttribute(U_C29__ONGLETS_2_FORM, uC29_Onglets_2Form);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param UC29_Onglets_2Form : The sceen form.
	 */
	@RequestMapping(value = "/UC29_Onglets_2.html" ,method = RequestMethod.GET)
	public void prepareUC29_Onglets_2(final HttpServletRequest request,final  Model model,final  @ModelAttribute("UC29_Onglets_2FormInit") UC29_Onglets_2Form uC29_Onglets_2Form){
		
		UC29_Onglets_2Form currentUC29_Onglets_2Form = uC29_Onglets_2Form;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(U_C29__ONGLETS_2_FORM) == null){
			if(currentUC29_Onglets_2Form!=null){
				request.getSession().setAttribute(U_C29__ONGLETS_2_FORM, currentUC29_Onglets_2Form);
			}else {
				currentUC29_Onglets_2Form = new UC29_Onglets_2Form();
				request.getSession().setAttribute(U_C29__ONGLETS_2_FORM, currentUC29_Onglets_2Form);	
			}
		} else {
			currentUC29_Onglets_2Form = (UC29_Onglets_2Form) request.getSession().getAttribute(U_C29__ONGLETS_2_FORM);
		}

					currentUC29_Onglets_2Form = (UC29_Onglets_2Form)populateDestinationForm(request.getSession(), currentUC29_Onglets_2Form);
		// Call all the Precontroller.
	currentUC29_Onglets_2Form.setAlwaysCallPreControllers(true);
		model.addAttribute(U_C29__ONGLETS_2_FORM, currentUC29_Onglets_2Form);
		request.getSession().setAttribute(U_C29__ONGLETS_2_FORM, currentUC29_Onglets_2Form);
		
	}
	
				/**
	 * method callFindallstates
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callFindallstates(HttpServletRequest request,IForm form) {
		allStates = (List)get(request.getSession(),form, "allStates");  
										 
					try {
				// executing Findallstates in lnk_stadiums_global
				allStates = 	serviceState29.state29FindAll(
	);  
 
				put(request.getSession(), ALL_STATES,allStates);
								// processing variables Findallstates in lnk_stadiums_global

			} catch (ApplicationException e) { 
				// error handling for operation state29FindAll called Findallstates
				errorLogger("An error occured during the execution of the operation : state29FindAll",e); 
		
			}
	}
		/**
	 * method callFindallstadiums
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callFindallstadiums(HttpServletRequest request,IForm form) {
		allStadiums = (List)get(request.getSession(),form, "allStadiums");  
										 
					try {
				// executing Findallstadiums in lnk_stadiums_global
				allStadiums = 	serviceStadium29.stadium29FindAll(
	);  
 
				put(request.getSession(), ALL_STADIUMS,allStadiums);
								// processing variables Findallstadiums in lnk_stadiums_global

			} catch (ApplicationException e) { 
				// error handling for operation stadium29FindAll called Findallstadiums
				errorLogger("An error occured during the execution of the operation : stadium29FindAll",e); 
		
			}
	}
		
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("UC29_Onglets_2Controller [ ");
			strBToS.append(ALL_STATES);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(allStates);
			strBToS.append(END_TO_STRING_DELIMITER);
			strBToS.append(ALL_STADIUMS);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(allStadiums);
			strBToS.append(END_TO_STRING_DELIMITER);
		// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(uC29_Onglets_2Validator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsUC29_Onglets_2Controller!=null); 
		return strBToS.toString();
	}
}
