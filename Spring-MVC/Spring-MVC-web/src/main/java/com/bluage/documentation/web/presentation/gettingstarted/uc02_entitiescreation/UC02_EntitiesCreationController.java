/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.gettingstarted.uc02_entitiescreation ;

// Import declaration.
// Java imports.
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.bluage.documentation.business.gettingstarted.uc02_entitiescreation.bos.Team02BO;
import com.bluage.documentation.business.gettingstarted.uc02_entitiescreation.entities.daofinder.State02DAOFinderImpl;
import com.bluage.documentation.service.gettingstarted.uc02_entitiescreation.ServiceTeam02;
import com.netfective.bluage.core.exception.ApplicationException;
import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;
import com.netfective.bluage.core.web.IForm;

/**
 * Class : UC02_EntitiesCreationController
 */
@Controller
@Scope("prototype")
@SessionAttributes("uC02_EntitiesCreationForm")
@RequestMapping(value= "/presentation/gettingstarted/uc02_entitiescreation")
public class UC02_EntitiesCreationController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC02_EntitiesCreationController.class);
	
	//Current form name
	private static final String U_C02__ENTITIES_CREATION_FORM = "uC02_EntitiesCreationForm";

		//CONSTANT: allStates
	private static final String ALL_STATES = "allStates";
							//CONSTANT: resultDelete
	private static final String RESULT_DELETE = "resultDelete";
	//CONSTANT: teamToDelete
	private static final String TEAM_TO_DELETE = "teamToDelete";
	//CONSTANT: allTeams
	private static final String ALL_TEAMS = "allTeams";
	//CONSTANT: teamToCreate
	private static final String TEAM_TO_CREATE = "teamToCreate";
	
	/**
	 * Property:customDateEditorsUC02_EntitiesCreationController
	 */
	final private Map<String,CustomDateEditor> customDateEditorsUC02_EntitiesCreationController = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:uC02_EntitiesCreationValidator
	 */
	final private UC02_EntitiesCreationValidator uC02_EntitiesCreationValidator = new UC02_EntitiesCreationValidator();
	
	/**
	 * Service declaration : serviceTeam02.
	 */
	@Autowired
	private ServiceTeam02 serviceTeam02;
	
	/**
	 * Pre Controller declaration : uC02_PreControllerFindAllTeams
	 */
	@Autowired
	private UC02_PreControllerFindAllTeamsController uC02_PreControllerFindAllTeams;

	/**
	 * Generic Finder : state02DAOFinderImpl.
	 */
	@Autowired
	private State02DAOFinderImpl state02DAOFinderImpl;
	
	
	// Initialise all the instances for UC02_EntitiesCreationController
			// Initialize the instance allStates for the state lnk_update
		private List allStates; // Initialize the instance allStates for UC02_EntitiesCreationController
										// Declare the instance resultDelete
		private boolean resultDelete;
		// Declare the instance teamToDelete
		private Team02BO teamToDelete;
		// Declare the instance allTeams
		private List allTeams;
		// Declare the instance teamToCreate
		private Team02BO teamToCreate;
								/**
	 * Operation : lnk_delete
 	 * @param model : 
 	 * @param uC02_EntitiesCreation : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC02_EntitiesCreation/lnk_delete.html",method = RequestMethod.POST)
	public String lnk_delete(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC02_EntitiesCreationForm") UC02_EntitiesCreationForm  uC02_EntitiesCreationForm, final BindingResult bindingResult ,final @RequestParam(ID_REQUEST_PARAM) Integer index){
		
		
		
		LOGGER.info("Begin method : lnk_delete"); 
		infoLogger(uC02_EntitiesCreationForm); 
		uC02_EntitiesCreationForm.setAlwaysCallPreControllers(false); 
		// initialization for lnk_delete
		init();
		
		String destinationPath = null; 
		

										 callDeleteselectedteam(request,uC02_EntitiesCreationForm , index);
			
 callFindafterdelete(request,uC02_EntitiesCreationForm );
						// Init the destination form com.bluage.documentation.web.presentation.gettingstarted.uc02_entitiescreation.UC02_EntitiesCreationForm or not from lnk_delete
			// Populate the destination form with all the instances returned from lnk_delete.
			final IForm uC02_EntitiesCreationForm2 = populateDestinationForm(request.getSession(), uC02_EntitiesCreationForm); 
					
			// Add the form to the model.
			model.addAttribute("uC02_EntitiesCreationForm", uC02_EntitiesCreationForm2); 
			
			request.getSession().setAttribute("uC02_EntitiesCreationForm", uC02_EntitiesCreationForm2);
			
			// "OK" CASE => destination screen path from lnk_delete
			LOGGER.info("Go to the screen 'UC02_EntitiesCreation'.");
			// Redirect (PRG) from lnk_delete
			destinationPath =  "redirect:/presentation/gettingstarted/uc02_entitiescreation/UC02_EntitiesCreation.html";
						
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_delete 
	}
	
				/**
	 * Operation : lnk_create
 	 * @param model : 
 	 * @param uC02_EntitiesCreation : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC02_EntitiesCreation/lnk_create.html",method = RequestMethod.POST)
	public String lnk_create(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC02_EntitiesCreationForm") UC02_EntitiesCreationForm  uC02_EntitiesCreationForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : lnk_create"); 
		infoLogger(uC02_EntitiesCreationForm); 
		uC02_EntitiesCreationForm.setAlwaysCallPreControllers(false); 
		// initialization for lnk_create
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state lnk_create if the validation fail, you will be forward to the screen : "/presentation/gettingstarted/uc02_entitiescreation/UC02_EntitiesCreation"
		if(errorsMessages(request,response,uC02_EntitiesCreationForm,bindingResult,"", uC02_EntitiesCreationValidator, customDateEditorsUC02_EntitiesCreationController)){ 
			return "/presentation/gettingstarted/uc02_entitiescreation/UC02_EntitiesCreation"; 
		}

					 callCreateTeam02(request,uC02_EntitiesCreationForm );
			
 callFindallTeam02(request,uC02_EntitiesCreationForm );
						// Init the destination form com.bluage.documentation.web.presentation.gettingstarted.uc02_entitiescreation.UC02_EntitiesCreationForm or not from lnk_create
			// Populate the destination form with all the instances returned from lnk_create.
			final IForm uC02_EntitiesCreationForm2 = populateDestinationForm(request.getSession(), uC02_EntitiesCreationForm); 
					
			// Add the form to the model.
			model.addAttribute("uC02_EntitiesCreationForm", uC02_EntitiesCreationForm2); 
			
			request.getSession().setAttribute("uC02_EntitiesCreationForm", uC02_EntitiesCreationForm2);
			
			// "OK" CASE => destination screen path from lnk_create
			LOGGER.info("Go to the screen 'UC02_EntitiesCreation'.");
			// Redirect (PRG) from lnk_create
			destinationPath =  "redirect:/presentation/gettingstarted/uc02_entitiescreation/UC02_EntitiesCreation.html";
						
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_create 
	}
	
	
	/**
	* This method initialise the form : UC02_EntitiesCreationForm 
	* @return UC02_EntitiesCreationForm
	*/
	@ModelAttribute("UC02_EntitiesCreationFormInit")
	public void initUC02_EntitiesCreationForm(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, U_C02__ENTITIES_CREATION_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method UC02_EntitiesCreationForm."); //for lnk_create 
		}
		UC02_EntitiesCreationForm uC02_EntitiesCreationForm;
	
		if(request.getSession().getAttribute(U_C02__ENTITIES_CREATION_FORM) != null){
			uC02_EntitiesCreationForm = (UC02_EntitiesCreationForm)request.getSession().getAttribute(U_C02__ENTITIES_CREATION_FORM);
		} else {
			uC02_EntitiesCreationForm = new UC02_EntitiesCreationForm();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form UC02_EntitiesCreationForm.");
		}
		uC02_EntitiesCreationForm = (UC02_EntitiesCreationForm)populateDestinationForm(request.getSession(), uC02_EntitiesCreationForm);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  UC02_EntitiesCreationForm.");
		}
		model.addAttribute(U_C02__ENTITIES_CREATION_FORM, uC02_EntitiesCreationForm);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param UC02_EntitiesCreationForm : The sceen form.
	 */
	@RequestMapping(value = "/UC02_EntitiesCreation.html" ,method = RequestMethod.GET)
	public void prepareUC02_EntitiesCreation(final HttpServletRequest request,final  Model model,final  @ModelAttribute("UC02_EntitiesCreationFormInit") UC02_EntitiesCreationForm uC02_EntitiesCreationForm){
		
		UC02_EntitiesCreationForm currentUC02_EntitiesCreationForm = uC02_EntitiesCreationForm;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(U_C02__ENTITIES_CREATION_FORM) == null){
			if(currentUC02_EntitiesCreationForm!=null){
				request.getSession().setAttribute(U_C02__ENTITIES_CREATION_FORM, currentUC02_EntitiesCreationForm);
			}else {
				currentUC02_EntitiesCreationForm = new UC02_EntitiesCreationForm();
				request.getSession().setAttribute(U_C02__ENTITIES_CREATION_FORM, currentUC02_EntitiesCreationForm);	
			}
		} else {
			currentUC02_EntitiesCreationForm = (UC02_EntitiesCreationForm) request.getSession().getAttribute(U_C02__ENTITIES_CREATION_FORM);
		}

		try {
			List allStates = state02DAOFinderImpl.findAll();
			put(request.getSession(), ALL_STATES, allStates);
		} catch (ApplicationException e) {
			errorLogger("An error while calling the generic finder for the instance : allStates.",e);
		}
					currentUC02_EntitiesCreationForm = (UC02_EntitiesCreationForm)populateDestinationForm(request.getSession(), currentUC02_EntitiesCreationForm);
		// Call all the Precontroller.
	if ( currentUC02_EntitiesCreationForm.isAlwaysCallPreControllers()) {
	
		// Calling the PreController : UC02_PreControllerFindAllTeams.
		currentUC02_EntitiesCreationForm = (UC02_EntitiesCreationForm) uC02_PreControllerFindAllTeams.uC02_PreControllerFindAllTeamsControllerInit(request, model, currentUC02_EntitiesCreationForm);
		
	}
	currentUC02_EntitiesCreationForm.setAlwaysCallPreControllers(true);
		model.addAttribute(U_C02__ENTITIES_CREATION_FORM, currentUC02_EntitiesCreationForm);
		request.getSession().setAttribute(U_C02__ENTITIES_CREATION_FORM, currentUC02_EntitiesCreationForm);
		
	}
	
											/**
	 * method callDeleteselectedteam
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 * @param index Integer the current index 
	 */
	private void callDeleteselectedteam(HttpServletRequest request,IForm form,Integer index) {
		resultDelete = (Boolean)get(request.getSession(),form, "resultDelete");  
										 
					if(index!=null){  
			 teamToDelete = (Team02BO)((List)get(request.getSession(),form, "allTeams")).get(index);  
		} else {
			 teamToDelete= null; 
		}
				 
					try {
				// executing Deleteselectedteam in lnk_create
				resultDelete = 	serviceTeam02.team02Delete(
			teamToDelete
			);  
 
				put(request.getSession(), RESULT_DELETE,resultDelete);
								// processing variables Deleteselectedteam in lnk_create
				put(request.getSession(), TEAM_TO_DELETE,teamToDelete); 
			
			} catch (ApplicationException e) { 
				// error handling for operation team02Delete called Deleteselectedteam
				errorLogger("An error occured during the execution of the operation : team02Delete",e); 
		
			}
	}
		/**
	 * method callFindallTeam02
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callFindallTeam02(HttpServletRequest request,IForm form) {
		allTeams = (List)get(request.getSession(),form, "allTeams");  
										 
					try {
				// executing FindallTeam02 in lnk_create
				allTeams = 	serviceTeam02.team02FindAll(
	);  
 
				put(request.getSession(), ALL_TEAMS,allTeams);
								// processing variables FindallTeam02 in lnk_create

			} catch (ApplicationException e) { 
				// error handling for operation team02FindAll called FindallTeam02
				errorLogger("An error occured during the execution of the operation : team02FindAll",e); 
		
			}
	}
		/**
	 * method callFindafterdelete
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callFindafterdelete(HttpServletRequest request,IForm form) {
		allTeams = (List)get(request.getSession(),form, "allTeams");  
										 
					try {
				// executing Findafterdelete in lnk_create
				allTeams = 	serviceTeam02.team02FindAll(
	);  
 
				put(request.getSession(), ALL_TEAMS,allTeams);
								// processing variables Findafterdelete in lnk_create

			} catch (ApplicationException e) { 
				// error handling for operation team02FindAll called Findafterdelete
				errorLogger("An error occured during the execution of the operation : team02FindAll",e); 
		
			}
	}
					/**
	 * method callCreateTeam02
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callCreateTeam02(HttpServletRequest request,IForm form) {
					teamToCreate = (Team02BO)get(request.getSession(),form, "teamToCreate");  
										 
					try {
				// executing CreateTeam02 in lnk_create
	serviceTeam02.createTeam02(
			teamToCreate
			);  

								// processing variables CreateTeam02 in lnk_create
				put(request.getSession(), TEAM_TO_CREATE,teamToCreate); 
			
			} catch (ApplicationException e) { 
				// error handling for operation createTeam02 called CreateTeam02
				errorLogger("An error occured during the execution of the operation : createTeam02",e); 
		
			}
	}
		
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("UC02_EntitiesCreationController [ ");
				strBToS.append(ALL_STATES);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(allStates);
			strBToS.append(END_TO_STRING_DELIMITER);
									strBToS.append(RESULT_DELETE);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(resultDelete);
			strBToS.append(END_TO_STRING_DELIMITER);
			strBToS.append(TEAM_TO_DELETE);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(teamToDelete);
			strBToS.append(END_TO_STRING_DELIMITER);
			strBToS.append(ALL_TEAMS);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(allTeams);
			strBToS.append(END_TO_STRING_DELIMITER);
			strBToS.append(TEAM_TO_CREATE);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(teamToCreate);
			strBToS.append(END_TO_STRING_DELIMITER);
		// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(uC02_EntitiesCreationValidator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsUC02_EntitiesCreationController!=null); 
		return strBToS.toString();
	}
}
