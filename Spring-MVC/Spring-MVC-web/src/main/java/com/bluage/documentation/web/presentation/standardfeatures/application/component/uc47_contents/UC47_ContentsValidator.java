/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.application.component.uc47_contents ;


import org.apache.log4j.Logger;
import org.springframework.validation.Errors;

import com.netfective.bluage.core.validator.AbstractValidator;
/**
* Class :UC47_ContentsValidator
*/
public class UC47_ContentsValidator extends AbstractValidator{
	
	// LOGGER for the class UC47_ContentsValidator
	private static final Logger LOGGER = Logger.getLogger( UC47_ContentsValidator.class);
	
	
	/**
	* Operation validate for UC47_ContentsForm
	* @param obj : the current form (UC47_ContentsForm)
	* @param errors : The spring errors to return for the form UC47_ContentsForm
	*/
	public void validate(final Object obj,final Errors errors){
		LOGGER.info("Beginning method : validate"); // UC47_ContentsValidator
		UC47_ContentsForm cUC47_ContentsForm = (UC47_ContentsForm)obj; // UC47_ContentsValidator
		LOGGER.info("Ending method : validate the form "+ cUC47_ContentsForm.getClass().getName()); // UC47_ContentsValidator
	}

	/**
	* Method to implements to use spring validators (UC47_ContentsForm)
	* @param aClass : Class for the form UC47_ContentsForm
	* @return boolean
	*/
	public boolean supports(final Class aClass){
		return (new UC47_ContentsForm()).getClass().equals(aClass); // UC47_ContentsValidator
	}
}
