/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.application.datagrid.uc35_paginatorbdd ;

// Import declaration.
// Java imports.
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.bluage.documentation.service.standardfeatures.application.datagrid.uc35_paginatorbdd.ServicePaginatorBDD;
import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;
import com.netfective.bluage.core.web.IForm;

/**
 * Class : UC35_HomePageController
 */
@Controller
@Scope("prototype")
@SessionAttributes("uC35_HomePageForm")
@RequestMapping(value= "/presentation/standardfeatures/application/datagrid/uc35_paginatorbdd")
public class UC35_HomePageController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC35_HomePageController.class);
	
	//Current form name
	private static final String U_C35__HOME_PAGE_FORM = "uC35_HomePageForm";

	//CONSTANT: listPlayers
	private static final String LIST_PLAYERS = "listPlayers";
	
	/**
	 * Property:customDateEditorsUC35_HomePageController
	 */
	final private Map<String,CustomDateEditor> customDateEditorsUC35_HomePageController = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:uC35_HomePageValidator
	 */
	final private UC35_HomePageValidator uC35_HomePageValidator = new UC35_HomePageValidator();
	
	/**
	 * Service declaration : servicePaginatorBDD.
	 */
	@Autowired
	private ServicePaginatorBDD servicePaginatorBDD;
	
	
	// Initialise all the instances for UC35_HomePageController
		// Declare the instance listPlayers
		private List listPlayers;
					/**
	 * Operation : lnk_paginatorbdd
 	 * @param model : 
 	 * @param uC35_HomePage : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC35_HomePage/lnk_paginatorbdd.html",method = RequestMethod.GET)
	public String lnk_paginatorbdd(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC35_HomePageForm") UC35_HomePageForm  uC35_HomePageForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : lnk_paginatorbdd"); 
		infoLogger(uC35_HomePageForm); 
		uC35_HomePageForm.setAlwaysCallPreControllers(false); 
		// initialization for lnk_paginatorbdd
		init();
		
		String destinationPath = null; 
		

									// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.application.datagrid.uc35_paginatorbdd.UC35_PaginatorBDDForm or not from lnk_paginatorbdd
			final UC35_PaginatorBDDForm uC35_PaginatorBDDForm =  new UC35_PaginatorBDDForm();
			// Populate the destination form with all the instances returned from lnk_paginatorbdd.
			final IForm uC35_PaginatorBDDForm2 = populateDestinationForm(request.getSession(), uC35_PaginatorBDDForm); 
					
			// Add the form to the model.
			model.addAttribute("uC35_PaginatorBDDForm", uC35_PaginatorBDDForm2); 
			
			request.getSession().setAttribute("uC35_PaginatorBDDForm", uC35_PaginatorBDDForm2);
			
			// "OK" CASE => destination screen path from lnk_paginatorbdd
			LOGGER.info("Go to the screen 'UC35_PaginatorBDD'.");
			// Redirect (PRG) from lnk_paginatorbdd
			destinationPath =  "redirect:/presentation/standardfeatures/application/datagrid/uc35_paginatorbdd/UC35_PaginatorBDD.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_paginatorbdd 
	}
	
	
	/**
	* This method initialise the form : UC35_HomePageForm 
	* @return UC35_HomePageForm
	*/
	@ModelAttribute("UC35_HomePageFormInit")
	public void initUC35_HomePageForm(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, U_C35__HOME_PAGE_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method UC35_HomePageForm."); //for lnk_paginatorbdd 
		}
		UC35_HomePageForm uC35_HomePageForm;
	
		if(request.getSession().getAttribute(U_C35__HOME_PAGE_FORM) != null){
			uC35_HomePageForm = (UC35_HomePageForm)request.getSession().getAttribute(U_C35__HOME_PAGE_FORM);
		} else {
			uC35_HomePageForm = new UC35_HomePageForm();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form UC35_HomePageForm.");
		}
		uC35_HomePageForm = (UC35_HomePageForm)populateDestinationForm(request.getSession(), uC35_HomePageForm);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  UC35_HomePageForm.");
		}
		model.addAttribute(U_C35__HOME_PAGE_FORM, uC35_HomePageForm);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param UC35_HomePageForm : The sceen form.
	 */
	@RequestMapping(value = "/UC35_HomePage.html" ,method = RequestMethod.GET)
	public void prepareUC35_HomePage(final HttpServletRequest request,final  Model model,final  @ModelAttribute("UC35_HomePageFormInit") UC35_HomePageForm uC35_HomePageForm){
		
		UC35_HomePageForm currentUC35_HomePageForm = uC35_HomePageForm;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(U_C35__HOME_PAGE_FORM) == null){
			if(currentUC35_HomePageForm!=null){
				request.getSession().setAttribute(U_C35__HOME_PAGE_FORM, currentUC35_HomePageForm);
			}else {
				currentUC35_HomePageForm = new UC35_HomePageForm();
				request.getSession().setAttribute(U_C35__HOME_PAGE_FORM, currentUC35_HomePageForm);	
			}
		} else {
			currentUC35_HomePageForm = (UC35_HomePageForm) request.getSession().getAttribute(U_C35__HOME_PAGE_FORM);
		}

		currentUC35_HomePageForm = (UC35_HomePageForm)populateDestinationForm(request.getSession(), currentUC35_HomePageForm);
		// Call all the Precontroller.
	currentUC35_HomePageForm.setAlwaysCallPreControllers(true);
		model.addAttribute(U_C35__HOME_PAGE_FORM, currentUC35_HomePageForm);
		request.getSession().setAttribute(U_C35__HOME_PAGE_FORM, currentUC35_HomePageForm);
		
	}
	
		
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("UC35_HomePageController [ ");
			strBToS.append(LIST_PLAYERS);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(listPlayers);
			strBToS.append(END_TO_STRING_DELIMITER);
		// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(uC35_HomePageValidator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsUC35_HomePageController!=null); 
		return strBToS.toString();
	}
}
