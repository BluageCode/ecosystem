/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.business.gettingstarted.uc02_entitiescreation.entities.daofinder;


import javax.inject.Named;

import com.bluage.documentation.business.AbstractDaoFinderImpl;
import com.bluage.documentation.business.gettingstarted.uc02_entitiescreation.bos.Team02BO;

/**
 * This class represents the implementation of 
 * the search methods for the entity {@link Team02}.
 *
 * @see		AbstractDaoFinderImpl
 */
 
@Named ("com.bluage.documentation.business.gettingstarted.uc02_entitiescreation.entities.daofinder.Team02DAOFinderImpl") 
public class Team02DAOFinderImpl extends AbstractDaoFinderImpl<Team02BO>{
	
   /**
	* Default order
	*/
	private static final String DEFAULT_ORDER ="";

	/**
	* The associated BO 
	*/
	private final Class<Team02BO> refBo = com.bluage.documentation.business.gettingstarted.uc02_entitiescreation.bos.Team02BO.class;
	

	/**
	* Returns the associated BO
	* @return Class<Team02BO>
	*/
	@Override
	public final Class<Team02BO> getRefBo(){
		return this.refBo;
	}
	
	/**
	* Returns the default order by instruction
	* @return String
	*/
	@Override
	public final String getDefaultOrder(){
		return this.DEFAULT_ORDER;
	}
}
