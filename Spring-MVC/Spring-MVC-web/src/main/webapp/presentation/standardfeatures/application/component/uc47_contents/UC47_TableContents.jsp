<%@ include file="/include.jsp"  %>

	<form:form id="uC47_TableContents" commandName="uC47_TableContentsForm"  enctype="application/x-www-form-urlencoded" method="post">
<h2 class="rd_first"  > 
<fmt:message key="dynamicUC47_TableContents_STANDARD_FEATURES__APPLICATIO"/>
</h2>
<display:table uid="tab_players"  sort="list" requestURI="" name="sessionScope.uC47_TableContentsForm.allPlayers" class="rd_tabledata"  >
	<c:set   var="rownum">
		<%=(tab_players !=	null) ? new Integer(tab_players_rowNum.intValue() - 1) : new Integer(0) %>
	</c:set>
	<c:set   var="idRow">
		<%=(tab_players != null ) ? tab_players_rowNum.intValue() - 1 : -1 %>
	</c:set>
	<c:set   var="idTab">
		<%=(tab_players != null ) ? "tab_players" : null %>
	</c:set>
	<c:set var="tableHashId">
		<%= new org.displaytag.util.ParamEncoder("tab_players").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE) %>
	</c:set>
	<c:set var="tableHashId2">
		<%=request.getParameter(new org.displaytag.util.ParamEncoder("tab_players").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE)) %>
	</c:set>	
	
<display:column format=""  titleKey="dynamicUC47_TableContents_NAME">
		<span id="dyn_lastname">
	<c:out value="${tab_players.lastName}" />
		</span>
	<fmt:message key="dynamicUC47_TableContents_"/>
		<span id="dyn_firstname">
	<c:out value="${tab_players.firstName}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="dynamicUC47_TableContents_DATE_OF_BIRTH">
<span id="dyn_dateofbirth">
	<format:formatDateValidator value="${tab_players.dateOfBirth}" pattern="MM/dd/yyyy" />
</span>
	</display:column>
	
<display:column format=""  titleKey="dynamicUC47_TableContents_WEIGHT_KG">
		<span id="dyn_weight">
	<c:out value="${tab_players.weight}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="dynamicUC47_TableContents_HEIGHT_CM">
		<span id="dyn_height">
	<c:out value="${tab_players.height}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="dynamicUC47_TableContents_ESTIMATED_VALUE_M">
		<span id="dyn_value">
	<c:out value="${tab_players.estimatedValue}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="dynamicUC47_TableContents_POSITION">
		<span id="dyn_position">
	<c:out value="${tab_players.position.code}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="dynamicUC47_TableContents__96">
<a id="lnk_details[${rownum}]"onclick="javascript:btn_click_cExecuteUC47_TableContents(this.href);return false;"			href="<c:url value="/presentation/standardfeatures/application/component/uc47_contents/UC47_TableContents/lnk_details.html?id="/><c:out value="${idRow}" />&tab=<c:out value="tab_players" />&<c:out value="${tableHashId}"/>=<c:out value="${tableHashId2}"/>"
	 shape="rect"  >									 
<fmt:message key="dynamicUC47_TableContents_DETAILS"/>
</a>
</display:column>
	
<display:column format=""  titleKey="dynamicUC47_TableContents__97">
<a id="lnk_edit[${rownum}]"onclick="javascript:btn_click_cExecuteUC47_TableContents(this.href);return false;"			href="<c:url value="/presentation/standardfeatures/application/component/uc47_contents/UC47_TableContents/lnk_edit.html?id="/><c:out value="${idRow}" />&tab=<c:out value="tab_players" />&<c:out value="${tableHashId}"/>=<c:out value="${tableHashId2}"/>"
	 shape="rect"  >									 
<fmt:message key="dynamicUC47_TableContents_EDIT"/>
</a>
</display:column>
</display:table>
<label  > 
<fmt:message key="dynamicUC47_TableContents_DETAILS_LINKS_ALLOW_YOU_TO_SE"/>
</label>
	</form:form>
	<script language="javascript">
		function btn_click_cExecuteUC47_TableContents(url) {
			UC47_TableContentsvar = document.getElementById('uC47_TableContents');
			UC47_TableContentsvar.setAttribute('action', url);
			UC47_TableContentsvar.submit();
		}
		function btn_click_cSetActionUC47_TableContents(url) {
			UC47_TableContentsvar = document.getElementById('uC47_TableContents');
			UC47_TableContentsvar.setAttribute('action', url);
		}
	</script>
