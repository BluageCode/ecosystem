/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.business.standardfeatures.entities.uc69_cachehibernate.bos;

import java.util.Date;

import javax.inject.Inject;
import javax.inject.Named;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import com.bluage.documentation.business.common.bos.PersonBO;
import com.bluage.documentation.business.standardfeatures.entities.uc69_cachehibernate.entities.daofinder.Player69DAOFinderImpl;

/**
 * Player69BO
 *
* @see com.netfective.bluage.core.business.IBusinessObject
* * @see PersonBO
 */
@XmlRootElement

public class Player69BO
 extends PersonBO {
	
	/**
	 * Default SUID for Player69BO.
	 */
	private static final long serialVersionUID = 1L;
	

	@Inject
	@Named ("com.bluage.documentation.business.standardfeatures.entities.uc69_cachehibernate.entities.daofinder.Player69Finder") 
	private Player69DAOFinderImpl finder;

												/**
	 * Entity property: team69
	 *
	 */
private Team69BO team69;

					/**
	 * Entity property: dateOfBirth
	 *
	 */
private Date dateOfBirth;



   /**
	* public default constructor for class Player69BO
	*/
	public Player69BO(){
		//NOP Player69BO
	}


												/**
	 * Getter for team69 of class Player69BO.
	 *
	 * @return {@link com.bluage.documentation.business.standardfeatures.entities.uc69_cachehibernate.bos.Team69BO}
	 * @generated
	 */ 
public  Team69BO getTeam69() {
		Team69BO team690 = this.team69;
		if(this.team69 == null) {
			if(DAO_MODE.equals(this.getObjMode( ) ) ) {
				team690 = null;
			} else {
				this.team69 = new Team69BO();
				this.team69.setObjMode( this.getObjMode( ) );
				team690 = this.team69;
			}
		} else {
			this.team69.setObjMode( this.getObjMode( ) );
			if( HELPER_MODE.equals(this.getObjMode( ) ) ) {
				team690 = this.team69;
			}
			if( DAO_MODE.equals(this.getObjMode()) && this.team69.isEmpty ) {
				team690 = null;
			}
		}
		return team690;
	}


	/**
	 * Setter for team69 of class Player69BO.
	 * @param value {@link com.bluage.documentation.business.standardfeatures.entities.uc69_cachehibernate.bos.Team69BO}
	 * @generated
	 */

public  void setTeam69(final Team69BO team69) {
	if(team69 != null) {
		team69.isEmpty = false;
		team69.setObjMode( this.getObjMode( ) );
	}
	this.isEmpty = false;
	this.team69 = team69;
}

					/**
	 * Getter for dateOfBirth of class Player69BO.
	 *
	 * @return {@link java.util.Date}
	 * @generated
	 */ 
public  Date getDateOfBirth() {
	return this.dateOfBirth;
	}


	/**
	 * Setter for dateOfBirth of class Player69BO.
	 * @param value {@link java.util.Date}
	 * @generated
	 */

public  void setDateOfBirth(final Date dateOfBirth) {
	this.dateOfBirth = dateOfBirth;
}


/**
	 * Returns <code>true</code> if the argument is an Player69 instance and all identifiers for this entity
     * equal the identifiers of the argument entity. The <code>equals</code> method of the parent entity
     * will also need to return <code>true</code>. Returns <code>false</code> otherwise.
     *
     * @see PersonBO#hashCode()
	 * @param object  object to compare with current instance      
     * @return boolean true if equals false if not
     */
    public boolean equals(final Object object )
    {
		if ( this == object )
        {
            return true;
        }
        if ( !( object instanceof Player69BO ) )
        {
            return false;
        }
        final Player69BO that = (Player69BO)object;
    
    	            if (        				        			this.getId() == null
        	        ) {
        	return false;
        }
		return new EqualsBuilder()
			.append(this.getId(), that.getId())
			 	.isEquals();
	}
	/**

	 * Returns a hash code based on this entity's identifiers and the hash code of the parent entity.
     *
     * @see PersonBO#hashCode()
     * @return int
     */    
    public int hashCode() {
        return new HashCodeBuilder()
             .append(getId())
				 	.appendSuper(super.hashCode())
            .toHashCode();
    }
}

