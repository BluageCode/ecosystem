/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.service.advancedfeatures.valueobject.uc206_valueobject.voservice;


import javax.inject.Inject;
import javax.inject.Named;

import com.bluage.documentation.business.advancedfeatures.valueobject.uc206_valueobject.bos.Player206BO;
import com.bluage.documentation.business.advancedfeatures.valueobject.uc206_valueobject.bos.Position206BO;
import com.bluage.documentation.business.advancedfeatures.valueobject.uc206_valueobject.vo.Player206VO;
import com.bluage.documentation.business.advancedfeatures.valueobject.uc206_valueobject.vo.Position206VO;
import com.bluage.documentation.service.AbstractService;
import com.bluage.documentation.service.advancedfeatures.valueobject.uc206_valueobject.ServicePosition206;
import com.netfective.bluage.core.exception.ApplicationException;


/**
 * Defines  application VO services for ServicePlayer206TransferVO2BO
 *
 */
@Named ("com.bluage.documentation.service.advancedfeatures.valueobject.uc206_valueobject.voservice.ServicePlayer206TransferVO2BO")
public class ServicePlayer206TransferVO2BO  extends AbstractService{
					@Inject
		@Named ("com.bluage.documentation.service.advancedfeatures.valueobject.uc206_valueobject.ServicePosition206")
		private ServicePosition206 instanceServicePosition206;


	/**
	 *  Default constructor 
	 */
	public ServicePlayer206TransferVO2BO() {
		super();
	}



			
	
	/**    
	 * VO operation.
	 * @param vo {@link Player206VO}
		 * @param bo {@link Player206BO}
		 * @return {@link void}
	 */
	public void player206Transfer(Player206VO vo,Player206BO bo )
 throws ApplicationException {
privatePlayer206Transfer(vo,bo);
	}


	/**    
	 * Operation Transfert.
	 * @param vo {@link Player206VO}
		 * @param bo {@link Player206BO}
		 * @return {@link void}
	 */
	private void privatePlayer206Transfer(Player206VO vo,Player206BO bo )
 throws ApplicationException {
					player206TransferFkPositionVO(vo,bo);		
			}

		
			
	
	/**    
	 * VO operation.
	 * @param vo {@link Player206VO}
		 * @param bo {@link Player206BO}
		 * @return {@link void}
	 */
	public void player206TransferFkPositionVO(Player206VO vo,Player206BO bo )
 throws ApplicationException {
privatePlayer206TransferFkPositionVO(vo,bo);
	}


	
		/**    
	 * Transfer one relation operation .
	 * @param vo {@link Player206VO}
		 * @param bo {@link Player206BO}
		 * @return {@link void}
	 */
	private void privatePlayer206TransferFkPositionVO(Player206VO vo,Player206BO bo )
 throws ApplicationException {
					
		// ServicePosition206
		
		Position206BO relBO = new Position206BO(); 
		Position206BO resultBO = null;
		Position206VO
			relVO = vo.getPositionVO();
		if (relVO!=null && relVO.getCode() !=null){
			relBO.setCode(relVO.getCode());
			resultBO=instanceServicePosition206.position206FindByID(relBO);
		}
			bo.setPosition(resultBO);
	}

		}
