package uc54;

import org.junit.Test;
import test.common.AbstractBADocTestCase;

public class Add0 extends AbstractBADocTestCase {
	
	@Override
	public  String getName(){
		return "UC-54";
	}	


	@Test
	public void add0() throws Exception {
		// On teste l'UC54 - Standard Features/Services/Process - For
		
		selenium.open("");	
		selenium.click("link=Expand All");
		selenium.waitForPageToLoad("30000");
		selenium.click("link=UC54 - For");

		verifyTrue(selenium.isElementPresent("//h2[contains(text(),\"Standard Features > Services > Process > UC54 > For\")]"));
		verifyTrue(selenium.isElementPresent("//h4[contains(text(),\"List of players\")]"));
		verifyTrue(selenium.isElementPresent("//h4[contains(text(),\"Players selected for the team\")]"));
		selenium.click("//*[contains(@id,'lnk_showPlayers')]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_PlayersList')]//thead//tr[th[contains(text(),\"Last Name\")] and th[contains(text(),\"First Name\")] and th[contains(text(),\"Estimated Value (M$)\")] and th[contains(text(),\"Rookie\")] and th[contains(text(),\"Add to team\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_TeamPlayersList')]/thead//tr[th[contains(text(),\"Last Name\")] and th[contains(text(),\"First Name\")] and th[contains(text(),\"Estimated Value (M$)\")] and th[contains(text(),\"Rookie\")]]"));
		// On vérifie que les joueurs s'affichent correctement.
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_PlayersList')]/tbody//tr[td//span[contains(text(),\"Jack\")] and td//span[contains(text(),\"White\")] and td//span[contains(text(),\"25.0\")] and td//input[not(@checked)]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_PlayersList')]/tbody//tr[td//span[contains(text(),\"Robert\")] and td//span[contains(text(),\"Plant\")] and td//span[contains(text(),\"43.0\")] and td//input[not(@checked)]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_PlayersList')]/tbody//tr[td//span[contains(text(),\"Jim\")] and td//span[contains(text(),\"Morrisson\")] and td//span[contains(text(),\"38.0\")] and td//input[not(@checked)]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_PlayersList')]/tbody//tr[td//span[contains(text(),\"Roger\")] and td//span[contains(text(),\"Waters\")] and td//span[contains(text(),\"32.0\")] and td//input[@checked]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_PlayersList')]/tbody//tr[td//span[contains(text(),\"Trent\")] and td//span[contains(text(),\"Reznor\")] and td//span[contains(text(),\"29.0\")] and td//input[@checked]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_PlayersList')]/tbody//tr[td//span[contains(text(),\"Howard\")] and td//span[contains(text(),\"Lovecraft\")] and td//span[contains(text(),\"51.0\")] and td//input[not(@checked)]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_PlayersList')]/tbody//tr[td//span[contains(text(),\"Dan\")] and td//span[contains(text(),\"Simmons\")] and td//span[contains(text(),\"27.0\")] and td//input[@checked]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_PlayersList')]/tbody//tr[td//span[contains(text(),\"Stanley\")] and td//span[contains(text(),\"Kubrick\")] and td//span[contains(text(),\"16.0\")] and td//input[@checked]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_PlayersList')]/tbody//tr[td//span[contains(text(),\"Tim\")] and td//span[contains(text(),\"Burton\")] and td//span[contains(text(),\"14.0\")] and td//input[not(@checked)]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_PlayersList')]/tbody//tr[td//span[contains(text(),\"Isaac\")] and td//span[contains(text(),\"Asimov\")] and td//span[contains(text(),\"20.0\")] and td//input[@checked]]"));
		// On n'en sélectionne aucun.
		// On vérifie qu'aucune ligne n'existe dans tab_TeamPlayersList
		selenium.click("//*[contains(@id,'lnk_selectPlayers')]");
		selenium.waitForPageToLoad("30000");
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_TeamPlayersList')]/tbody//tr[td//span[contains(text(),\"Jack\")] and td//span[contains(text(),\"White\")] and td//span[contains(text(),\"25.0\")] and td//span[contains(text(),\"false\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_TeamPlayersList')]/tbody//tr[td//span[contains(text(),\"Robert\")] and td//span[contains(text(),\"Plant\")] and td//span[contains(text(),\"43.0\")] and td//span[contains(text(),\"false\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_TeamPlayersList')]/tbody//tr[td//span[contains(text(),\"Jim\")] and td//span[contains(text(),\"Morrisson\")] and td//span[contains(text(),\"38.0\")] and td//span[contains(text(),\"false\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_TeamPlayersList')]/tbody//tr[td//span[contains(text(),\"Roger\")] and td//span[contains(text(),\"Waters\")] and td//span[contains(text(),\"32.0\")] and td//span[contains(text(),\"true\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_TeamPlayersList')]/tbody//tr[td//span[contains(text(),\"Trent\")] and td//span[contains(text(),\"Reznor\")] and td//span[contains(text(),\"29.0\")] and td//span[contains(text(),\"true\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_TeamPlayersList')]/tbody//tr[td//span[contains(text(),\"Howard\")] and td//span[contains(text(),\"Lovecraft\")] and td//span[contains(text(),\"51.0\")] and td//span[contains(text(),\"false\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_TeamPlayersList')]/tbody//tr[td//span[contains(text(),\"Dan\")] and td//span[contains(text(),\"Simmons\")] and td//span[contains(text(),\"27.0\")] and td//span[contains(text(),\"true\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_TeamPlayersList')]/tbody//tr[td//span[contains(text(),\"Stanley\")] and td//span[contains(text(),\"Kubrick\")] and td//span[contains(text(),\"16.0\")] and td//span[contains(text(),\"true\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_TeamPlayersList')]/tbody//tr[td//span[contains(text(),\"Tim\")] and td//span[contains(text(),\"Burton\")] and td//span[contains(text(),\"14.0\")] and td//span[contains(text(),\"false\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_TeamPlayersList')]/tbody//tr[td//span[contains(text(),\"Isaac\")] and td//span[contains(text(),\"Asimov\")] and td//span[contains(text(),\"20.0\")] and td//span[contains(text(),\"true\")]]"));
	}


}
