package uc39;


import org.junit.Test;
import test.common.AbstractBADocTestCase;


public class ITuc39 extends AbstractBADocTestCase {
	
	@Override
	public  String getName(){
		return "UC-39";
	}
	
	@Test
	public void testITuc39() throws Exception {
		// On teste le Repeater Blu age - Standard Features/Application - Repeater
		selenium.open("");
		selenium.click("link=Expand All");
		selenium.waitForPageToLoad("30000");
		selenium.click("link=UC39 - Repeater");
		verifyTrue(selenium.isElementPresent("//h2[contains(text(),\"Standard Features > Application > Repeater > UC39 > Repeater\")]"));
		//  On charge les �quipes.
		selenium.click("//*[contains(@id,'lnk_loadTeams')]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie que les bonnes informations sont pr�sentes.
		verifyTrue(selenium.isElementPresent("//h2[contains(text(),\"Standard Features > Application > Repeater > UC39 > Repeater\")]"));
		// Pour Simple Use :
		verifyTrue(selenium.isElementPresent("//h4[contains(text(),'Regular Use')]"));
		assertTrue(selenium.isTextPresent(""));
		assertTrue(selenium.isElementPresent("//form/b[contains(text(),\"Tigers\")]|//form/b//span[contains(text(),\"Tigers\")]"));
		assertTrue(selenium.isTextPresent(""));
		assertTrue(selenium.isElementPresent("//form/b[contains(text(),\"Chargers\")]|//form/b//span[contains(text(),\"Chargers\")]"));
		assertTrue(selenium.isTextPresent(""));
		assertTrue(selenium.isElementPresent("//form/b[contains(text(),\"Bears\")]|//form/b//span[contains(text(),\"Bears\")]"));
		assertTrue(selenium.isTextPresent(""));
		assertTrue(selenium.isElementPresent("//form/b[contains(text(),\"Hawks\")]|//form/b//span[contains(text(),\"Hawks\")]"));
		assertTrue(selenium.isTextPresent(""));
		assertTrue(selenium.isElementPresent("//form/b[contains(text(),\"Braves\")]|//form/b//span[contains(text(),\"Braves\")]"));
		assertTrue(selenium.isTextPresent(""));
		assertTrue(selenium.isElementPresent("//form/b[contains(text(),\"Eagles\")]|//form/b//span[contains(text(),\"Eagles\")]"));
		assertTrue(selenium.isTextPresent(""));
		assertTrue(selenium.isElementPresent("//form/b[contains(text(),\"Cardinals\")]|//form/b//span[contains(text(),\"Cardinals\")]"));
		assertTrue(selenium.isTextPresent(""));
		assertTrue(selenium.isElementPresent("//form/b[contains(text(),\"Red Devils\")]|//form/b//span[contains(text(),\"Red Devils\")]"));
		assertTrue(selenium.isTextPresent(""));
		assertTrue(selenium.isElementPresent("//form/b[contains(text(),\"Mustangs\")]|//form/b//span[contains(text(),\"Mustangs\")]"));
		assertTrue(selenium.isTextPresent(""));
		assertTrue(selenium.isElementPresent("//form/b[contains(text(),\"Wolverines\")]|//form/b//span[contains(text(),\"Wolverines\")]"));
		assertTrue(selenium.isTextPresent(""));
		assertTrue(selenium.isElementPresent("//form/b[contains(text(),\"Crusaders\")]|//form/b//span[contains(text(),\"Crusaders\")]"));
		assertTrue(selenium.isTextPresent(""));
		assertTrue(selenium.isElementPresent("//form/b[contains(text(),\"Trojans\")]|//form/b//span[contains(text(),\"Trojans\")]"));
		assertTrue(selenium.isTextPresent(""));
		assertTrue(selenium.isElementPresent("//form/b[contains(text(),\"Vikings\")]|//form/b//span[contains(text(),\"Vikings\")]"));
		assertTrue(selenium.isTextPresent(""));
		assertTrue(selenium.isElementPresent("//form/b[contains(text(),\"Blue Devils\")]|//form/b//span[contains(text(),\"Blue Devils\")]"));
		assertTrue(selenium.isTextPresent(""));
		assertTrue(selenium.isElementPresent("//form/b[contains(text(),\"Mustangs\")]|//form/b//span[contains(text(),\"Mustangs\")]"));
		assertTrue(selenium.isTextPresent(""));
		assertTrue(selenium.isElementPresent("//form/b[contains(text(),\"Rockets\")]|//form/b//span[contains(text(),\"Rockets\")]"));
		assertTrue(selenium.isTextPresent(""));
		assertTrue(selenium.isElementPresent("//form/b[contains(text(),\"Trojans\")]|//form/b//span[contains(text(),\"Trojans\")]"));
		assertTrue(selenium.isTextPresent(""));
		assertTrue(selenium.isElementPresent("//form/b[contains(text(),\"Braves\")]|//form/b//span[contains(text(),\"Braves\")]"));
		assertTrue(selenium.isTextPresent(""));
		assertTrue(selenium.isElementPresent("//form/b[contains(text(),\"Spartans\")]|//form/b//span[contains(text(),\"Spartans\")]"));
		assertTrue(selenium.isTextPresent(""));
		assertTrue(selenium.isElementPresent("//form/b[contains(text(),\"Spartans\")]|//form/b//span[contains(text(),\"Spartans\")]"));
		// Pour Table Repeater :
		verifyTrue(selenium.isElementPresent("//h4[contains(text(),'Table Repeater')]"));
		assertTrue(selenium.isElementPresent("//table/tbody[tr/th[contains(text(),'Team Name')] and tr/th[contains(text(),'City')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td[contains(text(),'Charlotte')] and td[contains(text(),'Tigers')]]|//table/tbody/tr[td/span[contains(text(),'Charlotte')] and td/span[contains(text(),'Tigers')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td[contains(text(),'Detroit')] and td[contains(text(),'Chargers')]]|//table/tbody/tr[td/span[contains(text(),'Detroit')] and td/span[contains(text(),'Chargers')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td[contains(text(),'Kansas City')] and td[contains(text(),'Bears')]]|//table/tbody/tr[td/span[contains(text(),'Kansas City')] and td/span[contains(text(),'Bears')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td[contains(text(),'Detroit')] and td[contains(text(),'Hawks')]]|//table/tbody/tr[td/span[contains(text(),'Detroit')] and td/span[contains(text(),'Hawks')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td[contains(text(),'Omaha')] and td[contains(text(),'Braves')]]|//table/tbody/tr[td/span[contains(text(),'Omaha')] and td/span[contains(text(),'Braves')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td[contains(text(),'Raleigh')] and td[contains(text(),'Eagles')]]|//table/tbody/tr[td/span[contains(text(),'Raleigh')] and td/span[contains(text(),'Eagles')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td[contains(text(),'Oakland')] and td[contains(text(),'Cardinals')]]|//table/tbody/tr[td/span[contains(text(),'Oakland')] and td/span[contains(text(),'Cardinals')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td[contains(text(),'Kansas City')] and td[contains(text(),'Red Devils')]]|//table/tbody/tr[td/span[contains(text(),'Kansas City')] and td/span[contains(text(),'Red Devils')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td[contains(text(),'Fresno')] and td[contains(text(),'Mustangs')]]|//table/tbody/tr[td/span[contains(text(),'Fresno')] and td/span[contains(text(),'Mustangs')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td[contains(text(),'Sacramento')] and td[contains(text(),'Wolverines')]]|//table/tbody/tr[td/span[contains(text(),'Sacramento')] and td/span[contains(text(),'Wolverines')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td[contains(text(),'Nashville')] and td[contains(text(),'Crusaders')]]|//table/tbody/tr[td/span[contains(text(),'Nashville')] and td/span[contains(text(),'Crusaders')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td[contains(text(),'Fort Worth')] and td[contains(text(),'Trojans')]]|//table/tbody/tr[td/span[contains(text(),'Fort Worth')] and td/span[contains(text(),'Trojans')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td[contains(text(),'Tulsa')] and td[contains(text(),'Vikings')]]|//table/tbody/tr[td/span[contains(text(),'Tulsa')] and td/span[contains(text(),'Vikings')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td[contains(text(),'Cleveland')] and td[contains(text(),'Mustangs')]]|//table/tbody/tr[td/span[contains(text(),'Cleveland')] and td/span[contains(text(),'Mustangs')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td[contains(text(),'Charlotte')] and td[contains(text(),'Blue Devils')]]|//table/tbody/tr[td/span[contains(text(),'Charlotte')] and td/span[contains(text(),'Blue Devils')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td[contains(text(),'Milwaukee')] and td[contains(text(),'Rockets')]]|//table/tbody/tr[td/span[contains(text(),'Milwaukee')] and td/span[contains(text(),'Rockets')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td[contains(text(),'San Jose')] and td[contains(text(),'Trojans')]]|//table/tbody/tr[td/span[contains(text(),'San Jose')] and td/span[contains(text(),'Trojans')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td[contains(text(),'San Antonio')] and td[contains(text(),'Braves')]]|//table/tbody/tr[td/span[contains(text(),'San Antonio')] and td/span[contains(text(),'Braves')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td[contains(text(),'Portland')] and td[contains(text(),'Spartans')]]|//table/tbody/tr[td/span[contains(text(),'Portland')] and td/span[contains(text(),'Spartans')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td[contains(text(),'Tucson')] and td[contains(text(),'Spartans')]]|//table/tbody/tr[td/span[contains(text(),'Tucson')] and td/span[contains(text(),'Spartans')]]"));
		// Pour List Repeater :
		verifyTrue(selenium.isElementPresent("//h4[contains(text(),'List Repeater')]"));
		assertTrue(selenium.isElementPresent("//li[b[contains(text(),'Tigers')]]|//li[b/span[contains(text(),'Tigers')]]"));
		assertTrue(selenium.isElementPresent("//li[b[contains(text(),'Chargers')]]|//li[b/span[contains(text(),'Chargers')]]"));
		assertTrue(selenium.isElementPresent("//li[b[contains(text(),'Bears')]]|//li[b/span[contains(text(),'Bears')]]"));
		assertTrue(selenium.isElementPresent("//li[b[contains(text(),'Hawks')]]|//li[b/span[contains(text(),'Hawks')]]"));
		assertTrue(selenium.isElementPresent("//li[b[contains(text(),'Braves')]]|//li[b/span[contains(text(),'Braves')]]"));
		assertTrue(selenium.isElementPresent("//li[b[contains(text(),'Eagles')]]|//li[b/span[contains(text(),'Eagles')]]"));
		assertTrue(selenium.isElementPresent("//li[b[contains(text(),'Cardinals')]]|//li[b/span[contains(text(),'Cardinals')]]"));
		assertTrue(selenium.isElementPresent("//li[b[contains(text(),'Red Devils')]]|//li[b/span[contains(text(),'Red Devils')]]"));
		assertTrue(selenium.isElementPresent("//li[b[contains(text(),'Mustangs')]]|//li[b/span[contains(text(),'Mustangs')]]"));
		assertTrue(selenium.isElementPresent("//li[b[contains(text(),'Wolverines')]]|//li[b/span[contains(text(),'Wolverines')]]"));
		assertTrue(selenium.isElementPresent("//li[b[contains(text(),'Crusaders')]]|//li[b/span[contains(text(),'Crusaders')]]"));
		assertTrue(selenium.isElementPresent("//li[b[contains(text(),'Vikings')]]|//li[b/span[contains(text(),'Vikings')]]"));
		assertTrue(selenium.isElementPresent("//li[b[contains(text(),'Blue Devils')]]|//li[b/span[contains(text(),'Blue Devils')]]"));
		assertTrue(selenium.isElementPresent("//li[b[contains(text(),'Mustangs')]]|//li[b/span[contains(text(),'Mustangs')]]"));
		assertTrue(selenium.isElementPresent("//li[b[contains(text(),'Rockets')]]|//li[b/span[contains(text(),'Rockets')]]"));
		assertTrue(selenium.isElementPresent("//li[b[contains(text(),'Trojans')]]|//li[b/span[contains(text(),'Trojans')]]"));
		assertTrue(selenium.isElementPresent("//li[b[contains(text(),'Braves')]]|//li[b/span[contains(text(),'Braves')]]"));
		assertTrue(selenium.isElementPresent("//li[b[contains(text(),'Spartans')]]|//li[b/span[contains(text(),'Spartans')]]"));
		assertTrue(selenium.isElementPresent("//li[b[contains(text(),'Spartans')]]|//li[b/span[contains(text(),'Spartans')]]"));
	}

	
}
