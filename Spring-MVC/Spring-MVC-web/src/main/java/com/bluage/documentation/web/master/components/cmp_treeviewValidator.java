/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.master.components ;


import org.apache.log4j.Logger;
import org.springframework.validation.Errors;

import com.netfective.bluage.core.validator.AbstractValidator;
/**
* Class :cmp_treeviewValidator
*/
public class cmp_treeviewValidator extends AbstractValidator{
	
	// LOGGER for the class cmp_treeviewValidator
	private static final Logger LOGGER = Logger.getLogger( cmp_treeviewValidator.class);
	
	
	/**
	* Operation validate for cmp_treeviewForm
	* @param obj : the current form (cmp_treeviewForm)
	* @param errors : The spring errors to return for the form cmp_treeviewForm
	*/
	public void validate(final Object obj,final Errors errors){
		LOGGER.info("Beginning method : validate"); // cmp_treeviewValidator
		cmp_treeviewForm ccmp_treeviewForm = (cmp_treeviewForm)obj; // cmp_treeviewValidator
		LOGGER.info("Ending method : validate the form "+ ccmp_treeviewForm.getClass().getName()); // cmp_treeviewValidator
	}

	/**
	* Method to implements to use spring validators (cmp_treeviewForm)
	* @param aClass : Class for the form cmp_treeviewForm
	* @return boolean
	*/
	public boolean supports(final Class aClass){
		return (new cmp_treeviewForm()).getClass().equals(aClass); // cmp_treeviewValidator
	}
}
