package uc44;

import org.junit.Test;
import test.common.AbstractBADocTestCase;

public class ITuc44 extends AbstractBADocTestCase {
	
	@Override
	public  String getName(){
		return "UC-44";
	}

	@Test
	public void testITuc44() throws Exception {
		// On teste l'UC44 - Standard Features/Application/From - Auto Complete Table.
		selenium.open("");
		selenium.click("link=Expand All");
		selenium.waitForPageToLoad("30000");
		selenium.click("link=UC44 - Auto Complete Table");
		assertTrue(selenium.isElementPresent("//h2[contains(text(),'Standard Features > Application > Form > UC44 > Auto Complete Table')]"));
		// On clique sur le lien [Display Players]
		selenium.click("//a[contains(text(),'[Display Players]')]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie l'affichage de la page :
		assertTrue(selenium.isElementPresent("//h2[contains(text(),'Standard Features > Application > Form > UC44 > Auto Complete Table')]"));
		// On v�rifie les donn�es du tableau :
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/thead/tr[th[contains(text(),'Name')] and th[contains(text(),'Date of Birth')] and th[contains(text(),'Weight (kg)')] and th[contains(text(),'Estimated Value (M$)')] ]"));
		verifyEquals("12", selenium.getXpathCount("//table[contains(@id,'tab_players')]/tbody/tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Owen\")] and td//span[contains(text(),\"Little\")] and td//span[contains(text(),\"10/27/1981\")] and td//span[contains(text(),\"183\")] and td//span[contains(text(),\"135\")] and td//span[contains(text(),\"45.87\")] and td//span[contains(text(),\"Braves\")] and td//a[contains(text(),\"[edit]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Irvin\")] and td//span[contains(text(),\"Lee\")] and td//span[contains(text(),\"02/25/1988\")] and td//span[contains(text(),\"157\")] and td//span[contains(text(),\"88\")] and td//span[contains(text(),\"10.61\")] and td//span[contains(text(),\"Tigers\")] and td//a[contains(text(),\"[edit]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Wayne\")] and td//span[contains(text(),\"Rose\")] and td//span[contains(text(),\"10/28/1981\")] and td//span[contains(text(),\"244\")] and td//span[contains(text(),\"115\")] and td//span[contains(text(),\"20.41\")] and td//span[contains(text(),\"Tigers\")] and td//a[contains(text(),\"[edit]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Lucas\")] and td//span[contains(text(),\"Williamson\")] and td//span[contains(text(),\"06/29/1983\")] and td//span[contains(text(),\"151\")] and td//span[contains(text(),\"73\")] and td//span[contains(text(),\"44.36\")] and td//span[contains(text(),\"Tigers\")] and td//a[contains(text(),\"[edit]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Walter\")] and td//span[contains(text(),\"Reyes\")] and td//span[contains(text(),\"12/09/1984\")] and td//span[contains(text(),\"193\")] and td//span[contains(text(),\"104\")] and td//span[contains(text(),\"46.15\")] and td//span[contains(text(),\"Tigers\")] and td//a[contains(text(),\"[edit]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Kirk\")] and td//span[contains(text(),\"Cox\")] and td//span[contains(text(),\"05/10/1973\")] and td//span[contains(text(),\"179\")] and td//span[contains(text(),\"60\")] and td//span[contains(text(),\"26.58\")] and td//span[contains(text(),\"Chargers\")] and td//a[contains(text(),\"[edit]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Arthur\")] and td//span[contains(text(),\"Payne\")] and td//span[contains(text(),\"11/20/1981\")] and td//span[contains(text(),\"190\")] and td//span[contains(text(),\"66\")] and td//span[contains(text(),\"30.86\")] and td//span[contains(text(),\"Braves\")] and td//a[contains(text(),\"[edit]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Leroy\")] and td//span[contains(text(),\"Moore\")] and td//span[contains(text(),\"07/26/1987\")] and td//span[contains(text(),\"183\")] and td//span[contains(text(),\"89\")] and td//span[contains(text(),\"45.8\")] and td//span[contains(text(),\"Bears\")] and td//a[contains(text(),\"[edit]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Milton\")] and td//span[contains(text(),\"Johnson\")] and td//span[contains(text(),\"07/02/1969\")] and td//span[contains(text(),\"205\")] and td//span[contains(text(),\"68\")] and td//span[contains(text(),\"22.22\")] and td//span[contains(text(),\"Braves\")] and td//a[contains(text(),\"[edit]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Sammy\")] and td//span[contains(text(),\"Mendez\")] and td//span[contains(text(),\"07/01/1986\")] and td//span[contains(text(),\"234\")] and td//span[contains(text(),\"96\")] and td//span[contains(text(),\"2.27\")] and td//span[contains(text(),\"Bears\")] and td//a[contains(text(),\"[edit]\")]]")); 
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Rafael\")] and td//span[contains(text(),\"Steele\")] and td//span[contains(text(),\"11/25/1968\")] and td//span[contains(text(),\"153\")] and td//span[contains(text(),\"66\")] and td//span[contains(text(),\"15.48\")] and td//span[contains(text(),\"Tigers\")] and td//a[contains(text(),\"[edit]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Bob\")] and td//span[contains(text(),\"Jacobs\")] and td//span[contains(text(),\"05/21/1988\")] and td//span[contains(text(),\"172\")] and td//span[contains(text(),\"72\")] and td//span[contains(text(),\"13.23\")] and td//span[contains(text(),\"Bears\")] and td//a[contains(text(),\"[edit]\")]]"));
		// On �dite la ligne d'Owen Little :
		// On indexe la ligne de ce joueur :
		Number index = selenium.getElementIndex("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Owen\")] and td/span[contains(text(),\"Little\")]]");
		System.out.println("variable index vaut : " + index);
		// On clique sur [edit]
		selenium.click("//table[contains(@id,'tab_players')]/tbody/tr[" + index + " + 1]/td/a[contains(text(),\"[edit]\")]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie la pr�sence des messages d'erreur :
		selenium.type("//input[contains(@id,'txt_lastname')]", "");
		selenium.type("//input[contains(@id,'txt_firstname')]", "");
		selenium.type("//input[contains(@id,'txt_dateofbirth')]", "");
		selenium.click("//table[contains(@id,'tab_players')]/tbody/tr[" + index + " + 1]/td/a[contains(text(),\"[update]\")]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//span[contains(text(),'Last name is required.')]"));
		assertTrue(selenium.isElementPresent("//span[contains(text(),'First name is required.')]"));
		assertTrue(selenium.isElementPresent("//span[contains(text(),'Date of Birth is required.')]"));
		selenium.type("//input[contains(@id,'txt_lastname')]", "Powell");
		selenium.type("//input[contains(@id,'txt_firstname')]", "Fred");
		selenium.type("//input[contains(@id,'txt_dateofbirth')]", "25/12/1986");
		selenium.click("//table[contains(@id,'tab_players')]/tbody/tr[" + index + " + 1]/td/a[contains(text(),\"[update]\")]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//span[contains(text(),'Invalid format.')]"));
		selenium.type("//input[contains(@id,'txt_dateofbirth')]", "12/25/1986");
		// On teste l'auto complete Teams :
//		selenium.type("//input[contains(@id,'txt_teamName')]", "B");
		selenium.typeKeys("//input[contains(@id,'txt_teamName')]", "B");
		selenium.focus("//input[contains(@id,'txt_teamName')]");
		Thread.sleep(1000);
		verifyEquals("2", selenium.getXpathCount("//table[contains(@id,'suggest')]/tbody/tr|//div[contains(@id,'txt_teamName')]//li[@style='']"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'suggest')]/tbody/tr/td[contains(text(),'Bears')]|//div[contains(@id,'txt_teamName_div[0]')]/div/div/ul/li[1][contains(text(),'Bears')]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'suggest')]/tbody/tr/td[contains(text(),'Braves')]|//div[contains(@id,'txt_teamName_div[0]')]/div/div/ul/li[2][contains(text(),'Braves')]"));
		selenium.mouseOver("//table[contains(@id,'suggest')]/tbody/tr/td[contains(text(),'Braves')]|//div[contains(@id,'txt_teamName_div[0]')]/div/div/ul/li[2][contains(text(),'Braves')]");
		selenium.click("//table[contains(@id,'suggest')]/tbody/tr/td[contains(text(),'Braves')]|//div[contains(@id,'txt_teamName_div[0]')]/div/div/ul/li[2][contains(text(),'Braves')]");
		selenium.type("//input[contains(@id,'txt_teamName')]", "");
		selenium.typeKeys("//input[contains(@id,'txt_teamName')]", "C");
		selenium.focus("//input[contains(@id,'txt_teamName')]");
		Thread.sleep(1000);
		verifyEquals("1", selenium.getXpathCount("//table[contains(@id,'suggest')]/tbody/tr|//div[contains(@id,'txt_teamName')]//li[@style='']"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'suggest')]/tbody/tr/td[contains(text(),'Chargers')]|//div[contains(@id,'txt_teamName_div[0]')]/div/div/ul/li[1][contains(text(),'Chargers')]"));
		selenium.mouseOver("//table[contains(@id,'suggest')]/tbody/tr/td[contains(text(),'Chargers')]|//div[contains(@id,'txt_teamName_div[0]')]/div/div/ul/li[1][contains(text(),'Chargers')]");
		selenium.click("//table[contains(@id,'suggest')]/tbody/tr/td[contains(text(),'Chargers')]|//div[contains(@id,'txt_teamName_div[0]')]/div/div/ul/li[1][contains(text(),'Chargers')]");
		selenium.type("//input[contains(@id,'txt_teamName')]", "");
		selenium.typeKeys("//input[contains(@id,'txt_teamName')]", "H");
		selenium.focus("//input[contains(@id,'txt_teamName')]");
		Thread.sleep(1000);
		verifyEquals("1", selenium.getXpathCount("//table[contains(@id,'suggest')]/tbody/tr|//div[contains(@id,'txt_teamName')]//li[@style='']"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'suggest')]/tbody/tr/td[contains(text(),'Hawks')]|//div[contains(@id,'txt_teamName_div[0]')]/div/div/ul/li[1][contains(text(),'Hawks')]"));
		selenium.mouseOver("//table[contains(@id,'suggest')]/tbody/tr/td[contains(text(),'Hawks')]|//div[contains(@id,'txt_teamName_div[0]')]/div/div/ul/li[1][contains(text(),'Hawks')]");
		selenium.click("//table[contains(@id,'suggest')]/tbody/tr/td[contains(text(),'Hawks')]|//div[contains(@id,'txt_teamName_div[0]')]/div/div/ul/li[1][contains(text(),'Hawks')]");
		selenium.type("//input[contains(@id,'txt_teamName')]", "");
		selenium.typeKeys("//input[contains(@id,'txt_teamName')]", "Z");
		selenium.focus("//input[contains(@id,'txt_teamName')]");
		Thread.sleep(1000);
		verifyEquals("0", selenium.getXpathCount("//table[contains(@id,'suggest')]/tbody/tr|//div[contains(@id,'txt_teamName')]//li[@style='']"));
		selenium.type("//input[contains(@id,'txt_teamName')]", "");
		selenium.typeKeys("//input[contains(@id,'txt_teamName')]", "T");
		selenium.focus("//input[contains(@id,'txt_teamName')]");
		Thread.sleep(1000);
		verifyEquals("1", selenium.getXpathCount("//table[contains(@id,'suggest')]/tbody/tr|//div[contains(@id,'txt_teamName')]//li[@style='']"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'suggest')]/tbody/tr/td[contains(text(),'Tigers')]|//div[contains(@id,'txt_teamName_div[0]')]/div/div/ul/li[1][contains(text(),'Tigers')]"));
		selenium.mouseOver("//table[contains(@id,'suggest')]/tbody/tr/td[contains(text(),'Tigers')]|//div[contains(@id,'txt_teamName_div[0]')]/div/div/ul/li[1][contains(text(),'Tigers')]");
		selenium.click("//table[contains(@id,'suggest')]/tbody/tr/td[contains(text(),'Tigers')]|//div[contains(@id,'txt_teamName_div[0]')]/div/div/ul/li[1][contains(text(),'Tigers')]");
		selenium.click("//table[contains(@id,'tab_players')]/tbody/tr[" + index + " + 1]/td/a[contains(text(),\"[update]\")]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie que la ligne est bien �dit�e :
		verifyEquals("12", selenium.getXpathCount("//table[contains(@id,'suggest')]/tbody/tr|//div[contains(@id,'txt_teamName')]//li[@style='']"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Powell\")] and td//span[contains(text(),\"Fred\")] and td//span[contains(text(),\"12/25/1986\")] and td//span[contains(text(),\"183\")] and td//span[contains(text(),\"135\")] and td//span[contains(text(),\"45.87\")] and td//span[contains(text(),\"Tigers\")] and td//a[contains(text(),\"[edit]\")]]"));
	}


}
