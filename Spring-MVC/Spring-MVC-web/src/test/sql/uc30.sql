-- Table: page30


ALTER TABLE page30 DROP CONSTRAINT page30_pkey;
DROP TABLE page30;

CREATE TABLE page30
(
  id bigint NOT NULL,
  "version" integer NOT NULL,
  title character varying(40),
  CONSTRAINT page30_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);



INSERT INTO page30(
            id, "version", title)
    VALUES (1, 0, 'hello world');

INSERT INTO page30(
            id, "version", title)
    VALUES (2, 0, 'goodbye world');

