/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506

package com.netfective.bluage.core.exception;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
/**
 * La classe represente les differents types d exception system
 * 
 * <DL>
 * <DT><B>Nom du Projet: </B><DD>Bluage</DD>
 * <DT><B>Service: </B>
 * 
 */
public final class ExceptionTypes {

	// ApplicationError codes
	
	public static final String DB_NON_UNIQUE_OBJECT_KEY = "DB_NON_UNIQUE_OBJECT_KEY";
	
	public static final String NULL_KEY_NOT_ALLOWED = "NULL_KEY_NOT_ALLOWED"; 
	
	public static final String DB_OBJECT_DELETED_KEY = "DB_OBJECT_DELETED_KEY";
	
	public static final String INVALID_LOGIN_INFO_KEY = "INVALID_LOGIN_INFO_KEY";
	
	public static final String ACTION_NOT_ALLOWED_KEY = "ACTION_NOT_ALLOWED_KEY";
	
	public static final String RESSOURCE_NOT_ALLOWED_KEY = "RESSOURCE_NOT_ALLOWED_KEY";
	
	public static final String CONSTRAINT_VIOLATION = "CONSTRAINT_VIOLATION";
	
	public static final String NULL_COLUMN_NOT_ALLOWED = "NULL_COLUMN_NOT_ALLOWED"; 
	
	public static final String NULL_PARAMETER = "NULL_PARAMETER"; 
	
	public static final String DECLARATIVE_EXCEPTION ="error.application.exception.message";
	
	// Les codes erreurs de la categorie SYSTEM_EXCEPTION
	 /**
	  * Probleme d entree/Sortie
	  */
	 public static final int EXCEPTION_CODE_SYS_IO = 1;

	 /**
	  * Impossible de se connecter a la ressource demandee
	  */
	 public static final int EXCEPTION_CODE_SYS_CON_PB = 2;

	 /**
	  * Probleme d introspection (classes non trouvee,Methode non trouvee...)
	  */
	 public static final int EXCEPTION_CODE_SYS_INTROSPEC_PB = 3;

	 /**
	  * Probleme d hibernate (session ou transaction)
	  */
	 public static final int EXCEPTION_CODE_SYS_HIBERNATE_PB = 4;

	 /**
	  * Impossible de se connecter a la Base de donnees
	  */
	 public static final int EXCEPTION_CODE_SYS_DB_CON_PB = 5;

	 /**
	  * Probleme de driver la Base de donnees
	  */
	 public static final int EXCEPTION_CODE_SYS_DB_DRIVER_PB = 6;

	 /**
	  * Probleme de creation d objet dans la Base de donnees
	  */
	 public static final int EXCEPTION_CODE_SYS_DB_CREAT_PB = 7;

	 /**
	  * Probleme de modification d bjet dans la Base de donnees
	  */
	 public static final int EXCEPTION_CODE_SYS_DB_UPDATE_PB = 8;

	 /**
	  * Probleme de suppression d objet dans la Base de donnees
	  */
	 public static final int EXCEPTION_CODE_SYS_DB_DELETE_PB = 9;
	 
	/**
	  * Probleme de suppression d objet dans la Base de donnees
	  */
	 public static final int EXCEPTION_CODE_SYS_JDOM_PB = 10;
	
	/**
	  * Probleme de requete non trouvee dans queries.properties
	  */
	 public static final int EXCEPTION_CODE_SYS_QUERY_NOT_FOUND = 11; 
	 
	/**
	  * Probleme de mauvaise requete dans queries.properties
	  */
	 public static final int EXCEPTION_CODE_SYS_BAD_QUERY = 12; 
	 
	/**
	  * Probleme du parsing du fichier de mapping
	  */
	 public static final int EXCEPTION_CODE_SYS_MAPPING_PB = 13;
	 
	 /** 
	  * liste des messages d'>exception
	  */
	public static final List<String> EXCEPTION_TYPES = new ArrayList<String>(
			Arrays.asList(DB_NON_UNIQUE_OBJECT_KEY, NULL_KEY_NOT_ALLOWED,
					DB_OBJECT_DELETED_KEY, INVALID_LOGIN_INFO_KEY,
					ACTION_NOT_ALLOWED_KEY, RESSOURCE_NOT_ALLOWED_KEY,
					CONSTRAINT_VIOLATION, NULL_COLUMN_NOT_ALLOWED,
					NULL_PARAMETER, DECLARATIVE_EXCEPTION));
					
	/**
	 * private constructor for ExceptionTypes
	 */
	private ExceptionTypes(){}
}
