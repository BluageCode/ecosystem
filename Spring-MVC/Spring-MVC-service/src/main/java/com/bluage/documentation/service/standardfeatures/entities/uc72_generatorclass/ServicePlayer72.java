/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.service.standardfeatures.entities.uc72_generatorclass;


import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import com.bluage.documentation.business.standardfeatures.entities.uc72_generatorclass.bos.Player72BO;
import com.bluage.documentation.business.standardfeatures.entities.uc72_generatorclass.entities.dao.Player72DAOImpl;
import com.bluage.documentation.business.standardfeatures.entities.uc72_generatorclass.entities.daofinder.Player72DAOFinderImpl;
import com.bluage.documentation.service.AbstractService;
import com.bluage.documentation.service.standardfeatures.entities.uc72_generatorclass.daofinder.ServicePlayer72DAOMethodFinderImpl;
import com.netfective.bluage.core.exception.ApplicationException;

/**
 * Defines  application services for ServicePlayer72
  */
@Named("com.bluage.documentation.service.standardfeatures.entities.uc72_generatorclass.ServicePlayer72")
public class ServicePlayer72  extends AbstractService{


			@Inject
		@Named ("com.bluage.documentation.service.standardfeatures.entities.uc72_generatorclass.daofinder.ServicePlayer72DAOMethodFinder")
		private ServicePlayer72DAOMethodFinderImpl servicePlayer72Dao;
		@Inject
		@Named ("com.bluage.documentation.business.standardfeatures.entities.uc72_generatorclass.entities.daofinder.Player72DAOFinderImpl")
		private Player72DAOFinderImpl player72BOFinder;
						@Inject
		@Named ("com.bluage.documentation.business.standardfeatures.entities.uc72_generatorclass.entities.dao.Player72DAOImpl")
		private Player72DAOImpl player72BODao;
	


	/**
	 *  Default constructor 
	 */
	public ServicePlayer72() {
		super(); // Call super for default constructor : ServicePlayer72
	}
	


			
				
	/**    
	 * Operation Crud
	 * @param bo {@link Player72BO}
		 * @return {@link boolean}
	 */
	public boolean player72Create(Player72BO bo )
 throws ApplicationException {

					return player72BODao.create(bo);
	}

					
				
	/**    
	 * Operation Crud
	 * @param bo {@link Player72BO}
		 * @return {@link boolean}
	 */
	public boolean player72Delete(Player72BO bo )
 throws ApplicationException {

					return player72BODao.delete(bo);
	}

		
								
	/**    
	 * Operation Crud
	 * @return {@link List}
	 */
	public List player72FindAll( )
 throws ApplicationException {

					return player72BOFinder.findAll();
	}

		
			
	/**    
	 * Process operation.
	 * @param playerToCreate {@link Player72BO}
		 * @return {@link void}
	 */
	public void createPlayer72(Player72BO playerToCreate )
 throws ApplicationException {

			privateCreatePlayer72(playerToCreate);
	}
	

	/**    
	 * Process operation.
	 * @param playerToCreate {@link Player72BO}
		 * @return {@link void}
	 */
	private void privateCreatePlayer72(Player72BO playerToCreate )
 throws ApplicationException {
		// Central buffer node declaration for operation createPlayer72
			Player72BO	 player72Found =  new Player72BO();
					Long	 id =  Long.valueOf(0);
					// Calling an operation : Get Id
id = playerToCreate.getId();
// Calling an operation : Search Player
player72Found = searchPlayer72(id);
// Test on exception : 
if (player72Found!=null
) {
ApplicationException app = new ApplicationException("");
throw app;
}
// Calling an operation : Create player 70
player72Create(playerToCreate);

		}

					
				
	/**    
	 * Operation Crud
	 * @param bo {@link Player72BO}
		 * @return {@link Player72BO}
	 */
	public Player72BO player72FindByID(Player72BO bo )
 throws ApplicationException {

					return (Player72BO)player72BOFinder.findByPrimaryKey(getBoId(bo));
	}
	/**
	 *  Return the primary key from business object.
	 */
	private Object getBoId(Player72BO bo) {
				return bo.getId();
		}

		
			
	/**    
	 * Operation HQL.
	 * @param id {@link Long}
		 * @return {@link Player72BO}
	 */
	public Player72BO searchPlayer72(Long id )
 throws ApplicationException {
	
	return servicePlayer72Dao.searchPlayer72(id);
	}

					
				
	/**    
	 * Operation Crud
	 * @param bo {@link Player72BO}
		 * @return {@link boolean}
	 */
	public boolean player72Update(Player72BO bo )
 throws ApplicationException {

					return player72BODao.update(bo);
	}

		}
