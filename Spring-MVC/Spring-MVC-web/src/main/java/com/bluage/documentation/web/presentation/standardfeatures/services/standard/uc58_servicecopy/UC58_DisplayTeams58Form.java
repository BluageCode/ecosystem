/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.services.standard.uc58_servicecopy;


import java.io.Serializable;
import java.util.List;

import com.bluage.documentation.business.standardfeatures.services.standard.uc58_servicecopy.bos.Team58BO;
import com.netfective.bluage.core.web.AbstractForm;
import com.netfective.bluage.core.web.IForm;

/**
* Class : UC58_DisplayTeams58Form
*/
public class UC58_DisplayTeams58Form extends AbstractForm implements Serializable, IForm {
	
	/** Constant : serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	//CONSTANT : allTeams
	private static final String ALL_TEAMS = "allTeams";
	//CONSTANT : copyTeam
	private static final String COPY_TEAM = "copyTeam";
	//CONSTANT : selectedTeam
	private static final String SELECTED_TEAM = "selectedTeam";
	//CONSTANT : teamDetails
	private static final String TEAM_DETAILS = "teamDetails";
	/**
	 * 	Property: allTeams 
	 */
	private List<Team58BO> allTeams;
	/**
	 * 	Property: copyTeam 
	 */
	private Team58BO copyTeam;
	/**
	 * 	Property: selectedTeam 
	 */
	private Team58BO selectedTeam;
	/**
	 * 	Property: teamDetails 
	 */
	private Team58BO teamDetails;
/**
	 * Default constructor : UC58_DisplayTeams58Form
	 */
	public UC58_DisplayTeams58Form() {
		super();
		// Initialize : allTeams
		this.allTeams = new java.util.ArrayList<com.bluage.documentation.business.standardfeatures.services.standard.uc58_servicecopy.bos.Team58BO>();
		// Initialize : copyTeam
		this.copyTeam = null;
		// Initialize : selectedTeam
		this.selectedTeam = null;
		// Initialize : teamDetails
		this.teamDetails = null;
					this.setAlwaysCallPreControllers(true);
	}
	
	/**
	 * 	Getter : allTeams 
	 * 	@return : Return the allTeams instance.
	 */
	public List<Team58BO> getAllTeams(){
		return allTeams; // For UC58_DisplayTeams58Form
	}
	
	/**
	 * 	Setter : allTeams 
	 *  @param allTeamsinstance : The instance to set.
	 */
	public void setAllTeams(final List<Team58BO> allTeamsinstance){
		this.allTeams = allTeamsinstance;// For UC58_DisplayTeams58Form
	}
	/**
	 * 	Getter : copyTeam 
	 * 	@return : Return the copyTeam instance.
	 */
	public Team58BO getCopyTeam(){
		return copyTeam; // For UC58_DisplayTeams58Form
	}
	
	/**
	 * 	Setter : copyTeam 
	 *  @param copyTeaminstance : The instance to set.
	 */
	public void setCopyTeam(final Team58BO copyTeaminstance){
		this.copyTeam = copyTeaminstance;// For UC58_DisplayTeams58Form
	}
	/**
	 * 	Getter : selectedTeam 
	 * 	@return : Return the selectedTeam instance.
	 */
	public Team58BO getSelectedTeam(){
		return selectedTeam; // For UC58_DisplayTeams58Form
	}
	
	/**
	 * 	Setter : selectedTeam 
	 *  @param selectedTeaminstance : The instance to set.
	 */
	public void setSelectedTeam(final Team58BO selectedTeaminstance){
		this.selectedTeam = selectedTeaminstance;// For UC58_DisplayTeams58Form
	}
	/**
	 * 	Getter : teamDetails 
	 * 	@return : Return the teamDetails instance.
	 */
	public Team58BO getTeamDetails(){
		return teamDetails; // For UC58_DisplayTeams58Form
	}
	
	/**
	 * 	Setter : teamDetails 
	 *  @param teamDetailsinstance : The instance to set.
	 */
	public void setTeamDetails(final Team58BO teamDetailsinstance){
		this.teamDetails = teamDetailsinstance;// For UC58_DisplayTeams58Form
	}
/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UC58_DisplayTeams58Form [ "+
ALL_TEAMS +" = " + allTeams +COPY_TEAM +" = " + copyTeam +SELECTED_TEAM +" = " + selectedTeam +TEAM_DETAILS +" = " + teamDetails + "]";
	}
	
	/**
	 * Method setInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public void setInstanceByInstanceName(final String instanceName, final Object instance) {
		// Set the instance AllTeams.
		if(ALL_TEAMS.equals(instanceName)){// For UC58_DisplayTeams58Form
			this.setAllTeams((List<Team58BO>)instance); // For UC58_DisplayTeams58Form
		}
				// Set the instance CopyTeam.
		if(COPY_TEAM.equals(instanceName)){// For UC58_DisplayTeams58Form
			this.setCopyTeam((Team58BO)instance); // For UC58_DisplayTeams58Form
		}
				// Set the instance SelectedTeam.
		if(SELECTED_TEAM.equals(instanceName)){// For UC58_DisplayTeams58Form
			this.setSelectedTeam((Team58BO)instance); // For UC58_DisplayTeams58Form
		}
				// Set the instance TeamDetails.
		if(TEAM_DETAILS.equals(instanceName)){// For UC58_DisplayTeams58Form
			this.setTeamDetails((Team58BO)instance); // For UC58_DisplayTeams58Form
		}
			}
	
	/**
	 * Method getInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public Object getInstanceByInstanceName(final String instanceName){
		// Temporary object for tmpUC58_DisplayTeams58Form.
		Object tmpUC58_DisplayTeams58Form = null;
		
			
		// Get the instance AllTeams for UC58_DisplayTeams58Form.
		if(ALL_TEAMS.equals(instanceName)){ // For UC58_DisplayTeams58Form
			tmpUC58_DisplayTeams58Form = this.getAllTeams(); // For UC58_DisplayTeams58Form
		}
			
		// Get the instance CopyTeam for UC58_DisplayTeams58Form.
		if(COPY_TEAM.equals(instanceName)){ // For UC58_DisplayTeams58Form
			tmpUC58_DisplayTeams58Form = this.getCopyTeam(); // For UC58_DisplayTeams58Form
		}
			
		// Get the instance SelectedTeam for UC58_DisplayTeams58Form.
		if(SELECTED_TEAM.equals(instanceName)){ // For UC58_DisplayTeams58Form
			tmpUC58_DisplayTeams58Form = this.getSelectedTeam(); // For UC58_DisplayTeams58Form
		}
			
		// Get the instance TeamDetails for UC58_DisplayTeams58Form.
		if(TEAM_DETAILS.equals(instanceName)){ // For UC58_DisplayTeams58Form
			tmpUC58_DisplayTeams58Form = this.getTeamDetails(); // For UC58_DisplayTeams58Form
		}
		return tmpUC58_DisplayTeams58Form;// For UC58_DisplayTeams58Form
	}
	
			}
