/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.application.navigation.uc29_globalscope.package_one ;


import org.apache.log4j.Logger;
import org.springframework.validation.Errors;

import com.netfective.bluage.core.validator.AbstractValidator;
/**
* Class :UC29_PlayersValidator
*/
public class UC29_PlayersValidator extends AbstractValidator{
	
	// LOGGER for the class UC29_PlayersValidator
	private static final Logger LOGGER = Logger.getLogger( UC29_PlayersValidator.class);
	
	
	/**
	* Operation validate for UC29_PlayersForm
	* @param obj : the current form (UC29_PlayersForm)
	* @param errors : The spring errors to return for the form UC29_PlayersForm
	*/
	public void validate(final Object obj,final Errors errors){
		LOGGER.info("Beginning method : validate"); // UC29_PlayersValidator
		UC29_PlayersForm cUC29_PlayersForm = (UC29_PlayersForm)obj; // UC29_PlayersValidator
		LOGGER.info("Ending method : validate the form "+ cUC29_PlayersForm.getClass().getName()); // UC29_PlayersValidator
	}

	/**
	* Method to implements to use spring validators (UC29_PlayersForm)
	* @param aClass : Class for the form UC29_PlayersForm
	* @return boolean
	*/
	public boolean supports(final Class aClass){
		return (new UC29_PlayersForm()).getClass().equals(aClass); // UC29_PlayersValidator
	}
}
