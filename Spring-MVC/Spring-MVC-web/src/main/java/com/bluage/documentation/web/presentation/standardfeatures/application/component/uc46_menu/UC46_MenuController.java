/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.application.component.uc46_menu ;

// Import declaration.
// Java imports.
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.bluage.documentation.service.common.utility.ServiceUtility;
import com.netfective.bluage.core.exception.ApplicationException;
import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;
import com.netfective.bluage.core.web.IForm;

/**
 * Class : UC46_MenuController
 */
@Controller
@Scope("prototype")
@SessionAttributes("uC46_MenuForm")
@RequestMapping(value= "/presentation/standardfeatures/application/component/uc46_menu")
public class UC46_MenuController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC46_MenuController.class);
	
	//Current form name
	private static final String U_C46__MENU_FORM = "uC46_MenuForm";

	
	/**
	 * Property:customDateEditorsUC46_MenuController
	 */
	final private Map<String,CustomDateEditor> customDateEditorsUC46_MenuController = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:uC46_MenuValidator
	 */
	final private UC46_MenuValidator uC46_MenuValidator = new UC46_MenuValidator();
	
	/**
	 * Service declaration : serviceUtility.
	 */
	@Autowired
	private ServiceUtility serviceUtility;
	
	
	// Initialise all the instances for UC46_MenuController
			/**
	 * Operation : lnk_goToPage1
 	 * @param model : 
 	 * @param uC46_Menu : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC46_Menu/lnk_goToPage1.html",method = RequestMethod.POST)
	public String lnk_goToPage1(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC46_MenuForm") UC46_MenuForm  uC46_MenuForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : lnk_goToPage1"); 
		infoLogger(uC46_MenuForm); 
		uC46_MenuForm.setAlwaysCallPreControllers(false); 
		// initialization for lnk_goToPage1
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state lnk_goToPage1 if the validation fail, you will be forward to the screen : "/presentation/standardfeatures/application/component/uc46_menu/UC46_Menu"
		if(errorsMessages(request,response,uC46_MenuForm,bindingResult,"", uC46_MenuValidator, customDateEditorsUC46_MenuController)){ 
			return "/presentation/standardfeatures/application/component/uc46_menu/UC46_Menu"; 
		}

	 callGotoPage1(request,uC46_MenuForm );
											// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.application.component.uc46_menu.UC46_Page1Form or not from lnk_goToPage1
			final UC46_Page1Form uC46_Page1Form =  new UC46_Page1Form();
			// Populate the destination form with all the instances returned from lnk_goToPage1.
			final IForm uC46_Page1Form2 = populateDestinationForm(request.getSession(), uC46_Page1Form); 
					
			// Add the form to the model.
			model.addAttribute("uC46_Page1Form", uC46_Page1Form2); 
			
			request.getSession().setAttribute("uC46_Page1Form", uC46_Page1Form2);
			
			// "OK" CASE => destination screen path from lnk_goToPage1
			LOGGER.info("Go to the screen 'UC46_Page1'.");
			// Redirect (PRG) from lnk_goToPage1
			destinationPath =  "redirect:/presentation/standardfeatures/application/component/uc46_menu/UC46_Page1.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_goToPage1 
	}
	
				/**
	 * Operation : lnk_goToPage2
 	 * @param model : 
 	 * @param uC46_Menu : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC46_Menu/lnk_goToPage2.html",method = RequestMethod.POST)
	public String lnk_goToPage2(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC46_MenuForm") UC46_MenuForm  uC46_MenuForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : lnk_goToPage2"); 
		infoLogger(uC46_MenuForm); 
		uC46_MenuForm.setAlwaysCallPreControllers(false); 
		// initialization for lnk_goToPage2
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state lnk_goToPage2 if the validation fail, you will be forward to the screen : "/presentation/standardfeatures/application/component/uc46_menu/UC46_Menu"
		if(errorsMessages(request,response,uC46_MenuForm,bindingResult,"", uC46_MenuValidator, customDateEditorsUC46_MenuController)){ 
			return "/presentation/standardfeatures/application/component/uc46_menu/UC46_Menu"; 
		}

	 callGotoPage2(request,uC46_MenuForm );
											// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.application.component.uc46_menu.UC46_Page2Form or not from lnk_goToPage2
			final UC46_Page2Form uC46_Page2Form =  new UC46_Page2Form();
			// Populate the destination form with all the instances returned from lnk_goToPage2.
			final IForm uC46_Page2Form2 = populateDestinationForm(request.getSession(), uC46_Page2Form); 
					
			// Add the form to the model.
			model.addAttribute("uC46_Page2Form", uC46_Page2Form2); 
			
			request.getSession().setAttribute("uC46_Page2Form", uC46_Page2Form2);
			
			// "OK" CASE => destination screen path from lnk_goToPage2
			LOGGER.info("Go to the screen 'UC46_Page2'.");
			// Redirect (PRG) from lnk_goToPage2
			destinationPath =  "redirect:/presentation/standardfeatures/application/component/uc46_menu/UC46_Page2.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_goToPage2 
	}
	
	
	/**
	* This method initialise the form : UC46_MenuForm 
	* @return UC46_MenuForm
	*/
	@ModelAttribute("UC46_MenuFormInit")
	public void initUC46_MenuForm(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, U_C46__MENU_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method UC46_MenuForm."); //for lnk_goToPage2 
		}
		UC46_MenuForm uC46_MenuForm;
	
		if(request.getSession().getAttribute(U_C46__MENU_FORM) != null){
			uC46_MenuForm = (UC46_MenuForm)request.getSession().getAttribute(U_C46__MENU_FORM);
		} else {
			uC46_MenuForm = new UC46_MenuForm();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form UC46_MenuForm.");
		}
		uC46_MenuForm = (UC46_MenuForm)populateDestinationForm(request.getSession(), uC46_MenuForm);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  UC46_MenuForm.");
		}
		model.addAttribute(U_C46__MENU_FORM, uC46_MenuForm);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param UC46_MenuForm : The sceen form.
	 */
	@RequestMapping(value = "/UC46_Menu.html" ,method = RequestMethod.GET)
	public void prepareUC46_Menu(final HttpServletRequest request,final  Model model,final  @ModelAttribute("UC46_MenuFormInit") UC46_MenuForm uC46_MenuForm){
		
		UC46_MenuForm currentUC46_MenuForm = uC46_MenuForm;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(U_C46__MENU_FORM) == null){
			if(currentUC46_MenuForm!=null){
				request.getSession().setAttribute(U_C46__MENU_FORM, currentUC46_MenuForm);
			}else {
				currentUC46_MenuForm = new UC46_MenuForm();
				request.getSession().setAttribute(U_C46__MENU_FORM, currentUC46_MenuForm);	
			}
		} else {
			currentUC46_MenuForm = (UC46_MenuForm) request.getSession().getAttribute(U_C46__MENU_FORM);
		}

		currentUC46_MenuForm = (UC46_MenuForm)populateDestinationForm(request.getSession(), currentUC46_MenuForm);
		// Call all the Precontroller.
	currentUC46_MenuForm.setAlwaysCallPreControllers(true);
		model.addAttribute(U_C46__MENU_FORM, currentUC46_MenuForm);
		request.getSession().setAttribute(U_C46__MENU_FORM, currentUC46_MenuForm);
		
	}
	
	/**
	 * method callGotoPage1
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callGotoPage1(HttpServletRequest request,IForm form) {
					try {
				// executing GotoPage1 in lnk_goToPage2
	serviceUtility.doNothing(
	);  

								// processing variables GotoPage1 in lnk_goToPage2

			} catch (ApplicationException e) { 
				// error handling for operation doNothing called GotoPage1
				errorLogger("An error occured during the execution of the operation : doNothing",e); 
		
			}
	}
		/**
	 * method callGotoPage2
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callGotoPage2(HttpServletRequest request,IForm form) {
					try {
				// executing GotoPage2 in lnk_goToPage2
	serviceUtility.doNothing(
	);  

								// processing variables GotoPage2 in lnk_goToPage2

			} catch (ApplicationException e) { 
				// error handling for operation doNothing called GotoPage2
				errorLogger("An error occured during the execution of the operation : doNothing",e); 
		
			}
	}
		
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("UC46_MenuController [ ");
		// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(uC46_MenuValidator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsUC46_MenuController!=null); 
		return strBToS.toString();
	}
}
