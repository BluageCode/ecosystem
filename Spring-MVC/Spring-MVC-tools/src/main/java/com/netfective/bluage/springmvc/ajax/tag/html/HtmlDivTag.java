/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.netfective.bluage.springmvc.ajax.tag.html;

import java.io.IOException;
import java.text.MessageFormat;

import javax.servlet.jsp.JspException;

import com.netfective.bluage.springmvc.ajax.tag.TagUtils;
import com.netfective.bluage.springmvc.ajax.util.Constants;

/**
 * Class HtmlDivTag
 * @superclass HtmlTag
 * @interface Constants
 * @author NETFECTIVE TECHNOLOGY
 */
public class HtmlDivTag extends AbstractHtmlTag implements Constants {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 3142847854509567421L;
	
	/**
	* This method prints a start tag on output of <code>pageContext</code>
	* @return int
	*/
	public int doStartTag() throws JspException {
		
		super.doStartTag();
		
		final Object[] arguments = { TagUtils.getRealDivId(id),TagUtils.buildAttributes(getAttributes()) };
		final String balise = MessageFormat.format(START_OF_DIV, arguments);
		
		try {			
			pageContext.getOut().print(balise);
		} catch (IOException e) {
			throw new JspException(e);
		}
		return EVAL_BODY_INCLUDE;
	}

	/**
	* This method prints a end tag on output of <code>pageContext</code>
	* @return int
	*/
	public int doEndTag() throws JspException {
		
		try {
			pageContext.getOut().print(END_OF_DIV);		
		} catch (IOException e) {
			throw new JspException(e);
		}
		return EVAL_PAGE;
	}
}

