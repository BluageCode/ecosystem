/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.application.form.uc43_captcha ;

// Import declaration.
// Java imports.
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.bluage.documentation.business.standardfeatures.application.form.uc43_captcha.bos.Player43BO;
import com.bluage.documentation.business.standardfeatures.application.form.uc43_captcha.entities.daofinder.Player43DAOFinderImpl;
import com.bluage.documentation.business.standardfeatures.application.form.uc43_captcha.entities.daofinder.Position43DAOFinderImpl;
import com.bluage.documentation.service.standardfeatures.application.form.uc43_captcha.ServicePlayer43;
import com.netfective.bluage.core.exception.ApplicationException;
import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;
import com.netfective.bluage.core.web.IForm;
import com.octo.captcha.service.CaptchaServiceException;
import com.octo.captcha.service.image.DefaultManageableImageCaptchaService;

/**
 * Class : UC43_AddPlayerController
 */
@Controller
@Scope("prototype")
@SessionAttributes("uC43_AddPlayerForm")
@RequestMapping(value= "/presentation/standardfeatures/application/form/uc43_captcha")
public class UC43_AddPlayerController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC43_AddPlayerController.class);
	
	//Current form name
	private static final String U_C43__ADD_PLAYER_FORM = "uC43_AddPlayerForm";

		//CONSTANT: listPlayers table or repeater.
	private static final String LIST_PLAYERS = "listPlayers";
				//CONSTANT: allPositions
	private static final String ALL_POSITIONS = "allPositions";
				//CONSTANT: playerToAdd
	private static final String PLAYER_TO_ADD = "playerToAdd";
	//CONSTANT: selectedPlayer
	private static final String SELECTED_PLAYER = "selectedPlayer";
	
	/**
	 * Property:customDateEditorsUC43_AddPlayerController
	 */
	final private Map<String,CustomDateEditor> customDateEditorsUC43_AddPlayerController = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:uC43_AddPlayerValidator
	 */
	final private UC43_AddPlayerValidator uC43_AddPlayerValidator = new UC43_AddPlayerValidator();
	
	@Autowired
	private DefaultManageableImageCaptchaService captchaService;
	/**
	 * Service declaration : servicePlayer43.
	 */
	@Autowired
	private ServicePlayer43 servicePlayer43;
	
	/**
	 * Generic Finder : position43DAOFinderImpl.
	 */
	@Autowired
	private Position43DAOFinderImpl position43DAOFinderImpl;
	
	/**
	 * Generic Finder : player43DAOFinderImpl.
	 */
	@Autowired
	private Player43DAOFinderImpl player43DAOFinderImpl;
	
	
	// Initialise all the instances for UC43_AddPlayerController
			// Initialize the instance listPlayers for the state lnk_edit
		private List listPlayers; // Initialize the instance listPlayers for UC43_AddPlayerController
						// Initialize the instance allPositions for the state lnk_edit
		private List allPositions; // Initialize the instance allPositions for UC43_AddPlayerController
						// Declare the instance playerToAdd
		private Player43BO playerToAdd;
		// Declare the instance selectedPlayer
		private Player43BO selectedPlayer;
			/**
	 * Operation : lnk_add
 	 * @param model : 
 	 * @param uC43_AddPlayer : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC43_AddPlayer/lnk_add.html",method = RequestMethod.POST)
	public String lnk_add(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC43_AddPlayerForm") UC43_AddPlayerForm  uC43_AddPlayerForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : lnk_add"); 
		infoLogger(uC43_AddPlayerForm); 
		uC43_AddPlayerForm.setAlwaysCallPreControllers(false); 
		// initialization for lnk_add
		init();
		
		String destinationPath = null; 
		

		validateCaptcha(uC43_AddPlayerForm, bindingResult, request, "Captcha validation failed.");
										// Calling the validators for the state lnk_add if the validation fail, you will be forward to the screen : "/presentation/standardfeatures/application/form/uc43_captcha/UC43_AddPlayer"
		if(errorsMessages(request,response,uC43_AddPlayerForm,bindingResult,"", uC43_AddPlayerValidator, customDateEditorsUC43_AddPlayerController)){ 
			return "/presentation/standardfeatures/application/form/uc43_captcha/UC43_AddPlayer"; 
		}

					 callCreateplayer(request,uC43_AddPlayerForm );
						// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.application.form.uc43_captcha.UC43_AddPlayerForm or not from lnk_add
			// Populate the destination form with all the instances returned from lnk_add.
			final IForm uC43_AddPlayerForm2 = populateDestinationForm(request.getSession(), uC43_AddPlayerForm); 
					
			// Add the form to the model.
			model.addAttribute("uC43_AddPlayerForm", uC43_AddPlayerForm2); 
			
			request.getSession().setAttribute("uC43_AddPlayerForm", uC43_AddPlayerForm2);
			
			// "OK" CASE => destination screen path from lnk_add
			LOGGER.info("Go to the screen 'UC43_AddPlayer'.");
			// Redirect (PRG) from lnk_add
			destinationPath =  "redirect:/presentation/standardfeatures/application/form/uc43_captcha/UC43_AddPlayer.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_add 
	}
	
									/**
	 * Operation : lnk_delete
 	 * @param model : 
 	 * @param uC43_AddPlayer : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC43_AddPlayer/lnk_delete.html",method = RequestMethod.POST)
	public String lnk_delete(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC43_AddPlayerForm") UC43_AddPlayerForm  uC43_AddPlayerForm, final BindingResult bindingResult ,final @RequestParam(ID_REQUEST_PARAM) Integer index){
		
		
		
		LOGGER.info("Begin method : lnk_delete"); 
		infoLogger(uC43_AddPlayerForm); 
		uC43_AddPlayerForm.setAlwaysCallPreControllers(false); 
		// initialization for lnk_delete
		init();
		
		String destinationPath = null; 
		

										 callDeleteplayer(request,uC43_AddPlayerForm , index);
						// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.application.form.uc43_captcha.UC43_AddPlayerForm or not from lnk_delete
			// Populate the destination form with all the instances returned from lnk_delete.
			final IForm uC43_AddPlayerForm2 = populateDestinationForm(request.getSession(), uC43_AddPlayerForm); 
					
			// Add the form to the model.
			model.addAttribute("uC43_AddPlayerForm", uC43_AddPlayerForm2); 
			
			request.getSession().setAttribute("uC43_AddPlayerForm", uC43_AddPlayerForm2);
			
			// "OK" CASE => destination screen path from lnk_delete
			LOGGER.info("Go to the screen 'UC43_AddPlayer'.");
			// Redirect (PRG) from lnk_delete
			destinationPath =  "redirect:/presentation/standardfeatures/application/form/uc43_captcha/UC43_AddPlayer.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_delete 
	}
	
	
	/**
	* This method initialise the form : UC43_AddPlayerForm 
	* @return UC43_AddPlayerForm
	*/
	@ModelAttribute("UC43_AddPlayerFormInit")
	public void initUC43_AddPlayerForm(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, U_C43__ADD_PLAYER_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method UC43_AddPlayerForm."); //for lnk_delete 
		}
		UC43_AddPlayerForm uC43_AddPlayerForm;
	
		if(request.getSession().getAttribute(U_C43__ADD_PLAYER_FORM) != null){
			uC43_AddPlayerForm = (UC43_AddPlayerForm)request.getSession().getAttribute(U_C43__ADD_PLAYER_FORM);
		} else {
			uC43_AddPlayerForm = new UC43_AddPlayerForm();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form UC43_AddPlayerForm.");
		}
		uC43_AddPlayerForm = (UC43_AddPlayerForm)populateDestinationForm(request.getSession(), uC43_AddPlayerForm);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  UC43_AddPlayerForm.");
		}
		model.addAttribute(U_C43__ADD_PLAYER_FORM, uC43_AddPlayerForm);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param UC43_AddPlayerForm : The sceen form.
	 */
	@RequestMapping(value = "/UC43_AddPlayer.html" ,method = RequestMethod.GET)
	public void prepareUC43_AddPlayer(final HttpServletRequest request,final  Model model,final  @ModelAttribute("UC43_AddPlayerFormInit") UC43_AddPlayerForm uC43_AddPlayerForm){
		
		UC43_AddPlayerForm currentUC43_AddPlayerForm = uC43_AddPlayerForm;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(U_C43__ADD_PLAYER_FORM) == null){
			if(currentUC43_AddPlayerForm!=null){
				request.getSession().setAttribute(U_C43__ADD_PLAYER_FORM, currentUC43_AddPlayerForm);
			}else {
				currentUC43_AddPlayerForm = new UC43_AddPlayerForm();
				request.getSession().setAttribute(U_C43__ADD_PLAYER_FORM, currentUC43_AddPlayerForm);	
			}
		} else {
			currentUC43_AddPlayerForm = (UC43_AddPlayerForm) request.getSession().getAttribute(U_C43__ADD_PLAYER_FORM);
		}

		try {
			List allPositions = position43DAOFinderImpl.findAll();
			put(request.getSession(), ALL_POSITIONS, allPositions);
		} catch (ApplicationException e) {
			errorLogger("An error while calling the generic finder for the instance : allPositions.",e);
		}
		try {
			List listPlayers = player43DAOFinderImpl.findAll();
			put(request.getSession(), LIST_PLAYERS, listPlayers);
		} catch (ApplicationException e) {
			errorLogger("An error while calling the generic finder for the instance : listPlayers.",e);
		}
					currentUC43_AddPlayerForm = (UC43_AddPlayerForm)populateDestinationForm(request.getSession(), currentUC43_AddPlayerForm);
		// Call all the Precontroller.
	currentUC43_AddPlayerForm.setAlwaysCallPreControllers(true);
		model.addAttribute(U_C43__ADD_PLAYER_FORM, currentUC43_AddPlayerForm);
		request.getSession().setAttribute(U_C43__ADD_PLAYER_FORM, currentUC43_AddPlayerForm);
		
	}
	
				@RequestMapping("/UC43_AddPlayer/captcha.html")
	public void showForm(HttpServletRequest request, HttpServletResponse response) throws Exception {
		byte[] captchaChallengeAsJpeg = null;
		// the output stream to render the captcha image as jpeg into
		ByteArrayOutputStream jpegOutputStream = new ByteArrayOutputStream();
		try {
			// get the session id that will identify the generated captcha.
			// the same id must be used to validate the response, the session id
			// is a good candidate!
			String captchaId = request.getSession().getId();
			BufferedImage challenge = captchaService.getImageChallengeForID(captchaId, request.getLocale());
			ImageIO.write(challenge, CAPTCHA_IMAGE_FORMAT, jpegOutputStream);
		} catch (IllegalArgumentException e) {
			response.sendError(HttpServletResponse.SC_NOT_FOUND);
			return;
		} catch (CaptchaServiceException e) {
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			return;
		}
		captchaChallengeAsJpeg = jpegOutputStream.toByteArray();
		// flush it in the response
		response.setHeader("Cache-Control", "no-store");
		response.setHeader("Pragma", "no-cache");
		response.setDateHeader("Expires", 0);
		response.setContentType("image/" + CAPTCHA_IMAGE_FORMAT);
		ServletOutputStream responseOutputStream = response.getOutputStream();
		responseOutputStream.write(captchaChallengeAsJpeg);
		responseOutputStream.flush();
		responseOutputStream.close();
	}
	
	
	protected void validateCaptcha(UC43_AddPlayerForm  uC43_AddPlayerForm, BindingResult result, final HttpServletRequest request, String errorCode) {
		String response = uC43_AddPlayerForm.getCaptcha();
		// If the captcha field is already rejected
		if (null != result.getFieldError("captcha")) {
			return;
		}
		boolean validCaptcha = false;
		try {
			validCaptcha = captchaService.validateResponseForID(request.getSession().getId(), response);
		} catch (CaptchaServiceException e) {
			// should not happen, may be thrown if the id is not valid
			LOGGER.warn("validateCaptcha()", e);
		}
		if (!validCaptcha) {
			result.rejectValue("captcha", "", errorCode);
		}
	}
				/**
	 * method callCreateplayer
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callCreateplayer(HttpServletRequest request,IForm form) {
					playerToAdd = (Player43BO)get(request.getSession(),form, "playerToAdd");  
										 
					try {
				// executing Createplayer in lnk_delete
	servicePlayer43.player43Create(
			playerToAdd
			);  

													// processing variables Createplayer in lnk_delete
				put(request.getSession(), PLAYER_TO_ADD,playerToAdd); 
			
			} catch (ApplicationException e) { 
				// error handling for operation player43Create called Createplayer
				errorLogger("An error occured during the execution of the operation : player43Create",e); 
		
			}
	}
									/**
	 * method callDeleteplayer
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 * @param index Integer the current index 
	 */
	private void callDeleteplayer(HttpServletRequest request,IForm form,Integer index) {
					if(index!=null){  
			 selectedPlayer = (Player43BO)((List)get(request.getSession(),form, "listPlayers")).get(index);  
		} else {
			 selectedPlayer= null; 
		}
				 
					try {
				// executing Deleteplayer in lnk_delete
	servicePlayer43.player43Delete(
			selectedPlayer
			);  

													// processing variables Deleteplayer in lnk_delete
				put(request.getSession(), SELECTED_PLAYER,selectedPlayer); 
			
			} catch (ApplicationException e) { 
				// error handling for operation player43Delete called Deleteplayer
				errorLogger("An error occured during the execution of the operation : player43Delete",e); 
		
			}
	}
		/**
	* This method Binder.
	* @param binder : 
	*		 WebDataBinder
	*/
	@InitBinder
	protected void initBinder(final WebDataBinder binder){
			customDateEditorsUC43_AddPlayerController.put("playerToAdd.dateOfBirth", new CustomDateEditor(new SimpleDateFormat("MM/dd/yyyy"), true, "Invalid format."));
			binder.registerCustomEditor(Date.class, "playerToAdd.dateOfBirth", customDateEditorsUC43_AddPlayerController.get("playerToAdd.dateOfBirth"));
	}
	
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("UC43_AddPlayerController [ ");
				strBToS.append(LIST_PLAYERS);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(listPlayers);
			strBToS.append(END_TO_STRING_DELIMITER);
						strBToS.append(ALL_POSITIONS);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(allPositions);
			strBToS.append(END_TO_STRING_DELIMITER);
						strBToS.append(PLAYER_TO_ADD);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(playerToAdd);
			strBToS.append(END_TO_STRING_DELIMITER);
			strBToS.append(SELECTED_PLAYER);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(selectedPlayer);
			strBToS.append(END_TO_STRING_DELIMITER);
		// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(uC43_AddPlayerValidator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsUC43_AddPlayerController!=null); 
		return strBToS.toString();
	}
}
