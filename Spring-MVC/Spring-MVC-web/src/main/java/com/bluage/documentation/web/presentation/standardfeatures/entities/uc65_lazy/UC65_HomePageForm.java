/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.entities.uc65_lazy;


import java.io.Serializable;
import java.util.List;

import com.bluage.documentation.business.standardfeatures.entities.uc65_lazy.bos.Team65BO;
import com.netfective.bluage.core.web.AbstractForm;
import com.netfective.bluage.core.web.IForm;

/**
* Class : UC65_HomePageForm
*/
public class UC65_HomePageForm extends AbstractForm implements Serializable, IForm {
	
	/** Constant : serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	//CONSTANT : teams
	private static final String TEAMS = "teams";
	//CONSTANT : teamLazyFalse
	private static final String TEAM_LAZY_FALSE = "teamLazyFalse";
	//CONSTANT : teamLazyTrue
	private static final String TEAM_LAZY_TRUE = "teamLazyTrue";
	//CONSTANT : selectedTeam
	private static final String SELECTED_TEAM = "selectedTeam";
	/**
	 * 	Property: teams 
	 */
	private List<Team65BO> teams;
	/**
	 * 	Property: teamLazyFalse 
	 */
	private Team65BO teamLazyFalse;
	/**
	 * 	Property: teamLazyTrue 
	 */
	private Team65BO teamLazyTrue;
	/**
	 * 	Property: selectedTeam 
	 */
	private Team65BO selectedTeam;
/**
	 * Default constructor : UC65_HomePageForm
	 */
	public UC65_HomePageForm() {
		super();
		// Initialize : teams
		this.teams = new java.util.ArrayList<com.bluage.documentation.business.standardfeatures.entities.uc65_lazy.bos.Team65BO>();
		// Initialize : teamLazyFalse
		this.teamLazyFalse = null;
		// Initialize : teamLazyTrue
		this.teamLazyTrue = null;
		// Initialize : selectedTeam
		this.selectedTeam = null;
					this.setAlwaysCallPreControllers(true);
	}
	
	/**
	 * 	Getter : teams 
	 * 	@return : Return the teams instance.
	 */
	public List<Team65BO> getTeams(){
		return teams; // For UC65_HomePageForm
	}
	
	/**
	 * 	Setter : teams 
	 *  @param teamsinstance : The instance to set.
	 */
	public void setTeams(final List<Team65BO> teamsinstance){
		this.teams = teamsinstance;// For UC65_HomePageForm
	}
	/**
	 * 	Getter : teamLazyFalse 
	 * 	@return : Return the teamLazyFalse instance.
	 */
	public Team65BO getTeamLazyFalse(){
		return teamLazyFalse; // For UC65_HomePageForm
	}
	
	/**
	 * 	Setter : teamLazyFalse 
	 *  @param teamLazyFalseinstance : The instance to set.
	 */
	public void setTeamLazyFalse(final Team65BO teamLazyFalseinstance){
		this.teamLazyFalse = teamLazyFalseinstance;// For UC65_HomePageForm
	}
	/**
	 * 	Getter : teamLazyTrue 
	 * 	@return : Return the teamLazyTrue instance.
	 */
	public Team65BO getTeamLazyTrue(){
		return teamLazyTrue; // For UC65_HomePageForm
	}
	
	/**
	 * 	Setter : teamLazyTrue 
	 *  @param teamLazyTrueinstance : The instance to set.
	 */
	public void setTeamLazyTrue(final Team65BO teamLazyTrueinstance){
		this.teamLazyTrue = teamLazyTrueinstance;// For UC65_HomePageForm
	}
	/**
	 * 	Getter : selectedTeam 
	 * 	@return : Return the selectedTeam instance.
	 */
	public Team65BO getSelectedTeam(){
		return selectedTeam; // For UC65_HomePageForm
	}
	
	/**
	 * 	Setter : selectedTeam 
	 *  @param selectedTeaminstance : The instance to set.
	 */
	public void setSelectedTeam(final Team65BO selectedTeaminstance){
		this.selectedTeam = selectedTeaminstance;// For UC65_HomePageForm
	}
/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UC65_HomePageForm [ "+
TEAMS +" = " + teams +TEAM_LAZY_FALSE +" = " + teamLazyFalse +TEAM_LAZY_TRUE +" = " + teamLazyTrue +SELECTED_TEAM +" = " + selectedTeam + "]";
	}
	
	/**
	 * Method setInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public void setInstanceByInstanceName(final String instanceName, final Object instance) {
		// Set the instance Teams.
		if(TEAMS.equals(instanceName)){// For UC65_HomePageForm
			this.setTeams((List<Team65BO>)instance); // For UC65_HomePageForm
		}
				// Set the instance TeamLazyFalse.
		if(TEAM_LAZY_FALSE.equals(instanceName)){// For UC65_HomePageForm
			this.setTeamLazyFalse((Team65BO)instance); // For UC65_HomePageForm
		}
				// Set the instance TeamLazyTrue.
		if(TEAM_LAZY_TRUE.equals(instanceName)){// For UC65_HomePageForm
			this.setTeamLazyTrue((Team65BO)instance); // For UC65_HomePageForm
		}
				// Set the instance SelectedTeam.
		if(SELECTED_TEAM.equals(instanceName)){// For UC65_HomePageForm
			this.setSelectedTeam((Team65BO)instance); // For UC65_HomePageForm
		}
			}
	
	/**
	 * Method getInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public Object getInstanceByInstanceName(final String instanceName){
		// Temporary object for tmpUC65_HomePageForm.
		Object tmpUC65_HomePageForm = null;
		
			
		// Get the instance Teams for UC65_HomePageForm.
		if(TEAMS.equals(instanceName)){ // For UC65_HomePageForm
			tmpUC65_HomePageForm = this.getTeams(); // For UC65_HomePageForm
		}
			
		// Get the instance TeamLazyFalse for UC65_HomePageForm.
		if(TEAM_LAZY_FALSE.equals(instanceName)){ // For UC65_HomePageForm
			tmpUC65_HomePageForm = this.getTeamLazyFalse(); // For UC65_HomePageForm
		}
			
		// Get the instance TeamLazyTrue for UC65_HomePageForm.
		if(TEAM_LAZY_TRUE.equals(instanceName)){ // For UC65_HomePageForm
			tmpUC65_HomePageForm = this.getTeamLazyTrue(); // For UC65_HomePageForm
		}
			
		// Get the instance SelectedTeam for UC65_HomePageForm.
		if(SELECTED_TEAM.equals(instanceName)){ // For UC65_HomePageForm
			tmpUC65_HomePageForm = this.getSelectedTeam(); // For UC65_HomePageForm
		}
		return tmpUC65_HomePageForm;// For UC65_HomePageForm
	}
	
			}
