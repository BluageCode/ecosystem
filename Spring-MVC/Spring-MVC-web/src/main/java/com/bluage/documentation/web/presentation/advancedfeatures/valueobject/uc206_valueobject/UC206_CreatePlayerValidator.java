/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.advancedfeatures.valueobject.uc206_valueobject ;


import org.apache.log4j.Logger;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;

import com.netfective.bluage.core.validator.AbstractValidator;
import com.netfective.bluage.core.validator.ValidatorsUtil;
/**
* Class :UC206_CreatePlayerValidator
*/
public class UC206_CreatePlayerValidator extends AbstractValidator{
	
	// LOGGER for the class UC206_CreatePlayerValidator
	private static final Logger LOGGER = Logger.getLogger( UC206_CreatePlayerValidator.class);
	
	
	/**
	* Operation validate for UC206_CreatePlayerForm
	* @param obj : the current form (UC206_CreatePlayerForm)
	* @param errors : The spring errors to return for the form UC206_CreatePlayerForm
	*/
	public void validate(final Object obj,final Errors errors){
		LOGGER.info("Beginning method : validate"); // UC206_CreatePlayerValidator
		UC206_CreatePlayerForm cUC206_CreatePlayerForm = (UC206_CreatePlayerForm)obj; // UC206_CreatePlayerValidator
				ValidationUtils.rejectIfEmpty(errors, "screen206.player206VO.height", "", "Please enter a player height.");

		if(errors.getFieldError("screen206.player206VO.height")== null){
						if(LOGGER.isDebugEnabled()){
				LOGGER.debug("validation of the 'screen206.player206VO.height' field with the RangeValidator validator");
			}
			
			if(ValidatorsUtil.rangeValidator(cUC206_CreatePlayerForm.getScreen206().getPlayer206VO().getHeight(), "150", "240")== 0 ){
				errors.rejectValue("screen206.player206VO.height", "", "Player height should be between 150 and 240 cms.");
			} 
				
		}
				ValidationUtils.rejectIfEmpty(errors, "screen206.player206VO.firstName", "", "First name is required.");

		if(errors.getFieldError("screen206.player206VO.firstName")== null){
						if(LOGGER.isDebugEnabled()){
				LOGGER.debug("validation of the 'screen206.player206VO.firstName' field with the LengthValidator validator");
			}
			
			if(ValidatorsUtil.lengthValidator(cUC206_CreatePlayerForm.getScreen206().getPlayer206VO().getFirstName(), 1, 30) == 0){
				errors.rejectValue("screen206.player206VO.firstName", "", "First name should be less than 30 characters long.");
			} 
				
		}
				ValidationUtils.rejectIfEmpty(errors, "screen206.player206VO.lastName", "", "Last name is required.");

		if(errors.getFieldError("screen206.player206VO.lastName")== null){
						if(LOGGER.isDebugEnabled()){
				LOGGER.debug("validation of the 'screen206.player206VO.lastName' field with the LengthValidator validator");
			}
			
			if(ValidatorsUtil.lengthValidator(cUC206_CreatePlayerForm.getScreen206().getPlayer206VO().getLastName(), 1, 30) == 0){
				errors.rejectValue("screen206.player206VO.lastName", "", "Last name should be less than 30 characters long.");
			} 
				
		}
				ValidationUtils.rejectIfEmpty(errors, "screen206.player206VO.weight", "", "Please enter a player weight.");

		if(errors.getFieldError("screen206.player206VO.weight")== null){
						if(LOGGER.isDebugEnabled()){
				LOGGER.debug("validation of the 'screen206.player206VO.weight' field with the RangeValidator validator");
			}
			
			if(ValidatorsUtil.rangeValidator(cUC206_CreatePlayerForm.getScreen206().getPlayer206VO().getWeight(), "60", "140")== 0 ){
				errors.rejectValue("screen206.player206VO.weight", "", "Player weight should be between 60 and 140 kgs.");
			} 
				
		}
		LOGGER.info("Ending method : validate the form "+ cUC206_CreatePlayerForm.getClass().getName()); // UC206_CreatePlayerValidator
	}

	/**
	* Method to implements to use spring validators (UC206_CreatePlayerForm)
	* @param aClass : Class for the form UC206_CreatePlayerForm
	* @return boolean
	*/
	public boolean supports(final Class aClass){
		return (new UC206_CreatePlayerForm()).getClass().equals(aClass); // UC206_CreatePlayerValidator
	}
}
