/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.application.datagrid.uc37_collectionorder ;

// Import declaration.
// Java imports.
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.bluage.documentation.business.standardfeatures.application.datagrid.uc37_collectionorder.bos.Team37BO;
import com.bluage.documentation.service.standardfeatures.application.datagrid.uc37_collectionorder.ServiceCollectionOrder;
import com.netfective.bluage.core.exception.ApplicationException;
import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;
import com.netfective.bluage.core.web.IForm;

/**
 * Class : UC37_TeamsAndPlayersController
 */
@Controller
@Scope("prototype")
@SessionAttributes("uC37_TeamsAndPlayersForm")
@RequestMapping(value= "/presentation/standardfeatures/application/datagrid/uc37_collectionorder")
public class UC37_TeamsAndPlayersController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC37_TeamsAndPlayersController.class);
	
	//Current form name
	private static final String U_C37__TEAMS_AND_PLAYERS_FORM = "uC37_TeamsAndPlayersForm";

	//CONSTANT: team
	private static final String TEAM = "team";
		//CONSTANT: players table or repeater.
	private static final String PLAYERS = "players";
				
	/**
	 * Property:customDateEditorsUC37_TeamsAndPlayersController
	 */
	final private Map<String,CustomDateEditor> customDateEditorsUC37_TeamsAndPlayersController = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:uC37_TeamsAndPlayersValidator
	 */
	final private UC37_TeamsAndPlayersValidator uC37_TeamsAndPlayersValidator = new UC37_TeamsAndPlayersValidator();
	
	/**
	 * Service declaration : serviceCollectionOrder.
	 */
	@Autowired
	private ServiceCollectionOrder serviceCollectionOrder;
	
	
	// Initialise all the instances for UC37_TeamsAndPlayersController
		// Initialize the instance team of type com.bluage.documentation.business.standardfeatures.application.datagrid.uc37_collectionorder.bos.Team37BO with the value : null for UC37_TeamsAndPlayersController
		private Team37BO team;
			// Initialize the instance players for the state lnk_view
		private List players; // Initialize the instance players for UC37_TeamsAndPlayersController
									/**
	 * Operation : lnk_back
 	 * @param model : 
 	 * @param uC37_TeamsAndPlayers : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC37_TeamsAndPlayers/lnk_back.html",method = RequestMethod.GET)
	public String lnk_back(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC37_TeamsAndPlayersForm") UC37_TeamsAndPlayersForm  uC37_TeamsAndPlayersForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : lnk_back"); 
		infoLogger(uC37_TeamsAndPlayersForm); 
		uC37_TeamsAndPlayersForm.setAlwaysCallPreControllers(false); 
		// initialization for lnk_back
		init();
		
		String destinationPath = null; 
		

	 callBacktoparentspage(request,uC37_TeamsAndPlayersForm );
											// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.application.datagrid.uc37_collectionorder.UC37_TeamsForm or not from lnk_back
			final UC37_TeamsForm uC37_TeamsForm =  new UC37_TeamsForm();
			// Populate the destination form with all the instances returned from lnk_back.
			final IForm uC37_TeamsForm2 = populateDestinationForm(request.getSession(), uC37_TeamsForm); 
					
			// Add the form to the model.
			model.addAttribute("uC37_TeamsForm", uC37_TeamsForm2); 
			
			request.getSession().setAttribute("uC37_TeamsForm", uC37_TeamsForm2);
			
			// "OK" CASE => destination screen path from lnk_back
			LOGGER.info("Go to the screen 'UC37_Teams'.");
			// Redirect (PRG) from lnk_back
			destinationPath =  "redirect:/presentation/standardfeatures/application/datagrid/uc37_collectionorder/UC37_Teams.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_back 
	}
	
	
	/**
	* This method initialise the form : UC37_TeamsAndPlayersForm 
	* @return UC37_TeamsAndPlayersForm
	*/
	@ModelAttribute("UC37_TeamsAndPlayersFormInit")
	public void initUC37_TeamsAndPlayersForm(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, U_C37__TEAMS_AND_PLAYERS_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method UC37_TeamsAndPlayersForm."); //for lnk_back 
		}
		UC37_TeamsAndPlayersForm uC37_TeamsAndPlayersForm;
	
		if(request.getSession().getAttribute(U_C37__TEAMS_AND_PLAYERS_FORM) != null){
			uC37_TeamsAndPlayersForm = (UC37_TeamsAndPlayersForm)request.getSession().getAttribute(U_C37__TEAMS_AND_PLAYERS_FORM);
		} else {
			uC37_TeamsAndPlayersForm = new UC37_TeamsAndPlayersForm();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form UC37_TeamsAndPlayersForm.");
		}
		uC37_TeamsAndPlayersForm = (UC37_TeamsAndPlayersForm)populateDestinationForm(request.getSession(), uC37_TeamsAndPlayersForm);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  UC37_TeamsAndPlayersForm.");
		}
		model.addAttribute(U_C37__TEAMS_AND_PLAYERS_FORM, uC37_TeamsAndPlayersForm);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param UC37_TeamsAndPlayersForm : The sceen form.
	 */
	@RequestMapping(value = "/UC37_TeamsAndPlayers.html" ,method = RequestMethod.GET)
	public void prepareUC37_TeamsAndPlayers(final HttpServletRequest request,final  Model model,final  @ModelAttribute("UC37_TeamsAndPlayersFormInit") UC37_TeamsAndPlayersForm uC37_TeamsAndPlayersForm){
		
		UC37_TeamsAndPlayersForm currentUC37_TeamsAndPlayersForm = uC37_TeamsAndPlayersForm;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(U_C37__TEAMS_AND_PLAYERS_FORM) == null){
			if(currentUC37_TeamsAndPlayersForm!=null){
				request.getSession().setAttribute(U_C37__TEAMS_AND_PLAYERS_FORM, currentUC37_TeamsAndPlayersForm);
			}else {
				currentUC37_TeamsAndPlayersForm = new UC37_TeamsAndPlayersForm();
				request.getSession().setAttribute(U_C37__TEAMS_AND_PLAYERS_FORM, currentUC37_TeamsAndPlayersForm);	
			}
		} else {
			currentUC37_TeamsAndPlayersForm = (UC37_TeamsAndPlayersForm) request.getSession().getAttribute(U_C37__TEAMS_AND_PLAYERS_FORM);
		}

				currentUC37_TeamsAndPlayersForm = (UC37_TeamsAndPlayersForm)populateDestinationForm(request.getSession(), currentUC37_TeamsAndPlayersForm);
		// Call all the Precontroller.
	currentUC37_TeamsAndPlayersForm.setAlwaysCallPreControllers(true);
		model.addAttribute(U_C37__TEAMS_AND_PLAYERS_FORM, currentUC37_TeamsAndPlayersForm);
		request.getSession().setAttribute(U_C37__TEAMS_AND_PLAYERS_FORM, currentUC37_TeamsAndPlayersForm);
		
	}
	
			/**
	 * method callBacktoparentspage
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callBacktoparentspage(HttpServletRequest request,IForm form) {
					try {
				// executing Backtoparentspage in lnk_back
	serviceCollectionOrder.back(
	);  

								// processing variables Backtoparentspage in lnk_back

			} catch (ApplicationException e) { 
				// error handling for operation back called Backtoparentspage
				errorLogger("An error occured during the execution of the operation : back",e); 
		
			}
	}
		
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("UC37_TeamsAndPlayersController [ ");
			strBToS.append(TEAM);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(team);
			strBToS.append(END_TO_STRING_DELIMITER);
				strBToS.append(PLAYERS);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(players);
			strBToS.append(END_TO_STRING_DELIMITER);
					// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(uC37_TeamsAndPlayersValidator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsUC37_TeamsAndPlayersController!=null); 
		return strBToS.toString();
	}
}
