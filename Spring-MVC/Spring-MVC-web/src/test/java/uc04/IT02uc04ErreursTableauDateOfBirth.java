package uc04;

import org.junit.Test;
import test.common.AbstractBADocTestCase;

public class IT02uc04ErreursTableauDateOfBirth extends AbstractBADocTestCase {
	
	@Override
	public  String getName(){
		return "UC-04";
	}
	
	@Test
	public void testIT02uc04ErreursTableauDateOfBirth() throws Exception {
		// On teste l'UC04 - Getting started - Entities Modification
		// On teste dans le tableau "Entities Modification" le message d'erreur au niveau de Date of birth.
		selenium.open("");
		selenium.waitForPageToLoad("30000");
		selenium.click("link=Expand All");
		selenium.click("link=UC04 - Entities Modification");
		verifyTrue(selenium.isElementPresent("//h2[contains(text(),\"Getting started > UC04 > Entities Modification\")]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/thead/tr[th[contains(text(),\"Name\")] and th[contains(text(),\"Date of Birth\")] and th[contains(text(),\"Weight (kg)\")] and th[contains(text(),\"Height (cm)\")] and th[contains(text(),\"Estimated Value (M$)\")] and th[contains(text(),\"Position\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Laplace\")] and td/span[contains(text(),\"Bernard\")] and td/span[contains(text(),\"07/14/1980\")] and td/span[contains(text(),\"95\")] and td/span[contains(text(),\"191\")] and td/span[contains(text(),\"37.32\")] and td/span[contains(text(),\"PF\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Sorel\")] and td/span[contains(text(),\"Julien\")] and td/span[contains(text(),\"01/20/1975\")] and td/span[contains(text(),\"87\")] and td/span[contains(text(),\"182\")] and td/span[contains(text(),\"23.55\")] and td/span[contains(text(),\"C\")]]"));
		// On v�rifie que le joueur 'Julien Sorel' existe.
		assertTrue(selenium.isElementPresent("xpath=//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Sorel\")] and td/span[contains(text(),\"Julien\")]]"));
		// On indexe la ligne de 'Julien Sorel'.
		Number index = selenium.getElementIndex("xpath=//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Sorel\")] and td/span[contains(text(),\"Julien\")]]");
		selenium.click("xpath=//table[contains(@id,'tab_players')]/tbody/tr[" + index + " + 1]/td[7]/a[contains(text(),\"[edit]\")]");
		selenium.waitForPageToLoad("30000");
		// On entre une date de naissance erron�e.
		selenium.type("xpath=//table[contains(@id,'tab_players')]/tbody/tr[" + index + " +1]/td[2]/input", "20/01/1975");
		selenium.click("xpath=//table[contains(@id,'tab_players')]/tbody/tr[" + index + " +1]/td[7]/a[contains(text(),\"[update]\")]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie que le message d'erreur existe.
		assertTrue(selenium.isElementPresent("xpath=//table[contains(@id,'tab_players')]/tbody/tr[" + index + " +1]/td[2]/span[1]"));
		// On cr�e une variable pour la date du jour pr�sente dans le message d'erreur.
		String date = selenium.getExpression(selenium.getEval("var date = new Date();(date.getMonth() < 9 ? '0' : '') + (date.getMonth()+1)+'/'+(date.getDate() < 10 ? '0' : '')+date.getDate()+'/'+date.getFullYear();"));
		// On v�rifie que le message d'erreur s'affiche correctement.
		assertEquals("Failed to convert property value of type java.lang.String to required type java.util.Date for property allPlayers[0].dateOfBirth; nested exception is java.lang.IllegalArgumentException: Invalid format.", selenium.getText("xpath=//table[contains(@id,'tab_players')]/tbody/tr[" + index + " +1]/td[2]/span[1]"));
		// On entre la date de naissance d'origine.
		selenium.type("xpath=//table[contains(@id,'tab_players')]/tbody/tr[" + index + " +1]/td[2]/input", "01/20/1975");
		selenium.click("xpath=//table[contains(@id,'tab_players')]/tbody/tr[" + index + " +1]/td[7]/a[contains(text(),\"[update]\")]");
		selenium.waitForPageToLoad("30000");
	}

}
