/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.service.standardfeatures.application.datagrid.uc35_paginatorbdd;


import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import com.bluage.documentation.business.standardfeatures.application.datagrid.uc35_paginatorbdd.bos.Player35BO;
import com.bluage.documentation.business.standardfeatures.application.datagrid.uc35_paginatorbdd.entities.daofinder.Player35DAOFinderImpl;
import com.bluage.documentation.service.AbstractService;
import com.netfective.bluage.core.exception.ApplicationException;
import com.netfective.bluage.core.persistence.IPersistentObjectDAO;
import com.netfective.bluage.core.presentation.screen.paginator.IPageCommand;

/**
 * Defines  application services for ServicePaginatorBDD
  */
@Named("com.bluage.documentation.service.standardfeatures.application.datagrid.uc35_paginatorbdd.ServicePaginatorBDD")
public class ServicePaginatorBDD  extends AbstractService{


	


	/**
	 *  Default constructor 
	 */
	public ServicePaginatorBDD() {
		super(); // Call super for default constructor : ServicePaginatorBDD
	}
	
			@Inject
	@Named ("com.bluage.documentation.business.standardfeatures.application.datagrid.uc35_paginatorbdd.entities.daofinder.Player35DAOFinderImpl")
	private Player35DAOFinderImpl dao;

	/**
	 * constructor with...
	 * 
	 * @param dao
	 *		: {@link IPersistentObjectDAO} the DAO
	 */
	public ServicePaginatorBDD(Player35DAOFinderImpl dao) {
		this.dao = dao;
	}

								/**
	 * Operation displaysPlayers with paged
	 * @param command the IPageCommand
	 * @return {@link  java.util.List<com.bluage.documentation.business.standardfeatures.application.datagrid.uc35_paginatorbdd.bos.Player35BO>}
	 * @throws ApplicationException an Application Exception
     */
    public List<Player35BO> displaysPlayers(IPageCommand command) 
    	throws ApplicationException
	{
				int count = dao.count();

		command.setNumberOfPages(count / command.getItemsPerPage());
		command.setNumberOfCount(count);

		if (command.getCurrentPage() > command.getNumberOfPages()) {
			command.setCurrentPage(command.getNumberOfPages());
		}

		int startRecord = command.getCurrentPage() * command.getItemsPerPage();

		if (command.getSortLinkNatural() != 0) {
			return (List) dao.prepareResult(startRecord, command.getItemsPerPage());
		} else {
			return null;
		}
	}			

	

}
