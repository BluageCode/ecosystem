<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file="/include.jsp"  %>
<html >
<head >
<title  > 
<fmt:message key="cUC04_EntitiesModificationPlainPage_BLU_AGE_DOCUMENTATION"/>
</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"  > 
</meta>
<link href="<c:url value= "/presentation/_style/_style.css"/>" rel="stylesheet" type="text/css"  /> 
<link href="<c:url value= "/presentation/_include/jquery/rd_lib.css"/>" rel="stylesheet" type="text/css"  /> 
<script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/jquery.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/rd_setup.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/rd_lib.js"/>" space="preserve"  ></script><link rel="shortcut icon" href="<c:url value= "/presentation/_style/favicon.ico"/>"  /> 

</head>

<body class=" " > 
<div id="rd_ghost"  > 
<small  > 
<fmt:message key="cUC04_EntitiesModificationPlainPage___CSS_DEACTIVATED"/>
</small>
</div>
<div id="rd_jghost"  > 
<small  > 
<fmt:message key="cUC04_EntitiesModificationPlainPage___JAVASCRIPT_DEACTIVATED"/>
</small>
</div>
<div id="shape"  > 
<div class="ui-layout-center"  > 
<div class="east-north"  > 
	<form:form id="uC04_EntitiesModificationPlainPage" commandName="uC04_EntitiesModificationPlainPageForm"  enctype="application/x-www-form-urlencoded" method="post">
<h2 class="rd_first"  > 
<fmt:message key="cUC04_EntitiesModificationPlainPage_GETTING_STARTED__UC04__ENTIT"/>
</h2>
<table class="rd_tableform"  >
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC04_EntitiesModificationPlainPage_FIRST_NAME_REQUIREDFIELDVALID"/>
</label>
<label style="background: none;color: red;"  > 
<fmt:message key="cUC04_EntitiesModificationPlainPage_"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
<form:input id="txt_firstname" path="playerToUpdate.firstName"  size="30" 
		/>
</td> 
<td rowspan="1" colspan="1"  >
<form:errors path="playerToUpdate.firstName"/>
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC04_EntitiesModificationPlainPage_LAST_NAME_REQUIREDFIELDVALIDA"/>
</label>
<label style="background: none;color: red;"  > 
<fmt:message key="cUC04_EntitiesModificationPlainPage__57"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
<form:input id="txt_lastname" path="playerToUpdate.lastName"  size="30" 
		/>
</td> 
<td rowspan="1" colspan="1"  >
<form:errors path="playerToUpdate.lastName"/>
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC04_EntitiesModificationPlainPage_DATE_OF_BIRTH_MMDDYYYY"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
<form:input id="txt_dateofbirth" path="playerToUpdate.dateOfBirth"  size="12" 
		/>
</td> 
<td rowspan="1" colspan="1"  >
<form:errors path="playerToUpdate.dateOfBirth"/>
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC04_EntitiesModificationPlainPage_ESTIMATED_VALUE"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
<form:input id="txt_estimatedvalue" path="playerToUpdate.estimatedValue"  size="12" 
		/>
<fmt:message key="cUC04_EntitiesModificationPlainPage_M"/>
</td> 
<td rowspan="1" colspan="1"  >
<fmt:message key="cUC04_EntitiesModificationPlainPage__58"/>
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC04_EntitiesModificationPlainPage_WEIGHT_RANGEVALIDATOR"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
<form:input id="txt_weight" path="playerToUpdate.weight"  size="12" 
		/>
<fmt:message key="cUC04_EntitiesModificationPlainPage_KG"/>
</td> 
<td rowspan="1" colspan="1"  >
<form:errors path="playerToUpdate.weight"/>
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC04_EntitiesModificationPlainPage_HEIGHT_RANGEVALIDATOR"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
<form:input id="txt_height" path="playerToUpdate.height"  size="3" 
		/>
<fmt:message key="cUC04_EntitiesModificationPlainPage_CM"/>
</td> 
<td rowspan="1" colspan="1"  >
<form:errors path="playerToUpdate.height"/>
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC04_EntitiesModificationPlainPage_POSITION"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
<form:select id="slt_position" path="playerToUpdate.position.code"   
		>
		<form:options items="${uC04_EntitiesModificationPlainPageForm.allPositions}" itemValue="code" itemLabel="name" />
</form:select>
</td> 
<td rowspan="1" colspan="1"  >
<fmt:message key="cUC04_EntitiesModificationPlainPage__59"/>
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC04_EntitiesModificationPlainPage_ROOKIE"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
<form:checkbox id="chk_rookie" path="playerToUpdate.rookie"   
		/>
</td> 
<td rowspan="1" colspan="1"  >
<fmt:message key="cUC04_EntitiesModificationPlainPage__60"/>
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<fmt:message key="cUC04_EntitiesModificationPlainPage__61"/>
</td> 
<td rowspan="1" colspan="1"  >
<a id="lnk_update"onclick="javascript:btn_click_cExecuteUC04_EntitiesModificationPlainPage(this.href);return false;"				href="<c:url value="/presentation/gettingstarted/uc04_entitiesmodification/UC04_EntitiesModificationPlainPage/lnk_update.html"/>"
							 shape="rect"  >									 
<fmt:message key="cUC04_EntitiesModificationPlainPage_UPDATE_PLAYER"/>
</a>
</td> 
<td rowspan="1" colspan="1"  >
<fmt:message key="cUC04_EntitiesModificationPlainPage__62"/>
</td> 
</tr>
</table> 
<label style="background: none;color: red;"  > 
<fmt:message key="cUC04_EntitiesModificationPlainPage__63"/>
</label>
<fmt:message key="cUC04_EntitiesModificationPlainPage_REQUIRED_FIELDS_"/>
<br clear="none"  /> 
<br clear="none"  /> 
<a id="lnk_back"				href="<c:url value="/presentation/gettingstarted/uc04_entitiesmodification/UC04_EntitiesModification.html"/>"
													 shape="rect"  >									 
<fmt:message key="cUC04_EntitiesModificationPlainPage_BACK"/>
</a>
	</form:form>
	<script language="javascript">
		function btn_click_cExecuteUC04_EntitiesModificationPlainPage(url) {
			UC04_EntitiesModificationPlainPagevar = document.getElementById('uC04_EntitiesModificationPlainPage');
			UC04_EntitiesModificationPlainPagevar.setAttribute('action', url);
			UC04_EntitiesModificationPlainPagevar.submit();
		}
		function btn_click_cSetActionUC04_EntitiesModificationPlainPage(url) {
			UC04_EntitiesModificationPlainPagevar = document.getElementById('uC04_EntitiesModificationPlainPage');
			UC04_EntitiesModificationPlainPagevar.setAttribute('action', url);
		}
	</script>
</div>
<div class="east-center"  > 
<tiles:insertDefinition	name="presentation/gettingstarted/uc04_entitiesmodification/UC04_EntitiesModificationPlainPagecmp_tabs" />
</div>
</div>
<div class="ui-layout-north"  > 
<tiles:insertDefinition	name="presentation/gettingstarted/uc04_entitiesmodification/UC04_EntitiesModificationPlainPagecmp_header" />
</div>
<div class="ui-layout-south"  > 
<fmt:message key="cUC04_EntitiesModificationPlainPage___"/>
<span id="rd_jversion"  > 
<fmt:message key="cUC04_EntitiesModificationPlainPage_BLU_AGE_CORPORATION"/>
</span>
</div>
<div class="ui-layout-west"  > 
<tiles:insertDefinition	name="presentation/gettingstarted/uc04_entitiesmodification/UC04_EntitiesModificationPlainPagecmp_treeview" />
</div>
</div>
</body>
</html>
