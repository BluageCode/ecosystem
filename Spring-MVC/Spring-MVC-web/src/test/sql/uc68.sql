--<ScriptOptions statementTerminator=";"/>

ALTER TABLE professional68 DROP CONSTRAINT professional68_inheritance_fkc;

ALTER TABLE rookie68 DROP CONSTRAINT rookie68_inheritance_fkc;

ALTER TABLE rookie68 DROP CONSTRAINT rookie68_pkey;

ALTER TABLE professional68 DROP CONSTRAINT professional68_pkey;

ALTER TABLE player68 DROP CONSTRAINT player68_pkey;

DROP TABLE rookie68;

DROP TABLE professional68;

DROP TABLE player68;

CREATE TABLE rookie68 (
		id INT8 NOT NULL,
		university VARCHAR(80),
		height INT4,
		date_of_birth DATE,
		last_name VARCHAR(30),
		estimated_value FLOAT4,
		first_name VARCHAR(30),
		weight INT4
	);

CREATE TABLE professional68 (
		id INT8 NOT NULL,
		salary INT4,
		height INT4,
		date_of_birth DATE,
		last_name VARCHAR(30),
		estimated_value FLOAT4,
		first_name VARCHAR(30),
		weight INT4
	);

CREATE TABLE player68 (
		id INT8 NOT NULL,
		version INT4 NOT NULL,
		first_name VARCHAR(30) NOT NULL,
		last_name VARCHAR(30) NOT NULL,
		date_of_birth DATE,
		estimated_value FLOAT4,
		weight INT4,
		height INT4
	);

INSERT INTO player68 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height) VALUES (204, 0, 'Ricky', 'Dawson', '1990-03-03', 29.299999, 115, 168);
INSERT INTO player68 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height) VALUES (201, 0, 'Oliver', 'Frank', '1968-10-31', 0.11, 116, 228);
INSERT INTO player68 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height) VALUES (202, 0, 'Elbert', 'Cole', '1965-04-07', 3.95, 100, 171);
INSERT INTO player68 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height) VALUES (203, 0, 'Abel', 'Dennis', '1980-04-16', 30.51, 105, 208);
INSERT INTO player68 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height) VALUES (205, 0, 'Earl', 'Russell', '1981-12-15', 44.73, 60, 234);
INSERT INTO player68 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height) VALUES (207, 0, 'Howard', 'Ramos', '1970-08-25', 10.76, 107, 199);
INSERT INTO player68 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height) VALUES (208, 0, 'Allan', 'Meyer', '1969-02-14', 33.669998, 114, 229);
INSERT INTO player68 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height) VALUES (209, 0, 'Ruben', 'Pope', '1985-05-01', 44.66, 86, 201);
INSERT INTO player68 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height) VALUES (210, 0, 'Lester', 'Cross', '1976-02-21', 30.98, 69, 214);
INSERT INTO player68 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height) VALUES (211, 0, 'Keith', 'Barnett', '1976-11-10', 5.8099999, 66, 190);
INSERT INTO player68 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height) VALUES (212, 0, 'Ivan', 'Tate', '1972-02-08', 48.490002, 143, 175);
INSERT INTO player68 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height) VALUES (214, 0, 'Sylvester', 'Nash', '1965-07-10', 37.73, 68, 184);
INSERT INTO player68 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height) VALUES (215, 0, 'Michael', 'Hodges', '1988-04-01', 25.35, 158, 204);
INSERT INTO player68 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height) VALUES (216, 0, 'Jerome', 'Grant', '1982-06-28', 28.940001, 105, 205);
INSERT INTO player68 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height) VALUES (217, 0, 'Woodrow', 'Carpenter', '1987-08-08', 41.759998, 69, 224);
INSERT INTO player68 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height) VALUES (218, 0, 'Roderick', 'Robbins', '1975-07-29', 12.04, 115, 164);
INSERT INTO player68 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height) VALUES (219, 0, 'Christian', 'Chavez', '1983-05-17', 37.880001, 97, 209);
INSERT INTO player68 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height) VALUES (220, 0, 'Cecil', 'Conner', '1978-06-29', 39.709999, 160, 170);
INSERT INTO player68 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height) VALUES (206, 0, 'Nick', 'Garner', '1992-07-27', 47.84, 87, 221);
INSERT INTO player68 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height) VALUES (213, 0, 'Tommy', 'Wood', '1992-01-20', 13.93, 77, 173);

INSERT INTO professional68 (id, salary) VALUES (201, 125000);
INSERT INTO professional68 (id, salary) VALUES (203, 150000);
INSERT INTO professional68 (id, salary) VALUES (205, 175000);
INSERT INTO professional68 (id, salary) VALUES (206, 80000);
INSERT INTO professional68 (id, salary) VALUES (208, 98000);
INSERT INTO professional68 (id, salary) VALUES (213, 203000);
INSERT INTO professional68 (id, salary) VALUES (214, 123000);
INSERT INTO professional68 (id, salary) VALUES (216, 54000);
INSERT INTO professional68 (id, salary) VALUES (219, 32000);
INSERT INTO professional68 (id, salary) VALUES (220, 410000);

INSERT INTO rookie68 (id, university) VALUES (202, 'Cambridge');
INSERT INTO rookie68 (id, university) VALUES (204, 'Orlando');
INSERT INTO rookie68 (id, university) VALUES (207, 'Miami');
INSERT INTO rookie68 (id, university) VALUES (209, 'Dallas');
INSERT INTO rookie68 (id, university) VALUES (211, 'Houston');
INSERT INTO rookie68 (id, university) VALUES (212, 'Los Angeles');
INSERT INTO rookie68 (id, university) VALUES (210, 'San Fransisco');
INSERT INTO rookie68 (id, university) VALUES (215, 'San Antonio');
INSERT INTO rookie68 (id, university) VALUES (217, 'New York');
INSERT INTO rookie68 (id, university) VALUES (218, 'Chicago');
	
	
ALTER TABLE rookie68 ADD CONSTRAINT rookie68_pkey PRIMARY KEY (id);

ALTER TABLE professional68 ADD CONSTRAINT professional68_pkey PRIMARY KEY (id);

ALTER TABLE player68 ADD CONSTRAINT player68_pkey PRIMARY KEY (id);

ALTER TABLE professional68 ADD CONSTRAINT professional68_inheritance_fkc FOREIGN KEY (id)
	REFERENCES player68 (id);

ALTER TABLE rookie68 ADD CONSTRAINT rookie68_inheritance_fkc FOREIGN KEY (id)
	REFERENCES player68 (id);

