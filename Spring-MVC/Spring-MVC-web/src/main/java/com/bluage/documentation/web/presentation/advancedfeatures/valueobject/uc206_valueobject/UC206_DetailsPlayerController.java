/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.advancedfeatures.valueobject.uc206_valueobject ;

// Import declaration.
// Java imports.
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.bluage.documentation.business.advancedfeatures.valueobject.uc206_valueobject.vo.Player206VO;
import com.bluage.documentation.business.advancedfeatures.valueobject.uc206_valueobject.vo.Screen206VO;
import com.bluage.documentation.service.advancedfeatures.valueobject.uc206_valueobject.ServiceScreen206;
import com.netfective.bluage.core.exception.ApplicationException;
import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;
import com.netfective.bluage.core.web.IForm;

/**
 * Class : UC206_DetailsPlayerController
 */
@Controller
@Scope("prototype")
@SessionAttributes("uC206_DetailsPlayerForm")
@RequestMapping(value= "/presentation/advancedfeatures/valueobject/uc206_valueobject")
public class UC206_DetailsPlayerController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC206_DetailsPlayerController.class);
	
	//Current form name
	private static final String U_C206__DETAILS_PLAYER_FORM = "uC206_DetailsPlayerForm";

	//CONSTANT: selectedPlayer
	private static final String SELECTED_PLAYER = "selectedPlayer";
		//CONSTANT: screen206
	private static final String SCREEN206 = "screen206";
	//CONSTANT: playerVOs
	private static final String PLAYER_V_OS = "playerVOs";
	
	/**
	 * Property:customDateEditorsUC206_DetailsPlayerController
	 */
	final private Map<String,CustomDateEditor> customDateEditorsUC206_DetailsPlayerController = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:uC206_DetailsPlayerValidator
	 */
	final private UC206_DetailsPlayerValidator uC206_DetailsPlayerValidator = new UC206_DetailsPlayerValidator();
	
	/**
	 * Service declaration : serviceScreen206.
	 */
	@Autowired
	private ServiceScreen206 serviceScreen206;
	
	
	// Initialise all the instances for UC206_DetailsPlayerController
					// Initialize the instance selectedPlayer of type com.bluage.documentation.business.advancedfeatures.valueobject.uc206_valueobject.vo.Player206VO with the value : null for UC206_DetailsPlayerController
		private Player206VO selectedPlayer;
			// Declare the instance screen206
		private Screen206VO screen206;
		// Declare the instance playerVOs
		private List playerVOs;
					/**
	 * Operation : lnk_back
 	 * @param model : 
 	 * @param uC206_DetailsPlayer : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC206_DetailsPlayer/lnk_back.html",method = RequestMethod.GET)
	public String lnk_back(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC206_DetailsPlayerForm") UC206_DetailsPlayerForm  uC206_DetailsPlayerForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : lnk_back"); 
		infoLogger(uC206_DetailsPlayerForm); 
		uC206_DetailsPlayerForm.setAlwaysCallPreControllers(false); 
		// initialization for lnk_back
		init();
		
		String destinationPath = null; 
		

	 callInitializeScreen(request,uC206_DetailsPlayerForm );
			
				 callExecuteList(request,uC206_DetailsPlayerForm );
											// Init the destination form com.bluage.documentation.web.presentation.advancedfeatures.valueobject.uc206_valueobject.UC206_ListPlayerForm or not from lnk_back
			final UC206_ListPlayerForm uC206_ListPlayerForm =  new UC206_ListPlayerForm();
			// Populate the destination form with all the instances returned from lnk_back.
			final IForm uC206_ListPlayerForm2 = populateDestinationForm(request.getSession(), uC206_ListPlayerForm); 
					
			// Add the form to the model.
			model.addAttribute("uC206_ListPlayerForm", uC206_ListPlayerForm2); 
			
			request.getSession().setAttribute("uC206_ListPlayerForm", uC206_ListPlayerForm2);
			
			// "OK" CASE => destination screen path from lnk_back
			LOGGER.info("Go to the screen 'UC206_ListPlayer'.");
			// Redirect (PRG) from lnk_back
			destinationPath =  "redirect:/presentation/advancedfeatures/valueobject/uc206_valueobject/UC206_ListPlayer.html";
						
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_back 
	}
	
	
	/**
	* This method initialise the form : UC206_DetailsPlayerForm 
	* @return UC206_DetailsPlayerForm
	*/
	@ModelAttribute("UC206_DetailsPlayerFormInit")
	public void initUC206_DetailsPlayerForm(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, U_C206__DETAILS_PLAYER_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method UC206_DetailsPlayerForm."); //for lnk_back 
		}
		UC206_DetailsPlayerForm uC206_DetailsPlayerForm;
	
		if(request.getSession().getAttribute(U_C206__DETAILS_PLAYER_FORM) != null){
			uC206_DetailsPlayerForm = (UC206_DetailsPlayerForm)request.getSession().getAttribute(U_C206__DETAILS_PLAYER_FORM);
		} else {
			uC206_DetailsPlayerForm = new UC206_DetailsPlayerForm();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form UC206_DetailsPlayerForm.");
		}
		uC206_DetailsPlayerForm = (UC206_DetailsPlayerForm)populateDestinationForm(request.getSession(), uC206_DetailsPlayerForm);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  UC206_DetailsPlayerForm.");
		}
		model.addAttribute(U_C206__DETAILS_PLAYER_FORM, uC206_DetailsPlayerForm);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param UC206_DetailsPlayerForm : The sceen form.
	 */
	@RequestMapping(value = "/UC206_DetailsPlayer.html" ,method = RequestMethod.GET)
	public void prepareUC206_DetailsPlayer(final HttpServletRequest request,final  Model model,final  @ModelAttribute("UC206_DetailsPlayerFormInit") UC206_DetailsPlayerForm uC206_DetailsPlayerForm){
		
		UC206_DetailsPlayerForm currentUC206_DetailsPlayerForm = uC206_DetailsPlayerForm;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(U_C206__DETAILS_PLAYER_FORM) == null){
			if(currentUC206_DetailsPlayerForm!=null){
				request.getSession().setAttribute(U_C206__DETAILS_PLAYER_FORM, currentUC206_DetailsPlayerForm);
			}else {
				currentUC206_DetailsPlayerForm = new UC206_DetailsPlayerForm();
				request.getSession().setAttribute(U_C206__DETAILS_PLAYER_FORM, currentUC206_DetailsPlayerForm);	
			}
		} else {
			currentUC206_DetailsPlayerForm = (UC206_DetailsPlayerForm) request.getSession().getAttribute(U_C206__DETAILS_PLAYER_FORM);
		}

		currentUC206_DetailsPlayerForm = (UC206_DetailsPlayerForm)populateDestinationForm(request.getSession(), currentUC206_DetailsPlayerForm);
		// Call all the Precontroller.
	currentUC206_DetailsPlayerForm.setAlwaysCallPreControllers(true);
		model.addAttribute(U_C206__DETAILS_PLAYER_FORM, currentUC206_DetailsPlayerForm);
		request.getSession().setAttribute(U_C206__DETAILS_PLAYER_FORM, currentUC206_DetailsPlayerForm);
		
	}
	
	/**
	 * method callInitializeScreen
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callInitializeScreen(HttpServletRequest request,IForm form) {
		screen206 = (Screen206VO)get(request.getSession(),form, "screen206");  
										 
					try {
				// executing InitializeScreen in lnk_back
				screen206 = 	serviceScreen206.initializeScreen206(
	);  
 
				put(request.getSession(), SCREEN206,screen206);
								// processing variables InitializeScreen in lnk_back

			} catch (ApplicationException e) { 
				// error handling for operation initializeScreen206 called InitializeScreen
				errorLogger("An error occured during the execution of the operation : initializeScreen206",e); 
		
			}
	}
					/**
	 * method callExecuteList
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callExecuteList(HttpServletRequest request,IForm form) {
		playerVOs = (List)get(request.getSession(),form, "playerVOs");  
										 
					screen206 = (Screen206VO)get(request.getSession(),form, "screen206");  
										 
					try {
				// executing ExecuteList in lnk_back
				playerVOs = 	serviceScreen206.executeListPlayer(
			screen206
			);  
 
				put(request.getSession(), PLAYER_V_OS,playerVOs);
								// processing variables ExecuteList in lnk_back
				put(request.getSession(), SCREEN206,screen206); 
			
			} catch (ApplicationException e) { 
				// error handling for operation executeListPlayer called ExecuteList
				errorLogger("An error occured during the execution of the operation : executeListPlayer",e); 
		
			}
	}
		
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("UC206_DetailsPlayerController [ ");
			strBToS.append(SELECTED_PLAYER);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(selectedPlayer);
			strBToS.append(END_TO_STRING_DELIMITER);
				strBToS.append(SCREEN206);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(screen206);
			strBToS.append(END_TO_STRING_DELIMITER);
			strBToS.append(PLAYER_V_OS);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(playerVOs);
			strBToS.append(END_TO_STRING_DELIMITER);
		// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(uC206_DetailsPlayerValidator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsUC206_DetailsPlayerController!=null); 
		return strBToS.toString();
	}
}
