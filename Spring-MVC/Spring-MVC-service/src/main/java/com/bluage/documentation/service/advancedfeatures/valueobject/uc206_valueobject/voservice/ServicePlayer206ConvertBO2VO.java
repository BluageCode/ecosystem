/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.service.advancedfeatures.valueobject.uc206_valueobject.voservice;


import javax.inject.Inject;
import javax.inject.Named;

import com.bluage.documentation.business.advancedfeatures.valueobject.uc206_valueobject.bos.Player206BO;
import com.bluage.documentation.business.advancedfeatures.valueobject.uc206_valueobject.vo.Player206VO;
import com.bluage.documentation.service.AbstractService;
import com.netfective.bluage.core.exception.ApplicationException;


/**
 * Defines  application VO services for ServicePlayer206ConvertBO2VO
 *
 */
@Named ("com.bluage.documentation.service.advancedfeatures.valueobject.uc206_valueobject.voservice.ServicePlayer206ConvertBO2VO")
public class ServicePlayer206ConvertBO2VO  extends AbstractService{
					@Inject
		@Named ("com.bluage.documentation.service.advancedfeatures.valueobject.uc206_valueobject.voservice.ServicePlayer206CompleteVO")
		private ServicePlayer206CompleteVO instanceServicePlayer206CompleteVO;
					@Inject
		@Named ("com.bluage.documentation.service.advancedfeatures.valueobject.uc206_valueobject.voservice.ServicePlayer206Copy") 
		private ServicePlayer206Copy instanceServicePlayer206Copy;
			

	/**
	 *  Default constructor 
	 */
	public ServicePlayer206ConvertBO2VO() {
		super();
	}



			
	
	/**    
	 * VO operation.
	 * @param bo {@link Player206BO}
		 * @return {@link Player206VO}
	 */
	public Player206VO player206Convert(Player206BO bo )
 throws ApplicationException {
return privatePlayer206Convert(bo);
	}


	
		/**    
	 * Convert operation .
	 * @param bo {@link Player206BO}
		 * @return {@link Player206VO}
	 */
	private Player206VO privatePlayer206Convert(Player206BO bo )
 throws ApplicationException {
// ServicePlayer206Copy
		Player206VO vo = instanceServicePlayer206Copy.player206CopyBO2VO(bo);
		// ServicePlayer206CompleteVO	
		instanceServicePlayer206CompleteVO.player206CompleteFieldAsString(vo);
		return vo;		
	}

		
				

	
	/**    
	 * VO operation.
	 * @param bo {@link Player206BO}
		 * @return {@link Player206VO}
	 */
	public Player206VO player206ConvertWithFks(Player206BO bo )
 throws ApplicationException {
return privatePlayer206ConvertWithFks(bo);
	}


	
		/**    
	 * Convert with relations operation .
	 * @param bo {@link Player206BO}
		 * @return {@link Player206VO}
	 */
	private Player206VO privatePlayer206ConvertWithFks(Player206BO bo )
 throws ApplicationException {
Player206VO vo = player206Convert(bo);
		// ServicePlayer206CompleteVO
		instanceServicePlayer206CompleteVO.player206CompleteFks(vo, bo);	
		return vo;		
	}

		
				

	
	/**    
	 * VO operation.
	 * @param bo {@link Player206BO}
		 * @return {@link Player206VO}
	 */
	public Player206VO player206ConvertWithRelations(Player206BO bo )
 throws ApplicationException {
return privatePlayer206ConvertWithRelations(bo);
	}


	
		/**    
	 * Convert with relations operation .
	 * @param bo {@link Player206BO}
		 * @return {@link Player206VO}
	 */
	private Player206VO privatePlayer206ConvertWithRelations(Player206BO bo )
 throws ApplicationException {
Player206VO vo = player206Convert(bo);
		// ServicePlayer206CompleteVO
				instanceServicePlayer206CompleteVO.player206CompleteFks(vo, bo);
		instanceServicePlayer206CompleteVO.player206CompleteRelations(vo, bo);
		return vo;		
	}

		}
