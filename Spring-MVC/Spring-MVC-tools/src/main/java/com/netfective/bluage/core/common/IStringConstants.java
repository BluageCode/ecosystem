/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.netfective.bluage.core.common;

/**
 * This interface holds string constants mainly used for logging purpose;
 */
public interface IStringConstants {

	/**
	* Property:MESSAGE_SEPARATOR
	*/
	String MESSAGE_SEPARATOR = "_";
	
	/**
	 * Property:CONTROLLER
	 */
	String CONTROLLER = "Controller";

	/**
	 * Property:PRE_CONTROLLER
	 */
	String PRE_CONTROLLER = "PreController";

	/**
	 * Property:CONTROLLER_NAME
	 */
	String CONTROLLER_NAME = "name";

	/**
	 * Property:PRE_CONTROLLER_CLASS
	 */
	String PRE_CONTROLLER_CLASS = "class";

	/**
	 * Property:PRE_CONTROLLER_INIT
	 */
	String PRE_CONTROLLER_INIT = "init";

	/**
	 * Property:PRE_CONTROLLER_INITIALIZE
	 */
	String PRE_CONTROLLER_INITIALIZE = "initialize";

	/**
	 * Property:FILE_NAME
	 */
	String FILE_NAME = "preController-config.xml";

	/**
	 * L_BEG_METHOD
	 */
	String L_BEG_METHOD = "Begin method : ";

	/**
	 * L_END_METHOD
	 */
	String L_END_METHOD = "End method : ";

	/**
	 * s L_SOURCE_SCREEN
	 */
	String L_SOURCE_SCREEN = "Source Screen : ";

	/**
	 * L_DEST_SCREEN
	 */
	String L_DEST_SCREEN = "Destination Screen : ";

	/**
	 * L_FORWARD
	 */
	String L_FORWARD = " forward";

	/**
	 * L_FORWARD_DIRECT
	 */
	String L_FORWARD_DIRECT = "forwardDirect";

	/**
	 * L_APP_EXC_RELOAD_PAGE
	 */
	String L_APP_EXC_RELOAD_PAGE = "ApplicationException::reloadPageBecauseException";

	/**
	 * L_RELOAD_PAGE
	 */
	String L_RELOAD_PAGE = "reloadPageBecauseException";

	/**
	 * L_ACTION_NAME
	 */
	String L_ACTION_NAME = "Action name : ";

	/**
	 * L_OBT_INS_BY_NAME
	 */
	String L_OBT_INS_BY_NAME = "Obtaining instance by name : ";

	/**
	 * L_INS_NAME
	 */
	String L_INS_NAME = "instance name : ";

	/**
	 * L_CLASS_NAME
	 */
	String L_CLASS_NAME = "Class name: ";

	/**
	 * L_OBT_INS_BY_CLASS_NAME
	 */
	String L_OBT_INS_BY_CLASS_NAME = "Obtaining the instance by class name: ";

	/**
	 * L_SER_CONTR_IMPL
	 */
	String L_SER_CONTR_IMPL = "ServiceControllerImpl";

	/**
	 * L_MESS_BAD_TYPE_FOR_SER_PARAM
	 */
	String L_MESS_BAD_TYPE_FOR_SER_PARAM = "l'objet extrait du DataManager et representant le parametre du service n'a pas le type attendu";

	/**
	 * L_OBJECT
	 */
	String L_OBJECT = "Object";

	/**
	 * L_NAVIGATION_OPERATION
	 */
	String L_NAVIGATION_OPERATION = "; NAVIGATION OPERATION=";

	/**
	 * L_NULL
	 */
	String L_NULL = "null";

	/**
	 * L_NON_VALIDE
	 */
	String L_NON_VALIDE = "' non valide";

	/**
	 * SERVICE_RETURN_NOK = "NOK";
	 */
	String SERVICE_RETURN_NOK = "NOK";

	/**
	 * SERVICE_RETURN_OK = "OK";
	 */
	String SERVICE_RETURN_OK = "OK";

	/**
	 * NO_INSTANCE_NAME
	 */
	String NO_INSTANCE_NAME = "NO_INSTANCE_NAME";

	/**
	 * The name of the client cxf configuration file 'cxf-client.xml' String
	 * constant.
	 */
	String CXF_CLIENT_XML = "cxf-client.xml";//$NON-NLS-1$
	
	/**
	 * LIST_INSTANCES
	 */
	String LIST_INSTANCES = "listInstances";
	
	/**
	 * LIST_INSTANCES
	 */
	String PARENT_PATH_SCREEN_NAME = "parentpathscreenname";
	
	/**
	 * PARENT_HELPER_NAME
	 */
	String PARENT_HELPER_NAME = "parenthelpername";

}
