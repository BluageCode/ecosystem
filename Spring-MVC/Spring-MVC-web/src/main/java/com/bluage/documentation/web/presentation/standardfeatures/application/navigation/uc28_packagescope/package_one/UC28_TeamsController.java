/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.application.navigation.uc28_packagescope.package_one ;

// Import declaration.
// Java imports.
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.bluage.documentation.service.standardfeatures.application.navigation.uc28_packagescope.ServiceStadium28;
import com.bluage.documentation.service.standardfeatures.application.navigation.uc28_packagescope.ServiceState28;
import com.bluage.documentation.web.presentation.standardfeatures.application.navigation.uc28_packagescope.package_two.UC28_StadiumsForm;
import com.bluage.documentation.web.presentation.standardfeatures.application.navigation.uc28_packagescope.package_two.UC28_StatesForm;
import com.netfective.bluage.core.exception.ApplicationException;
import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;
import com.netfective.bluage.core.web.IForm;

/**
 * Class : UC28_TeamsController
 */
@Controller
@Scope("prototype")
@SessionAttributes("uC28_TeamsForm")
@RequestMapping(value= "/presentation/standardfeatures/application/navigation/uc28_packagescope/package_one")
public class UC28_TeamsController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC28_TeamsController.class);
	
	//Current form name
	private static final String U_C28__TEAMS_FORM = "uC28_TeamsForm";

	//CONSTANT: allTeams table or repeater.
	private static final String ALL_TEAMS = "allTeams";
				//CONSTANT: allStates
	private static final String ALL_STATES = "allStates";
	//CONSTANT: allStadiums
	private static final String ALL_STADIUMS = "allStadiums";
	
	/**
	 * Property:customDateEditorsUC28_TeamsController
	 */
	final private Map<String,CustomDateEditor> customDateEditorsUC28_TeamsController = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:uC28_TeamsValidator
	 */
	final private UC28_TeamsValidator uC28_TeamsValidator = new UC28_TeamsValidator();
	
	/**
	 * Service declaration : serviceState28.
	 */
	@Autowired
	private ServiceState28 serviceState28;
	
	/**
	 * Service declaration : serviceStadium28.
	 */
	@Autowired
	private ServiceStadium28 serviceStadium28;
	
	/**
	 * Pre Controller declaration : uC28_PreControllerFindAllTeams
	 */
	@Autowired
	private UC28_PreControllerFindAllTeamsController uC28_PreControllerFindAllTeams;

	
	// Initialise all the instances for UC28_TeamsController
		// Initialize the instance allTeams for the state findPages
		private List allTeams; // Initialize the instance allTeams for UC28_TeamsController
						// Declare the instance allStates
		private List allStates;
		// Declare the instance allStadiums
		private List allStadiums;
			/**
	 * Operation : lnk_states
 	 * @param model : 
 	 * @param uC28_Teams : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC28_Teams/lnk_states.html",method = RequestMethod.POST)
	public String lnk_states(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC28_TeamsForm") UC28_TeamsForm  uC28_TeamsForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : lnk_states"); 
		infoLogger(uC28_TeamsForm); 
		uC28_TeamsForm.setAlwaysCallPreControllers(false); 
		// initialization for lnk_states
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state lnk_states if the validation fail, you will be forward to the screen : "/presentation/standardfeatures/application/navigation/uc28_packagescope/package_one/UC28_Teams"
		if(errorsMessages(request,response,uC28_TeamsForm,bindingResult,"", uC28_TeamsValidator, customDateEditorsUC28_TeamsController)){ 
			return "/presentation/standardfeatures/application/navigation/uc28_packagescope/package_one/UC28_Teams"; 
		}

	 callFindallstates(request,uC28_TeamsForm );
											// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.application.navigation.uc28_packagescope.package_two.UC28_StatesForm or not from lnk_states
			final UC28_StatesForm uC28_StatesForm =  new UC28_StatesForm();
			// Populate the destination form with all the instances returned from lnk_states.
			final IForm uC28_StatesForm2 = populateDestinationForm(request.getSession(), uC28_StatesForm); 
					
			// Add the form to the model.
			model.addAttribute("uC28_StatesForm", uC28_StatesForm2); 
			
			request.getSession().setAttribute("uC28_StatesForm", uC28_StatesForm2);
			
			// "OK" CASE => destination screen path from lnk_states
			LOGGER.info("Go to the screen 'UC28_States'.");
			// Redirect (PRG) from lnk_states
			destinationPath =  "redirect:/presentation/standardfeatures/application/navigation/uc28_packagescope/package_two/UC28_States.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_states 
	}
	
				/**
	 * Operation : lnk_stadiums
 	 * @param model : 
 	 * @param uC28_Teams : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC28_Teams/lnk_stadiums.html",method = RequestMethod.POST)
	public String lnk_stadiums(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC28_TeamsForm") UC28_TeamsForm  uC28_TeamsForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : lnk_stadiums"); 
		infoLogger(uC28_TeamsForm); 
		uC28_TeamsForm.setAlwaysCallPreControllers(false); 
		// initialization for lnk_stadiums
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state lnk_stadiums if the validation fail, you will be forward to the screen : "/presentation/standardfeatures/application/navigation/uc28_packagescope/package_one/UC28_Teams"
		if(errorsMessages(request,response,uC28_TeamsForm,bindingResult,"", uC28_TeamsValidator, customDateEditorsUC28_TeamsController)){ 
			return "/presentation/standardfeatures/application/navigation/uc28_packagescope/package_one/UC28_Teams"; 
		}

	 callFindallstadiums(request,uC28_TeamsForm );
											// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.application.navigation.uc28_packagescope.package_two.UC28_StadiumsForm or not from lnk_stadiums
			final UC28_StadiumsForm uC28_StadiumsForm =  new UC28_StadiumsForm();
			// Populate the destination form with all the instances returned from lnk_stadiums.
			final IForm uC28_StadiumsForm2 = populateDestinationForm(request.getSession(), uC28_StadiumsForm); 
					
			// Add the form to the model.
			model.addAttribute("uC28_StadiumsForm", uC28_StadiumsForm2); 
			
			request.getSession().setAttribute("uC28_StadiumsForm", uC28_StadiumsForm2);
			
			// "OK" CASE => destination screen path from lnk_stadiums
			LOGGER.info("Go to the screen 'UC28_Stadiums'.");
			// Redirect (PRG) from lnk_stadiums
			destinationPath =  "redirect:/presentation/standardfeatures/application/navigation/uc28_packagescope/package_two/UC28_Stadiums.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_stadiums 
	}
	
	
	/**
	* This method initialise the form : UC28_TeamsForm 
	* @return UC28_TeamsForm
	*/
	@ModelAttribute("UC28_TeamsFormInit")
	public void initUC28_TeamsForm(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, U_C28__TEAMS_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method UC28_TeamsForm."); //for lnk_stadiums 
		}
		UC28_TeamsForm uC28_TeamsForm;
	
		if(request.getSession().getAttribute(U_C28__TEAMS_FORM) != null){
			uC28_TeamsForm = (UC28_TeamsForm)request.getSession().getAttribute(U_C28__TEAMS_FORM);
		} else {
			uC28_TeamsForm = new UC28_TeamsForm();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form UC28_TeamsForm.");
		}
		uC28_TeamsForm = (UC28_TeamsForm)populateDestinationForm(request.getSession(), uC28_TeamsForm);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  UC28_TeamsForm.");
		}
		model.addAttribute(U_C28__TEAMS_FORM, uC28_TeamsForm);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param UC28_TeamsForm : The sceen form.
	 */
	@RequestMapping(value = "/UC28_Teams.html" ,method = RequestMethod.GET)
	public void prepareUC28_Teams(final HttpServletRequest request,final  Model model,final  @ModelAttribute("UC28_TeamsFormInit") UC28_TeamsForm uC28_TeamsForm){
		
		UC28_TeamsForm currentUC28_TeamsForm = uC28_TeamsForm;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(U_C28__TEAMS_FORM) == null){
			if(currentUC28_TeamsForm!=null){
				request.getSession().setAttribute(U_C28__TEAMS_FORM, currentUC28_TeamsForm);
			}else {
				currentUC28_TeamsForm = new UC28_TeamsForm();
				request.getSession().setAttribute(U_C28__TEAMS_FORM, currentUC28_TeamsForm);	
			}
		} else {
			currentUC28_TeamsForm = (UC28_TeamsForm) request.getSession().getAttribute(U_C28__TEAMS_FORM);
		}

					currentUC28_TeamsForm = (UC28_TeamsForm)populateDestinationForm(request.getSession(), currentUC28_TeamsForm);
		// Call all the Precontroller.
	if ( currentUC28_TeamsForm.isAlwaysCallPreControllers()) {
	
		// Calling the PreController : UC28_PreControllerFindAllTeams.
		currentUC28_TeamsForm = (UC28_TeamsForm) uC28_PreControllerFindAllTeams.uC28_PreControllerFindAllTeamsControllerInit(request, model, currentUC28_TeamsForm);
		
	}
	currentUC28_TeamsForm.setAlwaysCallPreControllers(true);
		model.addAttribute(U_C28__TEAMS_FORM, currentUC28_TeamsForm);
		request.getSession().setAttribute(U_C28__TEAMS_FORM, currentUC28_TeamsForm);
		
	}
	
				/**
	 * method callFindallstates
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callFindallstates(HttpServletRequest request,IForm form) {
		allStates = (List)get(request.getSession(),form, "allStates");  
										 
					try {
				// executing Findallstates in lnk_stadiums
				allStates = 	serviceState28.state28FindAll(
	);  
 
				put(request.getSession(), ALL_STATES,allStates);
								// processing variables Findallstates in lnk_stadiums

			} catch (ApplicationException e) { 
				// error handling for operation state28FindAll called Findallstates
				errorLogger("An error occured during the execution of the operation : state28FindAll",e); 
		
			}
	}
		/**
	 * method callFindallstadiums
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callFindallstadiums(HttpServletRequest request,IForm form) {
		allStadiums = (List)get(request.getSession(),form, "allStadiums");  
										 
					try {
				// executing Findallstadiums in lnk_stadiums
				allStadiums = 	serviceStadium28.stadium28FindAll(
	);  
 
				put(request.getSession(), ALL_STADIUMS,allStadiums);
								// processing variables Findallstadiums in lnk_stadiums

			} catch (ApplicationException e) { 
				// error handling for operation stadium28FindAll called Findallstadiums
				errorLogger("An error occured during the execution of the operation : stadium28FindAll",e); 
		
			}
	}
		
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("UC28_TeamsController [ ");
			strBToS.append(ALL_TEAMS);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(allTeams);
			strBToS.append(END_TO_STRING_DELIMITER);
						strBToS.append(ALL_STATES);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(allStates);
			strBToS.append(END_TO_STRING_DELIMITER);
			strBToS.append(ALL_STADIUMS);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(allStadiums);
			strBToS.append(END_TO_STRING_DELIMITER);
		// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(uC28_TeamsValidator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsUC28_TeamsController!=null); 
		return strBToS.toString();
	}
}
