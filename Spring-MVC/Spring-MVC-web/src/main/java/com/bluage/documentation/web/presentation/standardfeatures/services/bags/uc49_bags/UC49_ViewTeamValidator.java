/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.services.bags.uc49_bags ;


import org.apache.log4j.Logger;
import org.springframework.validation.Errors;

import com.netfective.bluage.core.validator.AbstractValidator;
/**
* Class :UC49_ViewTeamValidator
*/
public class UC49_ViewTeamValidator extends AbstractValidator{
	
	// LOGGER for the class UC49_ViewTeamValidator
	private static final Logger LOGGER = Logger.getLogger( UC49_ViewTeamValidator.class);
	
	
	/**
	* Operation validate for UC49_ViewTeamForm
	* @param obj : the current form (UC49_ViewTeamForm)
	* @param errors : The spring errors to return for the form UC49_ViewTeamForm
	*/
	public void validate(final Object obj,final Errors errors){
		LOGGER.info("Beginning method : validate"); // UC49_ViewTeamValidator
		UC49_ViewTeamForm cUC49_ViewTeamForm = (UC49_ViewTeamForm)obj; // UC49_ViewTeamValidator
		LOGGER.info("Ending method : validate the form "+ cUC49_ViewTeamForm.getClass().getName()); // UC49_ViewTeamValidator
	}

	/**
	* Method to implements to use spring validators (UC49_ViewTeamForm)
	* @param aClass : Class for the form UC49_ViewTeamForm
	* @return boolean
	*/
	public boolean supports(final Class aClass){
		return (new UC49_ViewTeamForm()).getClass().equals(aClass); // UC49_ViewTeamValidator
	}
}
