/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.advancedfeatures.valueobject.uc206_valueobject;


import java.io.Serializable;
import java.util.List;

import com.bluage.documentation.business.advancedfeatures.valueobject.uc206_valueobject.vo.Screen206VO;
import com.netfective.bluage.core.web.AbstractForm;
import com.netfective.bluage.core.web.IForm;

/**
* Class : UC206_CreatePlayerForm
*/
public class UC206_CreatePlayerForm extends AbstractForm implements Serializable, IForm {
	
	/** Constant : serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	//CONSTANT : screen206
	private static final String SCREEN206 = "screen206";
	//CONSTANT : listPosition206
	private static final String LIST_POSITION206 = "listPosition206";
	/**
	 * 	Property: screen206 
	 */
	private Screen206VO screen206;
	/**
	 * 	Property: listPosition206 
	 */
	private List listPosition206;
/**
	 * Default constructor : UC206_CreatePlayerForm
	 */
	public UC206_CreatePlayerForm() {
		super();
		// Initialize : screen206
		this.screen206 = new Screen206VO();
		// Initialize : listPosition206
		this.listPosition206 = new java.util.ArrayList();
			this.setAlwaysCallPreControllers(true);
	}
	
	/**
	 * 	Getter : screen206 
	 * 	@return : Return the screen206 instance.
	 */
	public Screen206VO getScreen206(){
		return screen206; // For UC206_CreatePlayerForm
	}
	
	/**
	 * 	Setter : screen206 
	 *  @param screen206instance : The instance to set.
	 */
	public void setScreen206(final Screen206VO screen206instance){
		this.screen206 = screen206instance;// For UC206_CreatePlayerForm
	}
	/**
	 * 	Getter : listPosition206 
	 * 	@return : Return the listPosition206 instance.
	 */
	public List getListPosition206(){
		return listPosition206; // For UC206_CreatePlayerForm
	}
	
	/**
	 * 	Setter : listPosition206 
	 *  @param listPosition206instance : The instance to set.
	 */
	public void setListPosition206(final List listPosition206instance){
		this.listPosition206 = listPosition206instance;// For UC206_CreatePlayerForm
	}
/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UC206_CreatePlayerForm [ "+
SCREEN206 +" = " + screen206 +LIST_POSITION206 +" = " + listPosition206 + "]";
	}
	
	/**
	 * Method setInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public void setInstanceByInstanceName(final String instanceName, final Object instance) {
		// Set the instance Screen206.
		if(SCREEN206.equals(instanceName)){// For UC206_CreatePlayerForm
			this.setScreen206((Screen206VO)instance); // For UC206_CreatePlayerForm
		}
				// Set the instance ListPosition206.
		if(LIST_POSITION206.equals(instanceName)){// For UC206_CreatePlayerForm
			this.setListPosition206((List)instance); // For UC206_CreatePlayerForm
		}
				// Parent instance name for UC206_CreatePlayerForm
		if(SCREEN206.equals(instanceName) && screen206 != null){ // For UC206_CreatePlayerForm
			listPosition206.clear(); // For UC206_CreatePlayerForm
			listPosition206.addAll(screen206.getListPosition206());// For UC206_CreatePlayerForm
		}
		
	}
	
	/**
	 * Method getInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public Object getInstanceByInstanceName(final String instanceName){
		// Temporary object for tmpUC206_CreatePlayerForm.
		Object tmpUC206_CreatePlayerForm = null;
		
			
		// Get the instance Screen206 for UC206_CreatePlayerForm.
		if(SCREEN206.equals(instanceName)){ // For UC206_CreatePlayerForm
			tmpUC206_CreatePlayerForm = this.getScreen206(); // For UC206_CreatePlayerForm
		}
			
		// Get the instance ListPosition206 for UC206_CreatePlayerForm.
		if(LIST_POSITION206.equals(instanceName)){ // For UC206_CreatePlayerForm
			tmpUC206_CreatePlayerForm = this.getListPosition206(); // For UC206_CreatePlayerForm
		}
		return tmpUC206_CreatePlayerForm;// For UC206_CreatePlayerForm
	}
	
	}
