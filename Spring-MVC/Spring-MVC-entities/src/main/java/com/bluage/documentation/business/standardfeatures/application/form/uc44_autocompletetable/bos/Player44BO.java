/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.business.standardfeatures.application.form.uc44_autocompletetable.bos;

import javax.inject.Inject;
import javax.inject.Named;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import com.bluage.documentation.business.common.bos.PlayerBO;
import com.bluage.documentation.business.standardfeatures.application.form.uc44_autocompletetable.entities.daofinder.Player44DAOFinderImpl;

/**
 * Player44BO
 *
* @see com.netfective.bluage.core.business.IBusinessObject
* * @see PlayerBO
 */
@XmlRootElement

public class Player44BO
 extends PlayerBO {
	
	/**
	 * Default SUID for Player44BO.
	 */
	private static final long serialVersionUID = 1L;
	

	@Inject
	@Named ("com.bluage.documentation.business.standardfeatures.application.form.uc44_autocompletetable.entities.daofinder.Player44Finder") 
	private Player44DAOFinderImpl finder;

												/**
	 * Entity property: team44
	 *
	 */
private Team44BO team44;



   /**
	* public default constructor for class Player44BO
	*/
	public Player44BO(){
		//NOP Player44BO
	}


												/**
	 * Getter for team44 of class Player44BO.
	 *
	 * @return {@link com.bluage.documentation.business.standardfeatures.application.form.uc44_autocompletetable.bos.Team44BO}
	 * @generated
	 */ 
public  Team44BO getTeam44() {
		Team44BO team440 = this.team44;
		if(this.team44 == null) {
			if(DAO_MODE.equals(this.getObjMode( ) ) ) {
				team440 = null;
			} else {
				this.team44 = new Team44BO();
				this.team44.setObjMode( this.getObjMode( ) );
				team440 = this.team44;
			}
		} else {
			this.team44.setObjMode( this.getObjMode( ) );
			if( HELPER_MODE.equals(this.getObjMode( ) ) ) {
				team440 = this.team44;
			}
			if( DAO_MODE.equals(this.getObjMode()) && this.team44.isEmpty ) {
				team440 = null;
			}
		}
		return team440;
	}


	/**
	 * Setter for team44 of class Player44BO.
	 * @param value {@link com.bluage.documentation.business.standardfeatures.application.form.uc44_autocompletetable.bos.Team44BO}
	 * @generated
	 */

public  void setTeam44(final Team44BO team44) {
	if(team44 != null) {
		team44.isEmpty = false;
		team44.setObjMode( this.getObjMode( ) );
	}
	this.isEmpty = false;
	this.team44 = team44;
}


/**
	 * Returns <code>true</code> if the argument is an Player44 instance and all identifiers for this entity
     * equal the identifiers of the argument entity. The <code>equals</code> method of the parent entity
     * will also need to return <code>true</code>. Returns <code>false</code> otherwise.
     *
     * @see PlayerBO#hashCode()
	 * @param object  object to compare with current instance      
     * @return boolean true if equals false if not
     */
    public boolean equals(final Object object )
    {
		if ( this == object )
        {
            return true;
        }
        if ( !( object instanceof Player44BO ) )
        {
            return false;
        }
        final Player44BO that = (Player44BO)object;
    
    	            if (        				        			this.getId() == null
        	        ) {
        	return false;
        }
		return new EqualsBuilder()
			.append(this.getId(), that.getId())
			 	.isEquals();
	}
	/**

	 * Returns a hash code based on this entity's identifiers and the hash code of the parent entity.
     *
     * @see PlayerBO#hashCode()
     * @return int
     */    
    public int hashCode() {
        return new HashCodeBuilder()
             .append(getId())
				 	.appendSuper(super.hashCode())
            .toHashCode();
    }
}

