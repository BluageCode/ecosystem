<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file="/include.jsp"  %>
<html >
<head >
<title  > 
<fmt:message key="cUC28_Players_BLU_AGE_DOCUMENTATION"/>
</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"  > 
</meta>
<link href="<c:url value= "/presentation/_style/_style.css"/>" rel="stylesheet" type="text/css"  /> 
<link href="<c:url value= "/presentation/_include/jquery/rd_lib.css"/>" rel="stylesheet" type="text/css"  /> 
<script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/jquery.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/rd_setup.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/rd_lib.js"/>" space="preserve"  ></script><link rel="shortcut icon" href="<c:url value= "/presentation/_style/favicon.ico"/>"  /> 

</head>

<body class=" " > 
<div id="rd_ghost"  > 
<small  > 
<fmt:message key="cUC28_Players___CSS_DEACTIVATED"/>
</small>
</div>
<div id="rd_jghost"  > 
<small  > 
<fmt:message key="cUC28_Players___JAVASCRIPT_DEACTIVATED"/>
</small>
</div>
<div id="shape"  > 
<div class="ui-layout-center"  > 
<div class="east-north"  > 
	<form:form id="uC28_Players" commandName="uC28_PlayersForm"  enctype="application/x-www-form-urlencoded" method="post">
<h2 class="rd_first"  > 
<fmt:message key="cUC28_Players_STANDARD_FEATURES__APPLICATIO"/>
</h2>
<h2 class="rd_first"  > 
<fmt:message key="cUC28_Players_PACKAGE_SCOPE"/>
</h2>
<br clear="none"  /> 
<br clear="none"  /> 
<table class="rd_tableform2"  >
<tr  > 
<td rowspan="1" colspan="1"  >
<b  > 
<a id="lnk_teams"onclick="javascript:btn_click_cExecuteUC28_Players(this.href);return false;"				href="<c:url value="//presentation/standardfeatures/application/navigation/uc28_packagescope/package_one/UC28_Onglets_1/lnk_teams.html"/>"
							 shape="rect"  >									 
<fmt:message key="cUC28_Players_TEAMS"/>
</a>
</b>
</td> 
<td rowspan="1" colspan="1"  >
<b  > 
<a id="lnk_players"onclick="javascript:btn_click_cExecuteUC28_Players(this.href);return false;"				href="<c:url value="//presentation/standardfeatures/application/navigation/uc28_packagescope/package_one/UC28_Onglets_1/lnk_players.html"/>"
							 shape="rect"  >									 
<u  > 
<fmt:message key="cUC28_Players_PLAYERS"/>
</u>
</a>
</b>
</td> 
<td rowspan="1" colspan="1"  >
<b  > 
<a id="lnk_states"onclick="javascript:btn_click_cExecuteUC28_Players(this.href);return false;"				href="<c:url value="/presentation/standardfeatures/application/navigation/uc28_packagescope/package_one/UC28_Players/lnk_states.html"/>"
							 shape="rect"  >									 
<fmt:message key="cUC28_Players_STATES"/>
</a>
</b>
</td> 
<td rowspan="1" colspan="1"  >
<b  > 
<a id="lnk_stadiums"onclick="javascript:btn_click_cExecuteUC28_Players(this.href);return false;"				href="<c:url value="/presentation/standardfeatures/application/navigation/uc28_packagescope/package_one/UC28_Players/lnk_stadiums.html"/>"
							 shape="rect"  >									 
<fmt:message key="cUC28_Players_STADIUMS"/>
</a>
</b>
</td> 
</tr>
</table> 
<display:table uid="tab_Players"  sort="list" requestURI="" name="sessionScope.uC28_PlayersForm.allPlayers" class="rd_tabledata"  >
	<c:set   var="rownum">
		<%=(tab_Players !=	null) ? new Integer(tab_Players_rowNum.intValue() - 1) : new Integer(0) %>
	</c:set>
	<c:set   var="idRow">
		<%=(tab_Players != null ) ? tab_Players_rowNum.intValue() - 1 : -1 %>
	</c:set>
	<c:set   var="idTab">
		<%=(tab_Players != null ) ? "tab_Players" : null %>
	</c:set>
	<c:set var="tableHashId">
		<%= new org.displaytag.util.ParamEncoder("tab_Players").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE) %>
	</c:set>
	<c:set var="tableHashId2">
		<%=request.getParameter(new org.displaytag.util.ParamEncoder("tab_Players").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE)) %>
	</c:set>	
	
<display:column format=""  titleKey="cUC28_Players_NAME">
		<span id="dyn_lastname">
	<c:out value="${tab_Players.lastName}" />
		</span>
	<fmt:message key="cUC28_Players_"/>
		<span id="dyn_firstname">
	<c:out value="${tab_Players.firstName}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="cUC28_Players_DATE_OF_BIRTH">
<span id="dyn_dateofbirth">
	<format:formatDateValidator value="${tab_Players.dateOfBirth}" pattern="MM/dd/yyyy" />
</span>
	</display:column>
	
<display:column format=""  titleKey="cUC28_Players_WEIGHT_KG">
		<span id="dyn_weight">
	<c:out value="${tab_Players.weight}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="cUC28_Players_HEIGHT_CM">
		<span id="dyn_height">
	<c:out value="${tab_Players.height}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="cUC28_Players_ESTIMATED_VALUE_M">
		<span id="dyn_value">
	<c:out value="${tab_Players.estimatedValue}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="cUC28_Players_ROOKIE">
<form:checkbox id="chk_isRookie[${rownum}]" path="allPlayers[${rownum}].rookie"  name="isRookie" disabled="true"  
		/>
</display:column>
</display:table>
	</form:form>
	<script language="javascript">
		function btn_click_cExecuteUC28_Players(url) {
			UC28_Playersvar = document.getElementById('uC28_Players');
			UC28_Playersvar.setAttribute('action', url);
			UC28_Playersvar.submit();
		}
		function btn_click_cSetActionUC28_Players(url) {
			UC28_Playersvar = document.getElementById('uC28_Players');
			UC28_Playersvar.setAttribute('action', url);
		}
	</script>
</div>
<div class="east-center"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/application/navigation/uc28_packagescope/package_one/UC28_Playerscmp_tabs" />
</div>
</div>
<div class="ui-layout-north"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/application/navigation/uc28_packagescope/package_one/UC28_Playerscmp_header" />
</div>
<div class="ui-layout-south"  > 
<fmt:message key="cUC28_Players___"/>
<span id="rd_jversion"  > 
<fmt:message key="cUC28_Players_BLU_AGE_DOCUMENTATION_115"/>
</span>
</div>
<div class="ui-layout-west"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/application/navigation/uc28_packagescope/package_one/UC28_Playerscmp_treeview" />
</div>
</div>
</body>
</html>
