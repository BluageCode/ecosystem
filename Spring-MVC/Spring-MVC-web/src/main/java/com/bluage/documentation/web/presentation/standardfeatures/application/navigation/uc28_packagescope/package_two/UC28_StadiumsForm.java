/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.application.navigation.uc28_packagescope.package_two;


import java.io.Serializable;
import java.util.List;

import com.bluage.documentation.business.standardfeatures.application.navigation.uc28_packagescope.bos.Stadium28BO;
import com.netfective.bluage.core.web.AbstractForm;
import com.netfective.bluage.core.web.IForm;

/**
* Class : UC28_StadiumsForm
*/
public class UC28_StadiumsForm extends AbstractForm implements Serializable, IForm {
	
	/** Constant : serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	//CONSTANT : allStadiums
	private static final String ALL_STADIUMS = "allStadiums";
	//CONSTANT : allTeams
	private static final String ALL_TEAMS = "allTeams";
	//CONSTANT : allPlayers
	private static final String ALL_PLAYERS = "allPlayers";
	/**
	 * 	Property: allStadiums 
	 */
	private List<Stadium28BO> allStadiums;
	/**
	 * 	Property: allTeams 
	 */
	private List allTeams;
	/**
	 * 	Property: allPlayers 
	 */
	private List allPlayers;
/**
	 * Default constructor : UC28_StadiumsForm
	 */
	public UC28_StadiumsForm() {
		super();
		// Initialize : allStadiums
		this.allStadiums = new java.util.ArrayList<com.bluage.documentation.business.standardfeatures.application.navigation.uc28_packagescope.bos.Stadium28BO>();
		// Initialize : allTeams
		this.allTeams = null;
		// Initialize : allPlayers
		this.allPlayers = null;
					this.setAlwaysCallPreControllers(true);
	}
	
	/**
	 * 	Getter : allStadiums 
	 * 	@return : Return the allStadiums instance.
	 */
	public List<Stadium28BO> getAllStadiums(){
		return allStadiums; // For UC28_StadiumsForm
	}
	
	/**
	 * 	Setter : allStadiums 
	 *  @param allStadiumsinstance : The instance to set.
	 */
	public void setAllStadiums(final List<Stadium28BO> allStadiumsinstance){
		this.allStadiums = allStadiumsinstance;// For UC28_StadiumsForm
	}
	/**
	 * 	Getter : allTeams 
	 * 	@return : Return the allTeams instance.
	 */
	public List getAllTeams(){
		return allTeams; // For UC28_StadiumsForm
	}
	
	/**
	 * 	Setter : allTeams 
	 *  @param allTeamsinstance : The instance to set.
	 */
	public void setAllTeams(final List allTeamsinstance){
		this.allTeams = allTeamsinstance;// For UC28_StadiumsForm
	}
	/**
	 * 	Getter : allPlayers 
	 * 	@return : Return the allPlayers instance.
	 */
	public List getAllPlayers(){
		return allPlayers; // For UC28_StadiumsForm
	}
	
	/**
	 * 	Setter : allPlayers 
	 *  @param allPlayersinstance : The instance to set.
	 */
	public void setAllPlayers(final List allPlayersinstance){
		this.allPlayers = allPlayersinstance;// For UC28_StadiumsForm
	}
/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UC28_StadiumsForm [ "+
ALL_STADIUMS +" = " + allStadiums +ALL_TEAMS +" = " + allTeams +ALL_PLAYERS +" = " + allPlayers + "]";
	}
	
	/**
	 * Method setInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public void setInstanceByInstanceName(final String instanceName, final Object instance) {
		// Set the instance AllStadiums.
		if(ALL_STADIUMS.equals(instanceName)){// For UC28_StadiumsForm
			this.setAllStadiums((List<Stadium28BO>)instance); // For UC28_StadiumsForm
		}
				// Set the instance AllTeams.
		if(ALL_TEAMS.equals(instanceName)){// For UC28_StadiumsForm
			this.setAllTeams((List)instance); // For UC28_StadiumsForm
		}
				// Set the instance AllPlayers.
		if(ALL_PLAYERS.equals(instanceName)){// For UC28_StadiumsForm
			this.setAllPlayers((List)instance); // For UC28_StadiumsForm
		}
			}
	
	/**
	 * Method getInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public Object getInstanceByInstanceName(final String instanceName){
		// Temporary object for tmpUC28_StadiumsForm.
		Object tmpUC28_StadiumsForm = null;
		
			
		// Get the instance AllStadiums for UC28_StadiumsForm.
		if(ALL_STADIUMS.equals(instanceName)){ // For UC28_StadiumsForm
			tmpUC28_StadiumsForm = this.getAllStadiums(); // For UC28_StadiumsForm
		}
			
		// Get the instance AllTeams for UC28_StadiumsForm.
		if(ALL_TEAMS.equals(instanceName)){ // For UC28_StadiumsForm
			tmpUC28_StadiumsForm = this.getAllTeams(); // For UC28_StadiumsForm
		}
			
		// Get the instance AllPlayers for UC28_StadiumsForm.
		if(ALL_PLAYERS.equals(instanceName)){ // For UC28_StadiumsForm
			tmpUC28_StadiumsForm = this.getAllPlayers(); // For UC28_StadiumsForm
		}
		return tmpUC28_StadiumsForm;// For UC28_StadiumsForm
	}
	
			}
