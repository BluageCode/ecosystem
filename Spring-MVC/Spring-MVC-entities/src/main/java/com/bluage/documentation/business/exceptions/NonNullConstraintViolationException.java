/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package   com.bluage.documentation.business.exceptions;

// Import declaration
import org.springframework.dao.DataAccessException;


/**
* Class :NonNullConstraintViolationException
*/
public class NonNullConstraintViolationException extends DataAccessException {


	/**
	* This method Non Null Constraint Violation Exception 
	* @param msg : String
	*/
	public NonNullConstraintViolationException(String msg){

		super(msg);

	}


	/**
	* This method Non Null Constraint Violation Exception 
	* @param msg : String
	* @param ex : Throwable
	*/
	public NonNullConstraintViolationException(String msg,Throwable ex){

		super(msg,ex);

	}
}


