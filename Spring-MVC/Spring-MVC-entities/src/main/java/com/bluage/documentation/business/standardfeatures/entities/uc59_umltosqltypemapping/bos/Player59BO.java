/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.business.standardfeatures.entities.uc59_umltosqltypemapping.bos;

import java.util.Date;

import javax.inject.Inject;
import javax.inject.Named;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import com.bluage.documentation.business.common.bos.PersonBO;
import com.bluage.documentation.business.standardfeatures.entities.uc59_umltosqltypemapping.entities.daofinder.Player59DAOFinderImpl;

/**
 * Player59BO
 *
* @see com.netfective.bluage.core.business.IBusinessObject
* * @see PersonBO
 */
@XmlRootElement

public class Player59BO
 extends PersonBO {
	
	/**
	 * Default SUID for Player59BO.
	 */
	private static final long serialVersionUID = 1L;
	

	@Inject
	@Named ("com.bluage.documentation.business.standardfeatures.entities.uc59_umltosqltypemapping.entities.daofinder.Player59Finder") 
	private Player59DAOFinderImpl finder;

						/**
	 * Entity property: nickname
	 *
	 */
private String nickname;

											/**
	 * Entity property: mainPosition
	 *
	 */
private Position59BO mainPosition;

											/**
	 * Entity property: secondaryPosition
	 *
	 */
private Position59BO secondaryPosition;

					/**
	 * Entity property: dateOfBirth
	 *
	 */
private Date dateOfBirth;

					/**
	 * Entity property: estimatedValue
	 *
	 */
private Float estimatedValue;

					/**
	 * Entity property: weight
	 *
	 */
private Integer weight;

					/**
	 * Entity property: height
	 *
	 */
private Integer height;

					/**
	 * Entity property: rookie
	 *
	 */
private Boolean rookie;



   /**
	* public default constructor for class Player59BO
	*/
	public Player59BO(){
		//NOP Player59BO
	}


						/**
	 * Getter for nickname of class Player59BO.
	 *
	 * @return {@link java.lang.String}
	 * @generated
	 */ 
public  String getNickname() {
	return this.nickname;
	}


	/**
	 * Setter for nickname of class Player59BO.
	 * @param value {@link java.lang.String}
	 * @generated
	 */

public  void setNickname(final String nickname) {
	this.nickname = nickname;
}

											/**
	 * Getter for mainPosition of class Player59BO.
	 *
	 * @return {@link com.bluage.documentation.business.standardfeatures.entities.uc59_umltosqltypemapping.bos.Position59BO}
	 * @generated
	 */ 
public  Position59BO getMainPosition() {
		Position59BO mainPosition0 = this.mainPosition;
		if(this.mainPosition == null) {
			if(DAO_MODE.equals(this.getObjMode( ) ) ) {
				mainPosition0 = null;
			} else {
				this.mainPosition = new Position59BO();
				this.mainPosition.setObjMode( this.getObjMode( ) );
				mainPosition0 = this.mainPosition;
			}
		} else {
			this.mainPosition.setObjMode( this.getObjMode( ) );
			if( HELPER_MODE.equals(this.getObjMode( ) ) ) {
				mainPosition0 = this.mainPosition;
			}
			if( DAO_MODE.equals(this.getObjMode()) && this.mainPosition.isEmpty ) {
				mainPosition0 = null;
			}
		}
		return mainPosition0;
	}


	/**
	 * Setter for mainPosition of class Player59BO.
	 * @param value {@link com.bluage.documentation.business.standardfeatures.entities.uc59_umltosqltypemapping.bos.Position59BO}
	 * @generated
	 */

public  void setMainPosition(final Position59BO mainPosition) {
	if(mainPosition != null) {
		mainPosition.isEmpty = false;
		mainPosition.setObjMode( this.getObjMode( ) );
	}
	this.isEmpty = false;
	this.mainPosition = mainPosition;
}

											/**
	 * Getter for secondaryPosition of class Player59BO.
	 *
	 * @return {@link com.bluage.documentation.business.standardfeatures.entities.uc59_umltosqltypemapping.bos.Position59BO}
	 * @generated
	 */ 
public  Position59BO getSecondaryPosition() {
		Position59BO secondaryPosition0 = this.secondaryPosition;
		if(this.secondaryPosition == null) {
			if(DAO_MODE.equals(this.getObjMode( ) ) ) {
				secondaryPosition0 = null;
			} else {
				this.secondaryPosition = new Position59BO();
				this.secondaryPosition.setObjMode( this.getObjMode( ) );
				secondaryPosition0 = this.secondaryPosition;
			}
		} else {
			this.secondaryPosition.setObjMode( this.getObjMode( ) );
			if( HELPER_MODE.equals(this.getObjMode( ) ) ) {
				secondaryPosition0 = this.secondaryPosition;
			}
			if( DAO_MODE.equals(this.getObjMode()) && this.secondaryPosition.isEmpty ) {
				secondaryPosition0 = null;
			}
		}
		return secondaryPosition0;
	}


	/**
	 * Setter for secondaryPosition of class Player59BO.
	 * @param value {@link com.bluage.documentation.business.standardfeatures.entities.uc59_umltosqltypemapping.bos.Position59BO}
	 * @generated
	 */

public  void setSecondaryPosition(final Position59BO secondaryPosition) {
	if(secondaryPosition != null) {
		secondaryPosition.isEmpty = false;
		secondaryPosition.setObjMode( this.getObjMode( ) );
	}
	this.isEmpty = false;
	this.secondaryPosition = secondaryPosition;
}

					/**
	 * Getter for dateOfBirth of class Player59BO.
	 *
	 * @return {@link java.util.Date}
	 * @generated
	 */ 
public  Date getDateOfBirth() {
	return this.dateOfBirth;
	}


	/**
	 * Setter for dateOfBirth of class Player59BO.
	 * @param value {@link java.util.Date}
	 * @generated
	 */

public  void setDateOfBirth(final Date dateOfBirth) {
	this.dateOfBirth = dateOfBirth;
}

					/**
	 * Getter for estimatedValue of class Player59BO.
	 *
	 * @return {@link java.lang.Float}
	 * @generated
	 */ 
public  Float getEstimatedValue() {
	return this.estimatedValue;
	}


	/**
	 * Setter for estimatedValue of class Player59BO.
	 * @param value {@link java.lang.Float}
	 * @generated
	 */

public  void setEstimatedValue(final Float estimatedValue) {
	this.estimatedValue = estimatedValue;
}

					/**
	 * Getter for weight of class Player59BO.
	 *
	 * @return {@link java.lang.Integer}
	 * @generated
	 */ 
public  Integer getWeight() {
	return this.weight;
	}


	/**
	 * Setter for weight of class Player59BO.
	 * @param value {@link java.lang.Integer}
	 * @generated
	 */

public  void setWeight(final Integer weight) {
	this.weight = weight;
}

					/**
	 * Getter for height of class Player59BO.
	 *
	 * @return {@link java.lang.Integer}
	 * @generated
	 */ 
public  Integer getHeight() {
	return this.height;
	}


	/**
	 * Setter for height of class Player59BO.
	 * @param value {@link java.lang.Integer}
	 * @generated
	 */

public  void setHeight(final Integer height) {
	this.height = height;
}

					/**
	 * Getter for rookie of class Player59BO.
	 *
	 * @return {@link java.lang.Boolean}
	 * @generated
	 */ 
public  Boolean getRookie() {
	return this.rookie;
	}


	/**
	 * Setter for rookie of class Player59BO.
	 * @param value {@link java.lang.Boolean}
	 * @generated
	 */

public  void setRookie(final Boolean rookie) {
	this.rookie = rookie;
}


/**
	 * Returns <code>true</code> if the argument is an Player59 instance and all identifiers for this entity
     * equal the identifiers of the argument entity. The <code>equals</code> method of the parent entity
     * will also need to return <code>true</code>. Returns <code>false</code> otherwise.
     *
     * @see PersonBO#hashCode()
	 * @param object  object to compare with current instance      
     * @return boolean true if equals false if not
     */
    public boolean equals(final Object object )
    {
		if ( this == object )
        {
            return true;
        }
        if ( !( object instanceof Player59BO ) )
        {
            return false;
        }
        final Player59BO that = (Player59BO)object;
    
    	            if (        				        			this.getId() == null
        	        ) {
        	return false;
        }
		return new EqualsBuilder()
			.append(this.getId(), that.getId())
			 	.isEquals();
	}
	/**

	 * Returns a hash code based on this entity's identifiers and the hash code of the parent entity.
     *
     * @see PersonBO#hashCode()
     * @return int
     */    
    public int hashCode() {
        return new HashCodeBuilder()
             .append(getId())
				 	.appendSuper(super.hashCode())
            .toHashCode();
    }
}

