/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.business.standardfeatures.application.datagrid.uc35_paginatorbdd.bos;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.LazyInitializationException;

import com.bluage.documentation.business.common.bos.TeamBO;
import com.bluage.documentation.business.standardfeatures.application.datagrid.uc35_paginatorbdd.entities.daofinder.Team35DAOFinderImpl;
import com.netfective.bluage.core.exception.ApplicationException;

/**
 * Team35BO
 *
* @see com.netfective.bluage.core.business.IBusinessObject
* * @see TeamBO
 */
@XmlRootElement

public class Team35BO
 extends TeamBO {
	
	/**
	 * Default SUID for Team35BO.
	 */
	private static final long serialVersionUID = 1L;
	

	@Inject
	@Named ("com.bluage.documentation.business.standardfeatures.application.datagrid.uc35_paginatorbdd.entities.daofinder.Team35Finder") 
	private Team35DAOFinderImpl finder;

																			/**
	 * Entity property: player35s
	 *
	 */
private List<Player35BO> player35s = new ArrayList<Player35BO>();



   /**
	* public default constructor for class Team35BO
	*/
	public Team35BO(){
		//NOP Team35BO
	}


																			/**
	 * Getter for player35s of class Team35BO.
	 *
	 * @return {@link java.util.List<com.bluage.documentation.business.standardfeatures.application.datagrid.uc35_paginatorbdd.bos.Player35BO>}
	 * @generated
	 */ 
public  List<Player35BO> getPlayer35s() {
	return this.player35s;
			}


	/**
	 * Setter for player35s of class Team35BO.
	 * @param value {@link java.util.List<com.bluage.documentation.business.standardfeatures.application.datagrid.uc35_paginatorbdd.bos.Player35BO>}
	 * @generated
	 */

public  void setPlayer35s(final List<Player35BO> player35s) {
	this.player35s = player35s;
}

	/**
	 * Remove an entity from the com.bluage.documentation.business.standardfeatures.application.datagrid.uc35_paginatorbdd.bos.Player35BO collection.
	 *
	 * @param entity
	 *		: {@link List<Player35BO>} the entity
	 * @throws ApplicationException
     */
public void removeFromPlayer35s( Player35BO entity ) throws ApplicationException {
	try {
		if( getPlayer35s() != null && getPlayer35s().contains(entity) ) {
			getPlayer35s().remove( entity );
		}
	} catch ( LazyInitializationException e ) {
				Team35BO bo = ( Team35BO ) finder.findByPrimaryKey( getId() );
			finder.getHibernateTemplate().initialize( bo.getPlayer35s() );
			setPlayer35s( bo.getPlayer35s() );
			// evict object from session to avoid NonUniqueObjectException
			finder.getHibernateTemplate().evict( bo );
		
			if( getPlayer35s() != null && getPlayer35s().contains( entity ) ) {
				getPlayer35s().remove( entity );
			}
		}
	}
	
	/**
	 * Add entity to the com.bluage.documentation.business.standardfeatures.application.datagrid.uc35_paginatorbdd.bos.Player35BO collection.
	 *
	 * @param entity
	 *		{@link Player35BO} the entity
	 * @throws ApplicationException
     */
public void addToPlayer35s( Player35BO entity ) throws ApplicationException
	{
		try {
			if( getPlayer35s() != null && !getPlayer35s().contains( entity ) ) {
				getPlayer35s().add( entity );
			}
		} catch ( LazyInitializationException e ) {
			Team35BO bo = ( Team35BO ) finder.findByPrimaryKey( getId() );
		finder.getHibernateTemplate().initialize( bo.getPlayer35s() );
		setPlayer35s( bo.getPlayer35s() );
		// evict object from session to avoid NonUniqueObjectException
		finder.getHibernateTemplate().evict( bo );
		if( getPlayer35s() != null && !getPlayer35s().contains( entity ) ) {
			getPlayer35s().add( entity );
		}
	}
	}
	
	/**
	 * Add entitys to the com.bluage.documentation.business.standardfeatures.application.datagrid.uc35_paginatorbdd.bos.Player35BO collection
	 *
	 * @param entities			
	 *		: {@link java.util.List} A list of entities
	 * @throws ApplicationException 
     */	
public void addAllToPlayer35s( Collection<Player35BO> entities ) throws ApplicationException {
	for( Player35BO entity : entities) {
		if( getPlayer35s()!=null ) {
			addToPlayer35s( entity );
		}
	}
}
	
	/**
	 * Remove entities form the com.bluage.documentation.business.standardfeatures.application.datagrid.uc35_paginatorbdd.bos.Player35BO collection
	 *
	 * @param entities				
	 *		: {@link java.util.List} a list of entities
	 * @throws ApplicationException
     */	
public void removeAllFromPlayer35s( Collection<Player35BO> entities ) throws ApplicationException {
	for( Player35BO entity : entities) {
		if( getPlayer35s()!=null && getPlayer35s().contains( entity ) )	{		
			removeFromPlayer35s( entity );
		}
	}
}
	
	/**
	 * Remove all items form the collection com.bluage.documentation.business.standardfeatures.application.datagrid.uc35_paginatorbdd.bos.Player35BO
	 *
	 * @throws ApplicationException
     */	
public void removeAllFromPlayer35s() throws ApplicationException {
	List<Player35BO> entities = this.getPlayer35s();
	if ( entities == null ) {
		return;
	}
	entities.clear();
}

/**
	 * Returns <code>true</code> if the argument is an Team35 instance and all identifiers for this entity
     * equal the identifiers of the argument entity. The <code>equals</code> method of the parent entity
     * will also need to return <code>true</code>. Returns <code>false</code> otherwise.
     *
     * @see TeamBO#hashCode()
	 * @param object  object to compare with current instance      
     * @return boolean true if equals false if not
     */
    public boolean equals(final Object object )
    {
		if ( this == object )
        {
            return true;
        }
        if ( !( object instanceof Team35BO ) )
        {
            return false;
        }
        final Team35BO that = (Team35BO)object;
    
    	            if (        				        			this.getId() == null
        	        ) {
        	return false;
        }
		return new EqualsBuilder()
			.append(this.getId(), that.getId())
			 	.isEquals();
	}
	/**

	 * Returns a hash code based on this entity's identifiers and the hash code of the parent entity.
     *
     * @see TeamBO#hashCode()
     * @return int
     */    
    public int hashCode() {
        return new HashCodeBuilder()
             .append(getId())
				 	.appendSuper(super.hashCode())
            .toHashCode();
    }
}

