package uc72;


import org.junit.Test;
import test.common.AbstractBADocTestCase;

public class IT01uc72Affichage extends AbstractBADocTestCase {
	
	@Override
	public  String getName(){
		return "UC-72";
	}


	@Test
	public void testIT01uc72Affichage() throws Exception {
		// On teste l'UC72 - Standard Features/Entities - Generator Class
		
		selenium.open("");	
		selenium.click("link=Expand All");
		selenium.waitForPageToLoad("30000");
		selenium.click("link=UC72 - Generator Class");
		// On teste l'affichage de la page de garde :
		assertTrue(selenium.isElementPresent("//h2[contains(text(),'Standard Features > Entities > UC72 > Generator Class')]"));
		assertTrue(selenium.isElementPresent("//h4[contains(text(),'Menu')]"));
		assertTrue(selenium.isElementPresent("//a[contains(text(),'[Add a player]')]"));
		assertTrue(selenium.isElementPresent("//a[contains(text(),'[Add a team]')]"));
		// On teste l'affichage de 'Add a player' :
		selenium.click("//a[contains(text(),'[Add a player]')]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//h2[contains(text(),'Standard Features > Entities > UC72 > Generator Class')]"));
		assertTrue(selenium.isElementPresent("//h4[contains(text(),'Add Player')]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/label[contains(text(),'Id')] and td/input[contains(@id,'txt_id')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/label[contains(text(),'First Name')] and td/input[contains(@id,'txt_firstname')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/label[contains(text(),'Last Name')] and td/input[contains(@id,'txt_lastname')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/label[contains(text(),'Date of Birth (MM/dd/yyyy)')] and td/input[contains(@id,'txt_dateofbirth')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr/td/label[contains(text(),'Position')]"));
		assertTrue(selenium.isElementPresent("//option[contains(text(),'Point Guard')]"));
		assertTrue(selenium.isElementPresent("//option[contains(text(),'Shooting Guard')]"));
		assertTrue(selenium.isElementPresent("//option[contains(text(),'Small Forward')]"));
		assertTrue(selenium.isElementPresent("//option[contains(text(),'Power Forward')]"));
		assertTrue(selenium.isElementPresent("//option[contains(text(),'Center')]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/label[contains(text(),'Rookie')] and td/input[contains(@id,'chk_rookie')]]"));
		assertTrue(selenium.isElementPresent("//a[contains(text(),'[Create player]')]"));
		// On vérifie le tableau player :
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]//thead//tr[th/div/span[contains(text(),'Id')] and th/div/span[contains(text(),'Name')] and th/div/span[contains(text(),'Date of Birth')] and th/div/span[contains(text(),'Position')] and th[contains(text(),'Rookie')] and th[contains(text(),'Delete')]]|//table[contains(@id,'tab_players')]//thead//tr[th/a[contains(text(),'Id')] and th/a[contains(text(),'Name')] and th/a[contains(text(),'Date of Birth')] and th/a[contains(text(),'Position')] and th/a[contains(text(),'Rookie')] and th/a[contains(text(),'Delete')]]"));
		verifyEquals("3", selenium.getXpathCount("//table[contains(@id,'tab_players')]/tbody/tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),'1')] and td/span[contains(text(),'Frank')] and td/span[contains(text(),'Oliver')] and td/span[contains(text(),'10/31/1968')] and td/span[contains(text(),'PG')] and td/input[not(@checked)] and td/a[contains(text(),'[delete]')]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),'2')] and td/span[contains(text(),'Cole')] and td/span[contains(text(),'Elbert')] and td/span[contains(text(),'04/07/1965')] and td/span[contains(text(),'SG')] and td/input[not(@checked)] and td/a[contains(text(),'[delete]')]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),'3')] and td/span[contains(text(),'Dawson')] and td/span[contains(text(),'Ricky')] and td/span[contains(text(),'03/03/1990')] and td/span[contains(text(),'SF')] and td/input[@checked] and td/a[contains(text(),'[delete]')]]"));
		// On retourne sur la page de garde de l'UC :
		selenium.click("//a[contains(text(),'[back]')]");
		selenium.waitForPageToLoad("30000");
		// On vérifie qu'on est bien sur la page de garde :
		assertTrue(selenium.isElementPresent("//h2[contains(text(),'Standard Features > Entities > UC72 > Generator Class')]"));
		assertTrue(selenium.isElementPresent("//h4[contains(text(),'Menu')]"));
		// On teste l'affichage de 'Add a team' :
		selenium.click("//a[contains(text(),'[Add a team]')]");
		selenium.waitForPageToLoad("30000");
		// On teste l'affichage de 'Add a team' :
		assertTrue(selenium.isElementPresent("//h2[contains(text(),'Standard Features > Entities > UC72 > Generator Class')]"));
		assertTrue(selenium.isElementPresent("//h4[contains(text(),'Add Team')]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/label[contains(text(),'City')] and td/input[contains(@id,'txt_city')]]"));
		assertTrue(selenium.isElementPresent("//table/tbody/tr[td/label[contains(text(),'State')]]"));
		verifyEquals("50", selenium.getXpathCount("//option"));
		assertTrue(selenium.isElementPresent("//option[contains(text(),'Alabama')]"));
		assertTrue(selenium.isElementPresent("//option[contains(text(),'Alaska')]"));
		assertTrue(selenium.isElementPresent("//option[contains(text(),'Arizona')]"));
		assertTrue(selenium.isElementPresent("//option[contains(text(),'Arkansas')]"));
		assertTrue(selenium.isElementPresent("//option[contains(text(),'California')]"));
		assertTrue(selenium.isElementPresent("//option[contains(text(),'Colorado')]"));
		assertTrue(selenium.isElementPresent("//option[contains(text(),'Connecticut')]"));
		assertTrue(selenium.isElementPresent("//option[contains(text(),'Delaware')]"));
		assertTrue(selenium.isElementPresent("//option[contains(text(),'Florida')]"));
		assertTrue(selenium.isElementPresent("//option[contains(text(),'Gironde')]"));
		assertTrue(selenium.isElementPresent("//option[contains(text(),'Hawaii')]"));
		assertTrue(selenium.isElementPresent("//option[contains(text(),'Idaho')]"));
		assertTrue(selenium.isElementPresent("//option[contains(text(),'Illinois')]"));
		assertTrue(selenium.isElementPresent("//option[contains(text(),'Indiana')]"));
		assertTrue(selenium.isElementPresent("//option[contains(text(),'Iowa')]"));
		assertTrue(selenium.isElementPresent("//option[contains(text(),'Kansas')]"));
		assertTrue(selenium.isElementPresent("//option[contains(text(),'Kentucky')]"));
		assertTrue(selenium.isElementPresent("//option[contains(text(),'Louisiana')]"));
		assertTrue(selenium.isElementPresent("//option[contains(text(),'Maine')]"));
		assertTrue(selenium.isElementPresent("//option[contains(text(),'Maryland')]"));
		assertTrue(selenium.isElementPresent("//option[contains(text(),'Massachusetts')]"));
		assertTrue(selenium.isElementPresent("//option[contains(text(),'Michigan')]"));
		assertTrue(selenium.isElementPresent("//option[contains(text(),'Minnesota')]"));
		assertTrue(selenium.isElementPresent("//option[contains(text(),'Missouri')]"));
		assertTrue(selenium.isElementPresent("//option[contains(text(),'Montana')]"));
		assertTrue(selenium.isElementPresent("//option[contains(text(),'Nebraska')]"));
		assertTrue(selenium.isElementPresent("//option[contains(text(),'Nevada')]"));
		assertTrue(selenium.isElementPresent("//option[contains(text(),'New Hampshire')]"));
		assertTrue(selenium.isElementPresent("//option[contains(text(),'New Jersey')]"));
		assertTrue(selenium.isElementPresent("//option[contains(text(),'New Mexico')]"));
		assertTrue(selenium.isElementPresent("//option[contains(text(),'New York')]"));
		assertTrue(selenium.isElementPresent("//option[contains(text(),'North Carolina')]"));
		assertTrue(selenium.isElementPresent("//option[contains(text(),'North Dakota')]"));
		assertTrue(selenium.isElementPresent("//option[contains(text(),'Ohio')]"));
		assertTrue(selenium.isElementPresent("//option[contains(text(),'Oklahoma')]"));
		assertTrue(selenium.isElementPresent("//option[contains(text(),'Oregon')]"));
		assertTrue(selenium.isElementPresent("//option[contains(text(),'Pennsylvania')]"));
		assertTrue(selenium.isElementPresent("//option[contains(text(),'Rhode Island')]"));
		assertTrue(selenium.isElementPresent("//option[contains(text(),'South Carolina')]"));
		assertTrue(selenium.isElementPresent("//option[contains(text(),'South Dakota')]"));
		assertTrue(selenium.isElementPresent("//option[contains(text(),'Tennessee')]"));
		assertTrue(selenium.isElementPresent("//option[contains(text(),'Texas')]"));
		assertTrue(selenium.isElementPresent("//option[contains(text(),'Utah')]"));
		assertTrue(selenium.isElementPresent("//option[contains(text(),'Vermont')]"));
		assertTrue(selenium.isElementPresent("//option[contains(text(),'Virginia')]"));
		assertTrue(selenium.isElementPresent("//option[contains(text(),'Washington')]"));
		assertTrue(selenium.isElementPresent("//option[contains(text(),'West Virginia')]"));
		assertTrue(selenium.isElementPresent("//option[contains(text(),'Wisconsin')]"));
		assertTrue(selenium.isElementPresent("//option[contains(text(),'Wyoming')]"));
		assertTrue(selenium.isElementPresent("//a[contains(text(),'[Create a new team]')]"));
		// On vérifie le tableau teams :
		assertTrue(selenium.isElementPresent("//table[contains(@id,'table_teams')]/thead/tr[th/div/span[contains(text(),'City')] and th/div/span[contains(text(),'Name')]  and th/div/span[contains(text(),'State')] and th[contains(text(),'Delete')]]|//table[contains(@id,'table_teams')]/thead/tr[th/a[contains(text(),'City')] and th/a[contains(text(),'Name')]  and th/a[contains(text(),'State')] and th/a[contains(text(),'Delete')]]"));
		verifyEquals("6", selenium.getXpathCount("//table[contains(@id,'table_teams')]/tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'table_teams')]/tbody/tr[td/span[contains(text(),'Chicago')] and td/span[contains(text(),'Bulls')] and td/span[contains(text(),'Illinois')] and td/span[contains(text(),'IL')] and td/a[contains(text(),'[delete]')]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'table_teams')]/tbody/tr[td/span[contains(text(),'Bordeaux')] and td/span[contains(text(),'Girondins')] and td/span[contains(text(),'Gironde')] and td/span[contains(text(),'GI')] and td/a[contains(text(),'[delete]')]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'table_teams')]/tbody/tr[td/span[contains(text(),'Cleveland')] and td/span[contains(text(),'Cavaliers')] and td/span[contains(text(),'Ohio')] and td/span[contains(text(),'OH')] and td/a[contains(text(),'[delete]')]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'table_teams')]/tbody/tr[td/span[contains(text(),'Boston')] and td/span[contains(text(),'Celtics')] and td/span[contains(text(),'Massachusetts')] and td/span[contains(text(),'MA')] and td/a[contains(text(),'[delete]')]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'table_teams')]/tbody/tr[td/span[contains(text(),'Anchorage')] and td/span[contains(text(),'Furious Penguins')] and td/span[contains(text(),'Alaska')] and td/span[contains(text(),'AK')] and td/a[contains(text(),'[delete]')]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'table_teams')]/tbody/tr[td/span[contains(text(),'Juneau')] and td/span[contains(text(),'Wild Meese')] and td/span[contains(text(),'Alaska')] and td/span[contains(text(),'AK')] and td/a[contains(text(),'[delete]')]]"));
		// On retourne sur la page de garde de l'UC : 
		selenium.click("//a[contains(text(),'[back]')]");
		selenium.waitForPageToLoad("30000");
	}


}
