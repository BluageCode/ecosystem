/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.qauc.uc1001_tables ;


import org.apache.log4j.Logger;
import org.springframework.validation.Errors;

import com.netfective.bluage.core.validator.AbstractValidator;
/**
* Class :UC1001_TablesValidator
*/
public class UC1001_TablesValidator extends AbstractValidator{
	
	// LOGGER for the class UC1001_TablesValidator
	private static final Logger LOGGER = Logger.getLogger( UC1001_TablesValidator.class);
	
	
	/**
	* Operation validate for UC1001_TablesForm
	* @param obj : the current form (UC1001_TablesForm)
	* @param errors : The spring errors to return for the form UC1001_TablesForm
	*/
	public void validate(final Object obj,final Errors errors){
		LOGGER.info("Beginning method : validate"); // UC1001_TablesValidator
		UC1001_TablesForm cUC1001_TablesForm = (UC1001_TablesForm)obj; // UC1001_TablesValidator
		LOGGER.info("Ending method : validate the form "+ cUC1001_TablesForm.getClass().getName()); // UC1001_TablesValidator
	}

	/**
	* Method to implements to use spring validators (UC1001_TablesForm)
	* @param aClass : Class for the form UC1001_TablesForm
	* @return boolean
	*/
	public boolean supports(final Class aClass){
		return (new UC1001_TablesForm()).getClass().equals(aClass); // UC1001_TablesValidator
	}
}
