package uc34;

import org.junit.Test;
import test.common.AbstractBADocTestCase;

public class ITuc34Pagination extends AbstractBADocTestCase {
	
	@Override
	public  String getName(){
		return "UC-34";
	}
	
	@Test
	public void testITuc34Pagination() throws Exception {
		// On teste l'UC34 - Standard Features/Application/Datagrid - PaginationTable
		// On check la derni�re page.
		selenium.open("");
		selenium.click("link=Expand All");
		selenium.waitForPageToLoad("30000");
		selenium.click("link=UC34 - Pagination Table");
		verifyTrue(selenium.isElementPresent("//h2[contains(text(),\"Standard Features > Application > DataGrid > UC34 > Pagination Table\")]"));
		// On clique sur le lien Last
		selenium.click("xpath=(//span/a)[4]");
		selenium.waitForPageToLoad("30000");
		// On r�cup�re l'index du lien Next
		// Attention, l'index est l'index dans tous les fils
		// On ne peut donc pas faire un a[${index}]
		// A la place on va utiliser la notation /* pour r�cup�rer tous les fils
//		Number index = selenium.getElementIndex("//*[img[@alt=\"Next\"]] | //img[@alt=\"next\"]");
		// Dans tous les fils on r�cup�re celui qui est � l'emplacement index
//		System.out.println("//span[img[@alt=\"next\"]]/*[" + index + " - 2]");
		// On ne fait pas index - 1 car storeElementIndex commence � 0
		// Le [index] va donc r�cup�rer l'�l�ment avant le lien Next
		// On v�rifie donc que cet �l�ment est bien en gras comme pr�vu (/b)
		assertTrue(selenium.isElementPresent("//a/b[contains(text(),\"3\")]|//strong[contains(text(),\"3\")]"));
		// On teste l'UC34 - Standard Features/Application/Datagrid - PaginationTable
		// On check la derni�re page.
		// On clique sur le lien First
		selenium.click("xpath=(//span/a)[1]");
		selenium.waitForPageToLoad("30000");
		// On r�cup�re l'index du lien Prev
		// Attention, l'index est l'index dans tous les fils
		// On ne peut donc pas faire un a[${index}]
		// A la place on va utiliser la notation /* pour r�cup�rer tous les fils
//		Number index = selenium.getElementIndex("//img[substring(translate(@alt,'PREV','prev'),1,4)='prev']|//a[img[substring(translate(@alt,'PREV','prev'),1,4)='prev']]");
		// Dans tous les fils on r�cup�re celui qui est � l'emplacement index
		// Le [index] va donc r�cup�rer l'�l�ment avant le lien Prev
		// On v�rifie donc que cet �l�ment est bien en gras comme pr�vu (/b)
		assertTrue(selenium.isElementPresent("//span/strong[contains(text(),\"1\")]"));
		selenium.click("link=UC34 - Pagination Table");
		selenium.waitForPageToLoad("30000");
		// On check la derni�re page.
		// On clique sur le lien First
		selenium.click("xpath=(//span/a)[4]");
		selenium.waitForPageToLoad("30000");
		// On r�cup�re l'index du lien Prev
		// Attention, l'index est l'index dans tous les fils
		// On ne peut donc pas faire un a[${index}]
		// A la place on va utiliser la notation /* pour r�cup�rer tous les fils
//		index = selenium.getElementIndex("//*[img[@alt=\"Next\"]] | //img[@alt=\"next\"]");
		// Dans tous les fils on r�cup�re celui qui est � l'emplacement index
		// Le [index] va donc r�cup�rer l'�l�ment avant le lien Prev
		// On v�rifie donc que cet �l�ment est bien en gras comme pr�vu (/b)
		assertTrue(selenium.isElementPresent("//a/b[contains(text(),\"3\")]|//strong[contains(text(),\"3\")]"));
		// On teste fast next + fast prev + view
		selenium.click("link=UC34 - Pagination Table");
		selenium.waitForPageToLoad("30000");
		verifyTrue(selenium.isElementPresent("//h2[contains(text(),\"Standard Features > Application > DataGrid > UC34 > Pagination Table\")]"));
//		System.out.println("variable index vaut : " + index);
		// On indexe la ligne de Kansas City.
        Number index = selenium.getElementIndex("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Kansas City\")] and td/span[contains(text(),\"Bears\")]]");
		// On clique sur [view]
		selenium.click("//table[contains(@id,'tab_teams')]/tbody/tr[" + index + " + 1]/td/a[contains(text(),\"[view]\")]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie que l'on est sur la bonne page.
		assertEquals("Bears", selenium.getText("//*[contains(@id,'txt_TeamName')]"));
		assertEquals("Kansas City", selenium.getText("//*[contains(@id,'txt_Teamcity')]"));
		// On teste les liens.
		// On va sur la page 2.
		selenium.click("xpath=(//span/a)[8]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie qu'on est sur la page 2.
		assertTrue(selenium.isElementPresent("//a/b[contains(text(),\"2\")]|//strong[contains(text(),\"2\")]"));
		// On va tester le lien fast next.
//		selenium.click("//a[img[@alt=\"Fast next\"]]");
//		selenium.waitForPageToLoad("30000");
//		// Les sauts de pages sont de 3 pages. On v�rifie que l'on est bien sur la 5eme page.
//		assertTrue(selenium.isElementPresent("//a/b[contains(text(),\"5\")]|//strong[contains(text(),\"5\")]"));
//		// On teste le lien fast prev.
//		selenium.click("//a[img[@alt=\"Fast prev\"]]");
//		selenium.waitForPageToLoad("30000");
//		// Les sauts de pages sont de 3 pages. On v�rife que l'on est bien sur la 2eme page.
//		assertTrue(selenium.isElementPresent("//a/b[contains(text(),\"2\")]|//strong[contains(text(),\"2\")]"));
		// On teste le lien Prev.
		selenium.click("xpath=(//span/a)[2]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie que l'on est sur la premi�re page.
		assertTrue(selenium.isElementPresent("//a/b[contains(text(),\"1\")]|//strong[contains(text(),\"1\")]"));
		// On teste le lien Last.
		selenium.click("xpath=(//span/a)[9]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//strong[contains(text(),\"13\")]"));
		// On teste le lien First
		selenium.click("xpath=(//span/a)[2]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//strong[contains(text(),\"1\")]"));
		// On clique sur [back]
		selenium.click("//a[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//h2[@class=\"rd_first\"]"));
		// On teste les liens du tableau teams
		selenium.click("link=UC34 - Pagination Table");
		selenium.waitForPageToLoad("30000");
		verifyTrue(selenium.isElementPresent("//h2[contains(text(),\"Standard Features > Application > DataGrid > UC34 > Pagination Table\")]"));
		// On teste les liens.
		// On va sur la page 2.
		selenium.click("xpath=(//span/a)[1]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie qu'on est sur la page 2.
		assertTrue(selenium.isElementPresent("//a/b[contains(text(),\"2\")]|//strong[contains(text(),\"2\")]"));
		// On va tester le lien fast next.
//		selenium.click("//a[img[@alt=\"Fast next\"]]");
//		selenium.waitForPageToLoad("30000");
//		// Les sauts de pages sont de 3 pages. On v�rifie que l'on est bien sur la 5eme page.
//		assertTrue(selenium.isElementPresent("//a/b[contains(text(),\"3\")]|//strong[contains(text(),\"3\")]"));
//		// On teste le lien fast prev.
//		selenium.click("//a[img[@alt=\"Fast prev\"]]");
//		selenium.waitForPageToLoad("30000");
//		// Les sauts de pages sont de 3 pages. On v�rife que l'on est bien sur la 2eme page.
//		assertTrue(selenium.isElementPresent("//a/b[contains(text(),\"1\")]|//strong[contains(text(),\"1\")]"));
		// On teste le lien Prev.
		selenium.click("xpath=(//span/a)[2]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie que l'on est sur la premi�re page.
		assertTrue(selenium.isElementPresent("//a/b[contains(text(),\"1\")]|//strong[contains(text(),\"1\")]"));
		// On teste le lien Last.
		selenium.click("xpath=(//span/a)[4]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//a/b[contains(text(),\"3\")]|//strong[contains(text(),\"3\")]"));
		// On teste le lien First
		selenium.click("xpath=(//span/a)[1]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//a/b[contains(text(),\"1\")]|//strong[contains(text(),\"1\")]"));
		// On teste les premiere pages des teams
		selenium.click("link=UC34 - Pagination Table");
		selenium.waitForPageToLoad("30000");
		verifyTrue(selenium.isElementPresent("//h2[contains(text(),\"Standard Features > Application > DataGrid > UC34 > Pagination Table\")]"));
		Number nbLigne = selenium.getXpathCount("//table[contains(@id,'tab_teams')]/tbody/tr");
		String index2 = "1";
		// selenium.while("${index} <= ${nbLigne}");
		// On clique sur [view]
		selenium.click("//table[contains(@id,'tab_teams')]/tbody/tr[" + index2 + "]/td/a[contains(text(),\"[view]\")]");
		selenium.waitForPageToLoad("30000");
		// On teste les liens.
		// On va sur la page 2.
		selenium.click("xpath=(//span/a)[1]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie qu'on est sur la page 2.
		assertTrue(selenium.isElementPresent("//a/b[contains(text(),\"2\")]|//strong[contains(text(),\"2\")]"));
		// On va tester le lien fast next.
//		selenium.click("//a[img[@alt=\"Fast next\"]]");
//		selenium.waitForPageToLoad("30000");
//		// Les sauts de pages sont de 3 pages. On v�rifie que l'on est bien sur la 5eme page.
//		assertTrue(selenium.isElementPresent("//a/b[contains(text(),\"5\")]|//strong[contains(text(),\"5\")]"));
//		// On teste le lien fast prev.
//		selenium.click("//a[img[@alt=\"Fast prev\"]]");
//		selenium.waitForPageToLoad("30000");
//		// Les sauts de pages sont de 3 pages. On v�rife que l'on est bien sur la 2eme page.
//		assertTrue(selenium.isElementPresent("//a/b[contains(text(),\"2\")]|//strong[contains(text(),\"2\")]"));
		// On teste le lien Prev.
		selenium.click("xpath=(//span/a)[2]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie que l'on est sur la premi�re page.
		assertTrue(selenium.isElementPresent("//a/b[contains(text(),\"1\")]|//strong[contains(text(),\"1\")]"));
		// On teste le lien Last.
		selenium.click("xpath=(//span/a)[9]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//a/b[contains(text(),\"13\")]|//strong[contains(text(),\"13\")]"));
		// On teste le lien First
		selenium.click("xpath=(//span/a)[1]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//a/b[contains(text(),\"1\")]|//strong[contains(text(),\"1\")]"));
		index2 = selenium.getEval(index2 + " + 1");
		selenium.click("link=[back]");
		selenium.waitForPageToLoad("30000");
		// selenium.endWhile();
		// On teste les elements de pagination pour toutes les teams
		// Pour les Tigers Charlotte
		selenium.click("link=UC34 - Pagination Table");
		selenium.waitForPageToLoad("30000");
		verifyTrue(selenium.isElementPresent("//h2[contains(text(),\"Standard Features > Application > DataGrid > UC34 > Pagination Table\")]"));
		// On indexe la ligne de Charlotte.
		index = selenium.getElementIndex("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Charlotte\")] and td/span[contains(text(),\"Tigers\")]]");
		System.out.println("variable index vaut : " + index);
		// On clique sur [view]
		selenium.click("//table[contains(@id,'tab_teams')]/tbody/tr[" + index+ " + 1]/td/a[contains(text(),\"[view]\")]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie que l'on est sur la bonne page.
		assertEquals("Tigers", selenium.getText("//*[contains(@id,'txt_TeamName')]"));
		assertEquals("Charlotte", selenium.getText("//*[contains(@id,'txt_Teamcity')]"));
		// On teste les liens.
		// On va sur la page 2.
		selenium.click("xpath=(//span/a)[1]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie qu'on est sur la page 2.
		assertTrue(selenium.isElementPresent("//a/b[contains(text(),\"2\")]|//strong[contains(text(),\"2\")]"));
		// On va tester le lien fast next.
//		selenium.click("//a[img[@alt=\"Fast next\"]]");
//		selenium.waitForPageToLoad("30000");
//		// Les sauts de pages sont de 3 pages. On v�rifie que l'on est bien sur la 5eme page.
//		assertTrue(selenium.isElementPresent("//a/b[contains(text(),\"5\")]|//strong[contains(text(),\"5\")]"));
//		// On teste le lien fast prev.
//		selenium.click("//a[img[@alt=\"Fast prev\"]]");
//		selenium.waitForPageToLoad("30000");
//		// Les sauts de pages sont de 3 pages. On v�rife que l'on est bien sur la 2eme page.
//		assertTrue(selenium.isElementPresent("//a/b[contains(text(),\"2\")]|//strong[contains(text(),\"2\")]"));
		// On teste le lien Prev.
		selenium.click("xpath=(//span/a)[2]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie que l'on est sur la premi�re page.
		assertTrue(selenium.isElementPresent("//a/b[contains(text(),\"1\")]|//strong[contains(text(),\"1\")]"));
		// On teste le lien Last.
		selenium.click("xpath=(//span/a)[9]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//a/b[contains(text(),\"13\")]|//strong[contains(text(),\"13\")]"));
		// On teste le lien First
		selenium.click("xpath=(//span/a)[1]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//a/b[contains(text(),\"1\")]|//strong[contains(text(),\"1\")]"));
		// Pour les Chargers Detroit
		selenium.click("link=UC34 - Pagination Table");
		selenium.waitForPageToLoad("30000");
		verifyTrue(selenium.isElementPresent("//h2[contains(text(),\"Standard Features > Application > DataGrid > UC34 > Pagination Table\")]"));
		// On indexe la ligne de Detroit.
		index = selenium.getElementIndex("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Detroit\")] and td/span[contains(text(),\"Chargers\")]]");
		System.out.println("variable index vaut : " + index);
		// On clique sur [view]
		selenium.click("//table[contains(@id,'tab_teams')]/tbody/tr[" + index + " + 1]/td/a[contains(text(),\"[view]\")]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie que l'on est sur la bonne page.
		assertEquals("Chargers", selenium.getText("//*[contains(@id,'txt_TeamName')]"));
		assertEquals("Detroit", selenium.getText("//*[contains(@id,'txt_Teamcity')]"));
		// On teste les liens.
		// On va sur la page 2.
		selenium.click("xpath=(//span/a)[1]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie qu'on est sur la page 2.
		assertTrue(selenium.isElementPresent("//a/b[contains(text(),\"2\")]|//strong[contains(text(),\"2\")]"));
		// On va tester le lien fast next.
//		selenium.click("//a[img[@alt=\"Fast next\"]]");
//		selenium.waitForPageToLoad("30000");
//		// Les sauts de pages sont de 3 pages. On v�rifie que l'on est bien sur la 5eme page.
//		assertTrue(selenium.isElementPresent("//a/b[contains(text(),\"5\")]|//strong[contains(text(),\"5\")]"));
//		// On teste le lien fast prev.
//		selenium.click("//a[img[@alt=\"Fast prev\"]]");
//		selenium.waitForPageToLoad("30000");
//		// Les sauts de pages sont de 3 pages. On v�rife que l'on est bien sur la 2eme page.
//		assertTrue(selenium.isElementPresent("//a/b[contains(text(),\"2\")]|//strong[contains(text(),\"2\")]"));
		// On teste le lien Prev.
		selenium.click("xpath=(//span/a)[2]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie que l'on est sur la premi�re page.
		assertTrue(selenium.isElementPresent("//a/b[contains(text(),\"1\")]|//strong[contains(text(),\"1\")]"));
		// On teste le lien Last.
		selenium.click("xpath=(//span/a)[7]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//a/b[contains(text(),\"6\")]|//strong[contains(text(),\"6\")]"));
		// On teste le lien First
		selenium.click("xpath=(//span/a)[1]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//a/b[contains(text(),\"1\")]|//strong[contains(text(),\"1\")]"));
		// On clique sur [back]
		selenium.click("//a[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//h2[@class=\"rd_first\"]"));
		// Pour les Hawks Detroit
		selenium.click("link=UC34 - Pagination Table");
		selenium.waitForPageToLoad("30000");
		// On indexe la ligne de Detroit
		index = selenium.getElementIndex("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Detroit\")] and td/span[contains(text(),\"Hawks\")]]");
		System.out.println("variable index vaut : " + index);
		// On clique sur [view]
		selenium.click("//table[contains(@id,'tab_teams')]/tbody/tr[" + index + " + 1]/td/a[contains(text(),\"[view]\")]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie que l'on est sur la bonne page.
		assertEquals("Hawks", selenium.getText("//*[contains(@id,'txt_TeamName')]"));
		assertEquals("Detroit", selenium.getText("//*[contains(@id,'txt_Teamcity')]"));
		// On teste les liens.
		// On va sur la page 2.
		selenium.click("xpath=(//span/a)[1]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie qu'on est sur la page 2.
		assertTrue(selenium.isElementPresent("//a/b[contains(text(),\"2\")]|//strong[contains(text(),\"2\")]"));
		// On va tester le lien fast next.
//		selenium.click("//a[img[@alt=\"Fast next\"]]");
//		selenium.waitForPageToLoad("30000");
//		// Les sauts de pages sont de 3 pages. On v�rifie que l'on est bien sur la 5eme page.
//		assertTrue(selenium.isElementPresent("//a/b[contains(text(),\"5\")]|//strong[contains(text(),\"5\")]"));
//		// On teste le lien fast prev.
//		selenium.click("//a[img[@alt=\"Fast prev\"]]");
//		selenium.waitForPageToLoad("30000");
//		// Les sauts de pages sont de 3 pages. On v�rife que l'on est bien sur la 2eme page.
//		assertTrue(selenium.isElementPresent("//a/b[contains(text(),\"2\")]|//strong[contains(text(),\"2\")]"));
		// On teste le lien Prev.
		selenium.click("xpath=(//span/a)[2]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie que l'on est sur la premi�re page.
		assertTrue(selenium.isElementPresent("//a/b[contains(text(),\"1\")]|//strong[contains(text(),\"1\")]"));
		// On teste le lien Last.
		selenium.click("xpath=(//span/a)[9]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//a/b[contains(text(),\"8\")]|//strong[contains(text(),\"8\")]"));
		// On teste le lien First
		selenium.click("xpath=(//span/a)[1]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//a/b[contains(text(),\"1\")]|//strong[contains(text(),\"1\")]"));
		// On clique sur [back]
		selenium.click("//a[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//h2[@class=\"rd_first\"]"));
		selenium.click("link=UC34 - Pagination Table");
		selenium.waitForPageToLoad("30000");
		selenium.click("link=UC34 - Pagination Table");
		selenium.waitForPageToLoad("30000");
		// Pour Fresno
		selenium.click("link=UC34 - Pagination Table");
		selenium.waitForPageToLoad("30000");
		verifyTrue(selenium.isElementPresent("//h2[contains(text(),\"Standard Features > Application > DataGrid > UC34 > Pagination Table\")]"));
		// On clique sur 'Last''
		selenium.click("xpath=(//span/a)[4]");
		selenium.waitForPageToLoad("30000");
		// On indexe la ligne de Fresno.
		index = selenium.getElementIndex("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Fresno\")] and td/span[contains(text(),\"Mustangs\")]]");
		System.out.println("variable index vaut : " + index);
		// On clique sur [view]
		selenium.click("//table[contains(@id,'tab_teams')]/tbody/tr[" + index + " + 1]/td/a[contains(text(),\"[view]\")]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie que l'on est sur la bonne page.
		assertEquals("Mustangs", selenium.getText("//*[contains(@id,'txt_TeamName')]"));
		assertEquals("Fresno", selenium.getText("//*[contains(@id,'txt_Teamcity')]"));
		// On clique sur [back]
		selenium.click("//a[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//h2[@class=\"rd_first\"]"));
		// Pour KanasCity
		selenium.click("link=UC34 - Pagination Table");
		selenium.waitForPageToLoad("30000");
		verifyTrue(selenium.isElementPresent("//h2[contains(text(),\"Standard Features > Application > DataGrid > UC34 > Pagination Table\")]"));
		// On indexe la ligne de Kansas City.
		 index = selenium.getElementIndex("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Kansas City\")] and td/span[contains(text(),\"Bears\")]]");
		System.out.println("variable index vaut : " + index);
		// On clique sur [view]
		selenium.click("//table[contains(@id,'tab_teams')]/tbody/tr[" + index + " + 1]/td/a[contains(text(),\"[view]\")]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie que l'on est sur la bonne page.
		assertEquals("Bears", selenium.getText("//*[contains(@id,'txt_TeamName')]"));
		assertEquals("Kansas City", selenium.getText("//*[contains(@id,'txt_Teamcity')]"));
		// On teste les liens.
		// On va sur la page 2.
		selenium.click("xpath=(//span/a)[1]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie qu'on est sur la page 2.
		assertTrue(selenium.isElementPresent("//a/b[contains(text(),\"2\")]|//strong[contains(text(),\"2\")]"));
		// On va tester le lien fast next.
//		selenium.click("//a[img[@alt=\"Fast next\"]]");
//		selenium.waitForPageToLoad("30000");
//		// Les sauts de pages sont de 3 pages. On v�rifie que l'on est bien sur la 5eme page.
//		assertTrue(selenium.isElementPresent("//a/b[contains(text(),\"5\")]|//strong[contains(text(),\"5\")]"));
//		// On teste le lien fast prev.
//		selenium.click("//a[img[@alt=\"Fast prev\"]]");
//		selenium.waitForPageToLoad("30000");
//		// Les sauts de pages sont de 3 pages. On v�rife que l'on est bien sur la 2eme page.
//		assertTrue(selenium.isElementPresent("//a/b[contains(text(),\"2\")]|//strong[contains(text(),\"2\")]"));
		// On teste le lien Prev.
		selenium.click("xpath=(//span/a)[2]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie que l'on est sur la premi�re page.
		assertTrue(selenium.isElementPresent("//a/b[contains(text(),\"1\")]|//strong[contains(text(),\"1\")]"));
		// On teste le lien Last.
		selenium.click("xpath=(//span/a)[9]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//a/b[contains(text(),\"13\")]|//strong[contains(text(),\"13\")]"));
		// On teste le lien First
		selenium.click("xpath=(//span/a)[1]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//a/b[contains(text(),\"1\")]|//strong[contains(text(),\"1\")]"));
		// Pour Eagles Oakland
		selenium.click("link=UC34 - Pagination Table");
		selenium.waitForPageToLoad("30000");
		verifyTrue(selenium.isElementPresent("//h2[contains(text(),\"Standard Features > Application > DataGrid > UC34 > Pagination Table\")]"));
		// On clique sur 'Next'
		selenium.click("xpath=(//span/a)[1]");
		selenium.waitForPageToLoad("30000");
		// On indexe la ligne de Oakland.
		index = selenium.getElementIndex("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Oakland\")] and td/span[contains(text(),\"Cardinals\")]]");
		System.out.println("variable index vaut : " + index);
		// On clique sur [view]
		selenium.click("//table[contains(@id,'tab_teams')]/tbody/tr[" + index + " + 1]/td/a[contains(text(),\"[view]\")]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie que l'on est sur la bonne page.
		assertEquals("Cardinals", selenium.getText("//*[contains(@id,'txt_TeamName')]"));
		assertEquals("Oakland", selenium.getText("//*[contains(@id,'txt_Teamcity')]"));
		// On clique sur [back]
		selenium.click("//a[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//h2[@class=\"rd_first\"]"));
		// Pour Omaha
		selenium.click("link=UC34 - Pagination Table");
		selenium.waitForPageToLoad("30000");
		verifyTrue(selenium.isElementPresent("//h2[contains(text(),\"Standard Features > Application > DataGrid > UC34 > Pagination Table\")]"));
		// On clique sur 'Next'
		selenium.click("xpath=(//span/a)[1]");
		selenium.waitForPageToLoad("30000");
		// On indexe la ligne de Omaha.
		 index = selenium.getElementIndex("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Omaha\")] and td/span[contains(text(),\"Braves\")]]");
		System.out.println("variable index vaut : " + index);
		// On clique sur [view]
		selenium.click("//table[contains(@id,'tab_teams')]/tbody/tr[" + index + " + 1]/td/a[contains(text(),\"[view]\")]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie que l'on est sur la bonne page.
		assertEquals("Braves", selenium.getText("//*[contains(@id,'txt_TeamName')]"));
		assertEquals("Omaha", selenium.getText("//*[contains(@id,'txt_Teamcity')]"));
		// On teste les liens.
		// On va sur la page 2.
		selenium.click("xpath=(//span/a)[1]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie qu'on est sur la page 2.
		assertTrue(selenium.isElementPresent("//a/b[contains(text(),\"2\")]|//strong[contains(text(),\"2\")]"));
		// On va tester le lien fast next.
//		selenium.click("//a[img[@alt=\"Fast next\"]]");
//		selenium.waitForPageToLoad("30000");
//		// Les sauts de pages sont de 3 pages. On v�rifie que l'on est bien sur la 5eme page.
//		assertTrue(selenium.isElementPresent("//a/b[contains(text(),\"5\")]|//strong[contains(text(),\"5\")]"));
//		// On teste le lien fast prev.
//		selenium.click("//a[img[@alt=\"Fast prev\"]]");
//		selenium.waitForPageToLoad("30000");
//		// Les sauts de pages sont de 3 pages. On v�rife que l'on est bien sur la 2eme page.
//		assertTrue(selenium.isElementPresent("//a/b[contains(text(),\"2\")]|//strong[contains(text(),\"2\")]"));
		// On teste le lien Prev.
		selenium.click("xpath=(//span/a)[2]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie que l'on est sur la premi�re page.
		assertTrue(selenium.isElementPresent("//a/b[contains(text(),\"1\")]|//strong[contains(text(),\"1\")]"));
		// On teste le lien Last.
		selenium.click("xpath=(//span/a)[4]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//a/b[contains(text(),\"5\")]|//strong[contains(text(),\"5\")]"));
		// On teste le lien First
		selenium.click("xpath=(//span/a)[1]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//a/b[contains(text(),\"1\")]|//strong[contains(text(),\"1\")]"));
		// On clique sur [back]
		selenium.click("//a[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//h2[@class=\"rd_first\"]"));
		// Pour Raleigh
		selenium.click("link=UC34 - Pagination Table");
		selenium.waitForPageToLoad("30000");
		verifyTrue(selenium.isElementPresent("//h2[contains(text(),\"Standard Features > Application > DataGrid > UC34 > Pagination Table\")]"));
		// On clique sur 'Next'
		selenium.click("xpath=(//span/a)[1]");
		selenium.waitForPageToLoad("30000");
		// On indexe la ligne de Raleigh.
	index = selenium.getElementIndex("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Raleigh\")] and td/span[contains(text(),\"Eagles\")]]");
		System.out.println("variable index vaut : " + index);
		// On clique sur [view]
		selenium.click("//table[contains(@id,'tab_teams')]/tbody/tr[" + index + " + 1]/td/a[contains(text(),\"[view]\")]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie que l'on est sur la bonne page.
		assertEquals("Eagles", selenium.getText("//*[contains(@id,'txt_TeamName')]"));
		assertEquals("Raleigh", selenium.getText("//*[contains(@id,'txt_Teamcity')]"));
		// On clique sur [back]
		selenium.click("//a[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//h2[@class=\"rd_first\"]"));
		// Pour les Red Devils
		selenium.click("link=UC34 - Pagination Table");
		selenium.waitForPageToLoad("30000");
		verifyTrue(selenium.isElementPresent("//h2[contains(text(),\"Standard Features > Application > DataGrid > UC34 > Pagination Table\")]"));
		// On clique sur 'Next'
		selenium.click("xpath=(//span/a)[1]");
		selenium.waitForPageToLoad("30000");
		// On indexe la ligne de Kansas City.
		 index = selenium.getElementIndex("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Kansas City\")] and td/span[contains(text(),\"Red Devils\")]]");
		System.out.println("variable index vaut : " + index);
		// On clique sur [view]
		selenium.click("//table[contains(@id,'tab_teams')]/tbody/tr[" + index + " + 1]/td/a[contains(text(),\"[view]\")]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie que l'on est sur la bonne page.
		assertEquals("Red Devils", selenium.getText("//*[contains(@id,'txt_TeamName')]"));
		assertEquals("Kansas City", selenium.getText("//*[contains(@id,'txt_Teamcity')]"));
		// On clique sur [back]
		selenium.click("//a[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//h2[@class=\"rd_first\"]"));
	}

}
