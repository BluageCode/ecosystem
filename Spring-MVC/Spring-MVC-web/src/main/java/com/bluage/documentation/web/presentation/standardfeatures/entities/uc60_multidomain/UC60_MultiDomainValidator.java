/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.entities.uc60_multidomain ;


import org.apache.log4j.Logger;
import org.springframework.validation.Errors;

import com.netfective.bluage.core.validator.AbstractValidator;
/**
* Class :UC60_MultiDomainValidator
*/
public class UC60_MultiDomainValidator extends AbstractValidator{
	
	// LOGGER for the class UC60_MultiDomainValidator
	private static final Logger LOGGER = Logger.getLogger( UC60_MultiDomainValidator.class);
	
	
	/**
	* Operation validate for UC60_MultiDomainForm
	* @param obj : the current form (UC60_MultiDomainForm)
	* @param errors : The spring errors to return for the form UC60_MultiDomainForm
	*/
	public void validate(final Object obj,final Errors errors){
		LOGGER.info("Beginning method : validate"); // UC60_MultiDomainValidator
		UC60_MultiDomainForm cUC60_MultiDomainForm = (UC60_MultiDomainForm)obj; // UC60_MultiDomainValidator
		LOGGER.info("Ending method : validate the form "+ cUC60_MultiDomainForm.getClass().getName()); // UC60_MultiDomainValidator
	}

	/**
	* Method to implements to use spring validators (UC60_MultiDomainForm)
	* @param aClass : Class for the form UC60_MultiDomainForm
	* @return boolean
	*/
	public boolean supports(final Class aClass){
		return (new UC60_MultiDomainForm()).getClass().equals(aClass); // UC60_MultiDomainValidator
	}
}
