/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.entities.uc40_defaultsort ;

// Import declaration.
// Java imports.
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;

/**
 * Class : UC40_ExceptionController
 */
@Controller
@Scope("prototype")
@SessionAttributes("uC40_ExceptionForm")
@RequestMapping(value= "/presentation/standardfeatures/entities/uc40_defaultsort")
public class UC40_ExceptionController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC40_ExceptionController.class);
	
	//Current form name
	private static final String U_C40__EXCEPTION_FORM = "uC40_ExceptionForm";

	
	/**
	 * Property:customDateEditorsUC40_ExceptionController
	 */
	final private Map<String,CustomDateEditor> customDateEditorsUC40_ExceptionController = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:uC40_ExceptionValidator
	 */
	final private UC40_ExceptionValidator uC40_ExceptionValidator = new UC40_ExceptionValidator();
	
	
	// Initialise all the instances for UC40_ExceptionController

	/**
	* This method initialise the form : UC40_ExceptionForm 
	* @return UC40_ExceptionForm
	*/
	@ModelAttribute("UC40_ExceptionFormInit")
	public void initUC40_ExceptionForm(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, U_C40__EXCEPTION_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method UC40_ExceptionForm."); //for lnk_sort 
		}
		UC40_ExceptionForm uC40_ExceptionForm;
	
		if(request.getSession().getAttribute(U_C40__EXCEPTION_FORM) != null){
			uC40_ExceptionForm = (UC40_ExceptionForm)request.getSession().getAttribute(U_C40__EXCEPTION_FORM);
		} else {
			uC40_ExceptionForm = new UC40_ExceptionForm();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form UC40_ExceptionForm.");
		}
		uC40_ExceptionForm = (UC40_ExceptionForm)populateDestinationForm(request.getSession(), uC40_ExceptionForm);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  UC40_ExceptionForm.");
		}
		model.addAttribute(U_C40__EXCEPTION_FORM, uC40_ExceptionForm);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param UC40_ExceptionForm : The sceen form.
	 */
	@RequestMapping(value = "/UC40_Exception.html" ,method = RequestMethod.GET)
	public void prepareUC40_Exception(final HttpServletRequest request,final  Model model,final  @ModelAttribute("UC40_ExceptionFormInit") UC40_ExceptionForm uC40_ExceptionForm){
		
		UC40_ExceptionForm currentUC40_ExceptionForm = uC40_ExceptionForm;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(U_C40__EXCEPTION_FORM) == null){
			if(currentUC40_ExceptionForm!=null){
				request.getSession().setAttribute(U_C40__EXCEPTION_FORM, currentUC40_ExceptionForm);
			}else {
				currentUC40_ExceptionForm = new UC40_ExceptionForm();
				request.getSession().setAttribute(U_C40__EXCEPTION_FORM, currentUC40_ExceptionForm);	
			}
		} else {
			currentUC40_ExceptionForm = (UC40_ExceptionForm) request.getSession().getAttribute(U_C40__EXCEPTION_FORM);
		}

		currentUC40_ExceptionForm = (UC40_ExceptionForm)populateDestinationForm(request.getSession(), currentUC40_ExceptionForm);
		// Call all the Precontroller.
	currentUC40_ExceptionForm.setAlwaysCallPreControllers(true);
		model.addAttribute(U_C40__EXCEPTION_FORM, currentUC40_ExceptionForm);
		request.getSession().setAttribute(U_C40__EXCEPTION_FORM, currentUC40_ExceptionForm);
		
	}
	
	
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("UC40_ExceptionController [ ");
		// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(uC40_ExceptionValidator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsUC40_ExceptionController!=null); 
		return strBToS.toString();
	}
}
