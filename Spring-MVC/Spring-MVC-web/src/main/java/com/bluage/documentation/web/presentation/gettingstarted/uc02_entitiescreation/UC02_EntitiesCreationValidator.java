/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.gettingstarted.uc02_entitiescreation ;


import org.apache.log4j.Logger;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;

import com.netfective.bluage.core.validator.AbstractValidator;
/**
* Class :UC02_EntitiesCreationValidator
*/
public class UC02_EntitiesCreationValidator extends AbstractValidator{
	
	// LOGGER for the class UC02_EntitiesCreationValidator
	private static final Logger LOGGER = Logger.getLogger( UC02_EntitiesCreationValidator.class);
	
	
	/**
	* Operation validate for UC02_EntitiesCreationForm
	* @param obj : the current form (UC02_EntitiesCreationForm)
	* @param errors : The spring errors to return for the form UC02_EntitiesCreationForm
	*/
	public void validate(final Object obj,final Errors errors){
		LOGGER.info("Beginning method : validate"); // UC02_EntitiesCreationValidator
		UC02_EntitiesCreationForm cUC02_EntitiesCreationForm = (UC02_EntitiesCreationForm)obj; // UC02_EntitiesCreationValidator
				ValidationUtils.rejectIfEmpty(errors, "teamToCreate.name", "", "Team name is required.");

					ValidationUtils.rejectIfEmpty(errors, "teamToCreate.city", "", "City name is required.");

			LOGGER.info("Ending method : validate the form "+ cUC02_EntitiesCreationForm.getClass().getName()); // UC02_EntitiesCreationValidator
	}

	/**
	* Method to implements to use spring validators (UC02_EntitiesCreationForm)
	* @param aClass : Class for the form UC02_EntitiesCreationForm
	* @return boolean
	*/
	public boolean supports(final Class aClass){
		return (new UC02_EntitiesCreationForm()).getClass().equals(aClass); // UC02_EntitiesCreationValidator
	}
}
