/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.business.standardfeatures.application.datagrid.uc38_selectedlist.bos;

import javax.inject.Inject;
import javax.inject.Named;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import com.bluage.documentation.business.common.bos.PlayerBO;
import com.bluage.documentation.business.standardfeatures.application.datagrid.uc38_selectedlist.entities.daofinder.Player38DAOFinderImpl;

/**
 * Player38BO
 *
* @see com.netfective.bluage.core.business.IBusinessObject
* * @see PlayerBO
 */
@XmlRootElement

public class Player38BO
 extends PlayerBO {
	
	/**
	 * Default SUID for Player38BO.
	 */
	private static final long serialVersionUID = 1L;
	

	@Inject
	@Named ("com.bluage.documentation.business.standardfeatures.application.datagrid.uc38_selectedlist.entities.daofinder.Player38Finder") 
	private Player38DAOFinderImpl finder;

												/**
	 * Entity property: team38
	 *
	 */
private Team38BO team38;



   /**
	* public default constructor for class Player38BO
	*/
	public Player38BO(){
		//NOP Player38BO
	}


												/**
	 * Getter for team38 of class Player38BO.
	 *
	 * @return {@link com.bluage.documentation.business.standardfeatures.application.datagrid.uc38_selectedlist.bos.Team38BO}
	 * @generated
	 */ 
public  Team38BO getTeam38() {
		Team38BO team380 = this.team38;
		if(this.team38 == null) {
			if(DAO_MODE.equals(this.getObjMode( ) ) ) {
				team380 = null;
			} else {
				this.team38 = new Team38BO();
				this.team38.setObjMode( this.getObjMode( ) );
				team380 = this.team38;
			}
		} else {
			this.team38.setObjMode( this.getObjMode( ) );
			if( HELPER_MODE.equals(this.getObjMode( ) ) ) {
				team380 = this.team38;
			}
			if( DAO_MODE.equals(this.getObjMode()) && this.team38.isEmpty ) {
				team380 = null;
			}
		}
		return team380;
	}


	/**
	 * Setter for team38 of class Player38BO.
	 * @param value {@link com.bluage.documentation.business.standardfeatures.application.datagrid.uc38_selectedlist.bos.Team38BO}
	 * @generated
	 */

public  void setTeam38(final Team38BO team38) {
	if(team38 != null) {
		team38.isEmpty = false;
		team38.setObjMode( this.getObjMode( ) );
	}
	this.isEmpty = false;
	this.team38 = team38;
}


/**
	 * Returns <code>true</code> if the argument is an Player38 instance and all identifiers for this entity
     * equal the identifiers of the argument entity. The <code>equals</code> method of the parent entity
     * will also need to return <code>true</code>. Returns <code>false</code> otherwise.
     *
     * @see PlayerBO#hashCode()
	 * @param object  object to compare with current instance      
     * @return boolean true if equals false if not
     */
    public boolean equals(final Object object )
    {
		if ( this == object )
        {
            return true;
        }
        if ( !( object instanceof Player38BO ) )
        {
            return false;
        }
        final Player38BO that = (Player38BO)object;
    
    	            if (        				        			this.getId() == null
        	        ) {
        	return false;
        }
		return new EqualsBuilder()
			.append(this.getId(), that.getId())
			 	.isEquals();
	}
	/**

	 * Returns a hash code based on this entity's identifiers and the hash code of the parent entity.
     *
     * @see PlayerBO#hashCode()
     * @return int
     */    
    public int hashCode() {
        return new HashCodeBuilder()
             .append(getId())
				 	.appendSuper(super.hashCode())
            .toHashCode();
    }
}

