<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file="/include.jsp"  %>
<html >
<head >
<title  > 
<fmt:message key="cUC05_View_BLU_AGE_DOCUMENTATION"/>
</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"  > 
</meta>
<link href="<c:url value= "/presentation/_style/_style.css"/>" rel="stylesheet" type="text/css"  /> 
<link href="<c:url value= "/presentation/_include/jquery/rd_lib.css"/>" rel="stylesheet" type="text/css"  /> 
<script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/jquery.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/rd_setup.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/rd_lib.js"/>" space="preserve"  ></script><link rel="shortcut icon" href="<c:url value= "/presentation/_style/favicon.ico"/>"  /> 

</head>

<body class=" " > 
<div id="rd_ghost"  > 
<small  > 
<fmt:message key="cUC05_View___CSS_DEACTIVATED"/>
</small>
</div>
<div id="rd_jghost"  > 
<small  > 
<fmt:message key="cUC05_View___JAVASCRIPT_DEACTIVATED"/>
</small>
</div>
<div id="shape"  > 
<div class="ui-layout-center"  > 
<div class="east-north"  > 
	<form:form id="uC05_View" commandName="uC05_ViewForm"  enctype="application/x-www-form-urlencoded" method="post">
<h2 class="rd_first"  > 
<fmt:message key="cUC05_View_GETTING_STARTED__UC05__SEARC"/>
</h2>
<p align="right"  > 
<a id="lnk_back"				href="<c:url value="/presentation/gettingstarted/uc05_search/UC05_View/lnk_back.html"/>"
													 shape="rect"  >									 
<fmt:message key="cUC05_View_BACK"/>
</a>
</p>
<table cellpadding="0" cellspacing="0" class="table-form"  >
<tr  > 
<td colspan="1" rowspan="1" width="100"  >
<strong  > 
<fmt:message key="cUC05_View_FIRST_NAME_"/>
</strong>
</td> 
<td colspan="1" rowspan="1" width="409"  >
		<span id="dyn_firstname">
	<c:out value="${uC05_ViewForm.currentPlayer.firstName}" />
		</span>
	</td> 
<td colspan="1" rowspan="1" width="100"  >
<strong  > 
<fmt:message key="cUC05_View_LAST_NAME_"/>
</strong>
</td> 
<td colspan="1" rowspan="1"  >
		<span id="dyn_lastname">
	<c:out value="${uC05_ViewForm.currentPlayer.lastName}" />
		</span>
	</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<fmt:message key="cUC05_View_"/>
</td> 
<td rowspan="1" colspan="1"  >
<fmt:message key="cUC05_View__69"/>
</td> 
</tr>
<tr  > 
<td colspan="1" rowspan="1" width="100"  >
<strong  > 
<fmt:message key="cUC05_View_DATE_OF_BIRTH_"/>
</strong>
</td> 
<td colspan="1" rowspan="1" width="409"  >
<span id="dyn_dateofbirth">
	<format:formatDateValidator value="${uC05_ViewForm.currentPlayer.dateOfBirth}" pattern="MM/dd/yyyy" />
</span>
	</td> 
<td colspan="1" rowspan="1" width="120"  >
<strong  > 
<fmt:message key="cUC05_View_ESTIMATED_VALUE_"/>
</strong>
</td> 
<td colspan="1" rowspan="1"  >
		<span id="dyn_estimatedvalue">
	<c:out value="${uC05_ViewForm.currentPlayer.estimatedValue}" />
		</span>
	<fmt:message key="cUC05_View_M_______"/>
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<fmt:message key="cUC05_View__70"/>
</td> 
<td rowspan="1" colspan="1"  >
<fmt:message key="cUC05_View__71"/>
</td> 
</tr>
<tr  > 
<td colspan="1" rowspan="1" width="100"  >
<strong  > 
<fmt:message key="cUC05_View_WEIGHT_"/>
</strong>
</td> 
<td colspan="1" rowspan="1" width="409"  >
		<span id="dyn_dweight">
	<c:out value="${uC05_ViewForm.currentPlayer.weight}" />
		</span>
	<fmt:message key="cUC05_View_KG_______"/>
</td> 
<td colspan="1" rowspan="1" width="100"  >
<strong  > 
<fmt:message key="cUC05_View_HEIGHT_"/>
</strong>
</td> 
<td colspan="1" rowspan="1"  >
		<span id="dyn_height">
	<c:out value="${uC05_ViewForm.currentPlayer.height}" />
		</span>
	<fmt:message key="cUC05_View_CM_______"/>
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<fmt:message key="cUC05_View__72"/>
</td> 
<td rowspan="1" colspan="1"  >
<fmt:message key="cUC05_View__73"/>
</td> 
</tr>
<tr  > 
<td colspan="1" rowspan="1" width="100"  >
<strong  > 
<fmt:message key="cUC05_View_POSITION_"/>
</strong>
</td> 
<td colspan="1" rowspan="1"  >
		<span id="dyn_positionname">
	<c:out value="${uC05_ViewForm.currentPlayer.position.name}" />
		</span>
	<fmt:message key="cUC05_View__________________"/>
		<span id="dyn_positioncode">
	<c:out value="${uC05_ViewForm.currentPlayer.position.code}" />
		</span>
	<fmt:message key="cUC05_View______________________________"/>
</td> 
</tr>
</table> 
	</form:form>
	<script language="javascript">
		function btn_click_cExecuteUC05_View(url) {
			UC05_Viewvar = document.getElementById('uC05_View');
			UC05_Viewvar.setAttribute('action', url);
			UC05_Viewvar.submit();
		}
		function btn_click_cSetActionUC05_View(url) {
			UC05_Viewvar = document.getElementById('uC05_View');
			UC05_Viewvar.setAttribute('action', url);
		}
	</script>
</div>
<div class="east-center"  > 
<tiles:insertDefinition	name="presentation/gettingstarted/uc05_search/UC05_Viewcmp_tabs" />
</div>
</div>
<div class="ui-layout-north"  > 
<tiles:insertDefinition	name="presentation/gettingstarted/uc05_search/UC05_Viewcmp_header" />
</div>
<div class="ui-layout-south"  > 
<fmt:message key="cUC05_View___"/>
<span id="rd_jversion"  > 
<fmt:message key="cUC05_View_BLU_AGE_CORPORATION"/>
</span>
</div>
<div class="ui-layout-west"  > 
<tiles:insertDefinition	name="presentation/gettingstarted/uc05_search/UC05_Viewcmp_treeview" />
</div>
</div>
</body>
</html>
