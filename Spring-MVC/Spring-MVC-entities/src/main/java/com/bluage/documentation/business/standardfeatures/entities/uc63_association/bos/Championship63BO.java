/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.business.standardfeatures.entities.uc63_association.bos;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.inject.Inject;
import javax.inject.Named;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.LazyInitializationException;

import com.bluage.documentation.business.AbstractBusinessObject;
import com.bluage.documentation.business.standardfeatures.entities.uc63_association.entities.daofinder.Championship63DAOFinderImpl;
import com.netfective.bluage.core.exception.ApplicationException;

/**
 * Championship63BO
 *
* @see com.netfective.bluage.core.business.IBusinessObject
 */
@XmlRootElement

public class Championship63BO
 extends AbstractBusinessObject {
	
	/**
	 * Default SUID for Championship63BO.
	 */
	private static final long serialVersionUID = 1L;
	

	@Inject
	@Named ("com.bluage.documentation.business.standardfeatures.entities.uc63_association.entities.daofinder.Championship63Finder") 
	private Championship63DAOFinderImpl finder;
	/**
	 * Hibernate optimistic lock for class Championship63BO.
	 */
	private int version;

						/**
	 * Entity property: code
	 *
	 */
private String code;

					/**
	 * Entity property: name
	 *
	 */
private String name;

																		/**
	 * Entity property: player63s
	 *
	 */
private Set<Player63BO> player63s = new HashSet<Player63BO>();



   /**
	* public default constructor for class Championship63BO
	*/
	public Championship63BO(){
		//NOP Championship63BO
	}

	
	/**
	 * Getter for version for class Championship63BO
	 * @return int
	 */
	public int getVersion() {
		return this.version;
	}
	
	/**
	 * Setter for version for class Championship63BO
	 * @param int version
	 */
	public void setVersion(final int version) {
		this.version = version;
	}

						/**
	 * Getter for code of class Championship63BO.
	 *
	 * @return {@link java.lang.String}
	 * @generated
	 */ 
public  String getCode() {
	return this.code;
	}


	/**
	 * Setter for code of class Championship63BO.
	 * @param value {@link java.lang.String}
	 * @generated
	 */

public  void setCode(final String code) {
	this.isEmpty = (code == null);
	this.code = code;
}

					/**
	 * Getter for name of class Championship63BO.
	 *
	 * @return {@link java.lang.String}
	 * @generated
	 */ 
public  String getName() {
	return this.name;
	}


	/**
	 * Setter for name of class Championship63BO.
	 * @param value {@link java.lang.String}
	 * @generated
	 */

public  void setName(final String name) {
	this.name = name;
}

																		/**
	 * Getter for player63s of class Championship63BO.
	 *
	 * @return {@link java.util.Set<com.bluage.documentation.business.standardfeatures.entities.uc63_association.bos.Player63BO>}
	 * @generated
	 */ 
public  Set<Player63BO> getPlayer63s() {
	return this.player63s;
			}


	/**
	 * Setter for player63s of class Championship63BO.
	 * @param value {@link java.util.Set<com.bluage.documentation.business.standardfeatures.entities.uc63_association.bos.Player63BO>}
	 * @generated
	 */

public  void setPlayer63s(final Set<Player63BO> player63s) {
	this.player63s = player63s;
}

	/**
	 * Remove an entity from the com.bluage.documentation.business.standardfeatures.entities.uc63_association.bos.Player63BO collection.
	 *
	 * @param entity
	 *		: {@link Set<Player63BO>} the entity
	 * @throws ApplicationException
     */
public void removeFromPlayer63s( Player63BO entity ) throws ApplicationException {
	try {
		if( getPlayer63s() != null && getPlayer63s().contains(entity) ) {
			getPlayer63s().remove( entity );
		entity.getChampionship63s().remove(this);
		}
		} catch ( LazyInitializationException e ) {
				Championship63BO bo = ( Championship63BO ) finder.findByPrimaryKey( getCode() );
			finder.getHibernateTemplate().initialize( bo.getPlayer63s() );
			setPlayer63s( bo.getPlayer63s() );
			// evict object from session to avoid NonUniqueObjectException
			finder.getHibernateTemplate().evict( bo );
		
			if( getPlayer63s() != null && getPlayer63s().contains( entity ) ) {
				getPlayer63s().remove( entity );
				entity.getChampionship63s().remove( this );
			}
				}
	}
	
	/**
	 * Add entity to the com.bluage.documentation.business.standardfeatures.entities.uc63_association.bos.Player63BO collection.
	 *
	 * @param entity
	 *		{@link Player63BO} the entity
	 * @throws ApplicationException
     */
public void addToPlayer63s( Player63BO entity ) throws ApplicationException
	{
		try {
			if( getPlayer63s() != null && !getPlayer63s().contains( entity ) ) {
				getPlayer63s().add( entity );
				entity.getChampionship63s().add( this );
			}
			} catch ( LazyInitializationException e ) {
														Championship63BO bo = ( Championship63BO ) finder.findByPrimaryKey( getCode() );
		finder.getHibernateTemplate().initialize( bo.getPlayer63s() );
		setPlayer63s( bo.getPlayer63s() );
		// evict object from session to avoid NonUniqueObjectException
		finder.getHibernateTemplate().evict( bo );
		if( getPlayer63s() != null && !getPlayer63s().contains( entity ) ) {
			getPlayer63s().add( entity );
			entity.getChampionship63s().add( this );
		}
			}
	}
	
	/**
	 * Add entitys to the com.bluage.documentation.business.standardfeatures.entities.uc63_association.bos.Player63BO collection
	 *
	 * @param entities			
	 *		: {@link java.util.List} A list of entities
	 * @throws ApplicationException 
     */	
public void addAllToPlayer63s( Collection<Player63BO> entities ) throws ApplicationException {
	for( Player63BO entity : entities) {
		if( getPlayer63s()!=null ) {
			addToPlayer63s( entity );
		}
	}
}
	
	/**
	 * Remove entities form the com.bluage.documentation.business.standardfeatures.entities.uc63_association.bos.Player63BO collection
	 *
	 * @param entities				
	 *		: {@link java.util.List} a list of entities
	 * @throws ApplicationException
     */	
public void removeAllFromPlayer63s( Collection<Player63BO> entities ) throws ApplicationException {
	for( Player63BO entity : entities) {
		if( getPlayer63s()!=null && getPlayer63s().contains( entity ) )	{		
			removeFromPlayer63s( entity );
		}
	}
}
	
	/**
	 * Remove all items form the collection com.bluage.documentation.business.standardfeatures.entities.uc63_association.bos.Player63BO
	 *
	 * @throws ApplicationException
     */	
public void removeAllFromPlayer63s() throws ApplicationException {
	Set<Player63BO> entities = this.getPlayer63s();
	if ( entities == null ) {
		return;
	}
	Iterator<Player63BO> it = entities.iterator();
	while ( it.hasNext() ) {
    	Player63BO entity = it.next(); 
		entity.setChampionship63s( null ); 
	}
	entities.clear();
}

/**
	 * Returns <code>true</code> if the argument is an Championship63 instance and all identifiers for this entity
     * equal the identifiers of the argument entity. Returns <code>false</code> otherwise.
	 * @param object  object to compare with current instance      
     * @return boolean true if equals false if not
     */
    public boolean equals(final Object object )
    {
		if ( this == object )
        {
            return true;
        }
        if ( !( object instanceof Championship63BO ) )
        {
            return false;
        }
        final Championship63BO that = (Championship63BO)object;
    
    	            if (        				        			this.getCode() == null
        	        ) {
        	return false;
        }
		return new EqualsBuilder()
			.append(this.getCode(), that.getCode())
			 	.appendSuper(super.equals(object))
		 	.isEquals();
	}
	/**
     * Returns a hash code based on this entity's identifiers.     * @return int
     */    
    public int hashCode() {
        return new HashCodeBuilder()
             .append(getCode())
		            .toHashCode();
    }
}

