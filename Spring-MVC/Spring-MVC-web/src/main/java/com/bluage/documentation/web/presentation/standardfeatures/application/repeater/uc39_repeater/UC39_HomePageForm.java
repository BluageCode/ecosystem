/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.application.repeater.uc39_repeater;


import java.io.Serializable;
import java.util.List;

import com.netfective.bluage.core.web.AbstractForm;
import com.netfective.bluage.core.web.IForm;

/**
* Class : UC39_HomePageForm
*/
public class UC39_HomePageForm extends AbstractForm implements Serializable, IForm {
	
	/** Constant : serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	//CONSTANT : teams1
	private static final String TEAMS1 = "teams1";
	//CONSTANT : teams3
	private static final String TEAMS3 = "teams3";
	//CONSTANT : teams2
	private static final String TEAMS2 = "teams2";
	/**
	 * 	Property: teams1 
	 */
	private List teams1;
	/**
	 * 	Property: teams3 
	 */
	private List teams3;
	/**
	 * 	Property: teams2 
	 */
	private List teams2;
/**
	 * Default constructor : UC39_HomePageForm
	 */
	public UC39_HomePageForm() {
		super();
		// Initialize : teams1
		this.teams1 = null;
		// Initialize : teams3
		this.teams3 = null;
		// Initialize : teams2
		this.teams2 = null;
			this.setAlwaysCallPreControllers(true);
	}
	
	/**
	 * 	Getter : teams1 
	 * 	@return : Return the teams1 instance.
	 */
	public List getTeams1(){
		return teams1; // For UC39_HomePageForm
	}
	
	/**
	 * 	Setter : teams1 
	 *  @param teams1instance : The instance to set.
	 */
	public void setTeams1(final List teams1instance){
		this.teams1 = teams1instance;// For UC39_HomePageForm
	}
	/**
	 * 	Getter : teams3 
	 * 	@return : Return the teams3 instance.
	 */
	public List getTeams3(){
		return teams3; // For UC39_HomePageForm
	}
	
	/**
	 * 	Setter : teams3 
	 *  @param teams3instance : The instance to set.
	 */
	public void setTeams3(final List teams3instance){
		this.teams3 = teams3instance;// For UC39_HomePageForm
	}
	/**
	 * 	Getter : teams2 
	 * 	@return : Return the teams2 instance.
	 */
	public List getTeams2(){
		return teams2; // For UC39_HomePageForm
	}
	
	/**
	 * 	Setter : teams2 
	 *  @param teams2instance : The instance to set.
	 */
	public void setTeams2(final List teams2instance){
		this.teams2 = teams2instance;// For UC39_HomePageForm
	}
/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UC39_HomePageForm [ "+
TEAMS1 +" = " + teams1 +TEAMS3 +" = " + teams3 +TEAMS2 +" = " + teams2 + "]";
	}
	
	/**
	 * Method setInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public void setInstanceByInstanceName(final String instanceName, final Object instance) {
		// Set the instance Teams1.
		if(TEAMS1.equals(instanceName)){// For UC39_HomePageForm
			this.setTeams1((List)instance); // For UC39_HomePageForm
		}
				// Set the instance Teams3.
		if(TEAMS3.equals(instanceName)){// For UC39_HomePageForm
			this.setTeams3((List)instance); // For UC39_HomePageForm
		}
				// Set the instance Teams2.
		if(TEAMS2.equals(instanceName)){// For UC39_HomePageForm
			this.setTeams2((List)instance); // For UC39_HomePageForm
		}
			}
	
	/**
	 * Method getInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public Object getInstanceByInstanceName(final String instanceName){
		// Temporary object for tmpUC39_HomePageForm.
		Object tmpUC39_HomePageForm = null;
		
			
		// Get the instance Teams1 for UC39_HomePageForm.
		if(TEAMS1.equals(instanceName)){ // For UC39_HomePageForm
			tmpUC39_HomePageForm = this.getTeams1(); // For UC39_HomePageForm
		}
			
		// Get the instance Teams3 for UC39_HomePageForm.
		if(TEAMS3.equals(instanceName)){ // For UC39_HomePageForm
			tmpUC39_HomePageForm = this.getTeams3(); // For UC39_HomePageForm
		}
			
		// Get the instance Teams2 for UC39_HomePageForm.
		if(TEAMS2.equals(instanceName)){ // For UC39_HomePageForm
			tmpUC39_HomePageForm = this.getTeams2(); // For UC39_HomePageForm
		}
		return tmpUC39_HomePageForm;// For UC39_HomePageForm
	}
	
	}
