<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file="/include.jsp"  %>
<html >
<head >
<title  > 
<fmt:message key="cUC57_LoadPlayers_BLU_AGE_DOCUMENTATION"/>
</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"  > 
</meta>
<link href="<c:url value= "/presentation/_style/_style.css"/>" rel="stylesheet" type="text/css"  /> 
<link href="<c:url value= "/presentation/_include/jquery/rd_lib.css"/>" rel="stylesheet" type="text/css"  /> 
<script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/jquery.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/rd_setup.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/rd_lib.js"/>" space="preserve"  ></script><link rel="shortcut icon" href="<c:url value= "/presentation/_style/favicon.ico"/>"  /> 

</head>

<body class=" " > 
<div id="rd_ghost"  > 
<small  > 
<fmt:message key="cUC57_LoadPlayers___CSS_DEACTIVATED"/>
</small>
</div>
<div id="rd_jghost"  > 
<small  > 
<fmt:message key="cUC57_LoadPlayers___JAVASCRIPT_DEACTIVATED"/>
</small>
</div>
<div id="shape"  > 
<div class="ui-layout-center"  > 
<div class="east-north"  > 
	<form:form id="uC57_LoadPlayers" commandName="uC57_LoadPlayersForm"  enctype="application/x-www-form-urlencoded" method="post">
<h2 class="rd_first"  > 
<fmt:message key="cUC57_LoadPlayers_STANDARD_FEATURES__SERVICES_"/>
</h2>
<b  > 
<fmt:message key="cUC57_LoadPlayers__TEAM__"/>
</b>
		<span id="txt_TeamName">
	<c:out value="${uC57_LoadPlayersForm.team.name}" />
		</span>
	<b  > 
<fmt:message key="cUC57_LoadPlayers___CITY_"/>
</b>
		<span id="txt_Teamcity">
	<c:out value="${uC57_LoadPlayersForm.team.city}" />
		</span>
	<fmt:message key="cUC57_LoadPlayers__"/>
<a id="lnk_back"				href="<c:url value="/presentation/standardfeatures/services/hql/uc57_hqloperation/UC57_Search.html"/>"
													 shape="rect"  >									 
<fmt:message key="cUC57_LoadPlayers_BACK"/>
</a>
<h4  > 
<fmt:message key="cUC57_LoadPlayers_PLAYERS"/>
</h4>
<display:table uid="tab_players" pagesize="10"  sort="list" requestURI="" name="sessionScope.uC57_LoadPlayersForm.players" class="rd_tabledata"  >
	<c:set   var="rownum">
		<%=(tab_players !=	null) ? new Integer(tab_players_rowNum.intValue() - 1) : new Integer(0) %>
	</c:set>
	<c:set   var="idRow">
		<%=(tab_players != null ) ? tab_players_rowNum.intValue() - 1 : -1 %>
	</c:set>
	<c:set   var="idTab">
		<%=(tab_players != null ) ? "tab_players" : null %>
	</c:set>
	<c:set var="tableHashId">
		<%= new org.displaytag.util.ParamEncoder("tab_players").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE) %>
	</c:set>
	<c:set var="tableHashId2">
		<%=request.getParameter(new org.displaytag.util.ParamEncoder("tab_players").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE)) %>
	</c:set>	
	
<display:column format=""  titleKey="cUC57_LoadPlayers_NAME">
		<span id="txt_firstName">
	<c:out value="${tab_players.firstName}" />
		</span>
	<fmt:message key="cUC57_LoadPlayers___"/>
		<span id="txt_lastName">
	<c:out value="${tab_players.lastName}" />
		</span>
	</display:column>
</display:table>
	</form:form>
	<script language="javascript">
		function btn_click_cExecuteUC57_LoadPlayers(url) {
			UC57_LoadPlayersvar = document.getElementById('uC57_LoadPlayers');
			UC57_LoadPlayersvar.setAttribute('action', url);
			UC57_LoadPlayersvar.submit();
		}
		function btn_click_cSetActionUC57_LoadPlayers(url) {
			UC57_LoadPlayersvar = document.getElementById('uC57_LoadPlayers');
			UC57_LoadPlayersvar.setAttribute('action', url);
		}
	</script>
</div>
<div class="east-center"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/services/hql/uc57_hqloperation/UC57_LoadPlayerscmp_tabs" />
</div>
</div>
<div class="ui-layout-north"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/services/hql/uc57_hqloperation/UC57_LoadPlayerscmp_header" />
</div>
<div class="ui-layout-south"  > 
<fmt:message key="cUC57_LoadPlayers____207"/>
<span id="rd_jversion"  > 
<fmt:message key="cUC57_LoadPlayers_BLU_AGE_DOCUMENTATION_208"/>
</span>
</div>
<div class="ui-layout-west"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/services/hql/uc57_hqloperation/UC57_LoadPlayerscmp_treeview" />
</div>
</div>
</body>
</html>
