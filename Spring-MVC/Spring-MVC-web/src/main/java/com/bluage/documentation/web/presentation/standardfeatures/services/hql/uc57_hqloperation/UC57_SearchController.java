/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.services.hql.uc57_hqloperation ;

// Import declaration.
// Java imports.
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.bluage.documentation.business.standardfeatures.services.hql.uc57_hqloperation.bos.Screen57BO;
import com.bluage.documentation.business.standardfeatures.services.hql.uc57_hqloperation.bos.Team57BO;
import com.bluage.documentation.service.standardfeatures.services.hql.uc57_hqloperation.ServiceLoadPlayers57;
import com.bluage.documentation.service.standardfeatures.services.hql.uc57_hqloperation.ServiceSearch57;
import com.netfective.bluage.core.exception.ApplicationException;
import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;
import com.netfective.bluage.core.web.IForm;

/**
 * Class : UC57_SearchController
 */
@Controller
@Scope("prototype")
@SessionAttributes("uC57_SearchForm")
@RequestMapping(value= "/presentation/standardfeatures/services/hql/uc57_hqloperation")
public class UC57_SearchController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC57_SearchController.class);
	
	//Current form name
	private static final String U_C57__SEARCH_FORM = "uC57_SearchForm";

						//CONSTANT: searchResult
	private static final String SEARCH_RESULT = "searchResult";
	//CONSTANT: hqlCriteria
	private static final String HQL_CRITERIA = "hqlCriteria";
	//CONSTANT: hqlAttribute
	private static final String HQL_ATTRIBUTE = "hqlAttribute";
	//CONSTANT: team
	private static final String TEAM = "team";
	//CONSTANT: selectedTeam
	private static final String SELECTED_TEAM = "selectedTeam";
	
	/**
	 * Property:customDateEditorsUC57_SearchController
	 */
	final private Map<String,CustomDateEditor> customDateEditorsUC57_SearchController = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:uC57_SearchValidator
	 */
	final private UC57_SearchValidator uC57_SearchValidator = new UC57_SearchValidator();
	
	/**
	 * Service declaration : serviceLoadPlayers57.
	 */
	@Autowired
	private ServiceLoadPlayers57 serviceLoadPlayers57;
	
	/**
	 * Service declaration : serviceSearch57.
	 */
	@Autowired
	private ServiceSearch57 serviceSearch57;
	
	
	// Initialise all the instances for UC57_SearchController
								// Declare the instance searchResult
		private List searchResult;
		// Declare the instance hqlCriteria
		private Team57BO hqlCriteria;
		// Declare the instance hqlAttribute
		private Screen57BO hqlAttribute;
		// Declare the instance team
		private Team57BO team;
		// Declare the instance selectedTeam
		private Team57BO selectedTeam;
			/**
	 * Operation : lnk_searchTeams
 	 * @param model : 
 	 * @param uC57_Search : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC57_Search/lnk_searchTeams.html",method = RequestMethod.POST)
	public String lnk_searchTeams(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC57_SearchForm") UC57_SearchForm  uC57_SearchForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : lnk_searchTeams"); 
		infoLogger(uC57_SearchForm); 
		uC57_SearchForm.setAlwaysCallPreControllers(false); 
		// initialization for lnk_searchTeams
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state lnk_searchTeams if the validation fail, you will be forward to the screen : "/presentation/standardfeatures/services/hql/uc57_hqloperation/UC57_Search"
		if(errorsMessages(request,response,uC57_SearchForm,bindingResult,"", uC57_SearchValidator, customDateEditorsUC57_SearchController)){ 
			return "/presentation/standardfeatures/services/hql/uc57_hqloperation/UC57_Search"; 
		}

									 callSearchteambycriteria(request,uC57_SearchForm );
						// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.services.hql.uc57_hqloperation.UC57_SearchForm or not from lnk_searchTeams
			// Populate the destination form with all the instances returned from lnk_searchTeams.
			final IForm uC57_SearchForm2 = populateDestinationForm(request.getSession(), uC57_SearchForm); 
					
			// Add the form to the model.
			model.addAttribute("uC57_SearchForm", uC57_SearchForm2); 
			
			request.getSession().setAttribute("uC57_SearchForm", uC57_SearchForm2);
			
			// "OK" CASE => destination screen path from lnk_searchTeams
			LOGGER.info("Go to the screen 'UC57_Search'.");
			// Redirect (PRG) from lnk_searchTeams
			destinationPath =  "redirect:/presentation/standardfeatures/services/hql/uc57_hqloperation/UC57_Search.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_searchTeams 
	}
	
				/**
	 * Operation : lnk_loadPlayers
 	 * @param model : 
 	 * @param uC57_Search : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC57_Search/lnk_loadPlayers.html",method = RequestMethod.POST)
	public String lnk_loadPlayers(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC57_SearchForm") UC57_SearchForm  uC57_SearchForm, final BindingResult bindingResult ,final @RequestParam(ID_REQUEST_PARAM) Integer index){
		
		
		
		LOGGER.info("Begin method : lnk_loadPlayers"); 
		infoLogger(uC57_SearchForm); 
		uC57_SearchForm.setAlwaysCallPreControllers(false); 
		// initialization for lnk_loadPlayers
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state lnk_loadPlayers if the validation fail, you will be forward to the screen : "/presentation/standardfeatures/services/hql/uc57_hqloperation/UC57_Search"
		if(errorsMessages(request,response,uC57_SearchForm,bindingResult,"selectedTeam", uC57_SearchValidator, customDateEditorsUC57_SearchController)){ 
			return "/presentation/standardfeatures/services/hql/uc57_hqloperation/UC57_Search"; 
		}

										 callSearchPlayers(request,uC57_SearchForm , index);
											// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.services.hql.uc57_hqloperation.UC57_LoadPlayersForm or not from lnk_loadPlayers
			final UC57_LoadPlayersForm uC57_LoadPlayersForm =  new UC57_LoadPlayersForm();
			// Populate the destination form with all the instances returned from lnk_loadPlayers.
			final IForm uC57_LoadPlayersForm2 = populateDestinationForm(request.getSession(), uC57_LoadPlayersForm); 
					
			// Add the form to the model.
			model.addAttribute("uC57_LoadPlayersForm", uC57_LoadPlayersForm2); 
			
			request.getSession().setAttribute("uC57_LoadPlayersForm", uC57_LoadPlayersForm2);
			
			// "OK" CASE => destination screen path from lnk_loadPlayers
			LOGGER.info("Go to the screen 'UC57_LoadPlayers'.");
			// Redirect (PRG) from lnk_loadPlayers
			destinationPath =  "redirect:/presentation/standardfeatures/services/hql/uc57_hqloperation/UC57_LoadPlayers.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_loadPlayers 
	}
	
	
	/**
	* This method initialise the form : UC57_SearchForm 
	* @return UC57_SearchForm
	*/
	@ModelAttribute("UC57_SearchFormInit")
	public void initUC57_SearchForm(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, U_C57__SEARCH_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method UC57_SearchForm."); //for lnk_loadPlayers 
		}
		UC57_SearchForm uC57_SearchForm;
	
		if(request.getSession().getAttribute(U_C57__SEARCH_FORM) != null){
			uC57_SearchForm = (UC57_SearchForm)request.getSession().getAttribute(U_C57__SEARCH_FORM);
		} else {
			uC57_SearchForm = new UC57_SearchForm();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form UC57_SearchForm.");
		}
		uC57_SearchForm = (UC57_SearchForm)populateDestinationForm(request.getSession(), uC57_SearchForm);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  UC57_SearchForm.");
		}
		model.addAttribute(U_C57__SEARCH_FORM, uC57_SearchForm);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param UC57_SearchForm : The sceen form.
	 */
	@RequestMapping(value = "/UC57_Search.html" ,method = RequestMethod.GET)
	public void prepareUC57_Search(final HttpServletRequest request,final  Model model,final  @ModelAttribute("UC57_SearchFormInit") UC57_SearchForm uC57_SearchForm){
		
		UC57_SearchForm currentUC57_SearchForm = uC57_SearchForm;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(U_C57__SEARCH_FORM) == null){
			if(currentUC57_SearchForm!=null){
				request.getSession().setAttribute(U_C57__SEARCH_FORM, currentUC57_SearchForm);
			}else {
				currentUC57_SearchForm = new UC57_SearchForm();
				request.getSession().setAttribute(U_C57__SEARCH_FORM, currentUC57_SearchForm);	
			}
		} else {
			currentUC57_SearchForm = (UC57_SearchForm) request.getSession().getAttribute(U_C57__SEARCH_FORM);
		}

					currentUC57_SearchForm = (UC57_SearchForm)populateDestinationForm(request.getSession(), currentUC57_SearchForm);
		// Call all the Precontroller.
	currentUC57_SearchForm.setAlwaysCallPreControllers(true);
		model.addAttribute(U_C57__SEARCH_FORM, currentUC57_SearchForm);
		request.getSession().setAttribute(U_C57__SEARCH_FORM, currentUC57_SearchForm);
		
	}
	
										/**
	 * method callSearchteambycriteria
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callSearchteambycriteria(HttpServletRequest request,IForm form) {
		searchResult = (List)get(request.getSession(),form, "searchResult");  
										 
					hqlCriteria = (Team57BO)get(request.getSession(),form, "hqlCriteria");  
										 
					hqlAttribute = (Screen57BO)get(request.getSession(),form, "hqlAttribute");  
										 
					try {
				// executing Searchteambycriteria in lnk_loadPlayers
				searchResult = 	serviceSearch57.executeSearch(
			hqlCriteria
		,			hqlAttribute
			);  
 
				put(request.getSession(), SEARCH_RESULT,searchResult);
								// processing variables Searchteambycriteria in lnk_loadPlayers
				put(request.getSession(), HQL_CRITERIA,hqlCriteria); 
							put(request.getSession(), HQL_ATTRIBUTE,hqlAttribute); 
			
			} catch (ApplicationException e) { 
				// error handling for operation executeSearch called Searchteambycriteria
				errorLogger("An error occured during the execution of the operation : executeSearch",e); 
		
			}
	}
									/**
	 * method callSearchPlayers
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 * @param index Integer the current index 
	 */
	private void callSearchPlayers(HttpServletRequest request,IForm form,Integer index) {
		team = (Team57BO)get(request.getSession(),form, "team");  
										 
					if(index!=null){  
			 selectedTeam = (Team57BO)((List)get(request.getSession(),form, "searchResult")).get(index);  
		} else {
			 selectedTeam= null; 
		}
				 
					try {
				// executing SearchPlayers in lnk_loadPlayers
				team = 	serviceLoadPlayers57.loadPlayers(
			selectedTeam
			);  
 
				put(request.getSession(), TEAM,team);
								// processing variables SearchPlayers in lnk_loadPlayers
				put(request.getSession(), SELECTED_TEAM,selectedTeam); 
			
			} catch (ApplicationException e) { 
				// error handling for operation loadPlayers called SearchPlayers
				errorLogger("An error occured during the execution of the operation : loadPlayers",e); 
		
			}
	}
		
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("UC57_SearchController [ ");
								strBToS.append(SEARCH_RESULT);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(searchResult);
			strBToS.append(END_TO_STRING_DELIMITER);
			strBToS.append(HQL_CRITERIA);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(hqlCriteria);
			strBToS.append(END_TO_STRING_DELIMITER);
			strBToS.append(HQL_ATTRIBUTE);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(hqlAttribute);
			strBToS.append(END_TO_STRING_DELIMITER);
			strBToS.append(TEAM);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(team);
			strBToS.append(END_TO_STRING_DELIMITER);
			strBToS.append(SELECTED_TEAM);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(selectedTeam);
			strBToS.append(END_TO_STRING_DELIMITER);
		// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(uC57_SearchValidator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsUC57_SearchController!=null); 
		return strBToS.toString();
	}
}
