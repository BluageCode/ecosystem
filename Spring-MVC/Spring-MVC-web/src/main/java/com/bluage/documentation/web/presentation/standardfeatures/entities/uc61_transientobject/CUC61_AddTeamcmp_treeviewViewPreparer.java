/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.web.presentation.standardfeatures.entities.uc61_transientobject;

import org.apache.log4j.Logger;
import org.apache.tiles.AttributeContext;
import org.apache.tiles.context.TilesRequestContext;
import org.apache.tiles.preparer.ViewPreparer;

import com.bluage.documentation.web.master.components.cmp_treeviewForm;
import com.netfective.bluage.core.common.IStringConstants;
/**
 * Class : CUC61_AddTeamcmp_treeviewViewPreparer
 * Prepare the component cmp_treeview for the screen UC61_AddTeam.
 * 
 */
public class CUC61_AddTeamcmp_treeviewViewPreparer implements ViewPreparer , IStringConstants{

	private static final String UC61_ADDTEAMCMP_TREEVIEW = "cmp_treeviewComponentBean";
	
	private static final String COMPONENT_FORM_NAME = "com.bluage.documentation.web.master.components.cmp_treeviewForm";
	
	private static final String COMPONENT_SHORT_FORM_NAME = "cmp_treeviewForm";

	/**
	 * Property:LOGGER
	 */
	private static final Logger LOGGER = Logger.getLogger( CUC61_AddTeamcmp_treeviewViewPreparer.class);

	/**
	 * Method execute for CUC61_AddTeamcmp_treeviewViewPreparer
	 * @param tilesContext : The tiles context of the component  CUC61_AddTeamcmp_treeviewViewPreparer
	 * @param attributeContext : The attribute context of the component  CUC61_AddTeamcmp_treeviewViewPreparer
	 * @see org.apache.tiles.preparer.ViewPreparer#execute(org.apache.tiles.context.TilesRequestContext, org.apache.tiles.AttributeContext)
	 */
	public void execute(TilesRequestContext tilesContext, AttributeContext attributeContext) {
		
		LOGGER.info("Starting the component initialisation for CUC61_AddTeamcmp_treeviewViewPreparer "+ UC61_ADDTEAMCMP_TREEVIEW +" Component Form name : "+COMPONENT_FORM_NAME);
		

		// Init the component cmp_treeviewForm
		final cmp_treeviewForm componentScreen = new cmp_treeviewForm(); // For the form cmp_treeviewForm
		
		tilesContext.getSessionScope().put(COMPONENT_SHORT_FORM_NAME, componentScreen); // For the component CUC61_AddTeamcmp_treeviewViewPreparer
		
		LOGGER.info("The component is initialized (CUC61_AddTeamcmp_treeviewViewPreparer).");
	}
}
