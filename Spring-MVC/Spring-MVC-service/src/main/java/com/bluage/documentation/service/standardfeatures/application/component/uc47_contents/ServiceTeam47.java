/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.service.standardfeatures.application.component.uc47_contents;


import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import com.bluage.documentation.business.standardfeatures.application.component.uc47_contents.bos.Player47BO;
import com.bluage.documentation.business.standardfeatures.application.component.uc47_contents.bos.Team47BO;
import com.bluage.documentation.business.standardfeatures.application.component.uc47_contents.entities.daofinder.Team47DAOFinderImpl;
import com.bluage.documentation.service.AbstractService;
import com.bluage.documentation.service.standardfeatures.application.component.uc47_contents.daofinder.ServiceTeam47DAOMethodFinderImpl;
import com.netfective.bluage.core.exception.ApplicationException;

/**
 * Defines  application services for ServiceTeam47
  */
@Named("com.bluage.documentation.service.standardfeatures.application.component.uc47_contents.ServiceTeam47")
public class ServiceTeam47  extends AbstractService{


									@Inject
		@Named ("com.bluage.documentation.service.standardfeatures.application.component.uc47_contents.daofinder.ServiceTeam47DAOMethodFinder")
		private ServiceTeam47DAOMethodFinderImpl serviceTeam47Dao;
		@Inject
		@Named ("com.bluage.documentation.business.standardfeatures.application.component.uc47_contents.entities.daofinder.Team47DAOFinderImpl")
		private Team47DAOFinderImpl team47BOFinder;
	


	/**
	 *  Default constructor 
	 */
	public ServiceTeam47() {
		super(); // Call super for default constructor : ServiceTeam47
	}
	


			
				
	/**    
	 * Operation Crud
	 * @param bo {@link Team47BO}
		 * @return {@link Team47BO}
	 */
	public Team47BO team47FindByID(Team47BO bo )
 throws ApplicationException {

					return (Team47BO)team47BOFinder.findByPrimaryKey(getBoId(bo));
	}
	/**
	 *  Return the primary key from business object.
	 */
	private Object getBoId(Team47BO bo) {
				return bo.getId();
		}

		
				
	/**    
	 * Operation HQL.
	 * @param id {@link Long}
		 * @return {@link List}
	 */
	public List returnTeam47(Long id )
 throws ApplicationException {
	
	return serviceTeam47Dao.returnTeam47(id);
	}

		
			
	/**    
	 * Process operation.
	 * @param playerToUpdate {@link Player47BO}
		 * @return {@link List}
	 */
	public List returnTeamByPlayer47(Player47BO playerToUpdate )
 throws ApplicationException {

				return privateReturnTeamByPlayer47(playerToUpdate);
	}
	

	/**    
	 * Process operation.
	 * @param playerToUpdate {@link Player47BO}
		 * @return {@link List}
	 */
	private List privateReturnTeamByPlayer47(Player47BO playerToUpdate )
 throws ApplicationException {
						// Return Type declaration.
				List listTeam = new java.util.ArrayList();
					// Central buffer node declaration for operation returnTeamByPlayer47
			Long	 id =  Long.valueOf(0);
					// Calling an operation : 
id = playerToUpdate.getId();
// Calling an operation : 
listTeam = returnTeam47(id);

						return listTeam;
		}

		
			
	/**    
	 * Process operation.
	 * @param playerToUpdate {@link Player47BO}
		 * @return {@link List}
	 */
	public List searchTeamsNotPlayedByPlayer47(Player47BO playerToUpdate )
 throws ApplicationException {

				return privateSearchTeamsNotPlayedByPlayer47(playerToUpdate);
	}
	

	/**    
	 * Process operation.
	 * @param playerToUpdate {@link Player47BO}
		 * @return {@link List}
	 */
	private List privateSearchTeamsNotPlayedByPlayer47(Player47BO playerToUpdate )
 throws ApplicationException {
						// Return Type declaration.
				List listTeam = new java.util.ArrayList();
					// Central buffer node declaration for operation searchTeamsNotPlayedByPlayer47
			Long	 id =  Long.valueOf(0);
					// Calling an operation : get Id
id = playerToUpdate.getId();
// Calling an operation : Search Teams
listTeam = searchTeamsNotPlayed(id);

						return listTeam;
		}

		
				
	/**    
	 * Operation HQL.
	 * @param id {@link Long}
		 * @return {@link List}
	 */
	public List searchTeamsNotPlayed(Long id )
 throws ApplicationException {
	
	return serviceTeam47Dao.searchTeamsNotPlayed(id);
	}

		}
