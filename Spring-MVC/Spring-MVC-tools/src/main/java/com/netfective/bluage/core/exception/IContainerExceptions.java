/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.netfective.bluage.core.exception;
import java.util.List;

/**
 * @author NETFECTIVE TECHNOLOGY
 */
public interface IContainerExceptions {	
	
	/**
	 * Adds an errorMessage to applicationExceptions
	 * @param em
	 */
	void addApplicationException(ErrorMessage em);
	/**
	 * clears the errorMessages in applicationExceptions and sets the emptyApplicationExceptions flag to true
	 */
	void clearApplicationExceptions();
	/**
	 * clears the errorMessages in applicationExceptions
	 */
	void clearExceptionContainers();
	/**
	 * getter for applicationExceptions
	 * @return applicationExceptions List
	 */
	List getApplicationExceptions();
	/**
	 * getter for emptyApplicationExceptions
	 * @return emptyApplicationExceptions boolean
	 */
	boolean isEmptyApplicationExceptions();
	/**
	 * setter for applicationExceptions
	 * @param list List
	 */
	void setApplicationExceptions(List list);
	/**
	 * setter for emptyApplicationExceptions
	 * @param b boolean
	 */
	void setEmptyApplicationExceptions(boolean b);
	/**
	 * getter for existException
	 * @return existException boolean
	 */
	boolean isExistException();
	/**
	 * setter for existException
	 * @param b boolean
	 */
	void setExistException(boolean b);
}
