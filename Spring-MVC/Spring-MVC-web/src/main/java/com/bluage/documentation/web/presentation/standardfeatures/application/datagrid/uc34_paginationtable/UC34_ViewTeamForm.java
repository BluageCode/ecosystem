/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.application.datagrid.uc34_paginationtable;


import java.io.Serializable;
import java.util.List;

import com.bluage.documentation.business.standardfeatures.application.datagrid.uc34_paginationtable.bos.Player34BO;
import com.bluage.documentation.business.standardfeatures.application.datagrid.uc34_paginationtable.bos.Team34BO;
import com.netfective.bluage.core.web.AbstractForm;
import com.netfective.bluage.core.web.IForm;

/**
* Class : UC34_ViewTeamForm
*/
public class UC34_ViewTeamForm extends AbstractForm implements Serializable, IForm {
	
	/** Constant : serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	//CONSTANT : teamDetails
	private static final String TEAM_DETAILS = "teamDetails";
	//CONSTANT : listplayers
	private static final String LISTPLAYERS = "listplayers";
	/**
	 * 	Property: teamDetails 
	 */
	private Team34BO teamDetails;
	/**
	 * 	Property: listplayers 
	 */
	private List<Player34BO> listplayers;
/**
	 * Default constructor : UC34_ViewTeamForm
	 */
	public UC34_ViewTeamForm() {
		super();
		// Initialize : teamDetails
		this.teamDetails = new Team34BO();
		// Initialize : listplayers
		this.listplayers = new java.util.ArrayList<com.bluage.documentation.business.standardfeatures.application.datagrid.uc34_paginationtable.bos.Player34BO>();
					this.setAlwaysCallPreControllers(true);
	}
	
	/**
	 * 	Getter : teamDetails 
	 * 	@return : Return the teamDetails instance.
	 */
	public Team34BO getTeamDetails(){
		return teamDetails; // For UC34_ViewTeamForm
	}
	
	/**
	 * 	Setter : teamDetails 
	 *  @param teamDetailsinstance : The instance to set.
	 */
	public void setTeamDetails(final Team34BO teamDetailsinstance){
		this.teamDetails = teamDetailsinstance;// For UC34_ViewTeamForm
	}
	/**
	 * 	Getter : listplayers 
	 * 	@return : Return the listplayers instance.
	 */
	public List<Player34BO> getListplayers(){
		return listplayers; // For UC34_ViewTeamForm
	}
	
	/**
	 * 	Setter : listplayers 
	 *  @param listplayersinstance : The instance to set.
	 */
	public void setListplayers(final List<Player34BO> listplayersinstance){
		this.listplayers = listplayersinstance;// For UC34_ViewTeamForm
	}
/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UC34_ViewTeamForm [ "+
TEAM_DETAILS +" = " + teamDetails +LISTPLAYERS +" = " + listplayers + "]";
	}
	
	/**
	 * Method setInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public void setInstanceByInstanceName(final String instanceName, final Object instance) {
		// Set the instance TeamDetails.
		if(TEAM_DETAILS.equals(instanceName)){// For UC34_ViewTeamForm
			this.setTeamDetails((Team34BO)instance); // For UC34_ViewTeamForm
		}
				// Set the instance Listplayers.
		if(LISTPLAYERS.equals(instanceName)){// For UC34_ViewTeamForm
			this.setListplayers((List<Player34BO>)instance); // For UC34_ViewTeamForm
		}
				// Parent instance name for UC34_ViewTeamForm
		if(TEAM_DETAILS.equals(instanceName) && teamDetails != null){ // For UC34_ViewTeamForm
			listplayers.clear(); // For UC34_ViewTeamForm
			listplayers.addAll(teamDetails.getPlayers());// For UC34_ViewTeamForm
		}
		
	}
	
	/**
	 * Method getInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public Object getInstanceByInstanceName(final String instanceName){
		// Temporary object for tmpUC34_ViewTeamForm.
		Object tmpUC34_ViewTeamForm = null;
		
			
		// Get the instance TeamDetails for UC34_ViewTeamForm.
		if(TEAM_DETAILS.equals(instanceName)){ // For UC34_ViewTeamForm
			tmpUC34_ViewTeamForm = this.getTeamDetails(); // For UC34_ViewTeamForm
		}
			
		// Get the instance Listplayers for UC34_ViewTeamForm.
		if(LISTPLAYERS.equals(instanceName)){ // For UC34_ViewTeamForm
			tmpUC34_ViewTeamForm = this.getListplayers(); // For UC34_ViewTeamForm
		}
		return tmpUC34_ViewTeamForm;// For UC34_ViewTeamForm
	}
	
			}
