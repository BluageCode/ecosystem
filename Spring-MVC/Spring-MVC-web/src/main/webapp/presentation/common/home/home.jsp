<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file="/include.jsp"  %>
<html >
<head >
<title  > 
<fmt:message key="chome_BLU_AGE_DOCUMENTATION"/>
</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"  > 
</meta>
<link href="<c:url value= "/presentation/_style/_style.css"/>" rel="stylesheet" type="text/css"  /> 
<link href="<c:url value= "/presentation/_include/jquery/rd_lib.css"/>" rel="stylesheet" type="text/css"  /> 
<script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/jquery.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/rd_setup.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/rd_lib.js"/>" space="preserve"  ></script><link rel="shortcut icon" href="<c:url value= "/presentation/_style/favicon.ico"/>"  /> 
<script type="text/javascript" space="preserve"  >   var _gaq = _gaq || [];   _gaq.push(['_setAccount', 'UA-3792216-9']);   _gaq.push(['_trackPageview']);   (function() {     var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;     ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';     var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);   })(); 
</script>
</head>

<body class=" " > 
<div id="rd_ghost"  > 
<small  > 
<fmt:message key="chome___CSS_DEACTIVATED"/>
</small>
</div>
<div id="rd_jghost"  > 
<small  > 
<fmt:message key="chome___JAVASCRIPT_DEACTIVATED"/>
</small>
</div>
<div id="shape"  > 
<div class="ui-layout-center"  > 
<div class="east-north"  > 
	<form:form id="home" commandName="homeForm"  enctype="application/x-www-form-urlencoded" method="post">
<h2 class="rd_first"  > 
<fmt:message key="chome_OVERVIEW"/>
</h2>
<p  > 
<fmt:message key="chome________WELCOME_TO_THE_BLUAGE"/>
<br clear="none"  /> 
<fmt:message key="chome_PLEASE_TAKE_SOME_TIME_TO_READ_"/>
</p>
<p  > 
<fmt:message key="chome________"/>
<b  > 
<fmt:message key="chome_BLUAGE_IN_ACTION_PIM_CREATIO"/>
</b>
<fmt:message key="chome__IS_A_WEB_APPLICATION_FOR_EXP"/>
</p>
<p  > 
<fmt:message key="chome_________20"/>
<b  > 
<fmt:message key="chome_BLUAGE_IN_ACTION_PIM_CREATIO_21"/>
</b>
<fmt:message key="chome__HAS_ONE_PRIMARY_GOAL______"/>
</p>
<div style="padding-left: 20px"  > 
<fmt:message key="chome________PROVIDE_NONBLUAGE_CON"/>
<br clear="none"  /> 
</div>
<p  > 
<fmt:message key="chome_________22"/>
<b  > 
<fmt:message key="chome_BLUAGE_IN_ACTION_PIM_CREATIO_23"/>
</b>
<fmt:message key="chome__IS_NOT_AN_EXHAUSTIVE_VIEW_OF"/>
<a href="mailto:contact@bluage.com" shape="rect"  >
<fmt:message key="chome_CONTACTBLUAGECOM"/>
</a> 
</p>
<p  > 
<fmt:message key="chome_________24"/>
<b  > 
<fmt:message key="chome_BLUAGE_IN_ACTION_PIM_CREATIO_25"/>
</b>
<fmt:message key="chome__MODEL_MOCKUP_AND_GENERATED_"/>
</p>
<p  > 
<fmt:message key="chome_THIS_WEB_APPLICATION_IS_A_REPO"/>
</p>
<div style="padding-left: 20px"  > 
<fmt:message key="chome_________"/>
<b  > 
<fmt:message key="chome_GETTING_STARTED"/>
</b>
<fmt:message key="chome__SIMPLE_USE_CASES_WHEN_YOU_JU"/>
<br clear="none"  /> 
<fmt:message key="chome___"/>
<b  > 
<fmt:message key="chome_STANDARD_FEATURES"/>
</b>
<fmt:message key="chome__STANDARD_USAGE_OF_BLU_AGE"/>
<br clear="none"  /> 
<fmt:message key="chome____26"/>
<b  > 
<fmt:message key="chome_ADVANCED_FEATURES"/>
</b>
<fmt:message key="chome__ADVANCED_FEATURES_OF_BLU_AGE"/>
<br clear="none"  /> 
<fmt:message key="chome__"/>
<i  > 
<fmt:message key="chome_BASKETBALL_INFORMATION_ARE_USE"/>
</i>
<br clear="none"  /> 
</div>
<p  > 
<fmt:message key="chome________UNDER_EACH_OF_THESE_CAT"/>
<br clear="none"  /> 
</p>
<p  > 
<fmt:message key="chome_ALL_USE_CASES_ARE_ORGANIZED_IN"/>
</p>
<div style="padding-left: 20px"  > 
<fmt:message key="chome__________27"/>
<b  > 
<fmt:message key="chome_LEFT_PART"/>
</b>
<fmt:message key="chome__LIST_OF_USE_CASES_AND_CATEGO"/>
<br clear="none"  /> 
<fmt:message key="chome____28"/>
<b  > 
<fmt:message key="chome_RIGHTTOP_PART"/>
</b>
<fmt:message key="chome__EXECUTION_OF_THE_USE_CASE"/>
<br clear="none"  /> 
<fmt:message key="chome____29"/>
<b  > 
<fmt:message key="chome_RIGHTBOTTOM_PART"/>
</b>
<fmt:message key="chome__DOCUMENTATION_OF_THE_USE_CAS"/>
<br clear="none"  /> 
<div style="padding-left: 20px"  > 
<fmt:message key="chome__________"/>
<b  > 
<fmt:message key="chome_DESCRIPTION"/>
</b>
<fmt:message key="chome__GENERAL_DESCRIPTION_OF_THE_U"/>
<br clear="none"  /> 
<fmt:message key="chome___________30"/>
<b  > 
<fmt:message key="chome_MODEL"/>
</b>
<fmt:message key="chome__SCREEN_SHOTS_OF_ALL_THE_MODE"/>
<br clear="none"  /> 
<fmt:message key="chome____31"/>
<b  > 
<fmt:message key="chome_MOCKUP"/>
</b>
<fmt:message key="chome__HTML_SOURCE_CODE_OF_ALL_THE_"/>
<br clear="none"  /> 
<fmt:message key="chome___________32"/>
<b  > 
<fmt:message key="chome_WORKFLOW"/>
</b>
<fmt:message key="chome__SPECIFIC_WORKFLOW_CONFIGURAT"/>
<br clear="none"  /> 
<fmt:message key="chome___________33"/>
<b  > 
<fmt:message key="chome_GENERATED_CODE"/>
</b>
<fmt:message key="chome__FOCUS_ON_SPECIFIC_GENERATED_"/>
<br clear="none"  /> 
<fmt:message key="chome___________34"/>
<b  > 
<fmt:message key="chome_LINKS"/>
</b>
<fmt:message key="chome__EXTERNAL_LINKS_RELATED_TO_TH"/>
<br clear="none"  /> 
<fmt:message key="chome___________35"/>
<b  > 
<fmt:message key="chome_UPDATED"/>
</b>
<fmt:message key="chome__UPDATES_IMPLEMENTED_THROUGH_"/>
<br clear="none"  /> 
<fmt:message key="chome_________THE_MODEL_AND_MOCKUP_T"/>
<br clear="none"  /> 
</div>
</div>
<p  > 
<fmt:message key="chome________YOU_CAN_RESIZE_ALL_THE_"/>
<br clear="none"  /> 
<fmt:message key="chome__YOU_CAN_ALSO_DISPLAY__HIDE_A"/>
</p>
<p  > 
<fmt:message key="chome_EACH_USE_CASE_HAS"/>
</p>
<div style="padding-left: 20px"  > 
<fmt:message key="chome_________OWN_NUMBER_AND_NAME_"/>
<br clear="none"  /> 
<fmt:message key="chome_________OWN_ENTITIES_NAMED_BY"/>
<br clear="none"  /> 
<fmt:message key="chome_________3_PACKAGES_ENTITIES"/>
<br clear="none"  /> 
<fmt:message key="chome_________OWN_FOLDER_INCLUDING_"/>
</div>
<div style="visibility: hidden;"  > 
<a id="lien_cache"onclick="javascript:btn_click_cExecutehome(this.href);return false;"				href="<c:url value="/presentation/common/home/home/lien_cache.html"/>"
							 shape="rect"  >									 
<fmt:message key="chome_HOME"/>
</a>
</div>
	</form:form>
	<script language="javascript">
		function btn_click_cExecutehome(url) {
			homevar = document.getElementById('home');
			homevar.setAttribute('action', url);
			homevar.submit();
		}
		function btn_click_cSetActionhome(url) {
			homevar = document.getElementById('home');
			homevar.setAttribute('action', url);
		}
	</script>
</div>
<div class="east-center"  > 
<tiles:insertDefinition	name="presentation/common/home/homecmp_tabs" />
</div>
</div>
<div class="ui-layout-north"  > 
<tiles:insertDefinition	name="presentation/common/home/homecmp_header" />
</div>
<div class="ui-layout-south"  > 
<fmt:message key="chome______"/>
<span id="rd_jversion"  > 
<fmt:message key="chome_BLU_AGE_CORP"/>
</span>
</div>
<div class="ui-layout-west"  > 
<tiles:insertDefinition	name="presentation/common/home/homecmp_treeview" />
</div>
</div>
</body>
</html>
