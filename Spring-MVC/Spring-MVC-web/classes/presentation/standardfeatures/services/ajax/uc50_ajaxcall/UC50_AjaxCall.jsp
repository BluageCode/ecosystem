<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file="/include.jsp"  %>
<html >
<head >
<title  > 
<fmt:message key="cUC50_AjaxCall_BLU_AGE_DOCUMENTATION"/>
</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"  > 
</meta>
<link href="<c:url value= "/presentation/_style/_style.css"/>" rel="stylesheet" type="text/css"  /> 
<link href="<c:url value= "/presentation/_include/jquery/rd_lib.css"/>" rel="stylesheet" type="text/css"  /> 
<script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/jquery.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/rd_setup.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/rd_lib.js"/>" space="preserve"  ></script><link rel="shortcut icon" href="<c:url value= "/presentation/_style/favicon.ico"/>"  /> 

</head>

<body class=" " > 
<div id="rd_ghost"  > 
<small  > 
<fmt:message key="cUC50_AjaxCall___CSS_DEACTIVATED"/>
</small>
</div>
<div id="rd_jghost"  > 
<small  > 
<fmt:message key="cUC50_AjaxCall___JAVASCRIPT_DEACTIVATED"/>
</small>
</div>
<div id="shape"  > 
<div class="ui-layout-center"  > 
<div class="east-north"  > 
	<form:form id="uC50_AjaxCall" commandName="uC50_AjaxCallForm"  enctype="application/x-www-form-urlencoded" method="post">
<h2 class="rd_first"  > 
<fmt:message key="cUC50_AjaxCall_STANDARD_FEATURES__SERVICES_"/>
</h2>
<table class="rd_tableform"  >
<tr  > 
<td colspan="1" rowspan="1" width="200"  >
<label  > 
<fmt:message key="cUC50_AjaxCall_CHOOSE_A_TEAM_"/>
</label>
</td> 
<td colspan="1" rowspan="1" width="409"  >
<a4s:ajaxZone ajaxRendered="false" id="ajax_select" >
<form:select id="sel_team" path="selectedTeam.id"   
		>
		<form:options items="${uC50_AjaxCallForm.teamslist}" itemValue="id" itemLabel="name" />
</form:select>
</a4s:ajaxZone>
</td> 
<td rowspan="1" colspan="1"  >
<fmt:message key="cUC50_AjaxCall_"/>
</td> 
</tr>
<tr  > 
<td colspan="1" rowspan="1" width="200"  >
<label  > 
<fmt:message key="cUC50_AjaxCall_ROOKIE_"/>
</label>
</td> 
<td colspan="1" rowspan="1" width="409"  >
<a4s:ajaxZone ajaxRendered="false" id="ajax_checkbox" >
<form:checkbox id="chk_rookie" path="isRookie.rookie"   
		/>
</a4s:ajaxZone>
</td> 
<td rowspan="1" colspan="1"  >
<fmt:message key="cUC50_AjaxCall__201"/>
</td> 
</tr>
<tr  > 
<td colspan="1" rowspan="1" width="200"  >
<label  > 
<fmt:message key="cUC50_AjaxCall_ENTER_A_MINIMUM_VALUE_"/>
</label>
</td> 
<td colspan="1" rowspan="1" width="409"  >
<a4s:ajaxZone ajaxRendered="false" id="ajax_txt" >
<form:input id="txt_minimum_value" path="minimumValue.estimatedValue"  
		/>
<label id="hid_err_lbl" style="visibility:hidden; text-align:left"  > 
<fmt:message key="cUC50_AjaxCall_PLEASE_ENTER_A_CORRECT_VALUE"/>
</label>
</a4s:ajaxZone>
</td> 
<td rowspan="1" colspan="1"  >
<form:errors path="minimumValue.estimatedValue"/>
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<fmt:message key="cUC50_AjaxCall__202"/>
</td> 
<td colspan="1" rowspan="1" width="100"  >
<a4s:ajaxZone ajaxRendered="false" id="ajax_link" >
<a id="lnk_value"onclick="javascript:btn_click_cExecuteUC50_AjaxCall(this.href);return false;"				href="<c:url value="/presentation/standardfeatures/services/ajax/uc50_ajaxcall/UC50_AjaxCall/lnk_value.html"/>"
							 shape="rect"  >									 
<fmt:message key="cUC50_AjaxCall_VALIDATE_CRITERIA"/>
</a>
</a4s:ajaxZone>
</td> 
<td rowspan="1" colspan="1"  >
<fmt:message key="cUC50_AjaxCall__203"/>
</td> 
</tr>
</table> 
<br clear="none"  /> 
<a4s:ajaxZone ajaxRendered="false" id="ajax_table" >
<display:table uid="tab_players" pagesize="10"  sort="list" requestURI="" name="sessionScope.uC50_AjaxCallForm.players" class="rd_tabledata"  >
	<c:set   var="rownum">
		<%=(tab_players !=	null) ? new Integer(tab_players_rowNum.intValue() - 1) : new Integer(0) %>
	</c:set>
	<c:set   var="idRow">
		<%=(tab_players != null ) ? tab_players_rowNum.intValue() - 1 : -1 %>
	</c:set>
	<c:set   var="idTab">
		<%=(tab_players != null ) ? "tab_players" : null %>
	</c:set>
	<c:set var="tableHashId">
		<%= new org.displaytag.util.ParamEncoder("tab_players").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE) %>
	</c:set>
	<c:set var="tableHashId2">
		<%=request.getParameter(new org.displaytag.util.ParamEncoder("tab_players").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE)) %>
	</c:set>	
	
<display:column format=""  titleKey="cUC50_AjaxCall_FIRST_NAME">
		<span id="txt_firstname">
	<c:out value="${tab_players.firstName}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="cUC50_AjaxCall_LAST_NAME">
		<span id="txt_lastname">
	<c:out value="${tab_players.lastName}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="cUC50_AjaxCall_HEIGHT_CM">
		<span id="txt_height">
	<c:out value="${tab_players.height}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="cUC50_AjaxCall_WEIGHT_KG">
		<span id="txt_weight">
	<c:out value="${tab_players.weight}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="cUC50_AjaxCall_ESTIMATED_VALUE_M">
		<span id="txt_estimatedvalue">
	<c:out value="${tab_players.estimatedValue}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="cUC50_AjaxCall_DATE_OF_BIRTH">
<span id="txt_dateofbirth">
	<format:formatDateValidator value="${tab_players.dateOfBirth}" pattern="MM/dd/yyyy" />
</span>
	</display:column>
	
<display:column format=""  titleKey="cUC50_AjaxCall_CURRENT_TEAM">
		<span id="txt_team">
	<c:out value="${tab_players.team.name}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="cUC50_AjaxCall_ROOKIE">
<form:checkbox id="chk_rookie_table[${rownum}]" path="players[${rownum}].rookie"  name="chkRookieTable" disabled="true"  
		/>
</display:column>
</display:table>
</a4s:ajaxZone>
	</form:form>
	<script language="javascript">
		function btn_click_cExecuteUC50_AjaxCall(url) {
			UC50_AjaxCallvar = document.getElementById('uC50_AjaxCall');
			UC50_AjaxCallvar.setAttribute('action', url);
			UC50_AjaxCallvar.submit();
		}
		function btn_click_cSetActionUC50_AjaxCall(url) {
			UC50_AjaxCallvar = document.getElementById('uC50_AjaxCall');
			UC50_AjaxCallvar.setAttribute('action', url);
		}
	</script>
</div>
<div class="east-center"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/services/ajax/uc50_ajaxcall/UC50_AjaxCallcmp_tabs" />
</div>
</div>
<div class="ui-layout-north"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/services/ajax/uc50_ajaxcall/UC50_AjaxCallcmp_header" />
</div>
<div class="ui-layout-south"  > 
<fmt:message key="cUC50_AjaxCall___"/>
<span id="rd_jversion"  > 
<fmt:message key="cUC50_AjaxCall_BLU_AGE_DOCUMENTATION_204"/>
</span>
</div>
<div class="ui-layout-west"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/services/ajax/uc50_ajaxcall/UC50_AjaxCallcmp_treeview" />
</div>
</div>
</body>
</html>
