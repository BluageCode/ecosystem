--<ScriptOptions statementTerminator=";"/>

ALTER TABLE player203 DROP CONSTRAINT player203_pkey;

DROP TABLE player203;

CREATE TABLE player203 (
		id INT8 NOT NULL,
		version INT4 NOT NULL,
		first_name VARCHAR(1024),
		last_name VARCHAR(1024),
		value FLOAT4
	);

INSERT INTO player203 VALUES (1, 0, 'John', 'Smith', 20);	
	
ALTER TABLE player203 ADD CONSTRAINT player203_pkey PRIMARY KEY (id);

