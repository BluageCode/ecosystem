/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.entities.uc64_cascade;


import java.io.Serializable;
import java.util.List;

import com.bluage.documentation.business.standardfeatures.entities.uc64_cascade.bo.Team64BO;
import com.netfective.bluage.core.web.AbstractForm;
import com.netfective.bluage.core.web.IForm;

/**
* Class : UC64_UpdateTeamForm
*/
public class UC64_UpdateTeamForm extends AbstractForm implements Serializable, IForm {
	
	/** Constant : serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	//CONSTANT : allStates
	private static final String ALL_STATES = "allStates";
	//CONSTANT : teamToUpdate
	private static final String TEAM_TO_UPDATE = "teamToUpdate";
	/**
	 * 	Property: allStates 
	 */
	private List allStates;
	/**
	 * 	Property: teamToUpdate 
	 */
	private Team64BO teamToUpdate;
/**
	 * Default constructor : UC64_UpdateTeamForm
	 */
	public UC64_UpdateTeamForm() {
		super();
		// Initialize : allStates
		this.allStates = new java.util.ArrayList();
		// Initialize : teamToUpdate
		this.teamToUpdate = new Team64BO();
			this.setAlwaysCallPreControllers(true);
	}
	
	/**
	 * 	Getter : allStates 
	 * 	@return : Return the allStates instance.
	 */
	public List getAllStates(){
		return allStates; // For UC64_UpdateTeamForm
	}
	
	/**
	 * 	Setter : allStates 
	 *  @param allStatesinstance : The instance to set.
	 */
	public void setAllStates(final List allStatesinstance){
		this.allStates = allStatesinstance;// For UC64_UpdateTeamForm
	}
	/**
	 * 	Getter : teamToUpdate 
	 * 	@return : Return the teamToUpdate instance.
	 */
	public Team64BO getTeamToUpdate(){
		return teamToUpdate; // For UC64_UpdateTeamForm
	}
	
	/**
	 * 	Setter : teamToUpdate 
	 *  @param teamToUpdateinstance : The instance to set.
	 */
	public void setTeamToUpdate(final Team64BO teamToUpdateinstance){
		this.teamToUpdate = teamToUpdateinstance;// For UC64_UpdateTeamForm
	}
/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UC64_UpdateTeamForm [ "+
ALL_STATES +" = " + allStates +TEAM_TO_UPDATE +" = " + teamToUpdate + "]";
	}
	
	/**
	 * Method setInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public void setInstanceByInstanceName(final String instanceName, final Object instance) {
		// Set the instance AllStates.
		if(ALL_STATES.equals(instanceName)){// For UC64_UpdateTeamForm
			this.setAllStates((List)instance); // For UC64_UpdateTeamForm
		}
				// Set the instance TeamToUpdate.
		if(TEAM_TO_UPDATE.equals(instanceName)){// For UC64_UpdateTeamForm
			this.setTeamToUpdate((Team64BO)instance); // For UC64_UpdateTeamForm
		}
			}
	
	/**
	 * Method getInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public Object getInstanceByInstanceName(final String instanceName){
		// Temporary object for tmpUC64_UpdateTeamForm.
		Object tmpUC64_UpdateTeamForm = null;
		
			
		// Get the instance AllStates for UC64_UpdateTeamForm.
		if(ALL_STATES.equals(instanceName)){ // For UC64_UpdateTeamForm
			tmpUC64_UpdateTeamForm = this.getAllStates(); // For UC64_UpdateTeamForm
		}
			
		// Get the instance TeamToUpdate for UC64_UpdateTeamForm.
		if(TEAM_TO_UPDATE.equals(instanceName)){ // For UC64_UpdateTeamForm
			tmpUC64_UpdateTeamForm = this.getTeamToUpdate(); // For UC64_UpdateTeamForm
		}
		return tmpUC64_UpdateTeamForm;// For UC64_UpdateTeamForm
	}
	
	}
