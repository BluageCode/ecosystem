package uc59;


import org.junit.Test;
import test.common.AbstractBADocTestCase;

public class IT02uc59Data extends AbstractBADocTestCase {
	
	@Override
	public  String getName(){
		return "UC-59";
	}	


	@Test
	public void testIT02uc59Data() throws Exception {
		// On teste l'UC59 - Standard Features/Entities - UML to SQL Mapping
		
		
		selenium.open("");	
		selenium.click("link=Expand All");
		selenium.waitForPageToLoad("30000");
		selenium.click("link=UC59 - UML to SQL Mapping");
		
		verifyTrue(selenium.isElementPresent("//h2[contains(text(),\"Standard Features > Entities > UC59 > UML to SQL Mapping\")]"));
		// On teste les données.
		verifyEquals("10", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody//tr[td/span[contains(text(),\"Cole\")] and td/span[contains(text(),\"Elbert\")] and td/span[contains(text(),\"Elb\")] and td/span[contains(text(),\"04/07/1965\")] and td/span[contains(text(),\"Power Forward\")] and td/span[contains(text(),\"Power Forward\")] and td/span[contains(text(),\"3.95\")] and td//input[@checked]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody//tr[td/span[contains(text(),\"Dennis\")] and td/span[contains(text(),\"Abel\")] and td/span[contains(text(),\"Abel\")] and td/span[contains(text(),\"04/16/1980\")] and td/span[contains(text(),\"Small Forward\")] and td/span[contains(text(),\"Power Forward\")] and td/span[contains(text(),\"30.51\")] and td//input[@checked]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody//tr[td/span[contains(text(),\"Russell\")] and td/span[contains(text(),\"Earl\")] and td/span[contains(text(),\"Earl\")] and td/span[contains(text(),\"12/15/1981\")] and td/span[contains(text(),\"Power Forward\")] and td/span[contains(text(),\"Power Forward\")] and td/span[contains(text(),\"44.73\")] and td//input[not(@checked)]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody//tr[td/span[contains(text(),\"Ramos\")] and td/span[contains(text(),\"Howard\")] and td/span[contains(text(),\"Ho\")] and td/span[contains(text(),\"08/25/1970\")] and td/span[contains(text(),\"Shooting Guard\")] and td/span[contains(text(),\"Power Forward\")] and td/span[contains(text(),\"10.76\")] and td//input[not(@checked)]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody//tr[td/span[contains(text(),\"Meyer\")] and td/span[contains(text(),\"Allan\")] and td/span[contains(text(),\"Allan\")] and td/span[contains(text(),\"02/14/1969\")] and td/span[contains(text(),\"Point Guard\")] and td/span[contains(text(),\"Power Forward\")] and td/span[contains(text(),\"33.67\")] and td//input[not(@checked)]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody//tr[td/span[contains(text(),\"Barnett\")] and td/span[contains(text(),\"Keith\")] and td/span[contains(text(),\"Keith\")] and td/span[contains(text(),\"11/10/1976\")] and td/span[contains(text(),\"Power Forward\")] and td/span[contains(text(),\"Power Forward\")] and td/span[contains(text(),\"5.81\")] and td//input[not(@checked)]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody//tr[td/span[contains(text(),\"Nash\")] and td/span[contains(text(),\"Sylvester\")] and td/span[contains(text(),\"Sylv\")] and td/span[contains(text(),\"07/10/1965\")] and td/span[contains(text(),\"Power Forward\")] and td/span[contains(text(),\"Power Forward\")] and td/span[contains(text(),\"37.73\")] and td//input[@checked]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody//tr[td/span[contains(text(),\"Hodges\")] and td/span[contains(text(),\"Michael\")] and td/span[contains(text(),\"Mich\")] and td/span[contains(text(),\"04/01/1988\")] and td/span[contains(text(),\"Center\")] and td/span[contains(text(),\"Power Forward\")] and td/span[contains(text(),\"25.35\")] and td//input[not(@checked)]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody//tr[td/span[contains(text(),\"Grant\")] and td/span[contains(text(),\"Jerome\")] and td/span[contains(text(),\"Jems\")] and td/span[contains(text(),\"06/28/1982\")] and td/span[contains(text(),\"Small Forward\")] and td/span[contains(text(),\"Power Forward\")] and td/span[contains(text(),\"28.94\")] and td//input[@checked]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody//tr[td/span[contains(text(),\"Carpenter\")] and td/span[contains(text(),\"Woodrow\")] and td/span[contains(text(),\"Wood\")] and td/span[contains(text(),\"08/08/1987\")] and td/span[contains(text(),\"Shooting Guard\")] and td/span[contains(text(),\"Power Forward\")] and td/span[contains(text(),\"41.76\")] and td//input[@checked]]"));
		selenium.click("xpath=(//span/a)[1]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("10", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody//tr[td/span[contains(text(),\"Robbins\")] and td/span[contains(text(),\"Roderick\")] and td/span[contains(text(),\"Rod\")] and td/span[contains(text(),\"07/29/1975\")] and td/span[contains(text(),\"Center\")] and td/span[contains(text(),\"Shooting Guard\")] and td/span[contains(text(),\"12.04\")] and td//input[@checked]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody//tr[td/span[contains(text(),\"Conner\")] and td/span[contains(text(),\"Cecil\")] and td/span[contains(text(),\"Cecil\")] and td/span[contains(text(),\"06/29/1978\")] and td/span[contains(text(),\"Power Forward\")] and td/span[contains(text(),\"Shooting Guard\")] and td/span[contains(text(),\"39.71\")] and td//input[not(@checked)]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody//tr[td/span[contains(text(),\"Lambert\")] and td/span[contains(text(),\"Stephen\")] and td/span[contains(text(),\"Steph\")] and td/span[contains(text(),\"03/14/1977\")] and td/span[contains(text(),\"Point Guard\")] and td/span[contains(text(),\"Shooting Guard\")] and td/span[contains(text(),\"18.9\")] and td//input[not(@checked)]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody//tr[td/span[contains(text(),\"Chavez\")] and td/span[contains(text(),\"Christian\")] and td/span[contains(text(),\"Chricri\")] and td/span[contains(text(),\"05/17/1983\")] and td/span[contains(text(),\"Shooting Guard\")] and td/span[contains(text(),\"Shooting Guard\")] and td/span[contains(text(),\"37.88\")] and td//input[@checked]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody//tr[td/span[contains(text(),\"Pope\")] and td/span[contains(text(),\"Ruben\")] and td/span[contains(text(),\"Rub\")] and td/span[contains(text(),\"05/01/1985\")] and td/span[contains(text(),\"Shooting Guard\")] and td/span[contains(text(),\"Power Forward\")] and td/span[contains(text(),\"44.66\")] and td//input[@checked]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody//tr[td/span[contains(text(),\"Cross\")] and td/span[contains(text(),\"Lester\")] and td/span[contains(text(),\"Les\")] and td/span[contains(text(),\"02/21/1976\")] and td/span[contains(text(),\"Small Forward\")] and td/span[contains(text(),\"Power Forward\")] and td/span[contains(text(),\"30.98\")] and td//input[@checked]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody//tr[td/span[contains(text(),\"Tate\")] and td/span[contains(text(),\"Ivan\")] and td/span[contains(text(),\"Ivan\")] and td/span[contains(text(),\"02/08/1972\")] and td/span[contains(text(),\"Shooting Guard\")] and td/span[contains(text(),\"Power Forward\")] and td/span[contains(text(),\"48.49\")] and td//input[@checked]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody//tr[td/span[contains(text(),\"Frank\")] and td/span[contains(text(),\"Oliver\")] and td/span[contains(text(),\"Oliv\")] and td/span[contains(text(),\"10/31/1968\")] and td/span[contains(text(),\"Small Forward\")] and td/span[contains(text(),\"Power Forward\")] and td/span[contains(text(),\"0.11\")] and td//input[not(@checked)]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody//tr[td/span[contains(text(),\"Garner\")] and td/span[contains(text(),\"Nick\")] and td/span[contains(text(),\"Nick\")] and td/span[contains(text(),\"07/27/1992\")] and td/span[contains(text(),\"Power Forward\")] and td/span[contains(text(),\"Small Forward\")] and td/span[contains(text(),\"47.84\")] and td//input[@checked]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody//tr[td/span[contains(text(),\"Wood\")] and td/span[contains(text(),\"Tommy\")] and td/span[contains(text(),\"Tom\")] and td/span[contains(text(),\"01/20/1992\")] and td/span[contains(text(),\"Power Forward\")] and td/span[contains(text(),\"Small Forward\")] and td/span[contains(text(),\"13.93\")] and td//input[@checked]]"));
	}


}
