/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.gettingstarted.uc04_entitiesmodification ;


import org.apache.log4j.Logger;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;

import com.netfective.bluage.core.validator.AbstractValidator;
import com.netfective.bluage.core.validator.ValidatorsUtil;
/**
* Class :UC04_EntitiesModificationValidator
*/
public class UC04_EntitiesModificationValidator extends AbstractValidator{
	
	// LOGGER for the class UC04_EntitiesModificationValidator
	private static final Logger LOGGER = Logger.getLogger( UC04_EntitiesModificationValidator.class);
	
	
	/**
	* Operation validate for UC04_EntitiesModificationForm
	* @param obj : the current form (UC04_EntitiesModificationForm)
	* @param errors : The spring errors to return for the form UC04_EntitiesModificationForm
	*/
	public void validate(final Object obj,final Errors errors){
		LOGGER.info("Beginning method : validate"); // UC04_EntitiesModificationValidator
		UC04_EntitiesModificationForm cUC04_EntitiesModificationForm = (UC04_EntitiesModificationForm)obj; // UC04_EntitiesModificationValidator
							ValidationUtils.rejectIfEmpty(errors, "allPlayers["+getRequestIndex()+"].firstName", "", "First name is required.");

		if(errors.getFieldError("allPlayers["+getRequestIndex()+"].firstName")== null){
								if("playerToUpdate".equals(getTableElement())){
			if(LOGGER.isDebugEnabled()){
				LOGGER.debug("validation of the 'allPlayers["+getRequestIndex()+"].firstName' field with the LengthValidator validator");
			}
			
			if(ValidatorsUtil.lengthValidator(cUC04_EntitiesModificationForm.getAllPlayers().get(Integer.valueOf(getRequestIndex())).getFirstName(), 1, 30) == 0){
				errors.rejectValue("allPlayers["+getRequestIndex()+"].firstName", "", "First name should be less than 30 characters long.");
			} 
		}
				
		}
							ValidationUtils.rejectIfEmpty(errors, "allPlayers["+getRequestIndex()+"].lastName", "", "Last name is required.");

		if(errors.getFieldError("allPlayers["+getRequestIndex()+"].lastName")== null){
								if("playerToUpdate".equals(getTableElement())){
			if(LOGGER.isDebugEnabled()){
				LOGGER.debug("validation of the 'allPlayers["+getRequestIndex()+"].lastName' field with the LengthValidator validator");
			}
			
			if(ValidatorsUtil.lengthValidator(cUC04_EntitiesModificationForm.getAllPlayers().get(Integer.valueOf(getRequestIndex())).getLastName(), 1, 30) == 0){
				errors.rejectValue("allPlayers["+getRequestIndex()+"].lastName", "", "Last name should be less than 30 characters long.");
			} 
		}
				
		}
							ValidationUtils.rejectIfEmpty(errors, "allPlayers["+getRequestIndex()+"].dateOfBirth", "", "Date of birth is required.");

			LOGGER.info("Ending method : validate the form "+ cUC04_EntitiesModificationForm.getClass().getName()); // UC04_EntitiesModificationValidator
	}

	/**
	* Method to implements to use spring validators (UC04_EntitiesModificationForm)
	* @param aClass : Class for the form UC04_EntitiesModificationForm
	* @return boolean
	*/
	public boolean supports(final Class aClass){
		return (new UC04_EntitiesModificationForm()).getClass().equals(aClass); // UC04_EntitiesModificationValidator
	}
}
