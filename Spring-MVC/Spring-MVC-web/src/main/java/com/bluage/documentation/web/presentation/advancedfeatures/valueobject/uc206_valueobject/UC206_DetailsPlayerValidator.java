/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.advancedfeatures.valueobject.uc206_valueobject ;


import org.apache.log4j.Logger;
import org.springframework.validation.Errors;

import com.netfective.bluage.core.validator.AbstractValidator;
/**
* Class :UC206_DetailsPlayerValidator
*/
public class UC206_DetailsPlayerValidator extends AbstractValidator{
	
	// LOGGER for the class UC206_DetailsPlayerValidator
	private static final Logger LOGGER = Logger.getLogger( UC206_DetailsPlayerValidator.class);
	
	
	/**
	* Operation validate for UC206_DetailsPlayerForm
	* @param obj : the current form (UC206_DetailsPlayerForm)
	* @param errors : The spring errors to return for the form UC206_DetailsPlayerForm
	*/
	public void validate(final Object obj,final Errors errors){
		LOGGER.info("Beginning method : validate"); // UC206_DetailsPlayerValidator
		UC206_DetailsPlayerForm cUC206_DetailsPlayerForm = (UC206_DetailsPlayerForm)obj; // UC206_DetailsPlayerValidator
		LOGGER.info("Ending method : validate the form "+ cUC206_DetailsPlayerForm.getClass().getName()); // UC206_DetailsPlayerValidator
	}

	/**
	* Method to implements to use spring validators (UC206_DetailsPlayerForm)
	* @param aClass : Class for the form UC206_DetailsPlayerForm
	* @return boolean
	*/
	public boolean supports(final Class aClass){
		return (new UC206_DetailsPlayerForm()).getClass().equals(aClass); // UC206_DetailsPlayerValidator
	}
}
