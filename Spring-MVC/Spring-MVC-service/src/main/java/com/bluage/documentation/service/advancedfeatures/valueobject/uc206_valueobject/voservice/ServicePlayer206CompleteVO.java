/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.service.advancedfeatures.valueobject.uc206_valueobject.voservice;


import javax.inject.Inject;
import javax.inject.Named;

import com.bluage.documentation.business.advancedfeatures.valueobject.uc206_valueobject.bos.Player206BO;
import com.bluage.documentation.business.advancedfeatures.valueobject.uc206_valueobject.bos.Position206BO;
import com.bluage.documentation.business.advancedfeatures.valueobject.uc206_valueobject.vo.Player206VO;
import com.bluage.documentation.business.advancedfeatures.valueobject.uc206_valueobject.vo.Position206VO;
import com.bluage.documentation.service.AbstractService;
import com.netfective.bluage.core.exception.ApplicationException;


/**
 * Defines  application VO services for ServicePlayer206CompleteVO
 *
 */
@Named ("com.bluage.documentation.service.advancedfeatures.valueobject.uc206_valueobject.voservice.ServicePlayer206CompleteVO")
public class ServicePlayer206CompleteVO  extends AbstractService{
					@Inject
		@Named ("com.bluage.documentation.service.advancedfeatures.valueobject.uc206_valueobject.voservice.ServicePosition206ConvertBO2VO")
		private ServicePosition206ConvertBO2VO instanceServicePosition206ConvertBO2VO;
							

	/**
	 *  Default constructor 
	 */
	public ServicePlayer206CompleteVO() {
		super();
	}



			

	/**    
	 * VO operation.
	 * @param vo {@link Player206VO}
		 * @return {@link void}
	 */
	public void player206CompleteFieldAsString(Player206VO vo )
 throws ApplicationException {
privatePlayer206CompleteFieldAsString(vo);
	}



	/**    
	 * Complete field as String operation.
	 * @param vo {@link Player206VO}
		 * @return {@link void}
	 */
	private void privatePlayer206CompleteFieldAsString(Player206VO vo )
 throws ApplicationException{
		vo.setIdAsString(vo.idLongConvertToString(vo.getId()));
		vo.setDateOfBirthAsString(vo.dateOfBirthDateConvertToString(vo.getDateOfBirth()));
		vo.setEstimatedValueAsString(vo.estimatedValueFloatConvertToString(vo.getEstimatedValue()));
		vo.setWeightAsString(vo.weightIntegerConvertToString(vo.getWeight()));
		vo.setHeightAsString(vo.heightIntegerConvertToString(vo.getHeight()));
}

		
			
	
	/**    
	 * VO operation.
	 * @param vo {@link Player206VO}
		 * @param bo {@link Player206BO}
		 * @return {@link void}
	 */
	public void player206CompleteFks(Player206VO vo,Player206BO bo )
 throws ApplicationException {
privatePlayer206CompleteFks(vo,bo);
	}

	

		/**    
	 * complete relations operation .
	 * @param vo {@link Player206VO}
		 * @param bo {@link Player206BO}
		 * @return {@link void}
	 */
	private void privatePlayer206CompleteFks(Player206VO vo,Player206BO bo )
 throws ApplicationException {
				player206CompleteFkPositionVO(vo,bo);		
				}

		
			

	/**    
	 * VO operation.
	 * @param vo {@link Player206VO}
		 * @param bo {@link Player206BO}
		 * @return {@link void}
	 */
	public void player206CompleteFkPositionVO(Player206VO vo,Player206BO bo )
 throws ApplicationException {
privatePlayer206CompleteFkPositionVO(vo,bo);
	}


	
		/**    
	 * complete one relation String operation.
	 * @param vo {@link Player206VO}
		 * @param bo {@link Player206BO}
		 * @return {@link void}
	 */
	private void privatePlayer206CompleteFkPositionVO(Player206VO vo,Player206BO bo )
 throws ApplicationException {
Position206BO		relBO = bo.getPosition();
		Position206VO relVO = null;
		// ServicePosition206ConvertBO2VO
		if(relBO!=null){
			relVO = instanceServicePosition206ConvertBO2VO.position206Convert(relBO);
		}
			vo.setPositionVO(relVO);
	}

		
			
	
	/**    
	 * VO operation.
	 * @param vo {@link Player206VO}
		 * @param bo {@link Player206BO}
		 * @return {@link void}
	 */
	public void player206CompleteRelations(Player206VO vo,Player206BO bo )
 throws ApplicationException {
privatePlayer206CompleteRelations(vo,bo);
	}

	

		/**    
	 * complete relations operation .
	 * @param vo {@link Player206VO}
		 * @param bo {@link Player206BO}
		 * @return {@link void}
	 */
	private void privatePlayer206CompleteRelations(Player206VO vo,Player206BO bo )
 throws ApplicationException {
						}

		}
