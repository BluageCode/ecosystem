package uc47;

import org.junit.Test;
import test.common.AbstractBADocTestCase;

public class ITuc47 extends AbstractBADocTestCase {
	
	
	@Override
	public  String getName(){
		return "UC-47";
	}

	@Test
	public void testITuc47() throws Exception {
		// On teste l'UC47 - Standard Features/Application/Component - Contents
		selenium.open("");
		selenium.click("link=Expand All");
		selenium.waitForPageToLoad("30000");
		selenium.click("link=UC47 - Contents");
		assertTrue(selenium.isElementPresent("//h2[contains(text(),\"Standard Features > Application > Component > UC47 > Contents\")]"));
		// On v�rifie les donn�es de base de l'UC.
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//thead//tr[th[contains(text(),\"Name\")] and th[contains(text(),\"Date of Birth\")] and th[contains(text(),\"Weight (kg)\")] and th[contains(text(),\"Height (cm)\")] and th[contains(text(),\"Estimated Value (M$)\")] and th[contains(text(),\"Position\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Harry\")] and td//span[contains(text(),\"Davis\")] and td//span[contains(text(),\"04/14/1988\")] and td//span[contains(text(),\"237\")] and td//span[contains(text(),\"139\")] and td//span[contains(text(),\"9.69\")] and td//span[contains(text(),\"PF\")] and td//a[contains(text(),\"[edit]\")] and td//a[contains(text(),\"[details]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Loren\")] and td//span[contains(text(),\"Colon\")] and td//span[contains(text(),\"12/14/1971\")] and td//span[contains(text(),\"221\")] and td//span[contains(text(),\"132\")] and td//span[contains(text(),\"30.14\")] and td//span[contains(text(),\"SF\")] and td//a[contains(text(),\"[edit]\")] and td//a[contains(text(),\"[details]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Bob\")] and td//span[contains(text(),\"Clark\")] and td//span[contains(text(),\"09/04/1972\")] and td//span[contains(text(),\"224\")] and td//span[contains(text(),\"105\")] and td//span[contains(text(),\"24.02\")] and td//span[contains(text(),\"SG\")] and td//a[contains(text(),\"[edit]\")] and td//a[contains(text(),\"[details]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Loren\")] and td//span[contains(text(),\"Stokes\")] and td//span[contains(text(),\"07/17/1976\")] and td//span[contains(text(),\"186\")] and td//span[contains(text(),\"105\")] and td//span[contains(text(),\"30.97\")] and td//span[contains(text(),\"SG\")] and td//a[contains(text(),\"[edit]\")] and td//a[contains(text(),\"[details]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Shane\")] and td//span[contains(text(),\"Sandoval\")] and td//span[contains(text(),\"09/24/1975\")] and td//span[contains(text(),\"182\")] and td//span[contains(text(),\"131\")] and td//span[contains(text(),\"24.14\")] and td//span[contains(text(),\"PF\")] and td//a[contains(text(),\"[edit]\")] and td//a[contains(text(),\"[details]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Roosevelt\")] and td//span[contains(text(),\"Davis\")] and td//span[contains(text(),\"11/10/1983\")] and td//span[contains(text(),\"235\")] and td//span[contains(text(),\"114\")] and td//span[contains(text(),\"8.55\")] and td//span[contains(text(),\"C\")] and td//a[contains(text(),\"[edit]\")] and td//a[contains(text(),\"[details]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Alton\")] and td//span[contains(text(),\"Davis\")] and td//span[contains(text(),\"02/13/1970\")] and td//span[contains(text(),\"196\")] and td//span[contains(text(),\"120\")] and td//span[contains(text(),\"20.34\")] and td//span[contains(text(),\"C\")] and td//a[contains(text(),\"[edit]\")] and td//a[contains(text(),\"[details]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Marion\")] and td//span[contains(text(),\"Favis\")] and td//span[contains(text(),\"02/13/1970\")] and td//span[contains(text(),\"196\")] and td//span[contains(text(),\"120\")] and td//span[contains(text(),\"20.34\")] and td//span[contains(text(),\"C\")] and td//a[contains(text(),\"[edit]\")] and td//a[contains(text(),\"[details]\")]]"));
		assertTrue(selenium.isElementPresent("//*[contains(text(),'*details links allow you to see the teams in which selected player was from 01/01/1970')]"));
		// On v�rifie les d�tails pour Harry Davis :
		// Pour ce faire on indexe la ligne, et on affiche les details puis on edite.
		Number indexDavisH = selenium.getElementIndex("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Davis\")] and td/span[contains(text(),\"Harry\")]]");
		System.out.println("variable index vaut : " + indexDavisH);
		selenium.click("//table[contains(@id,'tab_players')]/tbody/tr[" + indexDavisH + " + 1]//td/a[contains(text(),\"[details]\")]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie l'affichage pour le joueur.
		assertTrue(selenium.isElementPresent("//h4[contains(text(),'Contents Repeater')]"));
		verifyEquals("2", selenium.getXpathCount("//span[contains(@id,'Team_name')]"));
		assertTrue(selenium.isElementPresent("//*[contains(text(),'Player')]"));
		assertTrue(selenium.isElementPresent("//b/span[contains(text(),'Davis') and contains(@id,'dyn_lastname')]"));
		assertTrue(selenium.isElementPresent("//b/span[contains(text(),'Harry') and contains(@id,'dyn_firstname')]"));
		assertTrue(selenium.isElementPresent("//span[contains(text(),\"Tigers\") and contains(@id,'Team_name')]"));
		assertTrue(selenium.isElementPresent("//span[contains(text(),\"Bears\") and contains(@id,'Team_name')]"));
		// On clique sur [edit]
		indexDavisH = selenium.getElementIndex("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Davis\")] and td/span[contains(text(),\"Harry\")]]");
		System.out.println("variable index vaut : " + indexDavisH);
		selenium.click("//table[contains(@id,'tab_players')]/tbody/tr[" + indexDavisH + " + 1]//td/a[contains(text(),\"[edit]\")]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie l'affichage  des details :
		// On v�rife l'affichage de l'UC.
		verifyTrue(selenium.isElementPresent("//h2[contains(text(),\"Standard Features > Application > Component > UC47 > Contents\")]"));
		assertTrue(selenium.isElementPresent("//label[contains(text(),'First Name')]"));
		assertTrue(selenium.isElementPresent("xpath=(//label[contains(text(),'(*)')])"));
		assertTrue(selenium.isElementPresent("//label[contains(text(),'Last Name')]"));
		assertTrue(selenium.isElementPresent("xpath=(//label[contains(text(),'(*)')])"));
		assertTrue(selenium.isElementPresent("//label[contains(text(),'Date of Birth (MM/dd/yyyy)')]"));
		assertTrue(selenium.isElementPresent("//label[contains(text(),'Estimated value')]"));
		assertTrue(selenium.isElementPresent("//label[contains(text(),'Weight')]"));
		assertTrue(selenium.isElementPresent("//label[contains(text(),'Height')]"));
		assertTrue(selenium.isElementPresent("//label[contains(text(),'Position')]"));
		assertTrue(selenium.isElementPresent("//label[contains(text(),'Rookie')]"));
		// On v�rife les positions:
		assertTrue(selenium.isElementPresent("//option[contains(text(),'Point Guard')]"));
		assertTrue(selenium.isElementPresent("//option[contains(text(),'Shooting Guard')]"));
		assertTrue(selenium.isElementPresent("//option[contains(text(),'Small Forward')]"));
		assertTrue(selenium.isElementPresent("//option[contains(text(),'Power Forward')]"));
		assertTrue(selenium.isElementPresent("//option[contains(text(),'Center')]"));
		assertTrue(selenium.isElementPresent("//h2[contains(text(),'Standard Features > Application > Component > UC47 > Contents')]"));
		assertTrue(selenium.isElementPresent("//h4[contains(text(),'Details')]"));
		assertTrue(selenium.isElementPresent("//*[contains(@id,'txt_firstname') and @value='Harry']"));
		assertTrue(selenium.isElementPresent("//*[contains(@id,'txt_lastname') and @value='Davis']"));
		assertTrue(selenium.isElementPresent("//*[contains(@id,'txt_dateofbirth') and @value='04/14/1988']"));
		assertTrue(selenium.isElementPresent("//*[contains(@id,'txt_estimatedvalue') and @value='9.69']"));
		assertTrue(selenium.isElementPresent("//*[contains(@id,'txt_weight') and @value='139']"));
		assertTrue(selenium.isElementPresent("//*[contains(@id,'txt_height') and @value='237']"));
		assertTrue(selenium.isElementPresent("//option[@value='PF' and @selected='selected']"));
		assertTrue(selenium.isElementPresent("//*//td//input[not(@checked)]"));
		assertTrue(selenium.isElementPresent("//a[contains(text(),'[Update player]')]"));
		// On v�rifie l'affichage add teams :
		assertTrue(selenium.isElementPresent("//select[contains(@id,'slt_team')]/option[contains(text(),'Chargers')]"));
		assertTrue(selenium.isElementPresent("//select[contains(@id,'slt_team')]/option[contains(text(),'Hawks')]"));
		assertTrue(selenium.isElementPresent("//select[contains(@id,'slt_team')]/option[contains(text(),'Braves')]"));
		assertTrue(selenium.isElementPresent("//a[contains(text(),'[Add Team]')]"));
		// On teste les messages d'erreur. 
		// Last & First name, height, weight, Date of birth. On entre des valeurs invalides dans ces champs.
		selenium.type("//*[contains(@id,'txt_firstname')]", "");
		selenium.type("//*[contains(@id,'txt_lastname')]", "");
		selenium.type("//*[contains(@id,'txt_dateofbirth')]", "14/04/1988");
		selenium.type("//*[contains(@id,'txt_weight')]", "142");
		selenium.type("//*[contains(@id,'txt_height')]", "241");
		selenium.click("//a[contains(text(),'[Update player]')]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//span[contains(@id,'error_txt_firstname')and contains(text(),'First name is required.')]|//span[contains(@id,'playerToUpdate.firstName.errors')and contains(text(),'First name is required.')]"));
		assertTrue(selenium.isElementPresent("//span[contains(@id,'error_txt_lastname') and contains(text(),'Last name is required.')]|//span[contains(@id,'playerToUpdate.lastName.errors') and contains(text(),'Last name is required.')]"));
		assertTrue(selenium.isElementPresent("//span[contains(@id,'error_txt_dateofbirth') and contains(text(),'Invalid format.')]|//span[contains(@id,'playerToUpdate.dateOfBirth.errors') and contains(text(),'Invalid format.')]"));
		assertTrue(selenium.isElementPresent("//span[contains(@id,'error_txt_height') and contains(text(),'Player height should be between 150 and 240 cms.')]|//span[contains(@id,'playerToUpdate.height.errors') and contains(text(),'Player height should be between 150 and 240 cms.')]"));
		assertTrue(selenium.isElementPresent("//span[contains(@id,'error_txt_weight') and contains(text(),'Player weight should be between 60 and 140 kgs.')]|//span[contains(@id,'playerToUpdate.weight.errors') and contains(text(),'Player weight should be between 60 and 140 kgs.')]"));
		// On retourne sur la page de garde de l'UC :
		selenium.click("//a[contains(text(),'[back]')]");
		selenium.waitForPageToLoad("30000");
		// Pour ce faire on indexe la ligne, et on affiche les details puis on edite.
		indexDavisH = selenium.getElementIndex("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Davis\")] and td/span[contains(text(),\"Harry\")]]");
		System.out.println("variable index vaut : " + indexDavisH);
		selenium.click("//table[contains(@id,'tab_players')]/tbody/tr[" + indexDavisH + " + 1]//td/a[contains(text(),\"[edit]\")]");
		selenium.waitForPageToLoad("30000");
		// On modifie les informations de ce joueurs.
		selenium.type("//*[contains(@id,'txt_firstname')]", "Jean");
		selenium.type("//*[contains(@id,'txt_lastname')]", "Pierre");
		selenium.type("//*[contains(@id,'txt_dateofbirth')]", "02/02/1984");
		selenium.type("//*[contains(@id,'txt_estimatedvalue')]", "10.42");
		selenium.type("//*[contains(@id,'txt_weight')]", "136");
		selenium.type("//*[contains(@id,'txt_height')]", "195");
		selenium.select("//select[contains(@id,'slt_position')]", "label=Center");
		// On valide les modifications
		selenium.click("//a[contains(text(),'[Update player]')]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie la pr�sence des teams non attribu�es dans le select :
		assertTrue(selenium.isElementPresent("//select[contains(@id,'slt_team')]/option[contains(text(),'Chargers')]"));
		assertTrue(selenium.isElementPresent("//select[contains(@id,'slt_team')]/option[contains(text(),'Hawks')]"));
		assertTrue(selenium.isElementPresent("//select[contains(@id,'slt_team')]/option[contains(text(),'Braves')]"));
		selenium.select("//select[contains(@id,'slt_team')]", "label=Chargers");
		Thread.sleep(1000);
		selenium.click("//a[contains(text(),'[Add Team]')]");
		selenium.waitForPageToLoad("30000");
		Thread.sleep(1000);
		// On v�rifie que les deux autres Teams sont encore affich�es
		assertTrue(selenium.isElementPresent("//select[contains(@id,'slt_team')]/option[contains(text(),'Hawks')]"));
		assertTrue(selenium.isElementPresent("//select[contains(@id,'slt_team')]/option[contains(text(),'Braves')]"));
		selenium.select("//select[contains(@id,'slt_team')]", "label=Hawks");
		selenium.click("//a[contains(text(),'[Add Team]')]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//select[contains(@id,'slt_team')]/option[contains(text(),'Braves')]"));
		selenium.select("//select[contains(@id,'slt_team')]", "label=Braves");
		selenium.click("//a[contains(text(),'[Add Team]')]");
		selenium.waitForPageToLoad("30000");
		assertFalse(selenium.isElementPresent("//select[contains(@id,'slt_team')]/option[contains(text(),'Chargers')]"));
		assertFalse(selenium.isElementPresent("//select[contains(@id,'slt_team')]/option[contains(text(),'Hawks')]"));
		assertFalse(selenium.isElementPresent("//select[contains(@id,'slt_team')]/option[contains(text(),'Braves')]"));
		// On retourne sur la page du tableau teams
		selenium.click("//a[contains(text(),'[back]')]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie les donn�es de base de l'UC.
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Jean\")] and td//span[contains(text(),\"Pierre\")] and td//span[contains(text(),\"02/02/1984\")] and td//span[contains(text(),\"136\")] and td//span[contains(text(),\"195\")] and td//span[contains(text(),\"10.42\")] and td//span[contains(text(),\"C\")] and td//a[contains(text(),\"[edit]\")] and td//a[contains(text(),\"[details]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Loren\")] and td//span[contains(text(),\"Colon\")] and td//span[contains(text(),\"12/14/1971\")] and td//span[contains(text(),\"221\")] and td//span[contains(text(),\"132\")] and td//span[contains(text(),\"30.14\")] and td//span[contains(text(),\"SF\")] and td//a[contains(text(),\"[edit]\")] and td//a[contains(text(),\"[details]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Bob\")] and td//span[contains(text(),\"Clark\")] and td//span[contains(text(),\"09/04/1972\")] and td//span[contains(text(),\"224\")] and td//span[contains(text(),\"105\")] and td//span[contains(text(),\"24.02\")] and td//span[contains(text(),\"SG\")] and td//a[contains(text(),\"[edit]\")] and td//a[contains(text(),\"[details]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Loren\")] and td//span[contains(text(),\"Stokes\")] and td//span[contains(text(),\"07/17/1976\")] and td//span[contains(text(),\"186\")] and td//span[contains(text(),\"105\")] and td//span[contains(text(),\"30.97\")] and td//span[contains(text(),\"SG\")] and td//a[contains(text(),\"[edit]\")] and td//a[contains(text(),\"[details]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Shane\")] and td//span[contains(text(),\"Sandoval\")] and td//span[contains(text(),\"09/24/1975\")] and td//span[contains(text(),\"182\")] and td//span[contains(text(),\"131\")] and td//span[contains(text(),\"24.14\")] and td//span[contains(text(),\"PF\")] and td//a[contains(text(),\"[edit]\")] and td//a[contains(text(),\"[details]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Roosevelt\")] and td//span[contains(text(),\"Davis\")] and td//span[contains(text(),\"11/10/1983\")] and td//span[contains(text(),\"235\")] and td//span[contains(text(),\"114\")] and td//span[contains(text(),\"8.55\")] and td//span[contains(text(),\"C\")] and td//a[contains(text(),\"[edit]\")] and td//a[contains(text(),\"[details]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Alton\")] and td//span[contains(text(),\"Davis\")] and td//span[contains(text(),\"02/13/1970\")] and td//span[contains(text(),\"196\")] and td//span[contains(text(),\"120\")] and td//span[contains(text(),\"20.34\")] and td//span[contains(text(),\"C\")] and td//a[contains(text(),\"[edit]\")] and td//a[contains(text(),\"[details]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Marion\")] and td//span[contains(text(),\"Favis\")] and td//span[contains(text(),\"02/13/1970\")] and td//span[contains(text(),\"196\")] and td//span[contains(text(),\"120\")] and td//span[contains(text(),\"20.34\")] and td//span[contains(text(),\"C\")] and td//a[contains(text(),\"[edit]\")] and td//a[contains(text(),\"[details]\")]]"));
		Number indexJP = selenium.getElementIndex("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Jean\")] and td/span[contains(text(),\"Pierre\")]]");
		System.out.println("variable index vaut : " + indexJP);
		selenium.click("//table[contains(@id,'tab_players')]/tbody/tr[" + indexJP + " + 1]//td/a[contains(text(),\"[details]\")]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie l'affichage pour le joueur modifi� Jean Pierre
		assertTrue(selenium.isElementPresent("//h4[contains(text(),'Contents Repeater')]"));
		verifyEquals("5", selenium.getXpathCount("//span[contains(@id,'Team_name')]"));
		assertTrue(selenium.isElementPresent("//*[contains(text(),'Player')]"));
		assertTrue(selenium.isElementPresent("//b/span[contains(text(),'Pierre') and contains(@id,'dyn_lastname')]"));
		assertTrue(selenium.isElementPresent("//b/span[contains(text(),'Jean') and contains(@id,'dyn_firstname')]"));
		assertTrue(selenium.isElementPresent("//span[contains(text(),\"Tigers\") and contains(@id,'Team_name')]"));
		assertTrue(selenium.isElementPresent("//span[contains(text(),\"Bears\") and contains(@id,'Team_name')]"));
		assertTrue(selenium.isElementPresent("//span[contains(text(),\"Chargers\") and contains(@id,'Team_name')]"));
		assertTrue(selenium.isElementPresent("//span[contains(text(),\"Hawks\") and contains(@id,'Team_name')]"));
		assertTrue(selenium.isElementPresent("//span[contains(text(),\"Braves\") and contains(@id,'Team_name')]"));
	}

	
}
