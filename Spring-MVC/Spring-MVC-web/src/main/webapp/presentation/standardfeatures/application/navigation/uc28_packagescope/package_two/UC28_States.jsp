<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file="/include.jsp"  %>
<html >
<head >
<title  > 
<fmt:message key="cUC28_States_BLU_AGE_DOCUMENTATION"/>
</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"  > 
</meta>
<link href="<c:url value= "/presentation/_style/_style.css"/>" rel="stylesheet" type="text/css"  /> 
<link href="<c:url value= "/presentation/_include/jquery/rd_lib.css"/>" rel="stylesheet" type="text/css"  /> 
<script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/jquery.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/rd_setup.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/rd_lib.js"/>" space="preserve"  ></script><link rel="shortcut icon" href="<c:url value= "/presentation/_style/favicon.ico"/>"  /> 

</head>

<body class=" " > 
<div id="rd_ghost"  > 
<small  > 
<fmt:message key="cUC28_States___CSS_DEACTIVATED"/>
</small>
</div>
<div id="rd_jghost"  > 
<small  > 
<fmt:message key="cUC28_States___JAVASCRIPT_DEACTIVATED"/>
</small>
</div>
<div id="shape"  > 
<div class="ui-layout-center"  > 
<div class="east-north"  > 
	<form:form id="uC28_States" commandName="uC28_StatesForm"  enctype="application/x-www-form-urlencoded" method="post">
<h2 class="rd_first"  > 
<fmt:message key="cUC28_States_STANDARD_FEATURES__APPLICATIO"/>
</h2>
<br clear="none"  /> 
<br clear="none"  /> 
<table class="rd_tableform2"  >
<tr  > 
<td rowspan="1" colspan="1"  >
<b  > 
<a id="lnk_teams"onclick="javascript:btn_click_cExecuteUC28_States(this.href);return false;"				href="<c:url value="/presentation/standardfeatures/application/navigation/uc28_packagescope/package_two/UC28_States/lnk_teams.html"/>"
							 shape="rect"  >									 
<fmt:message key="cUC28_States_TEAMS"/>
</a>
</b>
</td> 
<td rowspan="1" colspan="1"  >
<b  > 
<a id="lnk_players"onclick="javascript:btn_click_cExecuteUC28_States(this.href);return false;"				href="<c:url value="/presentation/standardfeatures/application/navigation/uc28_packagescope/package_two/UC28_States/lnk_players.html"/>"
							 shape="rect"  >									 
<fmt:message key="cUC28_States_PLAYERS"/>
</a>
</b>
</td> 
<td rowspan="1" colspan="1"  >
<b  > 
<a id="lnk_states"onclick="javascript:btn_click_cExecuteUC28_States(this.href);return false;"				href="<c:url value="//presentation/standardfeatures/application/navigation/uc28_packagescope/package_two/UC28_Onglets_2/lnk_states.html"/>"
							 shape="rect"  >									 
<u  > 
<fmt:message key="cUC28_States_STATES"/>
</u>
</a>
</b>
</td> 
<td rowspan="1" colspan="1"  >
<b  > 
<a id="lnk_stadiums"onclick="javascript:btn_click_cExecuteUC28_States(this.href);return false;"				href="<c:url value="//presentation/standardfeatures/application/navigation/uc28_packagescope/package_two/UC28_Onglets_2/lnk_stadiums.html"/>"
							 shape="rect"  >									 
<fmt:message key="cUC28_States_STADIUMS"/>
</a>
</b>
</td> 
</tr>
</table> 
<display:table uid="tab_states"  sort="list" requestURI="" name="sessionScope.uC28_StatesForm.allStates" class="rd_tabledata"  >
	<c:set   var="rownum">
		<%=(tab_states !=	null) ? new Integer(tab_states_rowNum.intValue() - 1) : new Integer(0) %>
	</c:set>
	<c:set   var="idRow">
		<%=(tab_states != null ) ? tab_states_rowNum.intValue() - 1 : -1 %>
	</c:set>
	<c:set   var="idTab">
		<%=(tab_states != null ) ? "tab_states" : null %>
	</c:set>
	<c:set var="tableHashId">
		<%= new org.displaytag.util.ParamEncoder("tab_states").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE) %>
	</c:set>
	<c:set var="tableHashId2">
		<%=request.getParameter(new org.displaytag.util.ParamEncoder("tab_states").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE)) %>
	</c:set>	
	
<display:column format=""  titleKey="cUC28_States_CODE">
		<span id="dyn_code">
	<c:out value="${tab_states.code}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="cUC28_States_NAME">
		<span id="dyn_name">
	<c:out value="${tab_states.name}" />
		</span>
	</display:column>
</display:table>
	</form:form>
	<script language="javascript">
		function btn_click_cExecuteUC28_States(url) {
			UC28_Statesvar = document.getElementById('uC28_States');
			UC28_Statesvar.setAttribute('action', url);
			UC28_Statesvar.submit();
		}
		function btn_click_cSetActionUC28_States(url) {
			UC28_Statesvar = document.getElementById('uC28_States');
			UC28_Statesvar.setAttribute('action', url);
		}
	</script>
</div>
<div class="east-center"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/application/navigation/uc28_packagescope/package_two/UC28_Statescmp_tabs" />
</div>
</div>
<div class="ui-layout-north"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/application/navigation/uc28_packagescope/package_two/UC28_Statescmp_header" />
</div>
<div class="ui-layout-south"  > 
<fmt:message key="cUC28_States___"/>
<span id="rd_jversion"  > 
<fmt:message key="cUC28_States_BLU_AGE_DOCUMENTATION_118"/>
</span>
</div>
<div class="ui-layout-west"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/application/navigation/uc28_packagescope/package_two/UC28_Statescmp_treeview" />
</div>
</div>
</body>
</html>
