/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.advancedfeatures.advancedtagsuse.uc205_popup ;

// Import declaration.
// Java imports.
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.bluage.documentation.service.advancedfeatures.advancedtagsuse.uc205_popup.ServicePlayer205;
import com.netfective.bluage.core.exception.ApplicationException;
import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;
import com.netfective.bluage.core.web.IForm;

/**
 * Class : popupController
 */
@Controller
@Scope("prototype")
@SessionAttributes("popupForm")
@RequestMapping(value= "/presentation/advancedfeatures/advancedtagsuse/uc205_popup")
public class popupController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(popupController.class);
	
	//Current form name
	private static final String POPUP_FORM = "popupForm";

				//CONSTANT: allPlayers
	private static final String ALL_PLAYERS = "allPlayers";
	
	/**
	 * Property:customDateEditorspopupController
	 */
	final private Map<String,CustomDateEditor> customDateEditorspopupController = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:popupValidator
	 */
	final private popupValidator popupValidator = new popupValidator();
	
	/**
	 * Service declaration : servicePlayer205.
	 */
	@Autowired
	private ServicePlayer205 servicePlayer205;
	
	
	// Initialise all the instances for popupController
						// Declare the instance allPlayers
		private List allPlayers;
			/**
	 * Operation : btn_back
 	 * @param model : 
 	 * @param popup : The form
 	 * @return
	 */
	@RequestMapping(value = "/popup/btn_back.html",method = RequestMethod.POST)
	public String btn_back(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("popupForm") popupForm  popupForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : btn_back"); 
		infoLogger(popupForm); 
		popupForm.setAlwaysCallPreControllers(false); 
		// initialization for btn_back
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state btn_back if the validation fail, you will be forward to the screen : "/presentation/advancedfeatures/advancedtagsuse/uc205_popup/popup"
		if(errorsMessages(request,response,popupForm,bindingResult,"", popupValidator, customDateEditorspopupController)){ 
			return "/presentation/advancedfeatures/advancedtagsuse/uc205_popup/popup"; 
		}

	 callBack(request,popupForm );
											// Init the destination form com.bluage.documentation.web.presentation.advancedfeatures.advancedtagsuse.uc205_popup.UC205_HomeForm or not from btn_back
			final UC205_HomeForm uC205_HomeForm =  new UC205_HomeForm();
			// Populate the destination form with all the instances returned from btn_back.
			final IForm uC205_HomeForm2 = populateDestinationForm(request.getSession(), uC205_HomeForm); 
					
			// Add the form to the model.
			model.addAttribute("uC205_HomeForm", uC205_HomeForm2); 
			
			request.getSession().setAttribute("uC205_HomeForm", uC205_HomeForm2);
			
			// "OK" CASE => destination screen path from btn_back
			LOGGER.info("Go to the screen 'UC205_Home'.");
			// Redirect (PRG) from btn_back
			destinationPath =  "redirect:/presentation/advancedfeatures/advancedtagsuse/uc205_popup/UC205_Home.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : btn_back 
	}
	
		/**
	 * Operation : lnk_display
 	 * @param model : 
 	 * @param popup : The form
 	 * @return
	 */
	@RequestMapping(value = "/popup/lnk_display.html",method = RequestMethod.POST)
	public String lnk_display(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("popupForm") popupForm  popupForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : lnk_display"); 
		infoLogger(popupForm); 
		popupForm.setAlwaysCallPreControllers(false); 
		// initialization for lnk_display
		init();
		
		String destinationPath = null; 
		

	 callFindAllPlayers(request,popupForm );
						// Init the destination form com.bluage.documentation.web.presentation.advancedfeatures.advancedtagsuse.uc205_popup.popupForm or not from lnk_display
			// Populate the destination form with all the instances returned from lnk_display.
			final IForm popupForm2 = populateDestinationForm(request.getSession(), popupForm); 
					
			// Add the form to the model.
			model.addAttribute("popupForm", popupForm2); 
			
			request.getSession().setAttribute("popupForm", popupForm2);
			
			// "OK" CASE => destination screen path from lnk_display
			LOGGER.info("Go to the screen 'popup'.");
			// Redirect (PRG) from lnk_display
			destinationPath =  "redirect:/presentation/advancedfeatures/advancedtagsuse/uc205_popup/popup.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_display 
	}
	
	
	/**
	* This method initialise the form : popupForm 
	* @return popupForm
	*/
	@ModelAttribute("popupFormInit")
	public void initpopupForm(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, POPUP_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method popupForm."); //for lnk_display 
		}
		popupForm popupForm;
	
		if(request.getSession().getAttribute(POPUP_FORM) != null){
			popupForm = (popupForm)request.getSession().getAttribute(POPUP_FORM);
		} else {
			popupForm = new popupForm();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form popupForm.");
		}
		popupForm = (popupForm)populateDestinationForm(request.getSession(), popupForm);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  popupForm.");
		}
		model.addAttribute(POPUP_FORM, popupForm);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param popupForm : The sceen form.
	 */
	@RequestMapping(value = "/popup.html" ,method = RequestMethod.GET)
	public void preparepopup(final HttpServletRequest request,final  Model model,final  @ModelAttribute("popupFormInit") popupForm popupForm){
		
		popupForm currentpopupForm = popupForm;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(POPUP_FORM) == null){
			if(currentpopupForm!=null){
				request.getSession().setAttribute(POPUP_FORM, currentpopupForm);
			}else {
				currentpopupForm = new popupForm();
				request.getSession().setAttribute(POPUP_FORM, currentpopupForm);	
			}
		} else {
			currentpopupForm = (popupForm) request.getSession().getAttribute(POPUP_FORM);
		}

					currentpopupForm = (popupForm)populateDestinationForm(request.getSession(), currentpopupForm);
		// Call all the Precontroller.
	currentpopupForm.setAlwaysCallPreControllers(true);
		model.addAttribute(POPUP_FORM, currentpopupForm);
		request.getSession().setAttribute(POPUP_FORM, currentpopupForm);
		
	}
	
				/**
	 * method callBack
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callBack(HttpServletRequest request,IForm form) {
		allPlayers = (List)get(request.getSession(),form, "allPlayers");  
										 
					try {
				// executing Back in lnk_display
				allPlayers = 	servicePlayer205.player205FindAll(
	);  
 
				put(request.getSession(), ALL_PLAYERS,allPlayers);
								// processing variables Back in lnk_display

			} catch (ApplicationException e) { 
				// error handling for operation player205FindAll called Back
				errorLogger("An error occured during the execution of the operation : player205FindAll",e); 
		
			}
	}
		/**
	 * method callFindAllPlayers
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callFindAllPlayers(HttpServletRequest request,IForm form) {
		allPlayers = (List)get(request.getSession(),form, "allPlayers");  
										 
					try {
				// executing FindAllPlayers in lnk_display
				allPlayers = 	servicePlayer205.player205FindAll(
	);  
 
				put(request.getSession(), ALL_PLAYERS,allPlayers);
								// processing variables FindAllPlayers in lnk_display

			} catch (ApplicationException e) { 
				// error handling for operation player205FindAll called FindAllPlayers
				errorLogger("An error occured during the execution of the operation : player205FindAll",e); 
		
			}
	}
		
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("popupController [ ");
						strBToS.append(ALL_PLAYERS);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(allPlayers);
			strBToS.append(END_TO_STRING_DELIMITER);
		// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(popupValidator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorspopupController!=null); 
		return strBToS.toString();
	}
}
