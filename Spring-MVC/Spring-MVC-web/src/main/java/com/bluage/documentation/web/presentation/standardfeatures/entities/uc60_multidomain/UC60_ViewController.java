/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.entities.uc60_multidomain ;

// Import declaration.
// Java imports.
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.bluage.documentation.business.standardfeatures.entities.uc60_multidomain.teams.bos.Team60BO;
import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;

/**
 * Class : UC60_ViewController
 */
@Controller
@Scope("prototype")
@SessionAttributes("uC60_ViewForm")
@RequestMapping(value= "/presentation/standardfeatures/entities/uc60_multidomain")
public class UC60_ViewController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC60_ViewController.class);
	
	//Current form name
	private static final String U_C60__VIEW_FORM = "uC60_ViewForm";

	//CONSTANT: viewPlayers
	private static final String VIEW_PLAYERS = "viewPlayers";
		//CONSTANT: listplayers table or repeater.
	private static final String LISTPLAYERS = "listplayers";
				
	/**
	 * Property:customDateEditorsUC60_ViewController
	 */
	final private Map<String,CustomDateEditor> customDateEditorsUC60_ViewController = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:uC60_ViewValidator
	 */
	final private UC60_ViewValidator uC60_ViewValidator = new UC60_ViewValidator();
	
	
	// Initialise all the instances for UC60_ViewController
		// Initialize the instance viewPlayers of type com.bluage.documentation.business.standardfeatures.entities.uc60_multidomain.teams.bos.Team60BO with the value : null for UC60_ViewController
		private Team60BO viewPlayers;
			// Initialize the instance listplayers for the state lnk_view
		private List listplayers; // Initialize the instance listplayers for UC60_ViewController
				
	/**
	* This method initialise the form : UC60_ViewForm 
	* @return UC60_ViewForm
	*/
	@ModelAttribute("UC60_ViewFormInit")
	public void initUC60_ViewForm(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, U_C60__VIEW_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method UC60_ViewForm."); //for lnk_view 
		}
		UC60_ViewForm uC60_ViewForm;
	
		if(request.getSession().getAttribute(U_C60__VIEW_FORM) != null){
			uC60_ViewForm = (UC60_ViewForm)request.getSession().getAttribute(U_C60__VIEW_FORM);
		} else {
			uC60_ViewForm = new UC60_ViewForm();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form UC60_ViewForm.");
		}
		uC60_ViewForm = (UC60_ViewForm)populateDestinationForm(request.getSession(), uC60_ViewForm);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  UC60_ViewForm.");
		}
		model.addAttribute(U_C60__VIEW_FORM, uC60_ViewForm);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param UC60_ViewForm : The sceen form.
	 */
	@RequestMapping(value = "/UC60_View.html" ,method = RequestMethod.GET)
	public void prepareUC60_View(final HttpServletRequest request,final  Model model,final  @ModelAttribute("UC60_ViewFormInit") UC60_ViewForm uC60_ViewForm){
		
		UC60_ViewForm currentUC60_ViewForm = uC60_ViewForm;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(U_C60__VIEW_FORM) == null){
			if(currentUC60_ViewForm!=null){
				request.getSession().setAttribute(U_C60__VIEW_FORM, currentUC60_ViewForm);
			}else {
				currentUC60_ViewForm = new UC60_ViewForm();
				request.getSession().setAttribute(U_C60__VIEW_FORM, currentUC60_ViewForm);	
			}
		} else {
			currentUC60_ViewForm = (UC60_ViewForm) request.getSession().getAttribute(U_C60__VIEW_FORM);
		}

				currentUC60_ViewForm = (UC60_ViewForm)populateDestinationForm(request.getSession(), currentUC60_ViewForm);
		// Call all the Precontroller.
	currentUC60_ViewForm.setAlwaysCallPreControllers(true);
		model.addAttribute(U_C60__VIEW_FORM, currentUC60_ViewForm);
		request.getSession().setAttribute(U_C60__VIEW_FORM, currentUC60_ViewForm);
		
	}
	
			
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("UC60_ViewController [ ");
			strBToS.append(VIEW_PLAYERS);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(viewPlayers);
			strBToS.append(END_TO_STRING_DELIMITER);
				strBToS.append(LISTPLAYERS);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(listplayers);
			strBToS.append(END_TO_STRING_DELIMITER);
					// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(uC60_ViewValidator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsUC60_ViewController!=null); 
		return strBToS.toString();
	}
}
