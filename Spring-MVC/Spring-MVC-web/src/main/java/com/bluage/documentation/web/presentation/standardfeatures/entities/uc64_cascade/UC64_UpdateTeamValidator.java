/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.entities.uc64_cascade ;


import org.apache.log4j.Logger;
import org.springframework.validation.Errors;

import com.netfective.bluage.core.validator.AbstractValidator;
/**
* Class :UC64_UpdateTeamValidator
*/
public class UC64_UpdateTeamValidator extends AbstractValidator{
	
	// LOGGER for the class UC64_UpdateTeamValidator
	private static final Logger LOGGER = Logger.getLogger( UC64_UpdateTeamValidator.class);
	
	
	/**
	* Operation validate for UC64_UpdateTeamForm
	* @param obj : the current form (UC64_UpdateTeamForm)
	* @param errors : The spring errors to return for the form UC64_UpdateTeamForm
	*/
	public void validate(final Object obj,final Errors errors){
		LOGGER.info("Beginning method : validate"); // UC64_UpdateTeamValidator
		UC64_UpdateTeamForm cUC64_UpdateTeamForm = (UC64_UpdateTeamForm)obj; // UC64_UpdateTeamValidator
		LOGGER.info("Ending method : validate the form "+ cUC64_UpdateTeamForm.getClass().getName()); // UC64_UpdateTeamValidator
	}

	/**
	* Method to implements to use spring validators (UC64_UpdateTeamForm)
	* @param aClass : Class for the form UC64_UpdateTeamForm
	* @return boolean
	*/
	public boolean supports(final Class aClass){
		return (new UC64_UpdateTeamForm()).getClass().equals(aClass); // UC64_UpdateTeamValidator
	}
}
