package uc03;

import org.junit.Test;
import test.common.AbstractBADocTestCase;

public class ITuc03Creation extends AbstractBADocTestCase {
	
	@Override
	public  String getName(){
		return "UC-03";
	}
	
	@Test
	public void testITuc03Creation() throws Exception {
		// On teste l'UC03 - Getting started - Form Validation
		// On teste l'UC en créant un joueur.
		selenium.open("");
		selenium.waitForPageToLoad("30000");
		selenium.click("link=Expand All");
		selenium.click("link=UC03 - Form Validation");
		verifyTrue(selenium.isElementPresent("//h2[contains(text(),\"Getting started > UC03 > Form Validation\")]"));
		// On entre des suites de caractères valides pour tous les champs.
		selenium.type("//*[contains(@id,'txt_firstname')]", "Jean");
		selenium.type("//*[contains(@id,'txt_lastname')]", "Pierre");
		selenium.type("//*[contains(@id,'txt_dateofbirth')]", "12/14/1980");
		selenium.type("//*[contains(@id,'txt_estimatedvalue')]", "25.5");
		selenium.type("//*[contains(@id,'txt_weight')]", "100");
		selenium.type("//*[contains(@id,'txt_height')]", "175");
		selenium.select("//*[contains(@id,'slt_position')]", "label=Small Forward");
		selenium.click("//*[contains(@id,'chk_rookie')]");
		assertTrue(selenium.isElementPresent("//*[contains(text(),'[Create player]')]"));
		selenium.click("//*[contains(@id,'lnk_create')]");
		selenium.waitForPageToLoad("30000");
		// On vérifie que le joueur a bien été crée.
		assertTrue(selenium.isElementPresent("//p[contains(text(),\"\nThe player has been created\n\")]"));
		// On teste le lien [back]
		assertTrue(selenium.isElementPresent("//*[contains(text(),'[Create another player]')]"));
		selenium.click("//*[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
		// On vérifie que l'on est sur la bonne page.
		assertEquals("Getting started > UC03 > Form Validation", selenium.getText("//h2"));
	}

	
}
