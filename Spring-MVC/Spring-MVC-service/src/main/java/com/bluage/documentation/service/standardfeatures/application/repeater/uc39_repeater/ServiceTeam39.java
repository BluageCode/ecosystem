/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.service.standardfeatures.application.repeater.uc39_repeater;


import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import com.bluage.documentation.business.standardfeatures.application.repeater.uc39_repeater.entities.daofinder.Team39DAOFinderImpl;
import com.bluage.documentation.service.AbstractService;
import com.netfective.bluage.core.exception.ApplicationException;

/**
 * Defines  application services for ServiceTeam39
  */
@Named("com.bluage.documentation.service.standardfeatures.application.repeater.uc39_repeater.ServiceTeam39")
public class ServiceTeam39  extends AbstractService{


			@Inject
		@Named ("com.bluage.documentation.business.standardfeatures.application.repeater.uc39_repeater.entities.daofinder.Team39DAOFinderImpl")
		private Team39DAOFinderImpl team39BOFinder;



	/**
	 *  Default constructor 
	 */
	public ServiceTeam39() {
		super(); // Call super for default constructor : ServiceTeam39
	}
	



								
	/**    
	 * Operation Crud
	 * @return {@link List}
	 */
	public List findAllTeam39( )
 throws ApplicationException {

					return team39BOFinder.findAll();
	}

		}
