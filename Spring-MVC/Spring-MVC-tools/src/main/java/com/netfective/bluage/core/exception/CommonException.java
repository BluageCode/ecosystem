/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.netfective.bluage.core.exception;
/**
 * 
 * @author @author NETFECTIVE TECHNOLOGY
 */
public class CommonException extends Exception {

	/**
	 * serial UID
	 */
	private static final long serialVersionUID = -8699992884351582111L;
	
	/**
	 * className field String
	 */
	private final String className;
	/**
	 * methodName field String
	 */
	private final String methodName;
	/**
	 * innerException Exception field
	 */
	private final Exception innerException;
	
	/**
	 * Constructeur par defaut de CommonException.
	 */
	public CommonException() {
		super();
		className="";
		methodName="";
		innerException=null;
	}
	
	/**
	 * Constructeur de CommonException avec message d exception
	 * @param mess Le message de l exception.
	 */
	public CommonException(final String mess) {
		super(mess);
		className="";
		methodName="";
		innerException=null;
	}
	
	/**
	 * Constructeur de CommonException avec exception imbriquee
	 * @param innerException Lexception imbriquee.
	 */
	public CommonException(final Exception innerException) {
		super(innerException);
		this.innerException = innerException;
		className="";
		methodName="";
	}
	
	/**
	 * Constructeur de CommonException avec message d exception et exception imbriquee..
	 * @param mess Le message de l exception.
	 * @param innerException  L exception imbriquee.
	 */
	public CommonException(final String mess, final Exception innerException) {
		super(mess, innerException);
		this.innerException = innerException;
		className="";
		methodName="";
	}
	
	/**
	 * Constructeur de CommonException sans message d exception.
	 * @param className le nom de la classe ou l exception s est produite.
	 * @param methodName le nom de la methode ou l exception s est produite.
	 */
	public CommonException(final String className, final String methodName) {
		super();
		this.className = className;
		this.methodName = methodName;
		this.innerException = null;
	}
	
	/**
	 * Constructeur de CommonException avec message d exception
	 * @param mess Le message de l exception.
	 * @param className le nom de la classe ou l exception s est produite.
	 * @param methodName le nom de la methode ou l exception s est produite.
	 */
	public CommonException(final String mess, final String className, final String methodName) {
		super(mess);
		this.className = className;
		this.methodName = methodName;
		this.innerException = null;
	}
	
	/**
	 * Constructeur de CommonException sans message d exception.
	 * @param className le nom de la classe ou l exception s est produite.
	 * @param methodName le nom de la methode ou l exception s est produite.
	 * @param innerException L exception imbriquee.
	 */
	public CommonException(final String className, final String methodName, final Exception innerException) {
		super(innerException);
		this.className = className;
		this.methodName = methodName;
		this.innerException = innerException;
	}
	
	/**
	 * Constructeur de CommonException avec message d exception et exception imbriquee..
	 * @param mess Le message de l exception
	 * @param className le nom de la classe ou l exception s est produite.
	 * @param methodName le nom de la methode ou l exception s est produite.
	 * @param innerException L exception imbriquee.
	 */
	public CommonException(final String mess, final String className, final String methodName, final Exception innerException) {
		super(mess, innerException);
		this.className = className;
		this.methodName = methodName;
		this.innerException = innerException;
	}
	
	/**
	 * Constructeur de CommonException avec message d exception et throwabke
	 * imbrique..
	 * @param msg
	 * @param ex
	 */
	public CommonException(final String msg, final Throwable ex) {
		super(msg, ex);
		className="";
		methodName="";
		innerException=null;
	}

	/**
	 * @return className
	 */
	public String getClassName() {
		return className;
	}

	/**
	 * @return innerException
	 */
	public Exception getInnerException() {
		return innerException;
	}

	/**
	 * @return methodName
	 */
	public String getMethodName() {
		return methodName;
	}
	
	/**
	 * @return ErrorMessage
	 */
	public ErrorMessage doI18n(){
		return null;
	}
}

