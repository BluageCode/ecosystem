/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.advancedfeatures.valueobject.uc206_valueobject ;


import org.apache.log4j.Logger;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;

import com.netfective.bluage.core.validator.AbstractValidator;
import com.netfective.bluage.core.validator.ValidatorsUtil;
/**
* Class :UC206_ListPlayerValidator
*/
public class UC206_ListPlayerValidator extends AbstractValidator{
	
	// LOGGER for the class UC206_ListPlayerValidator
	private static final Logger LOGGER = Logger.getLogger( UC206_ListPlayerValidator.class);
	
	
	/**
	* Operation validate for UC206_ListPlayerForm
	* @param obj : the current form (UC206_ListPlayerForm)
	* @param errors : The spring errors to return for the form UC206_ListPlayerForm
	*/
	public void validate(final Object obj,final Errors errors){
		LOGGER.info("Beginning method : validate"); // UC206_ListPlayerValidator
		UC206_ListPlayerForm cUC206_ListPlayerForm = (UC206_ListPlayerForm)obj; // UC206_ListPlayerValidator
							ValidationUtils.rejectIfEmpty(errors, "playerVOs["+getRequestIndex()+"].firstName", "", "First name is required.");

		if(errors.getFieldError("playerVOs["+getRequestIndex()+"].firstName")== null){
								if("selectedPlayer".equals(getTableElement())){
			if(LOGGER.isDebugEnabled()){
				LOGGER.debug("validation of the 'playerVOs["+getRequestIndex()+"].firstName' field with the LengthValidator validator");
			}
			
			if(ValidatorsUtil.lengthValidator(cUC206_ListPlayerForm.getPlayerVOs().get(Integer.valueOf(getRequestIndex())).getFirstName(), 1, 30) == 0){
				errors.rejectValue("playerVOs["+getRequestIndex()+"].firstName", "", "First name should be less than 30 characters long");
			} 
		}
				
		}
							ValidationUtils.rejectIfEmpty(errors, "playerVOs["+getRequestIndex()+"].lastName", "", "Last name is required.");

		if(errors.getFieldError("playerVOs["+getRequestIndex()+"].lastName")== null){
								if("selectedPlayer".equals(getTableElement())){
			if(LOGGER.isDebugEnabled()){
				LOGGER.debug("validation of the 'playerVOs["+getRequestIndex()+"].lastName' field with the LengthValidator validator");
			}
			
			if(ValidatorsUtil.lengthValidator(cUC206_ListPlayerForm.getPlayerVOs().get(Integer.valueOf(getRequestIndex())).getLastName(), 1, 30) == 0){
				errors.rejectValue("playerVOs["+getRequestIndex()+"].lastName", "", "Last name should be less than 30 characters long");
			} 
		}
				
		}
		LOGGER.info("Ending method : validate the form "+ cUC206_ListPlayerForm.getClass().getName()); // UC206_ListPlayerValidator
	}

	/**
	* Method to implements to use spring validators (UC206_ListPlayerForm)
	* @param aClass : Class for the form UC206_ListPlayerForm
	* @return boolean
	*/
	public boolean supports(final Class aClass){
		return (new UC206_ListPlayerForm()).getClass().equals(aClass); // UC206_ListPlayerValidator
	}
}
