/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.application.form.uc41_actionondropdownselection ;

// Import declaration.
// Java imports.
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.bluage.documentation.business.standardfeatures.application.form.uc41_actionondropdownselection.bos.Position41BO;
import com.bluage.documentation.business.standardfeatures.application.form.uc41_actionondropdownselection.bos.Position41ForSelectBO;
import com.bluage.documentation.service.standardfeatures.application.form.uc41_actionondropdownselection.ServicePlayer41;
import com.bluage.documentation.service.standardfeatures.application.form.uc41_actionondropdownselection.ServicePosition41;
import com.netfective.bluage.core.exception.ApplicationException;
import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;
import com.netfective.bluage.core.web.IForm;

/**
 * Class : UC41_DropDownActionController
 */
@Controller
@Scope("prototype")
@SessionAttributes("uC41_DropDownActionForm")
@RequestMapping(value= "/presentation/standardfeatures/application/form/uc41_actionondropdownselection")
public class UC41_DropDownActionController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC41_DropDownActionController.class);
	
	//Current form name
	private static final String U_C41__DROP_DOWN_ACTION_FORM = "uC41_DropDownActionForm";

			//CONSTANT: sel_positions
	private static final String SEL_POSITIONS = "sel_positions";
							//CONSTANT: players
	private static final String PLAYERS = "players";
	//CONSTANT: position
	private static final String POSITION = "position";
	//CONSTANT: positions
	private static final String POSITIONS = "positions";
	
	/**
	 * Property:customDateEditorsUC41_DropDownActionController
	 */
	final private Map<String,CustomDateEditor> customDateEditorsUC41_DropDownActionController = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:uC41_DropDownActionValidator
	 */
	final private UC41_DropDownActionValidator uC41_DropDownActionValidator = new UC41_DropDownActionValidator();
	
	/**
	 * Service declaration : servicePosition41.
	 */
	@Autowired
	private ServicePosition41 servicePosition41;
	
	/**
	 * Service declaration : servicePlayer41.
	 */
	@Autowired
	private ServicePlayer41 servicePlayer41;
	
	/**
	 * Pre Controller declaration : uC41_PreControllerFindAllPositions
	 */
	@Autowired
	private UC41_PreControllerFindAllPositionsController uC41_PreControllerFindAllPositions;

	
	// Initialise all the instances for UC41_DropDownActionController
				// Initialize the instance sel_positions for the state lnk_autocomplete
		private List sel_positions; // Initialize the instance sel_positions for UC41_DropDownActionController
										// Declare the instance players
		private List players;
		// Declare the instance position
		private Position41BO position;
		// Declare the instance positions
		private Position41ForSelectBO positions;
	/**
	 * Operation : sel_position
 	 * @param model : 
 	 * @param uC41_DropDownAction : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC41_DropDownAction/sel_position.html",method = RequestMethod.POST)
	public String sel_position(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC41_DropDownActionForm") UC41_DropDownActionForm  uC41_DropDownActionForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : sel_position"); 
		infoLogger(uC41_DropDownActionForm); 
		uC41_DropDownActionForm.setAlwaysCallPreControllers(false); 
		// initialization for sel_position
		init();
		
		String destinationPath = null; 
		

					 callLoadPlayers(request,uC41_DropDownActionForm );
			
 callLoadPositions(request,uC41_DropDownActionForm );
						// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.application.form.uc41_actionondropdownselection.UC41_DropDownActionForm or not from sel_position
			// Populate the destination form with all the instances returned from sel_position.
			final IForm uC41_DropDownActionForm2 = populateDestinationForm(request.getSession(), uC41_DropDownActionForm); 
					
			// Add the form to the model.
			model.addAttribute("uC41_DropDownActionForm", uC41_DropDownActionForm2); 
			
			request.getSession().setAttribute("uC41_DropDownActionForm", uC41_DropDownActionForm2);
			
			// "OK" CASE => destination screen path from sel_position
			LOGGER.info("Go to the screen 'UC41_DropDownAction'.");
			// Redirect (PRG) from sel_position
			destinationPath =  "redirect:/presentation/standardfeatures/application/form/uc41_actionondropdownselection/UC41_DropDownAction.html";
						
						
	
	
		return destinationPath; // Returns the destination path for the state : sel_position 
	}
	
		/**
	 * Operation : chx_displayPosition
 	 * @param model : 
 	 * @param uC41_DropDownAction : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC41_DropDownAction/chx_displayPosition.html",method = RequestMethod.POST)
	public String chx_displayPosition(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC41_DropDownActionForm") UC41_DropDownActionForm  uC41_DropDownActionForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : chx_displayPosition"); 
		infoLogger(uC41_DropDownActionForm); 
		uC41_DropDownActionForm.setAlwaysCallPreControllers(false); 
		// initialization for chx_displayPosition
		init();
		
		String destinationPath = null; 
		

									 callDoNothingChkBox(request,uC41_DropDownActionForm );
						// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.application.form.uc41_actionondropdownselection.UC41_DropDownActionForm or not from chx_displayPosition
			// Populate the destination form with all the instances returned from chx_displayPosition.
			final IForm uC41_DropDownActionForm2 = populateDestinationForm(request.getSession(), uC41_DropDownActionForm); 
					
			// Add the form to the model.
			model.addAttribute("uC41_DropDownActionForm", uC41_DropDownActionForm2); 
			
			request.getSession().setAttribute("uC41_DropDownActionForm", uC41_DropDownActionForm2);
			
			// "OK" CASE => destination screen path from chx_displayPosition
			LOGGER.info("Go to the screen 'UC41_DropDownAction'.");
			// Redirect (PRG) from chx_displayPosition
			destinationPath =  "redirect:/presentation/standardfeatures/application/form/uc41_actionondropdownselection/UC41_DropDownAction.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : chx_displayPosition 
	}
	
	
	/**
	* This method initialise the form : UC41_DropDownActionForm 
	* @return UC41_DropDownActionForm
	*/
	@ModelAttribute("UC41_DropDownActionFormInit")
	public void initUC41_DropDownActionForm(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, U_C41__DROP_DOWN_ACTION_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method UC41_DropDownActionForm."); //for chx_displayPosition 
		}
		UC41_DropDownActionForm uC41_DropDownActionForm;
	
		if(request.getSession().getAttribute(U_C41__DROP_DOWN_ACTION_FORM) != null){
			uC41_DropDownActionForm = (UC41_DropDownActionForm)request.getSession().getAttribute(U_C41__DROP_DOWN_ACTION_FORM);
		} else {
			uC41_DropDownActionForm = new UC41_DropDownActionForm();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form UC41_DropDownActionForm.");
		}
		uC41_DropDownActionForm = (UC41_DropDownActionForm)populateDestinationForm(request.getSession(), uC41_DropDownActionForm);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  UC41_DropDownActionForm.");
		}
		model.addAttribute(U_C41__DROP_DOWN_ACTION_FORM, uC41_DropDownActionForm);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param UC41_DropDownActionForm : The sceen form.
	 */
	@RequestMapping(value = "/UC41_DropDownAction.html" ,method = RequestMethod.GET)
	public void prepareUC41_DropDownAction(final HttpServletRequest request,final  Model model,final  @ModelAttribute("UC41_DropDownActionFormInit") UC41_DropDownActionForm uC41_DropDownActionForm){
		
		UC41_DropDownActionForm currentUC41_DropDownActionForm = uC41_DropDownActionForm;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(U_C41__DROP_DOWN_ACTION_FORM) == null){
			if(currentUC41_DropDownActionForm!=null){
				request.getSession().setAttribute(U_C41__DROP_DOWN_ACTION_FORM, currentUC41_DropDownActionForm);
			}else {
				currentUC41_DropDownActionForm = new UC41_DropDownActionForm();
				request.getSession().setAttribute(U_C41__DROP_DOWN_ACTION_FORM, currentUC41_DropDownActionForm);	
			}
		} else {
			currentUC41_DropDownActionForm = (UC41_DropDownActionForm) request.getSession().getAttribute(U_C41__DROP_DOWN_ACTION_FORM);
		}

				currentUC41_DropDownActionForm = (UC41_DropDownActionForm)populateDestinationForm(request.getSession(), currentUC41_DropDownActionForm);
		// Call all the Precontroller.
	if ( currentUC41_DropDownActionForm.isAlwaysCallPreControllers()) {
	
		// Calling the PreController : UC41_PreControllerFindAllPositions.
		currentUC41_DropDownActionForm = (UC41_DropDownActionForm) uC41_PreControllerFindAllPositions.uC41_PreControllerFindAllPositionsControllerInit(request, model, currentUC41_DropDownActionForm);
		
	}
	currentUC41_DropDownActionForm.setAlwaysCallPreControllers(true);
		model.addAttribute(U_C41__DROP_DOWN_ACTION_FORM, currentUC41_DropDownActionForm);
		request.getSession().setAttribute(U_C41__DROP_DOWN_ACTION_FORM, currentUC41_DropDownActionForm);
		
	}
	
						/**
	 * method callLoadPlayers
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callLoadPlayers(HttpServletRequest request,IForm form) {
		players = (List)get(request.getSession(),form, "players");  
										 
					position = (Position41BO)get(request.getSession(),form, "position");  
										 
					try {
				// executing LoadPlayers in chx_displayPosition
				players = 	servicePlayer41.loadPlayers41(
			position
			);  
 
				put(request.getSession(), PLAYERS,players);
								// processing variables LoadPlayers in chx_displayPosition
				put(request.getSession(), POSITION,position); 
			
			} catch (ApplicationException e) { 
				// error handling for operation loadPlayers41 called LoadPlayers
				errorLogger("An error occured during the execution of the operation : loadPlayers41",e); 
		
			}
	}
								/**
	 * method callDoNothingChkBox
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callDoNothingChkBox(HttpServletRequest request,IForm form) {
					players = (List)get(request.getSession(),form, "players");  
										 
					position = (Position41BO)get(request.getSession(),form, "position");  
										 
					try {
				// executing DoNothingChkBox in chx_displayPosition
	servicePlayer41.doNothingBo(
			players
		,			position
			);  

								// processing variables DoNothingChkBox in chx_displayPosition
				put(request.getSession(), PLAYERS,players); 
							put(request.getSession(), POSITION,position); 
			
			} catch (ApplicationException e) { 
				// error handling for operation doNothingBo called DoNothingChkBox
				errorLogger("An error occured during the execution of the operation : doNothingBo",e); 
		
			}
	}
		/**
	 * method callLoadPositions
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callLoadPositions(HttpServletRequest request,IForm form) {
		positions = (Position41ForSelectBO)get(request.getSession(),form, "positions");  
										 
					try {
				// executing LoadPositions in chx_displayPosition
				positions = 	servicePosition41.loadPositions41(
	);  
 
				put(request.getSession(), POSITIONS,positions);
								// processing variables LoadPositions in chx_displayPosition

			} catch (ApplicationException e) { 
				// error handling for operation loadPositions41 called LoadPositions
				errorLogger("An error occured during the execution of the operation : loadPositions41",e); 
		
			}
	}
		
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("UC41_DropDownActionController [ ");
					strBToS.append(SEL_POSITIONS);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(sel_positions);
			strBToS.append(END_TO_STRING_DELIMITER);
									strBToS.append(PLAYERS);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(players);
			strBToS.append(END_TO_STRING_DELIMITER);
			strBToS.append(POSITION);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(position);
			strBToS.append(END_TO_STRING_DELIMITER);
			strBToS.append(POSITIONS);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(positions);
			strBToS.append(END_TO_STRING_DELIMITER);
		// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(uC41_DropDownActionValidator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsUC41_DropDownActionController!=null); 
		return strBToS.toString();
	}
}
