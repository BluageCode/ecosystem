/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.application.form.uc45_autocomplete;


import java.io.Serializable;
import java.util.List;

import com.bluage.documentation.business.standardfeatures.application.form.uc45_autocomplete.bos.Player45BO;
import com.netfective.bluage.core.web.AbstractForm;
import com.netfective.bluage.core.web.IForm;

/**
* Class : uc45_autocompleteForm
*/
public class uc45_autocompleteForm extends AbstractForm implements Serializable, IForm {
	
	/** Constant : serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	//CONSTANT : instance_players
	private static final String INSTANCE_PLAYERS = "instance_players";
	//CONSTANT : name
	private static final String NAME = "name";
	/**
	 * 	Property: instance_players 
	 */
	private List instance_players;
	/**
	 * 	Property: name 
	 */
	private Player45BO name;
/**
	 * Default constructor : uc45_autocompleteForm
	 */
	public uc45_autocompleteForm() {
		super();
		// Initialize : instance_players
		this.instance_players = new java.util.ArrayList();
		// Initialize : name
		this.name = new Player45BO();
			this.setAlwaysCallPreControllers(true);
	}
	
	/**
	 * 	Getter : instance_players 
	 * 	@return : Return the instance_players instance.
	 */
	public List getInstance_players(){
		return instance_players; // For uc45_autocompleteForm
	}
	
	/**
	 * 	Setter : instance_players 
	 *  @param instance_playersinstance : The instance to set.
	 */
	public void setInstance_players(final List instance_playersinstance){
		this.instance_players = instance_playersinstance;// For uc45_autocompleteForm
	}
	/**
	 * 	Getter : name 
	 * 	@return : Return the name instance.
	 */
	public Player45BO getName(){
		return name; // For uc45_autocompleteForm
	}
	
	/**
	 * 	Setter : name 
	 *  @param nameinstance : The instance to set.
	 */
	public void setName(final Player45BO nameinstance){
		this.name = nameinstance;// For uc45_autocompleteForm
	}
/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "uc45_autocompleteForm [ "+
INSTANCE_PLAYERS +" = " + instance_players +NAME +" = " + name + "]";
	}
	
	/**
	 * Method setInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public void setInstanceByInstanceName(final String instanceName, final Object instance) {
		// Set the instance Instance_players.
		if(INSTANCE_PLAYERS.equals(instanceName)){// For uc45_autocompleteForm
			this.setInstance_players((List)instance); // For uc45_autocompleteForm
		}
				// Set the instance Name.
		if(NAME.equals(instanceName)){// For uc45_autocompleteForm
			this.setName((Player45BO)instance); // For uc45_autocompleteForm
		}
			}
	
	/**
	 * Method getInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public Object getInstanceByInstanceName(final String instanceName){
		// Temporary object for tmpuc45_autocompleteForm.
		Object tmpuc45_autocompleteForm = null;
		
			
		// Get the instance Instance_players for uc45_autocompleteForm.
		if(INSTANCE_PLAYERS.equals(instanceName)){ // For uc45_autocompleteForm
			tmpuc45_autocompleteForm = this.getInstance_players(); // For uc45_autocompleteForm
		}
			
		// Get the instance Name for uc45_autocompleteForm.
		if(NAME.equals(instanceName)){ // For uc45_autocompleteForm
			tmpuc45_autocompleteForm = this.getName(); // For uc45_autocompleteForm
		}
		return tmpuc45_autocompleteForm;// For uc45_autocompleteForm
	}
	
	}
