/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.entities.uc69_cachehibernate ;

// Import declaration.
// Java imports.
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.bluage.documentation.business.standardfeatures.entities.uc69_cachehibernate.bos.Team69BO;
import com.bluage.documentation.service.standardfeatures.entities.uc69_cachehibernate.ServiceTeam;
import com.netfective.bluage.core.exception.ApplicationException;
import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;
import com.netfective.bluage.core.web.IForm;

/**
 * Class : UC69_HomePageController
 */
@Controller
@Scope("prototype")
@SessionAttributes("uC69_HomePageForm")
@RequestMapping(value= "/presentation/standardfeatures/entities/uc69_cachehibernate")
public class UC69_HomePageController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC69_HomePageController.class);
	
	//Current form name
	private static final String U_C69__HOME_PAGE_FORM = "uC69_HomePageForm";

	//CONSTANT: allTeams table or repeater.
	private static final String ALL_TEAMS = "allTeams";
				//CONSTANT: team
	private static final String TEAM = "team";
	//CONSTANT: selectedTeam
	private static final String SELECTED_TEAM = "selectedTeam";
	
	/**
	 * Property:customDateEditorsUC69_HomePageController
	 */
	final private Map<String,CustomDateEditor> customDateEditorsUC69_HomePageController = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:uC69_HomePageValidator
	 */
	final private UC69_HomePageValidator uC69_HomePageValidator = new UC69_HomePageValidator();
	
	/**
	 * Service declaration : serviceTeam.
	 */
	@Autowired
	private ServiceTeam serviceTeam;
	
	/**
	 * Pre Controller declaration : uC69_PreControllerFindAllTeams
	 */
	@Autowired
	private UC69_PreControllerFindAllTeamsController uC69_PreControllerFindAllTeams;

	
	// Initialise all the instances for UC69_HomePageController
		// Initialize the instance allTeams for the state lnk_addteam
		private List allTeams; // Initialize the instance allTeams for UC69_HomePageController
						// Declare the instance team
		private Team69BO team;
		// Declare the instance selectedTeam
		private Team69BO selectedTeam;
			/**
	 * Operation : lnk_detail
 	 * @param model : 
 	 * @param uC69_HomePage : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC69_HomePage/lnk_detail.html",method = RequestMethod.POST)
	public String lnk_detail(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC69_HomePageForm") UC69_HomePageForm  uC69_HomePageForm, final BindingResult bindingResult ,final @RequestParam(ID_REQUEST_PARAM) Integer index){
		
		
		
		LOGGER.info("Begin method : lnk_detail"); 
		infoLogger(uC69_HomePageForm); 
		uC69_HomePageForm.setAlwaysCallPreControllers(false); 
		// initialization for lnk_detail
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state lnk_detail if the validation fail, you will be forward to the screen : "/presentation/standardfeatures/entities/uc69_cachehibernate/UC69_HomePage"
		if(errorsMessages(request,response,uC69_HomePageForm,bindingResult,"selectedTeam", uC69_HomePageValidator, customDateEditorsUC69_HomePageController)){ 
			return "/presentation/standardfeatures/entities/uc69_cachehibernate/UC69_HomePage"; 
		}

										 callFindTeamByID(request,uC69_HomePageForm , index);
											// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.entities.uc69_cachehibernate.UC69_DetailTeamForm or not from lnk_detail
			final UC69_DetailTeamForm uC69_DetailTeamForm =  new UC69_DetailTeamForm();
			// Populate the destination form with all the instances returned from lnk_detail.
			final IForm uC69_DetailTeamForm2 = populateDestinationForm(request.getSession(), uC69_DetailTeamForm); 
					
			// Add the form to the model.
			model.addAttribute("uC69_DetailTeamForm", uC69_DetailTeamForm2); 
			
			request.getSession().setAttribute("uC69_DetailTeamForm", uC69_DetailTeamForm2);
			
			// "OK" CASE => destination screen path from lnk_detail
			LOGGER.info("Go to the screen 'UC69_DetailTeam'.");
			// Redirect (PRG) from lnk_detail
			destinationPath =  "redirect:/presentation/standardfeatures/entities/uc69_cachehibernate/UC69_DetailTeam.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_detail 
	}
	
	
	/**
	* This method initialise the form : UC69_HomePageForm 
	* @return UC69_HomePageForm
	*/
	@ModelAttribute("UC69_HomePageFormInit")
	public void initUC69_HomePageForm(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, U_C69__HOME_PAGE_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method UC69_HomePageForm."); //for lnk_detail 
		}
		UC69_HomePageForm uC69_HomePageForm;
	
		if(request.getSession().getAttribute(U_C69__HOME_PAGE_FORM) != null){
			uC69_HomePageForm = (UC69_HomePageForm)request.getSession().getAttribute(U_C69__HOME_PAGE_FORM);
		} else {
			uC69_HomePageForm = new UC69_HomePageForm();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form UC69_HomePageForm.");
		}
		uC69_HomePageForm = (UC69_HomePageForm)populateDestinationForm(request.getSession(), uC69_HomePageForm);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  UC69_HomePageForm.");
		}
		model.addAttribute(U_C69__HOME_PAGE_FORM, uC69_HomePageForm);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param UC69_HomePageForm : The sceen form.
	 */
	@RequestMapping(value = "/UC69_HomePage.html" ,method = RequestMethod.GET)
	public void prepareUC69_HomePage(final HttpServletRequest request,final  Model model,final  @ModelAttribute("UC69_HomePageFormInit") UC69_HomePageForm uC69_HomePageForm){
		
		UC69_HomePageForm currentUC69_HomePageForm = uC69_HomePageForm;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(U_C69__HOME_PAGE_FORM) == null){
			if(currentUC69_HomePageForm!=null){
				request.getSession().setAttribute(U_C69__HOME_PAGE_FORM, currentUC69_HomePageForm);
			}else {
				currentUC69_HomePageForm = new UC69_HomePageForm();
				request.getSession().setAttribute(U_C69__HOME_PAGE_FORM, currentUC69_HomePageForm);	
			}
		} else {
			currentUC69_HomePageForm = (UC69_HomePageForm) request.getSession().getAttribute(U_C69__HOME_PAGE_FORM);
		}

				currentUC69_HomePageForm = (UC69_HomePageForm)populateDestinationForm(request.getSession(), currentUC69_HomePageForm);
		// Call all the Precontroller.
	if ( currentUC69_HomePageForm.isAlwaysCallPreControllers()) {
	
		// Calling the PreController : UC69_PreControllerFindAllTeams.
		currentUC69_HomePageForm = (UC69_HomePageForm) uC69_PreControllerFindAllTeams.uC69_PreControllerFindAllTeamsControllerInit(request, model, currentUC69_HomePageForm);
		
	}
	currentUC69_HomePageForm.setAlwaysCallPreControllers(true);
		model.addAttribute(U_C69__HOME_PAGE_FORM, currentUC69_HomePageForm);
		request.getSession().setAttribute(U_C69__HOME_PAGE_FORM, currentUC69_HomePageForm);
		
	}
	
										/**
	 * method callFindTeamByID
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 * @param index Integer the current index 
	 */
	private void callFindTeamByID(HttpServletRequest request,IForm form,Integer index) {
		team = (Team69BO)get(request.getSession(),form, "team");  
										 
					if(index!=null){  
			 selectedTeam = (Team69BO)((List)get(request.getSession(),form, "allTeams")).get(index);  
		} else {
			 selectedTeam= null; 
		}
				 
					try {
				// executing FindTeamByID in lnk_detail
				team = 	serviceTeam.team69FindByID(
			selectedTeam
			);  
 
				put(request.getSession(), TEAM,team);
								// processing variables FindTeamByID in lnk_detail
				put(request.getSession(), SELECTED_TEAM,selectedTeam); 
			
			} catch (ApplicationException e) { 
				// error handling for operation team69FindByID called FindTeamByID
				errorLogger("An error occured during the execution of the operation : team69FindByID",e); 
		
			}
	}
		
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("UC69_HomePageController [ ");
			strBToS.append(ALL_TEAMS);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(allTeams);
			strBToS.append(END_TO_STRING_DELIMITER);
						strBToS.append(TEAM);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(team);
			strBToS.append(END_TO_STRING_DELIMITER);
			strBToS.append(SELECTED_TEAM);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(selectedTeam);
			strBToS.append(END_TO_STRING_DELIMITER);
		// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(uC69_HomePageValidator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsUC69_HomePageController!=null); 
		return strBToS.toString();
	}
}
