ALTER TABLE player42 DROP CONSTRAINT player42_team_fkc;

ALTER TABLE team42 DROP CONSTRAINT team42_pkey;

ALTER TABLE player42 DROP CONSTRAINT player42_pkey;

ALTER TABLE team42 DROP CONSTRAINT team42_name_key;


DROP TABLE player42;

DROP TABLE team42;


CREATE TABLE player42 (
	id bigint NOT NULL,
	version integer NOT NULL,
	first_name character varying(30),
	last_name character varying(30),
	date_of_birth date,
	estimated_value real,
	weight integer, 
	height integer, 
	rookie boolean, 
	team_fk bigint
);



ALTER TABLE ONLY player42
    ADD CONSTRAINT player42_pkey PRIMARY KEY (id);
  
CREATE TABLE team42 (
    id bigint NOT NULL,
    version integer NOT NULL,
    city character varying(30),
    name character varying(30)
);



ALTER TABLE ONLY team42
    ADD CONSTRAINT team42_pkey PRIMARY KEY (id);

ALTER TABLE ONLY player42
    ADD CONSTRAINT player42_team_fkc FOREIGN KEY (team_fk) REFERENCES team42(id);
    
ALTER TABLE team42
  ADD CONSTRAINT team42_name_key UNIQUE(name);


INSERT INTO team42 VALUES (1, 0, 'Charlotte', 'Tigers');
INSERT INTO team42 VALUES (2, 0, 'Kansas City', 'Bears');
INSERT INTO team42 VALUES (3, 0, 'Detroit', 'Chargers');
INSERT INTO team42 VALUES (4, 0, 'Detroit', 'Hawks');
INSERT INTO team42 VALUES (5, 0, 'Omaha', 'Braves');

INSERT INTO player42 VALUES (287, 0, 'Owen', 'Little', '1981-10-27', 45.869999, 135, 183, false, 5);
INSERT INTO player42 VALUES (288, 0, 'Irvin', 'Lee', '1988-02-25', 10.61, 88, 157, false, 1);
INSERT INTO player42 VALUES (289, 0, 'Wayne', 'Rose', '1981-10-28', 20.41, 115, 238, false, 1);
INSERT INTO player42 VALUES (290, 0, 'Lucas', 'Williamson', '1983-06-29', 38.360001, 73, 151, false, 1);
INSERT INTO player42 VALUES (291, 0, 'Walter', 'Reyes', '1984-12-09', 46.150002, 104, 193, false, 1);
INSERT INTO player42 VALUES (292, 0, 'Kirk', 'Cox', '1973-05-10', 26.58, 60, 179, false, 3);
INSERT INTO player42 VALUES (293, 0, 'Arthur', 'Payne', '1981-11-20', 30.860001, 66, 190, false, 5);
INSERT INTO player42 VALUES (294, 0, 'Leroy', 'Moore', '1987-07-26', 45.799999, 89, 183, false, 2);
INSERT INTO player42 VALUES (295, 0, 'Milton', 'Johnson', '1969-07-02', 22.219999, 68, 205, false, 5);
INSERT INTO player42 VALUES (296, 0, 'Sammy', 'Mendez', '1986-07-01', 2.27, 96, 234, false, 2);
INSERT INTO player42 VALUES (297, 0, 'Rafael', 'Steele', '1968-11-25', 15.48, 66, 153, false, 1);
INSERT INTO player42 VALUES (298, 0, 'Bob', 'Jacobs', '1988-05-21', 13.23, 72, 172, false, 2);
INSERT INTO player42 VALUES (299, 0, 'Jared', 'Stevenson', '1974-05-03', 12.11, 125, 187, false, 4);
INSERT INTO player42 VALUES (300, 0, 'Shannon', 'Barton', '1980-03-10', 23.809999, 77, 197, false, 2);
INSERT INTO player42 VALUES (206, 0, 'Nick', 'Garner', '1992-07-27', 47.84, 87, 221, true, 2);
INSERT INTO player42 VALUES (213, 0, 'Tommy', 'Wood', '1992-01-20', 13.93, 77, 173, true, 2);
INSERT INTO player42 VALUES (235, 0, 'Angel', 'Turner', '1990-08-17', 16.85, 64, 209, true, 4);
INSERT INTO player42 VALUES (238, 0, 'Brad', 'Houston', '1992-02-28', 33.389999, 81, 181, true, 5);
INSERT INTO player42 VALUES (301, 0, 'Erick', 'Mccarthy', '1970-12-07', 3.76, 75, 150, false, 5);
INSERT INTO player42 VALUES (302, 0, 'Justin', 'Johnson', '1988-02-27', 47.950001, 137, 161, false, 2);
INSERT INTO player42 VALUES (303, 0, 'Elmer', 'Hansen', '1984-07-12', 12.86, 88, 213, false, 4);
INSERT INTO player42 VALUES (304, 0, 'Everett', 'Sherman', '1978-11-03', 23.280001, 118, 213, false, 2);
INSERT INTO player42 VALUES (305, 0, 'Rafael', 'Ward', '1967-02-08', 38.77, 91, 178, false, 1);
INSERT INTO player42 VALUES (306, 0, 'Kenny', 'Rogers', '1965-02-02', 5.4400001, 68, 153, false, 1);
INSERT INTO player42 VALUES (308, 0, 'Lonnie', 'Brady', '1987-12-11', 11.83, 77, 219, false, 2);
INSERT INTO player42 VALUES (309, 0, 'Sergio', 'Taylor', '1984-08-29', 25.290001, 133, 225, false, 4);
INSERT INTO player42 VALUES (310, 0, 'Byron', 'Morton', '1981-03-28', 21.4, 154, 196, false, 1);
INSERT INTO player42 VALUES (311, 0, 'Bob', 'Estrada', '1975-10-20', 39.59, 98, 197, false, 1);
INSERT INTO player42 VALUES (312, 0, 'Frank', 'Strickland', '1970-06-27', 21.879999, 116, 234, false, 2);
INSERT INTO player42 VALUES (313, 0, 'Johnathan', 'Norman', '1978-08-20', 17.620001, 117, 214, false, 5);
INSERT INTO player42 VALUES (314, 0, 'Leonard', 'Stephens', '1965-12-02', 13.12, 130, 168, false, 2);
INSERT INTO player42 VALUES (315, 0, 'Tim', 'Taylor', '1985-01-26', 8.1599998, 146, 167, false, 2);
INSERT INTO player42 VALUES (316, 0, 'Bobby', 'Weaver', '1979-11-25', 3.1099999, 99, 227, false, 5);
INSERT INTO player42 VALUES (318, 0, 'Bernard', 'Wise', '1970-02-01', 48.48, 66, 219, false, 5);
INSERT INTO player42 VALUES (319, 0, 'Rodolfo', 'Wong', '1968-08-10', 26.969999, 84, 191, false, 1);
INSERT INTO player42 VALUES (320, 0, 'Matthew', 'Haynes', '1982-07-16', 18.91, 102, 218, true, null);
INSERT INTO player42 VALUES (321, 0, 'Clayton', 'Doyle', '1976-12-10', 36.240002, 127, 177, true, null);
INSERT INTO player42 VALUES (322, 0, 'Floyd', 'Hampton', '1988-03-03', 21.67, 115, 178, false, null);
INSERT INTO player42 VALUES (323, 0, 'Timmy', 'Walker', '1969-04-14', 45.91, 149, 190, false, null);
INSERT INTO player42 VALUES (324, 0, 'Jon', 'Hines', '1980-12-27', 24.879999, 68, 155, false, null);
INSERT INTO player42 VALUES (325, 0, 'Orlando', 'Burns', '1973-09-02', 19.139999, 159, 194, false, null);
INSERT INTO player42 VALUES (326, 0, 'Randal', 'Mendez', '1974-03-04', 21.200001, 141, 175, false, null);
INSERT INTO player42 VALUES (327, 0, 'Jeremy', 'Holmes', '1983-12-08', 38.299999, 118, 234, false, null);
INSERT INTO player42 VALUES (330, 0, 'Shannon', 'Watkins', '1979-02-07', 25.66, 69, 168, false, null);
INSERT INTO player42 VALUES (331, 0, 'Ralph', 'Burton', '1969-11-13', 7.52, 85, 175, false, null);
INSERT INTO player42 VALUES (332, 0, 'Bobby', 'Jefferson', '1985-10-06', 17.4, 128, 240, false, null);
INSERT INTO player42 VALUES (333, 0, 'Travis', 'Williamson', '1974-08-23', 20.299999, 115, 174, false, null);
INSERT INTO player42 VALUES (334, 0, 'Wayne', 'Clarke', '1965-07-24', 24.9, 114, 154, false, null);
INSERT INTO player42 VALUES (335, 0, 'Saul', 'Zimmerman', '1988-12-28', 45.82, 149, 201, false, null);
INSERT INTO player42 VALUES (337, 0, 'Bradford', 'Porter', '1977-06-02', 16.190001, 111, 234, true, null);
INSERT INTO player42 VALUES (339, 0, 'Bernard', 'Roy', '1987-05-07', 25.15, 65, 172, true, null);
