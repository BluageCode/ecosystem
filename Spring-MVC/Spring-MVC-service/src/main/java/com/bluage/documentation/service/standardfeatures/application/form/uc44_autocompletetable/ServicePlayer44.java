/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.service.standardfeatures.application.form.uc44_autocompletetable;


import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import com.bluage.documentation.business.standardfeatures.application.form.uc44_autocompletetable.bos.Player44BO;
import com.bluage.documentation.business.standardfeatures.application.form.uc44_autocompletetable.bos.Team44BO;
import com.bluage.documentation.business.standardfeatures.application.form.uc44_autocompletetable.entities.dao.Player44DAOImpl;
import com.bluage.documentation.service.AbstractService;
import com.bluage.documentation.service.standardfeatures.application.form.uc44_autocompletetable.daofinder.ServicePlayer44DAOMethodFinderImpl;
import com.netfective.bluage.core.exception.ApplicationException;

/**
 * Defines  application services for ServicePlayer44
  */
@Named("com.bluage.documentation.service.standardfeatures.application.form.uc44_autocompletetable.ServicePlayer44")
public class ServicePlayer44  extends AbstractService{


			@Inject
		@Named ("com.bluage.documentation.service.standardfeatures.application.form.uc44_autocompletetable.daofinder.ServicePlayer44DAOMethodFinder")
		private ServicePlayer44DAOMethodFinderImpl servicePlayer44Dao;
		@Inject
		@Named ("com.bluage.documentation.business.standardfeatures.application.form.uc44_autocompletetable.entities.dao.Player44DAOImpl")
		private Player44DAOImpl player44BODao;
					@Inject
		@Named ("com.bluage.documentation.service.standardfeatures.application.form.uc44_autocompletetable.ServiceTeam44")
		private ServiceTeam44  instanceServiceTeam44;
						


	/**
	 *  Default constructor 
	 */
	public ServicePlayer44() {
		super(); // Call super for default constructor : ServicePlayer44
	}
	



				
	/**    
	 * Operation HQL.
	 * @return {@link List}
	 */
	public List loadPlayers44( )
 throws ApplicationException {
	
	return servicePlayer44Dao.loadPlayers44();
	}

		
			
	/**    
	 * Process operation.
	 * @param playerToUpdate {@link Player44BO}
		 * @param teamToUpdate {@link Team44BO}
		 * @return {@link void}
	 */
	public void updatePlayer(Player44BO playerToUpdate,Team44BO teamToUpdate )
 throws ApplicationException {

			privateUpdatePlayer(playerToUpdate,teamToUpdate);
	}
	

	/**    
	 * Process operation.
	 * @param playerToUpdate {@link Player44BO}
		 * @param teamToUpdate {@link Team44BO}
		 * @return {@link void}
	 */
	private void privateUpdatePlayer(Player44BO playerToUpdate,Team44BO teamToUpdate )
 throws ApplicationException {
				// Central buffer node declaration for operation updatePlayer
			Team44BO	 teamFound =  new Team44BO();
					String	 name =  "";
						// Calling an operation : Get Name
name = teamToUpdate.getName();
// Calling an operation : Search Team
teamFound = instanceServiceTeam44.searchTeam44(name);
// Test on exception : 
if (teamFound==null
) {
ApplicationException app = new ApplicationException("");
throw app;
}
// Calling an operation : 
playerToUpdate.setTeam44(teamFound);
// Calling an operation : 
player44Update(playerToUpdate);

		}

					
				
	/**    
	 * Operation Crud
	 * @param bo {@link Player44BO}
		 * @return {@link boolean}
	 */
	public boolean player44Update(Player44BO bo )
 throws ApplicationException {

					return player44BODao.update(bo);
	}

		}
