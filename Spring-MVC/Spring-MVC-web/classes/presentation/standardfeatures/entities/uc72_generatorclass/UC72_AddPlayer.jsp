<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file="/include.jsp"  %>
<html >
<head >
<title  > 
<fmt:message key="cUC72_AddPlayer_BLU_AGE_DOCUMENTATION"/>
</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"  > 
</meta>
<link href="<c:url value= "/presentation/_style/_style.css"/>" rel="stylesheet" type="text/css"  /> 
<link href="<c:url value= "/presentation/_include/jquery/rd_lib.css"/>" rel="stylesheet" type="text/css"  /> 
<script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/jquery.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/rd_setup.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/rd_lib.js"/>" space="preserve"  ></script><link rel="shortcut icon" href="<c:url value= "/presentation/_style/favicon.ico"/>"  /> 

</head>

<body class=" " > 
<div id="rd_ghost"  > 
<small  > 
<fmt:message key="cUC72_AddPlayer___CSS_DEACTIVATED"/>
</small>
</div>
<div id="rd_jghost"  > 
<small  > 
<fmt:message key="cUC72_AddPlayer___JAVASCRIPT_DEACTIVATED"/>
</small>
</div>
<div id="shape"  > 
<div class="ui-layout-center"  > 
<div class="east-north"  > 
	<form:form id="uC72_AddPlayer" commandName="uC72_AddPlayerForm"  enctype="application/x-www-form-urlencoded" method="post">
<h2 class="rd_first"  > 
<fmt:message key="cUC72_AddPlayer_STANDARD_FEATURES__ENTITIES_"/>
</h2>
<h4 class="rd_first"  > 
<fmt:message key="cUC72_AddPlayer_ADD_PLAYER"/>
</h4>
<table class="rd_tableform"  >
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC72_AddPlayer_ID_"/>
</label>
<label style="background: none;color: red;"  > 
<fmt:message key="cUC72_AddPlayer_"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
<form:input id="txt_id" path="playerToCreate.id"  
		/>
</td> 
<td rowspan="1" colspan="1"  >
<form:errors path="playerToCreate.id"/>
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC72_AddPlayer_FIRST_NAME_"/>
</label>
<label style="background: none;color: red;"  > 
<fmt:message key="cUC72_AddPlayer__178"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
<form:input id="txt_firstname" path="playerToCreate.firstName"  size="30" 
		/>
</td> 
<td rowspan="1" colspan="1"  >
<form:errors path="playerToCreate.firstName"/>
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC72_AddPlayer_LAST_NAME_"/>
</label>
<label style="background: none;color: red;"  > 
<fmt:message key="cUC72_AddPlayer__179"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
<form:input id="txt_lastname" path="playerToCreate.lastName"  size="30" 
		/>
</td> 
<td rowspan="1" colspan="1"  >
<form:errors path="playerToCreate.lastName"/>
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC72_AddPlayer_DATE_OF_BIRTH_MMDDYYYY"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
<form:input id="txt_dateofbirth" path="playerToCreate.dateOfBirth"  size="12" 
		/>
</td> 
<td rowspan="1" colspan="1"  >
<form:errors path="playerToCreate.dateOfBirth"/>
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC72_AddPlayer_POSITION"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
<form:select id="slt_position" path="playerToCreate.position.code"   
		>
		<form:options items="${uC72_AddPlayerForm.allPositions}" itemValue="code" itemLabel="name" />
</form:select>
</td> 
<td rowspan="1" colspan="1"  >
<fmt:message key="cUC72_AddPlayer__180"/>
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC72_AddPlayer_ROOKIE"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
<form:checkbox id="chk_rookie" path="playerToCreate.rookie"   
		/>
</td> 
<td rowspan="1" colspan="1"  >
<fmt:message key="cUC72_AddPlayer__181"/>
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<fmt:message key="cUC72_AddPlayer__182"/>
</td> 
<td rowspan="1" colspan="1"  >
<a id="lnk_create"onclick="javascript:btn_click_cExecuteUC72_AddPlayer(this.href);return false;"				href="<c:url value="/presentation/standardfeatures/entities/uc72_generatorclass/UC72_AddPlayer/lnk_create.html"/>"
							 shape="rect"  >									 
<fmt:message key="cUC72_AddPlayer_CREATE_PLAYER"/>
</a>
</td> 
<td rowspan="1" colspan="1"  >
<fmt:message key="cUC72_AddPlayer__183"/>
</td> 
</tr>
</table> 
<label style="background: none;color: red;"  > 
<fmt:message key="cUC72_AddPlayer__184"/>
</label>
<fmt:message key="cUC72_AddPlayer_REQUIRED_ENTRY____________"/>
<br clear="none"  /> 
<br clear="none"  /> 
<display:table uid="tab_players"  sort="list" requestURI="" name="sessionScope.uC72_AddPlayerForm.listPlayers" class="rd_tabledata"  >
	<c:set   var="rownum">
		<%=(tab_players !=	null) ? new Integer(tab_players_rowNum.intValue() - 1) : new Integer(0) %>
	</c:set>
	<c:set   var="idRow">
		<%=(tab_players != null ) ? tab_players_rowNum.intValue() - 1 : -1 %>
	</c:set>
	<c:set   var="idTab">
		<%=(tab_players != null ) ? "tab_players" : null %>
	</c:set>
	<c:set var="tableHashId">
		<%= new org.displaytag.util.ParamEncoder("tab_players").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE) %>
	</c:set>
	<c:set var="tableHashId2">
		<%=request.getParameter(new org.displaytag.util.ParamEncoder("tab_players").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE)) %>
	</c:set>	
	
<display:column format=""  sortable="true" titleKey="cUC72_AddPlayer_ID">
		<span id="dyn_id">
	<c:out value="${tab_players.id}" />
		</span>
	</display:column>
	
<display:column format=""  sortable="true" titleKey="cUC72_AddPlayer_NAME">
		<span id="dyn_lastname">
	<c:out value="${tab_players.lastName}" />
		</span>
	<fmt:message key="cUC72_AddPlayer______________"/>
		<span id="dyn_firstname">
	<c:out value="${tab_players.firstName}" />
		</span>
	</display:column>
	
<display:column format=""  sortable="true" titleKey="cUC72_AddPlayer_DATE_OF_BIRTH">
<span id="dyn_dateofbirth">
	<format:formatDateValidator value="${tab_players.dateOfBirth}" pattern="MM/dd/yyyy" />
</span>
	</display:column>
	
<display:column format=""  sortable="true" titleKey="cUC72_AddPlayer_POSITION_185">
		<span id="dyn_position">
	<c:out value="${tab_players.position.code}" />
		</span>
	</display:column>
	
<display:column format=""  sortable="true" titleKey="cUC72_AddPlayer_ROOKIE_186">
<form:checkbox id="chk_rookie[${rownum}]" path="listPlayers[${rownum}].rookie"  name="isRookie" disabled="true"  
		/>
</display:column>
	
<display:column format=""  sortable="true" titleKey="cUC72_AddPlayer_DELETE">
<a id="lnk_delete[${rownum}]"onclick="javascript:btn_click_cExecuteUC72_AddPlayer(this.href);return false;"			href="<c:url value="/presentation/standardfeatures/entities/uc72_generatorclass/UC72_AddPlayer/lnk_delete.html?id="/><c:out value="${idRow}" />&tab=<c:out value="tab_players" />&<c:out value="${tableHashId}"/>=<c:out value="${tableHashId2}"/>"
	 shape="rect"  >									 
<fmt:message key="cUC72_AddPlayer_DELETE_187"/>
</a>
</display:column>
</display:table>
<br clear="none"  /> 
<a id="lnk_back"				href="<c:url value="/presentation/standardfeatures/entities/uc72_generatorclass/UC72_Home.html"/>"
													 shape="rect"  >									 
<fmt:message key="cUC72_AddPlayer_BACK"/>
</a>
	</form:form>
	<script language="javascript">
		function btn_click_cExecuteUC72_AddPlayer(url) {
			UC72_AddPlayervar = document.getElementById('uC72_AddPlayer');
			UC72_AddPlayervar.setAttribute('action', url);
			UC72_AddPlayervar.submit();
		}
		function btn_click_cSetActionUC72_AddPlayer(url) {
			UC72_AddPlayervar = document.getElementById('uC72_AddPlayer');
			UC72_AddPlayervar.setAttribute('action', url);
		}
	</script>
</div>
<div class="east-center"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/entities/uc72_generatorclass/UC72_AddPlayercmp_tabs" />
</div>
</div>
<div class="ui-layout-north"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/entities/uc72_generatorclass/UC72_AddPlayercmp_header" />
</div>
<div class="ui-layout-south"  > 
<fmt:message key="cUC72_AddPlayer________"/>
<span id="rd_jversion"  > 
<fmt:message key="cUC72_AddPlayer_BLU_AGE_DOCUMENTATION_188"/>
</span>
</div>
<div class="ui-layout-west"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/entities/uc72_generatorclass/UC72_AddPlayercmp_treeview" />
</div>
</div>
</body>
</html>
