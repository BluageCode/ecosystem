<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file="/include.jsp"  %>
<html >
<head >
	<script src="<%=request.getContextPath()%>/inc/yui/yahoo-dom-event/yahoo-dom-event.js"></script>
	<script	src="<%=request.getContextPath()%>/inc/yui/datasource/datasource-min.js"></script>
	<script src="<%=request.getContextPath()%>/inc/yui/get/get-min.js"></script>
	<script src="<%=request.getContextPath()%>/inc/yui/connection/connection-min.js"></script>
	<script src="<%=request.getContextPath()%>/inc/yui/animation/animation-min.js"></script>
	<script src="<%=request.getContextPath()%>/inc/yui/json/json-min.js"></script>
	<script src="<%=request.getContextPath()%>/inc/yui/autocomplete/autocomplete-min.js"></script>
	<link type="text/css" rel="stylesheet" href="<%=request.getContextPath()%>/inc/yui/autocomplete/assets/skins/sam/autocomplete.css" ></link>
	<script language="javascript">
	function setAutocomplete(instancenamelist, node_for_id, node_for_id_div, nbMaxResults){
		//<!--
		var auto_req = '<%=request.getContextPath()%>/presentation/standardfeatures/application/form/uc44_autocompletetable/'+instancenamelist+'autocomplete.html?';
		var auto_oDS = new YAHOO.util.XHRDataSource(auto_req);   
	    // Set the responseType   
	    auto_oDS.responseType = YAHOO.util.XHRDataSource.TYPE_TEXT;   
	    // Define the schema of the delimited results   
	    auto_oDS.responseSchema = {   
	        recordDelim: ",",   
	        fieldDelim: "\t"  
	     };   
		var node_AutoComp = new YAHOO.widget.AutoComplete(node_for_id,node_for_id_div, auto_oDS);
		node_AutoComp.queryQuestionMark = false
		if (nbMaxResults != null){
			node_AutoComp.maxResultsDisplayed =nbMaxResults;
		}
		// -->
	}		
	</script>
<title  > 
<fmt:message key="cUC44_AutocompleteTable_BLU_AGE_DOCUMENTATION"/>
</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"  > 
</meta>
<link href="<c:url value= "/presentation/_style/_style.css"/>" rel="stylesheet" type="text/css"  /> 
<link href="<c:url value= "/presentation/_include/jquery/rd_lib.css"/>" rel="stylesheet" type="text/css"  /> 
<script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/jquery.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/rd_setup.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/rd_lib.js"/>" space="preserve"  ></script><script language="javascript" space="preserve"  > jQuery.noConflict();  
</script><link rel="shortcut icon" href="<c:url value= "/presentation/_style/favicon.ico"/>"  /> 

</head>

<body class=" yui-skin-sam" > 
<div id="rd_ghost"  > 
<small  > 
<fmt:message key="cUC44_AutocompleteTable___CSS_DEACTIVATED"/>
</small>
</div>
<div id="rd_jghost"  > 
<small  > 
<fmt:message key="cUC44_AutocompleteTable___JAVASCRIPT_DEACTIVATED"/>
</small>
</div>
<div id="shape"  > 
<div class="ui-layout-center"  > 
<div class="east-north"  > 
	<form:form id="uC44_AutocompleteTable" commandName="uC44_AutocompleteTableForm"  enctype="application/x-www-form-urlencoded" method="post">
<h2 class="rd_first"  > 
<fmt:message key="cUC44_AutocompleteTable_STANDARD_FEATURES__APPLICATIO"/>
</h2>
<display:table uid="tab_players"  sort="list" requestURI="" name="sessionScope.uC44_AutocompleteTableForm.allPlayers" class="rd_tabledata"  >
	<c:set   var="rownum">
		<%=(tab_players !=	null) ? new Integer(tab_players_rowNum.intValue() - 1) : new Integer(0) %>
	</c:set>
	<c:set   var="idRow">
		<%=(tab_players != null ) ? tab_players_rowNum.intValue() - 1 : -1 %>
	</c:set>
	<c:set   var="idTab">
		<%=(tab_players != null ) ? "tab_players" : null %>
	</c:set>
	<c:set var="tableHashId">
		<%= new org.displaytag.util.ParamEncoder("tab_players").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE) %>
	</c:set>
	<c:set var="tableHashId2">
		<%=request.getParameter(new org.displaytag.util.ParamEncoder("tab_players").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE)) %>
	</c:set>	
	
<display:column format=""  titleKey="cUC44_AutocompleteTable_NAME">
<c:if test="${uC44_AutocompleteTableForm.selectedRow!=idRow || uC44_AutocompleteTableForm.selectedTab!=idTab}">
		<span id="dyn_lastname">
	<c:out value="${tab_players.lastName}" />
		</span>
	<fmt:message key="cUC44_AutocompleteTable_"/>
		<span id="dyn_firstname">
	<c:out value="${tab_players.firstName}" />
		</span>
	</c:if>
<c:if test="${uC44_AutocompleteTableForm.selectedRow==idRow && uC44_AutocompleteTableForm.selectedTab==idTab}">
<form:input id="txt_lastname[${rownum}]" path="allPlayers[${rownum}].lastName"  
		/>
<form:errors path="allPlayers[${rownum}].lastName"/>
<fmt:message key="cUC44_AutocompleteTable_____"/>
<form:input id="txt_firstname[${rownum}]" path="allPlayers[${rownum}].firstName"  
		/>
<form:errors path="allPlayers[${rownum}].firstName"/>
</c:if>
</display:column>
	
<display:column format=""  titleKey="cUC44_AutocompleteTable_DATE_OF_BIRTH">
<c:if test="${uC44_AutocompleteTableForm.selectedRow!=idRow || uC44_AutocompleteTableForm.selectedTab!=idTab}">
<span id="dyn_dateofbirth">
	<format:formatDateValidator value="${tab_players.dateOfBirth}" pattern="MM/dd/yyyy" />
</span>
	</c:if>
<c:if test="${uC44_AutocompleteTableForm.selectedRow==idRow && uC44_AutocompleteTableForm.selectedTab==idTab}">
<form:input id="txt_dateofbirth[${rownum}]" path="allPlayers[${rownum}].dateOfBirth"  
		/>
<form:errors path="allPlayers[${rownum}].dateOfBirth"/>
</c:if>
</display:column>
	
<display:column format=""  titleKey="cUC44_AutocompleteTable_WEIGHT_KG">
		<span id="dyn_weight">
	<c:out value="${tab_players.weight}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="cUC44_AutocompleteTable_HEIGHT_CM">
		<span id="dyn_height">
	<c:out value="${tab_players.height}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="cUC44_AutocompleteTable_ESTIMATED_VALUE_M">
		<span id="dyn_value">
	<c:out value="${tab_players.estimatedValue}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="cUC44_AutocompleteTable_TEAM">
<c:if test="${uC44_AutocompleteTableForm.selectedRow!=idRow || uC44_AutocompleteTableForm.selectedTab!=idTab}">
		<span id="dyn_team">
	<c:out value="${tab_players.team44.name}" />
		</span>
	</c:if>
<c:if test="${uC44_AutocompleteTableForm.selectedRow==idRow && uC44_AutocompleteTableForm.selectedTab==idTab}">
<form:input id="txt_teamName[${rownum}]" path="selectedTeam.name"  
		/>
	<div id="txt_teamName_div[${rownum}]"></div>
	<script>
		setAutocomplete("instance_teams", "txt_teamName[${rownum}]", "txt_teamName_div[${rownum}]",0);
	</script>
<form:errors path="selectedTeam.name"/>
</c:if>
</display:column>
	
<display:column format=""  titleKey="cUC44_AutocompleteTable__111">
<c:if test="${uC44_AutocompleteTableForm.selectedRow!=idRow || uC44_AutocompleteTableForm.selectedTab!=idTab}">
<a id="lnk_edit[${rownum}]"onclick="javascript:btn_click_cExecuteUC44_AutocompleteTable(this.href);return false;"			href="<c:url value="/presentation/standardfeatures/application/form/uc44_autocompletetable/UC44_AutocompleteTable/lnk_edit.html?id="/><c:out value="${idRow}" />&tab=<c:out value="tab_players" />&<c:out value="${tableHashId}"/>=<c:out value="${tableHashId2}"/>"
	 shape="rect"  >									 
<fmt:message key="cUC44_AutocompleteTable_EDIT"/>
</a>
</c:if>
<c:if test="${uC44_AutocompleteTableForm.selectedRow==idRow && uC44_AutocompleteTableForm.selectedTab==idTab}">
<a id="lnk_update[${rownum}]"onclick="javascript:btn_click_cExecuteUC44_AutocompleteTable(this.href);return false;"			href="<c:url value="/presentation/standardfeatures/application/form/uc44_autocompletetable/UC44_AutocompleteTable/lnk_update.html?id="/><c:out value="${idRow}" />&tab=<c:out value="tab_players" />&<c:out value="${tableHashId}"/>=<c:out value="${tableHashId2}"/>"
	 shape="rect"  >									 
<fmt:message key="cUC44_AutocompleteTable_UPDATE"/>
</a>
</c:if>
</display:column>
</display:table>
	</form:form>
	<script language="javascript">
		function btn_click_cExecuteUC44_AutocompleteTable(url) {
			UC44_AutocompleteTablevar = document.getElementById('uC44_AutocompleteTable');
			UC44_AutocompleteTablevar.setAttribute('action', url);
			UC44_AutocompleteTablevar.submit();
		}
		function btn_click_cSetActionUC44_AutocompleteTable(url) {
			UC44_AutocompleteTablevar = document.getElementById('uC44_AutocompleteTable');
			UC44_AutocompleteTablevar.setAttribute('action', url);
		}
	</script>
</div>
<div class="east-center"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/application/form/uc44_autocompletetable/UC44_AutocompleteTablecmp_tabs" />
</div>
</div>
<div class="ui-layout-north"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/application/form/uc44_autocompletetable/UC44_AutocompleteTablecmp_header" />
</div>
<div class="ui-layout-south"  > 
<fmt:message key="cUC44_AutocompleteTable___"/>
<span id="rd_jversion"  > 
<fmt:message key="cUC44_AutocompleteTable_BLU_AGE_CORPORATION"/>
</span>
</div>
<div class="ui-layout-west"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/application/form/uc44_autocompletetable/UC44_AutocompleteTablecmp_treeview" />
</div>
</div>
</body>
</html>
