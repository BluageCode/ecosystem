--<ScriptOptions statementTerminator=";"/>

ALTER TABLE player32 DROP CONSTRAINT player32_position_fkc;

ALTER TABLE position32 DROP CONSTRAINT position32_pkey;

ALTER TABLE player32 DROP CONSTRAINT player32_pkey;

DROP TABLE player32;

DROP TABLE position32;

CREATE TABLE player32 (
		id INT8 NOT NULL,
		version INT4 NOT NULL,
		first_name VARCHAR(30) NOT NULL,
		last_name VARCHAR(30) NOT NULL,
		date_of_birth DATE,
		estimated_value FLOAT4,
		played_games INT4,
		number_of_baskets INT4,
		rookie BOOL,
		efficacy FLOAT4,
		position_fk VARCHAR(2) NOT NULL
	);

CREATE TABLE position32 (
		code VARCHAR(2) NOT NULL,
		version INT4 NOT NULL,
		name VARCHAR(20) NOT NULL
	);


INSERT INTO position32 VALUES ('PG', 0, 'Point Guard');
INSERT INTO position32 VALUES ('SG', 0, 'Shooting Guard');
INSERT INTO position32 VALUES ('SF', 0, 'Small Forward');
INSERT INTO position32 VALUES ('PF', 0, 'Power Forward');
INSERT INTO position32 VALUES ('C', 0, 'Center');
	
INSERT INTO player32 VALUES (201, 0, 'Loren', 'Stokes', '1976-07-17', 30.969999, 18, 17, false, 0.00, 'C');
INSERT INTO player32 VALUES (202, 0, 'Loren', 'Colon', '1971-12-14', 30.139999, 17, 15, false, 0.00, 'SF');
INSERT INTO player32 VALUES (203, 0, 'Shane', 'Sandoval', '1975-09-24', 24.139999, 13, 9, false, 0.00, 'PF');
INSERT INTO player32 VALUES (204, 0, 'Bob', 'Clark', '1972-09-04', 24.02, 15, 10, false, 0.00,'SG');
INSERT INTO player32 VALUES (205, 0, 'Alton', 'Davis', '1970-02-13', 20.34, 16, 9, false, 0.00, 'SG');
INSERT INTO player32 VALUES (206, 0, 'Marion', 'Favis', '1970-02-13', 20.34, 15, 8, false, 0.00, 'PF');
INSERT INTO player32 VALUES (207, 0, 'Harry', 'Davis', '1988-04-14', 9.6899996, 10, 2, true, 0.00, 'PF');
INSERT INTO player32 VALUES (208, 0, 'Roosevelt', 'Davis', '1983-11-10', 8.5500002, 12, 1, true, 0.00, 'PF');

ALTER TABLE position32 ADD CONSTRAINT position32_pkey PRIMARY KEY (code);

ALTER TABLE player32 ADD CONSTRAINT player32_pkey PRIMARY KEY (id);

ALTER TABLE player32 ADD CONSTRAINT player32_position_fkc FOREIGN KEY (position_fk)
	REFERENCES position32 (code);

