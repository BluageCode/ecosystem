/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.service.advancedfeatures.valueobject.uc206_valueobject.voservice;


import javax.inject.Inject;
import javax.inject.Named;

import com.bluage.documentation.business.advancedfeatures.valueobject.uc206_valueobject.bos.Position206BO;
import com.bluage.documentation.business.advancedfeatures.valueobject.uc206_valueobject.vo.Position206VO;
import com.bluage.documentation.service.AbstractService;
import com.netfective.bluage.core.exception.ApplicationException;


/**
 * Defines  application VO services for ServicePosition206ConvertBO2VO
 *
 */
@Named ("com.bluage.documentation.service.advancedfeatures.valueobject.uc206_valueobject.voservice.ServicePosition206ConvertBO2VO")
public class ServicePosition206ConvertBO2VO  extends AbstractService{
			@Inject
		@Named ("com.bluage.documentation.service.advancedfeatures.valueobject.uc206_valueobject.voservice.ServicePosition206Copy") 
		private ServicePosition206Copy instanceServicePosition206Copy;
							

	/**
	 *  Default constructor 
	 */
	public ServicePosition206ConvertBO2VO() {
		super();
	}



			
	
	/**    
	 * VO operation.
	 * @param bo {@link Position206BO}
		 * @return {@link Position206VO}
	 */
	public Position206VO position206Convert(Position206BO bo )
 throws ApplicationException {
return privatePosition206Convert(bo);
	}


	
		/**    
	 * Convert operation .
	 * @param bo {@link Position206BO}
		 * @return {@link Position206VO}
	 */
	private Position206VO privatePosition206Convert(Position206BO bo )
 throws ApplicationException {
// ServicePosition206Copy
		Position206VO vo = instanceServicePosition206Copy.position206CopyBO2VO(bo);
		return vo;		
	}

		}
