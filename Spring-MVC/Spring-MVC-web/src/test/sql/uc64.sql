--<ScriptOptions statementTerminator=";"/>

ALTER TABLE player64 DROP CONSTRAINT player64_team_fkc;

ALTER TABLE audience64 DROP CONSTRAINT audience64_team64_fkc;

ALTER TABLE team64 DROP CONSTRAINT team64_coach_fkc;

ALTER TABLE team64 DROP CONSTRAINT team64_state_fkc;

ALTER TABLE player64 DROP CONSTRAINT player64_pkey;

ALTER TABLE coach64 DROP CONSTRAINT coach64_pkey;

ALTER TABLE state64 DROP CONSTRAINT state64_pkey;

ALTER TABLE audience64 DROP CONSTRAINT audience64_pkey;

ALTER TABLE team64 DROP CONSTRAINT team64_pkey;

DROP TABLE player64;

DROP TABLE state64;

DROP TABLE team64;

DROP TABLE audience64;

DROP TABLE coach64;


CREATE TABLE player64 (
		id INT8 NOT NULL,
		version INT4 NOT NULL,
		first_name VARCHAR(1024),
		last_name VARCHAR(1024),
		date_of_birth DATE,
		estimated_value INT8,
		weight INT4,
		height INT4,
		rookie BOOL,
		team_fk INT8 NOT NULL
	);

CREATE TABLE state64 (
		code VARCHAR(1024) NOT NULL,
		version INT4 NOT NULL,
		name VARCHAR(1024)
	);

CREATE TABLE team64 (
		id INT8 NOT NULL,
		version INT4 NOT NULL,
		city VARCHAR(1024),
		name VARCHAR(1024),
		state_fk VARCHAR(1024) NOT NULL,
		coach_fk INT8 NOT NULL
	);

CREATE TABLE audience64 (
		code VARCHAR(1024) NOT NULL,
		version INT4 NOT NULL,
		name VARCHAR(1024) NOT NULL,
		team64_fk INT8
	);

CREATE TABLE coach64 (
		id INT8 NOT NULL,
		version INT4 NOT NULL,
		name VARCHAR(1024)
	);

INSERT INTO coach64 (id, version, name) VALUES (1, 0, 'Harry');
INSERT INTO coach64 (id, version, name) VALUES (2, 0, 'Loren');
INSERT INTO coach64 (id, version, name) VALUES (3, 0, 'Bob');
INSERT INTO coach64 (id, version, name) VALUES (4, 0, 'Gerald');
INSERT INTO coach64 (id, version, name) VALUES (5, 0, 'Louis');
INSERT INTO coach64 (id, version, name) VALUES (6, 0, 'Cameron');
INSERT INTO coach64 (id, version, name) VALUES (7, 0, 'Shane');
INSERT INTO coach64 (id, version, name) VALUES (8, 0, 'Keith');
INSERT INTO coach64 (id, version, name) VALUES (9, 0, 'Ivan');
INSERT INTO coach64 (id, version, name) VALUES (11, 0, 'Tommy');
INSERT INTO coach64 (id, version, name) VALUES (12, 0, 'Sylvester');
INSERT INTO coach64 (id, version, name) VALUES (13, 0, 'Michael');
INSERT INTO coach64 (id, version, name) VALUES (14, 0, 'Jerome');
INSERT INTO coach64 (id, version, name) VALUES (15, 0, 'Woodrow');

INSERT INTO state64 (code, version, name) VALUES ('CA', 0, 'California');
INSERT INTO state64 (code, version, name) VALUES ('CT', 0, 'Connecticut');
INSERT INTO state64 (code, version, name) VALUES ('DE', 0, 'Delaware');
INSERT INTO state64 (code, version, name) VALUES ('FL', 0, 'Florida');
INSERT INTO state64 (code, version, name) VALUES ('HI', 0, 'Hawaii');
INSERT INTO state64 (code, version, name) VALUES ('IA', 0, 'Iowa');
INSERT INTO state64 (code, version, name) VALUES ('AL', 1, 'Alabama');
INSERT INTO state64 (code, version, name) VALUES ('AZ', 1, 'Arizona');
INSERT INTO state64 (code, version, name) VALUES ('GA', 1, 'Arkansas');
INSERT INTO state64 (code, version, name) VALUES ('AK', 1, 'Alaska');
INSERT INTO state64 (code, version, name) VALUES ('CO', 1, 'Colorado');

INSERT INTO team64 (id, version, city, name, state_fk, coach_fk) VALUES (3, 1, 'Detroit', 'Chargers', 'AZ', 3);
INSERT INTO team64 (id, version, city, name, state_fk, coach_fk) VALUES (10, 0, 'Sacramento', 'Wolverines', 'IA', 8);
INSERT INTO team64 (id, version, city, name, state_fk, coach_fk) VALUES (9, 0, 'Fresno', 'Mustangs', 'HI', 9);
INSERT INTO team64 (id, version, city, name, state_fk, coach_fk) VALUES (8, 0, 'Kansas City', 'Red Devils', 'GA', 11);
INSERT INTO team64 (id, version, city, name, state_fk, coach_fk) VALUES (20, 0, 'Tucson', 'Spartans', 'FL', 12);
INSERT INTO team64 (id, version, city, name, state_fk, coach_fk) VALUES (19, 0, 'Portland', 'Spartans', 'DE', 13);
INSERT INTO team64 (id, version, city, name, state_fk, coach_fk) VALUES (18, 0, 'San Antonio', 'Braves', 'CT', 14);
INSERT INTO team64 (id, version, city, name, state_fk, coach_fk) VALUES (17, 0, 'San Jose', 'Trojans', 'CO', 15);
INSERT INTO team64 (id, version, city, name, state_fk, coach_fk) VALUES (5, 5, 'Omahaff', 'Braves', 'CA', 5);
INSERT INTO team64 (id, version, city, name, state_fk, coach_fk) VALUES (1, 2, 'Charlotte', 'Tigers', 'HI', 1);
INSERT INTO team64 (id, version, city, name, state_fk, coach_fk) VALUES (4, 4, 'Spring', 'Hawks', 'CO', 4);

INSERT INTO audience64 (code, version, name, team64_fk) VALUES ('1', 0, 'Tigers', 1);
INSERT INTO audience64 (code, version, name, team64_fk) VALUES ('3', 0, 'Chargers', 3);
INSERT INTO audience64 (code, version, name, team64_fk) VALUES ('4', 0, 'Hawks', 4);
INSERT INTO audience64 (code, version, name, team64_fk) VALUES ('5', 0, 'Braves', 5);

INSERT INTO player64 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk) VALUES (201, 0, 'Oliver', 'Frank', '1968-10-31', 123, 116, 228, false, 1);
INSERT INTO player64 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk) VALUES (205, 0, 'Harry', 'Davis', '1988-04-14', 6456, 159, 237, false, 3);
INSERT INTO player64 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk) VALUES (235, 0, 'Bob', 'Clark', '1972-09-04', 567, 105, 224, false, 1);
INSERT INTO player64 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk) VALUES (206, 0, 'Ruben', 'Pope', '1985-05-01', 4534, 86, 201, false, 1);
INSERT INTO player64 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk) VALUES (202, 0, 'Ivan', 'Tate', '1972-02-08', 3445, 143, 175, false, 3);
INSERT INTO player64 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk) VALUES (207, 0, 'Tommy', 'Wood', '1992-01-20', 53422, 77, 173, true, 5);
INSERT INTO player64 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk) VALUES (208, 0, 'Sylvester', 'Nash', '1965-07-10', 34267, 68, 184, false, 5);
INSERT INTO player64 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk) VALUES (203, 0, 'Michael', 'Hodges', '1988-04-01', 34543, 158, 204, false, 3);
INSERT INTO player64 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk) VALUES (209, 0, 'Jerome', 'Grant', '1982-06-28', 23435, 105, 205, false, 4);
INSERT INTO player64 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk) VALUES (210, 0, 'Woodrow', 'Carpenter', '1987-08-08', 745654, 69, 224, false, 1);
INSERT INTO player64 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk) VALUES (204, 0, 'Cecil', 'Conner', '1978-06-29', 97887, 160, 170, false, 3);
INSERT INTO player64 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk) VALUES (211, 0, 'Enrique', 'Wilkins', '1967-01-21', 1323, 75, 240, false, 10);
INSERT INTO player64 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk) VALUES (212, 0, 'Christian', 'Chavez', '1983-05-17', 459873, 97, 209, false, 4);
INSERT INTO player64 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk) VALUES (213, 0, 'Simon', 'Klein', '1987-01-21', 6609, 63, 203, false, 20);
INSERT INTO player64 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk) VALUES (214, 0, 'Blake', 'Montgomery', '1992-11-16', 7004, 113, 205, true, 20);
INSERT INTO player64 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk) VALUES (215, 0, 'Troy', 'Rowe', '1973-02-12', 54656, 94, 211, false, 19);
INSERT INTO player64 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk) VALUES (216, 0, 'Eddie', 'Boyd', '1967-10-05', 7657, 118, 232, false, 19);
INSERT INTO player64 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk) VALUES (217, 0, 'Daniel', 'Vargas', '1979-04-21', 8875, 117, 155, false, 18);
INSERT INTO player64 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk) VALUES (218, 0, 'Sam', 'Farmer', '1987-11-29', 7656, 98, 201, false, 18);
INSERT INTO player64 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk) VALUES (219, 0, 'Felipe', 'Olson', '1976-09-28', 4566, 129, 189, false, 17);
INSERT INTO player64 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk) VALUES (220, 0, 'Aaron', 'Mullins', '1972-01-15', 4564, 86, 239, false, 17);
INSERT INTO player64 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk) VALUES (221, 0, 'Bruce', 'Adkins', '1986-05-24', 7664, 150, 209, false, 17);
INSERT INTO player64 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk) VALUES (222, 0, 'Nelson', 'Davis', '1978-05-16', 3245, 119, 171, false, 10);
INSERT INTO player64 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk) VALUES (223, 0, 'Michael', 'Simon', '1975-12-23', 3344, 118, 222, false, 10);
INSERT INTO player64 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk) VALUES (224, 0, 'Marshall', 'Soto', '1977-10-04', 65461, 82, 172, false, 9);
INSERT INTO player64 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk) VALUES (225, 0, 'Vernon', 'Lawson', '1974-09-13', 6434, 146, 208, false, 9);
INSERT INTO player64 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk) VALUES (226, 0, 'Guadalupe', 'Banks', '1986-07-03', 546456, 111, 167, false, 8);
INSERT INTO player64 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk) VALUES (227, 0, 'Gustavo', 'Romero', '1978-02-10', 456546, 130, 167, false, 8);
INSERT INTO player64 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk) VALUES (228, 0, 'Gerald', 'Jimenez', '1970-01-22', 543, 63, 166, false, 5);
INSERT INTO player64 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk) VALUES (229, 0, 'Salvatore', 'Robertson', '1979-12-22', 546546, 156, 156, false, 5);
INSERT INTO player64 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk) VALUES (230, 0, 'Freddie', 'Kelly', '1967-03-22', 6546, 81, 196, false, 5);
INSERT INTO player64 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk) VALUES (231, 0, 'Gary', 'Christensen', '1974-03-13', 8413, 145, 170, false, 5);
INSERT INTO player64 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk) VALUES (232, 0, 'Felipe', 'Lawrence', '1981-04-19', 7686, 150, 219, false, 4);
INSERT INTO player64 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk) VALUES (233, 0, 'Jesus', 'Jimenez', '1985-09-01', 5642, 133, 159, false, 4);
INSERT INTO player64 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk) VALUES (234, 0, 'Ralph', 'Castillo', '1975-07-29', 546543, 115, 229, false, 4);


	
ALTER TABLE player64 ADD CONSTRAINT player64_pkey PRIMARY KEY (id);

ALTER TABLE coach64 ADD CONSTRAINT coach64_pkey PRIMARY KEY (id);

ALTER TABLE state64 ADD CONSTRAINT state64_pkey PRIMARY KEY (code);

ALTER TABLE audience64 ADD CONSTRAINT audience64_pkey PRIMARY KEY (code);

ALTER TABLE team64 ADD CONSTRAINT team64_pkey PRIMARY KEY (id);

ALTER TABLE player64 ADD CONSTRAINT player64_team_fkc FOREIGN KEY (team_fk)
	REFERENCES team64 (id);

ALTER TABLE audience64 ADD CONSTRAINT audience64_team64_fkc FOREIGN KEY (team64_fk)
	REFERENCES team64 (id);

ALTER TABLE team64 ADD CONSTRAINT team64_coach_fkc FOREIGN KEY (coach_fk)
	REFERENCES coach64 (id);

ALTER TABLE team64 ADD CONSTRAINT team64_state_fkc FOREIGN KEY (state_fk)
	REFERENCES state64 (code);

