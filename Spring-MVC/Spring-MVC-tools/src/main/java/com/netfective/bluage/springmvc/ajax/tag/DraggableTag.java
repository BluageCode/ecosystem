/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.netfective.bluage.springmvc.ajax.tag;

import java.io.IOException;
import java.text.MessageFormat;

import javax.servlet.jsp.JspException;

import com.netfective.bluage.springmvc.ajax.tag.html.HtmlDivTag;
import com.netfective.bluage.springmvc.ajax.util.Constants;
import com.netfective.bluage.springmvc.ajax.util.RequestUtils;


/**
 * Class DraggableTag
 * @superclass HtmlDivTag
 * @interface Constants
 * @author NETFECTIVE TECHNOLOGY
 */
public class DraggableTag extends HtmlDivTag implements Constants {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1655701117950379545L;

	/**
	* GHOSTING_DEFAULT_VALUE is a boolean constant
	*/
	private static final boolean GHOSTING_DEFAULT_VALUE = true;

	/**
	* REVERT_DEFAULT_VALUE is a boolean constant
	*/
	private static final boolean REVERT_DEFAULT_VALUE = true;
	
	/**
	* Instance name
	*/
	private String dragName;

	/**
	* Instance property
	*/
	private String dragProperty;

	/**
	* Scope value
	*/
	private String scope;

	/**
	* Name of the group of DraggableTag where is this instance
	*/
	private String dragGroup;

	/**
	* Ghosting script option
	*/
	private boolean ghosting = GHOSTING_DEFAULT_VALUE;

	/**
	* Revert script option
	*/
	private boolean revert = REVERT_DEFAULT_VALUE;

	/**
	* This method prints a end tag on output of <code>pageContext</code>
	* @return int
	*/
	public int doEndTag() throws JspException {
		
		super.doEndTag();
		final Object[] arguments = { "'" + TagUtils.getRealDivId(id) + "'", buildScriptOptions() };
		final String script = MessageFormat.format(CREATE_DRAGGABLE_SCRIPT, arguments);
		
		try {				
			pageContext.getOut().println(TagUtils.buildScript(script));
		} catch (IOException e) {
			throw new JspException(e);
		}
		return EVAL_PAGE;
	}

	/**
	* This method give all script options
	 * @return Object
	 * @throws JspException
	 */
	private Object buildScriptOptions() throws JspException {
		final StringBuilder strbuff = new StringBuilder();
		strbuff.append("{");
		Object value = RequestUtils.lookup(pageContext, dragName, dragProperty,
				null);
		if (value == null){
			value = "";
		}
		strbuff.append("dragValue:'");
		strbuff.append(value.toString());
		strbuff.append("'");
		strbuff.append(", ");

		if (dragGroup != null && dragGroup.trim().length() > 0) {
			strbuff.append("dragGroup:'");
			strbuff.append(dragGroup);
			strbuff.append("'");
			strbuff.append(", ");
		}

		strbuff.append("revert:'");
		strbuff.append(revert);
		strbuff.append("'");
		strbuff.append(", ");

		strbuff.append("ghosting:'");
		strbuff.append(ghosting);
		strbuff.append("'");
		strbuff.append(" ");

		strbuff.append("}");

		return strbuff.toString();
	}

	// ------------------------------------------------------------------------Getter_Setter
	/**
	* Getter for dragGroup
	* @return String
	*/
	public String getDragGroup() {
		return dragGroup;
	}

	/**
	* Setter for dragGroup
	* @param String
	*/
	public void setDragGroup(final String dragGroup) {
		this.dragGroup = dragGroup;
	}

	/**
	* Getter for ghosting
	* @return boolean
	*/
	public boolean isGhosting() {
		return ghosting;
	}

	/**
	* Setter for ghosting
	* @param boolean
	*/
	public void setGhosting(final boolean ghosting) {
		this.ghosting = ghosting;
	}

	/**
	* Getter for revert
	* @return boolean
	*/
	public boolean isRevert() {
		return revert;
	}

	/**
	* Setter for revert
	* @param boolean
	*/
	public void setRevert(final boolean revert) {
		this.revert = revert;
	}

	/**
	* Getter for scope
	* @return String
	*/
	public String getScope() {
		return scope;
	}

	/**
	* Setter for scope
	* @param String
	*/
	public void setScope(final String scope) {
		this.scope = scope;
	}

	/**
	* Getter for dragName
	* @return String
	*/
	public String getDragName() {
		return dragName;
	}

	/**
	* Setter for dragName
	* @param String
	*/
	public void setDragName(final String dragName) {
		this.dragName = dragName;
	}

	/**
	* Getter for dragProperty
	* @return String
	*/
	public String getDragProperty() {
		return dragProperty;
	}

	/**
	* Setter for dragProperty
	* @param String
	*/
	public void setDragProperty(final String dragProperty) {
		this.dragProperty = dragProperty;
	}

}
