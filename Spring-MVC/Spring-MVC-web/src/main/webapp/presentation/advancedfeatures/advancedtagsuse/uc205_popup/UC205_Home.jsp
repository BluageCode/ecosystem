<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file="/include.jsp"  %>
<html >
<head >
	<link href="<%=request.getContextPath()%>/inc/popup/css/modal.css" rel="stylesheet" />
	<link href="<%=request.getContextPath()%>/inc/popup/css/dhtmlwindow.css" rel="stylesheet" />
	<script language="javascript">	var popup_path = "<%=request.getContextPath()%>/inc/popup/"; </script>
	<script src="<%=request.getContextPath()%>/inc/popup/js/dhtmlwindow.js"></script>
	<script src="<%=request.getContextPath()%>/inc/popup/js/modal.js"></script>
<title  > 
<fmt:message key="cUC205_Home_BLU_AGE_DOCUMENTATION"/>
</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"  > 
</meta>
<link href="<c:url value= "/presentation/_style/_style.css"/>" rel="stylesheet" type="text/css"  /> 
<link href="<c:url value= "/presentation/_include/jquery/rd_lib.css"/>" rel="stylesheet" type="text/css"  /> 
<script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/jquery.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/rd_setup.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/rd_lib.js"/>" space="preserve"  ></script><link rel="shortcut icon" href="<c:url value= "/presentation/_style/favicon.ico"/>"  /> 

</head>

<body class=" " > 
<div id="rd_ghost"  > 
<small  > 
<fmt:message key="cUC205_Home___CSS_DEACTIVATED"/>
</small>
</div>
<div id="rd_jghost"  > 
<small  > 
<fmt:message key="cUC205_Home___JAVASCRIPT_DEACTIVATED"/>
</small>
</div>
<div id="shape"  > 
<div class="ui-layout-center"  > 
<div class="east-north"  > 
<h2 class="rd_first"  > 
<fmt:message key="cUC205_Home_ADVANCED_FEATURES__ADVANCED_T"/>
</h2>
<tiles:insertDefinition	name="presentation/advancedfeatures/advancedtagsuse/uc205_popup/UC205_Homepopup_component" />
	<form:form id="uC205_Home" commandName="uC205_HomeForm"  enctype="application/x-www-form-urlencoded" method="post">
<br clear="none"  /> 
<a id="lnk_display"											href="<c:url value="/presentation/advancedfeatures/advancedtagsuse/uc205_popup/UC205_Home.html"/>?popupId=cp_traitement"
													 shape="rect"  >									 
<fmt:message key="cUC205_Home_DISPLAY_PLAYERS"/>
</a>
	</form:form>
	<script language="javascript">
		function btn_click_cExecuteUC205_Home(url) {
			UC205_Homevar = document.getElementById('uC205_Home');
			UC205_Homevar.setAttribute('action', url);
			UC205_Homevar.submit();
		}
		function btn_click_cSetActionUC205_Home(url) {
			UC205_Homevar = document.getElementById('uC205_Home');
			UC205_Homevar.setAttribute('action', url);
		}
	</script>
</div>
<div class="east-center"  > 
<tiles:insertDefinition	name="presentation/advancedfeatures/advancedtagsuse/uc205_popup/UC205_Homecmp_tabs" />
</div>
</div>
<div class="ui-layout-north"  > 
<tiles:insertDefinition	name="presentation/advancedfeatures/advancedtagsuse/uc205_popup/UC205_Homecmp_header" />
</div>
<div class="ui-layout-south"  > 
<fmt:message key="cUC205_Home___"/>
<span id="rd_jversion"  > 
<fmt:message key="cUC205_Home_BLU_AGE_CORPORATION"/>
</span>
</div>
<div class="ui-layout-west"  > 
<tiles:insertDefinition	name="presentation/advancedfeatures/advancedtagsuse/uc205_popup/UC205_Homecmp_treeview" />
</div>
</div>
</body>
</html>
