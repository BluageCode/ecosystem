/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.application.navigation.uc31_preaction;


import java.io.Serializable;
import java.util.List;

import com.bluage.documentation.business.standardfeatures.application.navigation.uc31_preaction.bos.Player31BO;
import com.netfective.bluage.core.web.AbstractForm;
import com.netfective.bluage.core.web.IForm;

/**
* Class : UC31_DisplayPlayersForm
*/
public class UC31_DisplayPlayersForm extends AbstractForm implements Serializable, IForm {
	
	/** Constant : serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	//CONSTANT : playersList
	private static final String PLAYERS_LIST = "playersList";
	/**
	 * 	Property: playersList 
	 */
	private List<Player31BO> playersList;
/**
	 * Default constructor : UC31_DisplayPlayersForm
	 */
	public UC31_DisplayPlayersForm() {
		super();
		// Initialize : playersList
		this.playersList = new java.util.ArrayList<com.bluage.documentation.business.standardfeatures.application.navigation.uc31_preaction.bos.Player31BO>();
					this.setAlwaysCallPreControllers(true);
	}
	
	/**
	 * 	Getter : playersList 
	 * 	@return : Return the playersList instance.
	 */
	public List<Player31BO> getPlayersList(){
		return playersList; // For UC31_DisplayPlayersForm
	}
	
	/**
	 * 	Setter : playersList 
	 *  @param playersListinstance : The instance to set.
	 */
	public void setPlayersList(final List<Player31BO> playersListinstance){
		this.playersList = playersListinstance;// For UC31_DisplayPlayersForm
	}
/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UC31_DisplayPlayersForm [ "+
PLAYERS_LIST +" = " + playersList + "]";
	}
	
	/**
	 * Method setInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public void setInstanceByInstanceName(final String instanceName, final Object instance) {
		// Set the instance PlayersList.
		if(PLAYERS_LIST.equals(instanceName)){// For UC31_DisplayPlayersForm
			this.setPlayersList((List<Player31BO>)instance); // For UC31_DisplayPlayersForm
		}
			}
	
	/**
	 * Method getInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public Object getInstanceByInstanceName(final String instanceName){
		// Temporary object for tmpUC31_DisplayPlayersForm.
		Object tmpUC31_DisplayPlayersForm = null;
		
			
		// Get the instance PlayersList for UC31_DisplayPlayersForm.
		if(PLAYERS_LIST.equals(instanceName)){ // For UC31_DisplayPlayersForm
			tmpUC31_DisplayPlayersForm = this.getPlayersList(); // For UC31_DisplayPlayersForm
		}
		return tmpUC31_DisplayPlayersForm;// For UC31_DisplayPlayersForm
	}
	
			}
