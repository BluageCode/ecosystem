-- uc204 - creation

ALTER TABLE player204 DROP CONSTRAINT player204_position_fkc;

ALTER TABLE player204 DROP CONSTRAINT player204_league_fkc;

ALTER TABLE team204 DROP CONSTRAINT team204_coach_fkc;

ALTER TABLE player204 DROP CONSTRAINT player204_team_fkc;

ALTER TABLE position204 DROP CONSTRAINT position204_pkey;

ALTER TABLE team204 DROP CONSTRAINT team204_pkey;

ALTER TABLE coach204 DROP CONSTRAINT coach204_pkey;

ALTER TABLE player204 DROP CONSTRAINT player204_pkey;

ALTER TABLE league204 DROP CONSTRAINT league204_pkey;

DROP TABLE position204;

DROP TABLE player204;

DROP TABLE team204;

DROP TABLE coach204;

DROP TABLE league204;




CREATE TABLE coach204 (
	id bigint NOT NULL,
	version integer NOT NULL,
	first_name character varying(30),
	last_name character varying(30)
);



ALTER TABLE ONLY coach204
    ADD CONSTRAINT coach204_pkey PRIMARY KEY (id);

CREATE TABLE player204 (
	id bigint NOT NULL,
	version integer NOT NULL,
	first_name character varying(30),
	last_name character varying(30),
	date_of_birth date,
	estimated_value real,
	weight integer, 
	height integer, 
	rookie boolean, 
	team_fk bigint,
	position_fk character varying(2) NOT NULL,
    league_fk character varying(10) NOT NULL
);



ALTER TABLE ONLY player204
    ADD CONSTRAINT player204_pkey PRIMARY KEY (id);
    
CREATE TABLE position204 (
    code character varying(2) NOT NULL,
    version integer NOT NULL,
    name character varying(20) NOT NULL
);



ALTER TABLE ONLY position204
    ADD CONSTRAINT position204_pkey PRIMARY KEY (code);
    
CREATE TABLE team204 (
    id bigint NOT NULL,
    version integer NOT NULL,
    city character varying(30),
    name character varying(30) UNIQUE, 
    coach_fk bigint NOT NULL
);



ALTER TABLE ONLY team204
    ADD CONSTRAINT team204_pkey PRIMARY KEY (id);

CREATE TABLE league204 (
    code character varying(10) NOT NULL,
    version integer NOT NULL,
    professional boolean NOT NULL
);


INSERT INTO coach204 VALUES (1, 0, 'Harry', 'Davis');
INSERT INTO coach204 VALUES (2, 0, 'Loren', 'Colon');
INSERT INTO coach204 VALUES (3, 0, 'Bob', 'Clark');
INSERT INTO coach204 VALUES (4, 0, 'Gerald', 'Fox');
INSERT INTO coach204 VALUES (5, 0, 'Louis', 'Stokes');
INSERT INTO coach204 VALUES (6, 0, 'Cameron', 'Horton');
INSERT INTO coach204 VALUES (7, 0, 'Shane', 'Sandoval');
INSERT INTO coach204 VALUES (8, 0, 'Keith', 'Barnett');
INSERT INTO coach204 VALUES (9, 0, 'Ivan', 'Tate');
INSERT INTO coach204 VALUES (11, 0, 'Tommy', 'Wood');
INSERT INTO coach204 VALUES (12, 0, 'Sylvester', 'Nash');
INSERT INTO coach204 VALUES (13, 0, 'Michael', 'Hodges');
INSERT INTO coach204 VALUES (14, 0, 'Jerome', 'Grant');
INSERT INTO coach204 VALUES (15, 0, 'Woodrow', 'Carpenter');
    
INSERT INTO team204 VALUES (1, 0, 'Charlotte', 'Tigers', 1);
INSERT INTO team204 VALUES (2, 0, 'Kansas City', 'Bears', 2);
INSERT INTO team204 VALUES (3, 0, 'Detroit', 'Chargers', 3);
INSERT INTO team204 VALUES (4, 0, 'Detroit', 'Hawks', 4);
INSERT INTO team204 VALUES (5, 0, 'Omaha', 'Braves', 5);
INSERT INTO team204 VALUES (6, 0, 'Raleigh', 'Eagles', 6);
INSERT INTO team204 VALUES (7, 0, 'Oakland', 'Cardinals', 7);
INSERT INTO team204 VALUES (8, 0, 'Kansas City', 'Red Devils', 8);
INSERT INTO team204 VALUES (9, 0, 'Fresno', 'Mustangs', 9);

INSERT INTO position204 VALUES ('PG', 0, 'Point Guard');
INSERT INTO position204 VALUES ('SG', 0, 'Shooting Guard');
INSERT INTO position204 VALUES ('SF', 0, 'Small Forward');
INSERT INTO position204 VALUES ('PF', 0, 'Power Forward');
INSERT INTO position204 VALUES ('C', 0, 'Center');

INSERT INTO league204 VALUES ('NBA', 0, true);
INSERT INTO league204 VALUES ('IBL', 0, true);
INSERT INTO league204 VALUES ('PBL', 0, true);
INSERT INTO league204 VALUES ('ABA', 0, false);
INSERT INTO league204 VALUES ('EBA', 0, false);
INSERT INTO league204 VALUES ('WCBL', 0, false);

INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (201, 0, 'Oliver', 'Frank', '1968-10-31', 0.11, 116, 228, false, null, 'PG', 'NBA');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (202, 0, 'Elbert', 'Cole', '1965-04-07', 3.95, 100, 171, false, null, 'SG', 'NBA');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (203, 0, 'Abel', 'Dennis', '1980-04-16', 30.51, 105, 208, false, null, 'SF', 'NBA');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (204, 0, 'Ricky', 'Dawson', '1990-03-03', 29.299999, 115, 168, true, null, 'C', 'NBA');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (205, 0, 'Earl', 'Russell', '1981-12-15', 44.73, 60, 234, false, null, 'PF', 'NBA');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (206, 0, 'Nick', 'Garner', '1992-07-27', 47.84, 87, 221, true, null, 'PG', 'NBA');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (207, 0, 'Howard', 'Ramos', '1970-08-25', 10.76, 107, 199, false, 1, 'SG', 'NBA');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (208, 0, 'Allan', 'Meyer', '1969-02-14', 33.669998, 114, 229, false, 1, 'SF', 'NBA');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (209, 0, 'Ruben', 'Pope', '1985-05-01', 44.66, 86, 201, false, 1, 'C', 'NBA');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (210, 0, 'Lester', 'Cross', '1976-02-21', 30.98, 69, 214, false, 1, 'PF', 'NBA');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (218, 0, 'Roderick', 'Robbins', '1975-07-29', 12.04, 115, 164, false, 1, 'SF', 'NBA');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (219, 0, 'Christian', 'Chavez', '1983-05-17', 37.880001, 97, 209, false, 2, 'C', 'IBL');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (220, 0, 'Cecil', 'Conner', '1978-06-29', 39.709999, 160, 170, false, 2, 'PF', 'IBL');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (221, 0, 'Stephen', 'Lambert', '1977-03-14', 18.9, 107, 153, false, 2, 'PG', 'IBL');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (222, 0, 'Gustavo', 'Romero', '1978-02-10', 5.0300002, 130, 167, false, 2, 'SG', 'IBL');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (223, 0, 'Salvatore', 'Robertson', '1979-12-22', 36.169998, 156, 156, false, 2, 'SF', 'IBL');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (224, 0, 'Guadalupe', 'Banks', '1986-07-03', 30.209999, 111, 167, false, null, 'C', 'IBL');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (225, 0, 'Todd', 'Gilbert', '1976-03-13', 0.41999999, 115, 211, false, null, 'PF', 'IBL');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (226, 0, 'Ralph', 'Castillo', '1975-07-29', 36.130001, 115, 229, false, null, 'PG', 'IBL');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (227, 0, 'Gerardo', 'George', '1982-03-17', 11.32, 151, 159, false, null, 'SG', 'IBL');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (228, 0, 'Vernon', 'Lawson', '1974-09-13', 22.48, 146, 208, false, null, 'SF', 'IBL');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (229, 0, 'Marshall', 'Soto', '1977-10-04', 35.639999, 82, 172, false, null, 'C', 'IBL');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (230, 0, 'Jim', 'Riley', '1971-10-20', 43.98, 104, 171, false, null, 'PF', 'IBL');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (231, 0, 'Matthew', 'Fitzgerald', '1982-08-09', 33.610001, 145, 230, false, 3, 'PG', 'IBL');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (232, 0, 'Stuart', 'Ford', '1983-09-22', 2.96, 86, 174, false, 3, 'SG', 'IBL');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (233, 0, 'Timmy', 'Miller', '1972-08-07', 35.169998, 89, 222, false, 3, 'SF', 'IBL');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (234, 0, 'Antonio', 'Steele', '1984-03-05', 41.650002, 62, 233, false, 3, 'C', 'IBL');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (236, 0, 'Jordan', 'Hayes', '1989-07-11', 44.419998, 113, 203, false, 3, 'PF', 'IBL');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (237, 0, 'Milton', 'White', '1970-12-20', 25.4, 132, 159, false, 3, 'PG', 'IBL');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (239, 0, 'Ralph', 'Crawford', '1974-10-06', 38.43, 106, 173, false, 3, 'SG', 'IBL');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (241, 0, 'Timmy', 'Carson', '1966-09-13', 13.62, 142, 176, false, 4, 'SF', 'PBL');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (242, 0, 'Alfonso', 'Harrison', '1981-07-28', 47.43, 147, 200, false, 4, 'C', 'PBL');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (243, 0, 'Salvador', 'Watson', '1984-10-02', 49.900002, 79, 173, false, 4, 'PF', 'PBL');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (244, 0, 'Nelson', 'Cook', '1966-03-01', 34.950001, 129, 186, false, 4, 'PG', 'PBL');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (245, 0, 'Vernon', 'Davis', '1971-10-19', 2.24, 108, 206, false, 4, 'PG', 'PBL');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (246, 0, 'Earnest', 'Maxwell', '1986-01-20', 42.16, 135, 238, false, 4, 'SG', 'PBL');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (247, 0, 'Dwayne', 'Stevenson', '1968-10-26', 43.02, 101, 226, false, 4, 'SF', 'PBL');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (248, 0, 'Archie', 'Roberson', '1974-08-11', 7.1500001, 101, 152, false, 4, 'C', 'PBL');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (249, 0, 'Darryl', 'Norris', '1965-01-22', 47.700001, 121, 202, false, null, 'PF', 'PBL');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (250, 0, 'Dean', 'Stokes', '1974-10-15', 49.09, 150, 166, false, null, 'PG', 'PBL');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (251, 0, 'Allen', 'Freeman', '1969-09-03', 33.279999, 99, 201, false, null, 'SG', 'PBL');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (252, 0, 'Brett', 'Weaver', '1971-10-04', 14.83, 75, 234, false, null, 'SF', 'PBL');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (253, 0, 'Jerome', 'Griffin', '1987-12-19', 9.1499996, 146, 229, false, null, 'C', 'PBL');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (254, 0, 'John', 'Lynch', '1980-09-06', 28.35, 123, 201, false, null, 'PF', 'PBL');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (256, 0, 'Jody', 'Elliott', '1972-05-07', 45.290001, 66, 220, false, null, 'PG', 'PBL');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (257, 0, 'Willis', 'Francis', '1977-06-21', 23.91, 77, 223, false, 4, 'SG', 'PBL');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (259, 0, 'Stanley', 'Wolfe', '1985-12-14', 0.50999999, 129, 211, false, 4, 'SF', 'PBL');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (260, 0, 'Don', 'Griffith', '1975-10-31', 24.25, 101, 212, false, null, 'C', 'PBL');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (262, 0, 'Edwin', 'Reese', '1989-06-20', 10.7, 103, 184, false, null, 'PF', 'PBL');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (263, 0, 'Salvador', 'Bryant', '1965-08-24', 35.830002, 106, 216, false, null, 'PG', 'PBL');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (265, 0, 'Pablo', 'Rhodes', '1982-06-17', 31.6, 108, 201, false, null, 'SG', 'PBL');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (266, 0, 'Dana', 'Hamilton', '1983-05-30', 33.599998, 97, 175, false, null, 'SF', 'PBL');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (267, 0, 'Cecil', 'Wells', '1989-12-26', 38.610001, 122, 189, false, null, 'C', 'PBL');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (268, 0, 'Earl', 'Barrett', '1982-09-15', 45.34, 138, 208, false, null, 'PF', 'NBA');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (269, 0, 'Paul', 'Page', '1969-12-13', 25.809999, 125, 193, false, null, 'PG', 'NBA');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (270, 0, 'Freddie', 'Williamson', '1976-10-10', 3.8, 105, 218, false, null, 'SG', 'NBA');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (271, 0, 'Gilbert', 'Johnston', '1981-12-30', 40.259998, 61, 178, false, null, 'SF', 'NBA');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (272, 0, 'Darnell', 'Williamson', '1978-06-15', 45.080002, 157, 190, false, 1, 'C', 'NBA');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (273, 0, 'Joshua', 'Hardy', '1975-04-09', 9.6300001, 150, 172, false, 1, 'PF', 'NBA');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (274, 0, 'Dale', 'Colon', '1984-09-28', 9.25, 68, 160, false, null, 'PG', 'NBA');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (275, 0, 'Clarence', 'Johnson', '1972-05-25', 2.02, 128, 187, false, 1, 'SG', 'NBA');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (276, 0, 'Rolando', 'Waters', '1966-02-26', 7.0999999, 67, 201, false, null, 'SF', 'NBA');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (277, 0, 'Dominic', 'Martinez', '1967-03-04', 1.34, 127, 203, false, 1, 'C', 'NBA');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (278, 0, 'Ricardo', 'Oliver', '1968-05-02', 20.5, 147, 152, false, 5, 'PF', 'NBA');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (279, 0, 'Edward', 'Hogan', '1986-02-02', 23.24, 75, 158, false, 5, 'PG', 'NBA');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (280, 0, 'Gerald', 'Sanders', '1976-03-03', 21.68, 127, 207, false, 5, 'SG', 'NBA');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (281, 0, 'Noel', 'Oliver', '1984-11-24', 45.18, 112, 160, false, 5, 'SF', 'NBA');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (282, 0, 'Preston', 'Medina', '1974-02-15', 0.33000001, 95, 213, false, 5, 'C', 'NBA');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (283, 0, 'Abraham', 'Drake', '1985-11-27', 21.77, 94, 207, false, 5, 'PF', 'NBA');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (284, 0, 'Julian', 'Gibbs', '1985-07-01', 23.34, 80, 163, false, 5, 'PG', 'NBA');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (285, 0, 'Juan', 'Dawson', '1987-01-14', 9.9799995, 75, 204, false, 5, 'SG', 'NBA');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (286, 0, 'Marvin', 'Mitchell', '1981-05-27', 7.9400001, 91, 210, false, 5, 'SF', 'NBA');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (287, 0, 'Owen', 'Little', '1981-10-27', 45.869999, 135, 183, false, 5, 'C', 'NBA');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (288, 0, 'Irvin', 'Lee', '1988-02-25', 10.61, 88, 157, false, 5, 'PF', 'NBA');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (289, 0, 'Wayne', 'Rose', '1981-10-28', 20.41, 115, 238, false, 5, 'PG', 'NBA');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (290, 0, 'Lucas', 'Williamson', '1983-06-29', 38.360001, 73, 151, false, 5, 'SG', 'NBA');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (291, 0, 'Walter', 'Reyes', '1984-12-09', 46.150002, 104, 193, false, 6, 'SF', 'ABA');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (292, 0, 'Kirk', 'Cox', '1973-05-10', 26.58, 60, 179, false, 6, 'C', 'ABA');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (293, 0, 'Arthur', 'Payne', '1981-11-20', 30.860001, 66, 190, false, 6, 'PF', 'ABA');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (294, 0, 'Leroy', 'Moore', '1987-07-26', 45.799999, 89, 183, false, 6, 'PG', 'ABA');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (295, 0, 'Milton', 'Johnson', '1969-07-02', 22.219999, 68, 205, false, 6, 'SG', 'ABA');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (296, 0, 'Sammy', 'Mendez', '1986-07-01', 2.27, 96, 234, false, 6, 'SF', 'ABA');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (297, 0, 'Rafael', 'Steele', '1968-11-25', 15.48, 66, 153, false, 6, 'C', 'ABA');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (298, 0, 'Bob', 'Jacobs', '1988-05-21', 13.23, 72, 172, false, 6, 'PF', 'ABA');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (299, 0, 'Jared', 'Stevenson', '1974-05-03', 12.11, 125, 187, false, null, 'PG', 'ABA');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (300, 0, 'Shannon', 'Barton', '1980-03-10', 23.809999, 77, 197, false, null, 'SG', 'ABA');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (301, 0, 'Erick', 'Mccarthy', '1970-12-07', 3.76, 75, 150, false, null, 'SF', 'ABA');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (302, 0, 'Justin', 'Johnson', '1988-02-27', 47.950001, 137, 161, false, null, 'C', 'ABA');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (303, 0, 'Elmer', 'Hansen', '1984-07-12', 12.86, 88, 213, false, null, 'PF', 'ABA');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (304, 0, 'Everett', 'Sherman', '1978-11-03', 23.280001, 118, 213, false, null, 'PG', 'ABA');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (305, 0, 'Rafael', 'Ward', '1967-02-08', 38.77, 91, 178, false, null, 'SG', 'ABA');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (306, 0, 'Kenny', 'Rogers', '1965-02-02', 5.4400001, 68, 153, false, null, 'SF', 'ABA');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (308, 0, 'Lonnie', 'Brady', '1987-12-11', 11.83, 77, 219, false, null, 'C', 'ABA');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (309, 0, 'Sergio', 'Taylor', '1984-08-29', 25.290001, 133, 225, false, null, 'PF', 'ABA');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (310, 0, 'Byron', 'Morton', '1981-03-28', 21.4, 154, 196, false, null, 'PG', 'ABA');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (311, 0, 'Bob', 'Estrada', '1975-10-20', 39.59, 98, 197, false, null, 'SG', 'ABA');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (312, 0, 'Frank', 'Strickland', '1970-06-27', 21.879999, 116, 234, false, null, 'SF', 'ABA');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (313, 0, 'Johnathan', 'Norman', '1978-08-20', 17.620001, 117, 214, false, 7, 'C', 'EBA');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (314, 0, 'Leonard', 'Stephens', '1965-12-02', 13.12, 130, 168, false, 7, 'PF', 'EBA');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (315, 0, 'Tim', 'Taylor', '1985-01-26', 8.1599998, 146, 167, false, 7, 'PG', 'EBA');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (316, 0, 'Bobby', 'Weaver', '1979-11-25', 3.1099999, 99, 227, false, 7, 'SG', 'EBA');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (318, 0, 'Bernard', 'Wise', '1970-02-01', 48.48, 66, 219, false, 7, 'SF', 'EBA');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (319, 0, 'Rodolfo', 'Wong', '1968-08-10', 26.969999, 84, 191, false, null, 'C', 'EBA');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (320, 0, 'Wayne', 'Clarke', '1965-07-24', 24.9, 114, 154, false, null, 'PF', 'EBA');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (321, 0, 'Saul', 'Zimmerman', '1988-12-28', 45.82, 149, 201, false, null, 'PG', 'EBA');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (322, 0, 'Floyd', 'Hampton', '1988-03-03', 21.67, 115, 178, false, null, 'SG', 'EBA');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (323, 0, 'Timmy', 'Walker', '1969-04-14', 45.91, 149, 190, false, null, 'SF', 'EBA');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (324, 0, 'Jon', 'Hines', '1980-12-27', 24.879999, 68, 155, false, null, 'C', 'EBA');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (325, 0, 'Orlando', 'Burns', '1973-09-02', 19.139999, 159, 194, false, 7, 'PF', 'EBA');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (326, 0, 'Randal', 'Mendez', '1974-03-04', 21.200001, 141, 175, false, 7, 'PG', 'EBA');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (327, 0, 'Jeremy', 'Holmes', '1983-12-08', 38.299999, 118, 234, false, 7, 'SG', 'EBA');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (330, 0, 'Shannon', 'Watkins', '1979-02-07', 25.66, 69, 168, false, 7, 'SF', 'EBA');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (331, 0, 'Ralph', 'Burton', '1969-11-13', 7.52, 85, 175, false, 7, 'C', 'EBA');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (332, 0, 'Bobby', 'Jefferson', '1985-10-06', 17.4, 128, 240, false, null, 'PF', 'EBA');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (333, 0, 'Travis', 'Williamson', '1974-08-23', 20.299999, 115, 154, false, null, 'PG', 'EBA');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (334, 0, 'Matthew', 'Haynes', '1982-07-16', 18.91, 102, 218, false, null, 'SG', 'EBA');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (335, 0, 'Clayton', 'Doyle', '1976-12-10', 36.240002, 127, 177, false, null, 'SF', 'EBA');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (337, 0, 'Bradford', 'Porter', '1977-06-02', 16.190001, 111, 234, false, 8, 'C', 'WCBL');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (339, 0, 'Bernard', 'Roy', '1987-05-07', 25.15, 65, 172, false, 8, 'PF', 'WCBL');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (340, 0, 'Abraham', 'Page', '1978-03-20', 19.24, 125, 213, false, 8, 'PG', 'WCBL');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (341, 0, 'Nathan', 'Banks', '1978-10-10', 7.1500001, 121, 161, false, 8, 'SG', 'WCBL');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (343, 0, 'Marvin', 'Davis', '1980-06-13', 11, 123, 205, false, 8, 'SF', 'WCBL');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (344, 0, 'Alberto', 'Dunn', '1969-11-23', 15.04, 127, 186, false, 8, 'C', 'WCBL');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (345, 0, 'Kim', 'Hammond', '1979-07-31', 0.22, 130, 155, false, null, 'PF', 'WCBL');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (346, 0, 'Sylvester', 'Rowe', '1970-12-16', 34.459999, 94, 164, false, 8, 'PG', 'WCBL');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (347, 0, 'Wallace', 'Wilkins', '1984-02-10', 12.66, 97, 182, false, 8, 'SG', 'WCBL');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (348, 0, 'Carroll', 'Pena', '1974-08-27', 40.279999, 153, 153, false, 8, 'SF', 'WCBL');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (349, 0, 'Gerard', 'Austin', '1983-09-10', 17.07, 130, 237, false, 8, 'C', 'WCBL');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (350, 0, 'Ed', 'Delgado', '1976-02-02', 4.4299998, 77, 152, false, 8, 'PF', 'WCBL');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (351, 0, 'Wallace', 'Perez', '1974-04-12', 31.299999, 81, 238, false, null, 'PG', 'WCBL');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (352, 0, 'Tom', 'Henry', '1981-01-11', 49.849998, 68, 158, false, null, 'SG', 'WCBL');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (353, 0, 'Jesus', 'Lloyd', '1988-11-11', 16.9, 122, 223, false, null, 'SF', 'WCBL');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (354, 0, 'Joshua', 'Bailey', '1980-03-25', 41.709999, 155, 240, false, null, 'C', 'WCBL');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (355, 0, 'Kyle', 'Cunningham', '1965-12-10', 24.57, 151, 200, false, null, 'PF', 'WCBL');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (356, 0, 'Spencer', 'Ward', '1971-11-13', 35.48, 119, 235, false, null, 'PG', 'WCBL');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (358, 0, 'Matthew', 'Baker', '1985-09-16', 4.25, 85, 188, false, null, 'SG', 'WCBL');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (359, 0, 'Louis', 'Gonzales', '1988-04-08', 35.790001, 121, 215, false, null, 'SF', 'WCBL');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (360, 0, 'Derrick', 'Mullins', '1984-06-16', 40.549999, 156, 199, false, null, 'C', 'WCBL');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (361, 0, 'Gordon', 'Greene', '1970-05-07', 17.450001, 144, 206, false, null, 'PF', 'WCBL');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (363, 0, 'Darren', 'Cortez', '1983-03-19', 37.66, 92, 180, false, null, 'PG', 'WCBL');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (364, 0, 'Courtney', 'Perry', '1969-09-16', 4.98, 94, 162, false, 2, 'SG', 'IBL');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (365, 0, 'Bert', 'Wheeler', '1977-11-27', 1.15, 132, 206, false, 2, 'SF', 'IBL');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (366, 0, 'Corey', 'Herrera', '1977-06-25', 45.860001, 110, 190, false, 2, 'C', 'IBL');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (367, 0, 'Myron', 'Atkins', '1973-10-20', 45.389999, 129, 199, false, null, 'PF', 'IBL');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (368, 0, 'Joseph', 'Cunningham', '1975-11-22', 0.31999999, 84, 158, false, null, 'PG', 'IBL');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (369, 0, 'Sidney', 'Phelps', '1965-05-05', 35.439999, 117, 238, false, null, 'SG', 'IBL');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (371, 0, 'Pete', 'Daniel', '1968-03-10', 17.110001, 117, 193, false, null, 'SF', 'IBL');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (373, 0, 'Alex', 'Estrada', '1974-03-06', 26.129999, 117, 229, false, null, 'C', 'IBL');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (374, 0, 'Juan', 'Norris', '1968-08-29', 35.139999, 81, 211, false, null, 'PF', 'IBL');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (375, 0, 'Alfredo', 'Hicks', '1965-08-21', 39.150002, 117, 156, false, null, 'PG', 'IBL');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (376, 0, 'Salvatore', 'Townsend', '1974-01-19', 22.709999, 135, 228, false, 4, 'SG', 'PBL');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (378, 0, 'Troy', 'Perry', '1988-06-19', 8.7600002, 78, 192, false, 4, 'SF', 'PBL');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (379, 0, 'Floyd', 'Welch', '1975-08-22', 40.990002, 135, 155, false, null, 'C', 'PBL');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (380, 0, 'Gustavo', 'Saunders', '1983-08-27', 45.939999, 109, 160, false, 4, 'PF', 'PBL');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (381, 0, 'Darnell', 'Moreno', '1966-07-09', 43.34, 99, 168, false, null, 'PG', 'PBL');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (382, 0, 'Jamie', 'Leonard', '1980-08-27', 23.27, 157, 195, false, 4, 'SG', 'PBL');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (383, 0, 'Hector', 'Walters', '1970-12-05', 49.02, 122, 236, false, 4, 'SF', 'PBL');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (384, 0, 'Earl', 'Clark', '1970-11-20', 7.1900001, 85, 183, false, 4, 'C', 'PBL');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (386, 0, 'Kelvin', 'Stewart', '1976-12-03', 9.3800001, 112, 188, false, 4, 'PF', 'PBL');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (387, 0, 'Myron', 'Roberts', '1973-03-08', 19.67, 61, 170, false, 4, 'PG', 'PBL');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (388, 0, 'Calvin', 'Frazier', '1989-12-09', 12.98, 141, 223, false, 6, 'SG', 'ABA');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (389, 0, 'Lloyd', 'Allison', '1975-06-20', 10.77, 132, 188, false, 6, 'SF', 'ABA');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (390, 0, 'Leroy', 'Bowman', '1980-01-24', 41.459999, 138, 155, false, null, 'C', 'ABA');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (391, 0, 'Bruce', 'Holloway', '1981-03-20', 41.209999, 69, 166, false, null, 'PF', 'ABA');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (392, 0, 'Phil', 'Thompson', '1970-02-09', 5.4699998, 103, 169, false, 9, 'PG', 'ABA');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (393, 0, 'Lance', 'Boone', '1980-04-08', 28.66, 109, 182, false, 9, 'SG', 'ABA');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (394, 0, 'Felix', 'Carlson', '1968-08-07', 49.459999, 102, 167, false, 9, 'SF', 'ABA');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (395, 0, 'Reginald', 'Gray', '1986-08-23', 9.2600002, 131, 172, false, 9, 'C', 'ABA');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (396, 0, 'Allen', 'Wolfe', '1988-12-11', 14.68, 109, 180, false, 9, 'PF', 'ABA');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (397, 0, 'Marcos', 'Perry', '1968-01-15', 10.8, 153, 215, false, 9, 'PG', 'ABA');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (398, 0, 'Van', 'Pena', '1981-10-06', 8.4399996, 125, 240, false, 9, 'SG', 'ABA');
INSERT INTO player204 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk, position_fk, league_fk) VALUES (399, 0, 'Otis', 'Harvey', '1978-03-29', 14.47, 77, 166, false, 9, 'SF', 'ABA');



ALTER TABLE ONLY league204
    ADD CONSTRAINT league204_pkey PRIMARY KEY (code);

ALTER TABLE ONLY team204
    ADD CONSTRAINT team204_coach_fkc FOREIGN KEY (coach_fk) REFERENCES coach204(id);

ALTER TABLE ONLY player204
    ADD CONSTRAINT player204_team_fkc FOREIGN KEY (team_fk) REFERENCES team204(id);

ALTER TABLE ONLY player204
    ADD CONSTRAINT player204_position_fkc FOREIGN KEY (position_fk) REFERENCES position204(code);
	
ALTER TABLE ONLY player204
    ADD CONSTRAINT player204_league_fkc FOREIGN KEY (league_fk) REFERENCES league204(code);
