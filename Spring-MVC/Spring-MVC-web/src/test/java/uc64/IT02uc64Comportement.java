package uc64;


import org.junit.Test;
import test.common.AbstractBADocTestCase;

public class IT02uc64Comportement extends AbstractBADocTestCase {

	@Override
	public  String getName(){
		return "UC-64";
	}

	@Test
	public void testIT02uc64Comportement() throws Exception {
		// On teste l'UC64 - Standard Features/Entities - Cascade
			
		selenium.open("");	
		selenium.click("link=Expand All");
		selenium.waitForPageToLoad("30000");
		selenium.click("link=UC64 - Cascade");
		
		verifyTrue(selenium.isElementPresent("//h2[contains(text(),\"Standard Features > Entities > UC64 > Cascade\")]"));
		// On modifie une Team.
		// On indexe la ligne des Chargers.
		Number indexChargers = selenium.getElementIndex("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Chargers\")] and td/span[contains(text(),\"Detroit\")] and td/span[contains(text(),\"Arizona\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]");
		System.out.println("variable index vaut : " + indexChargers);
		// On clique sur [details]
		selenium.click("//table[contains(@id,'tab_teams')]/tbody/tr[" + indexChargers + " + 1]/td/a[contains(text(),\"[details]\")]");
		selenium.waitForPageToLoad("30000");
		// On vérifie que les données Teams s'affichent.
		assertTrue(selenium.isElementPresent("//table/tbody[tr/td[2]/input[@value=\"Chargers\"] and tr/td[2]/input[@value=\"Detroit\"] and tr/td[2]//option[@selected=\"selected\" and contains(text(),\"Arizona\")]]"));
		// On modifie le nom de l'equipe.
		selenium.type("//*[contains(@id,'txt_name')]", "Chorgers");
		selenium.click("//*[contains(@id,'lnk_update')]");
		selenium.waitForPageToLoad("30000");
		// On vérfie que la Team est bien modifiée.
		// Le tableau étant dynamique, on va sur la dernière page.
		selenium.click("xpath=(//span/a)[4]");
		selenium.waitForPageToLoad("30000");
		// On vérifie que les Chargers n'existent plus.
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Chargers\")] and td/span[contains(text(),\"Detroit\")] and td/span[contains(text(),\"Arizona\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		// On vérifie que la ligne Chorgers existe.
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Chorgers\")] and td/span[contains(text(),\"Detroit\")] and td/span[contains(text(),\"Arizona\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		// On retourne sur la premiège page de Teams.
		selenium.click("xpath=(//span/a)[1]");
		selenium.waitForPageToLoad("30000");
		// On modofie la Team Wolverines.
		Number indexWolverines = selenium.getElementIndex("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Wolverines\")] and td/span[contains(text(),\"Sacramento\")] and td/span[contains(text(),\"Iowa\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]");
		System.out.println("variable index vaut : " + indexWolverines);
		// On vérifie que les données teams s'affichent.
		selenium.click("//table[contains(@id,'tab_teams')]/tbody/tr[" + indexWolverines + " + 1]/td/a[contains(text(),\"[details]\")]");
		selenium.waitForPageToLoad("30000");
		// On change le state de la Team.
		selenium.select("//*[contains(@id,\"slt_state\")]", "label=California");
		selenium.click("//*[contains(@id,'lnk_update')]");
		selenium.waitForPageToLoad("30000");
		// On vérfie que la Team est bien modifiée.
		// Le tableau étant dynamique, on va sur la dernière page.
		selenium.click("xpath=(//span/a)[4]");
		selenium.waitForPageToLoad("30000");
		// On vérifie que les Wolverines ont changé de State.
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Wolverines\")] and td/span[contains(text(),\"Sacramento\")] and td/span[contains(text(),\"Iowa\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		// On vérifie que la ligne Wolverines-CA existe.
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Wolverines\")] and td/span[contains(text(),\"Sacramento\")] and td/span[contains(text(),\"California\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		// On retourne sur la premiège page de Teams.
		selenium.click("xpath=(//span/a)[1]");
		selenium.waitForPageToLoad("30000");
		// On teste la suppression des States avec Teams.
		// On supprime un State avec Team : California
		// On index la ligne.
		Number indexCalifornia = selenium.getElementIndex("//table[contains(@id,'tab_states')]/tbody/tr[td/span[contains(text(),\"California\")] and td/a[contains(text(),\"[delete]\")]]");
		System.out.println("variable index vaut : " + indexCalifornia);
		// On clique sur [delete].
		selenium.click("//table[contains(@id,'tab_states')]/tbody/tr[" + indexCalifornia + " + 1]/td/a[contains(text(),\"[delete]\")]");
		selenium.waitForPageToLoad("30000");
		// On vérifie que le message d'erreur s'affiche.
		assertEquals("An exception has occurred", selenium.getText("//h5"));
		assertEquals("You must delete the teams with this state before deleting the state.", selenium.getText("//span[contains(@id,'applicationErrorsDiv')]/table/tbody/tr/td|//ul/li|//div[contains(@id,'Errors')]"));
		// On clique sur [back]
		selenium.click("//a[contains(@id,'btn_back')]");
		selenium.waitForPageToLoad("30000");
		// On supprime toutes les Teams.
		// On vérifie pour chaque Team supprimée les tableaux : teams, players, audiences..
		// Pour les Mustangs :
		// On index la ligne.
		Number indexMustangs = selenium.getElementIndex("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Mustangs\")] and td/span[contains(text(),\"Fresno\")] and td/span[contains(text(),\"Hawaii\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]");
		System.out.println("variable index vaut : " + indexMustangs);
		// On clique sur [delete]
		selenium.click("//table[contains(@id,'tab_teams')]/tbody/tr[" + indexMustangs + " + 1]/td/a[contains(text(),\"[delete]\")]");
		selenium.waitForPageToLoad("30000");
		// On vérifie le tableau Teams.
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_teams')]//tbody//tr"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Mustangs\")] and td/span[contains(text(),\"Fresno\")] and td/span[contains(text(),\"Hawaii\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Red Devils\")] and td/span[contains(text(),\"Kansas City\")] and td/span[contains(text(),\"Arkansas\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Spartans\")] and td/span[contains(text(),\"Tucson\")] and td/span[contains(text(),\"Florida\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Spartans\")] and td/span[contains(text(),\"Portland\")] and td/span[contains(text(),\"Delaware\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Braves\")] and td/span[contains(text(),\"San Antonio\")] and td/span[contains(text(),\"Connecticut\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		selenium.click("xpath=(//span/a)[1]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_teams')]//tbody//tr"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Mustangs\")] and td/span[contains(text(),\"Fresno\")] and td/span[contains(text(),\"Hawaii\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Trojans\")] and td/span[contains(text(),\"San Jose\")] and td/span[contains(text(),\"Colorado\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Braves\")] and td/span[contains(text(),\"Omahaff\")] and td/span[contains(text(),\"California\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Tigers\")] and td/span[contains(text(),\"Charlotte\")] and td/span[contains(text(),\"Hawaii\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Hawks\")] and td/span[contains(text(),\"Spring\")] and td/span[contains(text(),\"Colorado\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		selenium.click("xpath=(//span/a)[4]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("2", selenium.getXpathCount("//table[contains(@id,'tab_teams')]//tbody//tr"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Mustangs\")] and td/span[contains(text(),\"Fresno\")] and td/span[contains(text(),\"Hawaii\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Chorgers\")] and td/span[contains(text(),\"Detroit\")] and td/span[contains(text(),\"Arizona\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Wolverines\")] and td/span[contains(text(),\"Sacramento\")] and td/span[contains(text(),\"California\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		// On retourne sur la premiège page de Teams.
		selenium.click("xpath=(//span/a)[1]");
		selenium.waitForPageToLoad("30000");
		// On vérifie que les joueurs des Mustangs sont bien supprimés.
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Oliver\")] and td//span[contains(text(),\"Frank\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Harry\")] and td//span[contains(text(),\"Davis\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Bob\")] and td//span[contains(text(),\"Clark\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Ruben\")] and td//span[contains(text(),\"Pope\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Soto\")] and td/span[contains(text(),\"Marshall\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Lawson\")] and td/span[contains(text(),\"Vernon\")]]"));
		selenium.click("xpath=(//span/a)[12]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Ivan\")] and td//span[contains(text(),\"Tate\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Tommy\")] and td//span[contains(text(),\"Wood\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Sylvester\")] and td//span[contains(text(),\"Nash\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Michael\")] and td//span[contains(text(),\"Hodges\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Soto\")] and td/span[contains(text(),\"Marshall\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Lawson\")] and td/span[contains(text(),\"Vernon\")]]"));
		selenium.click("xpath=(//span/a)[14]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Jerome\")] and td//span[contains(text(),\"Grant\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Woodrow\")] and td//span[contains(text(),\"Carpenter\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Cecil\")] and td//span[contains(text(),\"Conner\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Enrique\")] and td//span[contains(text(),\"Wilkins\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Soto\")] and td/span[contains(text(),\"Marshall\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Lawson\")] and td/span[contains(text(),\"Vernon\")]]"));
		selenium.click("xpath=(//span/a)[14]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Christian\")] and td//span[contains(text(),\"Chavez\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Simon\")] and td//span[contains(text(),\"Klein\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Blake\")] and td//span[contains(text(),\"Montgomery\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Troy\")] and td//span[contains(text(),\"Rowe\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Soto\")] and td/span[contains(text(),\"Marshall\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Lawson\")] and td/span[contains(text(),\"Vernon\")]]"));
		selenium.click("xpath=(//span/a)[14]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Eddie\")] and td//span[contains(text(),\"Boyd\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Daniel\")] and td//span[contains(text(),\"Vargas\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Sam\")] and td//span[contains(text(),\"Farmer\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Felipe\")] and td//span[contains(text(),\"Olson\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Soto\")] and td/span[contains(text(),\"Marshall\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Lawson\")] and td/span[contains(text(),\"Vernon\")]]"));
		selenium.click("xpath=(//span/a)[14]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Aaron\")] and td//span[contains(text(),\"Mullins\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Bruce\")] and td//span[contains(text(),\"Adkins\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Nelson\")] and td//span[contains(text(),\"Davis\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Michael\")] and td//span[contains(text(),\"Simon\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Soto\")] and td/span[contains(text(),\"Marshall\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Lawson\")] and td/span[contains(text(),\"Vernon\")]]"));
		selenium.click("xpath=(//span/a)[14]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Guadalupe\")] and td//span[contains(text(),\"Banks\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Gustavo\")] and td//span[contains(text(),\"Romero\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Gerald\")] and td//span[contains(text(),\"Jimenez\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Salvatore\")] and td//span[contains(text(),\"Robertson\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Soto\")] and td/span[contains(text(),\"Marshall\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Lawson\")] and td/span[contains(text(),\"Vernon\")]]"));
		selenium.click("xpath=(//span/a)[14]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Freddie\")] and td//span[contains(text(),\"Kelly\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Gary\")] and td//span[contains(text(),\"Christensen\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Felipe\")] and td//span[contains(text(),\"Lawrence\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Jesus\")] and td//span[contains(text(),\"Jimenez\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Soto\")] and td/span[contains(text(),\"Marshall\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Lawson\")] and td/span[contains(text(),\"Vernon\")]]"));
		selenium.click("xpath=(//span/a)[14]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("1", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Ralph\")] and td//span[contains(text(),\"Castillo\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Soto\")] and td/span[contains(text(),\"Marshall\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Lawson\")] and td/span[contains(text(),\"Vernon\")]]"));
		// On retourne sur la première page de Players.
		selenium.click("xpath=(//span/a)[5]");
		selenium.waitForPageToLoad("30000");
		// On vérifie les Coachs.
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_coachs')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Harry\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Loren\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Bob\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Gerald\")]]"));
		selenium.click("xpath=(//span/a)[17]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_coachs')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Louis\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Cameron\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Shane\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Keith\")]]"));
		selenium.click("xpath=(//span/a)[19]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_coachs')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Ivan\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Tommy\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Sylvester\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Michael\")]]"));
		selenium.click("xpath=(//span/a)[19]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("2", selenium.getXpathCount("//table[contains(@id,'tab_coachs')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Jerome\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Woodrow\")]]"));
		selenium.click("xpath=(//span/a)[14]");
		selenium.waitForPageToLoad("30000");
		// On vérifie les States.
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_state')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"California\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Connecticut\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Delaware\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Florida\")] and td/a[contains(text(),\"[delete]\")]]"));
		selenium.click("xpath=(//span/a)[21]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_state')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Hawaii\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Iowa\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Alabama\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Arizona\")] and td/a[contains(text(),\"[delete]\")]]"));
		selenium.click("xpath=(//span/a)[23]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("3", selenium.getXpathCount("//table[contains(@id,'tab_state')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Arkansas\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Alaska\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Colorado\")] and td/a[contains(text(),\"[delete]\")]]"));
		selenium.click("xpath=(//span/a)[19]");
		selenium.waitForPageToLoad("30000");
		// On vérifie les audiences.
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_audiences')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_audiences')]/tbody/tr[td/span[contains(text(),\"Tigers\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_audiences')]/tbody/tr[td/span[contains(text(),\"Chargers\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_audiences')]/tbody/tr[td/span[contains(text(),\"Hawks\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_audiences')]/tbody/tr[td/span[contains(text(),\"Braves\")]]"));
		// Pour les Red Devils :
		// On indexe la ligne
		Number indexRedDevils = selenium.getElementIndex("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Red Devils\")] and td/span[contains(text(),\"Kansas City\")] and td/span[contains(text(),\"Arkansas\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]");
		System.out.println("variable index vaut : " + indexRedDevils);
		// On clique sur [delete]
		selenium.click("//table[contains(@id,'tab_teams')]/tbody/tr[" + indexRedDevils + " + 1]/td/a[contains(text(),\"[delete]\")]");
		selenium.waitForPageToLoad("30000");
		// On vérifie le tableau Teams.
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Mustangs\")] and td/span[contains(text(),\"Fresno\")] and td/span[contains(text(),\"Hawaii\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Red Devils\")] and td/span[contains(text(),\"Kansas City\")] and td/span[contains(text(),\"Arkansas\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_teams')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Spartans\")] and td/span[contains(text(),\"Tucson\")] and td/span[contains(text(),\"Florida\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Spartans\")] and td/span[contains(text(),\"Portland\")] and td/span[contains(text(),\"Delaware\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Braves\")] and td/span[contains(text(),\"San Antonio\")] and td/span[contains(text(),\"Connecticut\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Trojans\")] and td/span[contains(text(),\"San Jose\")] and td/span[contains(text(),\"Colorado\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		selenium.click("xpath=(//span/a)[1]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_teams')]//tbody//tr"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Mustangs\")] and td/span[contains(text(),\"Fresno\")] and td/span[contains(text(),\"Hawaii\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Red Devils\")] and td/span[contains(text(),\"Kansas City\")] and td/span[contains(text(),\"Arkansas\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Braves\")] and td/span[contains(text(),\"Omahaff\")] and td/span[contains(text(),\"California\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Tigers\")] and td/span[contains(text(),\"Charlotte\")] and td/span[contains(text(),\"Hawaii\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Hawks\")] and td/span[contains(text(),\"Spring\")] and td/span[contains(text(),\"Colorado\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Chorgers\")] and td/span[contains(text(),\"Detroit\")] and td/span[contains(text(),\"Arizona\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		selenium.click("xpath=(//span/a)[4]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("1", selenium.getXpathCount("//table[contains(@id,'tab_teams')]//tbody//tr"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Mustangs\")] and td/span[contains(text(),\"Fresno\")] and td/span[contains(text(),\"Hawaii\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Red Devils\")] and td/span[contains(text(),\"Kansas City\")] and td/span[contains(text(),\"Arkansas\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Wolverines\")] and td/span[contains(text(),\"Sacramento\")] and td/span[contains(text(),\"California\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		// On retourne sur la premiège page de Teams.
		selenium.click("xpath=(//span/a)[1]");
		selenium.waitForPageToLoad("30000");
		// On vérifie que les joueurs des Red Devils sont bien supprimés.
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Oliver\")] and td//span[contains(text(),\"Frank\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Harry\")] and td//span[contains(text(),\"Davis\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Bob\")] and td//span[contains(text(),\"Clark\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Ruben\")] and td//span[contains(text(),\"Pope\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Banks\")] and td/span[contains(text(),\"Guadalupe\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Romero\")] and td/span[contains(text(),\"Gustavo\")]]"));
		selenium.click("xpath=(//span/a)[5]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Ivan\")] and td//span[contains(text(),\"Tate\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Tommy\")] and td//span[contains(text(),\"Wood\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Sylvester\")] and td//span[contains(text(),\"Nash\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Michael\")] and td//span[contains(text(),\"Hodges\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Banks\")] and td/span[contains(text(),\"Guadalupe\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Romero\")] and td/span[contains(text(),\"Gustavo\")]]"));
		selenium.click("xpath=(//span/a)[8]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Jerome\")] and td//span[contains(text(),\"Grant\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Woodrow\")] and td//span[contains(text(),\"Carpenter\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Cecil\")] and td//span[contains(text(),\"Conner\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Enrique\")] and td//span[contains(text(),\"Wilkins\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Banks\")] and td/span[contains(text(),\"Guadalupe\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Romero\")] and td/span[contains(text(),\"Gustavo\")]]"));
		selenium.click("xpath=(//span/a)[9]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Christian\")] and td//span[contains(text(),\"Chavez\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Simon\")] and td//span[contains(text(),\"Klein\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Blake\")] and td//span[contains(text(),\"Montgomery\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Troy\")] and td//span[contains(text(),\"Rowe\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Banks\")] and td/span[contains(text(),\"Guadalupe\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Romero\")] and td/span[contains(text(),\"Gustavo\")]]"));
		selenium.click("xpath=(//span/a)[10]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Eddie\")] and td//span[contains(text(),\"Boyd\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Daniel\")] and td//span[contains(text(),\"Vargas\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Sam\")] and td//span[contains(text(),\"Farmer\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Felipe\")] and td//span[contains(text(),\"Olson\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Banks\")] and td/span[contains(text(),\"Guadalupe\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Romero\")] and td/span[contains(text(),\"Gustavo\")]]"));
		selenium.click("xpath=(//span/a)[11]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Aaron\")] and td//span[contains(text(),\"Mullins\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Bruce\")] and td//span[contains(text(),\"Adkins\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Nelson\")] and td//span[contains(text(),\"Davis\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Michael\")] and td//span[contains(text(),\"Simon\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Banks\")] and td/span[contains(text(),\"Guadalupe\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Romero\")] and td/span[contains(text(),\"Gustavo\")]]"));
		selenium.click("xpath=(//span/a)[12]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Gerald\")] and td//span[contains(text(),\"Jimenez\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Salvatore\")] and td//span[contains(text(),\"Robertson\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Freddie\")] and td//span[contains(text(),\"Kelly\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Gary\")] and td//span[contains(text(),\"Christensen\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Banks\")] and td/span[contains(text(),\"Guadalupe\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Romero\")] and td/span[contains(text(),\"Gustavo\")]]"));
		selenium.click("xpath=(//span/a)[13]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("3", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Felipe\")] and td//span[contains(text(),\"Lawrence\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Jesus\")] and td//span[contains(text(),\"Jimenez\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Ralph\")] and td//span[contains(text(),\"Castillo\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Banks\")] and td/span[contains(text(),\"Guadalupe\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Romero\")] and td/span[contains(text(),\"Gustavo\")]]"));
		// On retourne sur la première page de Players.
		selenium.click("xpath=(//span/a)[5]");
		selenium.waitForPageToLoad("30000");
		// On vérifie les Coachs.
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_coachs')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Harry\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Loren\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Bob\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Gerald\")]]"));
		selenium.click("xpath= (//span[6]/a)[1]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_coachs')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Louis\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Cameron\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Shane\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Keith\")]]"));
		selenium.click("xpath= (//span[6]/a)[4]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_coachs')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Ivan\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Tommy\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Sylvester\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Michael\")]]"));
		selenium.click("xpath= (//span[6]/a)[5]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("2", selenium.getXpathCount("//table[contains(@id,'tab_coachs')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Jerome\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Woodrow\")]]"));
		selenium.click("xpath= (//span[6]/a)[1]");
		selenium.waitForPageToLoad("30000");
		// On vérifie les States.
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_state')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"California\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Connecticut\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Delaware\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Florida\")] and td/a[contains(text(),\"[delete]\")]]"));
		selenium.click("xpath= (//span[8]/a)[1]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_state')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Hawaii\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Iowa\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Alabama\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Arizona\")] and td/a[contains(text(),\"[delete]\")]]"));
		selenium.click("xpath= (//span[8]/a)[4]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("3", selenium.getXpathCount("//table[contains(@id,'tab_state')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Arkansas\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Alaska\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Colorado\")] and td/a[contains(text(),\"[delete]\")]]"));
		selenium.click("xpath= (//span[8]/a)[1]");
		selenium.waitForPageToLoad("30000");
		// On vérifie les audiences.
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_audiences')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_audiences')]/tbody/tr[td/span[contains(text(),\"Tigers\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_audiences')]/tbody/tr[td/span[contains(text(),\"Chargers\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_audiences')]/tbody/tr[td/span[contains(text(),\"Hawks\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_audiences')]/tbody/tr[td/span[contains(text(),\"Braves\")]]"));
		// Pour les Spartans - Tucson :
		// On indexe la ligne
		Number indexSpartansT = selenium.getElementIndex("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Spartans\")] and td/span[contains(text(),\"Tucson\")] and td/span[contains(text(),\"Florida\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]");
		System.out.println("variable index vaut : " + indexSpartansT);
		// On clique sur [delete]
		selenium.click("//table[contains(@id,'tab_teams')]/tbody/tr[" + indexSpartansT + " + 1]/td/a[contains(text(),\"[delete]\")]");
		selenium.waitForPageToLoad("30000");
		// On vérifie le tableau Teams.
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Mustangs\")] and td/span[contains(text(),\"Fresno\")] and td/span[contains(text(),\"Hawaii\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Red Devils\")] and td/span[contains(text(),\"Kansas City\")] and td/span[contains(text(),\"Arkansas\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Spartans\")] and td/span[contains(text(),\"Tucson\")] and td/span[contains(text(),\"Florida\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_teams')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Spartans\")] and td/span[contains(text(),\"Portland\")] and td/span[contains(text(),\"Delaware\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Braves\")] and td/span[contains(text(),\"San Antonio\")] and td/span[contains(text(),\"Connecticut\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Trojans\")] and td/span[contains(text(),\"San Jose\")] and td/span[contains(text(),\"Colorado\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Braves\")] and td/span[contains(text(),\"Omahaff\")] and td/span[contains(text(),\"California\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		selenium.click("xpath= (//span[2]/a)[1]");
		selenium.waitForPageToLoad("30000");
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Mustangs\")] and td/span[contains(text(),\"Fresno\")] and td/span[contains(text(),\"Hawaii\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Spartans\")] and td/span[contains(text(),\"Tucson\")] and td/span[contains(text(),\"Florida\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Red Devils\")] and td/span[contains(text(),\"Kansas City\")] and td/span[contains(text(),\"Arkansas\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_teams')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Tigers\")] and td/span[contains(text(),\"Charlotte\")] and td/span[contains(text(),\"Hawaii\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Chorgers\")] and td/span[contains(text(),\"Detroit\")] and td/span[contains(text(),\"Arizona\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Hawks\")] and td/span[contains(text(),\"Spring\")] and td/span[contains(text(),\"Colorado\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Wolverines\")] and td/span[contains(text(),\"Sacramento\")] and td/span[contains(text(),\"California\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		// On retourne sur la premiège page de Teams.
		selenium.click("xpath= (//span[2]/a)[1]");
		selenium.waitForPageToLoad("30000");
		// On vérifie que les joueurs des Spartans - Tucson sont bien supprimés.
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Ruben\")] and td//span[contains(text(),\"Pope\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Oliver\")] and td//span[contains(text(),\"Frank\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Harry\")] and td//span[contains(text(),\"Davis\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Bob\")] and td//span[contains(text(),\"Clark\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Ruben\")] and td//span[contains(text(),\"Pope\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Klein\")] and td/span[contains(text(),\"Simon\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Montgomery\")] and td/span[contains(text(),\"Blake\")]]"));
		selenium.click("xpath= (//span[4]/a)[1]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Ivan\")] and td//span[contains(text(),\"Tate\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Tommy\")] and td//span[contains(text(),\"Wood\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Sylvester\")] and td//span[contains(text(),\"Nash\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Michael\")] and td//span[contains(text(),\"Hodges\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Klein\")] and td/span[contains(text(),\"Simon\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Montgomery\")] and td/span[contains(text(),\"Blake\")]]"));
		selenium.click("xpath= (//span[4]/a)[4]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Jerome\")] and td//span[contains(text(),\"Grant\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Woodrow\")] and td//span[contains(text(),\"Carpenter\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Cecil\")] and td//span[contains(text(),\"Conner\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Enrique\")] and td//span[contains(text(),\"Wilkins\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Klein\")] and td/span[contains(text(),\"Simon\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Montgomery\")] and td/span[contains(text(),\"Blake\")]]"));
		selenium.click("xpath= (//span[4]/a)[5]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Christian\")] and td//span[contains(text(),\"Chavez\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Troy\")] and td//span[contains(text(),\"Rowe\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Eddie\")] and td//span[contains(text(),\"Boyd\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Daniel\")] and td//span[contains(text(),\"Vargas\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Klein\")] and td/span[contains(text(),\"Simon\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Montgomery\")] and td/span[contains(text(),\"Blake\")]]"));
		selenium.click("xpath= (//span[4]/a)[6]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Sam\")] and td//span[contains(text(),\"Farmer\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Felipe\")] and td//span[contains(text(),\"Olson\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Aaron\")] and td//span[contains(text(),\"Mullins\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Bruce\")] and td//span[contains(text(),\"Adkins\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Klein\")] and td/span[contains(text(),\"Simon\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Montgomery\")] and td/span[contains(text(),\"Blake\")]]"));
		selenium.click("xpath= (//span[4]/a)[7]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Nelson\")] and td//span[contains(text(),\"Davis\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Michael\")] and td//span[contains(text(),\"Simon\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Gerald\")] and td//span[contains(text(),\"Jimenez\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Salvatore\")] and td//span[contains(text(),\"Robertson\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Klein\")] and td/span[contains(text(),\"Simon\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Montgomery\")] and td/span[contains(text(),\"Blake\")]]"));
		selenium.click("xpath= (//span[4]/a)[8]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Freddie\")] and td//span[contains(text(),\"Kelly\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Gary\")] and td//span[contains(text(),\"Christensen\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Felipe\")] and td//span[contains(text(),\"Lawrence\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Jesus\")] and td//span[contains(text(),\"Jimenez\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Klein\")] and td/span[contains(text(),\"Simon\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Montgomery\")] and td/span[contains(text(),\"Blake\")]]"));
		selenium.click("xpath= (//span[4]/a)[9]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("1", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,\"tab_players\")]//tbody//tr[td//span[contains(text(),\"Ralph\")] and td//span[contains(text(),\"Castillo\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Klein\")] and td/span[contains(text(),\"Simon\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Montgomery\")] and td/span[contains(text(),\"Blake\")]]"));
		// On retourne sur la première page de Players.
		selenium.click("xpath= (//span[4]/a)[1]");
		selenium.waitForPageToLoad("30000");
		// On vérifie les Coachs.
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_coachs')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Harry\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Loren\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Bob\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Gerald\")]]"));
		selenium.click("xpath= (//span[6]/a)[1]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_coachs')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Louis\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Cameron\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Shane\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Keith\")]]"));
		selenium.click("xpath= (//span[6]/a)[4]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_coachs')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Ivan\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Tommy\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Sylvester\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Michael\")]]"));
		selenium.click("xpath= (//span[6]/a)[5]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("2", selenium.getXpathCount("//table[contains(@id,'tab_coachs')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Jerome\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Woodrow\")]]"));
		selenium.click("xpath= (//span[6]/a)[1]");
		selenium.waitForPageToLoad("30000");
		// On vérifie les States.
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_state')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"California\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Connecticut\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Delaware\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Florida\")] and td/a[contains(text(),\"[delete]\")]]"));
		selenium.click("xpath= (//span[8]/a)[1]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_state')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Hawaii\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Iowa\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Alabama\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Arizona\")] and td/a[contains(text(),\"[delete]\")]]"));
		selenium.click("xpath= (//span[8]/a)[4]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("3", selenium.getXpathCount("//table[contains(@id,'tab_state')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Arkansas\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Alaska\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Colorado\")] and td/a[contains(text(),\"[delete]\")]]"));
		selenium.click("xpath= (//span[8]/a)[1]");
		selenium.waitForPageToLoad("30000");
		// On vérifie les audiences.
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_audiences')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_audiences')]/tbody/tr[td/span[contains(text(),\"Tigers\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_audiences')]/tbody/tr[td/span[contains(text(),\"Chargers\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_audiences')]/tbody/tr[td/span[contains(text(),\"Hawks\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_audiences')]/tbody/tr[td/span[contains(text(),\"Braves\")]]"));
		// Pour les Spartans - Portland :
		// On indexe la ligne
		Number indexSpartansP = selenium.getElementIndex("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Spartans\")] and td/span[contains(text(),\"Portland\")] and td/span[contains(text(),\"Delaware\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]");
		System.out.println("variable index vaut : " + indexSpartansP);
		// On clique sur [delete]
		selenium.click("//table[contains(@id,'tab_teams')]/tbody/tr[" + indexSpartansP + " + 1]/td/a[contains(text(),\"[delete]\")]");
		selenium.waitForPageToLoad("30000");
		// On vérifie le tableau Teams.
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Mustangs\")] and td/span[contains(text(),\"Fresno\")] and td/span[contains(text(),\"Hawaii\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Red Devils\")] and td/span[contains(text(),\"Kansas City\")] and td/span[contains(text(),\"Arkansas\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Spartans\")] and td/span[contains(text(),\"Tucson\")] and td/span[contains(text(),\"Florida\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Spartans\")] and td/span[contains(text(),\"Portland\")] and td/span[contains(text(),\"Delaware\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_teams')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Braves\")] and td/span[contains(text(),\"San Antonio\")] and td/span[contains(text(),\"Connecticut\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Trojans\")] and td/span[contains(text(),\"San Jose\")] and td/span[contains(text(),\"Colorado\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Braves\")] and td/span[contains(text(),\"Omahaff\")] and td/span[contains(text(),\"California\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Tigers\")] and td/span[contains(text(),\"Charlotte\")] and td/span[contains(text(),\"Hawaii\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Mustangs\")] and td/span[contains(text(),\"Fresno\")] and td/span[contains(text(),\"Hawaii\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Red Devils\")] and td/span[contains(text(),\"Kansas City\")] and td/span[contains(text(),\"Arkansas\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Spartans\")] and td/span[contains(text(),\"Tucson\")] and td/span[contains(text(),\"Florida\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Spartans\")] and td/span[contains(text(),\"Portland\")] and td/span[contains(text(),\"Delaware\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		selenium.click("xpath= (//span[2]/a)[1]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("3", selenium.getXpathCount("//table[contains(@id,'tab_teams')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Hawks\")] and td/span[contains(text(),\"Spring\")] and td/span[contains(text(),\"Colorado\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Chorgers\")] and td/span[contains(text(),\"Detroit\")] and td/span[contains(text(),\"Arizona\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Wolverines\")] and td/span[contains(text(),\"Sacramento\")] and td/span[contains(text(),\"California\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		// On retourne sur la premiège page de Teams.
		selenium.click("xpath= (//span[2]/a)[1]");
		selenium.waitForPageToLoad("30000");
		// On vérifie que les joueurs des Spartans - Portland sont bien supprimés.
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Rowe\")] and td/span[contains(text(),\"Troy\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Boyd\")] and td/span[contains(text(),\"Eddie\")]]"));
		selenium.click("xpath= (//span[4]/a)[1]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Rowe\")] and td/span[contains(text(),\"Troy\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Boyd\")] and td/span[contains(text(),\"Eddie\")]]"));
		selenium.click("xpath= (//span[4]/a)[4]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Rowe\")] and td/span[contains(text(),\"Troy\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Boyd\")] and td/span[contains(text(),\"Eddie\")]]"));
		selenium.click("xpath= (//span[4]/a)[5]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Rowe\")] and td/span[contains(text(),\"Troy\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Boyd\")] and td/span[contains(text(),\"Eddie\")]]"));
		selenium.click("xpath= (//span[4]/a)[6]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Rowe\")] and td/span[contains(text(),\"Troy\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Boyd\")] and td/span[contains(text(),\"Eddie\")]]"));
		selenium.click("xpath= (//span[4]/a)[7]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Rowe\")] and td/span[contains(text(),\"Troy\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Boyd\")] and td/span[contains(text(),\"Eddie\")]]"));
		selenium.click("xpath= (//span[4]/a)[8]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("3", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Rowe\")] and td/span[contains(text(),\"Troy\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Boyd\")] and td/span[contains(text(),\"Eddie\")]]"));
		// On retourne sur la première page de Players.
		selenium.click("xpath= (//span[4]/a)[1]");
		selenium.waitForPageToLoad("30000");
		// On vérifie les Coachs.
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_coachs')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Harry\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Loren\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Bob\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Gerald\")]]"));
		selenium.click("xpath= (//span[6]/a)[1]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_coachs')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Louis\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Cameron\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Shane\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Keith\")]]"));
		selenium.click("xpath= (//span[6]/a)[4]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_coachs')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Ivan\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Tommy\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Sylvester\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Michael\")]]"));
		selenium.click("xpath= (//span[6]/a)[5]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("2", selenium.getXpathCount("//table[contains(@id,'tab_coachs')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Jerome\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Woodrow\")]]"));
		selenium.click("xpath= (//span[6]/a)[1]");
		selenium.waitForPageToLoad("30000");
		// On vérife les States.
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_state')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"California\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Connecticut\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Delaware\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Florida\")] and td/a[contains(text(),\"[delete]\")]]"));
		selenium.click("xpath= (//span[8]/a)[1]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_state')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Hawaii\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Iowa\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Alabama\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Arizona\")] and td/a[contains(text(),\"[delete]\")]]"));
		selenium.click("xpath= (//span[8]/a)[4]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("3", selenium.getXpathCount("//table[contains(@id,'tab_state')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Arkansas\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Alaska\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Colorado\")] and td/a[contains(text(),\"[delete]\")]]"));
		selenium.click("xpath= (//span[8]/a)[1]");
		selenium.waitForPageToLoad("30000");
		// On vérifie les audiences.
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_audiences')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_audiences')]/tbody/tr[td/span[contains(text(),\"Tigers\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_audiences')]/tbody/tr[td/span[contains(text(),\"Chargers\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_audiences')]/tbody/tr[td/span[contains(text(),\"Hawks\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_audiences')]/tbody/tr[td/span[contains(text(),\"Braves\")]]"));
		// Pour les Braves - San Antonio :
		// On indexe la ligne
		Number indexBravesSA = selenium.getElementIndex("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Braves\")] and td/span[contains(text(),\"San Antonio\")] and td/span[contains(text(),\"Connecticut\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]");
		System.out.println("variable index vaut : " + indexBravesSA);
		// On clique sur [delete]
		selenium.click("//table[contains(@id,'tab_teams')]/tbody/tr[" + indexBravesSA + " + 1]/td/a[contains(text(),\"[delete]\")]");
		selenium.waitForPageToLoad("30000");
		// On vérifie le tableau Teams.
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Mustangs\")] and td/span[contains(text(),\"Fresno\")] and td/span[contains(text(),\"Hawaii\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Red Devils\")] and td/span[contains(text(),\"Kansas City\")] and td/span[contains(text(),\"Arkansas\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Spartans\")] and td/span[contains(text(),\"Tucson\")] and td/span[contains(text(),\"Florida\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Spartans\")] and td/span[contains(text(),\"Portland\")] and td/span[contains(text(),\"Delaware\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Braves\")] and td/span[contains(text(),\"San Antonio\")] and td/span[contains(text(),\"Connecticut\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_teams')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Trojans\")] and td/span[contains(text(),\"San Jose\")] and td/span[contains(text(),\"Colorado\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Braves\")] and td/span[contains(text(),\"Omahaff\")] and td/span[contains(text(),\"California\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Tigers\")] and td/span[contains(text(),\"Charlotte\")] and td/span[contains(text(),\"Hawaii\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Hawks\")] and td/span[contains(text(),\"Spring\")] and td/span[contains(text(),\"Colorado\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		selenium.click("xpath= (//span[2]/a)[1]");
		selenium.waitForPageToLoad("30000");
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Mustangs\")] and td/span[contains(text(),\"Fresno\")] and td/span[contains(text(),\"Hawaii\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Red Devils\")] and td/span[contains(text(),\"Kansas City\")] and td/span[contains(text(),\"Arkansas\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Spartans\")] and td/span[contains(text(),\"Tucson\")] and td/span[contains(text(),\"Florida\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Spartans\")] and td/span[contains(text(),\"Portland\")] and td/span[contains(text(),\"Delaware\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Braves\")] and td/span[contains(text(),\"San Antonio\")] and td/span[contains(text(),\"Connecticut\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		verifyEquals("2", selenium.getXpathCount("//table[contains(@id,'tab_teams')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Chorgers\")] and td/span[contains(text(),\"Detroit\")] and td/span[contains(text(),\"Arizona\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Wolverines\")] and td/span[contains(text(),\"Sacramento\")] and td/span[contains(text(),\"California\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		// On retourne sur la premiège page de Teams.
		selenium.click("xpath= (//span[2]/a)[1]");
		selenium.waitForPageToLoad("30000");
		// On vérifie que les joueurs des Braves - San Antonio sont bien supprimés.
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Farmer\")] and td/span[contains(text(),\"Sam\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Vargas\")] and td/span[contains(text(),\"Daniel\")]]"));
		selenium.click("xpath= (//span[4]/a)[7]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Vargas\")] and td/span[contains(text(),\"Daniel\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Farmer\")] and td/span[contains(text(),\"Sam\")]]"));
		selenium.click("xpath= (//span[4]/a)[9]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Vargas\")] and td/span[contains(text(),\"Daniel\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Farmer\")] and td/span[contains(text(),\"Sam\")]]"));
		selenium.click("xpath= (//span[4]/a)[9]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Vargas\")] and td/span[contains(text(),\"Daniel\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Farmer\")] and td/span[contains(text(),\"Sam\")]]"));
		selenium.click("xpath= (//span[4]/a)[9]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Vargas\")] and td/span[contains(text(),\"Daniel\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Farmer\")] and td/span[contains(text(),\"Sam\")]]"));
		selenium.click("xpath= (//span[4]/a)[9]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Vargas\")] and td/span[contains(text(),\"Daniel\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Farmer\")] and td/span[contains(text(),\"Sam\")]]"));
		selenium.click("xpath= (//span[4]/a)[9]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("1", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Vargas\")] and td/span[contains(text(),\"Daniel\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Farmer\")] and td/span[contains(text(),\"Sam\")]]"));
		// On retourne sur la première page de Players.
		selenium.click("xpath= (//span[4]/a)[1]");
		selenium.waitForPageToLoad("30000");
		// On vérifie les Coachs.
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_coachs')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Harry\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Loren\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Bob\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Gerald\")]]"));
		selenium.click("xpath= (//span[6]/a)[4]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_coachs')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Louis\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Cameron\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Shane\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Keith\")]]"));
		selenium.click("xpath= (//span[6]/a)[6]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_coachs')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Ivan\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Tommy\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Sylvester\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Michael\")]]"));
		selenium.click("xpath= (//span[6]/a)[6]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("2", selenium.getXpathCount("//table[contains(@id,'tab_coachs')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Jerome\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Woodrow\")]]"));
		selenium.click("xpath= (//span[6]/a)[1]");
		selenium.waitForPageToLoad("30000");
		// On vérifie les States
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_state')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"California\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Connecticut\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Delaware\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Florida\")] and td/a[contains(text(),\"[delete]\")]]"));
		selenium.click("xpath= (//span[8]/a)[3]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_state')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Hawaii\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Iowa\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Alabama\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Arizona\")] and td/a[contains(text(),\"[delete]\")]]"));
		selenium.click("xpath= (//span[8]/a)[5]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("3", selenium.getXpathCount("//table[contains(@id,'tab_state')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Arkansas\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Alaska\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Colorado\")] and td/a[contains(text(),\"[delete]\")]]"));
		selenium.click("xpath= (//span[8]/a)[1]");
		selenium.waitForPageToLoad("30000");
		// On vérifie les audiences.
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_audiences')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_audiences')]/tbody/tr[td/span[contains(text(),\"Tigers\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_audiences')]/tbody/tr[td/span[contains(text(),\"Chargers\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_audiences')]/tbody/tr[td/span[contains(text(),\"Hawks\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_audiences')]/tbody/tr[td/span[contains(text(),\"Braves\")]]"));
		// Pour les Trojans :
		// On indexe la ligne
		Number indexTrojans = selenium.getElementIndex("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Trojans\")] and td/span[contains(text(),\"San Jose\")] and td/span[contains(text(),\"Colorado\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]");
		System.out.println("variable index vaut : " + indexTrojans);
		// On clique sur [delete]
		selenium.click("//table[contains(@id,'tab_teams')]/tbody/tr[" + indexTrojans + " + 1]/td/a[contains(text(),\"[delete]\")]");
		selenium.waitForPageToLoad("30000");
		// On vérifie le tableau Teams.
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Mustangs\")] and td/span[contains(text(),\"Fresno\")] and td/span[contains(text(),\"Hawaii\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Red Devils\")] and td/span[contains(text(),\"Kansas City\")] and td/span[contains(text(),\"Arkansas\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Spartans\")] and td/span[contains(text(),\"Tucson\")] and td/span[contains(text(),\"Florida\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Spartans\")] and td/span[contains(text(),\"Portland\")] and td/span[contains(text(),\"Delaware\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Braves\")] and td/span[contains(text(),\"San Antonio\")] and td/span[contains(text(),\"Connecticut\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Trojans\")] and td/span[contains(text(),\"San José\")] and td/span[contains(text(),\"Colorado\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_teams')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Braves\")] and td/span[contains(text(),\"Omahaff\")] and td/span[contains(text(),\"California\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Tigers\")] and td/span[contains(text(),\"Charlotte\")] and td/span[contains(text(),\"Hawaii\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Hawks\")] and td/span[contains(text(),\"Spring\")] and td/span[contains(text(),\"Colorado\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Chorgers\")] and td/span[contains(text(),\"Detroit\")] and td/span[contains(text(),\"Arizona\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		selenium.click("xpath= (//span[2]/a)[2]");
		selenium.waitForPageToLoad("30000");
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Mustangs\")] and td/span[contains(text(),\"Fresno\")] and td/span[contains(text(),\"Hawaii\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Red Devils\")] and td/span[contains(text(),\"Kansas City\")] and td/span[contains(text(),\"Arkansas\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Spartans\")] and td/span[contains(text(),\"Tucson\")] and td/span[contains(text(),\"Florida\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Spartans\")] and td/span[contains(text(),\"Portland\")] and td/span[contains(text(),\"Delaware\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Braves\")] and td/span[contains(text(),\"San Antonio\")] and td/span[contains(text(),\"Connecticut\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Trojans\")] and td/span[contains(text(),\"San José\")] and td/span[contains(text(),\"Colorado\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		verifyEquals("1", selenium.getXpathCount("//table[contains(@id,'tab_teams')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Wolverines\")] and td/span[contains(text(),\"Sacramento\")] and td/span[contains(text(),\"California\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		// On retourne sur la premiège page de Teams.
		selenium.click("xpath= (//span[2]/a)[1]");
		selenium.waitForPageToLoad("30000");
		// On vérifie que les joueurs des Trojans sont bien supprimés.
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Olson\")] and td/span[contains(text(),\"Felipe\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Mullins\")] and td/span[contains(text(),\"Aaron\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Adkins\")] and td/span[contains(text(),\"Bruce\")]]"));
		selenium.click("xpath= (//span[4]/a)[6]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Olson\")] and td/span[contains(text(),\"Felipe\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Mullins\")] and td/span[contains(text(),\"Aaron\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Adkins\")] and td/span[contains(text(),\"Bruce\")]]"));
		selenium.click("xpath= (//span[4]/a)[8]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Olson\")] and td/span[contains(text(),\"Felipe\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Mullins\")] and td/span[contains(text(),\"Aaron\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Adkins\")] and td/span[contains(text(),\"Bruce\")]]"));
		selenium.click("xpath= (//span[4]/a)[8]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Olson\")] and td/span[contains(text(),\"Felipe\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Mullins\")] and td/span[contains(text(),\"Aaron\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Adkins\")] and td/span[contains(text(),\"Bruce\")]]"));
		selenium.click("xpath= (//span[4]/a)[8]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Olson\")] and td/span[contains(text(),\"Felipe\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Mullins\")] and td/span[contains(text(),\"Aaron\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Adkins\")] and td/span[contains(text(),\"Bruce\")]]"));
		selenium.click("xpath= (//span[4]/a)[8]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("2", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Olson\")] and td/span[contains(text(),\"Felipe\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Mullins\")] and td/span[contains(text(),\"Aaron\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Adkins\")] and td/span[contains(text(),\"Bruce\")]]"));
		// On retourne sur la première page de Players.
		selenium.click("xpath= (//span[4]/a)[1]");
		selenium.waitForPageToLoad("30000");
		// On vérifie les Coachs.
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_coachs')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Harry\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Loren\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Bob\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Gerald\")]]"));
		selenium.click("xpath= (//span[6]/a)[4]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_coachs')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Louis\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Cameron\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Shane\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Keith\")]]"));
		selenium.click("xpath= (//span[6]/a)[6]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_coachs')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Ivan\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Tommy\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Sylvester\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Michael\")]]"));
		selenium.click("xpath= (//span[6]/a)[6]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("2", selenium.getXpathCount("//table[contains(@id,'tab_coachs')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Jerome\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Woodrow\")]]"));
		selenium.click("xpath= (//span[6]/a)[1]");
		selenium.waitForPageToLoad("30000");
		// On vérife les States.
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_state')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"California\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Connecticut\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Delaware\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Florida\")] and td/a[contains(text(),\"[delete]\")]]"));
		selenium.click("xpath= (//span[8]/a)[3]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_state')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Hawaii\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Iowa\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Alabama\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Arizona\")] and td/a[contains(text(),\"[delete]\")]]"));
		selenium.click("xpath= (//span[8]/a)[5]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("3", selenium.getXpathCount("//table[contains(@id,'tab_state')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Arkansas\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Alaska\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Colorado\")] and td/a[contains(text(),\"[delete]\")]]"));
		selenium.click("xpath= (//span[8]/a)[1]");
		selenium.waitForPageToLoad("30000");
		// On vérifie les audiences.
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_audiences')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_audiences')]/tbody/tr[td/span[contains(text(),\"Tigers\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_audiences')]/tbody/tr[td/span[contains(text(),\"Chargers\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_audiences')]/tbody/tr[td/span[contains(text(),\"Hawks\")]]"));
		// Pour les Braves - Omahaff :
		// On indexe la ligne
		Number indexBravesO = selenium.getElementIndex("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Braves\")] and td/span[contains(text(),\"Omahaff\")] and td/span[contains(text(),\"California\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]");
		System.out.println("variable index vaut : " + indexBravesO);
		// On clique sur [delete]
		selenium.click("//table[contains(@id,'tab_teams')]/tbody/tr[" + indexBravesO + " + 1]/td/a[contains(text(),\"[delete]\")]");
		selenium.waitForPageToLoad("30000");
		// On vérifie le tableau Teams.
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Mustangs\")] and td/span[contains(text(),\"Fresno\")] and td/span[contains(text(),\"Hawaii\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Red Devils\")] and td/span[contains(text(),\"Kansas City\")] and td/span[contains(text(),\"Arkansas\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Spartans\")] and td/span[contains(text(),\"Tucson\")] and td/span[contains(text(),\"Florida\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Spartans\")] and td/span[contains(text(),\"Portland\")] and td/span[contains(text(),\"Delaware\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Braves\")] and td/span[contains(text(),\"San Antonio\")] and td/span[contains(text(),\"Connecticut\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Trojans\")] and td/span[contains(text(),\"San José\")] and td/span[contains(text(),\"Colorado\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Braves\")] and td/span[contains(text(),\"Omahaff\")] and td/span[contains(text(),\"California\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_teams')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Wolverines\")] and td/span[contains(text(),\"Sacramento\")] and td/span[contains(text(),\"California\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Tigers\")] and td/span[contains(text(),\"Charlotte\")] and td/span[contains(text(),\"Hawaii\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Chorgers\")] and td/span[contains(text(),\"Detroit\")] and td/span[contains(text(),\"Arizona\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Hawks\")] and td/span[contains(text(),\"Spring\")] and td/span[contains(text(),\"Colorado\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		// On vérifie que les joueurs des Braves - Omahaff sont bien supprimés.
		// On retourne sur la première page de Players.
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Wood\")] and td/span[contains(text(),\"Tommy\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Nash\")] and td/span[contains(text(),\"Sylvester\")]]"));
		selenium.click("xpath= (//span[4]/a)[4]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Wood\")] and td/span[contains(text(),\"Tommy\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Nash\")] and td/span[contains(text(),\"Sylvester\")]]"));
		selenium.click("xpath= (//span[4]/a)[6]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Wood\")] and td/span[contains(text(),\"Tommy\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Nash\")] and td/span[contains(text(),\"Sylvester\")]]"));
		selenium.click("xpath= (//span[4]/a)[6]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Wood\")] and td/span[contains(text(),\"Tommy\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Nash\")] and td/span[contains(text(),\"Sylvester\")]]"));
		// On retourne sur la première page des Players.
		selenium.click("xpath= (//span[4]/a)[1]");
		selenium.waitForPageToLoad("30000");
		// On vérifie les Coachs.
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_coachs')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Harry\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Loren\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Bob\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Gerald\")]]"));
		selenium.click("xpath= (//span[6]/a)[4]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_coachs')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Louis\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Cameron\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Shane\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Keith\")]]"));
		selenium.click("xpath= (//span[6]/a)[6]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_coachs')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Ivan\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Tommy\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Sylvester\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Michael\")]]"));
		selenium.click("xpath= (//span[6]/a)[6]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("2", selenium.getXpathCount("//table[contains(@id,'tab_coachs')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Jerome\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Woodrow\")]]"));
		selenium.click("xpath= (//span[6]/a)[1]");
		selenium.waitForPageToLoad("30000");
		// On vérife les States.
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_state')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"California\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Connecticut\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Delaware\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Florida\")] and td/a[contains(text(),\"[delete]\")]]"));
		selenium.click("xpath= (//span[8]/a)[3]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_state')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Hawaii\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Iowa\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Alabama\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Arizona\")] and td/a[contains(text(),\"[delete]\")]]"));
		selenium.click("xpath= (//span[8]/a)[5]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("3", selenium.getXpathCount("//table[contains(@id,'tab_state')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Arkansas\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Alaska\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Colorado\")] and td/a[contains(text(),\"[delete]\")]]"));
		selenium.click("xpath= (//span[8]/a)[1]");
		selenium.waitForPageToLoad("30000");
		// On vérifie les audiences.
		// On vérifie que les Braves n'apparaissent plus.
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_audiences')]/tbody/tr[td/span[contains(text(),\"Braves\")]]"));
		verifyEquals("3", selenium.getXpathCount("//table[contains(@id,'tab_audiences')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_audiences')]/tbody/tr[td/span[contains(text(),\"Tigers\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_audiences')]/tbody/tr[td/span[contains(text(),\"Chargers\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_audiences')]/tbody/tr[td/span[contains(text(),\"Hawks\")]]"));
		// Pour les Tigers :
		// On indexe la ligne.
		Number indexTigers = selenium.getElementIndex("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Tigers\")] and td/span[contains(text(),\"Charlotte\")] and td/span[contains(text(),\"Hawaii\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]");
		System.out.println("variable index vaut : " + indexTigers);
		// On clique sur [delete]
		selenium.click("//table[contains(@id,'tab_teams')]/tbody/tr[" + indexTigers + " + 1]/td/a[contains(text(),\"[delete]\")]");
		selenium.waitForPageToLoad("30000");
		// On vérifie le tableau Teams.
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Mustangs\")] and td/span[contains(text(),\"Fresno\")] and td/span[contains(text(),\"Hawaii\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Red Devils\")] and td/span[contains(text(),\"Kansas City\")] and td/span[contains(text(),\"Arkansas\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Spartans\")] and td/span[contains(text(),\"Tucson\")] and td/span[contains(text(),\"Florida\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Spartans\")] and td/span[contains(text(),\"Portland\")] and td/span[contains(text(),\"Delaware\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Braves\")] and td/span[contains(text(),\"San Antonio\")] and td/span[contains(text(),\"Connecticut\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Trojans\")] and td/span[contains(text(),\"San José\")] and td/span[contains(text(),\"Colorado\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Braves\")] and td/span[contains(text(),\"Omahaff\")] and td/span[contains(text(),\"California\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Tigers\")] and td/span[contains(text(),\"Charlotte\")] and td/span[contains(text(),\"Hawaii\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		verifyEquals("3", selenium.getXpathCount("//table[contains(@id,'tab_teams')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Wolverines\")] and td/span[contains(text(),\"Sacramento\")] and td/span[contains(text(),\"California\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Chorgers\")] and td/span[contains(text(),\"Detroit\")] and td/span[contains(text(),\"Arizona\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Hawks\")] and td/span[contains(text(),\"Spring\")] and td/span[contains(text(),\"Colorado\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		// On vérifie que les joueurs des Tigers sont bien supprimés.
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Woodrow\")] and td/span[contains(text(),\"Carpenter\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Pope\")] and td/span[contains(text(),\"Ruben\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Clark\")] and td/span[contains(text(),\"Bob\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Frank\")] and td/span[contains(text(),\"Oliver\")]]"));
		selenium.click("xpath= (//span[4]/a)[3]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Woodrow\")] and td/span[contains(text(),\"Carpenter\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Pope\")] and td/span[contains(text(),\"Ruben\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Clark\")] and td/span[contains(text(),\"Bob\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Frank\")] and td/span[contains(text(),\"Oliver\")]]"));
		selenium.click("xpath= (//span[4]/a)[5]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Woodrow\")] and td/span[contains(text(),\"Carpenter\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Pope\")] and td/span[contains(text(),\"Ruben\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Clark\")] and td/span[contains(text(),\"Bob\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Frank\")] and td/span[contains(text(),\"Oliver\")]]"));
		// On retourne sur la première page de Players.
		selenium.click("xpath= (//span[4]/a)[1]");
		selenium.waitForPageToLoad("30000");
		// On vérifie les Coachs.
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_coachs')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Harry\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Loren\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Bob\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Gerald\")]]"));
		selenium.click("xpath= (//span[6]/a)[4]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_coachs')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Louis\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Cameron\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Shane\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Keith\")]]"));
		selenium.click("xpath= (//span[6]/a)[6]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_coachs')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Ivan\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Tommy\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Sylvester\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Michael\")]]"));
		selenium.click("xpath= (//span[6]/a)[6]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("2", selenium.getXpathCount("//table[contains(@id,'tab_coachs')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Jerome\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Woodrow\")]]"));
		selenium.click("xpath= (//span[6]/a)[1]");
		selenium.waitForPageToLoad("30000");
		// On vérife les States.
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_state')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"California\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Connecticut\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Delaware\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Florida\")] and td/a[contains(text(),\"[delete]\")]]"));
		selenium.click("xpath= (//span[8]/a)[3]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_state')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Hawaii\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Iowa\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Alabama\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Arizona\")] and td/a[contains(text(),\"[delete]\")]]"));
		selenium.click("xpath= (//span[8]/a)[5]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("3", selenium.getXpathCount("//table[contains(@id,'tab_state')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Arkansas\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Alaska\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Colorado\")] and td/a[contains(text(),\"[delete]\")]]"));
		selenium.click("xpath= (//span[8]/a)[1]");
		selenium.waitForPageToLoad("30000");
		// On vérifie les audiences.
		// On vérifie que les Tigers n'apparaissent plus.
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_audiences')]/tbody/tr[td/span[contains(text(),\"Braves\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_audiences')]/tbody/tr[td/span[contains(text(),\"Tigers\")]]"));
		verifyEquals("2", selenium.getXpathCount("//table[contains(@id,'tab_audiences')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_audiences')]/tbody/tr[td/span[contains(text(),\"Chargers\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_audiences')]/tbody/tr[td/span[contains(text(),\"Hawks\")]]"));
		// Pour les Hawks :
		// On indexe la ligne.
		Number indexHawks = selenium.getElementIndex("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Hawks\")] and td/span[contains(text(),\"Spring\")] and td/span[contains(text(),\"Colorado\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]");
		System.out.println("variable index vaut : " + indexHawks);
		// On clique sur [delete]
		selenium.click("//table[contains(@id,'tab_teams')]/tbody/tr[" + indexHawks + " + 1]/td/a[contains(text(),\"[delete]\")]");
		selenium.waitForPageToLoad("30000");
		// On vérifie le tableau Teams.
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Mustangs\")] and td/span[contains(text(),\"Fresno\")] and td/span[contains(text(),\"Hawaii\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Red Devils\")] and td/span[contains(text(),\"Kansas City\")] and td/span[contains(text(),\"Arkansas\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Spartans\")] and td/span[contains(text(),\"Tucson\")] and td/span[contains(text(),\"Florida\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Spartans\")] and td/span[contains(text(),\"Portland\")] and td/span[contains(text(),\"Delaware\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Braves\")] and td/span[contains(text(),\"San Antonio\")] and td/span[contains(text(),\"Connecticut\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Trojans\")] and td/span[contains(text(),\"San José\")] and td/span[contains(text(),\"Colorado\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Braves\")] and td/span[contains(text(),\"Omahaff\")] and td/span[contains(text(),\"California\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Tigers\")] and td/span[contains(text(),\"Charlotte\")] and td/span[contains(text(),\"Hawaii\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Hawks\")] and td/span[contains(text(),\"Spring\")] and td/span[contains(text(),\"Colorado\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		verifyEquals("2", selenium.getXpathCount("//table[contains(@id,'tab_teams')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Wolverines\")] and td/span[contains(text(),\"Sacramento\")] and td/span[contains(text(),\"California\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Chorgers\")] and td/span[contains(text(),\"Detroit\")] and td/span[contains(text(),\"Arizona\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		// On vérifie que les joueurs des Hawks sont bien supprimés.
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Grant\")] and td/span[contains(text(),\"Jerome\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Christian\")] and td/span[contains(text(),\"Chavez\")]]"));
		selenium.click("xpath= (//span[4]/a)[2]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("3", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Grant\")] and td/span[contains(text(),\"Jerome\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Christian\")] and td/span[contains(text(),\"Chavez\")]]"));
		// On retourne sur la première page de Players.
		selenium.click("xpath= (//span[4]/a)[1]");
		selenium.waitForPageToLoad("30000");
		// On vérifie les Coachs.
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_coachs')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Harry\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Loren\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Bob\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Gerald\")]]"));
		selenium.click("xpath= (//span[6]/a)[4]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_coachs')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Louis\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Cameron\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Shane\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Keith\")]]"));
		selenium.click("xpath= (//span[6]/a)[6]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_coachs')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Ivan\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Tommy\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Sylvester\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Michael\")]]"));
		selenium.click("xpath= (//span[6]/a)[6]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("2", selenium.getXpathCount("//table[contains(@id,'tab_coachs')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Jerome\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Woodrow\")]]"));
		selenium.click("xpath= (//span[6]/a)[1]");
		selenium.waitForPageToLoad("30000");
		// On vérifie les States
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_state')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"California\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Connecticut\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Delaware\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Florida\")] and td/a[contains(text(),\"[delete]\")]]"));
		selenium.click("xpath= (//span[8]/a)[3]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_state')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Hawaii\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Iowa\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Alabama\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Arizona\")] and td/a[contains(text(),\"[delete]\")]]"));
		selenium.click("xpath= (//span[8]/a)[5]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("3", selenium.getXpathCount("//table[contains(@id,'tab_state')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Arkansas\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Alaska\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Colorado\")] and td/a[contains(text(),\"[delete]\")]]"));
		selenium.click("xpath= (//span[8]/a)[1]");
		selenium.waitForPageToLoad("30000");
		// On vérifie les audiences.
		// On vérifie que les Hawks n'apparaissent plus.
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_audiences')]/tbody/tr[td/span[contains(text(),\"Braves\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_audiences')]/tbody/tr[td/span[contains(text(),\"Tigers\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_audiences')]/tbody/tr[td/span[contains(text(),\"Hawks\")]]"));
		verifyEquals("1", selenium.getXpathCount("//table[contains(@id,'tab_audiences')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_audiences')]/tbody/tr[td/span[contains(text(),\"Chargers\")]]"));
		// Pour les Chargers2 :
		// On indexe la ligne.
		Number indexChorgers = selenium.getElementIndex("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Chorgers\")] and td/span[contains(text(),\"Detroit\")] and td/span[contains(text(),\"Arizona\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]");
		System.out.println("variable index vaut : " + indexChorgers);
		// On clique sur [delete]
		selenium.click("//table[contains(@id,'tab_teams')]/tbody/tr[" + indexChorgers + " + 1]/td/a[contains(text(),\"[delete]\")]");
		selenium.waitForPageToLoad("30000");
		// On vérifie le tableau Teams.
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Mustangs\")] and td/span[contains(text(),\"Fresno\")] and td/span[contains(text(),\"Hawaii\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Red Devils\")] and td/span[contains(text(),\"Kansas City\")] and td/span[contains(text(),\"Arkansas\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Spartans\")] and td/span[contains(text(),\"Tucson\")] and td/span[contains(text(),\"Florida\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Spartans\")] and td/span[contains(text(),\"Portland\")] and td/span[contains(text(),\"Delaware\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Braves\")] and td/span[contains(text(),\"San Antonio\")] and td/span[contains(text(),\"Connecticut\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Trojans\")] and td/span[contains(text(),\"San José\")] and td/span[contains(text(),\"Colorado\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Braves\")] and td/span[contains(text(),\"Omahaff\")] and td/span[contains(text(),\"California\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Tigers\")] and td/span[contains(text(),\"Charlotte\")] and td/span[contains(text(),\"Hawaii\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Hawks\")] and td/span[contains(text(),\"Spring\")] and td/span[contains(text(),\"Colorado\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Chargers2\")] and td/span[contains(text(),\"Detroit\")] and td/span[contains(text(),\"Arizona\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		verifyEquals("1", selenium.getXpathCount("//table[contains(@id,'tab_teams')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Wolverines\")] and td/span[contains(text(),\"Sacramento\")] and td/span[contains(text(),\"California\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		// On vérifie que les joueurs des Chargers2 sont bien supprimés.
		verifyEquals("3", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Harry\")] and td/span[contains(text(),\"Davis\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Ivan\")] and td/span[contains(text(),\"Tate\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Cecil\")] and td/span[contains(text(),\"Conner\")]]"));
		// On retourne sur la première page de Players.
//		selenium.click("xpath=(//*[img[translate(@alt,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')=\"first\"]])[1]");
//		selenium.waitForPageToLoad("30000");
		// On vérifie les Coachs.
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_coachs')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Harry\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Loren\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Bob\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Gerald\")]]"));
		selenium.click("xpath= (//span[6]/a)[4]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_coachs')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Louis\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Cameron\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Shane\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Keith\")]]"));
		selenium.click("xpath= (//span[6]/a)[6]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_coachs')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Ivan\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Tommy\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Sylvester\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Michael\")]]"));
		selenium.click("xpath= (//span[6]/a)[6]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("2", selenium.getXpathCount("//table[contains(@id,'tab_coachs')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Jerome\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Woodrow\")]]"));
		selenium.click("xpath= (//span[6]/a)[1]");
		selenium.waitForPageToLoad("30000");
		// On vérifie les States.
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_state')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"California\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Connecticut\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Delaware\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Florida\")] and td/a[contains(text(),\"[delete]\")]]"));
		selenium.click("xpath= (//span[8]/a)[3]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_state')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Hawaii\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Iowa\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Alabama\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Arizona\")] and td/a[contains(text(),\"[delete]\")]]"));
		selenium.click("xpath= (//span[8]/a)[5]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("3", selenium.getXpathCount("//table[contains(@id,'tab_state')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Arkansas\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Alaska\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Colorado\")] and td/a[contains(text(),\"[delete]\")]]"));
		selenium.click("xpath= (//span[8]/a)[1]");
		selenium.waitForPageToLoad("30000");
		// On vérifie les audiences.
		// On vérifie que les Chargers n'apparaissent plus.
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_audiences')]/tbody/tr[td/span[contains(text(),\"Braves\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_audiences')]/tbody/tr[td/span[contains(text(),\"Tigers\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_audiences')]/tbody/tr[td/span[contains(text(),\"Hawks\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_audiences')]/tbody/tr[td/span[contains(text(),\"Chargers\")]]"));
		String url = selenium.getLocation();
		String count = String.valueOf(url.indexOf("Struts") != -1 ? 1 : 0);
		verifyEquals(count, selenium.getXpathCount("//table[contains(@id,'tab_audiences')]//tbody//tr"));
		// Pour les Wolverines :
		// On indexe la ligne.
		indexWolverines = selenium.getElementIndex("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Wolverines\")] and td/span[contains(text(),\"Sacramento\")] and td/span[contains(text(),\"California\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]");
		System.out.println("variable index vaut : " + indexWolverines);
		// On clique sur [delete]
		selenium.click("//table[contains(@id,'tab_teams')]/tbody/tr[" + indexWolverines + " + 1]/td/a[contains(text(),\"[delete]\")]");
		selenium.waitForPageToLoad("30000");
		// On vérifie le tableau Teams.
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Mustangs\")] and td/span[contains(text(),\"Fresno\")] and td/span[contains(text(),\"Hawaii\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Red Devils\")] and td/span[contains(text(),\"Kansas City\")] and td/span[contains(text(),\"Arkansas\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Spartans\")] and td/span[contains(text(),\"Tucson\")] and td/span[contains(text(),\"Florida\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Spartans\")] and td/span[contains(text(),\"Portland\")] and td/span[contains(text(),\"Delaware\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Braves\")] and td/span[contains(text(),\"San Antonio\")] and td/span[contains(text(),\"Connecticut\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Trojans\")] and td/span[contains(text(),\"San José\")] and td/span[contains(text(),\"Colorado\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Braves\")] and td/span[contains(text(),\"Omahaff\")] and td/span[contains(text(),\"California\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Tigers\")] and td/span[contains(text(),\"Charlotte\")] and td/span[contains(text(),\"Hawaii\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Hawks\")] and td/span[contains(text(),\"Spring\")] and td/span[contains(text(),\"Colorado\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Chargers2\")] and td/span[contains(text(),\"Detroit\")] and td/span[contains(text(),\"Arizona\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Wolverines\")] and td/span[contains(text(),\"Sacramento\")] and td/span[contains(text(),\"California\")] and td/a[contains(text(),\"[delete]\")] and td/a[contains(text(),\"[details]\")]]"));
		url = selenium.getLocation();
		count = String.valueOf(url.indexOf("Struts") != -1 ? 1 : 0);
		verifyEquals(count, selenium.getXpathCount("//table[contains(@id,'tab_team')]//tbody//tr"));
		// On vérifie que les joueurs des Wolverines sont bien supprimés.
		url = selenium.getLocation();
		count = String.valueOf(url.indexOf("Struts") != -1 ? 1 : 0);
		verifyEquals(count, selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Enrique\")] and td/span[contains(text(),\"Wilkins\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Nelson\")] and td/span[contains(text(),\"Davis\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Michael\")] and td/span[contains(text(),\"Simon\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Ivan\")] and td/span[contains(text(),\"Tate\")]]"));
		// On vérifie les Coachs.
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_coachs')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Harry\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Loren\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Bob\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Gerald\")]]"));
		selenium.click("xpath= (//span[6]/a)[4]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_coachs')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Louis\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Cameron\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Shane\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Keith\")]]"));
		selenium.click("xpath= (//span[6]/a)[6]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_coachs')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Ivan\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Tommy\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Sylvester\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Michael\")]]"));
		selenium.click("xpath= (//span[6]/a)[6]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("2", selenium.getXpathCount("//table[contains(@id,'tab_coachs')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Jerome\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_coachs')]/tbody/tr[td/span[contains(text(),\"Woodrow\")]]"));
		selenium.click("xpath= (//span[6]/a)[1]");
		selenium.waitForPageToLoad("30000");
		// On vérifie les States.
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_state')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"California\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Connecticut\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Delaware\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Florida\")] and td/a[contains(text(),\"[delete]\")]]"));
		selenium.click("xpath= (//span[8]/a)[3]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_state')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Hawaii\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Iowa\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Alabama\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Arizona\")] and td/a[contains(text(),\"[delete]\")]]"));
		selenium.click("xpath= (//span[8]/a)[5]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("3", selenium.getXpathCount("//table[contains(@id,'tab_state')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Arkansas\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Alaska\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Colorado\")] and td/a[contains(text(),\"[delete]\")]]"));
		selenium.click("xpath= (//span[8]/a)[1]");
		selenium.waitForPageToLoad("30000");
		// On vérifie les audiences.
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_audiences')]/tbody/tr[td/span[contains(text(),\"Braves\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_audiences')]/tbody/tr[td/span[contains(text(),\"Tigers\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_audiences')]/tbody/tr[td/span[contains(text(),\"Hawks\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_audiences')]/tbody/tr[td/span[contains(text(),\"Chargers\")]]"));
		url = selenium.getLocation();
		count = String.valueOf(url.indexOf("Struts") != -1 ? 1 : 0);
		verifyEquals(count, selenium.getXpathCount("//table[contains(@id,'tab_audiences')]//tbody//tr"));
		// On supprime maintenant tous les States.
		// Pour ce faire, on indexe la ligne de chaque State.
		// On clique sur [delete]
		// On vérifie les données affichées/supprimées.
		// California :
		indexCalifornia = selenium.getElementIndex("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"California\")] and td/a[contains(text(),\"[delete]\")]]");
		System.out.println("variable index vaut : " + indexCalifornia);
		// On clique sur [delete]
		selenium.click("//table[contains(@id,'tab_states')]/tbody/tr[" + indexCalifornia + " + 1]/td/a[contains(text(),\"[delete]\")]");
		selenium.waitForPageToLoad("30000");
		// On vérifie les données States :
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"California\")] and td/a[contains(text(),\"[delete]\")]]"));
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_state')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Connecticut\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Delaware\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Florida\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Hawaii\")] and td/a[contains(text(),\"[delete]\")]]"));
		selenium.click("xpath= (//span[8]/a)[3]");
		selenium.waitForPageToLoad("30000");
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"California\")] and td/a[contains(text(),\"[delete]\")]]"));
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_state')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Iowa\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Alabama\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Arizona\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Arkansas\")] and td/a[contains(text(),\"[delete]\")]]"));
		selenium.click("xpath= (//span[8]/a)[5]");
		selenium.waitForPageToLoad("30000");
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"California\")] and td/a[contains(text(),\"[delete]\")]]"));
		verifyEquals("2", selenium.getXpathCount("//table[contains(@id,'tab_state')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Alaska\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Colorado\")] and td/a[contains(text(),\"[delete]\")]]"));
		// On retourne sur la première page du tableau States.
		selenium.click("xpath= (//span[8]/a)[1]");
		selenium.waitForPageToLoad("30000");
		// Connecticut :
		Number indexConnecticut = selenium.getElementIndex("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Connecticut\")] and td/a[contains(text(),\"[delete]\")]]");
		System.out.println("variable index vaut : " + indexConnecticut);
		// On clique sur [delete]
		selenium.click("//table[contains(@id,'tab_state')]/tbody/tr[" + indexConnecticut + " + 1]/td/a[contains(text(),\"[delete]\")]");
		selenium.waitForPageToLoad("30000");
		// On vérifie les données States :
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"California\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Connecticut\")] and td/a[contains(text(),\"[delete]\")]]"));
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_state')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Delaware\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Florida\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Hawaii\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Iowa\")] and td/a[contains(text(),\"[delete]\")]]"));
		selenium.click("xpath= (//span[8]/a)[3]");
		selenium.waitForPageToLoad("30000");
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"California\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Connecticut\")] and td/a[contains(text(),\"[delete]\")]]"));
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_state')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Alabama\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Arizona\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Arkansas\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Alaska\")] and td/a[contains(text(),\"[delete]\")]]"));
		selenium.click("xpath= (//span[8]/a)[5]");
		selenium.waitForPageToLoad("30000");
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Alaska\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Connecticut\")] and td/a[contains(text(),\"[delete]\")]]"));
		verifyEquals("1", selenium.getXpathCount("//table[contains(@id,'tab_state')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Colorado\")] and td/a[contains(text(),\"[delete]\")]]"));
		// On retourne sur la première page du tableau States.
		selenium.click("xpath= (//span[8]/a)[1]");
		selenium.waitForPageToLoad("30000");
		// Delaware :
		Number indexDelaware = selenium.getElementIndex("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Delaware\")] and td/a[contains(text(),\"[delete]\")]]");
		System.out.println("variable index vaut : " + indexDelaware);
		// On clique sur [delete]
		selenium.click("//table[contains(@id,'tab_state')]/tbody/tr[" + indexDelaware + " + 1]/td/a[contains(text(),\"[delete]\")]");
		selenium.waitForPageToLoad("30000");
		// On vérifie les données States :
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"California\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Connecticut\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Delaware\")] and td/a[contains(text(),\"[delete]\")]]"));
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_state')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Florida\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Hawaii\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Iowa\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Alabama\")] and td/a[contains(text(),\"[delete]\")]]"));
		selenium.click("xpath= (//span[8]/a)[2]");
		selenium.waitForPageToLoad("30000");
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"California\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Connecticut\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Delaware\")] and td/a[contains(text(),\"[delete]\")]]"));
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_state')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Arizona\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Arkansas\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Alaska\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Colorado\")] and td/a[contains(text(),\"[delete]\")]]"));
		// On retourne sur la première page du tableau States.
		selenium.click("xpath= (//span[8]/a)[1]");
		selenium.waitForPageToLoad("30000");
		// Florida :
		Number indexFlorida = selenium.getElementIndex("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Florida\")] and td/a[contains(text(),\"[delete]\")]]");
		System.out.println("variable index vaut : " + indexFlorida);
		// On clique sur [delete]
		selenium.click("//table[contains(@id,'tab_state')]/tbody/tr[" + indexFlorida + " + 1]/td/a[contains(text(),\"[delete]\")]");
		selenium.waitForPageToLoad("30000");
		// On vérifie les données States :
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"California\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Connecticut\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Delaware\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Florida\")] and td/a[contains(text(),\"[delete]\")]]"));
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_state')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Hawaii\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Iowa\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Alabama\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Arizona\")] and td/a[contains(text(),\"[delete]\")]]"));
		selenium.click("xpath= (//span[8]/a)[2]");
		selenium.waitForPageToLoad("30000");
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"California\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Connecticut\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Delaware\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Florida\")] and td/a[contains(text(),\"[delete]\")]]"));
		verifyEquals("3", selenium.getXpathCount("//table[contains(@id,'tab_state')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Arkansas\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Alaska\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Colorado\")] and td/a[contains(text(),\"[delete]\")]]"));
		// On retourne sur la première page du tableau States.
		selenium.click("xpath= (//span[8]/a)[1]");
		selenium.waitForPageToLoad("30000");
		// Hawaii :
		Number indexHawaii = selenium.getElementIndex("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Hawaii\")] and td/a[contains(text(),\"[delete]\")]]");
		System.out.println("variable index vaut : " + indexHawaii);
		// On clique sur [delete]
		selenium.click("//table[contains(@id,'tab_state')]/tbody/tr[" + indexHawaii + " + 1]/td/a[contains(text(),\"[delete]\")]");
		selenium.waitForPageToLoad("30000");
		// On vérifie les données States :
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"California\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Connecticut\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Delaware\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Florida\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Hawaii\")] and td/a[contains(text(),\"[delete]\")]]"));
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_state')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Iowa\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Alabama\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Arizona\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Arkansas\")] and td/a[contains(text(),\"[delete]\")]]"));
		selenium.click("xpath= (//span[8]/a)[2]");
		selenium.waitForPageToLoad("30000");
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"California\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Connecticut\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Delaware\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Florida\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Hawaii\")] and td/a[contains(text(),\"[delete]\")]]"));
		verifyEquals("2", selenium.getXpathCount("//table[contains(@id,'tab_state')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Alaska\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Colorado\")] and td/a[contains(text(),\"[delete]\")]]"));
		// On retourne sur la première page du tableau States.
		selenium.click("xpath= (//span[8]/a)[1]");
		selenium.waitForPageToLoad("30000");
		// Iowa :
		Number indexIowa = selenium.getElementIndex("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Iowa\")] and td/a[contains(text(),\"[delete]\")]]");
		System.out.println("variable index vaut : " + indexIowa);
		// On clique sur [delete]
		selenium.click("//table[contains(@id,'tab_state')]/tbody/tr[" + indexIowa + " + 1]/td/a[contains(text(),\"[delete]\")]");
		selenium.waitForPageToLoad("30000");
		// On vérifie les données States :
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"California\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Connecticut\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Delaware\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Florida\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Hawaii\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Iowa\")] and td/a[contains(text(),\"[delete]\")]]"));
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_state')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Alabama\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Arizona\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Arkansas\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Alaska\")] and td/a[contains(text(),\"[delete]\")]]"));
		selenium.click("xpath= (//span[8]/a)[2]");
		selenium.waitForPageToLoad("30000");
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"California\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Connecticut\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Delaware\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Florida\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Hawaii\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Iowa\")] and td/a[contains(text(),\"[delete]\")]]"));
		verifyEquals("1", selenium.getXpathCount("//table[contains(@id,'tab_state')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Colorado\")] and td/a[contains(text(),\"[delete]\")]]"));
		// On retourne sur la première page du tableau States.
		selenium.click("xpath= (//span[8]/a)[1]");
		selenium.waitForPageToLoad("30000");
		// Alabama :
		Number indexAlabama = selenium.getElementIndex("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Alabama\")] and td/a[contains(text(),\"[delete]\")]]");
		System.out.println("variable index vaut : " + indexAlabama);
		// On clique sur [delete]
		selenium.click("//table[contains(@id,'tab_state')]/tbody/tr[" + indexAlabama + " + 1]/td/a[contains(text(),\"[delete]\")]");
		selenium.waitForPageToLoad("30000");
		// On vérifie les données States :
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"California\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Connecticut\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Delaware\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Florida\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Hawaii\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Iowa\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Alabama\")] and td/a[contains(text(),\"[delete]\")]]"));
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_state')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Arizona\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Arkansas\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Alaska\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Colorado\")] and td/a[contains(text(),\"[delete]\")]]"));
		// Arizona :
		Number indexArizona = selenium.getElementIndex("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Arizona\")] and td/a[contains(text(),\"[delete]\")]]");
		System.out.println("variable index vaut : " + indexArizona);
		// On clique sur [delete]
		selenium.click("//table[contains(@id,'tab_state')]/tbody/tr[" + indexArizona + " + 1]/td/a[contains(text(),\"[delete]\")]");
		selenium.waitForPageToLoad("30000");
		// On vérifie les données States :
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"California\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Connecticut\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Delaware\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Florida\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Hawaii\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Iowa\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Alabama\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Arizona\")] and td/a[contains(text(),\"[delete]\")]]"));
		verifyEquals("3", selenium.getXpathCount("//table[contains(@id,'tab_state')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Arkansas\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Alaska\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Colorado\")] and td/a[contains(text(),\"[delete]\")]]"));
		// Arkansas :
		Number indexArkansas = selenium.getElementIndex("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Arkansas\")] and td/a[contains(text(),\"[delete]\")]]");
		System.out.println("variable index vaut : " + indexArkansas);
		// On clique sur [delete]
		selenium.click("//table[contains(@id,'tab_state')]/tbody/tr[" + indexArkansas + " + 1]/td/a[contains(text(),\"[delete]\")]");
		selenium.waitForPageToLoad("30000");
		// On vérifie les données States :
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"California\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Connecticut\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Delaware\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Florida\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Hawaii\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Iowa\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Alabama\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Arizona\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Arkansas\")] and td/a[contains(text(),\"[delete]\")]]"));
		verifyEquals("2", selenium.getXpathCount("//table[contains(@id,'tab_state')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Alaska\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Colorado\")] and td/a[contains(text(),\"[delete]\")]]"));
		// Alaska :
		Number indexAlaska = selenium.getElementIndex("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Alaska\")] and td/a[contains(text(),\"[delete]\")]]");
		System.out.println("variable index vaut : " + indexAlaska);
		// On clique sur [delete]
		selenium.click("//table[contains(@id,'tab_state')]/tbody/tr[" + indexAlaska + " + 1]/td/a[contains(text(),\"[delete]\")]");
		selenium.waitForPageToLoad("30000");
		// On vérifie les données States :
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"California\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Connecticut\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Delaware\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Florida\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Hawaii\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Iowa\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Alabama\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Arizona\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Arkansas\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Alaska\")] and td/a[contains(text(),\"[delete]\")]]"));
		verifyEquals("1", selenium.getXpathCount("//table[contains(@id,'tab_state')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Colorado\")] and td/a[contains(text(),\"[delete]\")]]"));
		// Colorado :
		Number indexColorado = selenium.getElementIndex("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Colorado\")] and td/a[contains(text(),\"[delete]\")]]");
		System.out.println("variable index vaut : " + indexColorado);
		// On clique sur [delete]
		selenium.click("//table[contains(@id,'tab_state')]/tbody/tr[" + indexColorado + " + 1]/td/a[contains(text(),\"[delete]\")]");
		selenium.waitForPageToLoad("30000");
		// On vérifie les données States :
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"California\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Connecticut\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Delaware\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Florida\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Hawaii\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Iowa\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Alabama\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Arizona\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Arkansas\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Alaska\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_state')]/tbody/tr[td/span[contains(text(),\"Colorado\")] and td/a[contains(text(),\"[delete]\")]]"));
		verifyEquals("0", selenium.getXpathCount("//table[contains(@id,'tab_state')]//tbody//tr"));
	}

	
}
