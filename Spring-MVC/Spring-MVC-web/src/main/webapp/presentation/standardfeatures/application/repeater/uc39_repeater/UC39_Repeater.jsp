<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file="/include.jsp"  %>
<html >
<head >
<title  > 
<fmt:message key="cUC39_Repeater_BLU_AGE_DOCUMENTATION"/>
</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"  > 
</meta>
<link href="<c:url value= "/presentation/_style/_style.css"/>" rel="stylesheet" type="text/css"  /> 
<link href="<c:url value= "/presentation/_include/jquery/rd_lib.css"/>" rel="stylesheet" type="text/css"  /> 
<script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/jquery.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/rd_setup.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/rd_lib.js"/>" space="preserve"  ></script><link rel="shortcut icon" href="<c:url value= "/presentation/_style/favicon.ico"/>"  /> 

</head>

<body class=" " > 
<div id="rd_ghost"  > 
<small  > 
<fmt:message key="cUC39_Repeater___CSS_DEACTIVATED"/>
</small>
</div>
<div id="rd_jghost"  > 
<small  > 
<fmt:message key="cUC39_Repeater___JAVASCRIPT_DEACTIVATED"/>
</small>
</div>
<div id="shape"  > 
<div class="ui-layout-center"  > 
<div class="east-north"  > 
	<form:form id="uC39_Repeater" commandName="uC39_RepeaterForm"  enctype="application/x-www-form-urlencoded" method="post">
<h2 class="rd_first"  > 
<fmt:message key="cUC39_Repeater_STANDARD_FEATURES__APPLICATIO"/>
</h2>
<h4  > 
<fmt:message key="cUC39_Repeater_REGULAR_USE"/>
</h4>
<c:forEach items="${uC39_RepeaterForm.teams1}" var="varrpt_teams1" varStatus="_countrpt_teams1" > 
<fmt:message key="cUC39_Repeater________TEAM"/>
<b  > 
		<span id="dyn_name1">
	<c:out value="${varrpt_teams1.name}" />
		</span>
	</b>
<fmt:message key="cUC39_Repeater________PLAYS_IN_______"/>
		<span id="dyn_city1">
	<c:out value="${varrpt_teams1.city}" />
		</span>
	<br clear="none"  /> 
</c:forEach> 
<br clear="none"  /> 
<h4  > 
<fmt:message key="cUC39_Repeater_TABLE_REPEATER"/>
</h4>
<table cellpadding="0" cellspacing="0"  >
<tr  > 
<th align="left" width="150" rowspan="1" colspan="1"  > 
<fmt:message key="cUC39_Repeater_TEAM_NAME"/>
</th>
<th align="left" rowspan="1" colspan="1"  > 
<fmt:message key="cUC39_Repeater_CITY"/>
</th>
</tr>
<c:forEach items="${uC39_RepeaterForm.teams2}" var="varrpt_teams2" varStatus="_countrpt_teams2" > 
<tr  > 
<td colspan="1" rowspan="1"  >
		<span id="dyn_city2">
	<c:out value="${varrpt_teams2.city}" />
		</span>
	</td> 
<td colspan="1" rowspan="1"  >
		<span id="dyn_name2">
	<c:out value="${varrpt_teams2.name}" />
		</span>
	</td> 
</tr>
</c:forEach> 
</table> 
<br clear="none"  /> 
<h4  > 
<fmt:message key="cUC39_Repeater_LIST_REPEATER"/>
</h4>
<ul  > 
<c:forEach items="${uC39_RepeaterForm.teams3}" var="varrpt_teams3" varStatus="_countrpt_teams3" > 
<li  > 
<fmt:message key="cUC39_Repeater__________TEAM"/>
<b  > 
		<span id="dyn_name3">
	<c:out value="${varrpt_teams3.name}" />
		</span>
	</b>
<fmt:message key="cUC39_Repeater__________PLAYS_IN_________"/>
		<span id="dyn_city3">
	<c:out value="${varrpt_teams3.city}" />
		</span>
	</li>
</c:forEach> 
</ul>
	</form:form>
	<script language="javascript">
		function btn_click_cExecuteUC39_Repeater(url) {
			UC39_Repeatervar = document.getElementById('uC39_Repeater');
			UC39_Repeatervar.setAttribute('action', url);
			UC39_Repeatervar.submit();
		}
		function btn_click_cSetActionUC39_Repeater(url) {
			UC39_Repeatervar = document.getElementById('uC39_Repeater');
			UC39_Repeatervar.setAttribute('action', url);
		}
	</script>
</div>
<div class="east-center"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/application/repeater/uc39_repeater/UC39_Repeatercmp_tabs" />
</div>
</div>
<div class="ui-layout-north"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/application/repeater/uc39_repeater/UC39_Repeatercmp_header" />
</div>
<div class="ui-layout-south"  > 
<fmt:message key="cUC39_Repeater_______"/>
<span id="rd_jversion"  > 
<fmt:message key="cUC39_Repeater_BLU_AGE_DOCUMENTATION_129"/>
</span>
</div>
<div class="ui-layout-west"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/application/repeater/uc39_repeater/UC39_Repeatercmp_treeview" />
</div>
</div>
</body>
</html>
