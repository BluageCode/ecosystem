/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.advancedfeatures.valueobject.uc206_valueobject ;


import org.apache.log4j.Logger;
import org.springframework.validation.Errors;

import com.netfective.bluage.core.validator.AbstractValidator;
/**
* Class :UC206_SearchPlayerValidator
*/
public class UC206_SearchPlayerValidator extends AbstractValidator{
	
	// LOGGER for the class UC206_SearchPlayerValidator
	private static final Logger LOGGER = Logger.getLogger( UC206_SearchPlayerValidator.class);
	
	
	/**
	* Operation validate for UC206_SearchPlayerForm
	* @param obj : the current form (UC206_SearchPlayerForm)
	* @param errors : The spring errors to return for the form UC206_SearchPlayerForm
	*/
	public void validate(final Object obj,final Errors errors){
		LOGGER.info("Beginning method : validate"); // UC206_SearchPlayerValidator
		UC206_SearchPlayerForm cUC206_SearchPlayerForm = (UC206_SearchPlayerForm)obj; // UC206_SearchPlayerValidator
		LOGGER.info("Ending method : validate the form "+ cUC206_SearchPlayerForm.getClass().getName()); // UC206_SearchPlayerValidator
	}

	/**
	* Method to implements to use spring validators (UC206_SearchPlayerForm)
	* @param aClass : Class for the form UC206_SearchPlayerForm
	* @return boolean
	*/
	public boolean supports(final Class aClass){
		return (new UC206_SearchPlayerForm()).getClass().equals(aClass); // UC206_SearchPlayerValidator
	}
}
