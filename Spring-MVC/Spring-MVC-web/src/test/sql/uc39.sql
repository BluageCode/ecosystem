--<ScriptOptions statementTerminator=";"/>

ALTER TABLE team39 DROP CONSTRAINT team39_pkey;

DROP TABLE team39;

CREATE TABLE team39 (
		id INT8 NOT NULL,
		version INT4 NOT NULL,
		city VARCHAR(1024),
		name VARCHAR(1024),
		flag BOOL,
		picto_url BYTEA

	);

INSERT INTO team39 VALUES (1, 0, 'Charlotte', 'Tigers', false, NULL);
INSERT INTO team39 VALUES (3, 0, 'Detroit', 'Chargers', true, NULL);
INSERT INTO team39 VALUES (2, 0, 'Kansas City', 'Bears', true, NULL);
INSERT INTO team39 VALUES (4, 0, 'Detroit', 'Hawks', false, NULL);
INSERT INTO team39 VALUES (5, 0, 'Omaha', 'Braves', false, NULL);
INSERT INTO team39 VALUES (6, 0, 'Raleigh', 'Eagles', false, NULL);
INSERT INTO team39 VALUES (7, 0, 'Oakland', 'Cardinals', false, NULL);
INSERT INTO team39 VALUES (8, 0, 'Kansas City', 'Red Devils', false, NULL);
INSERT INTO team39 VALUES (9, 0, 'Fresno', 'Mustangs', false, NULL);
INSERT INTO team39 VALUES (10, 0, 'Sacramento', 'Wolverines', false, NULL);
INSERT INTO team39 VALUES (11, 0, 'Nashville', 'Crusaders', false, NULL);
INSERT INTO team39 VALUES (12, 0, 'Fort Worth', 'Trojans', true, NULL);
INSERT INTO team39 VALUES (13, 0, 'Tulsa', 'Vikings', true, NULL);
INSERT INTO team39 VALUES (14, 0, 'Charlotte', 'Blue Devils', true, NULL);
INSERT INTO team39 VALUES (15, 0, 'Cleveland', 'Mustangs', true, NULL);
INSERT INTO team39 VALUES (16, 0, 'Milwaukee', 'Rockets', true, NULL);
INSERT INTO team39 VALUES (17, 0, 'San Jose', 'Trojans', false, NULL);
INSERT INTO team39 VALUES (18, 0, 'San Antonio', 'Braves', false, NULL);
INSERT INTO team39 VALUES (19, 0, 'Portland', 'Spartans', true, NULL);
INSERT INTO team39 VALUES (20, 0, 'Tucson', 'Spartans', false, NULL);	
	
ALTER TABLE team39 ADD CONSTRAINT team39_pkey PRIMARY KEY (id);



