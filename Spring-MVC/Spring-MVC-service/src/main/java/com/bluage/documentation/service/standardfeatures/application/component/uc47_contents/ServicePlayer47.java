/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.service.standardfeatures.application.component.uc47_contents;


import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import com.bluage.documentation.business.standardfeatures.application.component.uc47_contents.bos.Player47BO;
import com.bluage.documentation.business.standardfeatures.application.component.uc47_contents.entities.dao.Player47DAOImpl;
import com.bluage.documentation.business.standardfeatures.application.component.uc47_contents.entities.daofinder.Player47DAOFinderImpl;
import com.bluage.documentation.service.AbstractService;
import com.netfective.bluage.core.exception.ApplicationException;

/**
 * Defines  application services for ServicePlayer47
  */
@Named("com.bluage.documentation.service.standardfeatures.application.component.uc47_contents.ServicePlayer47")
public class ServicePlayer47  extends AbstractService{


			@Inject
		@Named ("com.bluage.documentation.business.standardfeatures.application.component.uc47_contents.entities.dao.Player47DAOImpl")
		private Player47DAOImpl player47BODao;
			@Inject
		@Named ("com.bluage.documentation.business.standardfeatures.application.component.uc47_contents.entities.daofinder.Player47DAOFinderImpl")
		private Player47DAOFinderImpl player47BOFinder;
	


	/**
	 *  Default constructor 
	 */
	public ServicePlayer47() {
		super(); // Call super for default constructor : ServicePlayer47
	}
	



								
	/**    
	 * Operation Crud
	 * @return {@link List}
	 */
	public List player47FindAll( )
 throws ApplicationException {

					return player47BOFinder.findAll();
	}

					
				
	/**    
	 * Operation Crud
	 * @param bo {@link Player47BO}
		 * @return {@link Player47BO}
	 */
	public Player47BO player47FindByID(Player47BO bo )
 throws ApplicationException {

					return (Player47BO)player47BOFinder.findByPrimaryKey(getBoId(bo));
	}
	/**
	 *  Return the primary key from business object.
	 */
	private Object getBoId(Player47BO bo) {
				return bo.getId();
		}

					
				
	/**    
	 * Operation Crud
	 * @param bo {@link Player47BO}
		 * @return {@link boolean}
	 */
	public boolean player47Update(Player47BO bo )
 throws ApplicationException {

					return player47BODao.update(bo);
	}

		}
