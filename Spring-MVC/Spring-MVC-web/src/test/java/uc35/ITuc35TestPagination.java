package uc35;

import org.junit.Test;
import test.common.AbstractBADocTestCase;

public class ITuc35TestPagination extends AbstractBADocTestCase {
	
	@Override
	public  String getName(){
		return "UC-35";
	}
	
	@Test
	public void testITuc35TestPagination() throws Exception {
		// On teste l'UC35 - Stand features/Application/Datagrid - Paginator BDD
		selenium.open("");
		selenium.click("link=Expand All");
		selenium.waitForPageToLoad("30000");
		selenium.click("link=UC35 - Paginator BDD");
		verifyTrue(selenium.isElementPresent("//h2[contains(text(),\"Standard Features > Application > DataGrid > UC35 > Paginator BDD\")]"));
		// On clique sur [Display Players]
		selenium.click("//*[contains(@id,'lnk_paginatorbdd')]");
		selenium.waitForPageToLoad("30000");
		// On teste les liens.
		// On va sur la page 2.
		selenium.click("xpath=(//span/a)[8]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie qu'on est sur la page 2.
		assertTrue(selenium.isElementPresent("//a/b[contains(text(),\"2\")]|//strong[contains(text(),\"2\")]"));
//		// On va tester le lien fast next.
//		selenium.click("//a[img[@alt=\"Fast next\"]]");
//		selenium.waitForPageToLoad("30000");
//		// Les sauts de pages sont de 4 pages. On v�rifie que l'on est bien sur la 5eme page.
//		assertTrue(selenium.isElementPresent("//a/b[contains(text(),\"6\")]|//strong[contains(text(),\"6\")]"));
//		// On teste le lien fast prev.
//		selenium.click("//a[img[@alt=\"Fast prev\"]]");
//		selenium.waitForPageToLoad("30000");
//		// Les sauts de pages sont de 4 pages. On v�rife que l'on est bien sur la 2eme page.
//		assertTrue(selenium.isElementPresent("//a/b[contains(text(),\"2\")]|//strong[contains(text(),\"2\")]"));
		// On teste le lien Prev.
		selenium.click("xpath=(//span/a)[2]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie que l'on est sur la premi�re page.
		assertTrue(selenium.isElementPresent("//a/b[contains(text(),\"1\")]|//strong[contains(text(),\"1\")]"));
//		// On teste le lien Last.
		selenium.click("xpath=(//span/a)[9]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//a/b[contains(text(),\"10\")]|//strong[contains(text(),\"10\")]"));
//		// On teste le lien First
		selenium.click("xpath=(//span/a)[1]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//a/b[contains(text(),\"1\")]|//strong[contains(text(),\"1\")]"));
	}


}
