/*
 * TimeoutFilter
 * 
 * Filter implementation coping with Jsf session timeout
 */

package com.bluage.documentation.web;
import java.io.IOException;
import java.util.logging.Logger;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
 
 
public class TimeoutFilter implements Filter { 
  private static final Logger log = Logger.getLogger("webapp.timeoutFilter"); 
 
  private static final String TIMOUT_PAGE = "faces/presentation/home/home.jsp"; 
 
  public void init(FilterConfig filterConfig) throws ServletException { 
  } 
 
  public void doFilter(ServletRequest request, ServletResponse response, 
      FilterChain filterChain) throws IOException, ServletException { 
    
	// Checking the type of request
	if ((request instanceof HttpServletRequest) 
        && (response instanceof HttpServletResponse)) { 
      HttpServletRequest hRequest = (HttpServletRequest) request; 
      HttpServletResponse hResponse = (HttpServletResponse) response; 
      
      // Checking whether the resource is protected or not 
      if (checkResource(hRequest)) { 
        String requestPath = hRequest.getRequestURI(); 
       
        // Checking session status
        if (checkSession(hRequest)) { 
            String timeoutUrl = hRequest.getContextPath() + "/" 
                + TIMOUT_PAGE; 
            // Redirection
            hResponse.sendRedirect(timeoutUrl); 
            return; 
        } 
      } 
      filterChain.doFilter(request, response); 
    } 
  }
 
  private boolean checkResource(HttpServletRequest request) { 
    String requestPath = request.getRequestURI(); 
    return !(requestPath.contains(TIMOUT_PAGE) || 
      requestPath.equals(request.getContextPath() + "/")); 
  } 
 
  private boolean checkSession(HttpServletRequest request) { 
    return request.getRequestedSessionId() != null 
        && !request.isRequestedSessionIdValid(); 
  } 
 
  public void destroy() { 
  } 
} 