<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file="/include.jsp"  %>
<html >
<head >
<title  > 
<fmt:message key="cUC206_CreatePlayer_BLU_AGE_DOCUMENTATION"/>
</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"  > 
</meta>
<link href="<c:url value= "/presentation/_style/_style.css"/>" rel="stylesheet" type="text/css"  /> 
<link href="<c:url value= "/presentation/_include/jquery/rd_lib.css"/>" rel="stylesheet" type="text/css"  /> 
<script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/jquery.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/rd_setup.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/rd_lib.js"/>" space="preserve"  ></script><link rel="shortcut icon" href="<c:url value= "/presentation/_style/favicon.ico"/>"  /> 

</head>

<body class=" " > 
<div id="rd_ghost"  > 
<small  > 
<fmt:message key="cUC206_CreatePlayer___CSS_DEACTIVATED"/>
</small>
</div>
<div id="rd_jghost"  > 
<small  > 
<fmt:message key="cUC206_CreatePlayer___JAVASCRIPT_DEACTIVATED"/>
</small>
</div>
<div id="shape"  > 
<div class="ui-layout-center"  > 
<div class="east-north"  > 
	<form:form id="uC206_CreatePlayer" commandName="uC206_CreatePlayerForm"  enctype="application/x-www-form-urlencoded" method="post">
<h2 class="rd_first"  > 
<fmt:message key="cUC206_CreatePlayer_ADVANCED_FEATURES__VALUE_OBJE"/>
</h2>
<table class="rd_tableform"  >
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC206_CreatePlayer_FIRST_NAME_REQUIREDFIELDVALID"/>
</label>
<label style="background: none;color: red;"  > 
<fmt:message key="cUC206_CreatePlayer_"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
<form:input id="txt_firstname" path="screen206.player206VO.firstName"  size="30" 
		/>
</td> 
<td rowspan="1" colspan="1"  >
<form:errors path="screen206.player206VO.firstName"/>
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC206_CreatePlayer_LAST_NAME_REQUIREDFIELDVALIDA"/>
</label>
<label style="background: none;color: red;"  > 
<fmt:message key="cUC206_CreatePlayer__5"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
<form:input id="txt_lastname" path="screen206.player206VO.lastName"  size="30" 
		/>
</td> 
<td rowspan="1" colspan="1"  >
<form:errors path="screen206.player206VO.lastName"/>
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC206_CreatePlayer_DATE_OF_BIRTH_MMDDYYYY"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
<form:input id="txt_dateofbirth" path="screen206.player206VO.dateOfBirth"  size="12" 
		/>
</td> 
<td rowspan="1" colspan="1"  >
<form:errors path="screen206.player206VO.dateOfBirth"/>
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC206_CreatePlayer_ESTIMATED_VALUE"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
<form:input id="txt_estimatedvalue" path="screen206.player206VO.estimatedValue"  size="12" 
		/>
<fmt:message key="cUC206_CreatePlayer_M"/>
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC206_CreatePlayer_WEIGHTRANGEVALIDATOR"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
<form:input id="txt_weight" path="screen206.player206VO.weight"  size="12" 
		/>
<fmt:message key="cUC206_CreatePlayer_KG"/>
</td> 
<td rowspan="1" colspan="1"  >
<form:errors path="screen206.player206VO.weight"/>
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC206_CreatePlayer_HEIGHTRANGEVALIDATOR"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
<form:input id="txt_height" path="screen206.player206VO.height"  size="3" 
		/>
<fmt:message key="cUC206_CreatePlayer_CM"/>
</td> 
<td rowspan="1" colspan="1"  >
<form:errors path="screen206.player206VO.height"/>
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC206_CreatePlayer_POSITION"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
<form:select id="select_position" path="screen206.player206VO.positionVO.code"   
		>
		<form:options items="${uC206_CreatePlayerForm.listPosition206}" itemValue="code" itemLabel="name" />
</form:select>
</td> 
<td rowspan="1" colspan="1"  >
<fmt:message key="cUC206_CreatePlayer__6"/>
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC206_CreatePlayer_ROOKIE"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
<form:checkbox id="chk_rookie" path="screen206.player206VO.rookie"   
		/>
</td> 
<td rowspan="1" colspan="1"  >
<fmt:message key="cUC206_CreatePlayer__7"/>
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<fmt:message key="cUC206_CreatePlayer__8"/>
</td> 
<td rowspan="1" colspan="1"  >
<a id="lnk_createPlayer"onclick="javascript:btn_click_cExecuteUC206_CreatePlayer(this.href);return false;"				href="<c:url value="/presentation/advancedfeatures/valueobject/uc206_valueobject/UC206_CreatePlayer/lnk_createPlayer.html"/>"
							 shape="rect"  >									 
<fmt:message key="cUC206_CreatePlayer_CREATE"/>
</a>
</td> 
<td rowspan="1" colspan="1"  >
<fmt:message key="cUC206_CreatePlayer__9"/>
</td> 
</tr>
</table> 
<label style="background: none;color: red;"  > 
<fmt:message key="cUC206_CreatePlayer__10"/>
</label>
<fmt:message key="cUC206_CreatePlayer_REQUIRED_FIELDS_"/>
<br clear="none"  /> 
<br clear="none"  /> 
<a id="lnk_back"				href="<c:url value="/presentation/advancedfeatures/valueobject/uc206_valueobject/UC206_HomePage.html"/>"
													 shape="rect"  >									 
<fmt:message key="cUC206_CreatePlayer_BACK"/>
</a>
	</form:form>
	<script language="javascript">
		function btn_click_cExecuteUC206_CreatePlayer(url) {
			UC206_CreatePlayervar = document.getElementById('uC206_CreatePlayer');
			UC206_CreatePlayervar.setAttribute('action', url);
			UC206_CreatePlayervar.submit();
		}
		function btn_click_cSetActionUC206_CreatePlayer(url) {
			UC206_CreatePlayervar = document.getElementById('uC206_CreatePlayer');
			UC206_CreatePlayervar.setAttribute('action', url);
		}
	</script>
</div>
<div class="east-center"  > 
<tiles:insertDefinition	name="presentation/advancedfeatures/valueobject/uc206_valueobject/UC206_CreatePlayercmp_tabs" />
</div>
</div>
<div class="ui-layout-north"  > 
<tiles:insertDefinition	name="presentation/advancedfeatures/valueobject/uc206_valueobject/UC206_CreatePlayercmp_header" />
</div>
<div class="ui-layout-south"  > 
<fmt:message key="cUC206_CreatePlayer_______"/>
<span id="rd_jversion"  > 
<fmt:message key="cUC206_CreatePlayer_BLU_AGE_DOCUMENTATION_11"/>
</span>
</div>
<div class="ui-layout-west"  > 
<tiles:insertDefinition	name="presentation/advancedfeatures/valueobject/uc206_valueobject/UC206_CreatePlayercmp_treeview" />
</div>
</div>
</body>
</html>
