/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.services.ajax.uc50_ajaxcall ;

// Import declaration.
// Java imports.
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.bluage.documentation.business.standardfeatures.services.ajax.uc50_ajaxcall.bos.Player50BO;
import com.bluage.documentation.business.standardfeatures.services.ajax.uc50_ajaxcall.bos.Team50BO;
import com.bluage.documentation.business.standardfeatures.services.ajax.uc50_ajaxcall.entities.daofinder.Team50DAOFinderImpl;
import com.bluage.documentation.service.standardfeatures.services.ajax.uc50_ajaxcall.ServiceAjaxCall;
import com.netfective.bluage.core.exception.ApplicationException;
import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;
import com.netfective.bluage.core.web.IForm;

/**
 * Class : UC50_AjaxCallController
 */
@Controller
@Scope("prototype")
@SessionAttributes("uC50_AjaxCallForm")
@RequestMapping(value= "/presentation/standardfeatures/services/ajax/uc50_ajaxcall")
public class UC50_AjaxCallController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC50_AjaxCallController.class);
	
	//Current form name
	private static final String U_C50__AJAX_CALL_FORM = "uC50_AjaxCallForm";

						//CONSTANT: teamslist
	private static final String TEAMSLIST = "teamslist";
				//CONSTANT: players
	private static final String PLAYERS = "players";
	//CONSTANT: selectedTeam
	private static final String SELECTED_TEAM = "selectedTeam";
	//CONSTANT: isRookie
	private static final String IS_ROOKIE = "isRookie";
	//CONSTANT: minimumValue
	private static final String MINIMUM_VALUE = "minimumValue";
	
	/**
	 * Property:customDateEditorsUC50_AjaxCallController
	 */
	final private Map<String,CustomDateEditor> customDateEditorsUC50_AjaxCallController = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:uC50_AjaxCallValidator
	 */
	final private UC50_AjaxCallValidator uC50_AjaxCallValidator = new UC50_AjaxCallValidator();
	
	/**
	 * Service declaration : serviceAjaxCall.
	 */
	@Autowired
	private ServiceAjaxCall serviceAjaxCall;
	
	/**
	 * Generic Finder : team50DAOFinderImpl.
	 */
	@Autowired
	private Team50DAOFinderImpl team50DAOFinderImpl;
	
	
	// Initialise all the instances for UC50_AjaxCallController
								// Initialize the instance teamslist for the state lnk_searchTeamsPostProcess
		private List teamslist; // Initialize the instance teamslist for UC50_AjaxCallController
						// Declare the instance players
		private List players;
		// Declare the instance selectedTeam
		private Team50BO selectedTeam;
		// Declare the instance isRookie
		private Player50BO isRookie;
		// Declare the instance minimumValue
		private Player50BO minimumValue;
			/**
	 * Operation : lnk_value
 	 * @param model : 
 	 * @param uC50_AjaxCall : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC50_AjaxCall/lnk_value.html",method = RequestMethod.POST)
	public String lnk_value(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC50_AjaxCallForm") UC50_AjaxCallForm  uC50_AjaxCallForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : lnk_value"); 
		infoLogger(uC50_AjaxCallForm); 
		uC50_AjaxCallForm.setAlwaysCallPreControllers(false); 
		// initialization for lnk_value
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state lnk_value if the validation fail, you will be forward to the screen : "/presentation/standardfeatures/services/ajax/uc50_ajaxcall/UC50_AjaxCall"
		if(errorsMessages(request,response,uC50_AjaxCallForm,bindingResult,"", uC50_AjaxCallValidator, customDateEditorsUC50_AjaxCallController)){ 
			return "/presentation/standardfeatures/services/ajax/uc50_ajaxcall/UC50_AjaxCall"; 
		}

													 callDisplayPlayersList(request,uC50_AjaxCallForm );
						// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.services.ajax.uc50_ajaxcall.UC50_AjaxCallForm or not from lnk_value
			// Populate the destination form with all the instances returned from lnk_value.
			final IForm uC50_AjaxCallForm2 = populateDestinationForm(request.getSession(), uC50_AjaxCallForm); 
					
			// Add the form to the model.
			model.addAttribute("uC50_AjaxCallForm", uC50_AjaxCallForm2); 
			
			request.getSession().setAttribute("uC50_AjaxCallForm", uC50_AjaxCallForm2);
			
			// "OK" CASE => destination screen path from lnk_value
			LOGGER.info("Go to the screen 'UC50_AjaxCall'.");
			// Ajax call do not redirect.
			destinationPath =  "/presentation/standardfeatures/services/ajax/uc50_ajaxcall/UC50_AjaxCall";
						
	
		destinationPath = processAjaxComponent(request.getSession(), uC50_AjaxCallForm, "UC50_AjaxCall", destinationPath, model); // Call the generic process for ajax for the state : lnk_value 
	
		return destinationPath; // Returns the destination path for the state : lnk_value 
	}
	
	/**
	 * Operation : lnk_value
 	 * @param model : 
 	 * @param uC50_AjaxCall : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC50_AjaxCall/lnk_value.html",method = RequestMethod.GET)
	public String lnk_value(final Model model,final @ModelAttribute("uC50_AjaxCallForm") UC50_AjaxCallForm  uC50_AjaxCallForm){
		LOGGER.info("Begin method : lnk_value"); // for state lnk_value 
		infoLogger(uC50_AjaxCallForm); // Form Diagnostic for UC50_AjaxCall state lnk_value 
		return "redirect:/presentation/standardfeatures/services/ajax/uc50_ajaxcall/UC50_AjaxCall.html"; // Ajax for lnk_value 	
	}
	
	/**
	* This method initialise the form : UC50_AjaxCallForm 
	* @return UC50_AjaxCallForm
	*/
	@ModelAttribute("UC50_AjaxCallFormInit")
	public void initUC50_AjaxCallForm(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, U_C50__AJAX_CALL_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method UC50_AjaxCallForm."); //for lnk_value 
		}
		UC50_AjaxCallForm uC50_AjaxCallForm;
	
		if(request.getSession().getAttribute(U_C50__AJAX_CALL_FORM) != null){
			uC50_AjaxCallForm = (UC50_AjaxCallForm)request.getSession().getAttribute(U_C50__AJAX_CALL_FORM);
		} else {
			uC50_AjaxCallForm = new UC50_AjaxCallForm();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form UC50_AjaxCallForm.");
		}
		uC50_AjaxCallForm = (UC50_AjaxCallForm)populateDestinationForm(request.getSession(), uC50_AjaxCallForm);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  UC50_AjaxCallForm.");
		}
		model.addAttribute(U_C50__AJAX_CALL_FORM, uC50_AjaxCallForm);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param UC50_AjaxCallForm : The sceen form.
	 */
	@RequestMapping(value = "/UC50_AjaxCall.html" ,method = RequestMethod.GET)
	public void prepareUC50_AjaxCall(final HttpServletRequest request,final  Model model,final  @ModelAttribute("UC50_AjaxCallFormInit") UC50_AjaxCallForm uC50_AjaxCallForm){
		
		UC50_AjaxCallForm currentUC50_AjaxCallForm = uC50_AjaxCallForm;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(U_C50__AJAX_CALL_FORM) == null){
			if(currentUC50_AjaxCallForm!=null){
				request.getSession().setAttribute(U_C50__AJAX_CALL_FORM, currentUC50_AjaxCallForm);
			}else {
				currentUC50_AjaxCallForm = new UC50_AjaxCallForm();
				request.getSession().setAttribute(U_C50__AJAX_CALL_FORM, currentUC50_AjaxCallForm);	
			}
		} else {
			currentUC50_AjaxCallForm = (UC50_AjaxCallForm) request.getSession().getAttribute(U_C50__AJAX_CALL_FORM);
		}

		try {
			List teamslist = team50DAOFinderImpl.findAll();
			put(request.getSession(), TEAMSLIST, teamslist);
		} catch (ApplicationException e) {
			errorLogger("An error while calling the generic finder for the instance : teamslist.",e);
		}
					currentUC50_AjaxCallForm = (UC50_AjaxCallForm)populateDestinationForm(request.getSession(), currentUC50_AjaxCallForm);
		// Call all the Precontroller.
	currentUC50_AjaxCallForm.setAlwaysCallPreControllers(true);
		model.addAttribute(U_C50__AJAX_CALL_FORM, currentUC50_AjaxCallForm);
		request.getSession().setAttribute(U_C50__AJAX_CALL_FORM, currentUC50_AjaxCallForm);
		
	}
	
													/**
	 * method callDisplayPlayersList
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callDisplayPlayersList(HttpServletRequest request,IForm form) {
		players = (List)get(request.getSession(),form, "players");  
										 
					selectedTeam = (Team50BO)get(request.getSession(),form, "selectedTeam");  
										 
					isRookie = (Player50BO)get(request.getSession(),form, "isRookie");  
										 
					minimumValue = (Player50BO)get(request.getSession(),form, "minimumValue");  
										 
					try {
				// executing DisplayPlayersList in lnk_value
				players = 	serviceAjaxCall.executeFindPlayers50(
			selectedTeam
		,			isRookie
		,			minimumValue
			);  
 
				put(request.getSession(), PLAYERS,players);
								// processing variables DisplayPlayersList in lnk_value
				put(request.getSession(), SELECTED_TEAM,selectedTeam); 
							put(request.getSession(), IS_ROOKIE,isRookie); 
							put(request.getSession(), MINIMUM_VALUE,minimumValue); 
			
			} catch (ApplicationException e) { 
				// error handling for operation executeFindPlayers50 called DisplayPlayersList
				errorLogger("An error occured during the execution of the operation : executeFindPlayers50",e); 
		
			}
	}
		
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("UC50_AjaxCallController [ ");
								strBToS.append(TEAMSLIST);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(teamslist);
			strBToS.append(END_TO_STRING_DELIMITER);
						strBToS.append(PLAYERS);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(players);
			strBToS.append(END_TO_STRING_DELIMITER);
			strBToS.append(SELECTED_TEAM);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(selectedTeam);
			strBToS.append(END_TO_STRING_DELIMITER);
			strBToS.append(IS_ROOKIE);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(isRookie);
			strBToS.append(END_TO_STRING_DELIMITER);
			strBToS.append(MINIMUM_VALUE);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(minimumValue);
			strBToS.append(END_TO_STRING_DELIMITER);
		// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(uC50_AjaxCallValidator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsUC50_AjaxCallController!=null); 
		return strBToS.toString();
	}
}
