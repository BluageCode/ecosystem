/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.application.component.uc46_menu ;

// Import declaration.
// Java imports.
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;

/**
 * Class : UC46_Page2Controller
 */
@Controller
@Scope("prototype")
@SessionAttributes("uC46_Page2Form")
@RequestMapping(value= "/presentation/standardfeatures/application/component/uc46_menu")
public class UC46_Page2Controller extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC46_Page2Controller.class);
	
	//Current form name
	private static final String U_C46__PAGE2_FORM = "uC46_Page2Form";

	
	/**
	 * Property:customDateEditorsUC46_Page2Controller
	 */
	final private Map<String,CustomDateEditor> customDateEditorsUC46_Page2Controller = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:uC46_Page2Validator
	 */
	final private UC46_Page2Validator uC46_Page2Validator = new UC46_Page2Validator();
	
	
	// Initialise all the instances for UC46_Page2Controller

	/**
	* This method initialise the form : UC46_Page2Form 
	* @return UC46_Page2Form
	*/
	@ModelAttribute("UC46_Page2FormInit")
	public void initUC46_Page2Form(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, U_C46__PAGE2_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method UC46_Page2Form."); //for lnk_goToPage2 
		}
		UC46_Page2Form uC46_Page2Form;
	
		if(request.getSession().getAttribute(U_C46__PAGE2_FORM) != null){
			uC46_Page2Form = (UC46_Page2Form)request.getSession().getAttribute(U_C46__PAGE2_FORM);
		} else {
			uC46_Page2Form = new UC46_Page2Form();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form UC46_Page2Form.");
		}
		uC46_Page2Form = (UC46_Page2Form)populateDestinationForm(request.getSession(), uC46_Page2Form);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  UC46_Page2Form.");
		}
		model.addAttribute(U_C46__PAGE2_FORM, uC46_Page2Form);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param UC46_Page2Form : The sceen form.
	 */
	@RequestMapping(value = "/UC46_Page2.html" ,method = RequestMethod.GET)
	public void prepareUC46_Page2(final HttpServletRequest request,final  Model model,final  @ModelAttribute("UC46_Page2FormInit") UC46_Page2Form uC46_Page2Form){
		
		UC46_Page2Form currentUC46_Page2Form = uC46_Page2Form;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(U_C46__PAGE2_FORM) == null){
			if(currentUC46_Page2Form!=null){
				request.getSession().setAttribute(U_C46__PAGE2_FORM, currentUC46_Page2Form);
			}else {
				currentUC46_Page2Form = new UC46_Page2Form();
				request.getSession().setAttribute(U_C46__PAGE2_FORM, currentUC46_Page2Form);	
			}
		} else {
			currentUC46_Page2Form = (UC46_Page2Form) request.getSession().getAttribute(U_C46__PAGE2_FORM);
		}

		currentUC46_Page2Form = (UC46_Page2Form)populateDestinationForm(request.getSession(), currentUC46_Page2Form);
		// Call all the Precontroller.
	currentUC46_Page2Form.setAlwaysCallPreControllers(true);
		model.addAttribute(U_C46__PAGE2_FORM, currentUC46_Page2Form);
		request.getSession().setAttribute(U_C46__PAGE2_FORM, currentUC46_Page2Form);
		
	}
	
	
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("UC46_Page2Controller [ ");
		// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(uC46_Page2Validator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsUC46_Page2Controller!=null); 
		return strBToS.toString();
	}
}
