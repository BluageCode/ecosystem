--<ScriptOptions statementTerminator=";"/>

ALTER TABLE many_to_many DROP CONSTRAINT championship63_player63s_fkc;

ALTER TABLE many_to_many DROP CONSTRAINT player63_championship63s_fkc;

ALTER TABLE a__championship63__player63 DROP CONSTRAINT player63_championship63s_fkc;

ALTER TABLE player63 DROP CONSTRAINT player63_team63_fkc;

ALTER TABLE state63 DROP CONSTRAINT state63_stadium63_fkc;

ALTER TABLE a__championship63__player63 DROP CONSTRAINT championship63_player63s_fkc;

ALTER TABLE a__player63__championship63 DROP CONSTRAINT player63_championship63s_fkc;

ALTER TABLE team63 DROP CONSTRAINT team63_coach_fkc;

ALTER TABLE a__player63__championship63 DROP CONSTRAINT championship63_player63s_fkc;

ALTER TABLE team63 DROP CONSTRAINT team63_state_fkc;

ALTER TABLE player63 DROP CONSTRAINT player63_pkey;

ALTER TABLE coach63 DROP CONSTRAINT coach63_pkey;

ALTER TABLE championship63 DROP CONSTRAINT championship63_pkey;

ALTER TABLE state63 DROP CONSTRAINT state63_pkey;

ALTER TABLE stadium63 DROP CONSTRAINT stadium63_pkey;

ALTER TABLE team63 DROP CONSTRAINT team63_pkey;

ALTER TABLE a__championship63__player63 DROP CONSTRAINT a__championship63__player63_pkey;

DROP TABLE many_to_many;

DROP TABLE stadium63;

DROP TABLE a__player63__championship63;

DROP TABLE state63;

DROP TABLE coach63;

DROP TABLE a__championship63__player63;

DROP TABLE championship63;

DROP TABLE team63;

DROP TABLE player63;

CREATE TABLE many_to_many (
	player63s_fk INT8 NOT NULL,
	championship63s_fk VARCHAR(1024) NOT NULL
);

CREATE TABLE stadium63 (
		code VARCHAR(1024) NOT NULL,
		version INT4 NOT NULL,
		name VARCHAR(1024)
	);

CREATE TABLE a__player63__championship63 (
		player63s_fk INT8 NOT NULL,
		championship63s_fk VARCHAR(1024) NOT NULL
	);

CREATE TABLE state63 (
		code VARCHAR(1024) NOT NULL,
		version INT4 NOT NULL,
		name VARCHAR(1024) NOT NULL,
		stadium63_fk VARCHAR(1024)
	);

CREATE TABLE coach63 (
		id INT8 NOT NULL,
		version INT4 NOT NULL,
		first_name VARCHAR(1024),
		last_name VARCHAR(1024)
	);

CREATE TABLE a__championship63__player63 (
		championship63s_fk VARCHAR(1024) NOT NULL,
		player63s_fk INT8 NOT NULL
	);

CREATE TABLE championship63 (
		code VARCHAR(1024) NOT NULL,
		version INT4 NOT NULL,
		name VARCHAR(1024)
	);

CREATE TABLE team63 (
		id INT8 NOT NULL,
		version INT4 NOT NULL,
		city VARCHAR(1024),
		name VARCHAR(1024),
		state_fk VARCHAR(1024) NOT NULL,
		coach_fk INT8 NOT NULL
	);

CREATE TABLE player63 (
		id INT8 NOT NULL,
		version INT4 NOT NULL,
		first_name VARCHAR(1024),
		last_name VARCHAR(1024),
		date_of_birth DATE,
		estimated_value FLOAT4,
		weight INT4,
		height INT4,
		rookie BOOL,
		team63_fk INT8
	);
INSERT INTO championship63 (code, version, name) VALUES ('1', 0, 'NCAA College');
INSERT INTO championship63 (code, version, name) VALUES ('2', 0, ' AAU Boys Basketball ');
INSERT INTO championship63 (code, version, name) VALUES ('3', 0, ' AAU Boys Basketball ');
INSERT INTO championship63 (code, version, name) VALUES ('4', 0, 'D2Basketball');

INSERT INTO coach63 (id, version, first_name, last_name) VALUES (1, 0, 'Harry', 'Davis');
INSERT INTO coach63 (id, version, first_name, last_name) VALUES (2, 0, 'Loren', 'Colon');
INSERT INTO coach63 (id, version, first_name, last_name) VALUES (3, 0, 'Bob', 'Clark');
INSERT INTO coach63 (id, version, first_name, last_name) VALUES (4, 0, 'Gerald', 'Fox');
INSERT INTO coach63 (id, version, first_name, last_name) VALUES (5, 0, 'Louis', 'Stokes');
INSERT INTO coach63 (id, version, first_name, last_name) VALUES (6, 0, 'Cameron', 'Horton');
INSERT INTO coach63 (id, version, first_name, last_name) VALUES (7, 0, 'Shane', 'Sandoval');

INSERT INTO stadium63 (code, version, name) VALUES ('4', 0, 'Heinz Field');
INSERT INTO stadium63 (code, version, name) VALUES ('5', 0, 'Jobing.com Arena');
INSERT INTO stadium63 (code, version, name) VALUES ('6', 0, 'Monster Park');
INSERT INTO stadium63 (code, version, name) VALUES ('7', 0, 'U.S. Cellular Field ');
INSERT INTO stadium63 (code, version, name) VALUES ('8', 0, 'Dick s Sporting Goods Park');
INSERT INTO stadium63 (code, version, name) VALUES ('1', 0, 'Hut Park ');
INSERT INTO stadium63 (code, version, name) VALUES ('2', 0, 'Canad Inns Stadium');
INSERT INTO stadium63 (code, version, name) VALUES ('3', 0, 'Gay Meadow');
INSERT INTO stadium63 (code, version, name) VALUES ('9', 0, 'American Airlines Arena');
INSERT INTO stadium63 (code, version, name) VALUES ('10', 0, 'New Louisville Basketball Arena');
INSERT INTO stadium63 (code, version, name) VALUES ('11', 0, 'Sutherland');
INSERT INTO stadium63 (code, version, name) VALUES ('12', 0, 'Spalding Arena');

INSERT INTO state63 (code, version, name, stadium63_fk) VALUES ('AK', 0, 'Alaska', '1');
INSERT INTO state63 (code, version, name, stadium63_fk) VALUES ('AL', 0, 'Alabama', '2');
INSERT INTO state63 (code, version, name, stadium63_fk) VALUES ('AR', 0, 'Arkansas', '3');
INSERT INTO state63 (code, version, name, stadium63_fk) VALUES ('AZ', 0, 'Arizona', '4');
INSERT INTO state63 (code, version, name, stadium63_fk) VALUES ('CA', 0, 'California', '5');
INSERT INTO state63 (code, version, name, stadium63_fk) VALUES ('CO', 0, 'Colorado', '6');
INSERT INTO state63 (code, version, name, stadium63_fk) VALUES ('CT', 0, 'Connecticut', '7');
INSERT INTO state63 (code, version, name, stadium63_fk) VALUES ('DE', 0, 'Delaware', '8');
INSERT INTO state63 (code, version, name, stadium63_fk) VALUES ('FL', 0, 'Florida', '9');
INSERT INTO state63 (code, version, name, stadium63_fk) VALUES ('GA', 0, 'Georgia', '10');
INSERT INTO state63 (code, version, name, stadium63_fk) VALUES ('HI', 0, 'Hawaii', '11');
INSERT INTO state63 (code, version, name, stadium63_fk) VALUES ('IA', 0, 'Iowa', '12');

INSERT INTO team63 (id, version, city, name, state_fk, coach_fk) VALUES (1, 0, 'Charlotte', 'Tigers', 'AK', 1);
INSERT INTO team63 (id, version, city, name, state_fk, coach_fk) VALUES (2, 0, 'Kansas City', 'Bears', 'AL', 2);
INSERT INTO team63 (id, version, city, name, state_fk, coach_fk) VALUES (3, 0, 'Detroit', 'Chargers', 'AR', 3);
INSERT INTO team63 (id, version, city, name, state_fk, coach_fk) VALUES (4, 0, 'Detroit', 'Hawks', 'AZ', 4);
INSERT INTO team63 (id, version, city, name, state_fk, coach_fk) VALUES (5, 0, 'Omaha', 'Braves', 'CA', 5);
INSERT INTO team63 (id, version, city, name, state_fk, coach_fk) VALUES (6, 0, 'Raleigh', 'Eagles', 'CO', 6);
INSERT INTO team63 (id, version, city, name, state_fk, coach_fk) VALUES (7, 0, 'Oakland', 'Cardinals', 'CT', 7);

INSERT INTO player63 VALUES (201, 0, 'Oliver', 'Frank', '1968-10-31', 0.11, 116, 228, false, 1);
INSERT INTO player63 VALUES (202, 0, 'Elbert', 'Cole', '1965-04-07', 3.95, 100, 171, false, 2);
INSERT INTO player63 VALUES (204, 0, 'Ricky', 'Dawson', '1990-03-03', 29.299999, 115, 168, true, 4);
INSERT INTO player63 VALUES (203, 0, 'Abel', 'Dennis', '1980-04-16', 30.51, 105, 208, false, 3);
INSERT INTO player63 VALUES (205, 0, 'Earl', 'Russell', '1981-12-15', 44.73, 60, 234, false, 5);
INSERT INTO player63 VALUES (206, 0, 'Nick', 'Garner', '1992-07-27', 47.84, 87, 221, true, 6);
INSERT INTO player63 VALUES (207, 0, 'Howard', 'Ramos', '1970-08-25', 10.76, 107, 199, false, 7);
INSERT INTO player63 VALUES (208, 0, 'Allan', 'Meyer', '1969-02-14', 33.669998, 114, 229, false, 7);
INSERT INTO player63 VALUES (209, 0, 'Ruben', 'Pope', '1985-05-01', 44.66, 86, 201, false, 6);
INSERT INTO player63 VALUES (210, 0, 'Lester', 'Cross', '1976-02-21', 30.98, 69, 214, false, 5);
INSERT INTO player63 VALUES (211, 0, 'Keith', 'Barnett', '1976-11-10', 5.8099999, 66, 190, false, 4);
INSERT INTO player63 VALUES (213, 0, 'Tommy', 'Wood', '1992-01-20', 13.93, 77, 173, true, 2);
INSERT INTO player63 VALUES (214, 0, 'Sylvester', 'Nash', '1965-07-10', 37.73, 68, 184, false, 2);
INSERT INTO player63 VALUES (215, 0, 'Michael', 'Hodges', '1988-04-01', 25.35, 158, 204, false, 3);
INSERT INTO player63 VALUES (216, 0, 'Jerome', 'Grant', '1982-06-28', 28.940001, 105, 205, false, 1);
INSERT INTO player63 VALUES (217, 0, 'Woodrow', 'Carpenter', '1987-08-08', 41.759998, 69, 224, false, 4);
INSERT INTO player63 VALUES (218, 0, 'Roderick', 'Robbins', '1975-07-29', 12.04, 115, 164, false, 3);
INSERT INTO player63 VALUES (219, 0, 'Christian', 'Chavez', '1983-05-17', 37.880001, 97, 209, false, 4);
INSERT INTO player63 VALUES (212, 0, 'Ivan', 'Tate', '1972-02-08', 48.490002, 143, 175, false, 3);
INSERT INTO player63 VALUES (401, 0, 'Gregg', 'Nash', '1972-01-11', 6.8600001, 147, 215, false, 1);
INSERT INTO player63 VALUES (402, 0, 'Floyd', 'Howell', '1979-08-25', 25.709999, 155, 192, false, 1);
INSERT INTO player63 VALUES (403, 0, 'Simon', 'Klein', '1987-01-21', 22.24, 63, 203, false, 1);
INSERT INTO player63 VALUES (404, 0, 'Blake', 'Montgomery', '1992-11-16', 37.470001, 113, 205, true, 1);
INSERT INTO player63 VALUES (405, 0, 'Doyle', 'Burns', '1990-02-23', 39.130001, 153, 225, true, 1);
INSERT INTO player63 VALUES (406, 0, 'Troy', 'Rowe', '1973-02-12', 26.82, 94, 211, false, 1);
INSERT INTO player63 VALUES (407, 0, 'Jesus', 'Jimenez', '1985-09-01', 17.73, 133, 159, false, 1);
INSERT INTO player63 VALUES (408, 0, 'Ross', 'Guerrero', '1992-01-26', 13.68, 136, 171, true, 1);
INSERT INTO player63 VALUES (409, 0, 'Eddie', 'Boyd', '1967-10-05', 21.719999, 118, 232, false, 1);
INSERT INTO player63 VALUES (410, 0, 'Luther', 'Peterson', '1985-06-09', 1.04, 83, 181, false, 1);
INSERT INTO player63 VALUES (411, 0, 'Daniel', 'Vargas', '1979-04-21', 30.43, 117, 155, false, 2);
INSERT INTO player63 VALUES (412, 0, 'Freddie', 'Kelly', '1967-03-22', 34.400002, 81, 196, false, 2);
INSERT INTO player63 VALUES (413, 0, 'Sam', 'Farmer', '1987-11-29', 37.580002, 98, 201, false, 2);
INSERT INTO player63 VALUES (414, 0, 'Randal', 'Hampton', '1967-12-01', 31.219999, 121, 202, false, 2);
INSERT INTO player63 VALUES (415, 0, 'Felipe', 'Olson', '1976-09-28', 8.0600004, 129, 189, false, 2);
INSERT INTO player63 VALUES (416, 0, 'Gary', 'Christensen', '1974-03-13', 21.52, 145, 170, false, 2);
INSERT INTO player63 VALUES (417, 0, 'Aaron', 'Mullins', '1972-01-15', 8.54, 86, 239, false, 2);
INSERT INTO player63 VALUES (418, 0, 'Miguel', 'Moore', '1967-03-26', 9.4799995, 67, 173, false, 2);
INSERT INTO player63 VALUES (419, 0, 'Bruce', 'Adkins', '1986-05-24', 43.720001, 150, 209, false, 2);
INSERT INTO player63 VALUES (420, 0, 'Felipe', 'Lawrence', '1981-04-19', 33.66, 150, 219, false, 3);
INSERT INTO player63 VALUES (421, 0, 'Nelson', 'Davis', '1978-05-16', 20.84, 119, 171, false, 3);
INSERT INTO player63 VALUES (422, 0, 'Jeremiah', 'Johnston', '1983-12-15', 14.57, 149, 200, false, 3);
INSERT INTO player63 VALUES (423, 0, 'Enrique', 'Wilkins', '1967-01-21', 47.740002, 75, 240, false, 3);
INSERT INTO player63 VALUES (424, 0, 'Gerald', 'Jimenez', '1970-01-22', 29.26, 63, 166, false, 3);
INSERT INTO player63 VALUES (425, 0, 'Michael', 'Simon', '1975-12-23', 43.950001, 118, 222, false, 3);
	
	
ALTER TABLE many_to_many ADD CONSTRAINT many_to_many_pkey PRIMARY KEY (championship63s_fk, player63s_fk);
	
ALTER TABLE player63 ADD CONSTRAINT player63_pkey PRIMARY KEY (id);

ALTER TABLE coach63 ADD CONSTRAINT coach63_pkey PRIMARY KEY (id);

ALTER TABLE championship63 ADD CONSTRAINT championship63_pkey PRIMARY KEY (code);

ALTER TABLE state63 ADD CONSTRAINT state63_pkey PRIMARY KEY (code);

ALTER TABLE stadium63 ADD CONSTRAINT stadium63_pkey PRIMARY KEY (code);

ALTER TABLE team63 ADD CONSTRAINT team63_pkey PRIMARY KEY (id);

ALTER TABLE a__championship63__player63 ADD CONSTRAINT a__championship63__player63_pkey PRIMARY KEY (championship63s_fk, player63s_fk);

ALTER TABLE many_to_many ADD CONSTRAINT championship63_player63s_fkc FOREIGN KEY (player63s_fk) 
	REFERENCES player63(id);

ALTER TABLE many_to_many ADD CONSTRAINT player63_championship63s_fkc FOREIGN KEY (championship63s_fk)
	REFERENCES championship63(code);

ALTER TABLE a__championship63__player63 ADD CONSTRAINT player63_championship63s_fkc FOREIGN KEY (championship63s_fk)
	REFERENCES championship63 (code);

ALTER TABLE player63 ADD CONSTRAINT player63_team63_fkc FOREIGN KEY (team63_fk)
	REFERENCES team63 (id);

ALTER TABLE state63 ADD CONSTRAINT state63_stadium63_fkc FOREIGN KEY (stadium63_fk)
	REFERENCES stadium63 (code);

ALTER TABLE a__championship63__player63 ADD CONSTRAINT championship63_player63s_fkc FOREIGN KEY (player63s_fk)
	REFERENCES player63 (id);

ALTER TABLE a__player63__championship63 ADD CONSTRAINT player63_championship63s_fkc FOREIGN KEY (championship63s_fk)
	REFERENCES championship63 (code);

ALTER TABLE team63 ADD CONSTRAINT team63_coach_fkc FOREIGN KEY (coach_fk)
	REFERENCES coach63 (id);

ALTER TABLE a__player63__championship63 ADD CONSTRAINT championship63_player63s_fkc FOREIGN KEY (player63s_fk)
	REFERENCES player63 (id);

ALTER TABLE team63 ADD CONSTRAINT team63_state_fkc FOREIGN KEY (state_fk)
	REFERENCES state63 (code);

