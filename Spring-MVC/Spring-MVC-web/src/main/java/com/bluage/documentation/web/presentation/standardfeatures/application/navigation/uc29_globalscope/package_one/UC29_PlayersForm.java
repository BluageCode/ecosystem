/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.application.navigation.uc29_globalscope.package_one;


import java.io.Serializable;
import java.util.List;

import com.bluage.documentation.business.standardfeatures.application.navigation.uc29_globalscope.bos.Player29BO;
import com.netfective.bluage.core.web.AbstractForm;
import com.netfective.bluage.core.web.IForm;

/**
* Class : UC29_PlayersForm
*/
public class UC29_PlayersForm extends AbstractForm implements Serializable, IForm {
	
	/** Constant : serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	//CONSTANT : allPlayers
	private static final String ALL_PLAYERS = "allPlayers";
	/**
	 * 	Property: allPlayers 
	 */
	private List<Player29BO> allPlayers;
/**
	 * Default constructor : UC29_PlayersForm
	 */
	public UC29_PlayersForm() {
		super();
		// Initialize : allPlayers
		this.allPlayers = new java.util.ArrayList<com.bluage.documentation.business.standardfeatures.application.navigation.uc29_globalscope.bos.Player29BO>();
					this.setAlwaysCallPreControllers(true);
	}
	
	/**
	 * 	Getter : allPlayers 
	 * 	@return : Return the allPlayers instance.
	 */
	public List<Player29BO> getAllPlayers(){
		return allPlayers; // For UC29_PlayersForm
	}
	
	/**
	 * 	Setter : allPlayers 
	 *  @param allPlayersinstance : The instance to set.
	 */
	public void setAllPlayers(final List<Player29BO> allPlayersinstance){
		this.allPlayers = allPlayersinstance;// For UC29_PlayersForm
	}
/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UC29_PlayersForm [ "+
ALL_PLAYERS +" = " + allPlayers + "]";
	}
	
	/**
	 * Method setInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public void setInstanceByInstanceName(final String instanceName, final Object instance) {
		// Set the instance AllPlayers.
		if(ALL_PLAYERS.equals(instanceName)){// For UC29_PlayersForm
			this.setAllPlayers((List<Player29BO>)instance); // For UC29_PlayersForm
		}
			}
	
	/**
	 * Method getInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public Object getInstanceByInstanceName(final String instanceName){
		// Temporary object for tmpUC29_PlayersForm.
		Object tmpUC29_PlayersForm = null;
		
			
		// Get the instance AllPlayers for UC29_PlayersForm.
		if(ALL_PLAYERS.equals(instanceName)){ // For UC29_PlayersForm
			tmpUC29_PlayersForm = this.getAllPlayers(); // For UC29_PlayersForm
		}
		return tmpUC29_PlayersForm;// For UC29_PlayersForm
	}
	
			}
