/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.business.standardfeatures.application.datagrid.uc37_collectionorder.bos;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.inject.Inject;
import javax.inject.Named;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.LazyInitializationException;

import com.bluage.documentation.business.common.bos.TeamBO;
import com.bluage.documentation.business.standardfeatures.application.datagrid.uc37_collectionorder.entities.daofinder.Team37DAOFinderImpl;
import com.netfective.bluage.core.exception.ApplicationException;

/**
 * Team37BO
 *
* @see com.netfective.bluage.core.business.IBusinessObject
* * @see TeamBO
 */
@XmlRootElement

public class Team37BO
 extends TeamBO {
	
	/**
	 * Default SUID for Team37BO.
	 */
	private static final long serialVersionUID = 1L;
	

	@Inject
	@Named ("com.bluage.documentation.business.standardfeatures.application.datagrid.uc37_collectionorder.entities.daofinder.Team37Finder") 
	private Team37DAOFinderImpl finder;

																			/**
	 * Entity property: player37s
	 *
	 */
private Set<Player37BO> player37s = new HashSet<Player37BO>();



   /**
	* public default constructor for class Team37BO
	*/
	public Team37BO(){
		//NOP Team37BO
	}


																			/**
	 * Getter for player37s of class Team37BO.
	 *
	 * @return {@link java.util.Set<com.bluage.documentation.business.standardfeatures.application.datagrid.uc37_collectionorder.bos.Player37BO>}
	 * @generated
	 */ 
public  Set<Player37BO> getPlayer37s() {
	return this.player37s;
			}


	/**
	 * Setter for player37s of class Team37BO.
	 * @param value {@link java.util.Set<com.bluage.documentation.business.standardfeatures.application.datagrid.uc37_collectionorder.bos.Player37BO>}
	 * @generated
	 */

public  void setPlayer37s(final Set<Player37BO> player37s) {
	this.player37s = player37s;
}

	/**
	 * Remove an entity from the com.bluage.documentation.business.standardfeatures.application.datagrid.uc37_collectionorder.bos.Player37BO collection.
	 *
	 * @param entity
	 *		: {@link Set<Player37BO>} the entity
	 * @throws ApplicationException
     */
public void removeFromPlayer37s( Player37BO entity ) throws ApplicationException {
	try {
		if( getPlayer37s() != null && getPlayer37s().contains(entity) ) {
			getPlayer37s().remove( entity );
		entity.setTeam37( null );
		}
		} catch ( LazyInitializationException e ) {
				Team37BO bo = ( Team37BO ) finder.findByPrimaryKey( getId() );
			finder.getHibernateTemplate().initialize( bo.getPlayer37s() );
			setPlayer37s( bo.getPlayer37s() );
			// evict object from session to avoid NonUniqueObjectException
			finder.getHibernateTemplate().evict( bo );
		
			if( getPlayer37s() != null && getPlayer37s().contains( entity ) ) {
				getPlayer37s().remove( entity );
				entity.setTeam37( null );
			}
				}
	}
	
	/**
	 * Add entity to the com.bluage.documentation.business.standardfeatures.application.datagrid.uc37_collectionorder.bos.Player37BO collection.
	 *
	 * @param entity
	 *		{@link Player37BO} the entity
	 * @throws ApplicationException
     */
public void addToPlayer37s( Player37BO entity ) throws ApplicationException
	{
		try {
			if( getPlayer37s() != null && !getPlayer37s().contains( entity ) ) {
				getPlayer37s().add( entity );
				entity.setTeam37( this );
			}
			} catch ( LazyInitializationException e ) {
			Team37BO bo = ( Team37BO ) finder.findByPrimaryKey( getId() );
		finder.getHibernateTemplate().initialize( bo.getPlayer37s() );
		setPlayer37s( bo.getPlayer37s() );
		// evict object from session to avoid NonUniqueObjectException
		finder.getHibernateTemplate().evict( bo );
		if( getPlayer37s() != null && !getPlayer37s().contains( entity ) ) {
			getPlayer37s().add( entity );
			entity.setTeam37( this );
		}
			}
	}
	
	/**
	 * Add entitys to the com.bluage.documentation.business.standardfeatures.application.datagrid.uc37_collectionorder.bos.Player37BO collection
	 *
	 * @param entities			
	 *		: {@link java.util.List} A list of entities
	 * @throws ApplicationException 
     */	
public void addAllToPlayer37s( Collection<Player37BO> entities ) throws ApplicationException {
	for( Player37BO entity : entities) {
		if( getPlayer37s()!=null ) {
			addToPlayer37s( entity );
		}
	}
}
	
	/**
	 * Remove entities form the com.bluage.documentation.business.standardfeatures.application.datagrid.uc37_collectionorder.bos.Player37BO collection
	 *
	 * @param entities				
	 *		: {@link java.util.List} a list of entities
	 * @throws ApplicationException
     */	
public void removeAllFromPlayer37s( Collection<Player37BO> entities ) throws ApplicationException {
	for( Player37BO entity : entities) {
		if( getPlayer37s()!=null && getPlayer37s().contains( entity ) )	{		
			removeFromPlayer37s( entity );
		}
	}
}
	
	/**
	 * Remove all items form the collection com.bluage.documentation.business.standardfeatures.application.datagrid.uc37_collectionorder.bos.Player37BO
	 *
	 * @throws ApplicationException
     */	
public void removeAllFromPlayer37s() throws ApplicationException {
	Set<Player37BO> entities = this.getPlayer37s();
	if ( entities == null ) {
		return;
	}
	Iterator<Player37BO> it = entities.iterator();
	while ( it.hasNext() ) {
    	Player37BO entity = it.next(); 
		entity.setTeam37( null ); 
	}
	entities.clear();
}

/**
	 * Returns <code>true</code> if the argument is an Team37 instance and all identifiers for this entity
     * equal the identifiers of the argument entity. The <code>equals</code> method of the parent entity
     * will also need to return <code>true</code>. Returns <code>false</code> otherwise.
     *
     * @see TeamBO#hashCode()
	 * @param object  object to compare with current instance      
     * @return boolean true if equals false if not
     */
    public boolean equals(final Object object )
    {
		if ( this == object )
        {
            return true;
        }
        if ( !( object instanceof Team37BO ) )
        {
            return false;
        }
        final Team37BO that = (Team37BO)object;
    
    	            if (        				        			this.getId() == null
        	        ) {
        	return false;
        }
		return new EqualsBuilder()
			.append(this.getId(), that.getId())
			 	.isEquals();
	}
	/**

	 * Returns a hash code based on this entity's identifiers and the hash code of the parent entity.
     *
     * @see TeamBO#hashCode()
     * @return int
     */    
    public int hashCode() {
        return new HashCodeBuilder()
             .append(getId())
				 	.appendSuper(super.hashCode())
            .toHashCode();
    }
}

