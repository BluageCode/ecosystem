/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.master.components;


import java.io.Serializable;
import java.util.List;

import com.bluage.documentation.business.standardfeatures.application.component.uc47_contents.bos.Screen47BO;
import com.bluage.documentation.business.standardfeatures.application.form.uc41_actionondropdownselection.bos.Position41ForSelectBO;
import com.netfective.bluage.core.web.AbstractForm;
import com.netfective.bluage.core.web.IForm;

/**
* Class : cmp_treeviewForm
*/
public class cmp_treeviewForm extends AbstractForm implements Serializable, IForm {
	
	/** Constant : serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	//CONSTANT : positions
	private static final String POSITIONS = "positions";
	//CONSTANT : allTeams
	private static final String ALL_TEAMS = "allTeams";
	//CONSTANT : screen47
	private static final String SCREEN47 = "screen47";
	//CONSTANT : allPlayers
	private static final String ALL_PLAYERS = "allPlayers";
	/**
	 * 	Property: positions 
	 */
	private Position41ForSelectBO positions;
	/**
	 * 	Property: allTeams 
	 */
	private List allTeams;
	/**
	 * 	Property: screen47 
	 */
	private Screen47BO screen47;
	/**
	 * 	Property: allPlayers 
	 */
	private List allPlayers;
/**
	 * Default constructor : cmp_treeviewForm
	 */
	public cmp_treeviewForm() {
		super();
		// Initialize : positions
		this.positions = null;
		// Initialize : allTeams
		this.allTeams = null;
		// Initialize : screen47
		this.screen47 = null;
		// Initialize : allPlayers
		this.allPlayers = null;
			this.setAlwaysCallPreControllers(true);
	}
	
	/**
	 * 	Getter : positions 
	 * 	@return : Return the positions instance.
	 */
	public Position41ForSelectBO getPositions(){
		return positions; // For cmp_treeviewForm
	}
	
	/**
	 * 	Setter : positions 
	 *  @param positionsinstance : The instance to set.
	 */
	public void setPositions(final Position41ForSelectBO positionsinstance){
		this.positions = positionsinstance;// For cmp_treeviewForm
	}
	/**
	 * 	Getter : allTeams 
	 * 	@return : Return the allTeams instance.
	 */
	public List getAllTeams(){
		return allTeams; // For cmp_treeviewForm
	}
	
	/**
	 * 	Setter : allTeams 
	 *  @param allTeamsinstance : The instance to set.
	 */
	public void setAllTeams(final List allTeamsinstance){
		this.allTeams = allTeamsinstance;// For cmp_treeviewForm
	}
	/**
	 * 	Getter : screen47 
	 * 	@return : Return the screen47 instance.
	 */
	public Screen47BO getScreen47(){
		return screen47; // For cmp_treeviewForm
	}
	
	/**
	 * 	Setter : screen47 
	 *  @param screen47instance : The instance to set.
	 */
	public void setScreen47(final Screen47BO screen47instance){
		this.screen47 = screen47instance;// For cmp_treeviewForm
	}
	/**
	 * 	Getter : allPlayers 
	 * 	@return : Return the allPlayers instance.
	 */
	public List getAllPlayers(){
		return allPlayers; // For cmp_treeviewForm
	}
	
	/**
	 * 	Setter : allPlayers 
	 *  @param allPlayersinstance : The instance to set.
	 */
	public void setAllPlayers(final List allPlayersinstance){
		this.allPlayers = allPlayersinstance;// For cmp_treeviewForm
	}
/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "cmp_treeviewForm [ "+
POSITIONS +" = " + positions +ALL_TEAMS +" = " + allTeams +SCREEN47 +" = " + screen47 +ALL_PLAYERS +" = " + allPlayers + "]";
	}
	
	/**
	 * Method setInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public void setInstanceByInstanceName(final String instanceName, final Object instance) {
		// Set the instance Positions.
		if(POSITIONS.equals(instanceName)){// For cmp_treeviewForm
			this.setPositions((Position41ForSelectBO)instance); // For cmp_treeviewForm
		}
				// Set the instance AllTeams.
		if(ALL_TEAMS.equals(instanceName)){// For cmp_treeviewForm
			this.setAllTeams((List)instance); // For cmp_treeviewForm
		}
				// Set the instance Screen47.
		if(SCREEN47.equals(instanceName)){// For cmp_treeviewForm
			this.setScreen47((Screen47BO)instance); // For cmp_treeviewForm
		}
				// Set the instance AllPlayers.
		if(ALL_PLAYERS.equals(instanceName)){// For cmp_treeviewForm
			this.setAllPlayers((List)instance); // For cmp_treeviewForm
		}
			}
	
	/**
	 * Method getInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public Object getInstanceByInstanceName(final String instanceName){
		// Temporary object for tmpcmp_treeviewForm.
		Object tmpcmp_treeviewForm = null;
		
			
		// Get the instance Positions for cmp_treeviewForm.
		if(POSITIONS.equals(instanceName)){ // For cmp_treeviewForm
			tmpcmp_treeviewForm = this.getPositions(); // For cmp_treeviewForm
		}
			
		// Get the instance AllTeams for cmp_treeviewForm.
		if(ALL_TEAMS.equals(instanceName)){ // For cmp_treeviewForm
			tmpcmp_treeviewForm = this.getAllTeams(); // For cmp_treeviewForm
		}
			
		// Get the instance Screen47 for cmp_treeviewForm.
		if(SCREEN47.equals(instanceName)){ // For cmp_treeviewForm
			tmpcmp_treeviewForm = this.getScreen47(); // For cmp_treeviewForm
		}
			
		// Get the instance AllPlayers for cmp_treeviewForm.
		if(ALL_PLAYERS.equals(instanceName)){ // For cmp_treeviewForm
			tmpcmp_treeviewForm = this.getAllPlayers(); // For cmp_treeviewForm
		}
		return tmpcmp_treeviewForm;// For cmp_treeviewForm
	}
	
	}
