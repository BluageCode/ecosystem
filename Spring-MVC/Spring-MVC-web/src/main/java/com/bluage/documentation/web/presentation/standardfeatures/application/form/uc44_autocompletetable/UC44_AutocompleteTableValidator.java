/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.application.form.uc44_autocompletetable ;


import org.apache.log4j.Logger;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;

import com.netfective.bluage.core.validator.AbstractValidator;
import com.netfective.bluage.core.validator.ValidatorsUtil;
/**
* Class :UC44_AutocompleteTableValidator
*/
public class UC44_AutocompleteTableValidator extends AbstractValidator{
	
	// LOGGER for the class UC44_AutocompleteTableValidator
	private static final Logger LOGGER = Logger.getLogger( UC44_AutocompleteTableValidator.class);
	
	
	/**
	* Operation validate for UC44_AutocompleteTableForm
	* @param obj : the current form (UC44_AutocompleteTableForm)
	* @param errors : The spring errors to return for the form UC44_AutocompleteTableForm
	*/
	public void validate(final Object obj,final Errors errors){
		LOGGER.info("Beginning method : validate"); // UC44_AutocompleteTableValidator
		UC44_AutocompleteTableForm cUC44_AutocompleteTableForm = (UC44_AutocompleteTableForm)obj; // UC44_AutocompleteTableValidator
							ValidationUtils.rejectIfEmpty(errors, "allPlayers["+getRequestIndex()+"].firstName", "", "First name is required.");

		if(errors.getFieldError("allPlayers["+getRequestIndex()+"].firstName")== null){
								if("selectedPlayer".equals(getTableElement())){
			if(LOGGER.isDebugEnabled()){
				LOGGER.debug("validation of the 'allPlayers["+getRequestIndex()+"].firstName' field with the LengthValidator validator");
			}
			
			if(ValidatorsUtil.lengthValidator(cUC44_AutocompleteTableForm.getAllPlayers().get(Integer.valueOf(getRequestIndex())).getFirstName(), 1, 30) == 0){
				errors.rejectValue("allPlayers["+getRequestIndex()+"].firstName", "", "First name should be less than 30 characters long.");
			} 
		}
				
		}
							ValidationUtils.rejectIfEmpty(errors, "allPlayers["+getRequestIndex()+"].lastName", "", "Last name is required.");

		if(errors.getFieldError("allPlayers["+getRequestIndex()+"].lastName")== null){
								if("selectedPlayer".equals(getTableElement())){
			if(LOGGER.isDebugEnabled()){
				LOGGER.debug("validation of the 'allPlayers["+getRequestIndex()+"].lastName' field with the LengthValidator validator");
			}
			
			if(ValidatorsUtil.lengthValidator(cUC44_AutocompleteTableForm.getAllPlayers().get(Integer.valueOf(getRequestIndex())).getLastName(), 1, 30) == 0){
				errors.rejectValue("allPlayers["+getRequestIndex()+"].lastName", "", "Last name should be less than 30 characters long.");
			} 
		}
				
		}
							ValidationUtils.rejectIfEmpty(errors, "selectedTeam.name", "", "Team name is required.");

								ValidationUtils.rejectIfEmpty(errors, "allPlayers["+getRequestIndex()+"].dateOfBirth", "", "Date of Birth is required.");

			LOGGER.info("Ending method : validate the form "+ cUC44_AutocompleteTableForm.getClass().getName()); // UC44_AutocompleteTableValidator
	}

	/**
	* Method to implements to use spring validators (UC44_AutocompleteTableForm)
	* @param aClass : Class for the form UC44_AutocompleteTableForm
	* @return boolean
	*/
	public boolean supports(final Class aClass){
		return (new UC44_AutocompleteTableForm()).getClass().equals(aClass); // UC44_AutocompleteTableValidator
	}
}
