/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.gettingstarted.uc05_search;


import java.io.Serializable;

import com.bluage.documentation.business.gettingstarted.uc05_search.bos.Player05BO;
import com.netfective.bluage.core.web.AbstractForm;
import com.netfective.bluage.core.web.IForm;

/**
* Class : UC05_ViewForm
*/
public class UC05_ViewForm extends AbstractForm implements Serializable, IForm {
	
	/** Constant : serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	//CONSTANT : currentPlayer
	private static final String CURRENT_PLAYER = "currentPlayer";
	/**
	 * 	Property: currentPlayer 
	 */
	private Player05BO currentPlayer;
/**
	 * Default constructor : UC05_ViewForm
	 */
	public UC05_ViewForm() {
		super();
		// Initialize : currentPlayer
		this.currentPlayer = new Player05BO();
			this.setAlwaysCallPreControllers(true);
	}
	
	/**
	 * 	Getter : currentPlayer 
	 * 	@return : Return the currentPlayer instance.
	 */
	public Player05BO getCurrentPlayer(){
		return currentPlayer; // For UC05_ViewForm
	}
	
	/**
	 * 	Setter : currentPlayer 
	 *  @param currentPlayerinstance : The instance to set.
	 */
	public void setCurrentPlayer(final Player05BO currentPlayerinstance){
		this.currentPlayer = currentPlayerinstance;// For UC05_ViewForm
	}
/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UC05_ViewForm [ "+
CURRENT_PLAYER +" = " + currentPlayer + "]";
	}
	
	/**
	 * Method setInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public void setInstanceByInstanceName(final String instanceName, final Object instance) {
		// Set the instance CurrentPlayer.
		if(CURRENT_PLAYER.equals(instanceName)){// For UC05_ViewForm
			this.setCurrentPlayer((Player05BO)instance); // For UC05_ViewForm
		}
			}
	
	/**
	 * Method getInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public Object getInstanceByInstanceName(final String instanceName){
		// Temporary object for tmpUC05_ViewForm.
		Object tmpUC05_ViewForm = null;
		
			
		// Get the instance CurrentPlayer for UC05_ViewForm.
		if(CURRENT_PLAYER.equals(instanceName)){ // For UC05_ViewForm
			tmpUC05_ViewForm = this.getCurrentPlayer(); // For UC05_ViewForm
		}
		return tmpUC05_ViewForm;// For UC05_ViewForm
	}
	
	}
