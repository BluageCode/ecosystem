--<ScriptOptions statementTerminator=";"/>

ALTER TABLE player60 DROP CONSTRAINT player60_team_fkc;

ALTER TABLE player60 DROP CONSTRAINT player60_pkey;

ALTER TABLE team60 DROP CONSTRAINT team60_pkey;

DROP TABLE team60;

DROP TABLE player60;

CREATE TABLE team60 (
		id INT8 NOT NULL,
		version INT4 NOT NULL,
		city VARCHAR(1024),
		name VARCHAR(1024)
	);

CREATE TABLE player60 (
		id INT8 NOT NULL,
		version INT4 NOT NULL,
		first_name VARCHAR(1024),
		last_name VARCHAR(1024),
		date_of_birth DATE,
		estimated_value FLOAT4,
		weight INT4,
		height INT4,
		rookie BOOL,
		team_fk INT8 NOT NULL
	);
	
INSERT INTO team60 (id, version, city, name) VALUES (1, 0, 'Charlotte', 'Tigers');
INSERT INTO team60 (id, version, city, name) VALUES (2, 0, 'Kansas City', 'Bears');
INSERT INTO team60 (id, version, city, name) VALUES (3, 0, 'Detroit', 'Chargers');
INSERT INTO team60 (id, version, city, name) VALUES (4, 0, 'Detroit', 'Hawks');
INSERT INTO team60 (id, version, city, name) VALUES (5, 0, 'Omaha', 'Braves');
INSERT INTO team60 (id, version, city, name) VALUES (6, 0, 'Raleigh', 'Eagles');
INSERT INTO team60 (id, version, city, name) VALUES (7, 0, 'Oakland', 'Cardinals');
INSERT INTO team60 (id, version, city, name) VALUES (8, 0, 'Kansas City', 'Red Devils');
INSERT INTO team60 (id, version, city, name) VALUES (9, 0, 'Fresno', 'Mustangs');
INSERT INTO team60 (id, version, city, name) VALUES (10, 0, 'Sacramento', 'Wolverines');
INSERT INTO team60 (id, version, city, name) VALUES (11, 0, 'Nashville', 'Crusaders');
INSERT INTO team60 (id, version, city, name) VALUES (12, 0, 'Fort Worth', 'Trojans');
INSERT INTO team60 (id, version, city, name) VALUES (13, 0, 'Tulsa', 'Vikings');
INSERT INTO team60 (id, version, city, name) VALUES (14, 0, 'Charlotte', 'Blue Devils');
INSERT INTO team60 (id, version, city, name) VALUES (15, 0, 'Cleveland', 'Mustangs');

INSERT INTO player60 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk) VALUES (201, 0, 'Oliver', 'Frank', '1968-10-31', 0.11, 116, 228, false, 1);
INSERT INTO player60 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk) VALUES (202, 0, 'Elbert', 'Cole', '1965-04-07', 3.95, 100, 171, false, 2);
INSERT INTO player60 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk) VALUES (203, 0, 'Abel', 'Dennis', '1980-04-16', 30.51, 105, 208, false, 3);
INSERT INTO player60 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk) VALUES (204, 0, 'Ricky', 'Dawson', '1990-03-03', 29.299999, 115, 168, true, 4);
INSERT INTO player60 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk) VALUES (205, 0, 'Earl', 'Russell', '1981-12-15', 44.73, 60, 234, false, 5);
INSERT INTO player60 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk) VALUES (206, 0, 'Nick', 'Garner', '1992-07-27', 47.84, 87, 221, true, 6);
INSERT INTO player60 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk) VALUES (207, 0, 'Howard', 'Ramos', '1970-08-25', 10.76, 107, 199, false, 7);
INSERT INTO player60 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk) VALUES (208, 0, 'Allan', 'Meyer', '1969-02-14', 33.669998, 114, 229, false, 8);
INSERT INTO player60 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk) VALUES (209, 0, 'Ruben', 'Pope', '1985-05-01', 44.66, 86, 201, false, 9);
INSERT INTO player60 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk) VALUES (210, 0, 'Lester', 'Cross', '1976-02-21', 30.98, 69, 214, false, 10);
INSERT INTO player60 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk) VALUES (211, 0, 'Keith', 'Barnett', '1976-11-10', 5.8099999, 66, 190, false, 2);
INSERT INTO player60 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk) VALUES (212, 0, 'Ivan', 'Tate', '1972-02-08', 48.490002, 143, 175, false, 1);
INSERT INTO player60 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk) VALUES (213, 0, 'Tommy', 'Wood', '1992-01-20', 13.93, 77, 173, true, 3);
INSERT INTO player60 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk) VALUES (214, 0, 'Sylvester', 'Nash', '1965-07-10', 37.73, 68, 184, false, 4);
INSERT INTO player60 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk) VALUES (215, 0, 'Michael', 'Hodges', '1988-04-01', 25.35, 158, 204, false, 5);
INSERT INTO player60 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk) VALUES (216, 0, 'Jerome', 'Grant', '1982-06-28', 28.940001, 105, 205, false, 6);
INSERT INTO player60 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, team_fk) VALUES (217, 0, 'Woodrow', 'Carpenter', '1987-08-08', 41.759998, 69, 224, false, 7);


ALTER TABLE player60 ADD CONSTRAINT player60_pkey PRIMARY KEY (id);

ALTER TABLE team60 ADD CONSTRAINT team60_pkey PRIMARY KEY (id);

ALTER TABLE player60 ADD CONSTRAINT player60_team_fkc FOREIGN KEY (team_fk)
	REFERENCES team60 (id);

