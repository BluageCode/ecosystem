/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.web.presentation.standardfeatures.application.datagrid.uc35_paginatorbdd.page.command;

import java.util.List;

import org.apache.log4j.Logger;

import com.bluage.documentation.business.standardfeatures.application.datagrid.uc35_paginatorbdd.bos.Player35BO;
import com.bluage.documentation.service.standardfeatures.application.datagrid.uc35_paginatorbdd.ServicePaginatorBDD;
import com.netfective.bluage.core.exception.ApplicationException;
import com.netfective.bluage.core.presentation.screen.paginator.AbstractPageCommand;
public class PageCommandListPlayers extends AbstractPageCommand {
		
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(PageCommandListPlayers.class);
	
	/**
	 * Constructor for PageCommandListPlayers.
	 * @param request HttpServletRequest
	 */
	public PageCommandListPlayers(int numberOfItems, int currentPage) {
		this.setItemsPerPage(numberOfItems);
		this.setNumberOfItems(numberOfItems);
		this.setCurrentPage(currentPage);
	}

		
	private List<Player35BO> results;
	
	/**
	 * @see com.netfective.bluage.core.presentation.screen.paginator.IPageCommand#getResults()
	 */
	public List<Player35BO> getResults() {
		return results;
	}

	/**
	 * @see com.netfective.bluage.core.presentation.screen.paginator.IPageCommand#updateResults()
	 */
	public void updateResults() {	    
	    try {
	    	ServicePaginatorBDD instanceServicePaginatorBDD = (ServicePaginatorBDD) getBean("com.bluage.documentation.service.standardfeatures.application.datagrid.uc35_paginatorbdd.ServicePaginatorBDD");
	    	results = instanceServicePaginatorBDD.displaysPlayers(this);
	    }
	    catch (ApplicationException e) {
			LOGGER.error("An error occured while callong the service : ServicePaginatorBDD with operation named : displaysPlayers. A null object will be returned.",e);
			results = null;
		}
	}
}
