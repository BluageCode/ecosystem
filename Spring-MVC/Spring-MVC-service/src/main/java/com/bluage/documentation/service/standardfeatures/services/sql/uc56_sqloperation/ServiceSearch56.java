/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.service.standardfeatures.services.sql.uc56_sqloperation;


import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import com.bluage.documentation.business.standardfeatures.services.sql.uc56_sqloperation.bos.Team56BO;
import com.bluage.documentation.service.AbstractService;
import com.bluage.documentation.service.standardfeatures.services.sql.uc56_sqloperation.daofinder.ServiceSearch56DAOMethodFinderImpl;
import com.netfective.bluage.core.exception.ApplicationException;

/**
 * Defines  application services for ServiceSearch56
  */
@Named("com.bluage.documentation.service.standardfeatures.services.sql.uc56_sqloperation.ServiceSearch56")
public class ServiceSearch56  extends AbstractService{


						@Inject
		@Named ("com.bluage.documentation.service.standardfeatures.services.sql.uc56_sqloperation.daofinder.ServiceSearch56DAOMethodFinder")
		private ServiceSearch56DAOMethodFinderImpl serviceSearch56Dao;
			


	/**
	 *  Default constructor 
	 */
	public ServiceSearch56() {
		super(); // Call super for default constructor : ServiceSearch56
	}
	



							
	/**    
	 * Operation SQL.
	 * @param name {@link String}
		 * @return {@link List}
	 */
	public List searchBySqlOperation(String name )
 throws ApplicationException {
	
	return serviceSearch56Dao.searchBySqlOperation(name);
	}

		
			
	/**    
	 * Process operation.
	 * @param team {@link Team56BO}
		 * @return {@link List}
	 */
	public List executeSearch(Team56BO team )
 throws ApplicationException {

				return privateExecuteSearch(team);
	}
	

	/**    
	 * Process operation.
	 * @param team {@link Team56BO}
		 * @return {@link List}
	 */
	private List privateExecuteSearch(Team56BO team )
 throws ApplicationException {
						// Return Type declaration.
				List searchResult = new java.util.ArrayList();
					// Central buffer node declaration for operation executeSearch
			String	 critName =  "";
					// Calling an operation : 
critName = team.getName();
/* Executing : */
critName="%"+critName+"%";
// Calling an operation : 
searchResult = searchBySqlOperation(critName);

						return searchResult;
		}

		
							
	/**    
	 * Operation SQL.
	 * @param name {@link String}
		 * @return {@link List}
	 */
	public List searchBySqlOperationPostProcess(String name )
 throws ApplicationException {
	
	return serviceSearch56Dao.searchBySqlOperationPostProcess(name);
	}

		
			
	/**    
	 * Process operation.
	 * @param team {@link Team56BO}
		 * @return {@link List}
	 */
	public List executeSearchPostProcess(Team56BO team )
 throws ApplicationException {

				return privateExecuteSearchPostProcess(team);
	}
	

	/**    
	 * Process operation.
	 * @param team {@link Team56BO}
		 * @return {@link List}
	 */
	private List privateExecuteSearchPostProcess(Team56BO team )
 throws ApplicationException {
						// Return Type declaration.
				List searchResultPostProcess = new java.util.ArrayList();
					// Central buffer node declaration for operation executeSearchPostProcess
			String	 name =  "";
					// Calling an operation : 
name = team.getName();
/* Executing : */
name="%"+name+"%";
// Calling an operation : 
searchResultPostProcess = searchBySqlOperationPostProcess(name);

						return searchResultPostProcess;
		}

		}
