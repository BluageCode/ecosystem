/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.application.datagrid.uc32_calculabletable ;

// Import declaration.
// Java imports.
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.bluage.documentation.business.standardfeatures.application.datagrid.uc32_calculabletable.bos.Player32BO;
import com.bluage.documentation.business.standardfeatures.application.datagrid.uc32_calculabletable.entities.daofinder.Player32DAOFinderImpl;
import com.bluage.documentation.service.standardfeatures.application.datagrid.uc32_calculabletable.Serviceplayer32Update;
import com.netfective.bluage.core.exception.ApplicationException;
import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;
import com.netfective.bluage.core.web.IForm;

/**
 * Class : UC32_calculableTableController
 */
@Controller
@Scope("prototype")
@SessionAttributes("uC32_calculableTableForm")
@RequestMapping(value= "/presentation/standardfeatures/application/datagrid/uc32_calculabletable")
public class UC32_calculableTableController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC32_calculableTableController.class);
	
	//Current form name
	private static final String U_C32_CALCULABLE_TABLE_FORM = "uC32_calculableTableForm";

	//CONSTANT: allPlayers table or repeater.
	private static final String ALL_PLAYERS = "allPlayers";
				//CONSTANT: playerToUpdate
	private static final String PLAYER_TO_UPDATE = "playerToUpdate";
	
	/**
	 * Property:customDateEditorsUC32_calculableTableController
	 */
	final private Map<String,CustomDateEditor> customDateEditorsUC32_calculableTableController = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:uC32_calculableTableValidator
	 */
	final private UC32_calculableTableValidator uC32_calculableTableValidator = new UC32_calculableTableValidator();
	
	/**
	 * Service declaration : serviceplayer32Update.
	 */
	@Autowired
	private Serviceplayer32Update serviceplayer32Update;
	
	/**
	 * Generic Finder : player32DAOFinderImpl.
	 */
	@Autowired
	private Player32DAOFinderImpl player32DAOFinderImpl;
	
	
	// Initialise all the instances for UC32_calculableTableController
		// Initialize the instance allPlayers for the state lnk_stadiums_global
		private List allPlayers; // Initialize the instance allPlayers for UC32_calculableTableController
						// Declare the instance playerToUpdate
		private Player32BO playerToUpdate;
			/**
	 * Operation : lnk_update
 	 * @param model : 
 	 * @param uC32_calculableTable : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC32_calculableTable/lnk_update.html",method = RequestMethod.POST)
	public String lnk_update(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC32_calculableTableForm") UC32_calculableTableForm  uC32_calculableTableForm, final BindingResult bindingResult ,final @RequestParam(ID_REQUEST_PARAM) Integer index){
		
		
		
		LOGGER.info("Begin method : lnk_update"); 
		infoLogger(uC32_calculableTableForm); 
		uC32_calculableTableForm.setAlwaysCallPreControllers(false); 
		// initialization for lnk_update
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state lnk_update if the validation fail, you will be forward to the screen : "/presentation/standardfeatures/application/datagrid/uc32_calculabletable/UC32_calculableTable"
		if(errorsMessages(request,response,uC32_calculableTableForm,bindingResult,"playerToUpdate", uC32_calculableTableValidator, customDateEditorsUC32_calculableTableController)){ 
			return "/presentation/standardfeatures/application/datagrid/uc32_calculabletable/UC32_calculableTable"; 
		}
		// Updating selected row for the state lnk_update 
		uC32_calculableTableForm.setSelectedRow(-1); //Set selected row for lnk_update 
		// Updating selected table for the state lnk_update 
		uC32_calculableTableForm.setSelectedTab(null); //reset selected row for lnk_update 

										 callUpdatePlayer32(request,uC32_calculableTableForm , index);
						// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.application.datagrid.uc32_calculabletable.UC32_calculableTableForm or not from lnk_update
			// Populate the destination form with all the instances returned from lnk_update.
			final IForm uC32_calculableTableForm2 = populateDestinationForm(request.getSession(), uC32_calculableTableForm); 
					
			// Add the form to the model.
			model.addAttribute("uC32_calculableTableForm", uC32_calculableTableForm2); 
			
			request.getSession().setAttribute("uC32_calculableTableForm", uC32_calculableTableForm2);
			
			// "OK" CASE => destination screen path from lnk_update
			LOGGER.info("Go to the screen 'UC32_calculableTable'.");
			// Redirect (PRG) from lnk_update
			destinationPath =  "redirect:/presentation/standardfeatures/application/datagrid/uc32_calculabletable/UC32_calculableTable.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_update 
	}
	
		/**
	 * Operation : lnk_edit
 	 * @param model : The model
 	 * @param uC32_calculableTable : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC32_calculableTable/lnk_edit.html")
	public String lnk_edit(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC32_calculableTableForm") UC32_calculableTableForm  uC32_calculableTableForm, BindingResult bindingResult, final @RequestParam(ID_REQUEST_PARAM) Integer index){
		uC32_calculableTableForm.setSelectedRow(index);	
		String tableId = request.getParameter(TAB_REQUEST_PARAM);
		uC32_calculableTableForm.setSelectedTab(tableId);
		
		Player32BO playerToUpdate = (Player32BO) uC32_calculableTableForm.getAllPlayers().get(index);
		uC32_calculableTableForm.setPlayerToUpdate(playerToUpdate);
		return "/presentation/standardfeatures/application/datagrid/uc32_calculabletable/UC32_calculableTable";
	}

	/**
	* This method initialise the form : UC32_calculableTableForm 
	* @return UC32_calculableTableForm
	*/
	@ModelAttribute("UC32_calculableTableFormInit")
	public void initUC32_calculableTableForm(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, U_C32_CALCULABLE_TABLE_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method UC32_calculableTableForm."); //for lnk_edit 
		}
		UC32_calculableTableForm uC32_calculableTableForm;
	
		if(request.getSession().getAttribute(U_C32_CALCULABLE_TABLE_FORM) != null){
			uC32_calculableTableForm = (UC32_calculableTableForm)request.getSession().getAttribute(U_C32_CALCULABLE_TABLE_FORM);
		} else {
			uC32_calculableTableForm = new UC32_calculableTableForm();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form UC32_calculableTableForm.");
		}
		uC32_calculableTableForm = (UC32_calculableTableForm)populateDestinationForm(request.getSession(), uC32_calculableTableForm);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  UC32_calculableTableForm.");
		}
		model.addAttribute(U_C32_CALCULABLE_TABLE_FORM, uC32_calculableTableForm);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param UC32_calculableTableForm : The sceen form.
	 */
	@RequestMapping(value = "/UC32_calculableTable.html" ,method = RequestMethod.GET)
	public void prepareUC32_calculableTable(final HttpServletRequest request,final  Model model,final  @ModelAttribute("UC32_calculableTableFormInit") UC32_calculableTableForm uC32_calculableTableForm){
		
		UC32_calculableTableForm currentUC32_calculableTableForm = uC32_calculableTableForm;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(U_C32_CALCULABLE_TABLE_FORM) == null){
			if(currentUC32_calculableTableForm!=null){
				request.getSession().setAttribute(U_C32_CALCULABLE_TABLE_FORM, currentUC32_calculableTableForm);
			}else {
				currentUC32_calculableTableForm = new UC32_calculableTableForm();
				request.getSession().setAttribute(U_C32_CALCULABLE_TABLE_FORM, currentUC32_calculableTableForm);	
			}
		} else {
			currentUC32_calculableTableForm = (UC32_calculableTableForm) request.getSession().getAttribute(U_C32_CALCULABLE_TABLE_FORM);
		}

		try {
			List allPlayers = player32DAOFinderImpl.findAll();
			put(request.getSession(), ALL_PLAYERS, allPlayers);
		} catch (ApplicationException e) {
			errorLogger("An error while calling the generic finder for the instance : allPlayers.",e);
		}
				currentUC32_calculableTableForm = (UC32_calculableTableForm)populateDestinationForm(request.getSession(), currentUC32_calculableTableForm);
		// Call all the Precontroller.
	currentUC32_calculableTableForm.setAlwaysCallPreControllers(true);
		model.addAttribute(U_C32_CALCULABLE_TABLE_FORM, currentUC32_calculableTableForm);
		request.getSession().setAttribute(U_C32_CALCULABLE_TABLE_FORM, currentUC32_calculableTableForm);
		
	}
	
										/**
	 * method callUpdatePlayer32
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 * @param index Integer the current index 
	 */
	private void callUpdatePlayer32(HttpServletRequest request,IForm form,Integer index) {
					if(index!=null){  
			 playerToUpdate = (Player32BO)((List)get(request.getSession(),form, "allPlayers")).get(index);  
		} else {
			 playerToUpdate= null; 
		}
				 
					try {
				// executing UpdatePlayer32 in lnk_edit
	serviceplayer32Update.player32Update(
			playerToUpdate
			);  

													// processing variables UpdatePlayer32 in lnk_edit
				put(request.getSession(), PLAYER_TO_UPDATE,playerToUpdate); 
			
			} catch (ApplicationException e) { 
				// error handling for operation player32Update called UpdatePlayer32
				errorLogger("An error occured during the execution of the operation : player32Update",e); 
		
			}
	}
		
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("UC32_calculableTableController [ ");
			strBToS.append(ALL_PLAYERS);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(allPlayers);
			strBToS.append(END_TO_STRING_DELIMITER);
						strBToS.append(PLAYER_TO_UPDATE);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(playerToUpdate);
			strBToS.append(END_TO_STRING_DELIMITER);
		// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(uC32_calculableTableValidator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsUC32_calculableTableController!=null); 
		return strBToS.toString();
	}
}
