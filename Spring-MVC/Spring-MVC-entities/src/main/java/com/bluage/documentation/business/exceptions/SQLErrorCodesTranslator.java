/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package   com.bluage.documentation.business.exceptions;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.support.SQLErrorCodeSQLExceptionTranslator;

import com.netfective.bluage.core.exception.ExceptionTypes;


/**		 
 * SQLErrorCodesTranslator
 *
 */
public class SQLErrorCodesTranslator extends SQLErrorCodeSQLExceptionTranslator {
	// SQL ERROR CODES FOR POSTGRESQL
	private static final int POSTGRESQL_DB_NON_UNIQUE_OBJECT_KEY = 23505;
	private static final int POSTGRESQL_NULL_KEY_NOT_ALLOWED = 23502;
	private static final int POSTGRESQL_CONSTRAINT_VIOLATION_1 = 23000;
	private static final int POSTGRESQL_CONSTRAINT_VIOLATION_2 = 40002;
	/**
	 * 
	 * 
	 * @param task A task
	 * @param sql						
	 * @param sqlex						
	 * @return DataAccessException :	A data access exception
	 */
	protected final DataAccessException customTranslate( final String task, final String sql, final SQLException sqlex ) {
 		// PostgreSQL
		if ( sqlex.getErrorCode(  ) == POSTGRESQL_DB_NON_UNIQUE_OBJECT_KEY ) {
			return new UniqueConstraintViolationException( ExceptionTypes.DB_NON_UNIQUE_OBJECT_KEY, sqlex );
		}	else if ( sqlex.getErrorCode(  ) == POSTGRESQL_NULL_KEY_NOT_ALLOWED ) {
			return new NonNullConstraintViolationException( ExceptionTypes.NULL_KEY_NOT_ALLOWED, sqlex );
		}	else if ( sqlex.getErrorCode(  )  == POSTGRESQL_CONSTRAINT_VIOLATION_1 || sqlex.getErrorCode(  )  == POSTGRESQL_CONSTRAINT_VIOLATION_2 ) {
			return  new ConstraintViolationException( ExceptionTypes.CONSTRAINT_VIOLATION, sqlex );
		}
		return null;
	}
}

