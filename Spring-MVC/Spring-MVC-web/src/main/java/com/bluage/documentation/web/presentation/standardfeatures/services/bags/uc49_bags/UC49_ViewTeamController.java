/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.services.bags.uc49_bags ;

// Import declaration.
// Java imports.
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.bluage.documentation.business.standardfeatures.services.bags.uc49_bags.bos.Team49BO;
import com.bluage.documentation.service.common.utility.ServiceUtility;
import com.netfective.bluage.core.exception.ApplicationException;
import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;
import com.netfective.bluage.core.web.IForm;

/**
 * Class : UC49_ViewTeamController
 */
@Controller
@Scope("prototype")
@SessionAttributes("uC49_ViewTeamForm")
@RequestMapping(value= "/presentation/standardfeatures/services/bags/uc49_bags")
public class UC49_ViewTeamController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC49_ViewTeamController.class);
	
	//Current form name
	private static final String U_C49__VIEW_TEAM_FORM = "uC49_ViewTeamForm";

	//CONSTANT: teamToDisplay
	private static final String TEAM_TO_DISPLAY = "teamToDisplay";
		
	/**
	 * Property:customDateEditorsUC49_ViewTeamController
	 */
	final private Map<String,CustomDateEditor> customDateEditorsUC49_ViewTeamController = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:uC49_ViewTeamValidator
	 */
	final private UC49_ViewTeamValidator uC49_ViewTeamValidator = new UC49_ViewTeamValidator();
	
	/**
	 * Service declaration : serviceUtility.
	 */
	@Autowired
	private ServiceUtility serviceUtility;
	
	
	// Initialise all the instances for UC49_ViewTeamController
		// Initialize the instance teamToDisplay of type com.bluage.documentation.business.standardfeatures.services.bags.uc49_bags.bos.Team49BO with the value : null for UC49_ViewTeamController
		private Team49BO teamToDisplay;
						/**
	 * Operation : lnk_back
 	 * @param model : 
 	 * @param uC49_ViewTeam : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC49_ViewTeam/lnk_back.html",method = RequestMethod.GET)
	public String lnk_back(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC49_ViewTeamForm") UC49_ViewTeamForm  uC49_ViewTeamForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : lnk_back"); 
		infoLogger(uC49_ViewTeamForm); 
		uC49_ViewTeamForm.setAlwaysCallPreControllers(false); 
		// initialization for lnk_back
		init();
		
		String destinationPath = null; 
		

	 callBack(request,uC49_ViewTeamForm );
											// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.services.bags.uc49_bags.UC49_BAGSForm or not from lnk_back
			final UC49_BAGSForm uC49_BAGSForm =  new UC49_BAGSForm();
			// Populate the destination form with all the instances returned from lnk_back.
			final IForm uC49_BAGSForm2 = populateDestinationForm(request.getSession(), uC49_BAGSForm); 
					
			// Add the form to the model.
			model.addAttribute("uC49_BAGSForm", uC49_BAGSForm2); 
			
			request.getSession().setAttribute("uC49_BAGSForm", uC49_BAGSForm2);
			
			// "OK" CASE => destination screen path from lnk_back
			LOGGER.info("Go to the screen 'UC49_BAGS'.");
			// Redirect (PRG) from lnk_back
			destinationPath =  "redirect:/presentation/standardfeatures/services/bags/uc49_bags/UC49_BAGS.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_back 
	}
	
	
	/**
	* This method initialise the form : UC49_ViewTeamForm 
	* @return UC49_ViewTeamForm
	*/
	@ModelAttribute("UC49_ViewTeamFormInit")
	public void initUC49_ViewTeamForm(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, U_C49__VIEW_TEAM_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method UC49_ViewTeamForm."); //for lnk_back 
		}
		UC49_ViewTeamForm uC49_ViewTeamForm;
	
		if(request.getSession().getAttribute(U_C49__VIEW_TEAM_FORM) != null){
			uC49_ViewTeamForm = (UC49_ViewTeamForm)request.getSession().getAttribute(U_C49__VIEW_TEAM_FORM);
		} else {
			uC49_ViewTeamForm = new UC49_ViewTeamForm();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form UC49_ViewTeamForm.");
		}
		uC49_ViewTeamForm = (UC49_ViewTeamForm)populateDestinationForm(request.getSession(), uC49_ViewTeamForm);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  UC49_ViewTeamForm.");
		}
		model.addAttribute(U_C49__VIEW_TEAM_FORM, uC49_ViewTeamForm);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param UC49_ViewTeamForm : The sceen form.
	 */
	@RequestMapping(value = "/UC49_ViewTeam.html" ,method = RequestMethod.GET)
	public void prepareUC49_ViewTeam(final HttpServletRequest request,final  Model model,final  @ModelAttribute("UC49_ViewTeamFormInit") UC49_ViewTeamForm uC49_ViewTeamForm){
		
		UC49_ViewTeamForm currentUC49_ViewTeamForm = uC49_ViewTeamForm;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(U_C49__VIEW_TEAM_FORM) == null){
			if(currentUC49_ViewTeamForm!=null){
				request.getSession().setAttribute(U_C49__VIEW_TEAM_FORM, currentUC49_ViewTeamForm);
			}else {
				currentUC49_ViewTeamForm = new UC49_ViewTeamForm();
				request.getSession().setAttribute(U_C49__VIEW_TEAM_FORM, currentUC49_ViewTeamForm);	
			}
		} else {
			currentUC49_ViewTeamForm = (UC49_ViewTeamForm) request.getSession().getAttribute(U_C49__VIEW_TEAM_FORM);
		}

		currentUC49_ViewTeamForm = (UC49_ViewTeamForm)populateDestinationForm(request.getSession(), currentUC49_ViewTeamForm);
		// Call all the Precontroller.
	currentUC49_ViewTeamForm.setAlwaysCallPreControllers(true);
		model.addAttribute(U_C49__VIEW_TEAM_FORM, currentUC49_ViewTeamForm);
		request.getSession().setAttribute(U_C49__VIEW_TEAM_FORM, currentUC49_ViewTeamForm);
		
	}
	
	/**
	 * method callBack
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callBack(HttpServletRequest request,IForm form) {
					try {
				// executing Back in lnk_back
	serviceUtility.doNothing(
	);  

								// processing variables Back in lnk_back

			} catch (ApplicationException e) { 
				// error handling for operation doNothing called Back
				errorLogger("An error occured during the execution of the operation : doNothing",e); 
		
			}
	}
		
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("UC49_ViewTeamController [ ");
			strBToS.append(TEAM_TO_DISPLAY);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(teamToDisplay);
			strBToS.append(END_TO_STRING_DELIMITER);
			// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(uC49_ViewTeamValidator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsUC49_ViewTeamController!=null); 
		return strBToS.toString();
	}
}
