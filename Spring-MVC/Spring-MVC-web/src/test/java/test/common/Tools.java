package test.common;

public class Tools {

	public static String version;
	public static String SpringMCV = "BA-Doc-Spring-MVC";
	public static String Struts = "BA-Doc-Struts";

	public static String getVersion() {

		version = System.getProperty("version");

		return version;
	}

	public static String IDSpringMCV(String helpername, String fieldID) {
		return "//*[contains(@id,'" + helpername + "." + fieldID + ".errors')]";
	}

	public static String IDStruts(String fieldID) {
		return "//*[contains(@id,'error_" + fieldID + "')]";
	}

}
