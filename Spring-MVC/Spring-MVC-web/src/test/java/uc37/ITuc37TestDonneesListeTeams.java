package uc37;

import org.junit.Test;
import test.common.AbstractBADocTestCase;

public class ITuc37TestDonneesListeTeams extends AbstractBADocTestCase {
	
	@Override
	public  String getName(){
		return "UC-37";
	}
	
	@Test
	public void testITuc37TestDonneesListeTeams() throws Exception {
		// On teste l'UC37 - Standard Features/Application/Datagrid - Collection Order
		selenium.open("");
		selenium.click("link=Expand All");
		selenium.waitForPageToLoad("30000");
		selenium.click("link=UC37 - Collection Order");
		verifyTrue(selenium.isElementPresent("//h2[contains(text(),\"Standard Features > Application > DataGrid > UC37 > Collection Order\")]"));
		// On vérifie que les données sont bien affichées.
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/thead/tr[th[contains(text(),\"Name\")] and th[contains(text(),\"City\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Charlotte\")] and td/span[contains(text(),\"Tigers\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Kansas City\")] and td/span[contains(text(),\"Bears\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Detroit\")] and td/span[contains(text(),\"Chargers\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Detroit\")] and td/span[contains(text(),\"Hawks\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Omaha\")] and td/span[contains(text(),\"Braves\")]]"));
		// On indexe la ligne de Charlotte.
		Number index = selenium.getElementIndex("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Charlotte\")] and td/span[contains(text(),\"Tigers\")]]");
		System.out.println("variable index vaut : " + index);
		// On clique sur [view]
		selenium.click("//table[contains(@id,'tab_teams')]/tbody/tr[" + index + " + 1]/td/a[contains(text(),\"[view]\")]");
		selenium.waitForPageToLoad("30000");
		// On vérifie que l'on est sur la bonne page.
		assertEquals("Tigers", selenium.getText("//*[contains(@id,'txt_TeamName')]"));
		assertEquals("Charlotte", selenium.getText("//*[contains(@id,'txt_Teamcity')]"));
		assertEquals("[back]", selenium.getText("//*[contains(@id,'back')]"));
		// On clique sur [back]
		selenium.click("//*[contains(@id,'back')]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//h2[contains(@class,'rd_first')]"));
	}

}
