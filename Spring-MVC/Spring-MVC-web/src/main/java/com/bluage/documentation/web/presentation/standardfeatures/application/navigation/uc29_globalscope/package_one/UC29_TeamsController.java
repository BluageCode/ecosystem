/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.application.navigation.uc29_globalscope.package_one ;

// Import declaration.
// Java imports.
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;

/**
 * Class : UC29_TeamsController
 */
@Controller
@Scope("prototype")
@SessionAttributes("uC29_TeamsForm")
@RequestMapping(value= "/presentation/standardfeatures/application/navigation/uc29_globalscope/package_one")
public class UC29_TeamsController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC29_TeamsController.class);
	
	//Current form name
	private static final String U_C29__TEAMS_FORM = "uC29_TeamsForm";

	//CONSTANT: allTeams table or repeater.
	private static final String ALL_TEAMS = "allTeams";
				
	/**
	 * Property:customDateEditorsUC29_TeamsController
	 */
	final private Map<String,CustomDateEditor> customDateEditorsUC29_TeamsController = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:uC29_TeamsValidator
	 */
	final private UC29_TeamsValidator uC29_TeamsValidator = new UC29_TeamsValidator();
	
	/**
	 * Pre Controller declaration : uC29_PreControllerFindAllTeams
	 */
	@Autowired
	private UC29_PreControllerFindAllTeamsController uC29_PreControllerFindAllTeams;

	
	// Initialise all the instances for UC29_TeamsController
		// Initialize the instance allTeams for the state findPlayers
		private List allTeams; // Initialize the instance allTeams for UC29_TeamsController
				
	/**
	* This method initialise the form : UC29_TeamsForm 
	* @return UC29_TeamsForm
	*/
	@ModelAttribute("UC29_TeamsFormInit")
	public void initUC29_TeamsForm(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, U_C29__TEAMS_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method UC29_TeamsForm."); //for findPlayers 
		}
		UC29_TeamsForm uC29_TeamsForm;
	
		if(request.getSession().getAttribute(U_C29__TEAMS_FORM) != null){
			uC29_TeamsForm = (UC29_TeamsForm)request.getSession().getAttribute(U_C29__TEAMS_FORM);
		} else {
			uC29_TeamsForm = new UC29_TeamsForm();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form UC29_TeamsForm.");
		}
		uC29_TeamsForm = (UC29_TeamsForm)populateDestinationForm(request.getSession(), uC29_TeamsForm);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  UC29_TeamsForm.");
		}
		model.addAttribute(U_C29__TEAMS_FORM, uC29_TeamsForm);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param UC29_TeamsForm : The sceen form.
	 */
	@RequestMapping(value = "/UC29_Teams.html" ,method = RequestMethod.GET)
	public void prepareUC29_Teams(final HttpServletRequest request,final  Model model,final  @ModelAttribute("UC29_TeamsFormInit") UC29_TeamsForm uC29_TeamsForm){
		
		UC29_TeamsForm currentUC29_TeamsForm = uC29_TeamsForm;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(U_C29__TEAMS_FORM) == null){
			if(currentUC29_TeamsForm!=null){
				request.getSession().setAttribute(U_C29__TEAMS_FORM, currentUC29_TeamsForm);
			}else {
				currentUC29_TeamsForm = new UC29_TeamsForm();
				request.getSession().setAttribute(U_C29__TEAMS_FORM, currentUC29_TeamsForm);	
			}
		} else {
			currentUC29_TeamsForm = (UC29_TeamsForm) request.getSession().getAttribute(U_C29__TEAMS_FORM);
		}

					currentUC29_TeamsForm = (UC29_TeamsForm)populateDestinationForm(request.getSession(), currentUC29_TeamsForm);
		// Call all the Precontroller.
	if ( currentUC29_TeamsForm.isAlwaysCallPreControllers()) {
	
		// Calling the PreController : UC29_PreControllerFindAllTeams.
		currentUC29_TeamsForm = (UC29_TeamsForm) uC29_PreControllerFindAllTeams.uC29_PreControllerFindAllTeamsControllerInit(request, model, currentUC29_TeamsForm);
		
	}
	currentUC29_TeamsForm.setAlwaysCallPreControllers(true);
		model.addAttribute(U_C29__TEAMS_FORM, currentUC29_TeamsForm);
		request.getSession().setAttribute(U_C29__TEAMS_FORM, currentUC29_TeamsForm);
		
	}
	
				
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("UC29_TeamsController [ ");
			strBToS.append(ALL_TEAMS);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(allTeams);
			strBToS.append(END_TO_STRING_DELIMITER);
					// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(uC29_TeamsValidator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsUC29_TeamsController!=null); 
		return strBToS.toString();
	}
}
