/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.gettingstarted.uc03_formvalidation;


import java.io.Serializable;
import java.util.List;

import com.bluage.documentation.business.gettingstarted.uc03_formvalidation.bos.Player03BO;
import com.netfective.bluage.core.web.AbstractForm;
import com.netfective.bluage.core.web.IForm;

/**
* Class : UC03_FormValidationForm
*/
public class UC03_FormValidationForm extends AbstractForm implements Serializable, IForm {
	
	/** Constant : serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	//CONSTANT : allPositions
	private static final String ALL_POSITIONS = "allPositions";
	//CONSTANT : playerToCreate
	private static final String PLAYER_TO_CREATE = "playerToCreate";
	/**
	 * 	Property: allPositions 
	 */
	private List allPositions;
	/**
	 * 	Property: playerToCreate 
	 */
	private Player03BO playerToCreate;
/**
	 * Default constructor : UC03_FormValidationForm
	 */
	public UC03_FormValidationForm() {
		super();
		// Initialize : allPositions
		this.allPositions = new java.util.ArrayList();
		// Initialize : playerToCreate
		this.playerToCreate = new Player03BO();
			this.setAlwaysCallPreControllers(true);
	}
	
	/**
	 * 	Getter : allPositions 
	 * 	@return : Return the allPositions instance.
	 */
	public List getAllPositions(){
		return allPositions; // For UC03_FormValidationForm
	}
	
	/**
	 * 	Setter : allPositions 
	 *  @param allPositionsinstance : The instance to set.
	 */
	public void setAllPositions(final List allPositionsinstance){
		this.allPositions = allPositionsinstance;// For UC03_FormValidationForm
	}
	/**
	 * 	Getter : playerToCreate 
	 * 	@return : Return the playerToCreate instance.
	 */
	public Player03BO getPlayerToCreate(){
		return playerToCreate; // For UC03_FormValidationForm
	}
	
	/**
	 * 	Setter : playerToCreate 
	 *  @param playerToCreateinstance : The instance to set.
	 */
	public void setPlayerToCreate(final Player03BO playerToCreateinstance){
		this.playerToCreate = playerToCreateinstance;// For UC03_FormValidationForm
	}
/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UC03_FormValidationForm [ "+
ALL_POSITIONS +" = " + allPositions +PLAYER_TO_CREATE +" = " + playerToCreate + "]";
	}
	
	/**
	 * Method setInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public void setInstanceByInstanceName(final String instanceName, final Object instance) {
		// Set the instance AllPositions.
		if(ALL_POSITIONS.equals(instanceName)){// For UC03_FormValidationForm
			this.setAllPositions((List)instance); // For UC03_FormValidationForm
		}
				// Set the instance PlayerToCreate.
		if(PLAYER_TO_CREATE.equals(instanceName)){// For UC03_FormValidationForm
			this.setPlayerToCreate((Player03BO)instance); // For UC03_FormValidationForm
		}
			}
	
	/**
	 * Method getInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public Object getInstanceByInstanceName(final String instanceName){
		// Temporary object for tmpUC03_FormValidationForm.
		Object tmpUC03_FormValidationForm = null;
		
			
		// Get the instance AllPositions for UC03_FormValidationForm.
		if(ALL_POSITIONS.equals(instanceName)){ // For UC03_FormValidationForm
			tmpUC03_FormValidationForm = this.getAllPositions(); // For UC03_FormValidationForm
		}
			
		// Get the instance PlayerToCreate for UC03_FormValidationForm.
		if(PLAYER_TO_CREATE.equals(instanceName)){ // For UC03_FormValidationForm
			tmpUC03_FormValidationForm = this.getPlayerToCreate(); // For UC03_FormValidationForm
		}
		return tmpUC03_FormValidationForm;// For UC03_FormValidationForm
	}
	
	}
