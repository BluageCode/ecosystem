/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.application.component.uc47_contents;


import java.io.Serializable;
import java.util.List;

import com.bluage.documentation.business.standardfeatures.application.component.uc47_contents.bos.Player47BO;
import com.bluage.documentation.business.standardfeatures.application.component.uc47_contents.bos.Screen47BO;
import com.bluage.documentation.business.standardfeatures.application.component.uc47_contents.bos.Team47BO;
import com.netfective.bluage.core.web.AbstractForm;
import com.netfective.bluage.core.web.IForm;

/**
* Class : UC47_ContentsForm
*/
public class UC47_ContentsForm extends AbstractForm implements Serializable, IForm {
	
	/** Constant : serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	//CONSTANT : allPlayers
	private static final String ALL_PLAYERS = "allPlayers";
	//CONSTANT : displayTable
	private static final String DISPLAY_TABLE = "displayTable";
	//CONSTANT : listTeam
	private static final String LIST_TEAM = "listTeam";
	//CONSTANT : playerToUpdate
	private static final String PLAYER_TO_UPDATE = "playerToUpdate";
	/**
	 * 	Property: allPlayers 
	 */
	private List allPlayers;
	/**
	 * 	Property: displayTable 
	 */
	private Screen47BO displayTable;
	/**
	 * 	Property: listTeam 
	 */
	private List<Team47BO> listTeam;
	/**
	 * 	Property: playerToUpdate 
	 */
	private Player47BO playerToUpdate;
/**
	 * Default constructor : UC47_ContentsForm
	 */
	public UC47_ContentsForm() {
		super();
		// Initialize : allPlayers
		this.allPlayers = new java.util.ArrayList();
		// Initialize : displayTable
		this.displayTable = new Screen47BO();
		// Initialize : listTeam
		this.listTeam = new java.util.ArrayList<com.bluage.documentation.business.standardfeatures.application.component.uc47_contents.bos.Team47BO>();
		// Initialize : playerToUpdate
		this.playerToUpdate = new Player47BO();
			this.setAlwaysCallPreControllers(true);
	}
	
	/**
	 * 	Getter : allPlayers 
	 * 	@return : Return the allPlayers instance.
	 */
	public List getAllPlayers(){
		return allPlayers; // For UC47_ContentsForm
	}
	
	/**
	 * 	Setter : allPlayers 
	 *  @param allPlayersinstance : The instance to set.
	 */
	public void setAllPlayers(final List allPlayersinstance){
		this.allPlayers = allPlayersinstance;// For UC47_ContentsForm
	}
	/**
	 * 	Getter : displayTable 
	 * 	@return : Return the displayTable instance.
	 */
	public Screen47BO getDisplayTable(){
		return displayTable; // For UC47_ContentsForm
	}
	
	/**
	 * 	Setter : displayTable 
	 *  @param displayTableinstance : The instance to set.
	 */
	public void setDisplayTable(final Screen47BO displayTableinstance){
		this.displayTable = displayTableinstance;// For UC47_ContentsForm
	}
	/**
	 * 	Getter : listTeam 
	 * 	@return : Return the listTeam instance.
	 */
	public List<Team47BO> getListTeam(){
		return listTeam; // For UC47_ContentsForm
	}
	
	/**
	 * 	Setter : listTeam 
	 *  @param listTeaminstance : The instance to set.
	 */
	public void setListTeam(final List<Team47BO> listTeaminstance){
		this.listTeam = listTeaminstance;// For UC47_ContentsForm
	}
	/**
	 * 	Getter : playerToUpdate 
	 * 	@return : Return the playerToUpdate instance.
	 */
	public Player47BO getPlayerToUpdate(){
		return playerToUpdate; // For UC47_ContentsForm
	}
	
	/**
	 * 	Setter : playerToUpdate 
	 *  @param playerToUpdateinstance : The instance to set.
	 */
	public void setPlayerToUpdate(final Player47BO playerToUpdateinstance){
		this.playerToUpdate = playerToUpdateinstance;// For UC47_ContentsForm
	}
/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UC47_ContentsForm [ "+
ALL_PLAYERS +" = " + allPlayers +DISPLAY_TABLE +" = " + displayTable +LIST_TEAM +" = " + listTeam +PLAYER_TO_UPDATE +" = " + playerToUpdate + "]";
	}
	
	/**
	 * Method setInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public void setInstanceByInstanceName(final String instanceName, final Object instance) {
		// Set the instance AllPlayers.
		if(ALL_PLAYERS.equals(instanceName)){// For UC47_ContentsForm
			this.setAllPlayers((List)instance); // For UC47_ContentsForm
		}
				// Set the instance DisplayTable.
		if(DISPLAY_TABLE.equals(instanceName)){// For UC47_ContentsForm
			this.setDisplayTable((Screen47BO)instance); // For UC47_ContentsForm
		}
				// Set the instance ListTeam.
		if(LIST_TEAM.equals(instanceName)){// For UC47_ContentsForm
			this.setListTeam((List<Team47BO>)instance); // For UC47_ContentsForm
		}
				// Set the instance PlayerToUpdate.
		if(PLAYER_TO_UPDATE.equals(instanceName)){// For UC47_ContentsForm
			this.setPlayerToUpdate((Player47BO)instance); // For UC47_ContentsForm
		}
			}
	
	/**
	 * Method getInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public Object getInstanceByInstanceName(final String instanceName){
		// Temporary object for tmpUC47_ContentsForm.
		Object tmpUC47_ContentsForm = null;
		
			
		// Get the instance AllPlayers for UC47_ContentsForm.
		if(ALL_PLAYERS.equals(instanceName)){ // For UC47_ContentsForm
			tmpUC47_ContentsForm = this.getAllPlayers(); // For UC47_ContentsForm
		}
			
		// Get the instance DisplayTable for UC47_ContentsForm.
		if(DISPLAY_TABLE.equals(instanceName)){ // For UC47_ContentsForm
			tmpUC47_ContentsForm = this.getDisplayTable(); // For UC47_ContentsForm
		}
			
		// Get the instance ListTeam for UC47_ContentsForm.
		if(LIST_TEAM.equals(instanceName)){ // For UC47_ContentsForm
			tmpUC47_ContentsForm = this.getListTeam(); // For UC47_ContentsForm
		}
			
		// Get the instance PlayerToUpdate for UC47_ContentsForm.
		if(PLAYER_TO_UPDATE.equals(instanceName)){ // For UC47_ContentsForm
			tmpUC47_ContentsForm = this.getPlayerToUpdate(); // For UC47_ContentsForm
		}
		return tmpUC47_ContentsForm;// For UC47_ContentsForm
	}
	
	}
