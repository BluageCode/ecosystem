/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.entities.uc68_heritagesubclass ;

// Import declaration.
// Java imports.
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.bluage.documentation.business.standardfeatures.entities.uc68_heritagesubclass.entities.daofinder.Player68DAOFinderImpl;
import com.bluage.documentation.business.standardfeatures.entities.uc68_heritagesubclass.entities.daofinder.Professional68DAOFinderImpl;
import com.bluage.documentation.business.standardfeatures.entities.uc68_heritagesubclass.entities.daofinder.Rookie68DAOFinderImpl;
import com.netfective.bluage.core.exception.ApplicationException;
import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;

/**
 * Class : UC68_HeritageSubclassController
 */
@Controller
@Scope("prototype")
@SessionAttributes("uC68_HeritageSubclassForm")
@RequestMapping(value= "/presentation/standardfeatures/entities/uc68_heritagesubclass")
public class UC68_HeritageSubclassController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC68_HeritageSubclassController.class);
	
	//Current form name
	private static final String U_C68__HERITAGE_SUBCLASS_FORM = "uC68_HeritageSubclassForm";

	//CONSTANT: allProfessionals table or repeater.
	private static final String ALL_PROFESSIONALS = "allProfessionals";
				//CONSTANT: allPlayers table or repeater.
	private static final String ALL_PLAYERS = "allPlayers";
				//CONSTANT: allRookies table or repeater.
	private static final String ALL_ROOKIES = "allRookies";
				
	/**
	 * Property:customDateEditorsUC68_HeritageSubclassController
	 */
	final private Map<String,CustomDateEditor> customDateEditorsUC68_HeritageSubclassController = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:uC68_HeritageSubclassValidator
	 */
	final private UC68_HeritageSubclassValidator uC68_HeritageSubclassValidator = new UC68_HeritageSubclassValidator();
	
	/**
	 * Generic Finder : player68DAOFinderImpl.
	 */
	@Autowired
	private Player68DAOFinderImpl player68DAOFinderImpl;
	
	/**
	 * Generic Finder : rookie68DAOFinderImpl.
	 */
	@Autowired
	private Rookie68DAOFinderImpl rookie68DAOFinderImpl;
	
	/**
	 * Generic Finder : professional68DAOFinderImpl.
	 */
	@Autowired
	private Professional68DAOFinderImpl professional68DAOFinderImpl;
	
	
	// Initialise all the instances for UC68_HeritageSubclassController
		// Initialize the instance allProfessionals for the state lnk_addteam
		private List allProfessionals; // Initialize the instance allProfessionals for UC68_HeritageSubclassController
						// Initialize the instance allPlayers for the state lnk_addteam
		private List allPlayers; // Initialize the instance allPlayers for UC68_HeritageSubclassController
						// Initialize the instance allRookies for the state lnk_addteam
		private List allRookies; // Initialize the instance allRookies for UC68_HeritageSubclassController
				
	/**
	* This method initialise the form : UC68_HeritageSubclassForm 
	* @return UC68_HeritageSubclassForm
	*/
	@ModelAttribute("UC68_HeritageSubclassFormInit")
	public void initUC68_HeritageSubclassForm(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, U_C68__HERITAGE_SUBCLASS_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method UC68_HeritageSubclassForm."); //for lnk_addteam 
		}
		UC68_HeritageSubclassForm uC68_HeritageSubclassForm;
	
		if(request.getSession().getAttribute(U_C68__HERITAGE_SUBCLASS_FORM) != null){
			uC68_HeritageSubclassForm = (UC68_HeritageSubclassForm)request.getSession().getAttribute(U_C68__HERITAGE_SUBCLASS_FORM);
		} else {
			uC68_HeritageSubclassForm = new UC68_HeritageSubclassForm();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form UC68_HeritageSubclassForm.");
		}
		uC68_HeritageSubclassForm = (UC68_HeritageSubclassForm)populateDestinationForm(request.getSession(), uC68_HeritageSubclassForm);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  UC68_HeritageSubclassForm.");
		}
		model.addAttribute(U_C68__HERITAGE_SUBCLASS_FORM, uC68_HeritageSubclassForm);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param UC68_HeritageSubclassForm : The sceen form.
	 */
	@RequestMapping(value = "/UC68_HeritageSubclass.html" ,method = RequestMethod.GET)
	public void prepareUC68_HeritageSubclass(final HttpServletRequest request,final  Model model,final  @ModelAttribute("UC68_HeritageSubclassFormInit") UC68_HeritageSubclassForm uC68_HeritageSubclassForm){
		
		UC68_HeritageSubclassForm currentUC68_HeritageSubclassForm = uC68_HeritageSubclassForm;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(U_C68__HERITAGE_SUBCLASS_FORM) == null){
			if(currentUC68_HeritageSubclassForm!=null){
				request.getSession().setAttribute(U_C68__HERITAGE_SUBCLASS_FORM, currentUC68_HeritageSubclassForm);
			}else {
				currentUC68_HeritageSubclassForm = new UC68_HeritageSubclassForm();
				request.getSession().setAttribute(U_C68__HERITAGE_SUBCLASS_FORM, currentUC68_HeritageSubclassForm);	
			}
		} else {
			currentUC68_HeritageSubclassForm = (UC68_HeritageSubclassForm) request.getSession().getAttribute(U_C68__HERITAGE_SUBCLASS_FORM);
		}

		try {
			List allPlayers = player68DAOFinderImpl.findAll();
			put(request.getSession(), ALL_PLAYERS, allPlayers);
		} catch (ApplicationException e) {
			errorLogger("An error while calling the generic finder for the instance : allPlayers.",e);
		}
		try {
			List allRookies = rookie68DAOFinderImpl.findAll();
			put(request.getSession(), ALL_ROOKIES, allRookies);
		} catch (ApplicationException e) {
			errorLogger("An error while calling the generic finder for the instance : allRookies.",e);
		}
		try {
			List allProfessionals = professional68DAOFinderImpl.findAll();
			put(request.getSession(), ALL_PROFESSIONALS, allProfessionals);
		} catch (ApplicationException e) {
			errorLogger("An error while calling the generic finder for the instance : allProfessionals.",e);
		}
								currentUC68_HeritageSubclassForm = (UC68_HeritageSubclassForm)populateDestinationForm(request.getSession(), currentUC68_HeritageSubclassForm);
		// Call all the Precontroller.
	currentUC68_HeritageSubclassForm.setAlwaysCallPreControllers(true);
		model.addAttribute(U_C68__HERITAGE_SUBCLASS_FORM, currentUC68_HeritageSubclassForm);
		request.getSession().setAttribute(U_C68__HERITAGE_SUBCLASS_FORM, currentUC68_HeritageSubclassForm);
		
	}
	
							
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("UC68_HeritageSubclassController [ ");
			strBToS.append(ALL_PROFESSIONALS);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(allProfessionals);
			strBToS.append(END_TO_STRING_DELIMITER);
						strBToS.append(ALL_PLAYERS);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(allPlayers);
			strBToS.append(END_TO_STRING_DELIMITER);
						strBToS.append(ALL_ROOKIES);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(allRookies);
			strBToS.append(END_TO_STRING_DELIMITER);
					// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(uC68_HeritageSubclassValidator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsUC68_HeritageSubclassController!=null); 
		return strBToS.toString();
	}
}
