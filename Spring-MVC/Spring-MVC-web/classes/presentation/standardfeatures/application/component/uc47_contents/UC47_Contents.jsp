<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file="/include.jsp"  %>
<html >
<head >
<title  > 
<fmt:message key="cUC47_Contents_BLU_AGE_DOCUMENTATION"/>
</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"  > 
</meta>
<link href="<c:url value= "/presentation/_style/_style.css"/>" rel="stylesheet" type="text/css"  /> 
<link href="<c:url value= "/presentation/_include/jquery/rd_lib.css"/>" rel="stylesheet" type="text/css"  /> 
<script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/jquery.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/rd_setup.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/rd_lib.js"/>" space="preserve"  ></script><link rel="shortcut icon" href="<c:url value= "/presentation/_style/favicon.ico"/>"  /> 

</head>

<body class=" " > 
<div id="rd_ghost"  > 
<small  > 
<fmt:message key="cUC47_Contents___CSS_DEACTIVATED"/>
</small>
</div>
<div id="rd_jghost"  > 
<small  > 
<fmt:message key="cUC47_Contents___JAVASCRIPT_DEACTIVATED"/>
</small>
</div>
<div id="shape"  > 
<div class="ui-layout-center"  > 
<div class="east-north"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/application/component/uc47_contents/UC47_Contentstablecontents" />
<c:if test="${uC47_ContentsForm.displayTable.table=='true'}"> 
<h4 class="rd_first"  > 
<fmt:message key="cUC47_Contents_CONTENTS_REPEATER"/>
</h4>
<br clear="none"  /> 
<fmt:message key="cUC47_Contents___PLAYER"/>
<b  > 
		<span id="dyn_lastname">
	<c:out value="${uC47_ContentsForm.playerToUpdate.lastName}" />
		</span>
	<fmt:message key="cUC47_Contents_____"/>
		<span id="dyn_firstname">
	<c:out value="${uC47_ContentsForm.playerToUpdate.firstName}" />
		</span>
	</b>
<fmt:message key="cUC47_Contents_HAS_PLAYED_IN_TEAMS_"/>
<br clear="none"  /> 
<br clear="none"  /> 
<c:forEach items="${uC47_ContentsForm.listTeam}" var="varrpt_team" varStatus="_countrpt_team" > 
<fmt:message key="cUC47_Contents______85"/>
		<span id="lbl_Team_name">
	<c:out value="${varrpt_team.name}" />
		</span>
	<br clear="none"  /> 
</c:forEach> 
</c:if>
</div>
<div class="east-center"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/application/component/uc47_contents/UC47_Contentscmp_tabs" />
</div>
</div>
<div class="ui-layout-north"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/application/component/uc47_contents/UC47_Contentscmp_header" />
</div>
<div class="ui-layout-south"  > 
<fmt:message key="cUC47_Contents___"/>
<span id="rd_jversion"  > 
<fmt:message key="cUC47_Contents_BLU_AGE_CORPORATION"/>
</span>
</div>
<div class="ui-layout-west"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/application/component/uc47_contents/UC47_Contentscmp_treeview" />
</div>
</div>
</body>
</html>
