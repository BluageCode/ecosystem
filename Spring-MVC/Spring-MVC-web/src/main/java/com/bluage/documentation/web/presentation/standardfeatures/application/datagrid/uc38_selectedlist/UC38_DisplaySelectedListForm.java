/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.application.datagrid.uc38_selectedlist;


import java.io.Serializable;
import java.util.List;

import com.bluage.documentation.business.standardfeatures.application.datagrid.uc38_selectedlist.bos.Player38BO;
import com.netfective.bluage.core.web.AbstractForm;
import com.netfective.bluage.core.web.IForm;

/**
* Class : UC38_DisplaySelectedListForm
*/
public class UC38_DisplaySelectedListForm extends AbstractForm implements Serializable, IForm {
	
	/** Constant : serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	//CONSTANT : teams
	private static final String TEAMS = "teams";
	//CONSTANT : allPlayers
	private static final String ALL_PLAYERS = "allPlayers";
	//CONSTANT : playerToUpdate
	private static final String PLAYER_TO_UPDATE = "playerToUpdate";
	/**
	 * 	Property: teams 
	 */
	private List teams;
	/**
	 * 	Property: allPlayers 
	 */
	private List<Player38BO> allPlayers;
	/**
	 * 	Property: playerToUpdate 
	 */
	private Player38BO playerToUpdate;
/**
	 * Default constructor : UC38_DisplaySelectedListForm
	 */
	public UC38_DisplaySelectedListForm() {
		super();
		// Initialize : teams
		this.teams = new java.util.ArrayList();
		// Initialize : allPlayers
		this.allPlayers = new java.util.ArrayList<com.bluage.documentation.business.standardfeatures.application.datagrid.uc38_selectedlist.bos.Player38BO>();
		// Initialize : playerToUpdate
		this.playerToUpdate = null;
					this.setAlwaysCallPreControllers(true);
	}
	
	/**
	 * 	Getter : teams 
	 * 	@return : Return the teams instance.
	 */
	public List getTeams(){
		return teams; // For UC38_DisplaySelectedListForm
	}
	
	/**
	 * 	Setter : teams 
	 *  @param teamsinstance : The instance to set.
	 */
	public void setTeams(final List teamsinstance){
		this.teams = teamsinstance;// For UC38_DisplaySelectedListForm
	}
	/**
	 * 	Getter : allPlayers 
	 * 	@return : Return the allPlayers instance.
	 */
	public List<Player38BO> getAllPlayers(){
		return allPlayers; // For UC38_DisplaySelectedListForm
	}
	
	/**
	 * 	Setter : allPlayers 
	 *  @param allPlayersinstance : The instance to set.
	 */
	public void setAllPlayers(final List<Player38BO> allPlayersinstance){
		this.allPlayers = allPlayersinstance;// For UC38_DisplaySelectedListForm
	}
	/**
	 * 	Getter : playerToUpdate 
	 * 	@return : Return the playerToUpdate instance.
	 */
	public Player38BO getPlayerToUpdate(){
		return playerToUpdate; // For UC38_DisplaySelectedListForm
	}
	
	/**
	 * 	Setter : playerToUpdate 
	 *  @param playerToUpdateinstance : The instance to set.
	 */
	public void setPlayerToUpdate(final Player38BO playerToUpdateinstance){
		this.playerToUpdate = playerToUpdateinstance;// For UC38_DisplaySelectedListForm
	}
/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UC38_DisplaySelectedListForm [ "+
TEAMS +" = " + teams +ALL_PLAYERS +" = " + allPlayers +PLAYER_TO_UPDATE +" = " + playerToUpdate + "]";
	}
	
	/**
	 * Method setInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public void setInstanceByInstanceName(final String instanceName, final Object instance) {
		// Set the instance Teams.
		if(TEAMS.equals(instanceName)){// For UC38_DisplaySelectedListForm
			this.setTeams((List)instance); // For UC38_DisplaySelectedListForm
		}
				// Set the instance AllPlayers.
		if(ALL_PLAYERS.equals(instanceName)){// For UC38_DisplaySelectedListForm
			this.setAllPlayers((List<Player38BO>)instance); // For UC38_DisplaySelectedListForm
		}
				// Set the instance PlayerToUpdate.
		if(PLAYER_TO_UPDATE.equals(instanceName)){// For UC38_DisplaySelectedListForm
			this.setPlayerToUpdate((Player38BO)instance); // For UC38_DisplaySelectedListForm
		}
			}
	
	/**
	 * Method getInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public Object getInstanceByInstanceName(final String instanceName){
		// Temporary object for tmpUC38_DisplaySelectedListForm.
		Object tmpUC38_DisplaySelectedListForm = null;
		
			
		// Get the instance Teams for UC38_DisplaySelectedListForm.
		if(TEAMS.equals(instanceName)){ // For UC38_DisplaySelectedListForm
			tmpUC38_DisplaySelectedListForm = this.getTeams(); // For UC38_DisplaySelectedListForm
		}
			
		// Get the instance AllPlayers for UC38_DisplaySelectedListForm.
		if(ALL_PLAYERS.equals(instanceName)){ // For UC38_DisplaySelectedListForm
			tmpUC38_DisplaySelectedListForm = this.getAllPlayers(); // For UC38_DisplaySelectedListForm
		}
			
		// Get the instance PlayerToUpdate for UC38_DisplaySelectedListForm.
		if(PLAYER_TO_UPDATE.equals(instanceName)){ // For UC38_DisplaySelectedListForm
			tmpUC38_DisplaySelectedListForm = this.getPlayerToUpdate(); // For UC38_DisplaySelectedListForm
		}
		return tmpUC38_DisplaySelectedListForm;// For UC38_DisplaySelectedListForm
	}
	
			}
