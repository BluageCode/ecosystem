--<ScriptOptions statementTerminator=";"/>

ALTER TABLE player74 DROP CONSTRAINT player74_position74_fkc;

ALTER TABLE player74 DROP CONSTRAINT player74_pkey;

ALTER TABLE position74 DROP CONSTRAINT position74_pkey;

DROP TABLE position74;

DROP TABLE player74;

CREATE TABLE position74 (
		code VARCHAR(1024) NOT NULL,
		version INT4 NOT NULL,
		name VARCHAR(1024)
	);

CREATE TABLE player74 (
		id INT8 NOT NULL,
		version INT4 NOT NULL,
		first_name VARCHAR(1024),
		last_name VARCHAR(1024),
		date_of_birth DATE,
		estimated_value INT8,
		weight INT4,
		height INT4,
		rookie BOOL,
		position74_fk VARCHAR(1024) NOT NULL
	);

INSERT INTO position74 (code, version, name) VALUES ('C', 0, 'Center');
INSERT INTO position74 (code, version, name) VALUES ('PF', 0, 'Power Forward');
INSERT INTO position74 (code, version, name) VALUES ('PG', 0, 'Point Guard');
INSERT INTO position74 (code, version, name) VALUES ('SF', 0, 'Small Forward');
INSERT INTO position74 (code, version, name) VALUES ('SG', 0, 'Shooting Guard');

INSERT INTO player74 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, position74_fk) VALUES (201, 0, 'Oliver', 'Frank', '1968-10-31', 0, 116, 228, false, 'SF');
INSERT INTO player74 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, position74_fk) VALUES (202, 0, 'Elbert', 'Cole', '1965-04-07', 4, 100, 171, true, 'PF');
INSERT INTO player74 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, position74_fk) VALUES (203, 0, 'Abel', 'Dennis', '1980-04-16', 31, 105, 208, false, 'SF');
INSERT INTO player74 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, position74_fk) VALUES (204, 0, 'Ricky', 'Dawson', '1990-03-03', 29, 115, 168, true, 'SG');
INSERT INTO player74 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, position74_fk) VALUES (205, 0, 'Earl', 'Russell', '1981-12-15', 45, 60, 234, false, 'PF');
INSERT INTO player74 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, position74_fk) VALUES (207, 0, 'Howard', 'Ramos', '1970-08-25', 11, 107, 199, false, 'SG');
INSERT INTO player74 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, position74_fk) VALUES (208, 0, 'Allan', 'Meyer', '1969-02-14', 34, 114, 229, false, 'PG');
INSERT INTO player74 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, position74_fk) VALUES (209, 0, 'Ruben', 'Pope', '1985-05-01', 45, 86, 201, true, 'SG');
INSERT INTO player74 (id, version, first_name, last_name, date_of_birth, estimated_value, weight, height, rookie, position74_fk) VALUES (210, 0, 'Lester', 'Cross', '1976-02-21', 31, 69, 214, true, 'SF');
	
	
ALTER TABLE player74 ADD CONSTRAINT player74_pkey PRIMARY KEY (id);

ALTER TABLE position74 ADD CONSTRAINT position74_pkey PRIMARY KEY (code);

ALTER TABLE player74 ADD CONSTRAINT player74_position74_fkc FOREIGN KEY (position74_fk)
	REFERENCES position74 (code);

