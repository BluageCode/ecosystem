package uc60;


import org.junit.Test;
import test.common.AbstractBADocTestCase;

public class ITuc60 extends AbstractBADocTestCase {
	
	@Override
	public  String getName(){
		return "UC-60";
	}	

	@Test
	public void testUC60() throws Exception {
		// On teste l'UC 60 - Standard Features/Entities - MultiDomain
	
		selenium.open("");	
		selenium.click("link=Expand All");
		selenium.waitForPageToLoad("30000");
		selenium.click("link=UC60 - MultiDomain");
		
		verifyTrue(selenium.isElementPresent("//h2[contains(text(),\"Standard Features > Entities > UC60 > MultiDomain\")]"));
		// On v�rifie les donn�es Team.
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_teams')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Charlotte\")] and td//span[contains(text(),\"Tigers\")] ]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Kansas City\")] and td//span[contains(text(),\"Bears\")] ]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Detroit\")] and td//span[contains(text(),\"Chargers\")] ]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Detroit\")] and td//span[contains(text(),\"Hawks\")] ]"));
		selenium.click("xpath=(//a)[5]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_teams')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Omaha\")] and td//span[contains(text(),\"Braves\")] ]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Raleigh\")] and td//span[contains(text(),\"Eagles\")] ]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Oakland\")] and td//span[contains(text(),\"Cardinals\")] ]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Kansas City\")] and td//span[contains(text(),\"Red Devils\")] ]"));
		selenium.click("xpath=(//a)[8]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_teams')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Fresno\")] and td//span[contains(text(),\"Mustangs\")] ]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Sacramento\")] and td//span[contains(text(),\"Wolverines\")] ]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Nashville\")] and td//span[contains(text(),\"Crusaders\")] ]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Fort Worth\")] and td//span[contains(text(),\"Trojans\")] ]"));
		selenium.click("xpath=(//a)[9]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("3", selenium.getXpathCount("//table[contains(@id,'tab_teams')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Tulsa\")] and td//span[contains(text(),\"Vikings\")] ]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Charlotte\")] and td//span[contains(text(),\"Blue Devils\")] ]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Cleveland\")] and td//span[contains(text(),\"Mustangs\")] ]"));
		// On v�rifie les donn�es Player.
		// On retourne sur la premi�re page.
		selenium.click("xpath=(//a)[4]");
		selenium.waitForPageToLoad("30000");
		// Pour les Tigers (Charlotte).
		// On indexe la ligne de Charlotte.
		Number indexTigers = selenium.getElementIndex("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Charlotte\")] and td/span[contains(text(),\"Tigers\")]]");
		System.out.println("variable index vaut : " + indexTigers);
		// On clique sur [view]
		selenium.click("//table[contains(@id,'tab_teams')]/tbody/tr[" + indexTigers + " + 1]/td/a[contains(text(),\"[view]\")]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie que l'on est sur la bonne page.
		assertEquals("Tigers", selenium.getText("//*[contains(@id,'txt_TeamName')]"));
		assertEquals("Charlotte", selenium.getText("//*[contains(@id,'txt_Teamcity')]"));
		// On v�rifie que les joueurs s'affichent correctement.
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody//tr[td//span[contains(text(),\"Oliver\")] and td//span[contains(text(),\"Frank\")] and td//span[contains(text(),\"10/31/1968\")] and td//span[contains(text(),\"0.11\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody//tr[td//span[contains(text(),\"Ivan\")] and td//span[contains(text(),\"Tate\")] and td//span[contains(text(),\"02/08/1972\")] and td//span[contains(text(),\"48.49\")]]"));
		// On teste le lien [back]
		selenium.click("//*[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
		// Pour les Bears - Kanas City
		// On indexe la ligne des Bears.
		Number indexBears = selenium.getElementIndex("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Kansas City\")] and td/span[contains(text(),\"Bears\")]]");
		System.out.println("variable index vaut : " + indexBears);
		// On clique sur [view]
		selenium.click("//table[contains(@id,'tab_teams')]/tbody/tr[" + indexBears + " + 1]/td/a[contains(text(),\"[view]\")]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie que l'on est sur la bonne page.
		assertEquals("Bears", selenium.getText("//*[contains(@id,'txt_TeamName')]"));
		assertEquals("Kansas City", selenium.getText("//*[contains(@id,'txt_Teamcity')]"));
		// On v�rifie que les joueurs s'affichent correctement.
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody//tr[td//span[contains(text(),\"Elbert\")] and td//span[contains(text(),\"Cole\")] and td//span[contains(text(),\"04/07/1965\")] and td//span[contains(text(),\"3.95\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody//tr[td//span[contains(text(),\"Keith\")] and td//span[contains(text(),\"Barnett\")] and td//span[contains(text(),\"11/10/1976\")] and td//span[contains(text(),\"5.81\")]]"));
		// On teste le lien [back]
		assertEquals("[back]", selenium.getText("//*[contains(@id,'lnk_back')]"));
		selenium.click("//*[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
		// Pour les Chargers - Detroit
		// On indexe la ligne des Chargers.
		Number indexChargers = selenium.getElementIndex("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Detroit\")] and td/span[contains(text(),\"Chargers\")]]");
		System.out.println("variable index vaut : " + indexChargers);
		// On clique sur [view]
		selenium.click("//table[contains(@id,'tab_teams')]/tbody/tr[" + indexChargers + " + 1]/td/a[contains(text(),\"[view]\")]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie que l'on est sur la bonne page.
		assertEquals("Chargers", selenium.getText("//*[contains(@id,'txt_TeamName')]"));
		assertEquals("Detroit", selenium.getText("//*[contains(@id,'txt_Teamcity')]"));
		// On v�rifie que les joueurs s'affichent correctement.
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody//tr[td//span[contains(text(),\"Abel\")] and td//span[contains(text(),\"Dennis\")] and td//span[contains(text(),\"04/16/1980\")] and td//span[contains(text(),\"30.51\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody//tr[td//span[contains(text(),\"Tommy\")] and td//span[contains(text(),\"Wood\")] and td//span[contains(text(),\"01/20/1992\")] and td//span[contains(text(),\"13.93\")]]"));
		// On teste le lien [back]
		selenium.click("//*[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
		// Pour les Hawks - Detroit
		// On indexe la ligne des Hawks.
		Number indexHawks = selenium.getElementIndex("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Detroit\")] and td/span[contains(text(),\"Hawks\")]]");
		System.out.println("variable index vaut : " + indexHawks);
		// On clique sur [view]
		selenium.click("//table[contains(@id,'tab_teams')]/tbody/tr[" + indexHawks + " + 1]/td/a[contains(text(),\"[view]\")]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie que l'on est sur la bonne page.
		assertEquals("Hawks", selenium.getText("//*[contains(@id,'txt_TeamName')]"));
		assertEquals("Detroit", selenium.getText("//*[contains(@id,'txt_Teamcity')]"));
		// On v�rifie que les joueurs s'affichent correctement.
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody//tr[td//span[contains(text(),\"Ricky\")] and td//span[contains(text(),\"Dawson\")] and td//span[contains(text(),\"03/03/1990\")] and td//span[contains(text(),\"29.3\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody//tr[td//span[contains(text(),\"Sylvester\")] and td//span[contains(text(),\"Nash\")] and td//span[contains(text(),\"07/10/1965\")] and td//span[contains(text(),\"37.73\")]]"));
		// On teste le lien [back]
		selenium.click("//*[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
		// On va sur la page 2.
		selenium.click("xpath=(//a)[8]");
		selenium.waitForPageToLoad("30000");
		// Pour les Braves - Omaha
		// On indexe la ligne des Braves.
		Number indexBraves = selenium.getElementIndex("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Omaha\")] and td/span[contains(text(),\"Braves\")]]");
		System.out.println("variable index vaut : " + indexBraves);
		// On clique sur [view]
		selenium.click("//table[contains(@id,'tab_teams')]/tbody/tr[" + indexBraves + " + 1]/td/a[contains(text(),\"[view]\")]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie que l'on est sur la bonne page.
		assertEquals("Braves", selenium.getText("//*[contains(@id,'txt_TeamName')]"));
		assertEquals("Omaha", selenium.getText("//*[contains(@id,'txt_Teamcity')]"));
		// On v�rifie que les joueurs s'affichent correctement.
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody//tr[td//span[contains(text(),\"Earl\")] and td//span[contains(text(),\"Russell\")] and td//span[contains(text(),\"12/15/1981\")] and td//span[contains(text(),\"44.73\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody//tr[td//span[contains(text(),\"Michael\")] and td//span[contains(text(),\"Hodges\")] and td//span[contains(text(),\"04/01/1988\")] and td//span[contains(text(),\"25.35\")]]"));
		// On teste le lien [back]
		selenium.click("//*[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
		// On va sur la page 2.
		selenium.click("xpath=(//a)[8]");
		selenium.waitForPageToLoad("30000");
		// Pour les Eagles - Raleigh
		// On indexe la ligne des Eagles.
		Number indexEagles = selenium.getElementIndex("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Raleigh\")] and td/span[contains(text(),\"Eagles\")]]");
		System.out.println("variable index vaut : " + indexEagles);
		// On clique sur [view]
		selenium.click("//table[contains(@id,'tab_teams')]/tbody/tr[" + indexEagles + " + 1]/td/a[contains(text(),\"[view]\")]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie que l'on est sur la bonne page.
		assertEquals("Eagles", selenium.getText("//*[contains(@id,'txt_TeamName')]"));
		assertEquals("Raleigh", selenium.getText("//*[contains(@id,'txt_Teamcity')]"));
		// On v�rifie que les joueurs s'affichent correctement.
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody//tr[td//span[contains(text(),\"Nick\")] and td//span[contains(text(),\"Garner\")] and td//span[contains(text(),\"07/27/1992\")] and td//span[contains(text(),\"47.84\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody//tr[td//span[contains(text(),\"Jerome\")] and td//span[contains(text(),\"Grant\")] and td//span[contains(text(),\"06/28/1982\")] and td//span[contains(text(),\"28.94\")]]"));
		// On teste le lien [back]
		selenium.click("//*[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
		// On va sur la page 2.
		selenium.click("xpath=(//a)[8]");
		selenium.waitForPageToLoad("30000");
		// Pour les Cardinals - Oakland
		// On indexe la ligne des Cardinals.
		Number indexCardinals = selenium.getElementIndex("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Oakland\")] and td/span[contains(text(),\"Cardinals\")]]");
		System.out.println("variable index vaut : " + indexCardinals);
		// On clique sur [view]
		selenium.click("//table[contains(@id,'tab_teams')]/tbody/tr[" + indexCardinals + " + 1]/td/a[contains(text(),\"[view]\")]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie que l'on est sur la bonne page.
		assertEquals("Cardinals", selenium.getText("//*[contains(@id,'txt_TeamName')]"));
		assertEquals("Oakland", selenium.getText("//*[contains(@id,'txt_Teamcity')]"));
		// On v�rifie que les joueurs s'affichent correctement.
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody//tr[td//span[contains(text(),\"Howard\")] and td//span[contains(text(),\"Ramos\")] and td//span[contains(text(),\"08/25/1970\")] and td//span[contains(text(),\"10.76\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody//tr[td//span[contains(text(),\"Woodrow\")] and td//span[contains(text(),\"Carpenter\")] and td//span[contains(text(),\"08/08/1987\")] and td//span[contains(text(),\"41.76\")]]"));
		// On teste le lien [back]
		selenium.click("//*[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
		// On va sur la page 2.
		selenium.click("xpath=(//a)[8]");
		selenium.waitForPageToLoad("30000");
		// Pour les Red Devils - Kansas City
		// On indexe la ligne des Hawks.
		Number indexRedDevils = selenium.getElementIndex("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Kansas City\")] and td/span[contains(text(),\"Red Devils\")]]");
		System.out.println("variable index vaut : " + indexRedDevils);
		// On clique sur [view]
		selenium.click("//table[contains(@id,'tab_teams')]/tbody/tr[" + indexRedDevils + " + 1]/td/a[contains(text(),\"[view]\")]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie que l'on est sur la bonne page.
		assertEquals("Red Devils", selenium.getText("//*[contains(@id,'txt_TeamName')]"));
		assertEquals("Kansas City", selenium.getText("//*[contains(@id,'txt_Teamcity')]"));
		// On v�rifie que les joueurs s'affichent correctement.
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody//tr[td//span[contains(text(),\"Allan\")] and td//span[contains(text(),\"Meyer\")] and td//span[contains(text(),\"02/14/1969\")] and td//span[contains(text(),\"33.67\")]]"));
		// On teste le lien [back]
		selenium.click("//*[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
		// On va sur la page 3.
		selenium.click("//a[contains(text(),\"3\")]");
		selenium.waitForPageToLoad("30000");
		// Pour les Mustangs - Fresno
		// On indexe la ligne des Mustangs.
		Number indexMustangs = selenium.getElementIndex("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Fresno\")] and td/span[contains(text(),\"Mustangs\")]]");
		System.out.println("variable index vaut : " + indexMustangs);
		// On clique sur [view]
		selenium.click("//table[contains(@id,'tab_teams')]/tbody/tr[" + indexMustangs + " + 1]/td/a[contains(text(),\"[view]\")]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie que l'on est sur la bonne page.
		assertEquals("Mustangs", selenium.getText("//*[contains(@id,'txt_TeamName')]"));
		assertEquals("Fresno", selenium.getText("//*[contains(@id,'txt_Teamcity')]"));
		// On v�rifie que les joueurs s'affichent correctement.
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody//tr[td//span[contains(text(),\"Ruben\")] and td//span[contains(text(),\"Pope\")] and td//span[contains(text(),\"05/01/1985\")] and td//span[contains(text(),\"44.66\")]]"));
		// On teste le lien [back]
		selenium.click("//*[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
		// On va sur la page 3.
		selenium.click("//a[contains(text(),\"3\")]");
		selenium.waitForPageToLoad("30000");
		// Pour les Wolverines - Sacramento
		// On indexe la ligne des Wolverines.
		Number indexWolverines = selenium.getElementIndex("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Sacramento\")] and td/span[contains(text(),\"Wolverines\")]]");
		System.out.println("variable index vaut : " + indexWolverines);
		// On clique sur [view]
		selenium.click("//table[contains(@id,'tab_teams')]/tbody/tr[" + indexWolverines + " + 1]/td/a[contains(text(),\"[view]\")]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie que l'on est sur la bonne page.
		assertEquals("Wolverines", selenium.getText("//*[contains(@id,'txt_TeamName')]"));
		assertEquals("Sacramento", selenium.getText("//*[contains(@id,'txt_Teamcity')]"));
		// On v�rifie que les joueurs s'affichent correctement.
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody//tr[td//span[contains(text(),\"Lester\")] and td//span[contains(text(),\"Cross\")] and td//span[contains(text(),\"02/21/1976\")] and td//span[contains(text(),\"30.98\")]]"));
		// On teste le lien [back]
		selenium.click("//*[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
		// On va sur la page 3.
		selenium.click("//a[contains(text(),\"3\")]");
		selenium.waitForPageToLoad("30000");
		// Pour les Crusaders - Nashville
		// On indexe la ligne des Crusaders.
		Number indexCrusaders = selenium.getElementIndex("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Nashville\")] and td/span[contains(text(),\"Crusaders\")]]");
		System.out.println("variable index vaut : " + indexCrusaders);
		// On clique sur [view]
		selenium.click("//table[contains(@id,'tab_teams')]/tbody/tr[" + indexCrusaders + " + 1]/td/a[contains(text(),\"[view]\")]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie que l'on est sur la bonne page.
		assertEquals("Crusaders", selenium.getText("//*[contains(@id,'txt_TeamName')]"));
		assertEquals("Nashville", selenium.getText("//*[contains(@id,'txt_Teamcity')]"));
		// On v�rifie que les joueurs s'affichent correctement.
		String url = selenium.getLocation();
		String count = String.valueOf(url.indexOf("Struts") != -1 ? 1 : 0);
		verifyEquals(count, selenium.getXpathCount("//table[contains(@id,'tab_players')]/tbody//tr"));
		// On teste le lien [back]
		selenium.click("//*[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
		// On va sur la page 3.
		selenium.click("//a[contains(text(),\"3\")]");
		selenium.waitForPageToLoad("30000");
		// Pour les Trojans - Fort Worth
		// On indexe la ligne des Trojans.
		Number indexTrojans = selenium.getElementIndex("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Fort Worth\")] and td/span[contains(text(),\"Trojans\")]]");
		System.out.println("variable index vaut : " + indexTrojans);
		// On clique sur [view]
		selenium.click("//table[contains(@id,'tab_teams')]/tbody/tr[" + indexTrojans + " + 1]/td/a[contains(text(),\"[view]\")]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie que l'on est sur la bonne page.
		assertEquals("Trojans", selenium.getText("//*[contains(@id,'txt_TeamName')]"));
		assertEquals("Fort Worth", selenium.getText("//*[contains(@id,'txt_Teamcity')]"));
		// On v�rifie que les joueurs s'affichent correctement.
		url = selenium.getLocation();
		count = String.valueOf(url.indexOf("Struts") != -1 ? 1 : 0);
		verifyEquals(count, selenium.getXpathCount("//table[contains(@id,'tab_player')]//tbody//tr"));
		// On teste le lien [back]
		selenium.click("//*[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
		// On va sur la page 4.
		selenium.click("xpath=(//a)[9]");
		selenium.waitForPageToLoad("30000");
		// Pour les Vikings - Tulsa
		// On indexe la ligne des Vikings.
		Number indexVikings = selenium.getElementIndex("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Tulsa\")] and td/span[contains(text(),\"Vikings\")]]");
		System.out.println("variable index vaut : " + indexVikings);
		// On clique sur [view]
		selenium.click("//table[contains(@id,'tab_teams')]/tbody/tr[" + indexVikings + " + 1]/td/a[contains(text(),\"[view]\")]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie que l'on est sur la bonne page.
		assertEquals("Vikings", selenium.getText("//*[contains(@id,'txt_TeamName')]"));
		assertEquals("Tulsa", selenium.getText("//*[contains(@id,'txt_Teamcity')]"));
		// On v�rifie que les joueurs s'affichent correctement.
		url = selenium.getLocation();
		count = String.valueOf(url.indexOf("Struts") != -1 ? 1 : 0);
		verifyEquals(count, selenium.getXpathCount("//table[contains(@id,'tab_player')]//tbody//tr"));
		// On teste le lien [back]
		selenium.click("//*[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
		// On va sur la page 3.
		selenium.click("//a[contains(text(),\"3\")]");
		selenium.waitForPageToLoad("30000");
		// Pour les Trojans - Fort Worth
		// On indexe la ligne des Trojans.
		indexTrojans = selenium.getElementIndex("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Fort Worth\")] and td/span[contains(text(),\"Trojans\")]]");
		System.out.println("variable index vaut : " + indexTrojans);
		// On clique sur [view]
		selenium.click("//table[contains(@id,'tab_teams')]/tbody/tr[" + indexTrojans + " + 1]/td/a[contains(text(),\"[view]\")]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie que l'on est sur la bonne page.
		assertEquals("Trojans", selenium.getText("//*[contains(@id,'txt_TeamName')]"));
		assertEquals("Fort Worth", selenium.getText("//*[contains(@id,'txt_Teamcity')]"));
		// On v�rifie que les joueurs s'affichent correctement.
		url = selenium.getLocation();
		count = String.valueOf(url.indexOf("Struts") != -1 ? 1 : 0);
		verifyEquals(count, selenium.getXpathCount("//table[contains(@id,'tab_player')]//tbody//tr"));
		// On teste le lien [back]
		selenium.click("//*[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
		// On va sur la page 4.
		selenium.click("xpath=(//a)[9]");
		selenium.waitForPageToLoad("30000");
		// Pour les Blue Devils - Charlotte
		// On indexe la ligne des Blue Devils.
		Number indexBlueDevils = selenium.getElementIndex("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Charlotte\")] and td/span[contains(text(),\"Blue Devils\")]]");
		System.out.println("variable index vaut : " + indexBlueDevils);
		// On clique sur [view]
		selenium.click("//table[contains(@id,'tab_teams')]/tbody/tr[" + indexBlueDevils + " + 1]/td/a[contains(text(),\"[view]\")]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie que l'on est sur la bonne page.
		assertEquals("Blue Devils", selenium.getText("//*[contains(@id,'txt_TeamName')]"));
		assertEquals("Charlotte", selenium.getText("//*[contains(@id,'txt_Teamcity')]"));
		// On v�rifie que les joueurs s'affichent correctement.
		url = selenium.getLocation();
		count = String.valueOf(url.indexOf("Struts") != -1 ? 1 : 0);
		verifyEquals(count, selenium.getXpathCount("//table[contains(@id,'tab_player')]//tbody//tr"));
		// On teste le lien [back]
		selenium.click("//*[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
		// On va sur la page 4.
		selenium.click("xpath=(//a)[9]");
		selenium.waitForPageToLoad("30000");
		// Pour les Mustangs - Cleveland
		// On indexe la ligne des Mustangs.
		Number indexMustangsC = selenium.getElementIndex("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Cleveland\")] and td/span[contains(text(),\"Mustangs\")]]");
		System.out.println("variable index vaut : " + indexTrojans);
		// On clique sur [view]
		selenium.click("//table[contains(@id,'tab_teams')]/tbody/tr[" + indexMustangsC + " + 1]/td/a[contains(text(),\"[view]\")]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie que l'on est sur la bonne page.
		assertEquals("Mustangs", selenium.getText("//*[contains(@id,'txt_TeamName')]"));
		assertEquals("Cleveland", selenium.getText("//*[contains(@id,'txt_Teamcity')]"));
		// On v�rifie que les joueurs s'affichent correctement.
		url = selenium.getLocation();
		count = String.valueOf(url.indexOf("Struts") != -1 ? 1 : 0);
		verifyEquals(count, selenium.getXpathCount("//table[contains(@id,'tab_player')]//tbody//tr"));
		// On teste le lien [back]
		selenium.click("//*[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
		// On va sur la page 4.
		selenium.click("xpath=(//a)[9]");
		selenium.waitForPageToLoad("30000");
	}

	
}
