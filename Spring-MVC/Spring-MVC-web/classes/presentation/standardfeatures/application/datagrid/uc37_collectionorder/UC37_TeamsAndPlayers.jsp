<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file="/include.jsp"  %>
<html >
<head >
<title  > 
<fmt:message key="cUC37_TeamsAndPlayers_BLU_AGE_DOCUMENTATION"/>
</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"  > 
</meta>
<link href="<c:url value= "/presentation/_style/_style.css"/>" rel="stylesheet" type="text/css"  /> 
<link href="<c:url value= "/presentation/_include/jquery/rd_lib.css"/>" rel="stylesheet" type="text/css"  /> 
<script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/jquery.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/rd_setup.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/rd_lib.js"/>" space="preserve"  ></script><link rel="shortcut icon" href="<c:url value= "/presentation/_style/favicon.ico"/>"  /> 

</head>

<body class=" " > 
<div id="rd_ghost"  > 
<small  > 
<fmt:message key="cUC37_TeamsAndPlayers___CSS_DEACTIVATED"/>
</small>
</div>
<div id="rd_jghost"  > 
<small  > 
<fmt:message key="cUC37_TeamsAndPlayers___JAVASCRIPT_DEACTIVATED"/>
</small>
</div>
<div id="shape"  > 
<div class="ui-layout-center"  > 
<div class="east-north"  > 
	<form:form id="uC37_TeamsAndPlayers" commandName="uC37_TeamsAndPlayersForm"  enctype="application/x-www-form-urlencoded" method="post">
<h2 class="rd_first"  > 
<fmt:message key="cUC37_TeamsAndPlayers_STANDARD_FEATURES__APPLICATIO"/>
</h2>
<b  > 
<fmt:message key="cUC37_TeamsAndPlayers___TEAM___"/>
</b>
		<span id="txt_TeamName">
	<c:out value="${uC37_TeamsAndPlayersForm.team.name}" />
		</span>
	<b  > 
<fmt:message key="cUC37_TeamsAndPlayers____CITY_"/>
</b>
		<span id="txt_Teamcity">
	<c:out value="${uC37_TeamsAndPlayersForm.team.city}" />
		</span>
	<fmt:message key="cUC37_TeamsAndPlayers_____"/>
<a id="lnk_back"				href="<c:url value="/presentation/standardfeatures/application/datagrid/uc37_collectionorder/UC37_TeamsAndPlayers/lnk_back.html"/>"
													 shape="rect"  >									 
<fmt:message key="cUC37_TeamsAndPlayers_BACK"/>
</a>
<h4  > 
<fmt:message key="cUC37_TeamsAndPlayers_PLAYERS"/>
</h4>
<display:table uid="tab_players" pagesize="10"  sort="list" requestURI="" name="sessionScope.uC37_TeamsAndPlayersForm.players" class="rd_tabledata"  >
	<c:set   var="rownum">
		<%=(tab_players !=	null) ? new Integer(tab_players_rowNum.intValue() - 1) : new Integer(0) %>
	</c:set>
	<c:set   var="idRow">
		<%=(tab_players != null ) ? tab_players_rowNum.intValue() - 1 : -1 %>
	</c:set>
	<c:set   var="idTab">
		<%=(tab_players != null ) ? "tab_players" : null %>
	</c:set>
	<c:set var="tableHashId">
		<%= new org.displaytag.util.ParamEncoder("tab_players").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE) %>
	</c:set>
	<c:set var="tableHashId2">
		<%=request.getParameter(new org.displaytag.util.ParamEncoder("tab_players").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE)) %>
	</c:set>	
	
<display:column format=""  titleKey="cUC37_TeamsAndPlayers_NAME">
		<span id="txt_firstName">
	<c:out value="${tab_players.firstName}" />
		</span>
	<fmt:message key="cUC37_TeamsAndPlayers_______"/>
		<span id="txt_lastName">
	<c:out value="${tab_players.lastName}" />
		</span>
	</display:column>
</display:table>
	</form:form>
	<script language="javascript">
		function btn_click_cExecuteUC37_TeamsAndPlayers(url) {
			UC37_TeamsAndPlayersvar = document.getElementById('uC37_TeamsAndPlayers');
			UC37_TeamsAndPlayersvar.setAttribute('action', url);
			UC37_TeamsAndPlayersvar.submit();
		}
		function btn_click_cSetActionUC37_TeamsAndPlayers(url) {
			UC37_TeamsAndPlayersvar = document.getElementById('uC37_TeamsAndPlayers');
			UC37_TeamsAndPlayersvar.setAttribute('action', url);
		}
	</script>
</div>
<div class="east-center"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/application/datagrid/uc37_collectionorder/UC37_TeamsAndPlayerscmp_tabs" />
</div>
</div>
<div class="ui-layout-north"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/application/datagrid/uc37_collectionorder/UC37_TeamsAndPlayerscmp_header" />
</div>
<div class="ui-layout-south"  > 
<fmt:message key="cUC37_TeamsAndPlayers___"/>
<span id="rd_jversion"  > 
<fmt:message key="cUC37_TeamsAndPlayers_BLU_AGE_CORPORATION"/>
</span>
</div>
<div class="ui-layout-west"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/application/datagrid/uc37_collectionorder/UC37_TeamsAndPlayerscmp_treeview" />
</div>
</div>
</body>
</html>
