/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.qauc.uc1001_tables;


import java.io.Serializable;
import java.util.List;

import com.bluage.documentation.business.qauc.uc1001_tables.bos.Player1001BO;
import com.bluage.documentation.business.qauc.uc1001_tables.bos.Team1001BO;
import com.netfective.bluage.core.web.AbstractForm;
import com.netfective.bluage.core.web.IForm;

/**
* Class : UC1001_ViewTeamForm
*/
public class UC1001_ViewTeamForm extends AbstractForm implements Serializable, IForm {
	
	/** Constant : serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	//CONSTANT : teamDetails
	private static final String TEAM_DETAILS = "teamDetails";
	//CONSTANT : listplayers
	private static final String LISTPLAYERS = "listplayers";
	/**
	 * 	Property: teamDetails 
	 */
	private Team1001BO teamDetails;
	/**
	 * 	Property: listplayers 
	 */
	private List<Player1001BO> listplayers;
/**
	 * Default constructor : UC1001_ViewTeamForm
	 */
	public UC1001_ViewTeamForm() {
		super();
		// Initialize : teamDetails
		this.teamDetails = new Team1001BO();
		// Initialize : listplayers
		this.listplayers = new java.util.ArrayList<com.bluage.documentation.business.qauc.uc1001_tables.bos.Player1001BO>();
					this.setAlwaysCallPreControllers(true);
	}
	
	/**
	 * 	Getter : teamDetails 
	 * 	@return : Return the teamDetails instance.
	 */
	public Team1001BO getTeamDetails(){
		return teamDetails; // For UC1001_ViewTeamForm
	}
	
	/**
	 * 	Setter : teamDetails 
	 *  @param teamDetailsinstance : The instance to set.
	 */
	public void setTeamDetails(final Team1001BO teamDetailsinstance){
		this.teamDetails = teamDetailsinstance;// For UC1001_ViewTeamForm
	}
	/**
	 * 	Getter : listplayers 
	 * 	@return : Return the listplayers instance.
	 */
	public List<Player1001BO> getListplayers(){
		return listplayers; // For UC1001_ViewTeamForm
	}
	
	/**
	 * 	Setter : listplayers 
	 *  @param listplayersinstance : The instance to set.
	 */
	public void setListplayers(final List<Player1001BO> listplayersinstance){
		this.listplayers = listplayersinstance;// For UC1001_ViewTeamForm
	}
/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UC1001_ViewTeamForm [ "+
TEAM_DETAILS +" = " + teamDetails +LISTPLAYERS +" = " + listplayers + "]";
	}
	
	/**
	 * Method setInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public void setInstanceByInstanceName(final String instanceName, final Object instance) {
		// Set the instance TeamDetails.
		if(TEAM_DETAILS.equals(instanceName)){// For UC1001_ViewTeamForm
			this.setTeamDetails((Team1001BO)instance); // For UC1001_ViewTeamForm
		}
				// Set the instance Listplayers.
		if(LISTPLAYERS.equals(instanceName)){// For UC1001_ViewTeamForm
			this.setListplayers((List<Player1001BO>)instance); // For UC1001_ViewTeamForm
		}
				// Parent instance name for UC1001_ViewTeamForm
		if(TEAM_DETAILS.equals(instanceName) && teamDetails != null){ // For UC1001_ViewTeamForm
			listplayers.clear(); // For UC1001_ViewTeamForm
			listplayers.addAll(teamDetails.getPlayers());// For UC1001_ViewTeamForm
		}
		
	}
	
	/**
	 * Method getInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public Object getInstanceByInstanceName(final String instanceName){
		// Temporary object for tmpUC1001_ViewTeamForm.
		Object tmpUC1001_ViewTeamForm = null;
		
			
		// Get the instance TeamDetails for UC1001_ViewTeamForm.
		if(TEAM_DETAILS.equals(instanceName)){ // For UC1001_ViewTeamForm
			tmpUC1001_ViewTeamForm = this.getTeamDetails(); // For UC1001_ViewTeamForm
		}
			
		// Get the instance Listplayers for UC1001_ViewTeamForm.
		if(LISTPLAYERS.equals(instanceName)){ // For UC1001_ViewTeamForm
			tmpUC1001_ViewTeamForm = this.getListplayers(); // For UC1001_ViewTeamForm
		}
		return tmpUC1001_ViewTeamForm;// For UC1001_ViewTeamForm
	}
	
			}
