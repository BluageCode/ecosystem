/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.business.standardfeatures.services.sql.uc56_sqloperation.bos;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.inject.Inject;
import javax.inject.Named;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.LazyInitializationException;

import com.bluage.documentation.business.common.bos.TeamBO;
import com.bluage.documentation.business.standardfeatures.services.sql.uc56_sqloperation.entities.daofinder.Team56DAOFinderImpl;
import com.netfective.bluage.core.exception.ApplicationException;

/**
 * Team56BO
 *
* @see com.netfective.bluage.core.business.IBusinessObject
* * @see TeamBO
 */
@XmlRootElement

public class Team56BO
 extends TeamBO {
	
	/**
	 * Default SUID for Team56BO.
	 */
	private static final long serialVersionUID = 1L;
	

	@Inject
	@Named ("com.bluage.documentation.business.standardfeatures.services.sql.uc56_sqloperation.entities.daofinder.Team56Finder") 
	private Team56DAOFinderImpl finder;

						/**
	 * Entity property: budget
	 *
	 */
private Integer budget;

											/**
	 * Entity property: state
	 *
	 */
private State56BO state;

																		/**
	 * Entity property: players
	 *
	 */
private Set<Player56BO> players = new HashSet<Player56BO>();



   /**
	* public default constructor for class Team56BO
	*/
	public Team56BO(){
		//NOP Team56BO
	}


						/**
	 * Getter for budget of class Team56BO.
	 *
	 * @return {@link java.lang.Integer}
	 * @generated
	 */ 
public  Integer getBudget() {
	return this.budget;
	}


	/**
	 * Setter for budget of class Team56BO.
	 * @param value {@link java.lang.Integer}
	 * @generated
	 */

public  void setBudget(final Integer budget) {
	this.budget = budget;
}

											/**
	 * Getter for state of class Team56BO.
	 *
	 * @return {@link com.bluage.documentation.business.standardfeatures.services.sql.uc56_sqloperation.bos.State56BO}
	 * @generated
	 */ 
public  State56BO getState() {
		State56BO state0 = this.state;
		if(this.state == null) {
			if(DAO_MODE.equals(this.getObjMode( ) ) ) {
				state0 = null;
			} else {
				this.state = new State56BO();
				this.state.setObjMode( this.getObjMode( ) );
				state0 = this.state;
			}
		} else {
			this.state.setObjMode( this.getObjMode( ) );
			if( HELPER_MODE.equals(this.getObjMode( ) ) ) {
				state0 = this.state;
			}
			if( DAO_MODE.equals(this.getObjMode()) && this.state.isEmpty ) {
				state0 = null;
			}
		}
		return state0;
	}


	/**
	 * Setter for state of class Team56BO.
	 * @param value {@link com.bluage.documentation.business.standardfeatures.services.sql.uc56_sqloperation.bos.State56BO}
	 * @generated
	 */

public  void setState(final State56BO state) {
	if(state != null) {
		state.isEmpty = false;
		state.setObjMode( this.getObjMode( ) );
	}
	this.isEmpty = false;
	this.state = state;
}

																		/**
	 * Getter for players of class Team56BO.
	 *
	 * @return {@link java.util.Set<com.bluage.documentation.business.standardfeatures.services.sql.uc56_sqloperation.bos.Player56BO>}
	 * @generated
	 */ 
public  Set<Player56BO> getPlayers() {
	return this.players;
			}


	/**
	 * Setter for players of class Team56BO.
	 * @param value {@link java.util.Set<com.bluage.documentation.business.standardfeatures.services.sql.uc56_sqloperation.bos.Player56BO>}
	 * @generated
	 */

public  void setPlayers(final Set<Player56BO> players) {
	this.players = players;
}

	/**
	 * Remove an entity from the com.bluage.documentation.business.standardfeatures.services.sql.uc56_sqloperation.bos.Player56BO collection.
	 *
	 * @param entity
	 *		: {@link Set<Player56BO>} the entity
	 * @throws ApplicationException
     */
public void removeFromPlayers( Player56BO entity ) throws ApplicationException {
	try {
		if( getPlayers() != null && getPlayers().contains(entity) ) {
			getPlayers().remove( entity );
		}
	} catch ( LazyInitializationException e ) {
				Team56BO bo = ( Team56BO ) finder.findByPrimaryKey( getId() );
			finder.getHibernateTemplate().initialize( bo.getPlayers() );
			setPlayers( bo.getPlayers() );
			// evict object from session to avoid NonUniqueObjectException
			finder.getHibernateTemplate().evict( bo );
		
			if( getPlayers() != null && getPlayers().contains( entity ) ) {
				getPlayers().remove( entity );
			}
		}
	}
	
	/**
	 * Add entity to the com.bluage.documentation.business.standardfeatures.services.sql.uc56_sqloperation.bos.Player56BO collection.
	 *
	 * @param entity
	 *		{@link Player56BO} the entity
	 * @throws ApplicationException
     */
public void addToPlayers( Player56BO entity ) throws ApplicationException
	{
		try {
			if( getPlayers() != null && !getPlayers().contains( entity ) ) {
				getPlayers().add( entity );
			}
		} catch ( LazyInitializationException e ) {
							Team56BO bo = ( Team56BO ) finder.findByPrimaryKey( getId() );
		finder.getHibernateTemplate().initialize( bo.getPlayers() );
		setPlayers( bo.getPlayers() );
		// evict object from session to avoid NonUniqueObjectException
		finder.getHibernateTemplate().evict( bo );
		if( getPlayers() != null && !getPlayers().contains( entity ) ) {
			getPlayers().add( entity );
		}
	}
	}
	
	/**
	 * Add entitys to the com.bluage.documentation.business.standardfeatures.services.sql.uc56_sqloperation.bos.Player56BO collection
	 *
	 * @param entities			
	 *		: {@link java.util.List} A list of entities
	 * @throws ApplicationException 
     */	
public void addAllToPlayers( Collection<Player56BO> entities ) throws ApplicationException {
	for( Player56BO entity : entities) {
		if( getPlayers()!=null ) {
			addToPlayers( entity );
		}
	}
}
	
	/**
	 * Remove entities form the com.bluage.documentation.business.standardfeatures.services.sql.uc56_sqloperation.bos.Player56BO collection
	 *
	 * @param entities				
	 *		: {@link java.util.List} a list of entities
	 * @throws ApplicationException
     */	
public void removeAllFromPlayers( Collection<Player56BO> entities ) throws ApplicationException {
	for( Player56BO entity : entities) {
		if( getPlayers()!=null && getPlayers().contains( entity ) )	{		
			removeFromPlayers( entity );
		}
	}
}
	
	/**
	 * Remove all items form the collection com.bluage.documentation.business.standardfeatures.services.sql.uc56_sqloperation.bos.Player56BO
	 *
	 * @throws ApplicationException
     */	
public void removeAllFromPlayers() throws ApplicationException {
	Set<Player56BO> entities = this.getPlayers();
	if ( entities == null ) {
		return;
	}
	entities.clear();
}

/**
	 * Returns <code>true</code> if the argument is an Team56 instance and all identifiers for this entity
     * equal the identifiers of the argument entity. The <code>equals</code> method of the parent entity
     * will also need to return <code>true</code>. Returns <code>false</code> otherwise.
     *
     * @see TeamBO#hashCode()
	 * @param object  object to compare with current instance      
     * @return boolean true if equals false if not
     */
    public boolean equals(final Object object )
    {
		if ( this == object )
        {
            return true;
        }
        if ( !( object instanceof Team56BO ) )
        {
            return false;
        }
        final Team56BO that = (Team56BO)object;
    
    	            if (        				        			this.getId() == null
        	        ) {
        	return false;
        }
		return new EqualsBuilder()
			.append(this.getId(), that.getId())
			 	.isEquals();
	}
	/**

	 * Returns a hash code based on this entity's identifiers and the hash code of the parent entity.
     *
     * @see TeamBO#hashCode()
     * @return int
     */    
    public int hashCode() {
        return new HashCodeBuilder()
             .append(getId())
				 	.appendSuper(super.hashCode())
            .toHashCode();
    }
}

