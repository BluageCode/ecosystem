/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.netfective.bluage.springmvc.ajax.tag;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.servlet.ServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;

import org.apache.log4j.Logger;

import com.netfective.bluage.springmvc.ajax.tag.html.HtmlDivTag;
import com.netfective.bluage.springmvc.ajax.util.AjaxUtils;
import com.netfective.bluage.springmvc.ajax.util.Constants;
/**
 * Class AjaxZoneTag
 * @superclass HtmlDivTag
 * @interface Constants
 * @author NETFECTIVE TECHNOLOGY
 */
public class AjaxZoneTag extends HtmlDivTag implements Constants {
    /**
     * LOGGER.
     */
    private static final  Logger LOGGER = Logger.getLogger(AjaxZoneTag.class);
    
	/**
	 * 
	 */
	private static final long serialVersionUID = -394872675247281718L;

	/**
	* DEFAULT_SKIP_INF_NOT_INCLUDED is contant boolean
	*/
	private static final boolean DEFAULT_SKIP_INF_NOT_INCLUDED = false;

	/**
	* DEFAULT_AJAX_RENDERED is contant boolean
	*/
	private static final boolean DEFAULT_AJAX_RENDERED = false;

	/**
	* If true tag body will be skip
	*/
	private boolean skipIfNotIncluded = DEFAULT_SKIP_INF_NOT_INCLUDED;

	/**
	* Enable ajax rendered
	*/
	private boolean ajaxRendered = DEFAULT_AJAX_RENDERED;

	private String reRendered = "";
	
	/**
	* This method prints a start tag on output of <code>pageContext</code>
	* @return int
	* @throw JspException
	*/
	public int doStartTag() throws JspException {

		// We are searching for the elements that need to be reRendered with this action.
		// For safety reasons, we have to delete all whitespaces.
		final String reRenderedString = AjaxUtils.deleteWhitespace((String)pageContext.getRequest().getParameter(ZONES_URL_KEY));
		
		final List elementIdToBeRerendered = new ArrayList();
		
		// Is the reRendered 
		if(AjaxUtils.isNotBlank(reRenderedString)) {
			// For all the elements separated by comma
			LOGGER.debug("All the elements that need to be rerendered are: [".concat(reRenderedString).concat("]"));
			Collections.addAll(elementIdToBeRerendered, reRenderedString.split(COMMA_SEPARATOR));
		}

		//auto rerendered or if we have to rerender a specific zone.
		if (ajaxRendered || elementIdToBeRerendered.contains(id)){
			LOGGER.debug("["+id+"] : Need to be refresh or fully rerendered.");
			// Refresh a specific zone.
			AjaxUtils.addZonesToRefresh(pageContext.getRequest(), id);	
		}
		
		super.doStartTag();
		try {
			pageContext.getOut().print(TagUtils.getZoneStartDelimiter(id));
		} catch (IOException e) {
			throw new JspException(e);
		}

		final ServletRequest request = pageContext.getRequest();

		if (skipIfNotIncluded && AjaxUtils.isAjaxRequest(request)
				&& !AjaxUtils.getZonesToRefresh(request).contains(id)){
			return SKIP_BODY;
		} else {
			return EVAL_BODY_INCLUDE;
		}
	}

	/**
	* This method prints a end tag on output of <code>pageContext</code>
	* @return int
	* @throw JspException
	*/
	public int doEndTag() throws JspException {
		try {
			pageContext.getOut().print(TagUtils.getZoneEndDelimiter(id));
		} catch (IOException e) {
			throw new JspException(e);
		}
		super.doEndTag();
		return EVAL_PAGE;
	}
	
	/**
	 * setter for pageContext
	 * @param pageContext PageContext
	 */
	public void setPageContext(final PageContext pageContext) {
		skipIfNotIncluded = DEFAULT_SKIP_INF_NOT_INCLUDED;
		super.setPageContext(pageContext);
	}
	
	/**
	 * getter for skipIfNotIncluded
	 * @return boolean
	 */
	public boolean isSkipIfNotIncluded() {
		return skipIfNotIncluded;
	}

	/**
	* Setter for skipIfNotIncluded
	 * @param skipIfNotIncluded
	 *            boolean
	 * @jsp.attribute required="false" rtexprvalue="true" type="boolean"
	 *                description="if the zone is not in the include list, tag
	 *                content is not evaluated."
	 */
	public void setSkipIfNotIncluded(final boolean skipIfNotIncluded) {
		this.skipIfNotIncluded = skipIfNotIncluded;
	}

	/**
	* Getter for ajaxRendered
	*/
	public boolean isAjaxRendered() {
		return ajaxRendered;
	}

	/**
	* Setter for ajaxRendered
	 * @param ajaxRendered
	 *            boolean
	 * @jsp.attribute required="false" rtexprvalue="true" type="boolean"
	 *                description="if is set to true the zone always rerendred."
	 */
	public void setAjaxRendered(final boolean ajaxRendered) {
		this.ajaxRendered = ajaxRendered;
	}

	/**
	 * Elements that need to be reRendered // Getter
	 */
	public String getReRendered() {
		return reRendered;
	}

	/**
	 * Elements that need to be reRendered // Setter
	 */
	public void setReRendered(final String reRendered) {
		this.reRendered = reRendered;
	}
}
