<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file="/include.jsp"  %>
<html >
<head >
<title  > 
<fmt:message key="cUC56_Search_BLU_AGE_DOCUMENTATION"/>
</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"  > 
</meta>
<link href="<c:url value= "/presentation/_style/_style.css"/>" rel="stylesheet" type="text/css"  /> 
<link href="<c:url value= "/presentation/_include/jquery/rd_lib.css"/>" rel="stylesheet" type="text/css"  /> 
<script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/jquery.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/rd_setup.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/rd_lib.js"/>" space="preserve"  ></script><link rel="shortcut icon" href="<c:url value= "/presentation/_style/favicon.ico"/>"  /> 

</head>

<body class=" " > 
<div id="rd_ghost"  > 
<small  > 
<fmt:message key="cUC56_Search___CSS_DEACTIVATED"/>
</small>
</div>
<div id="rd_jghost"  > 
<small  > 
<fmt:message key="cUC56_Search___JAVASCRIPT_DEACTIVATED"/>
</small>
</div>
<div id="shape"  > 
<div class="ui-layout-center"  > 
<div class="east-north"  > 
	<form:form id="uC56_Search" commandName="uC56_SearchForm"  enctype="application/x-www-form-urlencoded" method="post">
<h2 class="rd_first"  > 
<fmt:message key="cUC56_Search_STANDARD_FEATURES__SERVICES_"/>
</h2>
<table class="rd_tableform"  >
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC56_Search_NAME_"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
<form:input id="txt_name" path="sqlCriteria.name"  
		/>
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<fmt:message key="cUC56_Search_"/>
</td> 
<td rowspan="1" colspan="1"  >
<a id="lnk_searchTeams"onclick="javascript:btn_click_cExecuteUC56_Search(this.href);return false;"				href="<c:url value="/presentation/standardfeatures/services/sql/uc56_sqloperation/UC56_Search/lnk_searchTeams.html"/>"
							 shape="rect"  >									 
<fmt:message key="cUC56_Search_SEARCH_TEAMS_BY_NAME"/>
</a>
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<fmt:message key="cUC56_Search__222"/>
</td> 
<td rowspan="1" colspan="1"  >
<a id="lnk_searchTeamsPostProcess"onclick="javascript:btn_click_cExecuteUC56_Search(this.href);return false;"				href="<c:url value="/presentation/standardfeatures/services/sql/uc56_sqloperation/UC56_Search/lnk_searchTeamsPostProcess.html"/>"
							 shape="rect"  >									 
<fmt:message key="cUC56_Search_SEARCH_TEAMS_WITH_POST_PROCES"/>
</a>
</td> 
</tr>
</table> 
<h4  > 
<fmt:message key="cUC56_Search_REGULAR_RESULTS"/>
</h4>
<display:table uid="tab_searchResult"  sort="list" requestURI="" name="sessionScope.uC56_SearchForm.searchResult" class="rd_tabledata"  >
	<c:set   var="rownum">
		<%=(tab_searchResult !=	null) ? new Integer(tab_searchResult_rowNum.intValue() - 1) : new Integer(0) %>
	</c:set>
	<c:set   var="idRow">
		<%=(tab_searchResult != null ) ? tab_searchResult_rowNum.intValue() - 1 : -1 %>
	</c:set>
	<c:set   var="idTab">
		<%=(tab_searchResult != null ) ? "tab_searchResult" : null %>
	</c:set>
	<c:set var="tableHashId">
		<%= new org.displaytag.util.ParamEncoder("tab_searchResult").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE) %>
	</c:set>
	<c:set var="tableHashId2">
		<%=request.getParameter(new org.displaytag.util.ParamEncoder("tab_searchResult").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE)) %>
	</c:set>	
	
<display:column format=""  titleKey="cUC56_Search_NAME">
		<span id="txt_name">
	<c:out value="${tab_searchResult.name}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="cUC56_Search_CITY">
		<span id="txt_city">
	<c:out value="${tab_searchResult.city}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="cUC56_Search_STATE">
		<span id="txt_state">
	<c:out value="${tab_searchResult.state.name}" />
		</span>
	</display:column>
	
<display:column format=""  >
<a id="lnk_loadPlayers[${rownum}]"onclick="javascript:btn_click_cExecuteUC56_Search(this.href);return false;"			href="<c:url value="/presentation/standardfeatures/services/sql/uc56_sqloperation/UC56_Search/lnk_loadPlayers.html?id="/><c:out value="${idRow}" />&tab=<c:out value="tab_searchResult" />&<c:out value="${tableHashId}"/>=<c:out value="${tableHashId2}"/>"
	 shape="rect"  >									 
<fmt:message key="cUC56_Search__SEARCH_PLAYERS_"/>
</a>
</display:column>
</display:table>
<h4  > 
<fmt:message key="cUC56_Search_POST_PROCESSED_RESULTS"/>
</h4>
<display:table uid="tab_searchResultPostProcess"  sort="list" requestURI="" name="sessionScope.uC56_SearchForm.searchResultPostProcess" class="rd_tabledata"  >
	<c:set   var="rownum">
		<%=(tab_searchResultPostProcess !=	null) ? new Integer(tab_searchResultPostProcess_rowNum.intValue() - 1) : new Integer(0) %>
	</c:set>
	<c:set   var="idRow">
		<%=(tab_searchResultPostProcess != null ) ? tab_searchResultPostProcess_rowNum.intValue() - 1 : -1 %>
	</c:set>
	<c:set   var="idTab">
		<%=(tab_searchResultPostProcess != null ) ? "tab_searchResultPostProcess" : null %>
	</c:set>
	<c:set var="tableHashId">
		<%= new org.displaytag.util.ParamEncoder("tab_searchResultPostProcess").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE) %>
	</c:set>
	<c:set var="tableHashId2">
		<%=request.getParameter(new org.displaytag.util.ParamEncoder("tab_searchResultPostProcess").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE)) %>
	</c:set>	
	
<display:column format=""  titleKey="cUC56_Search_NAME_223">
		<span id="txt_name">
	<c:out value="${tab_searchResultPostProcess.name}" />
		</span>
	</display:column>
</display:table>
	</form:form>
	<script language="javascript">
		function btn_click_cExecuteUC56_Search(url) {
			UC56_Searchvar = document.getElementById('uC56_Search');
			UC56_Searchvar.setAttribute('action', url);
			UC56_Searchvar.submit();
		}
		function btn_click_cSetActionUC56_Search(url) {
			UC56_Searchvar = document.getElementById('uC56_Search');
			UC56_Searchvar.setAttribute('action', url);
		}
	</script>
</div>
<div class="east-center"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/services/sql/uc56_sqloperation/UC56_Searchcmp_tabs" />
</div>
</div>
<div class="ui-layout-north"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/services/sql/uc56_sqloperation/UC56_Searchcmp_header" />
</div>
<div class="ui-layout-south"  > 
<fmt:message key="cUC56_Search___"/>
<span id="rd_jversion"  > 
<fmt:message key="cUC56_Search_BLU_AGE_DOCUMENTATION_224"/>
</span>
</div>
<div class="ui-layout-west"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/services/sql/uc56_sqloperation/UC56_Searchcmp_treeview" />
</div>
</div>
</body>
</html>
