/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.entities.uc63_association ;

// Import declaration.
// Java imports.
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.bluage.documentation.business.standardfeatures.entities.uc63_association.entities.daofinder.Coach63DAOFinderImpl;
import com.bluage.documentation.business.standardfeatures.entities.uc63_association.entities.daofinder.Stadium63DAOFinderImpl;
import com.bluage.documentation.business.standardfeatures.entities.uc63_association.entities.daofinder.State63DAOFinderImpl;
import com.bluage.documentation.business.standardfeatures.entities.uc63_association.entities.daofinder.Team63DAOFinderImpl;
import com.netfective.bluage.core.exception.ApplicationException;
import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;

/**
 * Class : UC63_HomeController
 */
@Controller
@Scope("prototype")
@SessionAttributes("uC63_HomeForm")
@RequestMapping(value= "/presentation/standardfeatures/entities/uc63_association")
public class UC63_HomeController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC63_HomeController.class);
	
	//Current form name
	private static final String U_C63__HOME_FORM = "uC63_HomeForm";

	//CONSTANT: allStates table or repeater.
	private static final String ALL_STATES = "allStates";
				//CONSTANT: allStadiums table or repeater.
	private static final String ALL_STADIUMS = "allStadiums";
				//CONSTANT: allTeams table or repeater.
	private static final String ALL_TEAMS = "allTeams";
				//CONSTANT: allCoachs table or repeater.
	private static final String ALL_COACHS = "allCoachs";
				
	/**
	 * Property:customDateEditorsUC63_HomeController
	 */
	final private Map<String,CustomDateEditor> customDateEditorsUC63_HomeController = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:uC63_HomeValidator
	 */
	final private UC63_HomeValidator uC63_HomeValidator = new UC63_HomeValidator();
	
	/**
	 * Generic Finder : team63DAOFinderImpl.
	 */
	@Autowired
	private Team63DAOFinderImpl team63DAOFinderImpl;
	
	/**
	 * Generic Finder : coach63DAOFinderImpl.
	 */
	@Autowired
	private Coach63DAOFinderImpl coach63DAOFinderImpl;
	
	/**
	 * Generic Finder : state63DAOFinderImpl.
	 */
	@Autowired
	private State63DAOFinderImpl state63DAOFinderImpl;
	
	/**
	 * Generic Finder : stadium63DAOFinderImpl.
	 */
	@Autowired
	private Stadium63DAOFinderImpl stadium63DAOFinderImpl;
	
	
	// Initialise all the instances for UC63_HomeController
		// Initialize the instance allStates for the state lnk_addteam
		private List allStates; // Initialize the instance allStates for UC63_HomeController
						// Initialize the instance allStadiums for the state lnk_addteam
		private List allStadiums; // Initialize the instance allStadiums for UC63_HomeController
						// Initialize the instance allTeams for the state lnk_addteam
		private List allTeams; // Initialize the instance allTeams for UC63_HomeController
						// Initialize the instance allCoachs for the state lnk_addteam
		private List allCoachs; // Initialize the instance allCoachs for UC63_HomeController
				
	/**
	* This method initialise the form : UC63_HomeForm 
	* @return UC63_HomeForm
	*/
	@ModelAttribute("UC63_HomeFormInit")
	public void initUC63_HomeForm(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, U_C63__HOME_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method UC63_HomeForm."); //for lnk_addteam 
		}
		UC63_HomeForm uC63_HomeForm;
	
		if(request.getSession().getAttribute(U_C63__HOME_FORM) != null){
			uC63_HomeForm = (UC63_HomeForm)request.getSession().getAttribute(U_C63__HOME_FORM);
		} else {
			uC63_HomeForm = new UC63_HomeForm();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form UC63_HomeForm.");
		}
		uC63_HomeForm = (UC63_HomeForm)populateDestinationForm(request.getSession(), uC63_HomeForm);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  UC63_HomeForm.");
		}
		model.addAttribute(U_C63__HOME_FORM, uC63_HomeForm);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param UC63_HomeForm : The sceen form.
	 */
	@RequestMapping(value = "/UC63_Home.html" ,method = RequestMethod.GET)
	public void prepareUC63_Home(final HttpServletRequest request,final  Model model,final  @ModelAttribute("UC63_HomeFormInit") UC63_HomeForm uC63_HomeForm){
		
		UC63_HomeForm currentUC63_HomeForm = uC63_HomeForm;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(U_C63__HOME_FORM) == null){
			if(currentUC63_HomeForm!=null){
				request.getSession().setAttribute(U_C63__HOME_FORM, currentUC63_HomeForm);
			}else {
				currentUC63_HomeForm = new UC63_HomeForm();
				request.getSession().setAttribute(U_C63__HOME_FORM, currentUC63_HomeForm);	
			}
		} else {
			currentUC63_HomeForm = (UC63_HomeForm) request.getSession().getAttribute(U_C63__HOME_FORM);
		}

		try {
			List allTeams = team63DAOFinderImpl.findAll();
			put(request.getSession(), ALL_TEAMS, allTeams);
		} catch (ApplicationException e) {
			errorLogger("An error while calling the generic finder for the instance : allTeams.",e);
		}
		try {
			List allCoachs = coach63DAOFinderImpl.findAll();
			put(request.getSession(), ALL_COACHS, allCoachs);
		} catch (ApplicationException e) {
			errorLogger("An error while calling the generic finder for the instance : allCoachs.",e);
		}
		try {
			List allStates = state63DAOFinderImpl.findAll();
			put(request.getSession(), ALL_STATES, allStates);
		} catch (ApplicationException e) {
			errorLogger("An error while calling the generic finder for the instance : allStates.",e);
		}
		try {
			List allStadiums = stadium63DAOFinderImpl.findAll();
			put(request.getSession(), ALL_STADIUMS, allStadiums);
		} catch (ApplicationException e) {
			errorLogger("An error while calling the generic finder for the instance : allStadiums.",e);
		}
										currentUC63_HomeForm = (UC63_HomeForm)populateDestinationForm(request.getSession(), currentUC63_HomeForm);
		// Call all the Precontroller.
	currentUC63_HomeForm.setAlwaysCallPreControllers(true);
		model.addAttribute(U_C63__HOME_FORM, currentUC63_HomeForm);
		request.getSession().setAttribute(U_C63__HOME_FORM, currentUC63_HomeForm);
		
	}
	
									
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("UC63_HomeController [ ");
			strBToS.append(ALL_STATES);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(allStates);
			strBToS.append(END_TO_STRING_DELIMITER);
						strBToS.append(ALL_STADIUMS);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(allStadiums);
			strBToS.append(END_TO_STRING_DELIMITER);
						strBToS.append(ALL_TEAMS);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(allTeams);
			strBToS.append(END_TO_STRING_DELIMITER);
						strBToS.append(ALL_COACHS);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(allCoachs);
			strBToS.append(END_TO_STRING_DELIMITER);
					// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(uC63_HomeValidator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsUC63_HomeController!=null); 
		return strBToS.toString();
	}
}
