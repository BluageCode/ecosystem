--<ScriptOptions statementTerminator=";"/>

ALTER TABLE player38 DROP CONSTRAINT player38_team38_fkc;

ALTER TABLE team38 DROP CONSTRAINT team38_pkey;

ALTER TABLE player38 DROP CONSTRAINT player38_pkey;

ALTER TABLE position38 DROP CONSTRAINT position38_pkey;

DROP TABLE team38;

DROP TABLE player38;

DROP TABLE position38;

CREATE TABLE team38 (
		id INT8 NOT NULL,
		version INT4 NOT NULL,
		city VARCHAR(1024),
		name VARCHAR(1024)
	);

CREATE TABLE player38 (
		id INT8 NOT NULL,
		version INT4 NOT NULL,
		first_name VARCHAR(30) NOT NULL,
		last_name VARCHAR(30) NOT NULL,
		date_of_birth DATE,
		estimated_value FLOAT4,
		weight INT4,
		height INT4,
		rookie BOOL,
		team38_fk INT8
	);

CREATE TABLE position38 (
		code VARCHAR(2) NOT NULL,
		version INT4 NOT NULL,
		name VARCHAR(20) NOT NULL
	);

INSERT INTO player38 VALUES (287, 0, 'Owen', 'Little', '1981-10-27', 45.869999, 135, 183, false, 5);
INSERT INTO player38 VALUES (288, 0, 'Irvin', 'Lee', '1988-02-25', 10.61, 88, 157, false, 1);
INSERT INTO player38 VALUES (289, 0, 'Wayne', 'Rose', '1981-10-28', 20.41, 115, 238, false, 1);
INSERT INTO player38 VALUES (290, 0, 'Lucas', 'Williamson', '1983-06-29', 38.360001, 73, 151, false, 1);
INSERT INTO player38 VALUES (291, 0, 'Walter', 'Reyes', '1984-12-09', 46.150002, 104, 193, false, 1);
INSERT INTO player38 VALUES (292, 0, 'Kirk', 'Cox', '1973-05-10', 26.58, 60, 179, false, 3);
INSERT INTO player38 VALUES (293, 0, 'Arthur', 'Payne', '1981-11-20', 30.860001, 66, 190, false, 5);
INSERT INTO player38 VALUES (294, 0, 'Leroy', 'Moore', '1987-07-26', 45.799999, 89, 183, false, 2);
INSERT INTO player38 VALUES (295, 0, 'Milton', 'Johnson', '1969-07-02', 22.219999, 68, 205, false, 5);
INSERT INTO player38 VALUES (296, 0, 'Sammy', 'Mendez', '1986-07-01', 2.27, 96, 234, false, 2);
INSERT INTO player38 VALUES (297, 0, 'Rafael', 'Steele', '1968-11-25', 15.48, 66, 153, false, 1);
INSERT INTO player38 VALUES (298, 0, 'Bob', 'Jacobs', '1988-05-21', 13.23, 72, 172, false, 2);

INSERT INTO position38 VALUES ('PG', 0, 'Point Guard');
INSERT INTO position38 VALUES ('SF', 0, 'Small Forward');
INSERT INTO position38 VALUES ('C', 0, 'Center');
INSERT INTO position38 VALUES ('SG', 0, 'Shooting Guard');
INSERT INTO position38 VALUES ('PF', 0, 'Power Forward');

INSERT INTO team38 VALUES (1, 0, 'Charlotte', 'Tigers');
INSERT INTO team38 VALUES (2, 0, 'Kansas City', 'Bears');
INSERT INTO team38 VALUES (3, 0, 'Detroit', 'Chargers');
INSERT INTO team38 VALUES (4, 0, 'Detroit', 'Hawks');
INSERT INTO team38 VALUES (5, 0, 'Omaha', 'Braves');



ALTER TABLE team38 ADD CONSTRAINT team38_pkey PRIMARY KEY (id);

ALTER TABLE player38 ADD CONSTRAINT player38_pkey PRIMARY KEY (id);

ALTER TABLE position38 ADD CONSTRAINT position38_pkey PRIMARY KEY (code);

ALTER TABLE player38 ADD CONSTRAINT player38_team38_fkc FOREIGN KEY (team38_fk)
	REFERENCES team38 (id);

