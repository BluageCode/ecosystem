<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file="/include.jsp"  %>
<html >
<head >
<title  > 
<fmt:message key="cUC65_DisplaywithoutFetch_BLU_AGE_DOCUMENTATION"/>
</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"  > 
</meta>
<link href="<c:url value= "/presentation/_style/_style.css"/>" rel="stylesheet" type="text/css"  /> 
<link href="<c:url value= "/presentation/_include/jquery/rd_lib.css"/>" rel="stylesheet" type="text/css"  /> 
<script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/jquery.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/rd_setup.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/rd_lib.js"/>" space="preserve"  ></script><link rel="shortcut icon" href="<c:url value= "/presentation/_style/favicon.ico"/>"  /> 

</head>

<body class=" " > 
<div id="rd_ghost"  > 
<small  > 
<fmt:message key="cUC65_DisplaywithoutFetch___CSS_DEACTIVATED"/>
</small>
</div>
<div id="rd_jghost"  > 
<small  > 
<fmt:message key="cUC65_DisplaywithoutFetch___JAVASCRIPT_DEACTIVATED"/>
</small>
</div>
<div id="shape"  > 
<div class="ui-layout-center"  > 
<div class="east-north"  > 
	<form:form id="uC65_DisplaywithoutFetch" commandName="uC65_DisplaywithoutFetchForm"  enctype="application/x-www-form-urlencoded" method="post">
<h2 class="rd_first"  > 
<fmt:message key="cUC65_DisplaywithoutFetch_STANDARD_FEATURES__ENTITIES_"/>
</h2>
<b  > 
<fmt:message key="cUC65_DisplaywithoutFetch__TEAM__"/>
</b>
		<span id="txt_TeamName">
	<c:out value="${uC65_DisplaywithoutFetchForm.teamLazyFalse.name}" />
		</span>
	<b  > 
<fmt:message key="cUC65_DisplaywithoutFetch___CITY_"/>
</b>
		<span id="txt_Teamcity">
	<c:out value="${uC65_DisplaywithoutFetchForm.teamLazyFalse.city}" />
		</span>
	<fmt:message key="cUC65_DisplaywithoutFetch__"/>
<a id="lnk_back"				href="<c:url value="/presentation/standardfeatures/entities/uc65_lazy/UC65_HomePage.html"/>"
													 shape="rect"  >									 
<fmt:message key="cUC65_DisplaywithoutFetch_BACK"/>
</a>
<h4  > 
<fmt:message key="cUC65_DisplaywithoutFetch_ROOKIE_PLAYERS"/>
</h4>
<display:table uid="tab_players" pagesize="10"  sort="list" requestURI="" name="sessionScope.uC65_DisplaywithoutFetchForm.players" class="rd_tabledata"  >
	<c:set   var="rownum">
		<%=(tab_players !=	null) ? new Integer(tab_players_rowNum.intValue() - 1) : new Integer(0) %>
	</c:set>
	<c:set   var="idRow">
		<%=(tab_players != null ) ? tab_players_rowNum.intValue() - 1 : -1 %>
	</c:set>
	<c:set   var="idTab">
		<%=(tab_players != null ) ? "tab_players" : null %>
	</c:set>
	<c:set var="tableHashId">
		<%= new org.displaytag.util.ParamEncoder("tab_players").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE) %>
	</c:set>
	<c:set var="tableHashId2">
		<%=request.getParameter(new org.displaytag.util.ParamEncoder("tab_players").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE)) %>
	</c:set>	
	
<display:column format=""  titleKey="cUC65_DisplaywithoutFetch_NAME">
		<span id="txt_firstName">
	<c:out value="${tab_players.firstName}" />
		</span>
	<fmt:message key="cUC65_DisplaywithoutFetch___"/>
		<span id="txt_lastName">
	<c:out value="${tab_players.lastName}" />
		</span>
	</display:column>
</display:table>
	</form:form>
	<script language="javascript">
		function btn_click_cExecuteUC65_DisplaywithoutFetch(url) {
			UC65_DisplaywithoutFetchvar = document.getElementById('uC65_DisplaywithoutFetch');
			UC65_DisplaywithoutFetchvar.setAttribute('action', url);
			UC65_DisplaywithoutFetchvar.submit();
		}
		function btn_click_cSetActionUC65_DisplaywithoutFetch(url) {
			UC65_DisplaywithoutFetchvar = document.getElementById('uC65_DisplaywithoutFetch');
			UC65_DisplaywithoutFetchvar.setAttribute('action', url);
		}
	</script>
</div>
<div class="east-center"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/entities/uc65_lazy/UC65_DisplaywithoutFetchcmp_tabs" />
</div>
</div>
<div class="ui-layout-north"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/entities/uc65_lazy/UC65_DisplaywithoutFetchcmp_header" />
</div>
<div class="ui-layout-south"  > 
<fmt:message key="cUC65_DisplaywithoutFetch____165"/>
<span id="rd_jversion"  > 
<fmt:message key="cUC65_DisplaywithoutFetch_BLU_AGE_DOCUMENTATION_166"/>
</span>
</div>
<div class="ui-layout-west"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/entities/uc65_lazy/UC65_DisplaywithoutFetchcmp_treeview" />
</div>
</div>
</body>
</html>
