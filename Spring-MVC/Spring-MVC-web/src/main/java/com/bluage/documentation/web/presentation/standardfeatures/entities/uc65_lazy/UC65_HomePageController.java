/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.entities.uc65_lazy ;

// Import declaration.
// Java imports.
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.bluage.documentation.business.standardfeatures.entities.uc65_lazy.bos.Team65BO;
import com.bluage.documentation.business.standardfeatures.entities.uc65_lazy.entities.daofinder.Team65DAOFinderImpl;
import com.bluage.documentation.service.standardfeatures.entities.uc65_lazy.ServiceLoadPlayers;
import com.netfective.bluage.core.exception.ApplicationException;
import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;
import com.netfective.bluage.core.web.IForm;

/**
 * Class : UC65_HomePageController
 */
@Controller
@Scope("prototype")
@SessionAttributes("uC65_HomePageForm")
@RequestMapping(value= "/presentation/standardfeatures/entities/uc65_lazy")
public class UC65_HomePageController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC65_HomePageController.class);
	
	//Current form name
	private static final String U_C65__HOME_PAGE_FORM = "uC65_HomePageForm";

	//CONSTANT: teams table or repeater.
	private static final String TEAMS = "teams";
				//CONSTANT: teamLazyTrue
	private static final String TEAM_LAZY_TRUE = "teamLazyTrue";
	//CONSTANT: selectedTeam
	private static final String SELECTED_TEAM = "selectedTeam";
	//CONSTANT: teamLazyFalse
	private static final String TEAM_LAZY_FALSE = "teamLazyFalse";
	
	/**
	 * Property:customDateEditorsUC65_HomePageController
	 */
	final private Map<String,CustomDateEditor> customDateEditorsUC65_HomePageController = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:uC65_HomePageValidator
	 */
	final private UC65_HomePageValidator uC65_HomePageValidator = new UC65_HomePageValidator();
	
	/**
	 * Service declaration : serviceLoadPlayers.
	 */
	@Autowired
	private ServiceLoadPlayers serviceLoadPlayers;
	
	/**
	 * Generic Finder : team65DAOFinderImpl.
	 */
	@Autowired
	private Team65DAOFinderImpl team65DAOFinderImpl;
	
	
	// Initialise all the instances for UC65_HomePageController
		// Initialize the instance teams for the state findTeams
		private List teams; // Initialize the instance teams for UC65_HomePageController
						// Declare the instance teamLazyTrue
		private Team65BO teamLazyTrue;
		// Declare the instance selectedTeam
		private Team65BO selectedTeam;
		// Declare the instance teamLazyFalse
		private Team65BO teamLazyFalse;
			/**
	 * Operation : lnk_view_fetch
 	 * @param model : 
 	 * @param uC65_HomePage : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC65_HomePage/lnk_view_fetch.html",method = RequestMethod.POST)
	public String lnk_view_fetch(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC65_HomePageForm") UC65_HomePageForm  uC65_HomePageForm, final BindingResult bindingResult ,final @RequestParam(ID_REQUEST_PARAM) Integer index){
		
		
		
		LOGGER.info("Begin method : lnk_view_fetch"); 
		infoLogger(uC65_HomePageForm); 
		uC65_HomePageForm.setAlwaysCallPreControllers(false); 
		// initialization for lnk_view_fetch
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state lnk_view_fetch if the validation fail, you will be forward to the screen : "/presentation/standardfeatures/entities/uc65_lazy/UC65_HomePage"
		if(errorsMessages(request,response,uC65_HomePageForm,bindingResult,"selectedTeam", uC65_HomePageValidator, customDateEditorsUC65_HomePageController)){ 
			return "/presentation/standardfeatures/entities/uc65_lazy/UC65_HomePage"; 
		}

										 callLoadPlayersWithFetching(request,uC65_HomePageForm , index);
											// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.entities.uc65_lazy.UC65_DisplayPlayersForm or not from lnk_view_fetch
			final UC65_DisplayPlayersForm uC65_DisplayPlayersForm =  new UC65_DisplayPlayersForm();
			// Populate the destination form with all the instances returned from lnk_view_fetch.
			final IForm uC65_DisplayPlayersForm2 = populateDestinationForm(request.getSession(), uC65_DisplayPlayersForm); 
					
			// Add the form to the model.
			model.addAttribute("uC65_DisplayPlayersForm", uC65_DisplayPlayersForm2); 
			
			request.getSession().setAttribute("uC65_DisplayPlayersForm", uC65_DisplayPlayersForm2);
			
			// "OK" CASE => destination screen path from lnk_view_fetch
			LOGGER.info("Go to the screen 'UC65_DisplayPlayers'.");
			// Redirect (PRG) from lnk_view_fetch
			destinationPath =  "redirect:/presentation/standardfeatures/entities/uc65_lazy/UC65_DisplayPlayers.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_view_fetch 
	}
	
				/**
	 * Operation : lnk_view_nofetch
 	 * @param model : 
 	 * @param uC65_HomePage : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC65_HomePage/lnk_view_nofetch.html",method = RequestMethod.POST)
	public String lnk_view_nofetch(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC65_HomePageForm") UC65_HomePageForm  uC65_HomePageForm, final BindingResult bindingResult ,final @RequestParam(ID_REQUEST_PARAM) Integer index){
		
		
		
		LOGGER.info("Begin method : lnk_view_nofetch"); 
		infoLogger(uC65_HomePageForm); 
		uC65_HomePageForm.setAlwaysCallPreControllers(false); 
		// initialization for lnk_view_nofetch
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state lnk_view_nofetch if the validation fail, you will be forward to the screen : "/presentation/standardfeatures/entities/uc65_lazy/UC65_HomePage"
		if(errorsMessages(request,response,uC65_HomePageForm,bindingResult,"selectedTeam", uC65_HomePageValidator, customDateEditorsUC65_HomePageController)){ 
			return "/presentation/standardfeatures/entities/uc65_lazy/UC65_HomePage"; 
		}

										 callLoadPlayersWithoutFetching(request,uC65_HomePageForm , index);
											// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.entities.uc65_lazy.UC65_DisplaywithoutFetchForm or not from lnk_view_nofetch
			final UC65_DisplaywithoutFetchForm uC65_DisplaywithoutFetchForm =  new UC65_DisplaywithoutFetchForm();
			// Populate the destination form with all the instances returned from lnk_view_nofetch.
			final IForm uC65_DisplaywithoutFetchForm2 = populateDestinationForm(request.getSession(), uC65_DisplaywithoutFetchForm); 
					
			// Add the form to the model.
			model.addAttribute("uC65_DisplaywithoutFetchForm", uC65_DisplaywithoutFetchForm2); 
			
			request.getSession().setAttribute("uC65_DisplaywithoutFetchForm", uC65_DisplaywithoutFetchForm2);
			
			// "OK" CASE => destination screen path from lnk_view_nofetch
			LOGGER.info("Go to the screen 'UC65_DisplaywithoutFetch'.");
			// Redirect (PRG) from lnk_view_nofetch
			destinationPath =  "redirect:/presentation/standardfeatures/entities/uc65_lazy/UC65_DisplaywithoutFetch.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_view_nofetch 
	}
	
	
	/**
	* This method initialise the form : UC65_HomePageForm 
	* @return UC65_HomePageForm
	*/
	@ModelAttribute("UC65_HomePageFormInit")
	public void initUC65_HomePageForm(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, U_C65__HOME_PAGE_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method UC65_HomePageForm."); //for lnk_view_nofetch 
		}
		UC65_HomePageForm uC65_HomePageForm;
	
		if(request.getSession().getAttribute(U_C65__HOME_PAGE_FORM) != null){
			uC65_HomePageForm = (UC65_HomePageForm)request.getSession().getAttribute(U_C65__HOME_PAGE_FORM);
		} else {
			uC65_HomePageForm = new UC65_HomePageForm();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form UC65_HomePageForm.");
		}
		uC65_HomePageForm = (UC65_HomePageForm)populateDestinationForm(request.getSession(), uC65_HomePageForm);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  UC65_HomePageForm.");
		}
		model.addAttribute(U_C65__HOME_PAGE_FORM, uC65_HomePageForm);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param UC65_HomePageForm : The sceen form.
	 */
	@RequestMapping(value = "/UC65_HomePage.html" ,method = RequestMethod.GET)
	public void prepareUC65_HomePage(final HttpServletRequest request,final  Model model,final  @ModelAttribute("UC65_HomePageFormInit") UC65_HomePageForm uC65_HomePageForm){
		
		UC65_HomePageForm currentUC65_HomePageForm = uC65_HomePageForm;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(U_C65__HOME_PAGE_FORM) == null){
			if(currentUC65_HomePageForm!=null){
				request.getSession().setAttribute(U_C65__HOME_PAGE_FORM, currentUC65_HomePageForm);
			}else {
				currentUC65_HomePageForm = new UC65_HomePageForm();
				request.getSession().setAttribute(U_C65__HOME_PAGE_FORM, currentUC65_HomePageForm);	
			}
		} else {
			currentUC65_HomePageForm = (UC65_HomePageForm) request.getSession().getAttribute(U_C65__HOME_PAGE_FORM);
		}

		try {
			List teams = team65DAOFinderImpl.findAll();
			put(request.getSession(), TEAMS, teams);
		} catch (ApplicationException e) {
			errorLogger("An error while calling the generic finder for the instance : teams.",e);
		}
				currentUC65_HomePageForm = (UC65_HomePageForm)populateDestinationForm(request.getSession(), currentUC65_HomePageForm);
		// Call all the Precontroller.
	currentUC65_HomePageForm.setAlwaysCallPreControllers(true);
		model.addAttribute(U_C65__HOME_PAGE_FORM, currentUC65_HomePageForm);
		request.getSession().setAttribute(U_C65__HOME_PAGE_FORM, currentUC65_HomePageForm);
		
	}
	
										/**
	 * method callLoadPlayersWithFetching
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 * @param index Integer the current index 
	 */
	private void callLoadPlayersWithFetching(HttpServletRequest request,IForm form,Integer index) {
		teamLazyTrue = (Team65BO)get(request.getSession(),form, "teamLazyTrue");  
										 
					if(index!=null){  
			 selectedTeam = (Team65BO)((List)get(request.getSession(),form, "teams")).get(index);  
		} else {
			 selectedTeam= null; 
		}
				 
					try {
				// executing LoadPlayersWithFetching in lnk_view_nofetch
				teamLazyTrue = 	serviceLoadPlayers.loadPlayersWithFetching(
			selectedTeam
			);  
 
				put(request.getSession(), TEAM_LAZY_TRUE,teamLazyTrue);
								// processing variables LoadPlayersWithFetching in lnk_view_nofetch
				put(request.getSession(), SELECTED_TEAM,selectedTeam); 
			
			} catch (ApplicationException e) { 
				// error handling for operation loadPlayersWithFetching called LoadPlayersWithFetching
				errorLogger("An error occured during the execution of the operation : loadPlayersWithFetching",e); 
		
			}
	}
									/**
	 * method callLoadPlayersWithoutFetching
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 * @param index Integer the current index 
	 */
	private void callLoadPlayersWithoutFetching(HttpServletRequest request,IForm form,Integer index) {
		teamLazyFalse = (Team65BO)get(request.getSession(),form, "teamLazyFalse");  
										 
					if(index!=null){  
			 selectedTeam = (Team65BO)((List)get(request.getSession(),form, "teams")).get(index);  
		} else {
			 selectedTeam= null; 
		}
				 
					try {
				// executing LoadPlayersWithoutFetching in lnk_view_nofetch
				teamLazyFalse = 	serviceLoadPlayers.loadPlayersWithoutFetching(
			selectedTeam
			);  
 
				put(request.getSession(), TEAM_LAZY_FALSE,teamLazyFalse);
								// processing variables LoadPlayersWithoutFetching in lnk_view_nofetch
				put(request.getSession(), SELECTED_TEAM,selectedTeam); 
			
			} catch (ApplicationException e) { 
				// error handling for operation loadPlayersWithoutFetching called LoadPlayersWithoutFetching
				errorLogger("An error occured during the execution of the operation : loadPlayersWithoutFetching",e); 
		
			}
	}
		
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("UC65_HomePageController [ ");
			strBToS.append(TEAMS);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(teams);
			strBToS.append(END_TO_STRING_DELIMITER);
						strBToS.append(TEAM_LAZY_TRUE);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(teamLazyTrue);
			strBToS.append(END_TO_STRING_DELIMITER);
			strBToS.append(SELECTED_TEAM);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(selectedTeam);
			strBToS.append(END_TO_STRING_DELIMITER);
			strBToS.append(TEAM_LAZY_FALSE);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(teamLazyFalse);
			strBToS.append(END_TO_STRING_DELIMITER);
		// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(uC65_HomePageValidator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsUC65_HomePageController!=null); 
		return strBToS.toString();
	}
}
