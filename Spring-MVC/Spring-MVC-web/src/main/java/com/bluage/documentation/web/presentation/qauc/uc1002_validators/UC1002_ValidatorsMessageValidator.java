/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.qauc.uc1002_validators ;


import org.apache.log4j.Logger;
import org.springframework.validation.Errors;

import com.netfective.bluage.core.validator.AbstractValidator;
/**
* Class :UC1002_ValidatorsMessageValidator
*/
public class UC1002_ValidatorsMessageValidator extends AbstractValidator{
	
	// LOGGER for the class UC1002_ValidatorsMessageValidator
	private static final Logger LOGGER = Logger.getLogger( UC1002_ValidatorsMessageValidator.class);
	
	
	/**
	* Operation validate for UC1002_ValidatorsMessageForm
	* @param obj : the current form (UC1002_ValidatorsMessageForm)
	* @param errors : The spring errors to return for the form UC1002_ValidatorsMessageForm
	*/
	public void validate(final Object obj,final Errors errors){
		LOGGER.info("Beginning method : validate"); // UC1002_ValidatorsMessageValidator
		UC1002_ValidatorsMessageForm cUC1002_ValidatorsMessageForm = (UC1002_ValidatorsMessageForm)obj; // UC1002_ValidatorsMessageValidator
		LOGGER.info("Ending method : validate the form "+ cUC1002_ValidatorsMessageForm.getClass().getName()); // UC1002_ValidatorsMessageValidator
	}

	/**
	* Method to implements to use spring validators (UC1002_ValidatorsMessageForm)
	* @param aClass : Class for the form UC1002_ValidatorsMessageForm
	* @return boolean
	*/
	public boolean supports(final Class aClass){
		return (new UC1002_ValidatorsMessageForm()).getClass().equals(aClass); // UC1002_ValidatorsMessageValidator
	}
}
