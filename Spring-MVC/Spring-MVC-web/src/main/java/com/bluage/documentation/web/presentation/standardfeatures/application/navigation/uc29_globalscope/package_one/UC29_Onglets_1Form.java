/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.application.navigation.uc29_globalscope.package_one;


import java.io.Serializable;
import java.util.List;

import com.netfective.bluage.core.web.AbstractForm;
import com.netfective.bluage.core.web.IForm;

/**
* Class : UC29_Onglets_1Form
*/
public class UC29_Onglets_1Form extends AbstractForm implements Serializable, IForm {
	
	/** Constant : serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	//CONSTANT : allTeams
	private static final String ALL_TEAMS = "allTeams";
	//CONSTANT : allPlayers
	private static final String ALL_PLAYERS = "allPlayers";
	/**
	 * 	Property: allTeams 
	 */
	private List allTeams;
	/**
	 * 	Property: allPlayers 
	 */
	private List allPlayers;
/**
	 * Default constructor : UC29_Onglets_1Form
	 */
	public UC29_Onglets_1Form() {
		super();
		// Initialize : allTeams
		this.allTeams = null;
		// Initialize : allPlayers
		this.allPlayers = null;
		this.setAlwaysCallPreControllers(true);
	}
	
	/**
	 * 	Getter : allTeams 
	 * 	@return : Return the allTeams instance.
	 */
	public List getAllTeams(){
		return allTeams; // For UC29_Onglets_1Form
	}
	
	/**
	 * 	Setter : allTeams 
	 *  @param allTeamsinstance : The instance to set.
	 */
	public void setAllTeams(final List allTeamsinstance){
		this.allTeams = allTeamsinstance;// For UC29_Onglets_1Form
	}
	/**
	 * 	Getter : allPlayers 
	 * 	@return : Return the allPlayers instance.
	 */
	public List getAllPlayers(){
		return allPlayers; // For UC29_Onglets_1Form
	}
	
	/**
	 * 	Setter : allPlayers 
	 *  @param allPlayersinstance : The instance to set.
	 */
	public void setAllPlayers(final List allPlayersinstance){
		this.allPlayers = allPlayersinstance;// For UC29_Onglets_1Form
	}
/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UC29_Onglets_1Form [ "+
ALL_TEAMS +" = " + allTeams +ALL_PLAYERS +" = " + allPlayers + "]";
	}
	
	/**
	 * Method setInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public void setInstanceByInstanceName(final String instanceName, final Object instance) {
		// Set the instance AllTeams.
		if(ALL_TEAMS.equals(instanceName)){// For UC29_Onglets_1Form
			this.setAllTeams((List)instance); // For UC29_Onglets_1Form
		}
				// Set the instance AllPlayers.
		if(ALL_PLAYERS.equals(instanceName)){// For UC29_Onglets_1Form
			this.setAllPlayers((List)instance); // For UC29_Onglets_1Form
		}
			}
	
	/**
	 * Method getInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public Object getInstanceByInstanceName(final String instanceName){
		// Temporary object for tmpUC29_Onglets_1Form.
		Object tmpUC29_Onglets_1Form = null;
		
			
		// Get the instance AllTeams for UC29_Onglets_1Form.
		if(ALL_TEAMS.equals(instanceName)){ // For UC29_Onglets_1Form
			tmpUC29_Onglets_1Form = this.getAllTeams(); // For UC29_Onglets_1Form
		}
			
		// Get the instance AllPlayers for UC29_Onglets_1Form.
		if(ALL_PLAYERS.equals(instanceName)){ // For UC29_Onglets_1Form
			tmpUC29_Onglets_1Form = this.getAllPlayers(); // For UC29_Onglets_1Form
		}
		return tmpUC29_Onglets_1Form;// For UC29_Onglets_1Form
	}
	
}
