/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.gettingstarted.uc02_entitiescreation;


import java.io.Serializable;
import java.util.List;

import com.bluage.documentation.business.gettingstarted.uc02_entitiescreation.bos.Team02BO;
import com.netfective.bluage.core.web.AbstractForm;
import com.netfective.bluage.core.web.IForm;

/**
* Class : UC02_EntitiesCreationForm
*/
public class UC02_EntitiesCreationForm extends AbstractForm implements Serializable, IForm {
	
	/** Constant : serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	//CONSTANT : allStates
	private static final String ALL_STATES = "allStates";
	//CONSTANT : allTeams
	private static final String ALL_TEAMS = "allTeams";
	//CONSTANT : resultDelete
	private static final String RESULT_DELETE = "resultDelete";
	//CONSTANT : teamToCreate
	private static final String TEAM_TO_CREATE = "teamToCreate";
	//CONSTANT : teamToDelete
	private static final String TEAM_TO_DELETE = "teamToDelete";
	/**
	 * 	Property: allStates 
	 */
	private List allStates;
	/**
	 * 	Property: allTeams 
	 */
	private List<Team02BO> allTeams;
	/**
	 * 	Property: resultDelete 
	 */
	private boolean resultDelete;
	/**
	 * 	Property: teamToCreate 
	 */
	private Team02BO teamToCreate;
	/**
	 * 	Property: teamToDelete 
	 */
	private Team02BO teamToDelete;
/**
	 * Default constructor : UC02_EntitiesCreationForm
	 */
	public UC02_EntitiesCreationForm() {
		super();
		// Initialize : allStates
		this.allStates = new java.util.ArrayList();
		// Initialize : allTeams
		this.allTeams = new java.util.ArrayList<com.bluage.documentation.business.gettingstarted.uc02_entitiescreation.bos.Team02BO>();
		// Initialize : resultDelete
		this.resultDelete = false;
		// Initialize : teamToCreate
		this.teamToCreate = new Team02BO();
		// Initialize : teamToDelete
		this.teamToDelete = null;
					this.setAlwaysCallPreControllers(true);
	}
	
	/**
	 * 	Getter : allStates 
	 * 	@return : Return the allStates instance.
	 */
	public List getAllStates(){
		return allStates; // For UC02_EntitiesCreationForm
	}
	
	/**
	 * 	Setter : allStates 
	 *  @param allStatesinstance : The instance to set.
	 */
	public void setAllStates(final List allStatesinstance){
		this.allStates = allStatesinstance;// For UC02_EntitiesCreationForm
	}
	/**
	 * 	Getter : allTeams 
	 * 	@return : Return the allTeams instance.
	 */
	public List<Team02BO> getAllTeams(){
		return allTeams; // For UC02_EntitiesCreationForm
	}
	
	/**
	 * 	Setter : allTeams 
	 *  @param allTeamsinstance : The instance to set.
	 */
	public void setAllTeams(final List<Team02BO> allTeamsinstance){
		this.allTeams = allTeamsinstance;// For UC02_EntitiesCreationForm
	}
	/**
	 * 	Getter : resultDelete 
	 * 	@return : Return the resultDelete instance.
	 */
	public boolean getResultDelete(){
		return resultDelete; // For UC02_EntitiesCreationForm
	}
	
	/**
	 * 	Setter : resultDelete 
	 *  @param resultDeleteinstance : The instance to set.
	 */
	public void setResultDelete(final boolean resultDeleteinstance){
		this.resultDelete = resultDeleteinstance;// For UC02_EntitiesCreationForm
	}
	/**
	 * 	Getter : teamToCreate 
	 * 	@return : Return the teamToCreate instance.
	 */
	public Team02BO getTeamToCreate(){
		return teamToCreate; // For UC02_EntitiesCreationForm
	}
	
	/**
	 * 	Setter : teamToCreate 
	 *  @param teamToCreateinstance : The instance to set.
	 */
	public void setTeamToCreate(final Team02BO teamToCreateinstance){
		this.teamToCreate = teamToCreateinstance;// For UC02_EntitiesCreationForm
	}
	/**
	 * 	Getter : teamToDelete 
	 * 	@return : Return the teamToDelete instance.
	 */
	public Team02BO getTeamToDelete(){
		return teamToDelete; // For UC02_EntitiesCreationForm
	}
	
	/**
	 * 	Setter : teamToDelete 
	 *  @param teamToDeleteinstance : The instance to set.
	 */
	public void setTeamToDelete(final Team02BO teamToDeleteinstance){
		this.teamToDelete = teamToDeleteinstance;// For UC02_EntitiesCreationForm
	}
/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UC02_EntitiesCreationForm [ "+
ALL_STATES +" = " + allStates +ALL_TEAMS +" = " + allTeams +RESULT_DELETE +" = " + resultDelete +TEAM_TO_CREATE +" = " + teamToCreate +TEAM_TO_DELETE +" = " + teamToDelete + "]";
	}
	
	/**
	 * Method setInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public void setInstanceByInstanceName(final String instanceName, final Object instance) {
		// Set the instance AllStates.
		if(ALL_STATES.equals(instanceName)){// For UC02_EntitiesCreationForm
			this.setAllStates((List)instance); // For UC02_EntitiesCreationForm
		}
				// Set the instance AllTeams.
		if(ALL_TEAMS.equals(instanceName)){// For UC02_EntitiesCreationForm
			this.setAllTeams((List<Team02BO>)instance); // For UC02_EntitiesCreationForm
		}
						// Set the instance TeamToCreate.
		if(TEAM_TO_CREATE.equals(instanceName)){// For UC02_EntitiesCreationForm
			this.setTeamToCreate((Team02BO)instance); // For UC02_EntitiesCreationForm
		}
				// Set the instance TeamToDelete.
		if(TEAM_TO_DELETE.equals(instanceName)){// For UC02_EntitiesCreationForm
			this.setTeamToDelete((Team02BO)instance); // For UC02_EntitiesCreationForm
		}
			}
	
	/**
	 * Method getInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public Object getInstanceByInstanceName(final String instanceName){
		// Temporary object for tmpUC02_EntitiesCreationForm.
		Object tmpUC02_EntitiesCreationForm = null;
		
			
		// Get the instance AllStates for UC02_EntitiesCreationForm.
		if(ALL_STATES.equals(instanceName)){ // For UC02_EntitiesCreationForm
			tmpUC02_EntitiesCreationForm = this.getAllStates(); // For UC02_EntitiesCreationForm
		}
			
		// Get the instance AllTeams for UC02_EntitiesCreationForm.
		if(ALL_TEAMS.equals(instanceName)){ // For UC02_EntitiesCreationForm
			tmpUC02_EntitiesCreationForm = this.getAllTeams(); // For UC02_EntitiesCreationForm
		}
			
		// Get the instance ResultDelete for UC02_EntitiesCreationForm.
		if(RESULT_DELETE.equals(instanceName)){ // For UC02_EntitiesCreationForm
			tmpUC02_EntitiesCreationForm = java.lang.Boolean.valueOf(this.getResultDelete()); // For UC02_EntitiesCreationForm
		}
			
		// Get the instance TeamToCreate for UC02_EntitiesCreationForm.
		if(TEAM_TO_CREATE.equals(instanceName)){ // For UC02_EntitiesCreationForm
			tmpUC02_EntitiesCreationForm = this.getTeamToCreate(); // For UC02_EntitiesCreationForm
		}
			
		// Get the instance TeamToDelete for UC02_EntitiesCreationForm.
		if(TEAM_TO_DELETE.equals(instanceName)){ // For UC02_EntitiesCreationForm
			tmpUC02_EntitiesCreationForm = this.getTeamToDelete(); // For UC02_EntitiesCreationForm
		}
		return tmpUC02_EntitiesCreationForm;// For UC02_EntitiesCreationForm
	}
	
			}
