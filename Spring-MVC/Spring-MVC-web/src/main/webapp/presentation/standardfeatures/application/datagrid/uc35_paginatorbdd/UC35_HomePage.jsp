<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file="/include.jsp"  %>
<html >
<head >
<title  > 
<fmt:message key="cUC35_HomePage_BLU_AGE_DOCUMENTATION"/>
</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"  > 
</meta>
<link href="<c:url value= "/presentation/_style/_style.css"/>" rel="stylesheet" type="text/css"  /> 
<link href="<c:url value= "/presentation/_include/jquery/rd_lib.css"/>" rel="stylesheet" type="text/css"  /> 
<script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/jquery.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/rd_setup.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/rd_lib.js"/>" space="preserve"  ></script><link rel="shortcut icon" href="<c:url value= "/presentation/_style/favicon.ico"/>"  /> 

</head>

<body class=" " > 
<div id="rd_ghost"  > 
<small  > 
<fmt:message key="cUC35_HomePage___CSS_DEACTIVATED"/>
</small>
</div>
<div id="rd_jghost"  > 
<small  > 
<fmt:message key="cUC35_HomePage___JAVASCRIPT_DEACTIVATED"/>
</small>
</div>
<div id="shape"  > 
<div class="ui-layout-center"  > 
<div class="east-north"  > 
	<form:form id="uC35_HomePage" commandName="uC35_HomePageForm"  enctype="application/x-www-form-urlencoded" method="post">
<h2 class="rd_first"  > 
<fmt:message key="cUC35_HomePage_STANDARD_FEATURES__APPLICATIO"/>
</h2>
<a id="lnk_paginatorbdd"				href="<c:url value="/presentation/standardfeatures/application/datagrid/uc35_paginatorbdd/UC35_HomePage/lnk_paginatorbdd.html"/>"
													 shape="rect"  >									 
<fmt:message key="cUC35_HomePage_DISPLAY_PLAYERS"/>
</a>
	</form:form>
	<script language="javascript">
		function btn_click_cExecuteUC35_HomePage(url) {
			UC35_HomePagevar = document.getElementById('uC35_HomePage');
			UC35_HomePagevar.setAttribute('action', url);
			UC35_HomePagevar.submit();
		}
		function btn_click_cSetActionUC35_HomePage(url) {
			UC35_HomePagevar = document.getElementById('uC35_HomePage');
			UC35_HomePagevar.setAttribute('action', url);
		}
	</script>
</div>
<div class="east-center"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/application/datagrid/uc35_paginatorbdd/UC35_HomePagecmp_tabs" />
</div>
</div>
<div class="ui-layout-north"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/application/datagrid/uc35_paginatorbdd/UC35_HomePagecmp_header" />
</div>
<div class="ui-layout-south"  > 
<fmt:message key="cUC35_HomePage___"/>
<span id="rd_jversion"  > 
<fmt:message key="cUC35_HomePage_BLU_AGE_CORPORATION"/>
</span>
</div>
<div class="ui-layout-west"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/application/datagrid/uc35_paginatorbdd/UC35_HomePagecmp_treeview" />
</div>
</div>
</body>
</html>
