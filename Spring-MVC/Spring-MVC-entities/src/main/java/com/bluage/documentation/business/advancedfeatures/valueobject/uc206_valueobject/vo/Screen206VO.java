/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.business.advancedfeatures.valueobject.uc206_valueobject.vo;

import java.util.List;

import com.bluage.documentation.business.AbstractValueObject;
/**
 * Screen206VO
 *
 *
 *
 */
public class Screen206VO
 extends AbstractValueObject {

	/**
	 * Default SUID.
	 */
	private static final long serialVersionUID = -1001745811980615108L;
	
	/**
	 * Used to transfer back and forth the Hibernate optimistic lock.
	 */
	private int version;
	
	/**
	 * Getter for version
	 * @return version
	 */
	public int getVersion() {
		return this.version;
	}
	
	/**
	 * Setter for version
	 * @param int version
	 */
	public void setVersion(int value) {
		this.version = value;
	}
						private List listPosition206
;
	
	/**
	 * Getter for listPosition206.
	 * 
	 *
	 * @return {@link List}
	 */
	public List getListPosition206()
	{
		return this.listPosition206;
	}

	/**
	 * Setter for listPosition206.
	 * 
	 *
	 * @param value: {@link List}
	 */
	public void setListPosition206(List value) 
	{
		this.listPosition206 = value;
	}

	
						private List playerVOs
;
	
	/**
	 * Getter for playerVOs.
	 * 
	 *
	 * @return {@link List}
	 */
	public List getPlayerVOs()
	{
		return this.playerVOs;
	}

	/**
	 * Setter for playerVOs.
	 * 
	 *
	 * @param value: {@link List}
	 */
	public void setPlayerVOs(List value) 
	{
		this.playerVOs = value;
	}

	
						private Player206VO player206VO
;
	
	/**
	 * Getter for player206VO.
	 * 
	 *
	 * @return {@link Player206VO}
	 */
	public Player206VO getPlayer206VO()
	{
		return this.player206VO;
	}

	/**
	 * Setter for player206VO.
	 * 
	 *
	 * @param value: {@link Player206VO}
	 */
	public void setPlayer206VO(Player206VO value) 
	{
		this.player206VO = value;
	}

	
						private Player206VO player206CriteriaVO
;
	
	/**
	 * Getter for player206CriteriaVO.
	 * 
	 *
	 * @return {@link Player206VO}
	 */
	public Player206VO getPlayer206CriteriaVO()
	{
		return this.player206CriteriaVO;
	}

	/**
	 * Setter for player206CriteriaVO.
	 * 
	 *
	 * @param value: {@link Player206VO}
	 */
	public void setPlayer206CriteriaVO(Player206VO value) 
	{
		this.player206CriteriaVO = value;
	}

	
}
