/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.services.bags.uc49_bags;


import java.io.Serializable;
import java.util.List;

import com.bluage.documentation.business.standardfeatures.services.bags.uc49_bags.bos.Player49BO;
import com.bluage.documentation.business.standardfeatures.services.bags.uc49_bags.bos.Team49BO;
import com.netfective.bluage.core.web.AbstractForm;
import com.netfective.bluage.core.web.IForm;

/**
* Class : UC49_BAGSForm
*/
public class UC49_BAGSForm extends AbstractForm implements Serializable, IForm {
	
	/** Constant : serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	//CONSTANT : allPlayers
	private static final String ALL_PLAYERS = "allPlayers";
	//CONSTANT : teamToDisplay
	private static final String TEAM_TO_DISPLAY = "teamToDisplay";
	//CONSTANT : selectedPlayer
	private static final String SELECTED_PLAYER = "selectedPlayer";
	/**
	 * 	Property: allPlayers 
	 */
	private List<Player49BO> allPlayers;
	/**
	 * 	Property: teamToDisplay 
	 */
	private Team49BO teamToDisplay;
	/**
	 * 	Property: selectedPlayer 
	 */
	private Player49BO selectedPlayer;
/**
	 * Default constructor : UC49_BAGSForm
	 */
	public UC49_BAGSForm() {
		super();
		// Initialize : allPlayers
		this.allPlayers = new java.util.ArrayList<com.bluage.documentation.business.standardfeatures.services.bags.uc49_bags.bos.Player49BO>();
		// Initialize : teamToDisplay
		this.teamToDisplay = null;
		// Initialize : selectedPlayer
		this.selectedPlayer = null;
					this.setAlwaysCallPreControllers(true);
	}
	
	/**
	 * 	Getter : allPlayers 
	 * 	@return : Return the allPlayers instance.
	 */
	public List<Player49BO> getAllPlayers(){
		return allPlayers; // For UC49_BAGSForm
	}
	
	/**
	 * 	Setter : allPlayers 
	 *  @param allPlayersinstance : The instance to set.
	 */
	public void setAllPlayers(final List<Player49BO> allPlayersinstance){
		this.allPlayers = allPlayersinstance;// For UC49_BAGSForm
	}
	/**
	 * 	Getter : teamToDisplay 
	 * 	@return : Return the teamToDisplay instance.
	 */
	public Team49BO getTeamToDisplay(){
		return teamToDisplay; // For UC49_BAGSForm
	}
	
	/**
	 * 	Setter : teamToDisplay 
	 *  @param teamToDisplayinstance : The instance to set.
	 */
	public void setTeamToDisplay(final Team49BO teamToDisplayinstance){
		this.teamToDisplay = teamToDisplayinstance;// For UC49_BAGSForm
	}
	/**
	 * 	Getter : selectedPlayer 
	 * 	@return : Return the selectedPlayer instance.
	 */
	public Player49BO getSelectedPlayer(){
		return selectedPlayer; // For UC49_BAGSForm
	}
	
	/**
	 * 	Setter : selectedPlayer 
	 *  @param selectedPlayerinstance : The instance to set.
	 */
	public void setSelectedPlayer(final Player49BO selectedPlayerinstance){
		this.selectedPlayer = selectedPlayerinstance;// For UC49_BAGSForm
	}
/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UC49_BAGSForm [ "+
ALL_PLAYERS +" = " + allPlayers +TEAM_TO_DISPLAY +" = " + teamToDisplay +SELECTED_PLAYER +" = " + selectedPlayer + "]";
	}
	
	/**
	 * Method setInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public void setInstanceByInstanceName(final String instanceName, final Object instance) {
		// Set the instance AllPlayers.
		if(ALL_PLAYERS.equals(instanceName)){// For UC49_BAGSForm
			this.setAllPlayers((List<Player49BO>)instance); // For UC49_BAGSForm
		}
				// Set the instance TeamToDisplay.
		if(TEAM_TO_DISPLAY.equals(instanceName)){// For UC49_BAGSForm
			this.setTeamToDisplay((Team49BO)instance); // For UC49_BAGSForm
		}
				// Set the instance SelectedPlayer.
		if(SELECTED_PLAYER.equals(instanceName)){// For UC49_BAGSForm
			this.setSelectedPlayer((Player49BO)instance); // For UC49_BAGSForm
		}
			}
	
	/**
	 * Method getInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public Object getInstanceByInstanceName(final String instanceName){
		// Temporary object for tmpUC49_BAGSForm.
		Object tmpUC49_BAGSForm = null;
		
			
		// Get the instance AllPlayers for UC49_BAGSForm.
		if(ALL_PLAYERS.equals(instanceName)){ // For UC49_BAGSForm
			tmpUC49_BAGSForm = this.getAllPlayers(); // For UC49_BAGSForm
		}
			
		// Get the instance TeamToDisplay for UC49_BAGSForm.
		if(TEAM_TO_DISPLAY.equals(instanceName)){ // For UC49_BAGSForm
			tmpUC49_BAGSForm = this.getTeamToDisplay(); // For UC49_BAGSForm
		}
			
		// Get the instance SelectedPlayer for UC49_BAGSForm.
		if(SELECTED_PLAYER.equals(instanceName)){ // For UC49_BAGSForm
			tmpUC49_BAGSForm = this.getSelectedPlayer(); // For UC49_BAGSForm
		}
		return tmpUC49_BAGSForm;// For UC49_BAGSForm
	}
	
			}
