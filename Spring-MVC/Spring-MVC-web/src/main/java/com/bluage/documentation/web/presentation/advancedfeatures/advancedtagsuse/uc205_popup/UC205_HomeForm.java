/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.advancedfeatures.advancedtagsuse.uc205_popup;


import java.io.Serializable;
import java.util.List;

import com.netfective.bluage.core.web.AbstractForm;
import com.netfective.bluage.core.web.IForm;

/**
* Class : UC205_HomeForm
*/
public class UC205_HomeForm extends AbstractForm implements Serializable, IForm {
	
	/** Constant : serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	//CONSTANT : allPlayers
	private static final String ALL_PLAYERS = "allPlayers";
	//CONSTANT : cp_traitementShowPopUp
	private static final String CP_TRAITEMENT_SHOW_POP_UP = "cp_traitementShowPopUp";
	/**
	 * 	Property: allPlayers 
	 */
	private List allPlayers;
	/**
	 * 	Property: cp_traitementShowPopUp 
	 */
	private Boolean cp_traitementShowPopUp = Boolean.FALSE;
/**
	 * Default constructor : UC205_HomeForm
	 */
	public UC205_HomeForm() {
		super();
		// Initialize : allPlayers
		this.allPlayers = new java.util.ArrayList();
			this.setAlwaysCallPreControllers(true);
	}
	
	/**
	 * 	Getter : allPlayers 
	 * 	@return : Return the allPlayers instance.
	 */
	public List getAllPlayers(){
		return allPlayers; // For UC205_HomeForm
	}
	
	/**
	 * 	Setter : allPlayers 
	 *  @param allPlayersinstance : The instance to set.
	 */
	public void setAllPlayers(final List allPlayersinstance){
		this.allPlayers = allPlayersinstance;// For UC205_HomeForm
	}
	/**
	 * 	Getter : cp_traitementShowPopUp 
	 * 	@return: cp_traitementShowPopUpinstance
	 */
	public Boolean getCp_traitementShowPopUp(){
		return this.cp_traitementShowPopUp; 
	}
	
	/**
	 * 	Setter : cp_traitementShowPopUp 
	 * 	@param : cp_traitementShowPopUpinstance : the instance to set
	 */
	public void setCp_traitementShowPopUp(final Boolean cp_traitementShowPopUp){
		this.cp_traitementShowPopUp = cp_traitementShowPopUp; 
	}
/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UC205_HomeForm [ "+
ALL_PLAYERS +" = " + allPlayers + "]";
	}
	
	/**
	 * Method setInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public void setInstanceByInstanceName(final String instanceName, final Object instance) {
		// Set the instance AllPlayers.
		if(ALL_PLAYERS.equals(instanceName)){// For UC205_HomeForm
			this.setAllPlayers((List)instance); // For UC205_HomeForm
		}
				if(CP_TRAITEMENT_SHOW_POP_UP.equals(instanceName)){ // For UC205_HomeForm
			this.setCp_traitementShowPopUp((Boolean)instance); // For UC205_HomeForm
		}
	}
	
	/**
	 * Method getInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public Object getInstanceByInstanceName(final String instanceName){
		// Temporary object for tmpUC205_HomeForm.
		Object tmpUC205_HomeForm = null;
		
			
		// Get the instance AllPlayers for UC205_HomeForm.
		if(ALL_PLAYERS.equals(instanceName)){ // For UC205_HomeForm
			tmpUC205_HomeForm = this.getAllPlayers(); // For UC205_HomeForm
		}
		if(CP_TRAITEMENT_SHOW_POP_UP.equals(instanceName)){// For UC205_HomeForm
			tmpUC205_HomeForm = this.getCp_traitementShowPopUp();// For UC205_HomeForm
		}
		return tmpUC205_HomeForm;// For UC205_HomeForm
	}
	
	}
