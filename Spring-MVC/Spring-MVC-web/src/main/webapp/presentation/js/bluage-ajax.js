
BluageAjax.defaultInstanceName = "default";
BluageAjax.ErrorMassage = "BluageAjax error:\n XMLHttpRequest HTTP Error code:";

// constructor;
function BluageAjax() {

    this.id = BluageAjax.defaultInstanceName;
    this.formName = null;
    this.notSupported = false;
    this.delayBeforeContentUpdate = true;
    this.delayInMillis = 100;

    if (window.XMLHttpRequest) {
        this.req = new XMLHttpRequest();
    } else if (window.ActiveXObject) {
        try {
            this.req = new ActiveXObject("Msxml2.XMLHTTP");
        } catch(e) {
            try {
                this.req = new ActiveXObject("Microsoft.XMLHTTP");
            } catch(e1) {
                this.notSupported = true;
            }
        }
    }

    if (this.req == null || typeof this.req == "undefined"){
        this.notSupported = true;
	}
}

/**
* Stores substitutes SubmitButton names in to redo sustitution if a button was eventually inside a refresh zone.
*/
BluageAjax.prototype.substitutedSubmitButtons = new Array();
BluageAjax.prototype.substitutedSubmitButtonsInfo = new Object();

/**
* Returns a Form object that corresponds to formName property of this BluageAjax class instance.
*/
BluageAjax.prototype.findForm = function () {
    var form;
    if (this.formName != null){
        form = document.forms[this.formName];
    }else if (document.forms.length > 0){
        form = document.forms[0];
    }

    if (typeof form != "object"){
        alert("BluageAjax error: Form with name [" + this.formName + "] not found");
    }
    return form;
}


/**
* Binds this instance to window object using "BluageAjax."+this.id as a key.
*/
BluageAjax.prototype.bindById = function () {
    var key = "BluageAjax." + this.id;
    window[key] = this;
}

/**
* Finds an instance by id.
*/
BluageAjax.prototype.findInstance = function(id) {
    var key = "BluageAjax." + id;
    return window[key];
}

/**
* This function is used to submit all form fields by AJAX request to the server.
* If the form is submited with &lt;input type=submit|image&gt;, submitButton should be a reference to the DHTML object. Otherwise - undefined.
*/
BluageAjax.prototype.submitAndReload = function(zones, formName, additionalPostData, submitButton) {
	this.getZonesToReload = function() {
		if(zones==null){
			return "";
		}
		return zones;
	}
	this.formName=formName;
	this.submitAJAX(additionalPostData, submitButton);
}

/**
* This function is used to submit all form fields by AJAX request to the server.
* If the form is submited with &lt;input type=submit|image&gt;, submitButton should be a reference to the DHTML object. Otherwise - undefined.
*/
BluageAjax.prototype.submitURL = function(url, additionalPostData, submitButton, options) {

	if(options!=null && options.form != null) {
		this.formName = options.form;
	}
	var form = this.findForm();
    form.action = url;
	this.submitAJAX(additionalPostData, options);
}

/**
* This function is used to submit all form fields by AJAX request to the server.
* If the form is submited with &lt;input type=submit|image&gt;, submitButton should be a reference to the DHTML object. Otherwise - undefined.
*/
BluageAjax.prototype.submitAJAX = function(additionalPostData, options) {
    
    this.bindById();

    var form = this.findForm();

    var actionAttrNode = form.attributes["action"];
    var url = null;
    if( actionAttrNode != null){
    	url = actionAttrNode.nodeValue;
    }
    
    if ((url == null) || (url == "")){
        url = location.href;
    }
    var pos = url.indexOf("#");
        if (pos!=-1){
            url = url.substring(0,pos);
        }
        if ((url == null) || (url == "")){
            url = location.href;
        }
        pos = url.indexOf("#");
        
        if (pos!=-1){
            url = url.substring(0,pos);
        }

    var zones = null;
    
    if(options != null){
    	zones = options.zones;
    }

    this.dropPreviousRequest();

    this.req.open("POST", url, true);
    this.req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");

    var postData = this.preparePostData(additionalPostData);    
    
    if (zones != "" && zones != null && typeof zones != "undefined"){
        postData = 'aazones=' + encodeURIComponent(zones) + "&" + postData;
    }

    this.sendPreparedRequest(postData);
}
/**
* sends a GET request to the server.
*/
BluageAjax.prototype.getAJAX = function(url, zonesToRefresh) {
    
    if (this.notSupported){
        return this.onGetAjaxNotSupported(url);
    }

    this.bindById();

    if (zonesToRefresh == null || typeof zonesToRefresh == "undefined"){
        zonesToRefresh = "";
    }
    var urlDependentZones = this.getZonesToReload(url);
    if (urlDependentZones == null) {
        location.href = url;
        return;
    }

    if (urlDependentZones.length != 0){
        zonesToRefresh += "," + urlDependentZones;
    }

    this.dropPreviousRequest();

    url += (url.indexOf("?") != -1) ? "&" : "?";

    url += "a4srequest=true&aa_rand=" + Math.random();
    // avoid caching

    if (zonesToRefresh != null && zonesToRefresh != ""){
        url += '&aazones=' + encodeURIComponent(zonesToRefresh);
    }
    

    this.req.open("GET", url, true);

    this.sendPreparedRequest("");
}



/**
* @private
*/
BluageAjax.prototype.sendPreparedRequest = function (postData) {
    this.req.setRequestHeader("Accept", "text/xml");

    var callbackKey = this.id + "_callbackFunction";
    if (typeof window[callbackKey] == "undefined"){
        window[callbackKey] = new Function("bluageAjax.findInstance(\"" + this.id + "\").callback(); ");
    }
    this.req.onreadystatechange = window[callbackKey];

    this.showLoadingMessage();

    this.req.send(postData);

    this.onRequestSent();
}
/**
* Used internally by BluageAjax. Aborts previous request if not completed.
*/
BluageAjax.prototype.dropPreviousRequest = function() {
    if (this.req.readyState != 0 && this.req.readyState != 4) {
        // abort previous request if not completed
        this.req.abort();
        this.handlePrevousRequestAborted();
    }
}

/**
* Internally used to prepare Post data.
* If the form is submited with &lt;input type=submit|image&gt;, submitButton is a reference to the DHTML object. Otherwise - undefined.
*/
BluageAjax.prototype.preparePostData = function(submitButton) {
    var form = this.findForm();
    var result = "a4srequest=true";
    
    for (var i = 0; i < form.elements.length; i++) {
        var el = form.elements[i];
        if (el.tagName.toLowerCase() == "select") {
            for (var j = 0; j < el.options.length; j++) {
                var op = el.options[j];
                if (op.selected){
                    result += "&" + encodeURIComponent(el.name) + "=" + encodeURIComponent(op.value);
                }
            }
        } else if (el.tagName.toLowerCase() == "textarea") {
            result += "&" + encodeURIComponent(el.name) + "=" + encodeURIComponent(el.value);
        } else if (el.tagName.toLowerCase() == "input") {
            if (el.type.toLowerCase() == "checkbox" || el.type.toLowerCase() == "radio") {
                if (el.checked){
                    result += "&" + encodeURIComponent(el.name) + "=" + encodeURIComponent(el.value);
                }
            } else if (el.type.toLowerCase() == "submit") {
                if (el == submitButton) // is "el" the submit button that fired the form submit?
                    result += "&" + encodeURIComponent(el.name) + "=" + encodeURIComponent(el.value);
            } else if (el.type.toLowerCase() != "button") {
                result += "&" + encodeURIComponent(el.name) + "=" + encodeURIComponent(el.value);
            }
        }
    }
    if (typeof submitButton != 'undefined' && submitButton != null && typeof submitButton.type != 'undefined' && submitButton.type.toLowerCase() == "image") {
        if (submitButton.name == null || submitButton.name == "" || typeof submitButton.name == "undefined"){
            result += "&x=1&y=1"; // .x and .y coordinates calculation is not supported.
        }else{
            result += "&" + encodeURIComponent(submitButton.name) + ".x=1&" +
                      encodeURIComponent(submitButton.name) + ".y=1";
        }
    }
    return result;
}

/**
* Pauses the thread of execution for the specified number of milliseconds
* @private
*/
function delay(millis) {
    date = new Date();
    var curDate = null;
    do {
        curDate = new Date();
    }
    while (curDate - date < millis);
}

/**
* A callback. internally used
*/
BluageAjax.prototype.callback = function() {

    if (this.req.readyState == 4) {

        this.onBeforeResponseProcessing();
        this.hideLoadingMessage();

        if (this.req.status == 200) {

            if (this.req.getResponseHeader('content-type').toLowerCase().substring(0, 8) != 'text/xml'){
                alert("BluageAjax error : content-type in not text/xml : [" + this.req.getResponseHeader('content-type') + "]");
            }
            var docs = this.req.responseXML.getElementsByTagName("document");
            var redirects = this.req.responseXML.getElementsByTagName("redirect");
            var zones = this.req.responseXML.getElementsByTagName("zone");
            var exceptions = this.req.responseXML.getElementsByTagName("exception");
            var scripts = this.req.responseXML.getElementsByTagName("script");
            var images = this.req.responseXML.getElementsByTagName("image");

            if (redirects.length != 0) {
                var newURL = redirects[0].firstChild.data;
                location.href = newURL;
            }
            if (docs.length != 0) {
                var newContent = docs[0].firstChild.data;

                //cleanup ressources
                delete this.req;

                document.close();
                document.write(newContent);
                document.close();
            }

            if (images.length != 0) {
                var preLoad = new Array(images.length);
                for (var i = 0; i < images.length; i++) {
                    var img = images[i].firstChild;
                    if (img != null) {
                        preLoad[i] = new Image();
                        preLoad[i].src = img.data;
                    }
                }
                if (this.delayBeforeContentUpdate) {
                    delay(this.delayInMillis);
                }
            }

            if (zones.length != 0) {
                for (var i = 0; i < zones.length; i++) {
                    var zoneNode = zones[i];

                    var name = zoneNode.getAttribute("name");
                    var id = zoneNode.getAttribute("id");

                    var html = "";

                    for (var childIndex = 0; childIndex < zoneNode.childNodes.length; childIndex++) {
                        html += zoneNode.childNodes[childIndex].data
                    }

                    var zoneHolder = name!=null?
                                     document.getElementById(name):
                                     document.getElementById(id);

                    if (zoneHolder != null && typeof(zoneHolder) != "undefined") {
                        zoneHolder.innerHTML = html;
                    }

                }
            }
            if (exceptions.length != 0) {
                var e = exceptions[0];
                var type = e.getAttribute("type");
                var stackTrace = e.firstChild.data;
                this.handleException(type, stackTrace);
            }

            if (scripts.length != 0) {
            	//#PROTOTYPE $$
                for (var valueToIterate = 0; valueToIterate < scripts.length; valueToIterate++) {
                    // use $$$$i variable to avoid collision with "i" inside user script
                    var script = scripts[valueToIterate].firstChild;
                    if (script != null) {
                        script = script.data;
                        if (script.indexOf("document.write") != -1) {
                            this.handleException("document.write", "This script contains document.write(), which is not compatible with BluageAjax : \n\n" + script);
                        } else {

                            eval("var aaInstanceId = \""+this.id+"\"; \n"+script);
                        }
                    }
                }

                var globals = this.getGlobalScriptsDeclarationsList(script);
                if (globals != null){
                    for (var valueToIterate2 in globals) {
                        var objName = globals[valueToIterate2];
                        try {
                            window[objName] = eval(objName);
                        } catch(e) {
                        }
                    }
                }
            }

        } else {
            if (this.req.status != 0){
                this.showXmlHttpError(this.req.status);
            }
        }
        this.restoreSubstitutedSubmitButtons();
        this.onAfterResponseProcessing();

    }


}

/**
*  Default sample loading message show function. Overrride it if you like.
*/
BluageAjax.prototype.showLoadingMessage = function() {

    var div = document.getElementById("AA_" + this.id + "_loading_div");
    if (div == null) {
        div = document.createElement("DIV");

        document.body.appendChild(div);
        div.id = "AA_" + this.id + "_loading_div";

        div.innerHTML = "&nbsp;Loading...";
        div.style.position = "absolute";
        div.style.border = "1 solid black";
        div.style.color = "white";
        div.style.backgroundColor = "blue";
        div.style.width = "100px";
        div.style.heigth = "50px";
        div.style.fontFamily = "Arial, Helvetica, sans-serif";
        div.style.fontWeight = "bold";
        div.style.fontSize = "11px";
    }
    div.style.top = document.body.scrollTop + "px";
    div.style.left = (document.body.offsetWidth - 100 - (document.all?20:0)) + "px";

    div.style.display = "";
}

/**
*  Default sample loading message hide function. Overrride it if you like.
*/
BluageAjax.prototype.hideLoadingMessage = function() {
    var div = document.getElementById("AA_" + this.id + "_loading_div");
    if (div != null){
        div.style.display = "none";
    }

}

/**
* This function is used to facilitatte BluageAjax integration with existing projects/frameworks.
* It substitutes default Form.sumbit().
* The new implementation calls BluageAjax.isFormSubmitByAjax() function to find out if the form
* should be submitted in traditional way or by BluageAjax.
*/
BluageAjax.prototype.substituteFormSubmitFunction = function() {
    if (this.notSupported){
        return;
    }

    this.bindById();

    var form = this.findForm();

    form.submit_old = form.submit;
    var code = "var ajax = bluageAjax.findInstance(\"" + this.id + "\"); " +
               "if (typeof ajax !='object' || ! ajax.isFormSubmitByAjax() ) " +
               "ajax.findForm().submit_old();" +
               " else " +
               "ajax.submitAJAX();"
    form.submit = new Function(code);

}
/**
* Substitutes the default behavior of &lt;input type=submit|image&gt; to submit the form via BluageAjax.
*
* @param {boolean} indicates if existing onClick handlers should be preserved.
* If keepExistingOnClickHandler==true,
* Existing handler will be called first if it returns false, or if event.returnValue==false, BluageAjax will not
* continue form submission.
* If keepExistingOnClickHandler==false or undefines, existing onClick event handlers will be replaced.
*
* @param {Array} list of submitButtons and submitImages names. If the parameter is omitted or undefined,
* all elements will be processed
*/
BluageAjax.prototype.substituteSubmitButtonsBehavior = function (keepExistingOnClickHandler, elements) {
    if (this.notSupported)
        return;

    var form = this.findForm();
    if (elements == null || typeof elements == "undefined") { // process all elements
        elements = new Array();
        for (var i = 0; i < form.elements.length; i++) {
            elements.push(form.elements[i]);
        }

        var inputs = document.getElementsByTagName("input");
        for (var i = 0; i < inputs.length; i++) {
            var input = inputs[i];
            if (input.type != null && typeof input.type != "undefined" &&
                input.type.toLowerCase() == "image" && input.form == form) {
                elements.push(input);
            }
        }

        for (var i = 0; i < elements.length; i++) {
            var el = elements[i];
            if (el.tagName.toLowerCase() == "input" && (el.type.toLowerCase() == "submit"
                    || el.type.toLowerCase() == "image")) {
                this.substituteSubmitBehavior(el, keepExistingOnClickHandler);
            }
        }
    } else { //process only specified elements
        for (var i = 0; i < elements.length; i++) {
            var el = elements[i];
            if (el == null){
                continue;
            }

            if (typeof el != "object"){
                el = form.elements[el];
            }

            if (typeof el != "undefined") {
                if (el.tagName.toLowerCase() == "input" && (el.type.toLowerCase() == "submit"
                        || el.type.toLowerCase() == "image")){
                    this.substituteSubmitBehavior(el, keepExistingOnClickHandler);
                }
            }
        }
    }

}
/**
* Performs a single element behavior substitution
*
* @private
*/
BluageAjax.prototype.substituteSubmitBehavior = function (el, keepExistingOnClickHandler) {

    var inList = false;
    for (var i = 0; i < this.substitutedSubmitButtons.length; i++) {
        var btnName = this.substitutedSubmitButtons[i];
        if (btnName == el.name) {
            inList = true;
            break;
        }
    }
    if (!inList){
        this.substitutedSubmitButtons.push(el.name);
    }

    this.substitutedSubmitButtonsInfo[el.name] = keepExistingOnClickHandler;

    if (keepExistingOnClickHandler && (typeof el.onclick != "undefined") && ( el.onclick != null) && ( el.onclick != "")) {
        el.AA_old_onclick = el.onclick;
    }

    el.onclick = handleSubmitButtonClick;
    el.ajaxAnywhereId = this.id;
}

/**
*
* @private
*/
BluageAjax.prototype.restoreSubstitutedSubmitButtons = function() {
    if (this.substitutedSubmitButtons.length == 0)
        return;

    var form = this.findForm();

    for (var i = 0; i < this.substitutedSubmitButtons.length; i++) {
        var name = this.substitutedSubmitButtons[i];
        var el = form.elements[name];
        if (el != null && typeof el != "undefined") {
            if (el.onclick != handleSubmitButtonClick) {
                var keepExistingOnClickHandler = this.substitutedSubmitButtonsInfo[el.name];
                this.substituteSubmitBehavior(el, keepExistingOnClickHandler);
            }
        } else {
            //input type=image
            if (name != null && typeof name != "undefined" && name.length != 0) {
                var elements = document.getElementsByName(name);
                if (elements != null){
                    for (var j = 0; j < elements.length; j++) {
                        el = elements[j];
                        if (el != null && typeof el != "undefined"
                                && el.tagName.toLowerCase() == "input"
                                && typeof el.type != "undefined" && el.type.toLowerCase() == "image") {
                            if (el.onclick != handleSubmitButtonClick) {
                                var keepExistingOnClickHandler = this.substitutedSubmitButtonsInfo[el.name];
                                this.substituteSubmitBehavior(el, keepExistingOnClickHandler);
                            }
                        }
                    }
                }
            }
        }
    }
}

/**
* @private
*/
function handleSubmitButtonClick(_event) {

    if (typeof this.AA_old_onclick != "undefined") {
        if (false == this.AA_old_onclick(_event)){
            return false;
        }
        if (typeof window.event != "undefined"){
            if (window.event.returnValue == false){
                return false;
            }
        }
    }
    var onsubmit = this.form.onsubmit;
    if (typeof onsubmit == "function") {
        if (false == onsubmit(_event)){
            return false;
        }
        if (typeof window.event != "undefined"){
            if (window.event.returnValue == false){
                return false;
            }
        }
    }
    bluageAjax.findInstance(this.ajaxAnywhereId).submitAJAX('', this);

    return false;
}
/**
* Override this function if you use BluageAjax.substituteFormSubmitFunction() to
* dynamically inform BluageAjax of the method you want to use for the form submission.
*/
BluageAjax.prototype.isFormSubmitByAjax = function () {
    return true;
}

/**
* Some browsers (notably IE) do not load images from thier cache when content is updated using
* innerHTML. As a result, each image is re-requested from the server even though the image exists
* in the cache. To work around this issue, BluageAjax preloads images present in the new content
* and intrduces a brief dely (default of 100 milleseconds) before calling innerHTML.
* See http://support.microsoft.com/default.aspx?scid=kb;en-us;319546 for further details.
* This function can be used to change this behaviour.
* @param (boolean) isDelay
*/
BluageAjax.prototype.setDelayBeforeLoad = function (isDelay) {
    this.delayBeforeContentUpdate = isDelay;
}

/**
* Returns the current delay behavior.
*/
BluageAjax.prototype.isDelayBeforeLoad = function () {
    return this.delayBeforeContentUpdate;
}

/**
* Sets the delay period in milliseconds. The default delay is 100 milliseconds.
* @param (int) delayMillis
*/
BluageAjax.prototype.setDelayTime = function (delayMillis) {
    this.delayInMillis = delayMillis;
}

/**
* Returns the delay period in milliseconds.
*/
BluageAjax.prototype.getDelayTime = function () {
    return this.delayInMillis;
}

/**
*   If an exception is throws on the server-side during AJAX request, it will be processed
* by this function. The default implementation is alert(stackTrace);
* Override it if you need.
*/
BluageAjax.prototype.handleException = function(type, details) {
    alert(details);
}
/**
*   If an HTTP Error code returned during AJAX request, it will be processed
* by this function. The default implementation is alert(code);
* Override it if you need.
*/
BluageAjax.prototype.showXmlHttpError = function(code) {
    alert( BluageAjax.ErrorMassage + code);
}

/**
* Override it if you need.
*/
BluageAjax.prototype.handlePrevousRequestAborted = function() {
    // Only displayed on devmode.
	//alert("BluageAjax default error handler. INFO: previous AJAX request dropped")
}


/**
*   If the HTML received in responce to AJAX request contains JavaScript that defines new
* functions/variables, they must be propagated to the proper context. Override this method
* to return the Array of function/variable names.
*/
BluageAjax.prototype.getGlobalScriptsDeclarationsList = function(script) {
    return null;
}


/**
* Override this method to implement a custom action
*/
BluageAjax.prototype.onRequestSent = function () {
};
/**
* Override this method to implement a custom action
*/
BluageAjax.prototype.onBeforeResponseProcessing = function () {
};
/**
* Override this method to implement a custom action
*/
BluageAjax.prototype.onAfterResponseProcessing = function () {
};

/**
* Provides a default implementation from graceful degradation for getAJAX()
* calls location.href=url if XMLHttpRequest is unavailable, reloading the entire page .
*/
BluageAjax.prototype.onGetAjaxNotSupported = function (url) {
    location.href = url;
    return false;
};

/**
* submit the form in tradiditional way :
* @private
*/

BluageAjax.prototype.submitOld = function (form,submitButton){
    var submitHolder = null;
    if (submitButton!=null && typeof submitButton!="undefined"){
        submitHolder = document.createElement("input");
        submitHolder.setAttribute("type","hidden");
        submitHolder.setAttribute("name",submitButton.name);
        submitHolder.setAttribute("value",submitButton.value);
        form.appendChild(submitHolder);
    }

    if (typeof form.submit_old == "undefined"){
        form.submit();
    }
    else{
        form.submit_old();
    }

    if (submitButton!=null ){
        form.removeChild(submitHolder);
    }
}

// default instance.
bluageAjax = new BluageAjax();
bluageAjax.bindById();