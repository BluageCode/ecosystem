/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.service.standardfeatures.application.datagrid.uc37_collectionorder.daofinder;


import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateTemplate;

import com.bluage.documentation.business.standardfeatures.application.datagrid.uc37_collectionorder.bos.Team37BO;
import com.netfective.bluage.core.exception.ApplicationException;


/**
 * This class represents query invocation methods implementation
 * for ServiceCollectionOrder service.
 *
 * @see com.bluage.documentation.service.standardfeatures.application.datagrid.uc37_collectionorder.ServiceCollectionOrder
 */
 
@Named ("com.bluage.documentation.service.standardfeatures.application.datagrid.uc37_collectionorder.daofinder.ServiceCollectionOrderDAOMethodFinderImpl")
public class ServiceCollectionOrderDAOMethodFinderImpl {

	// LOGGER for ServiceCollectionOrderDAOMethodFinderImpl
	private static final Logger LOGGER = Logger.getLogger(ServiceCollectionOrderDAOMethodFinderImpl.class);
	
	/**
	* Hibernate Template (injected)
	*/
	@Inject
	private HibernateTemplate hibernateTemplate;

	
	/**
	 * setter for HibernateTemplate
	 * @param value : {@link HibernateTemplate}
	 */
	public void setHibernateTemplate( HibernateTemplate value )
	{
		hibernateTemplate = value;
	}
	

	/**    
	 * HQL operation.
	 * @param team {@link Team37BO}
		 * @return {@link Team37BO}
	 */
	public Team37BO loadTeamWithPlayers(Team37BO team )
 throws ApplicationException {
Session session = hibernateTemplate.getSessionFactory().getCurrentSession();
			return 
				privateLoadTeamWithPlayers(team, session);
	}

	
	/**    
	 * private delegation for HQL operation.
	 * @param team {@link Team37BO}
		 * @return {@link Team37BO}
	 */
	private Team37BO privateLoadTeamWithPlayers(Team37BO team , Session session)
 throws ApplicationException {
			Query query = session.createQuery(" SELECT t FROM Team37BO t JOIN FETCH t.player37s s WHERE t = :team ");

			query.setParameter( "team", team);
									Team37BO returnValue = null;
		try {
			Object result = query.uniqueResult();
			if( result != null ) {
				returnValue = ( com.bluage.documentation.business.standardfeatures.application.datagrid.uc37_collectionorder.bos.Team37BO )result;
			}				
		} catch(HibernateException ex) {
			LOGGER.error("An error occured while excecuting the HQL operation : com.bluage.documentation.service.standardfeatures.application.datagrid.uc37_collectionorder.loadTeamWithPlayers");
			throw new ApplicationException(ex);
		}
		return returnValue;
	}

	




}
