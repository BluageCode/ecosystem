/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.application.navigation.uc28_packagescope.package_two;


import java.io.Serializable;
import java.util.List;

import com.bluage.documentation.business.standardfeatures.application.navigation.uc28_packagescope.bos.State28BO;
import com.netfective.bluage.core.web.AbstractForm;
import com.netfective.bluage.core.web.IForm;

/**
* Class : UC28_StatesForm
*/
public class UC28_StatesForm extends AbstractForm implements Serializable, IForm {
	
	/** Constant : serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	//CONSTANT : allStates
	private static final String ALL_STATES = "allStates";
	//CONSTANT : allTeams
	private static final String ALL_TEAMS = "allTeams";
	//CONSTANT : allPlayers
	private static final String ALL_PLAYERS = "allPlayers";
	/**
	 * 	Property: allStates 
	 */
	private List<State28BO> allStates;
	/**
	 * 	Property: allTeams 
	 */
	private List allTeams;
	/**
	 * 	Property: allPlayers 
	 */
	private List allPlayers;
/**
	 * Default constructor : UC28_StatesForm
	 */
	public UC28_StatesForm() {
		super();
		// Initialize : allStates
		this.allStates = new java.util.ArrayList<com.bluage.documentation.business.standardfeatures.application.navigation.uc28_packagescope.bos.State28BO>();
		// Initialize : allTeams
		this.allTeams = null;
		// Initialize : allPlayers
		this.allPlayers = null;
					this.setAlwaysCallPreControllers(true);
	}
	
	/**
	 * 	Getter : allStates 
	 * 	@return : Return the allStates instance.
	 */
	public List<State28BO> getAllStates(){
		return allStates; // For UC28_StatesForm
	}
	
	/**
	 * 	Setter : allStates 
	 *  @param allStatesinstance : The instance to set.
	 */
	public void setAllStates(final List<State28BO> allStatesinstance){
		this.allStates = allStatesinstance;// For UC28_StatesForm
	}
	/**
	 * 	Getter : allTeams 
	 * 	@return : Return the allTeams instance.
	 */
	public List getAllTeams(){
		return allTeams; // For UC28_StatesForm
	}
	
	/**
	 * 	Setter : allTeams 
	 *  @param allTeamsinstance : The instance to set.
	 */
	public void setAllTeams(final List allTeamsinstance){
		this.allTeams = allTeamsinstance;// For UC28_StatesForm
	}
	/**
	 * 	Getter : allPlayers 
	 * 	@return : Return the allPlayers instance.
	 */
	public List getAllPlayers(){
		return allPlayers; // For UC28_StatesForm
	}
	
	/**
	 * 	Setter : allPlayers 
	 *  @param allPlayersinstance : The instance to set.
	 */
	public void setAllPlayers(final List allPlayersinstance){
		this.allPlayers = allPlayersinstance;// For UC28_StatesForm
	}
/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UC28_StatesForm [ "+
ALL_STATES +" = " + allStates +ALL_TEAMS +" = " + allTeams +ALL_PLAYERS +" = " + allPlayers + "]";
	}
	
	/**
	 * Method setInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public void setInstanceByInstanceName(final String instanceName, final Object instance) {
		// Set the instance AllStates.
		if(ALL_STATES.equals(instanceName)){// For UC28_StatesForm
			this.setAllStates((List<State28BO>)instance); // For UC28_StatesForm
		}
				// Set the instance AllTeams.
		if(ALL_TEAMS.equals(instanceName)){// For UC28_StatesForm
			this.setAllTeams((List)instance); // For UC28_StatesForm
		}
				// Set the instance AllPlayers.
		if(ALL_PLAYERS.equals(instanceName)){// For UC28_StatesForm
			this.setAllPlayers((List)instance); // For UC28_StatesForm
		}
			}
	
	/**
	 * Method getInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public Object getInstanceByInstanceName(final String instanceName){
		// Temporary object for tmpUC28_StatesForm.
		Object tmpUC28_StatesForm = null;
		
			
		// Get the instance AllStates for UC28_StatesForm.
		if(ALL_STATES.equals(instanceName)){ // For UC28_StatesForm
			tmpUC28_StatesForm = this.getAllStates(); // For UC28_StatesForm
		}
			
		// Get the instance AllTeams for UC28_StatesForm.
		if(ALL_TEAMS.equals(instanceName)){ // For UC28_StatesForm
			tmpUC28_StatesForm = this.getAllTeams(); // For UC28_StatesForm
		}
			
		// Get the instance AllPlayers for UC28_StatesForm.
		if(ALL_PLAYERS.equals(instanceName)){ // For UC28_StatesForm
			tmpUC28_StatesForm = this.getAllPlayers(); // For UC28_StatesForm
		}
		return tmpUC28_StatesForm;// For UC28_StatesForm
	}
	
			}
