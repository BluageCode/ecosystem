/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.services.process.uc52_conditional ;


import org.apache.log4j.Logger;
import org.springframework.validation.Errors;

import com.netfective.bluage.core.validator.AbstractValidator;
/**
* Class :UC52_ConditionalValidator
*/
public class UC52_ConditionalValidator extends AbstractValidator{
	
	// LOGGER for the class UC52_ConditionalValidator
	private static final Logger LOGGER = Logger.getLogger( UC52_ConditionalValidator.class);
	
	
	/**
	* Operation validate for UC52_ConditionalForm
	* @param obj : the current form (UC52_ConditionalForm)
	* @param errors : The spring errors to return for the form UC52_ConditionalForm
	*/
	public void validate(final Object obj,final Errors errors){
		LOGGER.info("Beginning method : validate"); // UC52_ConditionalValidator
		UC52_ConditionalForm cUC52_ConditionalForm = (UC52_ConditionalForm)obj; // UC52_ConditionalValidator
		LOGGER.info("Ending method : validate the form "+ cUC52_ConditionalForm.getClass().getName()); // UC52_ConditionalValidator
	}

	/**
	* Method to implements to use spring validators (UC52_ConditionalForm)
	* @param aClass : Class for the form UC52_ConditionalForm
	* @return boolean
	*/
	public boolean supports(final Class aClass){
		return (new UC52_ConditionalForm()).getClass().equals(aClass); // UC52_ConditionalValidator
	}
}
