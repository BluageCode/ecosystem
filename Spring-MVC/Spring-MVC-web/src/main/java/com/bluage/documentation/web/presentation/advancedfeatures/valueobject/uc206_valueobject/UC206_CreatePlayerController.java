/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.advancedfeatures.valueobject.uc206_valueobject ;

// Import declaration.
// Java imports.
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.bluage.documentation.business.advancedfeatures.valueobject.uc206_valueobject.vo.Screen206VO;
import com.bluage.documentation.service.advancedfeatures.valueobject.uc206_valueobject.ServiceScreen206;
import com.netfective.bluage.core.exception.ApplicationException;
import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;
import com.netfective.bluage.core.web.IForm;

/**
 * Class : UC206_CreatePlayerController
 */
@Controller
@Scope("prototype")
@SessionAttributes("uC206_CreatePlayerForm")
@RequestMapping(value= "/presentation/advancedfeatures/valueobject/uc206_valueobject")
public class UC206_CreatePlayerController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC206_CreatePlayerController.class);
	
	//Current form name
	private static final String U_C206__CREATE_PLAYER_FORM = "uC206_CreatePlayerForm";

		//CONSTANT: listPosition206
	private static final String LIST_POSITION206 = "listPosition206";
				//CONSTANT: screen206
	private static final String SCREEN206 = "screen206";
	
	/**
	 * Property:customDateEditorsUC206_CreatePlayerController
	 */
	final private Map<String,CustomDateEditor> customDateEditorsUC206_CreatePlayerController = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:uC206_CreatePlayerValidator
	 */
	final private UC206_CreatePlayerValidator uC206_CreatePlayerValidator = new UC206_CreatePlayerValidator();
	
	/**
	 * Service declaration : serviceScreen206.
	 */
	@Autowired
	private ServiceScreen206 serviceScreen206;
	
	
	// Initialise all the instances for UC206_CreatePlayerController
			// Initialize the instance listPosition206 for the state lnk_listPlayer
		private List listPosition206; // Initialize the instance listPosition206 for UC206_CreatePlayerController
						// Declare the instance screen206
		private Screen206VO screen206;
			/**
	 * Operation : lnk_createPlayer
 	 * @param model : 
 	 * @param uC206_CreatePlayer : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC206_CreatePlayer/lnk_createPlayer.html",method = RequestMethod.POST)
	public String lnk_createPlayer(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC206_CreatePlayerForm") UC206_CreatePlayerForm  uC206_CreatePlayerForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : lnk_createPlayer"); 
		infoLogger(uC206_CreatePlayerForm); 
		uC206_CreatePlayerForm.setAlwaysCallPreControllers(false); 
		// initialization for lnk_createPlayer
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state lnk_createPlayer if the validation fail, you will be forward to the screen : "/presentation/advancedfeatures/valueobject/uc206_valueobject/UC206_CreatePlayer"
		if(errorsMessages(request,response,uC206_CreatePlayerForm,bindingResult,"", uC206_CreatePlayerValidator, customDateEditorsUC206_CreatePlayerController)){ 
			return "/presentation/advancedfeatures/valueobject/uc206_valueobject/UC206_CreatePlayer"; 
		}

					 callExecuteCreate(request,uC206_CreatePlayerForm );
											// Init the destination form com.bluage.documentation.web.presentation.advancedfeatures.valueobject.uc206_valueobject.UC206_HomePageForm or not from lnk_createPlayer
			final UC206_HomePageForm uC206_HomePageForm =  new UC206_HomePageForm();
			// Populate the destination form with all the instances returned from lnk_createPlayer.
			final IForm uC206_HomePageForm2 = populateDestinationForm(request.getSession(), uC206_HomePageForm); 
					
			// Add the form to the model.
			model.addAttribute("uC206_HomePageForm", uC206_HomePageForm2); 
			
			request.getSession().setAttribute("uC206_HomePageForm", uC206_HomePageForm2);
			
			// "OK" CASE => destination screen path from lnk_createPlayer
			LOGGER.info("Go to the screen 'UC206_HomePage'.");
			// Redirect (PRG) from lnk_createPlayer
			destinationPath =  "redirect:/presentation/advancedfeatures/valueobject/uc206_valueobject/UC206_HomePage.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_createPlayer 
	}
	
	
	/**
	* This method initialise the form : UC206_CreatePlayerForm 
	* @return UC206_CreatePlayerForm
	*/
	@ModelAttribute("UC206_CreatePlayerFormInit")
	public void initUC206_CreatePlayerForm(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, U_C206__CREATE_PLAYER_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method UC206_CreatePlayerForm."); //for lnk_createPlayer 
		}
		UC206_CreatePlayerForm uC206_CreatePlayerForm;
	
		if(request.getSession().getAttribute(U_C206__CREATE_PLAYER_FORM) != null){
			uC206_CreatePlayerForm = (UC206_CreatePlayerForm)request.getSession().getAttribute(U_C206__CREATE_PLAYER_FORM);
		} else {
			uC206_CreatePlayerForm = new UC206_CreatePlayerForm();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form UC206_CreatePlayerForm.");
		}
		uC206_CreatePlayerForm = (UC206_CreatePlayerForm)populateDestinationForm(request.getSession(), uC206_CreatePlayerForm);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  UC206_CreatePlayerForm.");
		}
		model.addAttribute(U_C206__CREATE_PLAYER_FORM, uC206_CreatePlayerForm);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param UC206_CreatePlayerForm : The sceen form.
	 */
	@RequestMapping(value = "/UC206_CreatePlayer.html" ,method = RequestMethod.GET)
	public void prepareUC206_CreatePlayer(final HttpServletRequest request,final  Model model,final  @ModelAttribute("UC206_CreatePlayerFormInit") UC206_CreatePlayerForm uC206_CreatePlayerForm){
		
		UC206_CreatePlayerForm currentUC206_CreatePlayerForm = uC206_CreatePlayerForm;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(U_C206__CREATE_PLAYER_FORM) == null){
			if(currentUC206_CreatePlayerForm!=null){
				request.getSession().setAttribute(U_C206__CREATE_PLAYER_FORM, currentUC206_CreatePlayerForm);
			}else {
				currentUC206_CreatePlayerForm = new UC206_CreatePlayerForm();
				request.getSession().setAttribute(U_C206__CREATE_PLAYER_FORM, currentUC206_CreatePlayerForm);	
			}
		} else {
			currentUC206_CreatePlayerForm = (UC206_CreatePlayerForm) request.getSession().getAttribute(U_C206__CREATE_PLAYER_FORM);
		}

		currentUC206_CreatePlayerForm = (UC206_CreatePlayerForm)populateDestinationForm(request.getSession(), currentUC206_CreatePlayerForm);
		// Call all the Precontroller.
	currentUC206_CreatePlayerForm.setAlwaysCallPreControllers(true);
		model.addAttribute(U_C206__CREATE_PLAYER_FORM, currentUC206_CreatePlayerForm);
		request.getSession().setAttribute(U_C206__CREATE_PLAYER_FORM, currentUC206_CreatePlayerForm);
		
	}
	
				/**
	 * method callExecuteCreate
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callExecuteCreate(HttpServletRequest request,IForm form) {
					screen206 = (Screen206VO)get(request.getSession(),form, "screen206");  
										 
					try {
				// executing ExecuteCreate in lnk_createPlayer
	serviceScreen206.executeCreate(
			screen206
			);  

								// processing variables ExecuteCreate in lnk_createPlayer
				put(request.getSession(), SCREEN206,screen206); 
			
			} catch (ApplicationException e) { 
				// error handling for operation executeCreate called ExecuteCreate
				errorLogger("An error occured during the execution of the operation : executeCreate",e); 
		
			}
	}
		/**
	* This method Binder.
	* @param binder : 
	*		 WebDataBinder
	*/
	@InitBinder
	protected void initBinder(final WebDataBinder binder){
			customDateEditorsUC206_CreatePlayerController.put("screen206.player206VO.dateOfBirth", new CustomDateEditor(new SimpleDateFormat("MM/dd/yyyy"), true, "Invalid format."));
			binder.registerCustomEditor(Date.class, "screen206.player206VO.dateOfBirth", customDateEditorsUC206_CreatePlayerController.get("screen206.player206VO.dateOfBirth"));
	}
	
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("UC206_CreatePlayerController [ ");
				strBToS.append(LIST_POSITION206);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(listPosition206);
			strBToS.append(END_TO_STRING_DELIMITER);
						strBToS.append(SCREEN206);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(screen206);
			strBToS.append(END_TO_STRING_DELIMITER);
		// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(uC206_CreatePlayerValidator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsUC206_CreatePlayerController!=null); 
		return strBToS.toString();
	}
}
