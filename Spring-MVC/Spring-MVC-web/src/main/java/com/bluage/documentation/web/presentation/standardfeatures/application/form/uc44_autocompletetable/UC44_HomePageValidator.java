/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.application.form.uc44_autocompletetable ;


import org.apache.log4j.Logger;
import org.springframework.validation.Errors;

import com.netfective.bluage.core.validator.AbstractValidator;
/**
* Class :UC44_HomePageValidator
*/
public class UC44_HomePageValidator extends AbstractValidator{
	
	// LOGGER for the class UC44_HomePageValidator
	private static final Logger LOGGER = Logger.getLogger( UC44_HomePageValidator.class);
	
	
	/**
	* Operation validate for UC44_HomePageForm
	* @param obj : the current form (UC44_HomePageForm)
	* @param errors : The spring errors to return for the form UC44_HomePageForm
	*/
	public void validate(final Object obj,final Errors errors){
		LOGGER.info("Beginning method : validate"); // UC44_HomePageValidator
		UC44_HomePageForm cUC44_HomePageForm = (UC44_HomePageForm)obj; // UC44_HomePageValidator
		LOGGER.info("Ending method : validate the form "+ cUC44_HomePageForm.getClass().getName()); // UC44_HomePageValidator
	}

	/**
	* Method to implements to use spring validators (UC44_HomePageForm)
	* @param aClass : Class for the form UC44_HomePageForm
	* @return boolean
	*/
	public boolean supports(final Class aClass){
		return (new UC44_HomePageForm()).getClass().equals(aClass); // UC44_HomePageValidator
	}
}
