--<ScriptOptions statementTerminator=";"/>

ALTER TABLE player31 DROP CONSTRAINT player31_position_fkc;

ALTER TABLE position31 DROP CONSTRAINT position31_pkey;

ALTER TABLE player31 DROP CONSTRAINT player31_pkey;

DROP TABLE player31;

DROP TABLE position31;

CREATE TABLE player31 (
		id INT8 NOT NULL,
		version INT4 NOT NULL,
		first_name VARCHAR(30) NOT NULL,
		last_name VARCHAR(30) NOT NULL,
		date_of_birth DATE,
		estimated_value FLOAT4,
		weight INT4,
		height INT4,
		rookie BOOL,
		position_fk VARCHAR(2) NOT NULL
	);

CREATE TABLE position31 (
		code VARCHAR(2) NOT NULL,
		version INT4 NOT NULL,
		name VARCHAR(20) NOT NULL
	);


INSERT INTO position31 VALUES ('C', 0, 'Center');
INSERT INTO position31 VALUES ('PF', 0, 'Power Forward');
INSERT INTO position31 VALUES ('PG', 0, 'Point Guard');
INSERT INTO position31 VALUES ('SF', 0, 'Small Forward');
INSERT INTO position31 VALUES ('SG', 0, 'Shooting Guard');


INSERT INTO player31 VALUES (401, 0, 'Gregg', 'Nash', '1972-01-11', 6.8600001, 147, 215, false, 'PG');
INSERT INTO player31 VALUES (402, 0, 'Floyd', 'Howell', '1979-08-25', 25.709999, 155, 192, false, 'SG');
INSERT INTO player31 VALUES (403, 0, 'Simon', 'Klein', '1987-01-21', 22.24, 63, 203, false, 'SF');
INSERT INTO player31 VALUES (404, 0, 'Blake', 'Montgomery', '1992-11-16', 37.470001, 113, 205, true, 'SF');
INSERT INTO player31 VALUES (405, 0, 'Doyle', 'Burns', '1990-02-23', 39.130001, 153, 225, true, 'SG');
INSERT INTO player31 VALUES (406, 0, 'Troy', 'Rowe', '1973-02-12', 26.82, 94, 211, false, 'PF');
INSERT INTO player31 VALUES (407, 0, 'Jesus', 'Jimenez', '1985-09-01', 17.73, 133, 159, false, 'PF');
INSERT INTO player31 VALUES (408, 0, 'Ross', 'Guerrero', '1992-01-26', 13.68, 136, 171, true, 'SF');
INSERT INTO player31 VALUES (409, 0, 'Eddie', 'Boyd', '1967-10-05', 21.719999, 118, 232, false, 'PF');
INSERT INTO player31 VALUES (410, 0, 'Luther', 'Peterson', '1985-06-09', 1.04, 83, 181, false, 'SG');
INSERT INTO player31 VALUES (411, 0, 'Daniel', 'Vargas', '1979-04-21', 30.43, 117, 155, false, 'SG');
INSERT INTO player31 VALUES (412, 0, 'Freddie', 'Kelly', '1967-03-22', 34.400002, 81, 196, false, 'PF');
INSERT INTO player31 VALUES (413, 0, 'Sam', 'Farmer', '1987-11-29', 37.580002, 98, 201, false, 'SF');
INSERT INTO player31 VALUES (414, 0, 'Randal', 'Hampton', '1967-12-01', 31.219999, 121, 202, false, 'SG');
INSERT INTO player31 VALUES (415, 0, 'Felipe', 'Olson', '1976-09-28', 8.0600004, 129, 189, false, 'PF');
INSERT INTO player31 VALUES (416, 0, 'Gary', 'Christensen', '1974-03-13', 21.52, 145, 170, false, 'PG');
INSERT INTO player31 VALUES (417, 0, 'Aaron', 'Mullins', '1972-01-15', 8.54, 86, 239, false, 'SG');
INSERT INTO player31 VALUES (418, 0, 'Miguel', 'Moore', '1967-03-26', 9.4799995, 67, 173, false, 'PG');
INSERT INTO player31 VALUES (419, 0, 'Bruce', 'Adkins', '1986-05-24', 43.720001, 150, 209, false, 'SG');
INSERT INTO player31 VALUES (400, 2, 'Josh', 'Caldwell', '1988-04-26', 1.1, 119, 227, false, 'C');
INSERT INTO player31 VALUES (377, 0, 'Victor', 'Harrison', '1976-02-23', 12.65, 125, 166, false, 'SF');
INSERT INTO player31 VALUES (378, 0, 'Alton', 'Flores', '1988-09-24', 5.3299999, 154, 232, false, 'SG');
INSERT INTO player31 VALUES (379, 0, 'Antonio', 'Robertson', '1987-11-01', 16.719999, 110, 204, false, 'SG');
INSERT INTO player31 VALUES (380, 0, 'Seth', 'Gibson', '1987-03-02', 30.950001, 154, 187, false, 'PF');
INSERT INTO player31 VALUES (381, 0, 'Dale', 'Stevens', '1978-05-09', 45.59, 109, 182, false, 'SG');
INSERT INTO player31 VALUES (382, 0, 'Marcus', 'Sullivan', '1985-02-14', 25.75, 117, 192, false, 'PF');
INSERT INTO player31 VALUES (383, 0, 'Arthur', 'Harmon', '1969-09-22', 1.5700001, 116, 167, false, 'PF');
INSERT INTO player31 VALUES (384, 0, 'Herman', 'West', '1981-03-16', 16.200001, 89, 188, false, 'PF');

ALTER TABLE position31 ADD CONSTRAINT position31_pkey PRIMARY KEY (code);

ALTER TABLE player31 ADD CONSTRAINT player31_pkey PRIMARY KEY (id);

ALTER TABLE player31 ADD CONSTRAINT player31_position_fkc FOREIGN KEY (position_fk)
	REFERENCES position31 (code);
	