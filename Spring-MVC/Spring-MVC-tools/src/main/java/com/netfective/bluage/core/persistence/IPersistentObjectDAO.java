/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.netfective.bluage.core.persistence;
import java.util.List;

import com.netfective.bluage.core.business.IBusinessObject;
import com.netfective.bluage.core.exception.ApplicationException;

/**
 * IPersistentObjectDAO : defines generic persistence methods 
 * for business objects.
 */
public interface IPersistentObjectDAO<T> {

	/**
	 * <p>
	 * This method allow to create the BO in parametre. Return true if the
	 * operation is success.
	 * </p>
	 * 
	 * @param BO
	 * @return boolean: true if success
	 * @throws {@link ApplicationException}
	 */
	boolean create(IBusinessObject bo) throws ApplicationException;

	/**
	 * <p>
	 * This method allow to update the BO in parametre. Return true if the
	 * operation is success.
	 * </p>
	 * 
	 * @param BO
	 * @return boolean: true if success
	 * @throws {@link ApplicationException}
	 */
	boolean update(IBusinessObject bo) throws ApplicationException;

	/**
	 * <p>
	 * This method allow to delete the BO in parametre. Return true if the
	 * operation is success.
	 * </p>
	 * 
	 * @param BO
	 * @return boolean: true if success
	 * @throws {@link ApplicationException}
	 */
	boolean delete(IBusinessObject bo) throws ApplicationException;

	/**
	 * <p>
	 * This method allow to update the BO in parametre if exists else create it
	 * Return true if the operation is success.
	 * </p>
	 * 
	 * @param object
	 *            : BO
	 * @return boolean: true if success
	 * @throws {@link ApplicationException}
	 */

	boolean upsert(IBusinessObject bo) throws ApplicationException;

	/**
	 * <p>
	 * This method allow to create the BO list in parametre. Return the list
	 * size.
	 * </p>
	 * 
	 * @param objects
	 *            : BO List
	 * @return int: Created List size
	 * @throws {@link ApplicationException}
	 */
	boolean createAll(List<T> objects) throws ApplicationException;

	/**
	 * <p>
	 * This method allow to update the BO list in parametre. Return the list
	 * size.
	 * </p>
	 * 
	 * @param objects
	 *            : BO List
	 * @return int: Updated list size
	 * @throws {@link ApplicationException}
	 */
	boolean updateAll(List<T> objects) throws ApplicationException;

	/**
	 * <p>
	 * This method allow to delete the BO list in parametre. Return the deleted
	 * list size.
	 * </p>
	 * 
	 * @param objects
	 *            : BO List
	 * @return int: Deleted list size
	 * @throws {@link ApplicationException}
	 */
	boolean deleteAll(List<T> objects) throws ApplicationException;

	/**
	 * setter for clearSession
	 * @param value boolean
	 */
	void setClearSession(boolean value);

	/**
	 * If true flush and clear the session.
	 */
	boolean isClearSession();

	/**
	 * <code>true</code> flush and clear the session. <code>false</code> does
	 * nothing.
	 * 
	 * @param value
	 * @return
	 */
	void setRefreshSession(boolean value);

	/**
	 * If true flush and refresh the object in the session.
	 */
	boolean isRefreshSession();
}
