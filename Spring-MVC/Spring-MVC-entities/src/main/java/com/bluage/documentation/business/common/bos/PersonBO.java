/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.business.common.bos;

import javax.inject.Inject;
import javax.inject.Named;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import com.bluage.documentation.business.AbstractBusinessObject;
import com.bluage.documentation.business.common.entities.daofinder.PersonDAOFinderImpl;

/**
 * PersonBO
 *
* @see com.netfective.bluage.core.business.IBusinessObject
 */
@XmlRootElement

public class PersonBO
 extends AbstractBusinessObject {
	
	/**
	 * Default SUID for PersonBO.
	 */
	private static final long serialVersionUID = 1L;
	

	@Inject
	@Named ("com.bluage.documentation.business.common.entities.daofinder.PersonFinder") 
	private PersonDAOFinderImpl finder;
	/**
	 * Hibernate optimistic lock for class PersonBO.
	 */
	private int version;

						/**
	 * Entity property: id
	 *
	 */
private Long id;

					/**
	 * Entity property: firstName
	 *
	 */
private String firstName;

					/**
	 * Entity property: lastName
	 *
	 */
private String lastName;



   /**
	* public default constructor for class PersonBO
	*/
	public PersonBO(){
		//NOP PersonBO
	}

	
	/**
	 * Getter for version for class PersonBO
	 * @return int
	 */
	public int getVersion() {
		return this.version;
	}
	
	/**
	 * Setter for version for class PersonBO
	 * @param int version
	 */
	public void setVersion(final int version) {
		this.version = version;
	}

						/**
	 * Getter for id of class PersonBO.
	 *
	 * @return {@link java.lang.Long}
	 * @generated
	 */ 
public  Long getId() {
	return this.id;
	}


	/**
	 * Setter for id of class PersonBO.
	 * @param value {@link java.lang.Long}
	 * @generated
	 */

public  void setId(final Long id) {
	this.isEmpty = (id == null);
	this.id = id;
}

					/**
	 * Getter for firstName of class PersonBO.
	 *
	 * @return {@link java.lang.String}
	 * @generated
	 */ 
public  String getFirstName() {
	return this.firstName;
	}


	/**
	 * Setter for firstName of class PersonBO.
	 * @param value {@link java.lang.String}
	 * @generated
	 */

public  void setFirstName(final String firstName) {
	this.firstName = firstName;
}

					/**
	 * Getter for lastName of class PersonBO.
	 *
	 * @return {@link java.lang.String}
	 * @generated
	 */ 
public  String getLastName() {
	return this.lastName;
	}


	/**
	 * Setter for lastName of class PersonBO.
	 * @param value {@link java.lang.String}
	 * @generated
	 */

public  void setLastName(final String lastName) {
	this.lastName = lastName;
}


/**
	 * Returns <code>true</code> if the argument is an Person instance and all identifiers for this entity
     * equal the identifiers of the argument entity. Returns <code>false</code> otherwise.
	 * @param object  object to compare with current instance      
     * @return boolean true if equals false if not
     */
    public boolean equals(final Object object )
    {
		if ( this == object )
        {
            return true;
        }
        if ( !( object instanceof PersonBO ) )
        {
            return false;
        }
        final PersonBO that = (PersonBO)object;
    
    	            if (        				        			this.getId() == null
        	        ) {
        	return false;
        }
		return new EqualsBuilder()
			.append(this.getId(), that.getId())
			 	.appendSuper(super.equals(object))
		 	.isEquals();
	}
	/**
     * Returns a hash code based on this entity's identifiers.     * @return int
     */    
    public int hashCode() {
        return new HashCodeBuilder()
             .append(getId())
		            .toHashCode();
    }
}

