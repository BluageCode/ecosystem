/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.service;


import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;

import com.netfective.bluage.core.exception.ApplicationException;
import com.netfective.bluage.core.exception.UnexpectedException;

/** Class ExceptionServiceAspect */
@Aspect
public class ExceptionServiceAspect {
	private static final String STR = ".";
	private static final String IN_THE_SERVICE = ", in the service : ";
	private static final String ERROR_EXECUTING_THE_METHOD = "Error executing the method : ";
	
	/** Method manageServicesException.
	 * @param joinPoint  : Spring JoinPoint
	 * @param error  : The Exception
	 * @throws ApplicationException */
	@AfterThrowing(pointcut = "execution(* com.bluage.documentation.service..*.*(..))", throwing = "error")
	public void manageServicesException(JoinPoint joinPoint, Exception error)
			throws ApplicationException {
		if (error instanceof ApplicationException) {
			throw (ApplicationException) error;
		} else {
			StringBuilder strB = new StringBuilder(ERROR_EXECUTING_THE_METHOD);
			strB.append(joinPoint.getSignature().getName());
			strB.append(IN_THE_SERVICE);
			strB.append(joinPoint.getSignature().getDeclaringTypeName());
			strB.append(STR);
			throw new UnexpectedException( strB.toString(), error);
		}
	}
}
