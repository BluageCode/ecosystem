/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.application.navigation.uc29_globalscope.package_two ;

// Import declaration.
// Java imports.
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;

/**
 * Class : UC29_StadiumsController
 */
@Controller
@Scope("prototype")
@SessionAttributes("uC29_StadiumsForm")
@RequestMapping(value= "/presentation/standardfeatures/application/navigation/uc29_globalscope/package_two")
public class UC29_StadiumsController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC29_StadiumsController.class);
	
	//Current form name
	private static final String U_C29__STADIUMS_FORM = "uC29_StadiumsForm";

	//CONSTANT: allStadiums table or repeater.
	private static final String ALL_STADIUMS = "allStadiums";
				
	/**
	 * Property:customDateEditorsUC29_StadiumsController
	 */
	final private Map<String,CustomDateEditor> customDateEditorsUC29_StadiumsController = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:uC29_StadiumsValidator
	 */
	final private UC29_StadiumsValidator uC29_StadiumsValidator = new UC29_StadiumsValidator();
	
	
	// Initialise all the instances for UC29_StadiumsController
		// Initialize the instance allStadiums for the state findTeams
		private List allStadiums; // Initialize the instance allStadiums for UC29_StadiumsController
				
	/**
	* This method initialise the form : UC29_StadiumsForm 
	* @return UC29_StadiumsForm
	*/
	@ModelAttribute("UC29_StadiumsFormInit")
	public void initUC29_StadiumsForm(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, U_C29__STADIUMS_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method UC29_StadiumsForm."); //for findTeams 
		}
		UC29_StadiumsForm uC29_StadiumsForm;
	
		if(request.getSession().getAttribute(U_C29__STADIUMS_FORM) != null){
			uC29_StadiumsForm = (UC29_StadiumsForm)request.getSession().getAttribute(U_C29__STADIUMS_FORM);
		} else {
			uC29_StadiumsForm = new UC29_StadiumsForm();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form UC29_StadiumsForm.");
		}
		uC29_StadiumsForm = (UC29_StadiumsForm)populateDestinationForm(request.getSession(), uC29_StadiumsForm);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  UC29_StadiumsForm.");
		}
		model.addAttribute(U_C29__STADIUMS_FORM, uC29_StadiumsForm);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param UC29_StadiumsForm : The sceen form.
	 */
	@RequestMapping(value = "/UC29_Stadiums.html" ,method = RequestMethod.GET)
	public void prepareUC29_Stadiums(final HttpServletRequest request,final  Model model,final  @ModelAttribute("UC29_StadiumsFormInit") UC29_StadiumsForm uC29_StadiumsForm){
		
		UC29_StadiumsForm currentUC29_StadiumsForm = uC29_StadiumsForm;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(U_C29__STADIUMS_FORM) == null){
			if(currentUC29_StadiumsForm!=null){
				request.getSession().setAttribute(U_C29__STADIUMS_FORM, currentUC29_StadiumsForm);
			}else {
				currentUC29_StadiumsForm = new UC29_StadiumsForm();
				request.getSession().setAttribute(U_C29__STADIUMS_FORM, currentUC29_StadiumsForm);	
			}
		} else {
			currentUC29_StadiumsForm = (UC29_StadiumsForm) request.getSession().getAttribute(U_C29__STADIUMS_FORM);
		}

					currentUC29_StadiumsForm = (UC29_StadiumsForm)populateDestinationForm(request.getSession(), currentUC29_StadiumsForm);
		// Call all the Precontroller.
	currentUC29_StadiumsForm.setAlwaysCallPreControllers(true);
		model.addAttribute(U_C29__STADIUMS_FORM, currentUC29_StadiumsForm);
		request.getSession().setAttribute(U_C29__STADIUMS_FORM, currentUC29_StadiumsForm);
		
	}
	
				
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("UC29_StadiumsController [ ");
			strBToS.append(ALL_STADIUMS);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(allStadiums);
			strBToS.append(END_TO_STRING_DELIMITER);
					// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(uC29_StadiumsValidator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsUC29_StadiumsController!=null); 
		return strBToS.toString();
	}
}
