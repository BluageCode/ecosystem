--<ScriptOptions statementTerminator=";"/>

ALTER TABLE player50 DROP CONSTRAINT player50_team_fkc;

ALTER TABLE player50 DROP CONSTRAINT player50_pkey;

ALTER TABLE team50 DROP CONSTRAINT team50_pkey;

DROP TABLE player50;

DROP TABLE team50;

CREATE TABLE player50 (
		id INT8 NOT NULL,
		version INT4 NOT NULL,
		first_name VARCHAR(30) NOT NULL,
		last_name VARCHAR(30) NOT NULL,
		height INT4,
		weight INT4,
		estimated_value FLOAT4,
		date_of_birth DATE,
		rookie BOOL,
		team_fk INT8 NOT NULL
	);

CREATE TABLE team50 (
		id INT8 NOT NULL,
		version INT4 NOT NULL,
		city VARCHAR(30),
		name VARCHAR(30)
	);

INSERT INTO team50 VALUES (1, 0, 'Charlotte', 'Tigers');
INSERT INTO team50 VALUES (2, 0, 'Kansas City', 'Bears');
INSERT INTO team50 VALUES (3, 0, 'Detroit', 'Chargers');
INSERT INTO team50 VALUES (4, 0, 'Detroit', 'Hawks');
INSERT INTO team50 VALUES (5, 0, 'Omaha', 'Braves');
INSERT INTO team50 VALUES (6, 0, 'Raleigh', 'Eagles');
INSERT INTO team50 VALUES (7, 0, 'Oakland', 'Cardinals');
INSERT INTO team50 VALUES (8, 0, 'Kansas City', 'Red Devils');
INSERT INTO team50 VALUES (9, 0, 'Fresno', 'Mustangs');

INSERT INTO player50 (id, version, first_name, last_name, height, weight, estimated_value, date_of_birth, team_fk, rookie) VALUES (1, 0, 'Oliver', 'Frank', 228, 116, 0.11, '1968-10-31', 1, false);
INSERT INTO player50 (id, version, first_name, last_name, height, weight, estimated_value, date_of_birth, team_fk, rookie) VALUES (2, 0, 'Elbert', 'Cole', 171, 100, 3.95, '1965-04-07', 2, false);
INSERT INTO player50 (id, version, first_name, last_name, height, weight, estimated_value, date_of_birth, team_fk, rookie) VALUES (3, 0, 'Abel', 'Dennis', 208, 105, 30.51, '1980-04-16', 3, false);
INSERT INTO player50 (id, version, first_name, last_name, height, weight, estimated_value, date_of_birth, team_fk, rookie) VALUES (4, 0, 'Earl', 'Russel', 228, 116, 29.3, '1981-12-15', 1, false);
INSERT INTO player50 (id, version, first_name, last_name, height, weight, estimated_value, date_of_birth, team_fk, rookie) VALUES (5, 0, 'Nick', 'Garner', 228, 116, 44.73, '1992-07-27', 1, true);
INSERT INTO player50 (id, version, first_name, last_name, height, weight, estimated_value, date_of_birth, team_fk, rookie) VALUES (6, 0, 'Howard', 'Ramos', 228, 116, 47.84, '1970-08-25', 4, false);
INSERT INTO player50 (id, version, first_name, last_name, height, weight, estimated_value, date_of_birth, team_fk, rookie) VALUES (7, 0, 'Allan', 'Meyer', 229, 114, 33.67, '1969-02-14', 8, false);
INSERT INTO player50 (id, version, first_name, last_name, height, weight, estimated_value, date_of_birth, team_fk, rookie) VALUES (8, 0, 'Ruben', 'Pope', 201, 86, 44.66, '1985-05-01', 7, false);
INSERT INTO player50 (id, version, first_name, last_name, height, weight, estimated_value, date_of_birth, team_fk, rookie) VALUES (9, 0, 'Lester', 'Cross', 214, 69, 30.98, '1976-02-21', 5, false);
INSERT INTO player50 (id, version, first_name, last_name, height, weight, estimated_value, date_of_birth, team_fk, rookie) VALUES (10, 0, 'Keith', 'Barnett', 190, 66, 5.81, '1976-11-10', 6, false);
INSERT INTO player50 (id, version, first_name, last_name, height, weight, estimated_value, date_of_birth, team_fk, rookie) VALUES (11, 0, 'Ivan', 'Tate', 175, 143, 48.49, '1972-02-08', 7, false);
INSERT INTO player50 (id, version, first_name, last_name, height, weight, estimated_value, date_of_birth, team_fk, rookie) VALUES (12, 0, 'Tommy', 'Wood', 173, 77, 13.93, '1992-01-20', 3, true);
INSERT INTO player50 (id, version, first_name, last_name, height, weight, estimated_value, date_of_birth, team_fk, rookie) VALUES (13, 0, 'Sylvester', 'Nash', 184, 68, 37.73, '1965-07-10', 4, false);
INSERT INTO player50 (id, version, first_name, last_name, height, weight, estimated_value, date_of_birth, team_fk, rookie) VALUES (14, 0, 'Michael', 'Hodges', 204, 158, 25.35, '1988-04-01', 5, false);
INSERT INTO player50 (id, version, first_name, last_name, height, weight, estimated_value, date_of_birth, team_fk, rookie) VALUES (15, 0, 'Jerome', 'Grant', 205, 105, 28.94, '1982-06-28', 6, false);
INSERT INTO player50 (id, version, first_name, last_name, height, weight, estimated_value, date_of_birth, team_fk, rookie) VALUES (16, 0, 'Woodrow', 'Carpenter', 224, 69, 41.75, '1987-08-08', 7, false);
INSERT INTO player50 (id, version, first_name, last_name, height, weight, estimated_value, date_of_birth, team_fk, rookie) VALUES (17, 0, 'Roderick', 'Robbins', 164, 115, 12.043, '1975-07-29', 8, false);

	
	
ALTER TABLE player50 ADD CONSTRAINT player50_pkey PRIMARY KEY (id);

ALTER TABLE team50 ADD CONSTRAINT team50_pkey PRIMARY KEY (id);

ALTER TABLE player50 ADD CONSTRAINT player50_team_fkc FOREIGN KEY (team_fk)
	REFERENCES team50 (id);

