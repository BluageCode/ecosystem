package uc37;

import org.junit.Test;
import test.common.AbstractBADocTestCase;

public class ITuc37TestPagination extends AbstractBADocTestCase {
	
	@Override
	public  String getName(){
		return "UC-37";
	}
	
	@Test
	public void testITuc37TestPagination() throws Exception {
		// On teste l'UC37 - Standard Features/Application/Datagrid - Collection Order
		selenium.open("");
		selenium.click("link=Expand All");
		selenium.waitForPageToLoad("30000");
		selenium.click("link=UC37 - Collection Order");
		verifyTrue(selenium.isElementPresent("//h2[contains(text(),\"Standard Features > Application > DataGrid > UC37 > Collection Order\")]"));
		// On indexe la ligne de Charlotte.
		Number index = selenium.getElementIndex("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Charlotte\")] and td/span[contains(text(),\"Tigers\")]]");
		System.out.println("variable index vaut : " + index);
		// On clique sur [view]
		selenium.click("//table[contains(@id,'tab_teams')]/tbody/tr[" + index + " + 1]/td/a[contains(text(),\"[view]\")]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie que l'on est sur la bonne page.
		verifyTrue(selenium.isElementPresent("//h2[contains(text(),\"Standard Features > Application > DataGrid > UC37 > Collection Order\")]"));
		verifyTrue(selenium.isElementPresent("//h4[contains(text(),\"Players\")]"));
		assertTrue(selenium.isElementPresent("//table/thead/tr/th[contains(text(),'Name')]"));
		assertTrue(selenium.isElementPresent("//a[contains(text(),'[back]')]"));
		assertEquals("Tigers", selenium.getText("//*[contains(@id,'txt_TeamName')]"));
		assertEquals("Charlotte", selenium.getText("//*[contains(@id,'txt_Teamcity')]"));
		// On teste les liens.
		// On va sur la page 2.
		selenium.click("xpath=(//span/a)[1]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie qu'on est sur la page 2.
		assertTrue(selenium.isElementPresent("//a/b[contains(text(),\"2\")]|//strong[contains(text(),\"2\")]"));
//		// On va tester le lien fast next.
//		selenium.click("//a[img[@alt=\"Fast next\"]]");
//		selenium.waitForPageToLoad("30000");
//		// Les sauts de pages sont de 3 pages. On v�rifie que l'on est bien sur la 5eme page.
//		assertTrue(selenium.isElementPresent("//a/b[contains(text(),\"6\")]|//strong[contains(text(),\"6\")]"));
//		// On teste le lien fast prev.
//		selenium.click("//a[img[@alt=\"Fast prev\"]]");
//		selenium.waitForPageToLoad("30000");
//		// Les sauts de pages sont de 3 pages. On v�rife que l'on est bien sur la 2eme page.
//		assertTrue(selenium.isElementPresent("//a/b[contains(text(),\"1\")]|//strong[contains(text(),\"1\")]"));
		// On teste le lien Prev.
		selenium.click("xpath=(//span/a)[2]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie que l'on est sur la premi�re page.
		assertTrue(selenium.isElementPresent("//a/b[contains(text(),\"1\")]|//strong[contains(text(),\"1\")]"));
		// On teste le lien Last.
		selenium.click("xpath=(//span/a)[7]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//a/b[contains(text(),\"6\")]|//strong[contains(text(),\"6\")]"));
		// On teste le lien First
		selenium.click("xpath=(//span/a)[1]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//a/b[contains(text(),\"1\")]|//strong[contains(text(),\"1\")]"));
		// On clique sur [back]
		selenium.click("//*[contains(@id,'back')]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//h2[contains(@class,'rd_first')]"));
		// Test Pagination Detroit Chargers
		selenium.click("link=UC37 - Collection Order");
		selenium.waitForPageToLoad("30000");
		verifyTrue(selenium.isElementPresent("//h2[contains(text(),\"Standard Features > Application > DataGrid > UC37 > Collection Order\")]"));
		// On indexe la ligne de Detroit.
		Number index2 = selenium.getElementIndex("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Detroit\")] and td/span[contains(text(),\"Chargers\")]]");
		System.out.println("variable index vaut : " + index2);
		// On clique sur [view]
		selenium.click("//table[contains(@id,'tab_teams')]/tbody/tr[" + index2 + " + 1]/td/a[contains(text(),\"[view]\")]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie que l'on est sur la bonne page.
		verifyTrue(selenium.isElementPresent("//h2[contains(text(),\"Standard Features > Application > DataGrid > UC37 > Collection Order\")]"));
		verifyTrue(selenium.isElementPresent("//h4[contains(text(),\"Players\")]"));
		assertTrue(selenium.isElementPresent("//table/thead/tr/th[contains(text(),'Name')]"));
		assertTrue(selenium.isElementPresent("//a[contains(text(),'[back]')]"));
		assertEquals("Chargers", selenium.getText("//*[contains(@id,'txt_TeamName')]"));
		assertEquals("Detroit", selenium.getText("//*[contains(@id,'txt_Teamcity')]"));
		// On teste les liens.
		// On va sur la page 2.
		selenium.click("xpath=(//span/a)[1]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie qu'on est sur la page 2.
		assertTrue(selenium.isElementPresent("//a/b[contains(text(),\"2\")]|//strong[contains(text(),\"2\")]"));
		// On va tester le lien fast next.
//		selenium.click("//a[img[@alt=\"Fast next\"]]");
//		selenium.waitForPageToLoad("30000");
//		// Les sauts de pages sont de 3 pages. On v�rifie que l'on est bien sur la 5eme page.
//		assertTrue(selenium.isElementPresent("//a/b[contains(text(),\"3\")]|//strong[contains(text(),\"3\")]"));
//		// On teste le lien fast prev.
//		selenium.click("//a[img[@alt=\"Fast prev\"]]");
//		selenium.waitForPageToLoad("30000");
//		// Les sauts de pages sont de 3 pages. On v�rife que l'on est bien sur la 2eme page.
//		assertTrue(selenium.isElementPresent("//a/b[contains(text(),\"1\")]|//strong[contains(text(),\"1\")]"));
		// On teste le lien Prev.
		selenium.click("xpath=(//span/a)[2]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie que l'on est sur la premi�re page.
		assertTrue(selenium.isElementPresent("//a/b[contains(text(),\"1\")]|//strong[contains(text(),\"1\")]"));
		// On teste le lien Last.
		selenium.click("xpath=(//span/a)[4]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//a/b[contains(text(),\"3\")]|//strong[contains(text(),\"3\")]"));
		// On teste le lien First
		selenium.click("xpath=(//span/a)[1]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//a/b[contains(text(),\"1\")]|//strong[contains(text(),\"1\")]"));
		// On clique sur [back]
		selenium.click("//*[contains(@id,'back')]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//h2[contains(@class,'rd_first')]"));
		// Test Pagination Detroit Hawks
		selenium.click("link=UC37 - Collection Order");
		selenium.waitForPageToLoad("30000");
		verifyTrue(selenium.isElementPresent("//h2[contains(text(),\"Standard Features > Application > DataGrid > UC37 > Collection Order\")]"));
		// On indexe la ligne de Detroit.
		Number index3 = selenium.getElementIndex("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Detroit\")] and td/span[contains(text(),\"Hawks\")]]");
		System.out.println("variable index vaut : " + index3);
		// On clique sur [view]
		selenium.click("//table[contains(@id,'tab_teams')]/tbody/tr[" + index3 + " + 1]/td/a[contains(text(),\"[view]\")]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie que l'on est sur la bonne page.
		verifyTrue(selenium.isElementPresent("//h2[contains(text(),\"Standard Features > Application > DataGrid > UC37 > Collection Order\")]"));
		verifyTrue(selenium.isElementPresent("//h4[contains(text(),\"Players\")]"));
		assertTrue(selenium.isElementPresent("//table/thead/tr/th[contains(text(),'Name')]"));
		assertTrue(selenium.isElementPresent("//a[contains(text(),'[back]')]"));
		assertEquals("Hawks", selenium.getText("//*[contains(@id,'txt_TeamName')]"));
		assertEquals("Detroit", selenium.getText("//*[contains(@id,'txt_Teamcity')]"));
		// On teste les liens.
		// On va sur la page 2.
		selenium.click("xpath=(//span/a)[1]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie qu'on est sur la page 2.
		assertTrue(selenium.isElementPresent("//a/b[contains(text(),\"2\")]|//strong[contains(text(),\"2\")]"));
		// On va tester le lien fast next.
//		selenium.click("//a[img[@alt=\"Fast next\"]]");
//		selenium.waitForPageToLoad("30000");
//		// Les sauts de pages sont de 3 pages. On v�rifie que l'on est bien sur la 5eme page.
//		assertTrue(selenium.isElementPresent("//a/b[contains(text(),\"4\")]|//strong[contains(text(),\"4\")]"));
//		// On teste le lien fast prev.
//		selenium.click("//a[img[@alt=\"Fast prev\"]]");
//		selenium.waitForPageToLoad("30000");
//		// Les sauts de pages sont de 3 pages. On v�rife que l'on est bien sur la 2eme page.
//		assertTrue(selenium.isElementPresent("//a/b[contains(text(),\"1\")]|//strong[contains(text(),\"1\")]"));
		// On teste le lien Prev.
		selenium.click("xpath=(//span/a)[2]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie que l'on est sur la premi�re page.
		assertTrue(selenium.isElementPresent("//a/b[contains(text(),\"1\")]|//strong[contains(text(),\"1\")]"));
		// On teste le lien Last.
		selenium.click("xpath=(//span/a)[5]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//a/b[contains(text(),\"4\")]|//strong[contains(text(),\"4\")]"));
		// On teste le lien First
		selenium.click("xpath=(//span/a)[1]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//a/b[contains(text(),\"1\")]|//strong[contains(text(),\"1\")]"));
		// On clique sur [back]
		selenium.click("//*[contains(@id,'back')]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//h2[contains(@class,'rd_first')]"));
		// Test Pagination Kanas City Bears
		selenium.click("link=UC37 - Collection Order");
		selenium.waitForPageToLoad("30000");
		verifyTrue(selenium.isElementPresent("//h2[contains(text(),\"Standard Features > Application > DataGrid > UC37 > Collection Order\")]"));
		// On indexe la ligne de Kansas City.
		Number index4 = selenium.getElementIndex("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Kansas City\")] and td/span[contains(text(),\"Bears\")]]");
		System.out.println("variable index vaut : " + index4);
		// On clique sur [view]
		selenium.click("//table[contains(@id,'tab_teams')]/tbody/tr[" + index4 + " + 1]/td/a[contains(text(),\"[view]\")]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie que l'on est sur la bonne page.
		verifyTrue(selenium.isElementPresent("//h2[contains(text(),\"Standard Features > Application > DataGrid > UC37 > Collection Order\")]"));
		verifyTrue(selenium.isElementPresent("//h4[contains(text(),\"Players\")]"));
		assertTrue(selenium.isElementPresent("//table/thead/tr/th[contains(text(),'Name')]"));
		assertTrue(selenium.isElementPresent("//a[contains(text(),'[back]')]"));
		assertEquals("Bears", selenium.getText("//*[contains(@id,'txt_TeamName')]"));
		assertEquals("Kansas City", selenium.getText("//*[contains(@id,'txt_Teamcity')]"));
		// On teste les liens.
		// On va sur la page 2.
		selenium.click("xpath=(//span/a)[1]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie qu'on est sur la page 2.
		assertTrue(selenium.isElementPresent("//a/b[contains(text(),\"2\")]|//strong[contains(text(),\"2\")]"));
//		// On va tester le lien fast next.
//		selenium.click("//a[img[@alt=\"Fast next\"]]");
//		selenium.waitForPageToLoad("30000");
//		// Les sauts de pages sont de 3 pages. On v�rifie que l'on est bien sur la 5eme page.
//		assertTrue(selenium.isElementPresent("//a/b[contains(text(),\"6\")]|//strong[contains(text(),\"6\")]"));
//		// On teste le lien fast prev.
//		selenium.click("//a[img[@alt=\"Fast prev\"]]");
//		selenium.waitForPageToLoad("30000");
//		// Les sauts de pages sont de 3 pages. On v�rife que l'on est bien sur la 2eme page.
//		assertTrue(selenium.isElementPresent("//a/b[contains(text(),\"1\")]|//strong[contains(text(),\"1\")]"));
		// On teste le lien Prev.
		selenium.click("xpath=(//span/a)[2]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie que l'on est sur la premi�re page.
		assertTrue(selenium.isElementPresent("//a/b[contains(text(),\"1\")]|//strong[contains(text(),\"1\")]"));
		// On teste le lien Last.
		selenium.click("xpath=(//span/a)[7]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//a/b[contains(text(),\"6\")]|//strong[contains(text(),\"6\")]"));
		// On teste le lien First
		selenium.click("xpath=(//span/a)[1]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//a/b[contains(text(),\"1\")]|//strong[contains(text(),\"1\")]"));
		// On clique sur [back]
		selenium.click("//*[contains(@id,'back')]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//h2[contains(@class,'rd_first')]"));
		// Test Pagination Omaha Braves
		selenium.click("link=UC37 - Collection Order");
		selenium.waitForPageToLoad("30000");
		verifyTrue(selenium.isElementPresent("//h2[contains(text(),\"Standard Features > Application > DataGrid > UC37 > Collection Order\")]"));
		// On indexe la ligne de Omaha
		Number index5 = selenium.getElementIndex("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Omaha\")] and td/span[contains(text(),\"Braves\")]]");
		System.out.println("variable index vaut : " + index5);
		// On clique sur [view]
		selenium.click("//table[contains(@id,'tab_teams')]/tbody/tr[" + index5 + " + 1]/td/a[contains(text(),\"[view]\")]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie que l'on est sur la bonne page.
		verifyTrue(selenium.isElementPresent("//h2[contains(text(),\"Standard Features > Application > DataGrid > UC37 > Collection Order\")]"));
		verifyTrue(selenium.isElementPresent("//h4[contains(text(),\"Players\")]"));
		assertTrue(selenium.isElementPresent("//table/thead/tr/th[contains(text(),'Name')]"));
		assertTrue(selenium.isElementPresent("//a[contains(text(),'[back]')]"));
		assertEquals("Braves", selenium.getText("//*[contains(@id,'txt_TeamName')]"));
		assertEquals("Omaha", selenium.getText("//*[contains(@id,'txt_Teamcity')]"));
		// On teste les liens.
		// On va sur la page 2.
		selenium.click("xpath=(//span/a)[1]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie qu'on est sur la page 2.
		assertTrue(selenium.isElementPresent("//a/b[contains(text(),\"2\")]|//strong[contains(text(),\"2\")]"));
		// On va tester le lien fast next.
//		selenium.click("//a[img[@alt=\"Fast next\"]]");
//		selenium.waitForPageToLoad("30000");
//		// Les sauts de pages sont de 3 pages. On v�rifie que l'on est bien sur la 5eme page.
//		assertTrue(selenium.isElementPresent("//a/b[contains(text(),\"3\")]|//strong[contains(text(),\"3\")]"));
//		// On teste le lien fast prev.
//		selenium.click("//a[img[@alt=\"Fast prev\"]]");
//		selenium.waitForPageToLoad("30000");
//		// Les sauts de pages sont de 3 pages. On v�rife que l'on est bien sur la 2eme page.
//		assertTrue(selenium.isElementPresent("//a/b[contains(text(),\"1\")]|//strong[contains(text(),\"1\")]"));
		// On teste le lien Prev.
		selenium.click("xpath=(//span/a)[2]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie que l'on est sur la premi�re page.
		assertTrue(selenium.isElementPresent("//a/b[contains(text(),\"1\")]|//strong[contains(text(),\"1\")]"));
		// On teste le lien Last.
		selenium.click("xpath=(//span/a)[4]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//a/b[contains(text(),\"3\")]|//strong[contains(text(),\"3\")]"));
		// On teste le lien First
		selenium.click("xpath=(//span/a)[1]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//a/b[contains(text(),\"1\")]|//strong[contains(text(),\"1\")]"));
		// On clique sur [back]
		selenium.click("//*[contains(@id,'back')]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//h2[contains(@class,'rd_first')]"));
	}

	
}
