/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.service.standardfeatures.application.navigation.uc30_navigation;


import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import com.bluage.documentation.business.standardfeatures.application.navigation.uc30_navigation.bos.Page30BO;
import com.bluage.documentation.service.AbstractService;
import com.bluage.documentation.service.standardfeatures.application.navigation.uc30_navigation.daofinder.ServiceNavigationDAOMethodFinderImpl;
import com.netfective.bluage.core.exception.ApplicationException;

/**
 * Defines  application services for ServiceNavigation
  */
@Named("com.bluage.documentation.service.standardfeatures.application.navigation.uc30_navigation.ServiceNavigation")
public class ServiceNavigation  extends AbstractService{


			@Inject
		@Named ("com.bluage.documentation.service.standardfeatures.application.navigation.uc30_navigation.daofinder.ServiceNavigationDAOMethodFinder")
		private ServiceNavigationDAOMethodFinderImpl serviceNavigationDao;
						


	/**
	 *  Default constructor 
	 */
	public ServiceNavigation() {
		super(); // Call super for default constructor : ServiceNavigation
	}
	



			
	/**    
	 * Process operation.
	 * @return {@link void}
	 */
	public void doNothing( )
 throws ApplicationException {

			privateDoNothing();
	}
	

	/**    
	 * Process operation.
	 * @return {@link void}
	 */
	private void privateDoNothing( )
 throws ApplicationException {
// Central buffer node declaration for operation doNothing
			
		}

		
				
	/**    
	 * Operation HQL.
	 * @return {@link List}
	 */
	public List loadPages( )
 throws ApplicationException {
	
	return serviceNavigationDao.loadPages();
	}

		
			
	/**    
	 * Process operation.
	 * @param selectedPage {@link Page30BO}
		 * @return {@link Integer}
	 */
	public Integer getPage(Page30BO selectedPage )
 throws ApplicationException {

				return privateGetPage(selectedPage);
	}
	

	/**    
	 * Process operation.
	 * @param selectedPage {@link Page30BO}
		 * @return {@link Integer}
	 */
	private Integer privateGetPage(Page30BO selectedPage )
 throws ApplicationException {
						// Return Type declaration.
				Integer value = Integer.valueOf(0);
					// Central buffer node declaration for operation getPage
			// Calling an operation : 
value = selectedPage.getId();

						return value;
		}

		}
