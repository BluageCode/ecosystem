/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.application.datagrid.uc38_selectedlist ;


import org.apache.log4j.Logger;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;

import com.netfective.bluage.core.validator.AbstractValidator;
import com.netfective.bluage.core.validator.ValidatorsUtil;
/**
* Class :UC38_DisplaySelectedListValidator
*/
public class UC38_DisplaySelectedListValidator extends AbstractValidator{
	
	// LOGGER for the class UC38_DisplaySelectedListValidator
	private static final Logger LOGGER = Logger.getLogger( UC38_DisplaySelectedListValidator.class);
	
	
	/**
	* Operation validate for UC38_DisplaySelectedListForm
	* @param obj : the current form (UC38_DisplaySelectedListForm)
	* @param errors : The spring errors to return for the form UC38_DisplaySelectedListForm
	*/
	public void validate(final Object obj,final Errors errors){
		LOGGER.info("Beginning method : validate"); // UC38_DisplaySelectedListValidator
		UC38_DisplaySelectedListForm cUC38_DisplaySelectedListForm = (UC38_DisplaySelectedListForm)obj; // UC38_DisplaySelectedListValidator
							ValidationUtils.rejectIfEmpty(errors, "allPlayers["+getRequestIndex()+"].firstName", "", "First name is required.");

		if(errors.getFieldError("allPlayers["+getRequestIndex()+"].firstName")== null){
								if("playerToUpdate".equals(getTableElement())){
			if(LOGGER.isDebugEnabled()){
				LOGGER.debug("validation of the 'allPlayers["+getRequestIndex()+"].firstName' field with the LengthValidator validator");
			}
			
			if(ValidatorsUtil.lengthValidator(cUC38_DisplaySelectedListForm.getAllPlayers().get(Integer.valueOf(getRequestIndex())).getFirstName(), 1, 30) == 0){
				errors.rejectValue("allPlayers["+getRequestIndex()+"].firstName", "", "First name should be less than 30 characters long.");
			} 
		}
				
		}
							ValidationUtils.rejectIfEmpty(errors, "allPlayers["+getRequestIndex()+"].lastName", "", "Last name is required.");

		if(errors.getFieldError("allPlayers["+getRequestIndex()+"].lastName")== null){
								if("playerToUpdate".equals(getTableElement())){
			if(LOGGER.isDebugEnabled()){
				LOGGER.debug("validation of the 'allPlayers["+getRequestIndex()+"].lastName' field with the LengthValidator validator");
			}
			
			if(ValidatorsUtil.lengthValidator(cUC38_DisplaySelectedListForm.getAllPlayers().get(Integer.valueOf(getRequestIndex())).getLastName(), 1, 30) == 0){
				errors.rejectValue("allPlayers["+getRequestIndex()+"].lastName", "", "Last name should be less than 30 characters long.");
			} 
		}
				
		}
							ValidationUtils.rejectIfEmpty(errors, "allPlayers["+getRequestIndex()+"].dateOfBirth", "", "Date of birth is required.");

			LOGGER.info("Ending method : validate the form "+ cUC38_DisplaySelectedListForm.getClass().getName()); // UC38_DisplaySelectedListValidator
	}

	/**
	* Method to implements to use spring validators (UC38_DisplaySelectedListForm)
	* @param aClass : Class for the form UC38_DisplaySelectedListForm
	* @return boolean
	*/
	public boolean supports(final Class aClass){
		return (new UC38_DisplaySelectedListForm()).getClass().equals(aClass); // UC38_DisplaySelectedListValidator
	}
}
