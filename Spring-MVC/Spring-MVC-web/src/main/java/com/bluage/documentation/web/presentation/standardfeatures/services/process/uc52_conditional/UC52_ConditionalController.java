/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.services.process.uc52_conditional ;

// Import declaration.
// Java imports.
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.bluage.documentation.business.standardfeatures.services.process.uc52_conditional.bos.Player52BO;
import com.bluage.documentation.service.standardfeatures.services.process.uc52_conditional.ServicePlayer52;
import com.bluage.documentation.service.standardfeatures.services.process.uc52_conditional.ServicePlayer52FindAll;
import com.netfective.bluage.core.exception.ApplicationException;
import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;
import com.netfective.bluage.core.web.IForm;

/**
 * Class : UC52_ConditionalController
 */
@Controller
@Scope("prototype")
@SessionAttributes("uC52_ConditionalForm")
@RequestMapping(value= "/presentation/standardfeatures/services/process/uc52_conditional")
public class UC52_ConditionalController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC52_ConditionalController.class);
	
	//Current form name
	private static final String U_C52__CONDITIONAL_FORM = "uC52_ConditionalForm";

					//CONSTANT: newPlayer
	private static final String NEW_PLAYER = "newPlayer";
	//CONSTANT: players
	private static final String PLAYERS = "players";
	
	/**
	 * Property:customDateEditorsUC52_ConditionalController
	 */
	final private Map<String,CustomDateEditor> customDateEditorsUC52_ConditionalController = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:uC52_ConditionalValidator
	 */
	final private UC52_ConditionalValidator uC52_ConditionalValidator = new UC52_ConditionalValidator();
	
	/**
	 * Service declaration : servicePlayer52.
	 */
	@Autowired
	private ServicePlayer52 servicePlayer52;
	
	/**
	 * Service declaration : servicePlayer52FindAll.
	 */
	@Autowired
	private ServicePlayer52FindAll servicePlayer52FindAll;
	
	
	// Initialise all the instances for UC52_ConditionalController
							// Declare the instance newPlayer
		private Player52BO newPlayer;
		// Declare the instance players
		private List players;
			/**
	 * Operation : lnk_addPlayer
 	 * @param model : 
 	 * @param uC52_Conditional : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC52_Conditional/lnk_addPlayer.html",method = RequestMethod.POST)
	public String lnk_addPlayer(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC52_ConditionalForm") UC52_ConditionalForm  uC52_ConditionalForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : lnk_addPlayer"); 
		infoLogger(uC52_ConditionalForm); 
		uC52_ConditionalForm.setAlwaysCallPreControllers(false); 
		// initialization for lnk_addPlayer
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state lnk_addPlayer if the validation fail, you will be forward to the screen : "/presentation/standardfeatures/services/process/uc52_conditional/UC52_Conditional"
		if(errorsMessages(request,response,uC52_ConditionalForm,bindingResult,"", uC52_ConditionalValidator, customDateEditorsUC52_ConditionalController)){ 
			return "/presentation/standardfeatures/services/process/uc52_conditional/UC52_Conditional"; 
		}

					 callAddnewplayer(request,uC52_ConditionalForm );
						// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.services.process.uc52_conditional.UC52_ConditionalForm or not from lnk_addPlayer
			// Populate the destination form with all the instances returned from lnk_addPlayer.
			final IForm uC52_ConditionalForm2 = populateDestinationForm(request.getSession(), uC52_ConditionalForm); 
					
			// Add the form to the model.
			model.addAttribute("uC52_ConditionalForm", uC52_ConditionalForm2); 
			
			request.getSession().setAttribute("uC52_ConditionalForm", uC52_ConditionalForm2);
			
			// "OK" CASE => destination screen path from lnk_addPlayer
			LOGGER.info("Go to the screen 'UC52_Conditional'.");
			// Redirect (PRG) from lnk_addPlayer
			destinationPath =  "redirect:/presentation/standardfeatures/services/process/uc52_conditional/UC52_Conditional.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_addPlayer 
	}
	
				/**
	 * Operation : lnk_showPlayers
 	 * @param model : 
 	 * @param uC52_Conditional : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC52_Conditional/lnk_showPlayers.html",method = RequestMethod.POST)
	public String lnk_showPlayers(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC52_ConditionalForm") UC52_ConditionalForm  uC52_ConditionalForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : lnk_showPlayers"); 
		infoLogger(uC52_ConditionalForm); 
		uC52_ConditionalForm.setAlwaysCallPreControllers(false); 
		// initialization for lnk_showPlayers
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state lnk_showPlayers if the validation fail, you will be forward to the screen : "/presentation/standardfeatures/services/process/uc52_conditional/UC52_Conditional"
		if(errorsMessages(request,response,uC52_ConditionalForm,bindingResult,"", uC52_ConditionalValidator, customDateEditorsUC52_ConditionalController)){ 
			return "/presentation/standardfeatures/services/process/uc52_conditional/UC52_Conditional"; 
		}

	 callFindallplayers(request,uC52_ConditionalForm );
						// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.services.process.uc52_conditional.UC52_ConditionalForm or not from lnk_showPlayers
			// Populate the destination form with all the instances returned from lnk_showPlayers.
			final IForm uC52_ConditionalForm2 = populateDestinationForm(request.getSession(), uC52_ConditionalForm); 
					
			// Add the form to the model.
			model.addAttribute("uC52_ConditionalForm", uC52_ConditionalForm2); 
			
			request.getSession().setAttribute("uC52_ConditionalForm", uC52_ConditionalForm2);
			
			// "OK" CASE => destination screen path from lnk_showPlayers
			LOGGER.info("Go to the screen 'UC52_Conditional'.");
			// Redirect (PRG) from lnk_showPlayers
			destinationPath =  "redirect:/presentation/standardfeatures/services/process/uc52_conditional/UC52_Conditional.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_showPlayers 
	}
	
	
	/**
	* This method initialise the form : UC52_ConditionalForm 
	* @return UC52_ConditionalForm
	*/
	@ModelAttribute("UC52_ConditionalFormInit")
	public void initUC52_ConditionalForm(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, U_C52__CONDITIONAL_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method UC52_ConditionalForm."); //for lnk_showPlayers 
		}
		UC52_ConditionalForm uC52_ConditionalForm;
	
		if(request.getSession().getAttribute(U_C52__CONDITIONAL_FORM) != null){
			uC52_ConditionalForm = (UC52_ConditionalForm)request.getSession().getAttribute(U_C52__CONDITIONAL_FORM);
		} else {
			uC52_ConditionalForm = new UC52_ConditionalForm();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form UC52_ConditionalForm.");
		}
		uC52_ConditionalForm = (UC52_ConditionalForm)populateDestinationForm(request.getSession(), uC52_ConditionalForm);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  UC52_ConditionalForm.");
		}
		model.addAttribute(U_C52__CONDITIONAL_FORM, uC52_ConditionalForm);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param UC52_ConditionalForm : The sceen form.
	 */
	@RequestMapping(value = "/UC52_Conditional.html" ,method = RequestMethod.GET)
	public void prepareUC52_Conditional(final HttpServletRequest request,final  Model model,final  @ModelAttribute("UC52_ConditionalFormInit") UC52_ConditionalForm uC52_ConditionalForm){
		
		UC52_ConditionalForm currentUC52_ConditionalForm = uC52_ConditionalForm;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(U_C52__CONDITIONAL_FORM) == null){
			if(currentUC52_ConditionalForm!=null){
				request.getSession().setAttribute(U_C52__CONDITIONAL_FORM, currentUC52_ConditionalForm);
			}else {
				currentUC52_ConditionalForm = new UC52_ConditionalForm();
				request.getSession().setAttribute(U_C52__CONDITIONAL_FORM, currentUC52_ConditionalForm);	
			}
		} else {
			currentUC52_ConditionalForm = (UC52_ConditionalForm) request.getSession().getAttribute(U_C52__CONDITIONAL_FORM);
		}

					currentUC52_ConditionalForm = (UC52_ConditionalForm)populateDestinationForm(request.getSession(), currentUC52_ConditionalForm);
		// Call all the Precontroller.
	currentUC52_ConditionalForm.setAlwaysCallPreControllers(true);
		model.addAttribute(U_C52__CONDITIONAL_FORM, currentUC52_ConditionalForm);
		request.getSession().setAttribute(U_C52__CONDITIONAL_FORM, currentUC52_ConditionalForm);
		
	}
	
							/**
	 * method callAddnewplayer
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callAddnewplayer(HttpServletRequest request,IForm form) {
					newPlayer = (Player52BO)get(request.getSession(),form, "newPlayer");  
										 
					try {
				// executing Addnewplayer in lnk_showPlayers
	servicePlayer52.addNewPlayer(
			newPlayer
			);  

								// processing variables Addnewplayer in lnk_showPlayers
				put(request.getSession(), NEW_PLAYER,newPlayer); 
			
			} catch (ApplicationException e) { 
				// error handling for operation addNewPlayer called Addnewplayer
				errorLogger("An error occured during the execution of the operation : addNewPlayer",e); 
		
			}
	}
		/**
	 * method callFindallplayers
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callFindallplayers(HttpServletRequest request,IForm form) {
		players = (List)get(request.getSession(),form, "players");  
										 
					try {
				// executing Findallplayers in lnk_showPlayers
				players = 	servicePlayer52FindAll.player52FindAll(
	);  
 
				put(request.getSession(), PLAYERS,players);
								// processing variables Findallplayers in lnk_showPlayers

			} catch (ApplicationException e) { 
				// error handling for operation player52FindAll called Findallplayers
				errorLogger("An error occured during the execution of the operation : player52FindAll",e); 
		
			}
	}
		
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("UC52_ConditionalController [ ");
							strBToS.append(NEW_PLAYER);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(newPlayer);
			strBToS.append(END_TO_STRING_DELIMITER);
			strBToS.append(PLAYERS);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(players);
			strBToS.append(END_TO_STRING_DELIMITER);
		// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(uC52_ConditionalValidator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsUC52_ConditionalController!=null); 
		return strBToS.toString();
	}
}
