/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.business.gettingstarted.uc01_helloworld.bos;

import javax.xml.bind.annotation.XmlRootElement;

import com.bluage.documentation.business.AbstractBusinessObject;

/**
 * HelloWorld01BO
 *
* @see com.netfective.bluage.core.business.IBusinessObject
 */
@XmlRootElement

public class HelloWorld01BO
 extends AbstractBusinessObject {
	
	/**
	 * Default SUID for HelloWorld01BO.
	 */
	private static final long serialVersionUID = 1L;
	

						/**
	 * Entity property: text
	 *
	 */
private String text;



   /**
	* public default constructor for class HelloWorld01BO
	*/
	public HelloWorld01BO(){
		//NOP HelloWorld01BO
	}


						/**
	 * Getter for text of class HelloWorld01BO.
	 *
	 * @return {@link java.lang.String}
	 * @generated
	 */ 
public  String getText() {
	return this.text;
	}


	/**
	 * Setter for text of class HelloWorld01BO.
	 * @param value {@link java.lang.String}
	 * @generated
	 */

public  void setText(final String text) {
	this.text = text;
}


}

