/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.service.advancedfeatures.valueobject.uc206_valueobject;


import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;

import com.bluage.documentation.business.advancedfeatures.valueobject.uc206_valueobject.bos.Player206BO;
import com.bluage.documentation.business.advancedfeatures.valueobject.uc206_valueobject.entities.dao.Player206DAOImpl;
import com.bluage.documentation.business.advancedfeatures.valueobject.uc206_valueobject.entities.daofinder.Player206DAOFinderImpl;
import com.bluage.documentation.service.AbstractService;
import com.bluage.documentation.service.advancedfeatures.valueobject.uc206_valueobject.daofinder.ServicePlayer206DAOMethodFinderImpl;
import com.netfective.bluage.core.exception.ApplicationException;

/**
 * Defines  application services for ServicePlayer206
  */
@Named("com.bluage.documentation.service.advancedfeatures.valueobject.uc206_valueobject.ServicePlayer206")
public class ServicePlayer206  extends AbstractService{


			@Inject
		@Named ("com.bluage.documentation.business.advancedfeatures.valueobject.uc206_valueobject.entities.dao.Player206DAOImpl")
		private Player206DAOImpl player206BODao;
			@Inject
		@Named ("com.bluage.documentation.business.advancedfeatures.valueobject.uc206_valueobject.entities.daofinder.Player206DAOFinderImpl")
		private Player206DAOFinderImpl player206BOFinder;
		@Inject
		@Named ("com.bluage.documentation.service.advancedfeatures.valueobject.uc206_valueobject.daofinder.ServicePlayer206DAOMethodFinder")
		private ServicePlayer206DAOMethodFinderImpl servicePlayer206Dao;



	/**
	 *  Default constructor 
	 */
	public ServicePlayer206() {
		super(); // Call super for default constructor : ServicePlayer206
	}
	


			
				
	/**    
	 * Operation Crud
	 * @param bo {@link Player206BO}
		 * @return {@link Player206BO}
	 */
	public Player206BO player206FindByID(Player206BO bo )
 throws ApplicationException {

					return (Player206BO)player206BOFinder.findByPrimaryKey(getBoId(bo));
	}
	/**
	 *  Return the primary key from business object.
	 */
	private Object getBoId(Player206BO bo) {
				return bo.getId();
		}

					
								
	/**    
	 * Operation Crud
	 * @param bo {@link Player206BO}
		 * @return {@link List}
	 */
	public List player206FindByProperties(Player206BO bo )
 throws ApplicationException {

					return player206BOFinder.findByProperties(boToCriterias(bo));
	}
	/**
	 * Transform a business object into criteria.
	 * @param boInstance {@link Player206BO} business object instance.
	 * @return a Map containing the search criteria.
	 */
	private Map<String, Object> boToCriterias(Player206BO boInstance) {
		Map<String, Object> criterias = new Hashtable<String, Object>();
    	boInstance.setObjMode("DAO");    	
	    if(boInstance.getPosition() != null && boInstance.getPosition().getCode() != null) {
    		criterias.put("position", boInstance.getPosition());    		
    	}
											    	if(boInstance.getId() != null) {
    		criterias.put("id", boInstance.getId());
    	}
		    	if(boInstance.getFirstName() != null && !"".equals(boInstance.getFirstName())) {
			criterias.put("firstName",boInstance.getFirstName());
		}
		    	if(boInstance.getLastName() != null && !"".equals(boInstance.getLastName())) {
			criterias.put("lastName",boInstance.getLastName());
		}
		    	if(boInstance.getDateOfBirth() != null) {
    		criterias.put("dateOfBirth", boInstance.getDateOfBirth());
    	}
		    	if(boInstance.getEstimatedValue() != null) {
    		criterias.put("estimatedValue", boInstance.getEstimatedValue());
    	}
		    	if(boInstance.getHeight() != null) {
    		criterias.put("height", boInstance.getHeight());
    	}
		    	if(boInstance.getWeight() != null) {
    		criterias.put("weight", boInstance.getWeight());
    	}
		    	if(boInstance.getRookie() != null) {
    		criterias.put("rookie", boInstance.getRookie());
    	}
		 		return criterias;
	}				

					
				
	/**    
	 * Operation Crud
	 * @param bo {@link Player206BO}
		 * @return {@link boolean}
	 */
	public boolean player206Update(Player206BO bo )
 throws ApplicationException {

					return player206BODao.update(bo);
	}

					
				
	/**    
	 * Operation Crud
	 * @param bo {@link Player206BO}
		 * @return {@link boolean}
	 */
	public boolean player206Create(Player206BO bo )
 throws ApplicationException {

					return player206BODao.create(bo);
	}

		
								
	/**    
	 * Operation Crud
	 * @return {@link List}
	 */
	public List player206FindAll( )
 throws ApplicationException {

					return player206BOFinder.findAll();
	}

		
				
	/**    
	 * Operation HQL.
	 * @param lastName {@link String}
		 * @return {@link List}
	 */
	public List player206FindByCriteria(String lastName )
 throws ApplicationException {
	
	return servicePlayer206Dao.player206FindByCriteria(lastName);
	}

		}
