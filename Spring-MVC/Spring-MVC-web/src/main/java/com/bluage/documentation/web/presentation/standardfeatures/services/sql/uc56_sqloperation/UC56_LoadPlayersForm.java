/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.services.sql.uc56_sqloperation;


import java.io.Serializable;
import java.util.List;

import com.bluage.documentation.business.standardfeatures.services.sql.uc56_sqloperation.bos.Player56BO;
import com.bluage.documentation.business.standardfeatures.services.sql.uc56_sqloperation.bos.Team56BO;
import com.netfective.bluage.core.web.AbstractForm;
import com.netfective.bluage.core.web.IForm;

/**
* Class : UC56_LoadPlayersForm
*/
public class UC56_LoadPlayersForm extends AbstractForm implements Serializable, IForm {
	
	/** Constant : serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	//CONSTANT : players
	private static final String PLAYERS = "players";
	//CONSTANT : team
	private static final String TEAM = "team";
	/**
	 * 	Property: players 
	 */
	private List<Player56BO> players;
	/**
	 * 	Property: team 
	 */
	private Team56BO team;
/**
	 * Default constructor : UC56_LoadPlayersForm
	 */
	public UC56_LoadPlayersForm() {
		super();
		// Initialize : players
		this.players = new java.util.ArrayList<com.bluage.documentation.business.standardfeatures.services.sql.uc56_sqloperation.bos.Player56BO>();
		// Initialize : team
		this.team = new Team56BO();
					this.setAlwaysCallPreControllers(true);
	}
	
	/**
	 * 	Getter : players 
	 * 	@return : Return the players instance.
	 */
	public List<Player56BO> getPlayers(){
		return players; // For UC56_LoadPlayersForm
	}
	
	/**
	 * 	Setter : players 
	 *  @param playersinstance : The instance to set.
	 */
	public void setPlayers(final List<Player56BO> playersinstance){
		this.players = playersinstance;// For UC56_LoadPlayersForm
	}
	/**
	 * 	Getter : team 
	 * 	@return : Return the team instance.
	 */
	public Team56BO getTeam(){
		return team; // For UC56_LoadPlayersForm
	}
	
	/**
	 * 	Setter : team 
	 *  @param teaminstance : The instance to set.
	 */
	public void setTeam(final Team56BO teaminstance){
		this.team = teaminstance;// For UC56_LoadPlayersForm
	}
/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UC56_LoadPlayersForm [ "+
PLAYERS +" = " + players +TEAM +" = " + team + "]";
	}
	
	/**
	 * Method setInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public void setInstanceByInstanceName(final String instanceName, final Object instance) {
		// Set the instance Players.
		if(PLAYERS.equals(instanceName)){// For UC56_LoadPlayersForm
			this.setPlayers((List<Player56BO>)instance); // For UC56_LoadPlayersForm
		}
				// Set the instance Team.
		if(TEAM.equals(instanceName)){// For UC56_LoadPlayersForm
			this.setTeam((Team56BO)instance); // For UC56_LoadPlayersForm
		}
				// Parent instance name for UC56_LoadPlayersForm
		if(TEAM.equals(instanceName) && team != null){ // For UC56_LoadPlayersForm
			players.clear(); // For UC56_LoadPlayersForm
			players.addAll(team.getPlayers());// For UC56_LoadPlayersForm
		}
		
	}
	
	/**
	 * Method getInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public Object getInstanceByInstanceName(final String instanceName){
		// Temporary object for tmpUC56_LoadPlayersForm.
		Object tmpUC56_LoadPlayersForm = null;
		
			
		// Get the instance Players for UC56_LoadPlayersForm.
		if(PLAYERS.equals(instanceName)){ // For UC56_LoadPlayersForm
			tmpUC56_LoadPlayersForm = this.getPlayers(); // For UC56_LoadPlayersForm
		}
			
		// Get the instance Team for UC56_LoadPlayersForm.
		if(TEAM.equals(instanceName)){ // For UC56_LoadPlayersForm
			tmpUC56_LoadPlayersForm = this.getTeam(); // For UC56_LoadPlayersForm
		}
		return tmpUC56_LoadPlayersForm;// For UC56_LoadPlayersForm
	}
	
			}
