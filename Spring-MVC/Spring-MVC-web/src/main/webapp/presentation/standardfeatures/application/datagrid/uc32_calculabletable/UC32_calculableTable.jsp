<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file="/include.jsp"  %>
<html >
<head >
<title  > 
<fmt:message key="cUC32_calculableTable_BLU_AGE_DOCUMENTATION"/>
</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"  > 
</meta>
<link href="<c:url value= "/presentation/_style/_style.css"/>" rel="stylesheet" type="text/css"  /> 
<link href="<c:url value= "/presentation/_include/jquery/rd_lib.css"/>" rel="stylesheet" type="text/css"  /> 
<script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/jquery.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/rd_setup.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/rd_lib.js"/>" space="preserve"  ></script><script type="text/javascript" space="preserve"  >   $(document).ready(function(){    var shots;    var nbScored;    var efficacy;    var colAccuracy;        $("[id*=tab_players] tbody tr").each(function() {     shots = $(this).find("[id*=dyn_shots]").html();     nbScored = $(this).find("[id*=dyn_scored]").html();     colAccuracy = $(this).find("[id*=dyn_efficacy]").html();     rookie = $(this).find("[id*=dyn_isRookie]").html();     if (shots==0)      $(this).find("[id*=dyn_efficacy]").html(" ");     else      {     efficacy = (((nbScored/shots)*100));     efficacy = parseInt(efficacy*100 + .5) / 100;           if(nbScored != null)      $(this).find("[id*=dyn_efficacy]").html(efficacy);     else      $(this).find("[id*=dyn_efficacy]").html(" ");      }          })   }); 
</script><link rel="shortcut icon" href="<c:url value= "/presentation/_style/favicon.ico"/>"  /> 

</head>

<body class=" " > 
<div id="rd_ghost"  > 
<small  > 
<fmt:message key="cUC32_calculableTable___CSS_DEACTIVATED"/>
</small>
</div>
<div id="rd_jghost"  > 
<small  > 
<fmt:message key="cUC32_calculableTable___JAVASCRIPT_DEACTIVATED"/>
</small>
</div>
<div id="shape"  > 
<div class="ui-layout-center"  > 
<div class="east-north"  > 
	<form:form id="uC32_calculableTable" commandName="uC32_calculableTableForm"  enctype="application/x-www-form-urlencoded" method="post">
<h2 class="rd_first"  > 
<fmt:message key="cUC32_calculableTable_STANDARD_FEATURES__APPLICATIO"/>
</h2>
<display:table uid="tab_players"  sort="list" requestURI="" name="sessionScope.uC32_calculableTableForm.allPlayers" class="rd_tabledata"  >
	<c:set   var="rownum">
		<%=(tab_players !=	null) ? new Integer(tab_players_rowNum.intValue() - 1) : new Integer(0) %>
	</c:set>
	<c:set   var="idRow">
		<%=(tab_players != null ) ? tab_players_rowNum.intValue() - 1 : -1 %>
	</c:set>
	<c:set   var="idTab">
		<%=(tab_players != null ) ? "tab_players" : null %>
	</c:set>
	<c:set var="tableHashId">
		<%= new org.displaytag.util.ParamEncoder("tab_players").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE) %>
	</c:set>
	<c:set var="tableHashId2">
		<%=request.getParameter(new org.displaytag.util.ParamEncoder("tab_players").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE)) %>
	</c:set>	
	
<display:column format=""  titleKey="cUC32_calculableTable_NAME">
		<span id="dyn_lastname">
	<c:out value="${tab_players.lastName}" />
		</span>
	<fmt:message key="cUC32_calculableTable_____"/>
		<span id="dyn_firstname">
	<c:out value="${tab_players.firstName}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="cUC32_calculableTable_DATE_OF_BIRTH">
<span id="dyn_dateofbirth">
	<format:formatDateValidator value="${tab_players.dateOfBirth}" pattern="MM/dd/yyyy" />
</span>
	</display:column>
	
<display:column format=""  titleKey="cUC32_calculableTable_ESTIMATED_VALUE_M">
		<span id="dyn_value">
	<c:out value="${tab_players.estimatedValue}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="cUC32_calculableTable_ROOKIE">
<form:checkbox id="chk_isRookie[${rownum}]" path="allPlayers[${rownum}].rookie"  name="isRookie" disabled="true"  
		/>
</display:column>
	
<display:column format=""  titleKey="cUC32_calculableTable_POSITION">
		<span id="dyn_position">
	<c:out value="${tab_players.position.code}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="cUC32_calculableTable_SHOTS">
<c:if test="${uC32_calculableTableForm.selectedRow!=idRow || uC32_calculableTableForm.selectedTab!=idTab}">
		<span id="dyn_shots">
	<c:out value="${tab_players.playedGames}" />
		</span>
	</c:if>
<c:if test="${uC32_calculableTableForm.selectedRow==idRow && uC32_calculableTableForm.selectedTab==idTab}">
<form:input id="txt_shots[${rownum}]" path="allPlayers[${rownum}].playedGames"  
		/>
</c:if>
</display:column>
	
<display:column format=""  titleKey="cUC32_calculableTable_SCORED">
<c:if test="${uC32_calculableTableForm.selectedRow!=idRow || uC32_calculableTableForm.selectedTab!=idTab}">
		<span id="dyn_scored">
	<c:out value="${tab_players.numberOfBaskets}" />
		</span>
	</c:if>
<c:if test="${uC32_calculableTableForm.selectedRow==idRow && uC32_calculableTableForm.selectedTab==idTab}">
<form:input id="txt_scored[${rownum}]" path="allPlayers[${rownum}].numberOfBaskets"  
		/>
</c:if>
</display:column>
	
<display:column format=""  titleKey="cUC32_calculableTable_ACCURACY_">
		<span id="dyn_efficacy">
	<c:out value="${tab_players.efficacy}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="cUC32_calculableTable_">
<c:if test="${uC32_calculableTableForm.selectedRow!=idRow || uC32_calculableTableForm.selectedTab!=idTab}">
<a id="lnk_edit[${rownum}]"onclick="javascript:btn_click_cExecuteUC32_calculableTable(this.href);return false;"			href="<c:url value="/presentation/standardfeatures/application/datagrid/uc32_calculabletable/UC32_calculableTable/lnk_edit.html?id="/><c:out value="${idRow}" />&tab=<c:out value="tab_players" />&<c:out value="${tableHashId}"/>=<c:out value="${tableHashId2}"/>"
	 shape="rect"  >									 
<fmt:message key="cUC32_calculableTable_EDIT"/>
</a>
</c:if>
<c:if test="${uC32_calculableTableForm.selectedRow==idRow && uC32_calculableTableForm.selectedTab==idTab}">
<a id="lnk_update[${rownum}]"onclick="javascript:btn_click_cExecuteUC32_calculableTable(this.href);return false;"			href="<c:url value="/presentation/standardfeatures/application/datagrid/uc32_calculabletable/UC32_calculableTable/lnk_update.html?id="/><c:out value="${idRow}" />&tab=<c:out value="tab_players" />&<c:out value="${tableHashId}"/>=<c:out value="${tableHashId2}"/>"
	 shape="rect"  >									 
<fmt:message key="cUC32_calculableTable_UPDATE"/>
</a>
</c:if>
</display:column>
</display:table>
	</form:form>
	<script language="javascript">
		function btn_click_cExecuteUC32_calculableTable(url) {
			UC32_calculableTablevar = document.getElementById('uC32_calculableTable');
			UC32_calculableTablevar.setAttribute('action', url);
			UC32_calculableTablevar.submit();
		}
		function btn_click_cSetActionUC32_calculableTable(url) {
			UC32_calculableTablevar = document.getElementById('uC32_calculableTable');
			UC32_calculableTablevar.setAttribute('action', url);
		}
	</script>
</div>
<div class="east-center"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/application/datagrid/uc32_calculabletable/UC32_calculableTablecmp_tabs" />
</div>
</div>
<div class="ui-layout-north"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/application/datagrid/uc32_calculabletable/UC32_calculableTablecmp_header" />
</div>
<div class="ui-layout-south"  > 
<fmt:message key="cUC32_calculableTable___"/>
<span id="rd_jversion"  > 
<fmt:message key="cUC32_calculableTable_BLU_AGE_CORPORATION"/>
</span>
</div>
<div class="ui-layout-west"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/application/datagrid/uc32_calculabletable/UC32_calculableTablecmp_treeview" />
</div>
</div>
</body>
</html>
