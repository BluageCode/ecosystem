ALTER TABLE player49 DROP CONSTRAINT player49_team_fkc;

ALTER TABLE player49 DROP CONSTRAINT player49_pkey;

ALTER TABLE team49 DROP CONSTRAINT team49_pkey;

DROP TABLE player49;

DROP TABLE team49;



CREATE TABLE player49 (
	id bigint NOT NULL,
	version integer NOT NULL,
	first_name character varying(30),
	last_name character varying(30),
	date_of_birth date,
	estimated_value real,
	weight integer, 
	height integer, 
	rookie boolean, 
	team_fk bigint
);



ALTER TABLE ONLY player49
    ADD CONSTRAINT player49_pkey PRIMARY KEY (id);
  
CREATE TABLE team49 (
    id bigint NOT NULL,
    version integer NOT NULL,
    city character varying(30),
    name character varying(30) UNIQUE
);



ALTER TABLE ONLY team49
    ADD CONSTRAINT team49_pkey PRIMARY KEY (id);

ALTER TABLE ONLY player49
    ADD CONSTRAINT player49_team_fkc FOREIGN KEY (team_fk) REFERENCES team49(id);

INSERT INTO team49 VALUES (1, 0, 'Charlotte', 'Tigers');
INSERT INTO team49 VALUES (2, 0, 'Kansas City', 'Bears');
INSERT INTO team49 VALUES (3, 0, 'Detroit', 'Chargers');
INSERT INTO team49 VALUES (4, 0, 'Detroit', 'Hawks');
INSERT INTO team49 VALUES (5, 0, 'Omaha', 'Braves');

INSERT INTO player49 VALUES (287, 0, 'Owen', 'Little', '1981-10-27', 45.869999, 95, 183, false, 5);
INSERT INTO player49 VALUES (288, 0, 'Irvin', 'Lee', '1988-02-25', 10.61, 88, 187, false, 1);
INSERT INTO player49 VALUES (289, 0, 'Wayne', 'Rose', '1981-10-28', 20.41, 115, 218, false, 1);
INSERT INTO player49 VALUES (290, 0, 'Lucas', 'Williamson', '1983-06-29', 38.360001, 73, 181, false, 1);
INSERT INTO player49 VALUES (291, 0, 'Walter', 'Reyes', '1984-12-09', 46.150002, 104, 193, false, 1);
INSERT INTO player49 VALUES (292, 0, 'Kirk', 'Cox', '1973-05-10', 26.58, 70, 179, false, 3);
INSERT INTO player49 VALUES (293, 0, 'Arthur', 'Payne', '1981-11-20', 30.860001, 86, 190, false, 4);
INSERT INTO player49 VALUES (294, 0, 'Leroy', 'Moore', '1987-07-26', 45.799999, 89, 183, false, 2);
INSERT INTO player49 VALUES (295, 0, 'Milton', 'Johnson', '1969-07-02', 22.219999, 94, 205, false, 5);
INSERT INTO player49 VALUES (296, 0, 'Sammy', 'Mendez', '1986-07-01', 2.27, 96, 234, false, 3);
INSERT INTO player49 VALUES (297, 0, 'Rafael', 'Steele', '1968-11-25', 15.48, 76, 183, false, 1);
INSERT INTO player49 VALUES (298, 0, 'Bob', 'Jacobs', '1988-05-21', 13.23, 72, 172, false, 2);
INSERT INTO player49 VALUES (299, 0, 'Jared', 'Stevenson', '1974-05-03', 12.11, 105, 187, false, 4);
INSERT INTO player49 VALUES (300, 0, 'Shannon', 'Barton', '1980-03-10', 23.809999, 77, 197, false, 3);
INSERT INTO player49 VALUES (206, 0, 'Nick', 'Garner', '1992-07-27', 47.84, 112, 221, true, 2);
INSERT INTO player49 VALUES (213, 0, 'Tommy', 'Wood', '1992-01-20', 13.93, 77, 173, true, 2);
INSERT INTO player49 VALUES (235, 0, 'Angel', 'Turner', '1990-08-17', 16.85, 84, 209, true, 4);
INSERT INTO player49 VALUES (238, 0, 'Brad', 'Houston', '1992-02-28', 33.389999, 81, 181, true, 5);
INSERT INTO player49 VALUES (301, 0, 'Erick', 'Mccarthy', '1970-12-07', 3.76, 85, 190, false, 5);
INSERT INTO player49 VALUES (302, 0, 'Justin', 'Johnson', '1988-02-27', 47.950001, 137, 201, false, 2);
INSERT INTO player49 VALUES (303, 0, 'Elmer', 'Hansen', '1984-07-12', 12.86, 88, 213, false, 4);
INSERT INTO player49 VALUES (304, 0, 'Everett', 'Sherman', '1978-11-03', 23.280001, 118, 213, false, 2);
INSERT INTO player49 VALUES (305, 0, 'Rafael', 'Ward', '1967-02-08', 38.77, 91, 178, false, 1);
INSERT INTO player49 VALUES (306, 0, 'Kenny', 'Rogers', '1965-02-02', 5.4400001, 78, 183, false, 1);
INSERT INTO player49 VALUES (308, 0, 'Lonnie', 'Brady', '1987-12-11', 11.83, 107, 219, false, 2);
INSERT INTO player49 VALUES (309, 0, 'Sergio', 'Taylor', '1984-08-29', 25.290001, 133, 225, false, 4);
INSERT INTO player49 VALUES (310, 0, 'Byron', 'Morton', '1981-03-28', 21.4, 104, 196, false, 1);
INSERT INTO player49 VALUES (311, 0, 'Bob', 'Estrada', '1975-10-20', 39.59, 98, 197, false, 1);
INSERT INTO player49 VALUES (312, 0, 'Frank', 'Strickland', '1970-06-27', 21.879999, 116, 224, false, 2);
INSERT INTO player49 VALUES (313, 0, 'Johnathan', 'Norman', '1978-08-20', 17.620001, 117, 214, false, 5);
INSERT INTO player49 VALUES (314, 0, 'Leonard', 'Stephens', '1965-12-02', 13.12, 100, 188, false, 2);
INSERT INTO player49 VALUES (315, 0, 'Tim', 'Taylor', '1985-01-26', 8.1599998, 106, 197, false, 2);
INSERT INTO player49 VALUES (316, 0, 'Bobby', 'Weaver', '1979-11-25', 3.1099999, 99, 227, false, 5);
INSERT INTO player49 VALUES (318, 0, 'Bernard', 'Wise', '1970-02-01', 48.48, 66, 219, false, 5);
INSERT INTO player49 VALUES (319, 0, 'Rodolfo', 'Wong', '1968-08-10', 26.969999, 84, 191, false, 1);
INSERT INTO player49 VALUES (320, 0, 'Matthew', 'Haynes', '1982-07-16', 18.91, 102, 218, true, 1);
INSERT INTO player49 VALUES (321, 0, 'Clayton', 'Doyle', '1976-12-10', 36.240002, 87, 177, true, 2);
INSERT INTO player49 VALUES (322, 0, 'Floyd', 'Hampton', '1988-03-03', 21.67, 85, 178, false, 3);
INSERT INTO player49 VALUES (323, 0, 'Timmy', 'Walker', '1969-04-14', 45.91, 109, 190, false, 4);
INSERT INTO player49 VALUES (324, 0, 'Jon', 'Hines', '1980-12-27', 24.879999, 88, 195, false, 5);
INSERT INTO player49 VALUES (325, 0, 'Orlando', 'Burns', '1973-09-02', 19.139999, 109, 194, false, 1);
INSERT INTO player49 VALUES (326, 0, 'Randal', 'Mendez', '1974-03-04', 21.200001, 91, 195, false, 2);
INSERT INTO player49 VALUES (327, 0, 'Jeremy', 'Holmes', '1983-12-08', 38.299999, 118, 224, false, 3);
INSERT INTO player49 VALUES (330, 0, 'Shannon', 'Watkins', '1979-02-07', 25.66, 99, 198, false, 4);
INSERT INTO player49 VALUES (331, 0, 'Ralph', 'Burton', '1969-11-13', 7.52, 85, 185, false, 5);
INSERT INTO player49 VALUES (332, 0, 'Bobby', 'Jefferson', '1985-10-06', 17.4, 128, 220, false, 1);
INSERT INTO player49 VALUES (333, 0, 'Travis', 'Williamson', '1974-08-23', 20.299999, 115, 194, false, 2);
INSERT INTO player49 VALUES (334, 0, 'Wayne', 'Clarke', '1965-07-24', 24.9, 114, 204, false, 3);
INSERT INTO player49 VALUES (335, 0, 'Saul', 'Zimmerman', '1988-12-28', 45.82, 109, 201, false, 4);
INSERT INTO player49 VALUES (337, 0, 'Bradford', 'Porter', '1977-06-02', 16.190001, 111, 214, true, 5);
INSERT INTO player49 VALUES (339, 0, 'Bernard', 'Roy', '1987-05-07', 25.15, 95, 192, true, 1);
