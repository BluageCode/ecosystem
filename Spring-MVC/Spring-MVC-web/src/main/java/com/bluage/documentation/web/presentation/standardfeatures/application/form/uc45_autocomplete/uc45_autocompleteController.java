/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.application.form.uc45_autocomplete ;

// Import declaration.
// Java imports.
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.bluage.documentation.business.standardfeatures.application.form.uc45_autocomplete.bos.Player45BO;
import com.bluage.documentation.business.standardfeatures.application.form.uc45_autocomplete.entities.daofinder.Player45DAOFinderImpl;
import com.netfective.bluage.core.exception.ApplicationException;
import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;

/**
 * Class : uc45_autocompleteController
 */
@Controller
@Scope("prototype")
@SessionAttributes("uc45_autocompleteForm")
@RequestMapping(value= "/presentation/standardfeatures/application/form/uc45_autocomplete")
public class uc45_autocompleteController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(uc45_autocompleteController.class);
	
	//Current form name
	private static final String UC45_AUTOCOMPLETE_FORM = "uc45_autocompleteForm";

	//CONSTANT: name
	private static final String NAME = "name";
		//CONSTANT: instance_players
	private static final String INSTANCE_PLAYERS = "instance_players";
				
	/**
	 * Property:customDateEditorsuc45_autocompleteController
	 */
	final private Map<String,CustomDateEditor> customDateEditorsuc45_autocompleteController = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:uc45_autocompleteValidator
	 */
	final private uc45_autocompleteValidator uc45_autocompleteValidator = new uc45_autocompleteValidator();
	
	/**
	 * Generic Finder : player45DAOFinderImpl.
	 */
	@Autowired
	private Player45DAOFinderImpl player45DAOFinderImpl;
	
	
	// Initialise all the instances for uc45_autocompleteController
		// Initialize the instance name of type com.bluage.documentation.business.standardfeatures.application.form.uc45_autocomplete.bos.Player45BO with the value : null for uc45_autocompleteController
		private Player45BO name;
			// Initialize the instance instance_players for the state lnk_autocomplete
		private List instance_players; // Initialize the instance instance_players for uc45_autocompleteController
				
	/**
	* This method initialise the form : uc45_autocompleteForm 
	* @return uc45_autocompleteForm
	*/
	@ModelAttribute("uc45_autocompleteFormInit")
	public void inituc45_autocompleteForm(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, UC45_AUTOCOMPLETE_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method uc45_autocompleteForm."); //for lnk_autocomplete 
		}
		uc45_autocompleteForm uc45_autocompleteForm;
	
		if(request.getSession().getAttribute(UC45_AUTOCOMPLETE_FORM) != null){
			uc45_autocompleteForm = (uc45_autocompleteForm)request.getSession().getAttribute(UC45_AUTOCOMPLETE_FORM);
		} else {
			uc45_autocompleteForm = new uc45_autocompleteForm();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form uc45_autocompleteForm.");
		}
		uc45_autocompleteForm = (uc45_autocompleteForm)populateDestinationForm(request.getSession(), uc45_autocompleteForm);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  uc45_autocompleteForm.");
		}
		model.addAttribute(UC45_AUTOCOMPLETE_FORM, uc45_autocompleteForm);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param uc45_autocompleteForm : The sceen form.
	 */
	@RequestMapping(value = "/uc45_autocomplete.html" ,method = RequestMethod.GET)
	public void prepareuc45_autocomplete(final HttpServletRequest request,final  Model model,final  @ModelAttribute("uc45_autocompleteFormInit") uc45_autocompleteForm uc45_autocompleteForm){
		
		uc45_autocompleteForm currentuc45_autocompleteForm = uc45_autocompleteForm;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(UC45_AUTOCOMPLETE_FORM) == null){
			if(currentuc45_autocompleteForm!=null){
				request.getSession().setAttribute(UC45_AUTOCOMPLETE_FORM, currentuc45_autocompleteForm);
			}else {
				currentuc45_autocompleteForm = new uc45_autocompleteForm();
				request.getSession().setAttribute(UC45_AUTOCOMPLETE_FORM, currentuc45_autocompleteForm);	
			}
		} else {
			currentuc45_autocompleteForm = (uc45_autocompleteForm) request.getSession().getAttribute(UC45_AUTOCOMPLETE_FORM);
		}

		try {
			List instance_players = player45DAOFinderImpl.findAll();
			put(request.getSession(), INSTANCE_PLAYERS, instance_players);
		} catch (ApplicationException e) {
			errorLogger("An error while calling the generic finder for the instance : instance_players.",e);
		}
		currentuc45_autocompleteForm = (uc45_autocompleteForm)populateDestinationForm(request.getSession(), currentuc45_autocompleteForm);
		// Call all the Precontroller.
	currentuc45_autocompleteForm.setAlwaysCallPreControllers(true);
		model.addAttribute(UC45_AUTOCOMPLETE_FORM, currentuc45_autocompleteForm);
		request.getSession().setAttribute(UC45_AUTOCOMPLETE_FORM, currentuc45_autocompleteForm);
		
	}
	
	/**
	 * Operation : instance_playersautocomplete
	 * 
	 * @param model
	 *            : The Spring MVC Model
	 * @param uc45_autocompleteForm
	 *            : The form
	 * @return
	 */
	@RequestMapping(value = "/instance_playersautocomplete.html", method = RequestMethod.GET)
	public @ResponseBody String instance_playersautocomplete(final @ModelAttribute("uc45_autocompleteForm") uc45_autocompleteForm uc45_autocompleteForm,final  @RequestParam String query) {

		StringBuffer buffer = new StringBuffer();
		List<Player45BO> bos = uc45_autocompleteForm.getInstance_players();
		for (Player45BO bo : bos) {
			String firstName = bo.getFirstName();
			if (firstName != null && firstName.toLowerCase().startsWith(query.toLowerCase())) {	
				buffer.append(firstName).append(",");
			}
		}
		
		return buffer.toString();
	}
	
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("uc45_autocompleteController [ ");
			strBToS.append(NAME);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(name);
			strBToS.append(END_TO_STRING_DELIMITER);
				strBToS.append(INSTANCE_PLAYERS);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(instance_players);
			strBToS.append(END_TO_STRING_DELIMITER);
					// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(uc45_autocompleteValidator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsuc45_autocompleteController!=null); 
		return strBToS.toString();
	}
}
