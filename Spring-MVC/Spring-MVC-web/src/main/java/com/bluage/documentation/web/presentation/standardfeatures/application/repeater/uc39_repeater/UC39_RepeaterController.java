/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.application.repeater.uc39_repeater ;

// Import declaration.
// Java imports.
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;

/**
 * Class : UC39_RepeaterController
 */
@Controller
@Scope("prototype")
@SessionAttributes("uC39_RepeaterForm")
@RequestMapping(value= "/presentation/standardfeatures/application/repeater/uc39_repeater")
public class UC39_RepeaterController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC39_RepeaterController.class);
	
	//Current form name
	private static final String U_C39__REPEATER_FORM = "uC39_RepeaterForm";

	//CONSTANT: teams1 table or repeater.
	private static final String TEAMS1 = "teams1";
				//CONSTANT: teams3 table or repeater.
	private static final String TEAMS3 = "teams3";
				//CONSTANT: teams2 table or repeater.
	private static final String TEAMS2 = "teams2";
				
	/**
	 * Property:customDateEditorsUC39_RepeaterController
	 */
	final private Map<String,CustomDateEditor> customDateEditorsUC39_RepeaterController = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:uC39_RepeaterValidator
	 */
	final private UC39_RepeaterValidator uC39_RepeaterValidator = new UC39_RepeaterValidator();
	
	
	// Initialise all the instances for UC39_RepeaterController
		// Initialize the instance teams1 for the state lnk_loadTeams
		private List teams1; // Initialize the instance teams1 for UC39_RepeaterController
						// Initialize the instance teams3 for the state lnk_loadTeams
		private List teams3; // Initialize the instance teams3 for UC39_RepeaterController
						// Initialize the instance teams2 for the state lnk_loadTeams
		private List teams2; // Initialize the instance teams2 for UC39_RepeaterController
				
	/**
	* This method initialise the form : UC39_RepeaterForm 
	* @return UC39_RepeaterForm
	*/
	@ModelAttribute("UC39_RepeaterFormInit")
	public void initUC39_RepeaterForm(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, U_C39__REPEATER_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method UC39_RepeaterForm."); //for lnk_loadTeams 
		}
		UC39_RepeaterForm uC39_RepeaterForm;
	
		if(request.getSession().getAttribute(U_C39__REPEATER_FORM) != null){
			uC39_RepeaterForm = (UC39_RepeaterForm)request.getSession().getAttribute(U_C39__REPEATER_FORM);
		} else {
			uC39_RepeaterForm = new UC39_RepeaterForm();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form UC39_RepeaterForm.");
		}
		uC39_RepeaterForm = (UC39_RepeaterForm)populateDestinationForm(request.getSession(), uC39_RepeaterForm);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  UC39_RepeaterForm.");
		}
		model.addAttribute(U_C39__REPEATER_FORM, uC39_RepeaterForm);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param UC39_RepeaterForm : The sceen form.
	 */
	@RequestMapping(value = "/UC39_Repeater.html" ,method = RequestMethod.GET)
	public void prepareUC39_Repeater(final HttpServletRequest request,final  Model model,final  @ModelAttribute("UC39_RepeaterFormInit") UC39_RepeaterForm uC39_RepeaterForm){
		
		UC39_RepeaterForm currentUC39_RepeaterForm = uC39_RepeaterForm;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(U_C39__REPEATER_FORM) == null){
			if(currentUC39_RepeaterForm!=null){
				request.getSession().setAttribute(U_C39__REPEATER_FORM, currentUC39_RepeaterForm);
			}else {
				currentUC39_RepeaterForm = new UC39_RepeaterForm();
				request.getSession().setAttribute(U_C39__REPEATER_FORM, currentUC39_RepeaterForm);	
			}
		} else {
			currentUC39_RepeaterForm = (UC39_RepeaterForm) request.getSession().getAttribute(U_C39__REPEATER_FORM);
		}

		currentUC39_RepeaterForm = (UC39_RepeaterForm)populateDestinationForm(request.getSession(), currentUC39_RepeaterForm);
		// Call all the Precontroller.
	currentUC39_RepeaterForm.setAlwaysCallPreControllers(true);
		model.addAttribute(U_C39__REPEATER_FORM, currentUC39_RepeaterForm);
		request.getSession().setAttribute(U_C39__REPEATER_FORM, currentUC39_RepeaterForm);
		
	}
	
	
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("UC39_RepeaterController [ ");
			strBToS.append(TEAMS1);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(teams1);
			strBToS.append(END_TO_STRING_DELIMITER);
						strBToS.append(TEAMS3);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(teams3);
			strBToS.append(END_TO_STRING_DELIMITER);
						strBToS.append(TEAMS2);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(teams2);
			strBToS.append(END_TO_STRING_DELIMITER);
					// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(uC39_RepeaterValidator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsUC39_RepeaterController!=null); 
		return strBToS.toString();
	}
}
