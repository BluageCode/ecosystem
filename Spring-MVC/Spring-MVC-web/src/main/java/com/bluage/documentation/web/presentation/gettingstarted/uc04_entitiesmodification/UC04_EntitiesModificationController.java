/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.gettingstarted.uc04_entitiesmodification ;

// Import declaration.
// Java imports.
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.bluage.documentation.business.gettingstarted.uc04_entitiesmodification.bos.Player04BO;
import com.bluage.documentation.business.gettingstarted.uc04_entitiesmodification.entities.daofinder.Player04DAOFinderImpl;
import com.bluage.documentation.service.gettingstarted.uc04_entitiesmodification.ServicePlayer04;
import com.netfective.bluage.core.exception.ApplicationException;
import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;
import com.netfective.bluage.core.web.IForm;

/**
 * Class : UC04_EntitiesModificationController
 */
@Controller
@Scope("prototype")
@SessionAttributes("uC04_EntitiesModificationForm")
@RequestMapping(value= "/presentation/gettingstarted/uc04_entitiesmodification")
public class UC04_EntitiesModificationController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC04_EntitiesModificationController.class);
	
	//Current form name
	private static final String U_C04__ENTITIES_MODIFICATION_FORM = "uC04_EntitiesModificationForm";

	//CONSTANT: allPlayers table or repeater.
	private static final String ALL_PLAYERS = "allPlayers";
				//CONSTANT: playerToUpdate
	private static final String PLAYER_TO_UPDATE = "playerToUpdate";
	
	/**
	 * Property:customDateEditorsUC04_EntitiesModificationController
	 */
	final private Map<String,CustomDateEditor> customDateEditorsUC04_EntitiesModificationController = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:uC04_EntitiesModificationValidator
	 */
	final private UC04_EntitiesModificationValidator uC04_EntitiesModificationValidator = new UC04_EntitiesModificationValidator();
	
	/**
	 * Service declaration : servicePlayer04.
	 */
	@Autowired
	private ServicePlayer04 servicePlayer04;
	
	/**
	 * Generic Finder : player04DAOFinderImpl.
	 */
	@Autowired
	private Player04DAOFinderImpl player04DAOFinderImpl;
	
	
	// Initialise all the instances for UC04_EntitiesModificationController
		// Initialize the instance allPlayers for the state lnk_back
		private List allPlayers; // Initialize the instance allPlayers for UC04_EntitiesModificationController
						// Declare the instance playerToUpdate
		private Player04BO playerToUpdate;
			/**
	 * Operation : lnk_update
 	 * @param model : 
 	 * @param uC04_EntitiesModification : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC04_EntitiesModification/lnk_update.html",method = RequestMethod.POST)
	public String lnk_update(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC04_EntitiesModificationForm") UC04_EntitiesModificationForm  uC04_EntitiesModificationForm, final BindingResult bindingResult ,final @RequestParam(ID_REQUEST_PARAM) Integer index){
		
		
		
		LOGGER.info("Begin method : lnk_update"); 
		infoLogger(uC04_EntitiesModificationForm); 
		uC04_EntitiesModificationForm.setAlwaysCallPreControllers(false); 
		// initialization for lnk_update
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state lnk_update if the validation fail, you will be forward to the screen : "/presentation/gettingstarted/uc04_entitiesmodification/UC04_EntitiesModification"
		if(errorsMessages(request,response,uC04_EntitiesModificationForm,bindingResult,"playerToUpdate", uC04_EntitiesModificationValidator, customDateEditorsUC04_EntitiesModificationController)){ 
			return "/presentation/gettingstarted/uc04_entitiesmodification/UC04_EntitiesModification"; 
		}
		// Updating selected row for the state lnk_update 
		uC04_EntitiesModificationForm.setSelectedRow(-1); //Set selected row for lnk_update 
		// Updating selected table for the state lnk_update 
		uC04_EntitiesModificationForm.setSelectedTab(null); //reset selected row for lnk_update 

										 callUpdateSelectedPlayer(request,uC04_EntitiesModificationForm , index);
						// Init the destination form com.bluage.documentation.web.presentation.gettingstarted.uc04_entitiesmodification.UC04_EntitiesModificationForm or not from lnk_update
			// Populate the destination form with all the instances returned from lnk_update.
			final IForm uC04_EntitiesModificationForm2 = populateDestinationForm(request.getSession(), uC04_EntitiesModificationForm); 
					
			// Add the form to the model.
			model.addAttribute("uC04_EntitiesModificationForm", uC04_EntitiesModificationForm2); 
			
			request.getSession().setAttribute("uC04_EntitiesModificationForm", uC04_EntitiesModificationForm2);
			
			// "OK" CASE => destination screen path from lnk_update
			LOGGER.info("Go to the screen 'UC04_EntitiesModification'.");
			// Redirect (PRG) from lnk_update
			destinationPath =  "redirect:/presentation/gettingstarted/uc04_entitiesmodification/UC04_EntitiesModification.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_update 
	}
	
									/**
	 * Operation : lnk_details
 	 * @param model : 
 	 * @param uC04_EntitiesModification : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC04_EntitiesModification/lnk_details.html",method = RequestMethod.POST)
	public String lnk_details(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC04_EntitiesModificationForm") UC04_EntitiesModificationForm  uC04_EntitiesModificationForm, final BindingResult bindingResult ,final @RequestParam(ID_REQUEST_PARAM) Integer index){
		
		
		
		LOGGER.info("Begin method : lnk_details"); 
		infoLogger(uC04_EntitiesModificationForm); 
		uC04_EntitiesModificationForm.setAlwaysCallPreControllers(false); 
		// initialization for lnk_details
		init();
		
		String destinationPath = null; 
		

										 callFindplayerbyid(request,uC04_EntitiesModificationForm , index);
											// Init the destination form com.bluage.documentation.web.presentation.gettingstarted.uc04_entitiesmodification.UC04_EntitiesModificationPlainPageForm or not from lnk_details
			final UC04_EntitiesModificationPlainPageForm uC04_EntitiesModificationPlainPageForm =  new UC04_EntitiesModificationPlainPageForm();
			// Populate the destination form with all the instances returned from lnk_details.
			final IForm uC04_EntitiesModificationPlainPageForm2 = populateDestinationForm(request.getSession(), uC04_EntitiesModificationPlainPageForm); 
					
			// Add the form to the model.
			model.addAttribute("uC04_EntitiesModificationPlainPageForm", uC04_EntitiesModificationPlainPageForm2); 
			
			request.getSession().setAttribute("uC04_EntitiesModificationPlainPageForm", uC04_EntitiesModificationPlainPageForm2);
			
			// "OK" CASE => destination screen path from lnk_details
			LOGGER.info("Go to the screen 'UC04_EntitiesModificationPlainPage'.");
			// Redirect (PRG) from lnk_details
			destinationPath =  "redirect:/presentation/gettingstarted/uc04_entitiesmodification/UC04_EntitiesModificationPlainPage.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_details 
	}
	
		/**
	 * Operation : lnk_edit
 	 * @param model : The model
 	 * @param uC04_EntitiesModification : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC04_EntitiesModification/lnk_edit.html")
	public String lnk_edit(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC04_EntitiesModificationForm") UC04_EntitiesModificationForm  uC04_EntitiesModificationForm, BindingResult bindingResult, final @RequestParam(ID_REQUEST_PARAM) Integer index){
		uC04_EntitiesModificationForm.setSelectedRow(index);	
		String tableId = request.getParameter(TAB_REQUEST_PARAM);
		uC04_EntitiesModificationForm.setSelectedTab(tableId);
		
		Player04BO playerToUpdate = (Player04BO) uC04_EntitiesModificationForm.getAllPlayers().get(index);
		uC04_EntitiesModificationForm.setPlayerToUpdate(playerToUpdate);
		return "/presentation/gettingstarted/uc04_entitiesmodification/UC04_EntitiesModification";
	}
	/**
	 * Operation : btn_cancel
 	 * @param model : 
 	 * @param uC04_EntitiesModification : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC04_EntitiesModification/btn_cancel.html")
	public String btn_cancel(final HttpServletRequest request,final HttpServletResponse response, final Model model,final @ModelAttribute("uC04_EntitiesModificationForm") UC04_EntitiesModificationForm  uC04_EntitiesModificationForm, BindingResult bindingResult){
		uC04_EntitiesModificationForm.setSelectedRow(-1);
		uC04_EntitiesModificationForm.setSelectedTab(null);
		uC04_EntitiesModificationForm.setPlayerToUpdate(null);
		return "redirect:/presentation/gettingstarted/uc04_entitiesmodification/UC04_EntitiesModification.html";
	}

	/**
	* This method initialise the form : UC04_EntitiesModificationForm 
	* @return UC04_EntitiesModificationForm
	*/
	@ModelAttribute("UC04_EntitiesModificationFormInit")
	public void initUC04_EntitiesModificationForm(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, U_C04__ENTITIES_MODIFICATION_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method UC04_EntitiesModificationForm."); //for btn_cancel 
		}
		UC04_EntitiesModificationForm uC04_EntitiesModificationForm;
	
		if(request.getSession().getAttribute(U_C04__ENTITIES_MODIFICATION_FORM) != null){
			uC04_EntitiesModificationForm = (UC04_EntitiesModificationForm)request.getSession().getAttribute(U_C04__ENTITIES_MODIFICATION_FORM);
		} else {
			uC04_EntitiesModificationForm = new UC04_EntitiesModificationForm();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form UC04_EntitiesModificationForm.");
		}
		uC04_EntitiesModificationForm = (UC04_EntitiesModificationForm)populateDestinationForm(request.getSession(), uC04_EntitiesModificationForm);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  UC04_EntitiesModificationForm.");
		}
		model.addAttribute(U_C04__ENTITIES_MODIFICATION_FORM, uC04_EntitiesModificationForm);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param UC04_EntitiesModificationForm : The sceen form.
	 */
	@RequestMapping(value = "/UC04_EntitiesModification.html" ,method = RequestMethod.GET)
	public void prepareUC04_EntitiesModification(final HttpServletRequest request,final  Model model,final  @ModelAttribute("UC04_EntitiesModificationFormInit") UC04_EntitiesModificationForm uC04_EntitiesModificationForm){
		
		UC04_EntitiesModificationForm currentUC04_EntitiesModificationForm = uC04_EntitiesModificationForm;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(U_C04__ENTITIES_MODIFICATION_FORM) == null){
			if(currentUC04_EntitiesModificationForm!=null){
				request.getSession().setAttribute(U_C04__ENTITIES_MODIFICATION_FORM, currentUC04_EntitiesModificationForm);
			}else {
				currentUC04_EntitiesModificationForm = new UC04_EntitiesModificationForm();
				request.getSession().setAttribute(U_C04__ENTITIES_MODIFICATION_FORM, currentUC04_EntitiesModificationForm);	
			}
		} else {
			currentUC04_EntitiesModificationForm = (UC04_EntitiesModificationForm) request.getSession().getAttribute(U_C04__ENTITIES_MODIFICATION_FORM);
		}

		try {
			List allPlayers = player04DAOFinderImpl.findAll();
			put(request.getSession(), ALL_PLAYERS, allPlayers);
		} catch (ApplicationException e) {
			errorLogger("An error while calling the generic finder for the instance : allPlayers.",e);
		}
				currentUC04_EntitiesModificationForm = (UC04_EntitiesModificationForm)populateDestinationForm(request.getSession(), currentUC04_EntitiesModificationForm);
		// Call all the Precontroller.
	currentUC04_EntitiesModificationForm.setAlwaysCallPreControllers(true);
		model.addAttribute(U_C04__ENTITIES_MODIFICATION_FORM, currentUC04_EntitiesModificationForm);
		request.getSession().setAttribute(U_C04__ENTITIES_MODIFICATION_FORM, currentUC04_EntitiesModificationForm);
		
	}
	
										/**
	 * method callUpdateSelectedPlayer
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 * @param index Integer the current index 
	 */
	private void callUpdateSelectedPlayer(HttpServletRequest request,IForm form,Integer index) {
					if(index!=null){  
			 playerToUpdate = (Player04BO)((List)get(request.getSession(),form, "allPlayers")).get(index);  
		} else {
			 playerToUpdate= null; 
		}
				 
					try {
				// executing UpdateSelectedPlayer in btn_cancel
	servicePlayer04.player04Update(
			playerToUpdate
			);  

													// processing variables UpdateSelectedPlayer in btn_cancel
				put(request.getSession(), PLAYER_TO_UPDATE,playerToUpdate); 
			
			} catch (ApplicationException e) { 
				// error handling for operation player04Update called UpdateSelectedPlayer
				errorLogger("An error occured during the execution of the operation : player04Update",e); 
		
			}
	}
									/**
	 * method callFindplayerbyid
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 * @param index Integer the current index 
	 */
	private void callFindplayerbyid(HttpServletRequest request,IForm form,Integer index) {
		if(index!=null){  
			 playerToUpdate = (Player04BO)((List)get(request.getSession(),form, "allPlayers")).get(index);  
		} else {
			 playerToUpdate= null; 
		}
				 
					if(index!=null){  
			 playerToUpdate = (Player04BO)((List)get(request.getSession(),form, "allPlayers")).get(index);  
		} else {
			 playerToUpdate= null; 
		}
				 
					try {
				// executing Findplayerbyid in btn_cancel
				playerToUpdate = 	servicePlayer04.player04FindByID(
			playerToUpdate
			);  
 
				put(request.getSession(), PLAYER_TO_UPDATE,playerToUpdate);
								// processing variables Findplayerbyid in btn_cancel
				put(request.getSession(), PLAYER_TO_UPDATE,playerToUpdate); 
			
			} catch (ApplicationException e) { 
				// error handling for operation player04FindByID called Findplayerbyid
				errorLogger("An error occured during the execution of the operation : player04FindByID",e); 
		
			}
	}
		/**
	* This method Binder.
	* @param binder : 
	*		 WebDataBinder
	*/
	@InitBinder
	protected void initBinder(final WebDataBinder binder){
			customDateEditorsUC04_EntitiesModificationController.put("allPlayers.dateOfBirth", new CustomDateEditor(new SimpleDateFormat("MM/dd/yyyy"), true, "Invalid format."));
			binder.registerCustomEditor(Date.class, "allPlayers.dateOfBirth", customDateEditorsUC04_EntitiesModificationController.get("allPlayers.dateOfBirth"));
	}
	
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("UC04_EntitiesModificationController [ ");
			strBToS.append(ALL_PLAYERS);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(allPlayers);
			strBToS.append(END_TO_STRING_DELIMITER);
						strBToS.append(PLAYER_TO_UPDATE);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(playerToUpdate);
			strBToS.append(END_TO_STRING_DELIMITER);
		// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(uC04_EntitiesModificationValidator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsUC04_EntitiesModificationController!=null); 
		return strBToS.toString();
	}
}
