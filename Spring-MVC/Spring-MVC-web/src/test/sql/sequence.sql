DROP SEQUENCE hibernate_sequence;


CREATE SEQUENCE hibernate_sequence

INCREMENT BY 1

NO MAXVALUE

NO MINVALUE

CACHE 1

START WITH 330;

ALTER TABLE public.hibernate_sequence OWNER TO "BA-Doc-Dev";
