package uc57;


import org.junit.Test;
import test.common.AbstractBADocTestCase;

public class ITuc57TestTeams extends AbstractBADocTestCase {

	@Override
	public  String getName(){
		return "UC-57";
	}	


	@Test
	public void iTuc57TestTeams() throws Exception {
		// On teste l'UC57 - Standard Features/Services/HQL - HQL Operation.
		selenium.open("");	
		selenium.click("link=Expand All");
		selenium.waitForPageToLoad("30000");
		selenium.click("link=UC57 - HQL Operation");
		verifyTrue(selenium.isElementPresent("//h2[contains(text(),\"Standard Features > Services > HQL > UC57 > HQL Operation\")]"));
		// On teste le champ de recherche.
		selenium.type("//*[contains(@id,'txt_name')]", "Chargers");
		selenium.type("//input[contains(@id,'txt_firstresult')]", "0");
		selenium.type("//input[contains(@id,'txt_maxresult')]", "10");
		selenium.click("//a[contains(@id,'lnk_searchTeams')]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_searchResult')]/tbody//tr[td//span[contains(text(),'3')] and td//span[contains(text(),\"Chargers\")] and td//span[contains(text(),\"Detroit\")] and td//span[contains(text(),\"Arkansas\")] and td/a[contains(text(),'[Search Players]')]]"));
		selenium.type("//*[contains(@id,'txt_name')]", "Braves");
		selenium.click("//*[contains(@id,'lnk_searchTeams')]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_searchResult')]/tbody//tr[td//span[contains(text(),'5')] and td//span[contains(text(),\"Braves\")] and td//span[contains(text(),\"Omaha\")] and td//span[contains(text(),\"Arkansas\")] and td/a[contains(text(),'[Search Players]')]]"));
		// On vérifie que toutes les données Teams s'affichent.
		selenium.type("//*[contains(@id,'txt_name')]", "");
		selenium.click("//a[contains(@id,'lnk_searchTeams')]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_searchResult')]/thead//tr[th[contains(text(),'Id')] and th[contains(text(),\"Name\")] and th[contains(text(),\"City\")] and th[contains(text(),'State')]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_searchResult')]/tbody//tr[td//span[contains(text(),'1')] and td//span[contains(text(),\"Tigers\")] and td//span[contains(text(),\"Charlotte\")] and td//span[contains(text(),\"Alaska\")] and td/a[contains(text(),'[Search Players]')]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_searchResult')]/tbody//tr[td//span[contains(text(),'2')] and td//span[contains(text(),\"Bears\")] and td//span[contains(text(),\"Kansas City\")] and td//span[contains(text(),\"Alabama\")] and td/a[contains(text(),'[Search Players]')]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_searchResult')]/tbody//tr[td//span[contains(text(),'3')] and td//span[contains(text(),\"Chargers\")] and td//span[contains(text(),\"Detroit\")] and td//span[contains(text(),\"Arkansas\")] and td/a[contains(text(),'[Search Players]')]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_searchResult')]/tbody//tr[td//span[contains(text(),'4')] and td//span[contains(text(),\"Hawks\")] and td//span[contains(text(),\"Detroit\")] and td//span[contains(text(),\"Alaska\")] and td/a[contains(text(),'[Search Players]')]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_searchResult')]/tbody//tr[td//span[contains(text(),'5')] and td//span[contains(text(),\"Braves\")] and td//span[contains(text(),\"Omaha\")] and td//span[contains(text(),\"Arkansas\")] and td/a[contains(text(),'[Search Players]')]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_searchResult')]/tbody//tr[td//span[contains(text(),'6')] and td//span[contains(text(),\"Eagles\")] and td//span[contains(text(),\"Raleigh\")] and td//span[contains(text(),\"California\")] and td/a[contains(text(),'[Search Players]')]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_searchResult')]/tbody//tr[td//span[contains(text(),'7')] and td//span[contains(text(),\"Cardinals\")] and td//span[contains(text(),\"Oakland\")] and td//span[contains(text(),\"Colorado\")] and td/a[contains(text(),'[Search Players]')]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_searchResult')]/tbody//tr[td//span[contains(text(),'8')] and td//span[contains(text(),\"Red Devils\")] and td//span[contains(text(),\"Kansas City\")] and td//span[contains(text(),\"Connecticut\")] and td/a[contains(text(),'[Search Players]')]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_searchResult')]/tbody//tr[td//span[contains(text(),'9')] and td//span[contains(text(),\"Mustangs\")] and td//span[contains(text(),\"Fresno\")] and td//span[contains(text(),\"Delaware\")] and td/a[contains(text(),'[Search Players]')]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_searchResult')]/tbody//tr[td//span[contains(text(),'10')] and td//span[contains(text(),\"Wolverines\")] and td//span[contains(text(),\"Sacramento\")] and td//span[contains(text(),\"Florida\")] and td/a[contains(text(),'[Search Players]')]]"));
		// On teste le FirstResult
		selenium.type("//input[contains(@id,'txt_firstresult')]", "2");
		selenium.type("//input[contains(@id,'txt_maxresult')]", "10");
		selenium.click("//a[contains(@id,'lnk_searchTeams')]");
		selenium.waitForPageToLoad("30000");
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_searchResult')]/tbody//tr[td//span[contains(text(),'1')] and td//span[contains(text(),\"Tigers\")] and td//span[contains(text(),\"Charlotte\")] and td//span[contains(text(),\"Alaska\")] and td/a[contains(text(),'[Search Players]')]]"));
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_searchResult')]/tbody//tr[td//span[contains(text(),'2')] and td//span[contains(text(),\"Bears\")] and td//span[contains(text(),\"Kansas City\")] and td//span[contains(text(),\"Alabama\")] and td/a[contains(text(),'[Search Players]')]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_searchResult')]/tbody//tr[td//span[contains(text(),'3')] and td//span[contains(text(),\"Chargers\")] and td//span[contains(text(),\"Detroit\")] and td//span[contains(text(),\"Arkansas\")] and td/a[contains(text(),'[Search Players]')]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_searchResult')]/tbody//tr[td//span[contains(text(),'4')] and td//span[contains(text(),\"Hawks\")] and td//span[contains(text(),\"Detroit\")] and td//span[contains(text(),\"Alaska\")] and td/a[contains(text(),'[Search Players]')]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_searchResult')]/tbody//tr[td//span[contains(text(),'5')] and td//span[contains(text(),\"Braves\")] and td//span[contains(text(),\"Omaha\")] and td//span[contains(text(),\"Arkansas\")] and td/a[contains(text(),'[Search Players]')]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_searchResult')]/tbody//tr[td//span[contains(text(),'6')] and td//span[contains(text(),\"Eagles\")] and td//span[contains(text(),\"Raleigh\")] and td//span[contains(text(),\"California\")] and td/a[contains(text(),'[Search Players]')]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_searchResult')]/tbody//tr[td//span[contains(text(),'7')] and td//span[contains(text(),\"Cardinals\")] and td//span[contains(text(),\"Oakland\")] and td//span[contains(text(),\"Colorado\")] and td/a[contains(text(),'[Search Players]')]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_searchResult')]/tbody//tr[td//span[contains(text(),'8')] and td//span[contains(text(),\"Red Devils\")] and td//span[contains(text(),\"Kansas City\")] and td//span[contains(text(),\"Connecticut\")] and td/a[contains(text(),'[Search Players]')]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_searchResult')]/tbody//tr[td//span[contains(text(),'9')] and td//span[contains(text(),\"Mustangs\")] and td//span[contains(text(),\"Fresno\")] and td//span[contains(text(),\"Delaware\")] and td/a[contains(text(),'[Search Players]')]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_searchResult')]/tbody//tr[td//span[contains(text(),'10')] and td//span[contains(text(),\"Wolverines\")] and td//span[contains(text(),\"Sacramento\")] and td//span[contains(text(),\"Florida\")] and td/a[contains(text(),'[Search Players]')]]"));
	}


}
