/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.service.standardfeatures.application.navigation.uc31_preaction;


import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import com.bluage.documentation.service.AbstractService;
import com.bluage.documentation.service.standardfeatures.application.navigation.uc31_preaction.daofinder.ServicePlayer31FindAllDAOMethodFinderImpl;
import com.netfective.bluage.core.exception.ApplicationException;

/**
 * Defines  application services for ServicePlayer31FindAll
  */
@Named("com.bluage.documentation.service.standardfeatures.application.navigation.uc31_preaction.ServicePlayer31FindAll")
public class ServicePlayer31FindAll  extends AbstractService{


			@Inject
		@Named ("com.bluage.documentation.service.standardfeatures.application.navigation.uc31_preaction.daofinder.ServicePlayer31FindAllDAOMethodFinder")
		private ServicePlayer31FindAllDAOMethodFinderImpl servicePlayer31FindAllDao;



	/**
	 *  Default constructor 
	 */
	public ServicePlayer31FindAll() {
		super(); // Call super for default constructor : ServicePlayer31FindAll
	}
	



				
	/**    
	 * Operation HQL.
	 * @return {@link List}
	 */
	public List player31FindAll( )
 throws ApplicationException {
	
	return servicePlayer31FindAllDao.player31FindAll();
	}

		}
