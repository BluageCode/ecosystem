/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.qauc.uc1002_validators ;


import org.apache.log4j.Logger;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;

import com.netfective.bluage.core.validator.AbstractValidator;
import com.netfective.bluage.core.validator.ValidatorsUtil;
/**
* Class :UC1002_ValidatorsValidator
*/
public class UC1002_ValidatorsValidator extends AbstractValidator{
	
	// LOGGER for the class UC1002_ValidatorsValidator
	private static final Logger LOGGER = Logger.getLogger( UC1002_ValidatorsValidator.class);
	
	
	/**
	* Operation validate for UC1002_ValidatorsForm
	* @param obj : the current form (UC1002_ValidatorsForm)
	* @param errors : The spring errors to return for the form UC1002_ValidatorsForm
	*/
	public void validate(final Object obj,final Errors errors){
		LOGGER.info("Beginning method : validate"); // UC1002_ValidatorsValidator
		UC1002_ValidatorsForm cUC1002_ValidatorsForm = (UC1002_ValidatorsForm)obj; // UC1002_ValidatorsValidator
					if(LOGGER.isDebugEnabled()){
				LOGGER.debug("validation of the 'playerToCreate.height' field with the RangeValidator validator");
			}
			
			if(ValidatorsUtil.rangeValidator(cUC1002_ValidatorsForm.getPlayerToCreate().getHeight(), "150", "240")== 0 ){
				errors.rejectValue("playerToCreate.height", "", "Player height should be between 150 and 240 cms.");
			} 
				
				ValidationUtils.rejectIfEmpty(errors, "playerToCreate.firstName", "", "First name is required.");

		if(errors.getFieldError("playerToCreate.firstName")== null){
						if(LOGGER.isDebugEnabled()){
				LOGGER.debug("validation of the 'playerToCreate.firstName' field with the LengthValidator validator");
			}
			
			if(ValidatorsUtil.lengthValidator(cUC1002_ValidatorsForm.getPlayerToCreate().getFirstName(), 1, 30) == 0){
				errors.rejectValue("playerToCreate.firstName", "", "First name should be less than 30 characters long.");
			} 
				
		}
				ValidationUtils.rejectIfEmpty(errors, "playerToCreate.lastName", "", "Last name is required.");

		if(errors.getFieldError("playerToCreate.lastName")== null){
						if(LOGGER.isDebugEnabled()){
				LOGGER.debug("validation of the 'playerToCreate.lastName' field with the LengthValidator validator");
			}
			
			if(ValidatorsUtil.lengthValidator(cUC1002_ValidatorsForm.getPlayerToCreate().getLastName(), 1, 30) == 0){
				errors.rejectValue("playerToCreate.lastName", "", "Last name should be less than 30 characters long.");
			} 
				
		}
				
					if(LOGGER.isDebugEnabled()){
				LOGGER.debug("validation of the 'playerToCreate.weight' field with the RangeValidator validator");
			}
			
			if(ValidatorsUtil.rangeValidator(cUC1002_ValidatorsForm.getPlayerToCreate().getWeight(), "60", "140")== 0 ){
				errors.rejectValue("playerToCreate.weight", "", "Player weight should be between 60 and 140 kgs.");
			} 
				
					if(LOGGER.isDebugEnabled()){
				LOGGER.debug("validation of the 'playerToCreate.salary' field with the RangeValidator validator");
			}
			
			if(ValidatorsUtil.rangeValidator(cUC1002_ValidatorsForm.getPlayerToCreate().getSalary(), "0", "8000")== 0 ){
				errors.rejectValue("playerToCreate.salary", "", "The salary should be between 0 and 8000$");
			} 
				
		LOGGER.info("Ending method : validate the form "+ cUC1002_ValidatorsForm.getClass().getName()); // UC1002_ValidatorsValidator
	}

	/**
	* Method to implements to use spring validators (UC1002_ValidatorsForm)
	* @param aClass : Class for the form UC1002_ValidatorsForm
	* @return boolean
	*/
	public boolean supports(final Class aClass){
		return (new UC1002_ValidatorsForm()).getClass().equals(aClass); // UC1002_ValidatorsValidator
	}
}
