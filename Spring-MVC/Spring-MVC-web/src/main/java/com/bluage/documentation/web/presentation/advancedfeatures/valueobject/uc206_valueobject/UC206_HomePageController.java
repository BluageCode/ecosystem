/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.advancedfeatures.valueobject.uc206_valueobject ;

// Import declaration.
// Java imports.
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.bluage.documentation.business.advancedfeatures.valueobject.uc206_valueobject.vo.Screen206VO;
import com.bluage.documentation.service.advancedfeatures.valueobject.uc206_valueobject.ServiceScreen206;
import com.netfective.bluage.core.exception.ApplicationException;
import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;
import com.netfective.bluage.core.web.IForm;

/**
 * Class : UC206_HomePageController
 */
@Controller
@Scope("prototype")
@SessionAttributes("uC206_HomePageForm")
@RequestMapping(value= "/presentation/advancedfeatures/valueobject/uc206_valueobject")
public class UC206_HomePageController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC206_HomePageController.class);
	
	//Current form name
	private static final String U_C206__HOME_PAGE_FORM = "uC206_HomePageForm";

	//CONSTANT: screen206
	private static final String SCREEN206 = "screen206";
	//CONSTANT: playerVOs
	private static final String PLAYER_V_OS = "playerVOs";
	
	/**
	 * Property:customDateEditorsUC206_HomePageController
	 */
	final private Map<String,CustomDateEditor> customDateEditorsUC206_HomePageController = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:uC206_HomePageValidator
	 */
	final private UC206_HomePageValidator uC206_HomePageValidator = new UC206_HomePageValidator();
	
	/**
	 * Service declaration : serviceScreen206.
	 */
	@Autowired
	private ServiceScreen206 serviceScreen206;
	
	
	// Initialise all the instances for UC206_HomePageController
		// Declare the instance screen206
		private Screen206VO screen206;
		// Declare the instance playerVOs
		private List playerVOs;
			/**
	 * Operation : lnk_searchPlayer
 	 * @param model : 
 	 * @param uC206_HomePage : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC206_HomePage/lnk_searchPlayer.html",method = RequestMethod.POST)
	public String lnk_searchPlayer(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC206_HomePageForm") UC206_HomePageForm  uC206_HomePageForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : lnk_searchPlayer"); 
		infoLogger(uC206_HomePageForm); 
		uC206_HomePageForm.setAlwaysCallPreControllers(false); 
		// initialization for lnk_searchPlayer
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state lnk_searchPlayer if the validation fail, you will be forward to the screen : "/presentation/advancedfeatures/valueobject/uc206_valueobject/UC206_HomePage"
		if(errorsMessages(request,response,uC206_HomePageForm,bindingResult,"", uC206_HomePageValidator, customDateEditorsUC206_HomePageController)){ 
			return "/presentation/advancedfeatures/valueobject/uc206_valueobject/UC206_HomePage"; 
		}

	 callInitializeseachproperties(request,uC206_HomePageForm );
			
				 callInitializeallpositions(request,uC206_HomePageForm );
											// Init the destination form com.bluage.documentation.web.presentation.advancedfeatures.valueobject.uc206_valueobject.UC206_SearchPlayerForm or not from lnk_searchPlayer
			final UC206_SearchPlayerForm uC206_SearchPlayerForm =  new UC206_SearchPlayerForm();
			// Populate the destination form with all the instances returned from lnk_searchPlayer.
			final IForm uC206_SearchPlayerForm2 = populateDestinationForm(request.getSession(), uC206_SearchPlayerForm); 
					
			// Add the form to the model.
			model.addAttribute("uC206_SearchPlayerForm", uC206_SearchPlayerForm2); 
			
			request.getSession().setAttribute("uC206_SearchPlayerForm", uC206_SearchPlayerForm2);
			
			// "OK" CASE => destination screen path from lnk_searchPlayer
			LOGGER.info("Go to the screen 'UC206_SearchPlayer'.");
			// Redirect (PRG) from lnk_searchPlayer
			destinationPath =  "redirect:/presentation/advancedfeatures/valueobject/uc206_valueobject/UC206_SearchPlayer.html";
						
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_searchPlayer 
	}
	
				/**
	 * Operation : lnk_createPlayer
 	 * @param model : 
 	 * @param uC206_HomePage : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC206_HomePage/lnk_createPlayer.html",method = RequestMethod.POST)
	public String lnk_createPlayer(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC206_HomePageForm") UC206_HomePageForm  uC206_HomePageForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : lnk_createPlayer"); 
		infoLogger(uC206_HomePageForm); 
		uC206_HomePageForm.setAlwaysCallPreControllers(false); 
		// initialization for lnk_createPlayer
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state lnk_createPlayer if the validation fail, you will be forward to the screen : "/presentation/advancedfeatures/valueobject/uc206_valueobject/UC206_HomePage"
		if(errorsMessages(request,response,uC206_HomePageForm,bindingResult,"", uC206_HomePageValidator, customDateEditorsUC206_HomePageController)){ 
			return "/presentation/advancedfeatures/valueobject/uc206_valueobject/UC206_HomePage"; 
		}

	 callInitializeCreate(request,uC206_HomePageForm );
			
				 callInitializelistposition(request,uC206_HomePageForm );
											// Init the destination form com.bluage.documentation.web.presentation.advancedfeatures.valueobject.uc206_valueobject.UC206_CreatePlayerForm or not from lnk_createPlayer
			final UC206_CreatePlayerForm uC206_CreatePlayerForm =  new UC206_CreatePlayerForm();
			// Populate the destination form with all the instances returned from lnk_createPlayer.
			final IForm uC206_CreatePlayerForm2 = populateDestinationForm(request.getSession(), uC206_CreatePlayerForm); 
					
			// Add the form to the model.
			model.addAttribute("uC206_CreatePlayerForm", uC206_CreatePlayerForm2); 
			
			request.getSession().setAttribute("uC206_CreatePlayerForm", uC206_CreatePlayerForm2);
			
			// "OK" CASE => destination screen path from lnk_createPlayer
			LOGGER.info("Go to the screen 'UC206_CreatePlayer'.");
			// Redirect (PRG) from lnk_createPlayer
			destinationPath =  "redirect:/presentation/advancedfeatures/valueobject/uc206_valueobject/UC206_CreatePlayer.html";
						
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_createPlayer 
	}
	
				/**
	 * Operation : lnk_listPlayer
 	 * @param model : 
 	 * @param uC206_HomePage : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC206_HomePage/lnk_listPlayer.html",method = RequestMethod.POST)
	public String lnk_listPlayer(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC206_HomePageForm") UC206_HomePageForm  uC206_HomePageForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : lnk_listPlayer"); 
		infoLogger(uC206_HomePageForm); 
		uC206_HomePageForm.setAlwaysCallPreControllers(false); 
		// initialization for lnk_listPlayer
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state lnk_listPlayer if the validation fail, you will be forward to the screen : "/presentation/advancedfeatures/valueobject/uc206_valueobject/UC206_HomePage"
		if(errorsMessages(request,response,uC206_HomePageForm,bindingResult,"", uC206_HomePageValidator, customDateEditorsUC206_HomePageController)){ 
			return "/presentation/advancedfeatures/valueobject/uc206_valueobject/UC206_HomePage"; 
		}

	 callInitializeList(request,uC206_HomePageForm );
			
				 callListPlayers(request,uC206_HomePageForm );
											// Init the destination form com.bluage.documentation.web.presentation.advancedfeatures.valueobject.uc206_valueobject.UC206_ListPlayerForm or not from lnk_listPlayer
			final UC206_ListPlayerForm uC206_ListPlayerForm =  new UC206_ListPlayerForm();
			// Populate the destination form with all the instances returned from lnk_listPlayer.
			final IForm uC206_ListPlayerForm2 = populateDestinationForm(request.getSession(), uC206_ListPlayerForm); 
					
			// Add the form to the model.
			model.addAttribute("uC206_ListPlayerForm", uC206_ListPlayerForm2); 
			
			request.getSession().setAttribute("uC206_ListPlayerForm", uC206_ListPlayerForm2);
			
			// "OK" CASE => destination screen path from lnk_listPlayer
			LOGGER.info("Go to the screen 'UC206_ListPlayer'.");
			// Redirect (PRG) from lnk_listPlayer
			destinationPath =  "redirect:/presentation/advancedfeatures/valueobject/uc206_valueobject/UC206_ListPlayer.html";
						
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_listPlayer 
	}
	
	
	/**
	* This method initialise the form : UC206_HomePageForm 
	* @return UC206_HomePageForm
	*/
	@ModelAttribute("UC206_HomePageFormInit")
	public void initUC206_HomePageForm(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, U_C206__HOME_PAGE_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method UC206_HomePageForm."); //for lnk_listPlayer 
		}
		UC206_HomePageForm uC206_HomePageForm;
	
		if(request.getSession().getAttribute(U_C206__HOME_PAGE_FORM) != null){
			uC206_HomePageForm = (UC206_HomePageForm)request.getSession().getAttribute(U_C206__HOME_PAGE_FORM);
		} else {
			uC206_HomePageForm = new UC206_HomePageForm();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form UC206_HomePageForm.");
		}
		uC206_HomePageForm = (UC206_HomePageForm)populateDestinationForm(request.getSession(), uC206_HomePageForm);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  UC206_HomePageForm.");
		}
		model.addAttribute(U_C206__HOME_PAGE_FORM, uC206_HomePageForm);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param UC206_HomePageForm : The sceen form.
	 */
	@RequestMapping(value = "/UC206_HomePage.html" ,method = RequestMethod.GET)
	public void prepareUC206_HomePage(final HttpServletRequest request,final  Model model,final  @ModelAttribute("UC206_HomePageFormInit") UC206_HomePageForm uC206_HomePageForm){
		
		UC206_HomePageForm currentUC206_HomePageForm = uC206_HomePageForm;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(U_C206__HOME_PAGE_FORM) == null){
			if(currentUC206_HomePageForm!=null){
				request.getSession().setAttribute(U_C206__HOME_PAGE_FORM, currentUC206_HomePageForm);
			}else {
				currentUC206_HomePageForm = new UC206_HomePageForm();
				request.getSession().setAttribute(U_C206__HOME_PAGE_FORM, currentUC206_HomePageForm);	
			}
		} else {
			currentUC206_HomePageForm = (UC206_HomePageForm) request.getSession().getAttribute(U_C206__HOME_PAGE_FORM);
		}

		currentUC206_HomePageForm = (UC206_HomePageForm)populateDestinationForm(request.getSession(), currentUC206_HomePageForm);
		// Call all the Precontroller.
	currentUC206_HomePageForm.setAlwaysCallPreControllers(true);
		model.addAttribute(U_C206__HOME_PAGE_FORM, currentUC206_HomePageForm);
		request.getSession().setAttribute(U_C206__HOME_PAGE_FORM, currentUC206_HomePageForm);
		
	}
	
	/**
	 * method callInitializeseachproperties
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callInitializeseachproperties(HttpServletRequest request,IForm form) {
		screen206 = (Screen206VO)get(request.getSession(),form, "screen206");  
										 
					try {
				// executing Initializeseachproperties in lnk_listPlayer
				screen206 = 	serviceScreen206.initializeScreen206(
	);  
 
				put(request.getSession(), SCREEN206,screen206);
								// processing variables Initializeseachproperties in lnk_listPlayer

			} catch (ApplicationException e) { 
				// error handling for operation initializeScreen206 called Initializeseachproperties
				errorLogger("An error occured during the execution of the operation : initializeScreen206",e); 
		
			}
	}
		/**
	 * method callInitializeCreate
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callInitializeCreate(HttpServletRequest request,IForm form) {
		screen206 = (Screen206VO)get(request.getSession(),form, "screen206");  
										 
					try {
				// executing InitializeCreate in lnk_listPlayer
				screen206 = 	serviceScreen206.initializeScreen206(
	);  
 
				put(request.getSession(), SCREEN206,screen206);
								// processing variables InitializeCreate in lnk_listPlayer

			} catch (ApplicationException e) { 
				// error handling for operation initializeScreen206 called InitializeCreate
				errorLogger("An error occured during the execution of the operation : initializeScreen206",e); 
		
			}
	}
					/**
	 * method callInitializelistposition
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callInitializelistposition(HttpServletRequest request,IForm form) {
		screen206 = (Screen206VO)get(request.getSession(),form, "screen206");  
										 
					screen206 = (Screen206VO)get(request.getSession(),form, "screen206");  
										 
					try {
				// executing Initializelistposition in lnk_listPlayer
				screen206 = 	serviceScreen206.initializeListPosition(
			screen206
			);  
 
				put(request.getSession(), SCREEN206,screen206);
								// processing variables Initializelistposition in lnk_listPlayer
				put(request.getSession(), SCREEN206,screen206); 
			
			} catch (ApplicationException e) { 
				// error handling for operation initializeListPosition called Initializelistposition
				errorLogger("An error occured during the execution of the operation : initializeListPosition",e); 
		
			}
	}
		/**
	 * method callInitializeList
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callInitializeList(HttpServletRequest request,IForm form) {
		screen206 = (Screen206VO)get(request.getSession(),form, "screen206");  
										 
					try {
				// executing InitializeList in lnk_listPlayer
				screen206 = 	serviceScreen206.initializeScreen206(
	);  
 
				put(request.getSession(), SCREEN206,screen206);
								// processing variables InitializeList in lnk_listPlayer

			} catch (ApplicationException e) { 
				// error handling for operation initializeScreen206 called InitializeList
				errorLogger("An error occured during the execution of the operation : initializeScreen206",e); 
		
			}
	}
					/**
	 * method callListPlayers
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callListPlayers(HttpServletRequest request,IForm form) {
		playerVOs = (List)get(request.getSession(),form, "playerVOs");  
										 
					screen206 = (Screen206VO)get(request.getSession(),form, "screen206");  
										 
					try {
				// executing ListPlayers in lnk_listPlayer
				playerVOs = 	serviceScreen206.executeListPlayer(
			screen206
			);  
 
				put(request.getSession(), PLAYER_V_OS,playerVOs);
								// processing variables ListPlayers in lnk_listPlayer
				put(request.getSession(), SCREEN206,screen206); 
			
			} catch (ApplicationException e) { 
				// error handling for operation executeListPlayer called ListPlayers
				errorLogger("An error occured during the execution of the operation : executeListPlayer",e); 
		
			}
	}
					/**
	 * method callInitializeallpositions
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callInitializeallpositions(HttpServletRequest request,IForm form) {
					screen206 = (Screen206VO)get(request.getSession(),form, "screen206");  
										 
					try {
				// executing Initializeallpositions in lnk_listPlayer
	serviceScreen206.initializeListPosition(
			screen206
			);  

													// processing variables Initializeallpositions in lnk_listPlayer
				put(request.getSession(), SCREEN206,screen206); 
			
			} catch (ApplicationException e) { 
				// error handling for operation initializeListPosition called Initializeallpositions
				errorLogger("An error occured during the execution of the operation : initializeListPosition",e); 
		
			}
	}
		
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("UC206_HomePageController [ ");
			strBToS.append(SCREEN206);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(screen206);
			strBToS.append(END_TO_STRING_DELIMITER);
			strBToS.append(PLAYER_V_OS);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(playerVOs);
			strBToS.append(END_TO_STRING_DELIMITER);
		// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(uC206_HomePageValidator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsUC206_HomePageController!=null); 
		return strBToS.toString();
	}
}
