/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.netfective.bluage.core.web ;

/**
 * @author NETFECTIVE TECHNOLOGY
 */
public abstract class AbstractForm{
	
	private Boolean alwaysCallPreControllers;
	
	private Integer selectedRow;
	
	private String selectedTab;
	
	/**
	 * getter for selectedRow
	 * @return selectedRow Integer
	 */
	public Integer getSelectedRow() {
		return selectedRow;
	}
	/**
	 * setter for selectedRow
	 * @param value Integer
	 */
	public void setSelectedRow(final Integer value) {
		this.selectedRow = value;
	}
	/**
	 * getter for selectedTab
	 * @return selectedTab String
	 */
	public String getSelectedTab() {
		return selectedTab;
	}
	/**
	 * setter for selectedTab
	 * @param selectedTab String
	 */
	public void setSelectedTab(final String selectedTab) {
		this.selectedTab = selectedTab;
	}
	/**
	 * getter for alwaysCallPreControllers
	 * @return alwaysCallPreControllers Boolean
	 */
	public Boolean isAlwaysCallPreControllers() {
		return alwaysCallPreControllers;
	}
	/**
	 * setter for alwaysCallPreControllers
	 * @param alwaysCallPreControllers Boolean
	 */
	public void setAlwaysCallPreControllers(final Boolean alwaysCallPreControllers) {
		this.alwaysCallPreControllers = alwaysCallPreControllers;
	}
}
