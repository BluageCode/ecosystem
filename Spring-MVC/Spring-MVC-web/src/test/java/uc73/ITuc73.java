package uc73;

import org.junit.Test;
import test.common.AbstractBADocTestCase;

public class ITuc73 extends AbstractBADocTestCase {
	
	@Override
	public  String getName(){
		return "UC-73";
	}
	
	@Test
	public void testUC73() throws Exception {
		// On teste l'UC73 - Standard Features/Exeption - Exeption
		
		selenium.open("");	
		selenium.click("link=Expand All");
		selenium.waitForPageToLoad("300000");
		selenium.click("link=UC73 - Exception");
		verifyTrue(selenium.isElementPresent("//h2[contains(text(),\"Standard Features > Exception > UC73 > Exception\")]"));
		// On vérifie que la table est vide.
		// On vérifie que rien ne s'affiche.
		selenium.select("//*[contains(@id,'select_team')]", "label=---");
		String url = selenium.getLocation();
		String count = String.valueOf(url.indexOf("Struts") != -1 ? 1 : 0);
		verifyEquals(count, selenium.getXpathCount("//table[contains(@id,'tab_searchResult')]//tbody//tr"));
		Thread.sleep(1000);
		// On sélectionne les Tigers.
		selenium.select("//*[contains(@id,'select_team')]", "label=Tigers");
		selenium.waitForPageToLoad("30000");
		// On vérifie les données pour les Tigers.
		verifyEquals("6", selenium.getXpathCount("//table[contains(@id,'tab_searchResult')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_searchResult')]/tbody/tr[td/span[contains(text(),\"Frank\")] and td/span[contains(text(),\"Oliver\")] and td/span[contains(text(),\"228\")] and td/span[contains(text(),\"116\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_searchResult')]/tbody/tr[td/span[contains(text(),\"Pope\")] and td/span[contains(text(),\"Ruben\")] and td/span[contains(text(),\"201\")] and td/span[contains(text(),\"86\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_searchResult')]/tbody/tr[td/span[contains(text(),\"Cross\")] and td/span[contains(text(),\"Lester\")] and td/span[contains(text(),\"214\")] and td/span[contains(text(),\"69\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_searchResult')]/tbody/tr[td/span[contains(text(),\"Carpenter\")] and td/span[contains(text(),\"Woodrow\")] and td/span[contains(text(),\"224\")] and td/span[contains(text(),\"69\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_searchResult')]/tbody/tr[td/span[contains(text(),\"Romero\")] and td/span[contains(text(),\"Gustavo\")] and td/span[contains(text(),\"167\")] and td/span[contains(text(),\"130\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_searchResult')]/tbody/tr[td/span[contains(text(),\"George\")] and td/span[contains(text(),\"Gerardo\")] and td/span[contains(text(),\"159\")] and td/span[contains(text(),\"151\")] and td/a[contains(text(),\"[delete]\")]]"));
		// On efface les joueurs.
		// On indexe chaque ligne avant de l'effacer.
		// Pour Olivier Frank.
		Number indexOFrank = selenium.getElementIndex("//table[contains(@id,'tab_searchResult')]/tbody/tr[td/span[contains(text(),\"Frank\")] and td/span[contains(text(),\"Oliver\")]]");
		System.out.println("variable index vaut : " + indexOFrank);
		// On clique sur [delete]
		selenium.click("//table[contains(@id,'tab_searchResult')]/tbody/tr[" + indexOFrank + " + 1]/td/a[contains(text(),\"[delete]\")]");
		selenium.waitForPageToLoad("30000");
		// On vérife que le message d'erreur apparait.
		assertTrue(selenium.isElementPresent("//table[contains(@id,'applicationErrors')]  | //*/ul/li"));
		verifyEquals("It's not authorized to delete a ready player.", selenium.getText("//table[contains(@id,'applicationErrors')]/tbody/tr/td  | //*/ul/li"));
		// On clique sur [back]
		assertEquals("[back]", selenium.getText("//*[contains(@id,'lnk_back')]"));
		selenium.click("//*[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
		// On sélectionne les Tigers.
		selenium.select("//*[contains(@id,'select_team')]", "label=Tigers");
		selenium.waitForPageToLoad("30000");
		// Pour Ruben Pope.
		Number indexRPope = selenium.getElementIndex("//table[contains(@id,'tab_searchResult')]/tbody/tr[td/span[contains(text(),\"Ruben\")] and td/span[contains(text(),\"Pope\")]]");
		System.out.println("variable index vaut : " + indexRPope);
		// On clique sur [delete]
		selenium.click("//table[contains(@id,'tab_searchResult')]/tbody/tr[" + indexRPope + " + 1]/td/a[contains(text(),\"[delete]\")]");
		selenium.waitForPageToLoad("30000");
		// On vérife que le message d'erreur apparait.
		assertTrue(selenium.isElementPresent("//table[contains(@id,'applicationErrors')]  | //*/ul/li"));
		verifyEquals("It's not authorized to delete a ready player.", selenium.getText("//table[contains(@id,'applicationErrors')]/tbody/tr/td  | //*/ul/li"));
		// On clique sur [back]
		selenium.click("//*[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
		// On sélectionne les Tigers.
		selenium.select("//*[contains(@id,'select_team')]", "label=Tigers");
		selenium.waitForPageToLoad("30000");
		// Pour Lester Cross.
		Number indexLCross = selenium.getElementIndex("//table[contains(@id,'tab_searchResult')]/tbody/tr[td/span[contains(text(),\"Lester\")] and td/span[contains(text(),\"Cross\")]]");
		System.out.println("variable index vaut : ${indexlCross}");
		// On clique sur [delete]
		selenium.click("//table[contains(@id,'tab_searchResult')]/tbody/tr[" + indexLCross + " + 1]/td/a[contains(text(),\"[delete]\")]");
		selenium.waitForPageToLoad("30000");
		// On vérife que le message d'erreur apparait.
		assertTrue(selenium.isElementPresent("//table[contains(@id,'applicationErrors')]  | //*/ul/li"));
		verifyEquals("It's not authorized to delete a ready player.", selenium.getText("//table[contains(@id,'applicationErrors')]/tbody/tr/td | //*/ul/li"));
		// On clique sur [back]
		selenium.click("//*[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
		// On sélectionne les Tigers.
		selenium.select("//*[contains(@id,'select_team')]", "label=Tigers");
		selenium.waitForPageToLoad("30000");
		// Pour Woodrow Carpenter.
		Number indexWCarpenter = selenium.getElementIndex("//table[contains(@id,'tab_searchResult')]/tbody/tr[td/span[contains(text(),\"Carpenter\")] and td/span[contains(text(),\"Woodrow\")]]");
		System.out.println("variable index vaut : " + indexWCarpenter);
		// On clique sur [delete]
		selenium.click("//table[contains(@id,'tab_searchResult')]/tbody/tr[" + indexWCarpenter + " + 1]/td/a[contains(text(),\"[delete]\")]");
		selenium.waitForPageToLoad("30000");
		// On vérife que le message d'erreur apparait.
		assertTrue(selenium.isElementPresent("//table[contains(@id,'applicationErrors')]  | //*/ul/li"));
		verifyEquals("It's not authorized to delete a ready player.", selenium.getText("//table[contains(@id,'applicationErrors')]/tbody/tr/td  | //*/ul/li"));
		// On clique sur [back]
		selenium.click("//*[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
		// On sélectionne les Tigers.
		selenium.select("//*[contains(@id,'select_team')]", "label=Tigers");
		selenium.waitForPageToLoad("30000");
		// Pour Gustavo Romeo.
		Number indexGRomero = selenium.getElementIndex("//table[contains(@id,'tab_searchResult')]/tbody/tr[td/span[contains(text(),\"Gustavo\")] and td/span[contains(text(),\"Romero\")]]");
		System.out.println("variable index vaut : " + indexGRomero);
		// On clique sur [delete]
		selenium.click("//table[contains(@id,'tab_searchResult')]/tbody/tr[" + indexGRomero + " + 1]/td/a[contains(text(),\"[delete]\")]");
		selenium.waitForPageToLoad("30000");
		// On vérife que la ligne est bien effacée.
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Romero\")] and td/span[contains(text(),\"Gustavo\")]]"));
		verifyEquals("5", selenium.getXpathCount("//table[contains(@id,'tab_searchResult')]//tbody//tr"));
		// On sélectionne les Tigers.
		// Pour Gerardo George.
		Number indexGGerardo = selenium.getElementIndex("//table[contains(@id,'tab_searchResult')]/tbody/tr[td/span[contains(text(),\"Gerardo\")] and td/span[contains(text(),\"George\")]]");
		System.out.println("variable index vaut : " + indexGGerardo);
		// On clique sur [delete]
		selenium.click("//table[contains(@id,'tab_searchResult')]/tbody/tr[" + indexGGerardo + " + 1]/td/a[contains(text(),\"[delete]\")]");
		selenium.waitForPageToLoad("30000");
		// On vérife que la ligne est bien effacée.
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Gerardo\")] and td/span[contains(text(),\"George\")]]"));
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_searchResult')]//tbody//tr"));
		// On sélectionne les Bears.
		selenium.select("//*[contains(@id,'select_team')]", "label=Bears");
		selenium.waitForPageToLoad("30000");
		// On teste les données pour les Bears.
		verifyEquals("9", selenium.getXpathCount("//table[contains(@id,'tab_searchResult')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_searchResult')]/tbody/tr[td/span[contains(text(),\"Cole\")] and td/span[contains(text(),\"Elbert\")] and td/span[contains(text(),\"171\")] and td/span[contains(text(),\"100\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_searchResult')]/tbody/tr[td/span[contains(text(),\"Russel\")] and td/span[contains(text(),\"Earl\")] and td/span[contains(text(),\"234\")] and td/span[contains(text(),\"60\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_searchResult')]/tbody/tr[td/span[contains(text(),\"Garner\")] and td/span[contains(text(),\"Nick\")] and td/span[contains(text(),\"221\")] and td/span[contains(text(),\"87\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_searchResult')]/tbody/tr[td/span[contains(text(),\"Barnett\")] and td/span[contains(text(),\"Keith\")] and td/span[contains(text(),\"190\")] and td/span[contains(text(),\"66\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_searchResult')]/tbody/tr[td/span[contains(text(),\"Wood\")] and td/span[contains(text(),\"Tommy\")] and td/span[contains(text(),\"173\")] and td/span[contains(text(),\"77\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_searchResult')]/tbody/tr[td/span[contains(text(),\"Robbins\")] and td/span[contains(text(),\"Roderick\")] and td/span[contains(text(),\"164\")] and td/span[contains(text(),\"115\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_searchResult')]/tbody/tr[td/span[contains(text(),\"Chavez\")] and td/span[contains(text(),\"Christian\")] and td/span[contains(text(),\"209\")] and td/span[contains(text(),\"97\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_searchResult')]/tbody/tr[td/span[contains(text(),\"Banks\")] and td/span[contains(text(),\"Guadalupe\")] and td/span[contains(text(),\"167\")] and td/span[contains(text(),\"11\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_searchResult')]/tbody/tr[td/span[contains(text(),\"Soto\")] and td/span[contains(text(),\"Marshall\")] and td/span[contains(text(),\"172\")] and td/span[contains(text(),\"82\")] and td/a[contains(text(),\"[delete]\")]]"));
		// Pour ElbertCole.
		Number indexElbertCole = selenium.getElementIndex("//table[contains(@id,'tab_searchResult')]/tbody/tr[td/span[contains(text(),\"Elbert\")] and td/span[contains(text(),\"Cole\")]]");
		System.out.println("variable index vaut : " + indexElbertCole);
		// On clique sur [delete]
		selenium.click("//table[contains(@id,'tab_searchResult')]/tbody/tr[" + indexElbertCole + " + 1]/td/a[contains(text(),\"[delete]\")]");
		selenium.waitForPageToLoad("30000");
		// On vérife que la ligne est bien effacée.
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Elbert\")] and td/span[contains(text(),\"Cole\")]]"));
		verifyEquals("8", selenium.getXpathCount("//table[contains(@id,'tab_searchResult')]//tbody//tr"));
		// Pour EarlRussell.
		Number indexEarlRussell = selenium.getElementIndex("//table[contains(@id,'tab_searchResult')]/tbody/tr[td/span[contains(text(),\"Earl\")] and td/span[contains(text(),\"Russell\")]]");
		System.out.println("variable index vaut : " + indexEarlRussell);
		// On clique sur [delete]
		selenium.click("//table[contains(@id,'tab_searchResult')]/tbody/tr[" + indexEarlRussell + " + 1]/td/a[contains(text(),\"[delete]\")]");
		selenium.waitForPageToLoad("30000");
		// On vérife que le message d'erreur apparait.
		assertTrue(selenium.isElementPresent("//table[contains(@id,'applicationErrors')]  | //*/ul/li"));
		verifyEquals("It's not authorized to delete a ready player.", selenium.getText("//table[contains(@id,'applicationErrors')]/tbody/tr/td | //*/ul/li"));
		// On clique sur [back]
		selenium.click("//*[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
		// On sélectionne les Bears.
		selenium.select("//*[contains(@id,'select_team')]", "label=Bears");
		selenium.waitForPageToLoad("30000");
		// Pour NickGarner.
		Number indexNickGarner = selenium.getElementIndex("//table[contains(@id,'tab_searchResult')]/tbody/tr[td/span[contains(text(),\"Nick\")] and td/span[contains(text(),\"Garner\")]]");
		System.out.println("variable index vaut : " + indexNickGarner);
		// On clique sur [delete]
		selenium.click("//table[contains(@id,'tab_searchResult')]/tbody/tr[" + indexNickGarner + " + 1]/td/a[contains(text(),\"[delete]\")]");
		selenium.waitForPageToLoad("30000");
		// On vérife que le message d'erreur apparait.
		assertTrue(selenium.isElementPresent("//table[contains(@id,'applicationErrors')]  | //*/ul/li"));
		verifyEquals("It's not authorized to delete a ready player.", selenium.getText("//table[contains(@id,'applicationErrors')]/tbody/tr/td  | //*/ul/li"));
		// On clique sur [back]
		selenium.click("//*[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
		// On sélectionne les Bears.
		selenium.select("//*[contains(@id,'select_team')]", "label=Bears");
		selenium.waitForPageToLoad("30000");
		// Pour KeithBarnett.
		Number indexKeithBarnett = selenium.getElementIndex("//table[contains(@id,'tab_searchResult')]/tbody/tr[td/span[contains(text(),\"Keith\")] and td/span[contains(text(),\"Barnett\")]]");
		System.out.println("variable index vaut : " + indexKeithBarnett);
		// On clique sur [delete]
		selenium.click("//table[contains(@id,'tab_searchResult')]/tbody/tr[" + indexKeithBarnett + " + 1]/td/a[contains(text(),\"[delete]\")]");
		selenium.waitForPageToLoad("30000");
		// On vérife que le message d'erreur apparait.
		assertTrue(selenium.isElementPresent("//table[contains(@id,'applicationErrors')]  | //*/ul/li"));
		verifyEquals("It's not authorized to delete a ready player.", selenium.getText("//table[contains(@id,'applicationErrors')]/tbody/tr/td  | //*/ul/li"));
		// On clique sur [back]
		selenium.click("//*[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
		// On sélectionne les Bears.
		selenium.select("//*[contains(@id,'select_team')]", "label=Bears");
		selenium.waitForPageToLoad("30000");
		// Pour TommyWood.
		Number indexTommyWood = selenium.getElementIndex("//table[contains(@id,'tab_searchResult')]/tbody/tr[td/span[contains(text(),\"Tommy\")] and td/span[contains(text(),\"Wood\")]]");
		System.out.println("variable index vaut : " + indexTommyWood);
		// On clique sur [delete]
		selenium.click("//table[contains(@id,'tab_searchResult')]/tbody/tr[" + indexTommyWood + " + 1]/td/a[contains(text(),\"[delete]\")]");
		selenium.waitForPageToLoad("30000");
		// On vérife que la ligne est bien effacée.
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Tommy\")] and td/span[contains(text(),\"Wood\")]]"));
		verifyEquals("8", selenium.getXpathCount("//table[contains(@id,'tab_searchResult')]//tbody//tr"));
		// Pour RoderickRobbins.
		Number indexRoderickRobbins = selenium.getElementIndex("//table[contains(@id,'tab_searchResult')]/tbody/tr[td/span[contains(text(),\"Roderick\")] and td/span[contains(text(),\"Robbins\")]]");
		System.out.println("variable index vaut : " + indexRoderickRobbins);
		// On clique sur [delete]
		selenium.click("//table[contains(@id,'tab_searchResult')]/tbody/tr[" + indexRoderickRobbins + " + 1]/td/a[contains(text(),\"[delete]\")]");
		selenium.waitForPageToLoad("30000");
		// On vérife que la ligne est bien effacée.
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Roderick\")] and td/span[contains(text(),\"Robbins\")]]"));
		verifyEquals("7", selenium.getXpathCount("//table[contains(@id,'tab_searchResult')]//tbody//tr"));
		// Pour ChristianChavez.
		Number indexChristianChavez = selenium.getElementIndex("//table[contains(@id,'tab_searchResult')]/tbody/tr[td/span[contains(text(),\"Christian\")] and td/span[contains(text(),\"Chavez\")]]");
		System.out.println("variable index vaut : " + indexChristianChavez);
		// On clique sur [delete]
		selenium.click("//table[contains(@id,'tab_searchResult')]/tbody/tr[" + indexChristianChavez + " + 1]/td/a[contains(text(),\"[delete]\")]");
		selenium.waitForPageToLoad("30000");
		// On vérife que le message d'erreur apparait.
		assertTrue(selenium.isElementPresent("//table[contains(@id,'applicationErrors')]  | //*/ul/li"));
		verifyEquals("It's not authorized to delete a ready player.", selenium.getText("//table[contains(@id,'applicationErrors')]/tbody/tr/td  | //*/ul/li"));
		// On clique sur [back]
		selenium.click("//*[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
		// On sélectionne les Bears.
		selenium.select("//*[contains(@id,'select_team')]", "label=Bears");
		selenium.waitForPageToLoad("30000");
		// Pour GuadalupeBanks.
		Number indexGuadalupeBanks = selenium.getElementIndex("//table[contains(@id,'tab_searchResult')]/tbody/tr[td/span[contains(text(),\"Guadalupe\")] and td/span[contains(text(),\"Banks\")]]");
		System.out.println("variable index vaut : " + indexGuadalupeBanks);
		// On clique sur [delete]
		selenium.click("//table[contains(@id,'tab_searchResult')]/tbody/tr[" + indexGuadalupeBanks + " + 1]/td/a[contains(text(),\"[delete]\")]");
		selenium.waitForPageToLoad("30000");
		// On vérife que la ligne est bien effacée.
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Guadalupe\")] and td/span[contains(text(),\"Banks\")]]"));
		verifyEquals("8", selenium.getXpathCount("//table[contains(@id,'tab_searchResult')]//tbody//tr"));
		// Pour MarshallSoto.
		Number indexMarshallSoto = selenium.getElementIndex("//table[contains(@id,'tab_searchResult')]/tbody/tr[td/span[contains(text(),\"Marshall\")] and td/span[contains(text(),\"Soto\")]]");
		System.out.println("variable index vaut : " + indexMarshallSoto);
		// On clique sur [delete]
		selenium.click("//table[contains(@id,'tab_searchResult')]/tbody/tr[" + indexMarshallSoto + " + 1]/td/a[contains(text(),\"[delete]\")]");
		selenium.waitForPageToLoad("30000");
		// On vérife que la ligne est bien effacée.
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Marshall\")] and td/span[contains(text(),\"Soto\")]]"));
		verifyEquals("7", selenium.getXpathCount("//table[contains(@id,'tab_searchResult')]//tbody//tr"));
		// On sélectionne les Chargers.
		selenium.select("//*[contains(@id,'select_team')]", "label=Chargers");
		selenium.waitForPageToLoad("30000");
		// On teste les données pour les Chargers.
		verifyEquals("8", selenium.getXpathCount("//table[contains(@id,'tab_searchResult')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_searchResult')]/tbody/tr[td/span[contains(text(),\"Dennis\")] and td/span[contains(text(),\"Abel\")] and td/span[contains(text(),\"208\")] and td/span[contains(text(),\"105\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_searchResult')]/tbody/tr[td/span[contains(text(),\"Dawson\")] and td/span[contains(text(),\"Ricky\")] and td/span[contains(text(),\"168\")] and td/span[contains(text(),\"115\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_searchResult')]/tbody/tr[td/span[contains(text(),\"Meyer\")] and td/span[contains(text(),\"Allan\")] and td/span[contains(text(),\"229\")] and td/span[contains(text(),\"114\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_searchResult')]/tbody/tr[td/span[contains(text(),\"Tate\")] and td/span[contains(text(),\"Ivan\")] and td/span[contains(text(),\"175\")] and td/span[contains(text(),\"143\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_searchResult')]/tbody/tr[td/span[contains(text(),\"Hodges\")] and td/span[contains(text(),\"Michael\")] and td/span[contains(text(),\"204\")] and td/span[contains(text(),\"158\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_searchResult')]/tbody/tr[td/span[contains(text(),\"Conner\")] and td/span[contains(text(),\"Cecil\")] and td/span[contains(text(),\"190\")] and td/span[contains(text(),\"160\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_searchResult')]/tbody/tr[td/span[contains(text(),\"Gilbert\")] and td/span[contains(text(),\"Todd\")] and td/span[contains(text(),\"211\")] and td/span[contains(text(),\"115\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_searchResult')]/tbody/tr[td/span[contains(text(),\"Lawson\")] and td/span[contains(text(),\"Vernon\")] and td/span[contains(text(),\"208\")] and td/span[contains(text(),\"146\")] and td/a[contains(text(),\"[delete]\")]]"));
		// Pour AbelDennis.
		Number indexAbelDennis = selenium.getElementIndex("//table[contains(@id,'tab_searchResult')]/tbody/tr[td/span[contains(text(),\"Abel\")] and td/span[contains(text(),\"Dennis\")]]");
		System.out.println("variable index vaut : " + indexAbelDennis);
		// On clique sur [delete]
		selenium.click("//table[contains(@id,'tab_searchResult')]/tbody/tr[" + indexAbelDennis + " + 1]/td/a[contains(text(),\"[delete]\")]");
		selenium.waitForPageToLoad("30000");
		// On vérife que le message d'erreur apparait.
		assertTrue(selenium.isElementPresent("//table[contains(@id,'applicationErrors')]  | //*/ul/li"));
		verifyEquals("It's not authorized to delete a ready player.", selenium.getText("//table[contains(@id,'applicationErrors')]/tbody/tr/td | //*/ul/li"));
		// On clique sur [back]
		selenium.click("//*[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
		// On sélectionne les Chargers.
		selenium.select("//*[contains(@id,'select_team')]", "label=Chargers");
		selenium.waitForPageToLoad("30000");
		// Pour RickyDawson.
		Number indexRickyDawson = selenium.getElementIndex("//table[contains(@id,'tab_searchResult')]/tbody/tr[td/span[contains(text(),\"Ricky\")] and td/span[contains(text(),\"Dawson\")]]");
		System.out.println("variable index vaut : " + indexRickyDawson);
		// On clique sur [delete]
		selenium.click("//table[contains(@id,'tab_searchResult')]/tbody/tr[" + indexRickyDawson + " + 1]/td/a[contains(text(),\"[delete]\")]");
		selenium.waitForPageToLoad("30000");
		// On vérife que la ligne est bien effacée.
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Ricky\")] and td/span[contains(text(),\"Dawson\")]]"));
		verifyEquals("7", selenium.getXpathCount("//table[contains(@id,'tab_searchResult')]//tbody//tr"));
		// Pour AllanMeyer.
		Number indexAllanMeyer = selenium.getElementIndex("//table[contains(@id,'tab_searchResult')]/tbody/tr[td/span[contains(text(),\"Allan\")] and td/span[contains(text(),\"Meyer\")]]");
		System.out.println("variable index vaut : " + indexAllanMeyer);
		// On clique sur [delete]
		selenium.click("//table[contains(@id,'tab_searchResult')]/tbody/tr[" + indexAllanMeyer + " + 1]/td/a[contains(text(),\"[delete]\")]");
		selenium.waitForPageToLoad("30000");
		// On vérife que le message d'erreur apparait.
		assertTrue(selenium.isElementPresent("//table[contains(@id,'applicationErrors')]  | //*/ul/li"));
		verifyEquals("It's not authorized to delete a ready player.", selenium.getText("//table[contains(@id,'applicationErrors')]/tbody/tr/td  | //*/ul/li"));
		// On clique sur [back]
		selenium.click("//*[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
		// On sélectionne les Chargers.
		selenium.select("//*[contains(@id,'select_team')]", "label=Chargers");
		selenium.waitForPageToLoad("30000");
		// Pour IvanTate.
		Number indexIvanTate = selenium.getElementIndex("//table[contains(@id,'tab_searchResult')]/tbody/tr[td/span[contains(text(),\"Ivan\")] and td/span[contains(text(),\"Tate\")]]");
		System.out.println("variable index vaut : " + indexIvanTate);
		// On clique sur [delete]
		selenium.click("//table[contains(@id,'tab_searchResult')]/tbody/tr[" + indexIvanTate + " + 1]/td/a[contains(text(),\"[delete]\")]");
		selenium.waitForPageToLoad("30000");
		// On vérife que la ligne est bien effacée.
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Ivan\")] and td/span[contains(text(),\"Tate\")]]"));
		verifyEquals("7", selenium.getXpathCount("//table[contains(@id,'tab_searchResult')]//tbody//tr"));
		// Pour MichaelHodges.
		Number indexMichaelHodges = selenium.getElementIndex("//table[contains(@id,'tab_searchResult')]/tbody/tr[td/span[contains(text(),\"Michael\")] and td/span[contains(text(),\"Hodges\")]]");
		System.out.println("variable index vaut : " + indexMichaelHodges);
		// On clique sur [delete]
		selenium.click("//table[contains(@id,'tab_searchResult')]/tbody/tr[" + indexMichaelHodges + " + 1]/td/a[contains(text(),\"[delete]\")]");
		selenium.waitForPageToLoad("30000");
		// On vérife que la ligne est bien effacée.
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Michael\")] and td/span[contains(text(),\"Hodges\")]]"));
		verifyEquals("6", selenium.getXpathCount("//table[contains(@id,'tab_searchResult')]//tbody//tr"));
		// Pour CecilConner.
		Number indexCecilConner = selenium.getElementIndex("//table[contains(@id,'tab_searchResult')]/tbody/tr[td/span[contains(text(),\"Cecil\")] and td/span[contains(text(),\"Conner\")]]");
		System.out.println("variable index vaut : " + indexCecilConner);
		// On clique sur [delete]
		selenium.click("//table[contains(@id,'tab_searchResult')]/tbody/tr[" + indexCecilConner + " + 1]/td/a[contains(text(),\"[delete]\")]");
		selenium.waitForPageToLoad("30000");
		// On vérife que la ligne est bien effacée.
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Cecil\")] and td/span[contains(text(),\"Conner\")]]"));
		verifyEquals("5", selenium.getXpathCount("//table[contains(@id,'tab_searchResult')]//tbody//tr"));
		// Pour ToddGilbert.
		Number indexToddGilbert = selenium.getElementIndex("//table[contains(@id,'tab_searchResult')]/tbody/tr[td/span[contains(text(),\"Todd\")] and td/span[contains(text(),\"Gilbert\")]]");
		System.out.println("variable index vaut : " + indexToddGilbert);
		// On clique sur [delete]
		selenium.click("//table[contains(@id,'tab_searchResult')]/tbody/tr[" + indexToddGilbert + " + 1]/td/a[contains(text(),\"[delete]\")]");
		selenium.waitForPageToLoad("30000");
		// On vérife que le message d'erreur apparait.
		assertTrue(selenium.isElementPresent("//table[contains(@id,'applicationErrors')]  | //*/ul/li"));
		verifyEquals("It's not authorized to delete a ready player.", selenium.getText("//table[contains(@id,'applicationErrors')]/tbody/tr/td  | //*/ul/li"));
		// On clique sur [back]
		selenium.click("//*[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
		// On sélectionne les Chargers.
		selenium.select("//*[contains(@id,'select_team')]", "label=Chargers");
		selenium.waitForPageToLoad("30000");
		// Pour VernonLawson.
		Number indexVernonLawson = selenium.getElementIndex("//table[contains(@id,'tab_searchResult')]/tbody/tr[td/span[contains(text(),\"Vernon\")] and td/span[contains(text(),\"Lawson\")]]");
		System.out.println("variable index vaut : " + indexVernonLawson);
		// On clique sur [delete]
		selenium.click("//table[contains(@id,'tab_searchResult')]/tbody/tr[" + indexVernonLawson + " + 1]/td/a[contains(text(),\"[delete]\")]");
		selenium.waitForPageToLoad("30000");
		// On vérife que le message d'erreur apparait.
		assertTrue(selenium.isElementPresent("//table[contains(@id,'applicationErrors')]  | //*/ul/li"));
		verifyEquals("It's not authorized to delete a ready player.", selenium.getText("//table[contains(@id,'applicationErrors')]/tbody/tr/td  | //*/ul/li"));
		// On clique sur [back]
		selenium.click("//*[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
		// On selectionne les Hawks.
		selenium.select("//*[contains(@id,'select_team')]", "label=Hawks");
		selenium.waitForPageToLoad("30000");
		// On teste les données pour les Hawks.
		verifyEquals("3", selenium.getXpathCount("//table[contains(@id,'tab_searchResult')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_searchResult')]/tbody/tr[td/span[contains(text(),\"Grant\")] and td/span[contains(text(),\"Jerome\")] and td/span[contains(text(),\"205\")] and td/span[contains(text(),\"105\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_searchResult')]/tbody/tr[td/span[contains(text(),\"Lambert\")] and td/span[contains(text(),\"Stephen\")] and td/span[contains(text(),\"153\")] and td/span[contains(text(),\"107\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_searchResult')]/tbody/tr[td/span[contains(text(),\"Castillo\")] and td/span[contains(text(),\"Ralph\")] and td/span[contains(text(),\"229\")] and td/span[contains(text(),\"115\")] and td/a[contains(text(),\"[delete]\")]]"));
		// Pour JeromeGrant.
		Number indexJeromeGrant = selenium.getElementIndex("//table[contains(@id,'tab_searchResult')]/tbody/tr[td/span[contains(text(),\"Jerome\")] and td/span[contains(text(),\"Grant\")]]");
		System.out.println("variable index vaut : " + indexJeromeGrant);
		// On clique sur [delete]
		selenium.click("//table[contains(@id,'tab_searchResult')]/tbody/tr[" + indexJeromeGrant + " + 1]/td/a[contains(text(),\"[delete]\")]");
		selenium.waitForPageToLoad("30000");
		// On vérife que le message d'erreur apparait.
		assertTrue(selenium.isElementPresent("//table[contains(@id,'applicationErrors')]  | //*/ul/li"));
		verifyEquals("It's not authorized to delete a ready player.", selenium.getText("//table[contains(@id,'applicationErrors')]/tbody/tr/td | //*/ul/li"));
		// On clique sur [back]
		selenium.click("//*[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
		// On sélectionne les Hawks.
		selenium.select("//*[contains(@id,'select_team')]", "label=Hawks");
		selenium.waitForPageToLoad("30000");
		// Pour StephenLambert.
		Number indexStephenLambert = selenium.getElementIndex("//table[contains(@id,'tab_searchResult')]/tbody/tr[td/span[contains(text(),\"Stephen\")] and td/span[contains(text(),\"Lambert\")]]");
		System.out.println("variable index vaut : " + indexStephenLambert);
		// On clique sur [delete]
		selenium.click("//table[contains(@id,'tab_searchResult')]/tbody/tr[" + indexStephenLambert + " + 1]/td/a[contains(text(),\"[delete]\")]");
		selenium.waitForPageToLoad("30000");
		// On vérife que la ligne est bien effacée.
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Stephen\")] and td/span[contains(text(),\"Lambert\")]]"));
		verifyEquals("2", selenium.getXpathCount("//table[contains(@id,'tab_searchResult')]//tbody//tr"));
		// Pour RalphCastillo.
		Number indexRalphCastillo = selenium.getElementIndex("//table[contains(@id,'tab_searchResult')]/tbody/tr[td/span[contains(text(),\"Ralph\")] and td/span[contains(text(),\"Castillo\")]]");
		System.out.println("variable index vaut : " + indexRalphCastillo);
		// On clique sur [delete]
		selenium.click("//table[contains(@id,'tab_searchResult')]/tbody/tr[" + indexRalphCastillo + " + 1]/td/a[contains(text(),\"[delete]\")]");
		selenium.waitForPageToLoad("30000");
		// On vérife que le message d'erreur apparait.
		assertTrue(selenium.isElementPresent("//table[contains(@id,'applicationErrors')]  | //*/ul/li"));
		verifyEquals("It's not authorized to delete a ready player.", selenium.getText("//table[contains(@id,'applicationErrors')]/tbody/tr/td  | //*/ul/li"));
		// On clique sur [back]
		selenium.click("//*[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
		// On séléctionne les Braves.
		selenium.select("//*[contains(@id,'select_team')]", "label=Braves");
		selenium.waitForPageToLoad("30000");
		// On teste les données pour les Braves.
		verifyEquals("4", selenium.getXpathCount("//table[contains(@id,'tab_searchResult')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_searchResult')]/tbody/tr[td/span[contains(text(),\"Ramos\")] and td/span[contains(text(),\"Howard\")] and td/span[contains(text(),\"199\")] and td/span[contains(text(),\"107\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_searchResult')]/tbody/tr[td/span[contains(text(),\"Nash\")] and td/span[contains(text(),\"Sylvester\")] and td/span[contains(text(),\"184\")] and td/span[contains(text(),\"68\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_searchResult')]/tbody/tr[td/span[contains(text(),\"Robertson\")] and td/span[contains(text(),\"Salvatore\")] and td/span[contains(text(),\"190\")] and td/span[contains(text(),\"156\")] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_searchResult')]/tbody/tr[td/span[contains(text(),\"Riley\")] and td/span[contains(text(),\"Jim\")] and td/span[contains(text(),\"171\")] and td/span[contains(text(),\"104\")] and td/a[contains(text(),\"[delete]\")]]"));
		// Pour HowardRamos.
		Number indexHowardRamos = selenium.getElementIndex("//table[contains(@id,'tab_searchResult')]/tbody/tr[td/span[contains(text(),\"Howard\")] and td/span[contains(text(),\"Ramos\")]]");
		System.out.println("variable index vaut : " + indexHowardRamos);
		// On clique sur [delete]
		selenium.click("//table[contains(@id,'tab_searchResult')]/tbody/tr[" + indexHowardRamos + " + 1]/td/a[contains(text(),\"[delete]\")]");
		selenium.waitForPageToLoad("30000");
		// On vérife que le message d'erreur apparait.
		assertTrue(selenium.isElementPresent("//table[contains(@id,'applicationErrors')]  | //*/ul/li"));
		verifyEquals("It's not authorized to delete a ready player.", selenium.getText("//table[contains(@id,'applicationErrors')]/tbody/tr/td | //*/ul/li"));
		// On clique sur [back]
		selenium.click("//*[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
		// On sélectionne les Braves.
		selenium.select("//*[contains(@id,'select_team')]", "label=Braves");
		selenium.waitForPageToLoad("30000");
		// Pour SylvesterNash.
		Number indexSylvesterNash = selenium.getElementIndex("//table[contains(@id,'tab_searchResult')]/tbody/tr[td/span[contains(text(),\"Sylvester\")] and td/span[contains(text(),\"Nash\")]]");
		System.out.println("variable index vaut : " + indexSylvesterNash);
		// On clique sur [delete]
		selenium.click("//table[contains(@id,'tab_searchResult')]/tbody/tr[" + indexSylvesterNash + " + 1]/td/a[contains(text(),\"[delete]\")]");
		selenium.waitForPageToLoad("30000");
		// On vérife que le message d'erreur apparait.
		assertTrue(selenium.isElementPresent("//table[contains(@id,'applicationErrors')]  | //*/ul/li"));
		verifyEquals("It's not authorized to delete a ready player.", selenium.getText("//table[contains(@id,'applicationErrors')]/tbody/tr/td  | //*/ul/li"));
		// On clique sur [back]
		selenium.click("//*[contains(@id,'lnk_back')]");
		selenium.waitForPageToLoad("30000");
		// On sélectionne les Braves.
		selenium.select("//*[contains(@id,'select_team')]", "label=Braves");
		selenium.waitForPageToLoad("30000");
		// Pour SalvatoreRobertson.
		Number indexSalvatoreRobertson = selenium.getElementIndex("//table[contains(@id,'tab_searchResult')]/tbody/tr[td/span[contains(text(),\"Salvatore\")] and td/span[contains(text(),\"Robertson\")]]");
		System.out.println("variable index vaut : " + indexSalvatoreRobertson);
		// On clique sur [delete]
		selenium.click("//table[contains(@id,'tab_searchResult')]/tbody/tr[" + indexSalvatoreRobertson + " + 1]/td/a[contains(text(),\"[delete]\")]");
		selenium.waitForPageToLoad("30000");
		// On vérife que la ligne est bien effacée.
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Salvatore\")] and td/span[contains(text(),\"Robertson\")]]"));
		verifyEquals("3", selenium.getXpathCount("//table[contains(@id,'tab_searchResult')]//tbody//tr"));
		// Pour JimRiley.
		Number indexJimRiley = selenium.getElementIndex("//table[contains(@id,'tab_searchResult')]/tbody/tr[td/span[contains(text(),\"Jim\")] and td/span[contains(text(),\"Riley\")]]");
		System.out.println("variable index vaut : " + indexJimRiley);
		// On clique sur [delete]
		selenium.click("//table[contains(@id,'tab_searchResult')]/tbody/tr[" + indexJimRiley + " + 1]/td/a[contains(text(),\"[delete]\")]");
		selenium.waitForPageToLoad("30000");
		// On vérife que la ligne est bien effacée.
		assertFalse(selenium.isElementPresent("//table[contains(@id,'tab_teams')]/tbody/tr[td/span[contains(text(),\"Jim\")] and td/span[contains(text(),\"Riley\")]]"));
		verifyEquals("2", selenium.getXpathCount("//table[contains(@id,'tab_searchResult')]//tbody//tr"));
	}

}
