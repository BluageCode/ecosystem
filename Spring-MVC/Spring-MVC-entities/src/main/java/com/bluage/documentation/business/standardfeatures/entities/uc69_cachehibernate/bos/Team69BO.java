/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.business.standardfeatures.entities.uc69_cachehibernate.bos;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.inject.Inject;
import javax.inject.Named;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.LazyInitializationException;

import com.bluage.documentation.business.common.bos.TeamBO;
import com.bluage.documentation.business.standardfeatures.entities.uc69_cachehibernate.entities.daofinder.Team69DAOFinderImpl;
import com.netfective.bluage.core.exception.ApplicationException;

/**
 * Team69BO
 *
* @see com.netfective.bluage.core.business.IBusinessObject
* * @see TeamBO
 */
@XmlRootElement

public class Team69BO
 extends TeamBO {
	
	/**
	 * Default SUID for Team69BO.
	 */
	private static final long serialVersionUID = 1L;
	

	@Inject
	@Named ("com.bluage.documentation.business.standardfeatures.entities.uc69_cachehibernate.entities.daofinder.Team69Finder") 
	private Team69DAOFinderImpl finder;

																			/**
	 * Entity property: players
	 *
	 */
private Set<Player69BO> players = new HashSet<Player69BO>();



   /**
	* public default constructor for class Team69BO
	*/
	public Team69BO(){
		//NOP Team69BO
	}


																			/**
	 * Getter for players of class Team69BO.
	 *
	 * @return {@link java.util.Set<com.bluage.documentation.business.standardfeatures.entities.uc69_cachehibernate.bos.Player69BO>}
	 * @generated
	 */ 
public  Set<Player69BO> getPlayers() {
	return this.players;
			}


	/**
	 * Setter for players of class Team69BO.
	 * @param value {@link java.util.Set<com.bluage.documentation.business.standardfeatures.entities.uc69_cachehibernate.bos.Player69BO>}
	 * @generated
	 */

public  void setPlayers(final Set<Player69BO> players) {
	this.players = players;
}

	/**
	 * Remove an entity from the com.bluage.documentation.business.standardfeatures.entities.uc69_cachehibernate.bos.Player69BO collection.
	 *
	 * @param entity
	 *		: {@link Set<Player69BO>} the entity
	 * @throws ApplicationException
     */
public void removeFromPlayers( Player69BO entity ) throws ApplicationException {
	try {
		if( getPlayers() != null && getPlayers().contains(entity) ) {
			getPlayers().remove( entity );
		entity.setTeam69( null );
		}
		} catch ( LazyInitializationException e ) {
				Team69BO bo = ( Team69BO ) finder.findByPrimaryKey( getId() );
			finder.getHibernateTemplate().initialize( bo.getPlayers() );
			setPlayers( bo.getPlayers() );
			// evict object from session to avoid NonUniqueObjectException
			finder.getHibernateTemplate().evict( bo );
		
			if( getPlayers() != null && getPlayers().contains( entity ) ) {
				getPlayers().remove( entity );
				entity.setTeam69( null );
			}
				}
	}
	
	/**
	 * Add entity to the com.bluage.documentation.business.standardfeatures.entities.uc69_cachehibernate.bos.Player69BO collection.
	 *
	 * @param entity
	 *		{@link Player69BO} the entity
	 * @throws ApplicationException
     */
public void addToPlayers( Player69BO entity ) throws ApplicationException
	{
		try {
			if( getPlayers() != null && !getPlayers().contains( entity ) ) {
				getPlayers().add( entity );
				entity.setTeam69( this );
			}
			} catch ( LazyInitializationException e ) {
			Team69BO bo = ( Team69BO ) finder.findByPrimaryKey( getId() );
		finder.getHibernateTemplate().initialize( bo.getPlayers() );
		setPlayers( bo.getPlayers() );
		// evict object from session to avoid NonUniqueObjectException
		finder.getHibernateTemplate().evict( bo );
		if( getPlayers() != null && !getPlayers().contains( entity ) ) {
			getPlayers().add( entity );
			entity.setTeam69( this );
		}
			}
	}
	
	/**
	 * Add entitys to the com.bluage.documentation.business.standardfeatures.entities.uc69_cachehibernate.bos.Player69BO collection
	 *
	 * @param entities			
	 *		: {@link java.util.List} A list of entities
	 * @throws ApplicationException 
     */	
public void addAllToPlayers( Collection<Player69BO> entities ) throws ApplicationException {
	for( Player69BO entity : entities) {
		if( getPlayers()!=null ) {
			addToPlayers( entity );
		}
	}
}
	
	/**
	 * Remove entities form the com.bluage.documentation.business.standardfeatures.entities.uc69_cachehibernate.bos.Player69BO collection
	 *
	 * @param entities				
	 *		: {@link java.util.List} a list of entities
	 * @throws ApplicationException
     */	
public void removeAllFromPlayers( Collection<Player69BO> entities ) throws ApplicationException {
	for( Player69BO entity : entities) {
		if( getPlayers()!=null && getPlayers().contains( entity ) )	{		
			removeFromPlayers( entity );
		}
	}
}
	
	/**
	 * Remove all items form the collection com.bluage.documentation.business.standardfeatures.entities.uc69_cachehibernate.bos.Player69BO
	 *
	 * @throws ApplicationException
     */	
public void removeAllFromPlayers() throws ApplicationException {
	Set<Player69BO> entities = this.getPlayers();
	if ( entities == null ) {
		return;
	}
	Iterator<Player69BO> it = entities.iterator();
	while ( it.hasNext() ) {
    	Player69BO entity = it.next(); 
		entity.setTeam69( null ); 
	}
	entities.clear();
}

/**
	 * Returns <code>true</code> if the argument is an Team69 instance and all identifiers for this entity
     * equal the identifiers of the argument entity. The <code>equals</code> method of the parent entity
     * will also need to return <code>true</code>. Returns <code>false</code> otherwise.
     *
     * @see TeamBO#hashCode()
	 * @param object  object to compare with current instance      
     * @return boolean true if equals false if not
     */
    public boolean equals(final Object object )
    {
		if ( this == object )
        {
            return true;
        }
        if ( !( object instanceof Team69BO ) )
        {
            return false;
        }
        final Team69BO that = (Team69BO)object;
    
    	            if (        				        			this.getId() == null
        	        ) {
        	return false;
        }
		return new EqualsBuilder()
			.append(this.getId(), that.getId())
			 	.isEquals();
	}
	/**

	 * Returns a hash code based on this entity's identifiers and the hash code of the parent entity.
     *
     * @see TeamBO#hashCode()
     * @return int
     */    
    public int hashCode() {
        return new HashCodeBuilder()
             .append(getId())
				 	.appendSuper(super.hashCode())
            .toHashCode();
    }
}

