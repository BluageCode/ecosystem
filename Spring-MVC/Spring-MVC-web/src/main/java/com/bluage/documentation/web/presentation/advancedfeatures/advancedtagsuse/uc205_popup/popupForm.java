/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.advancedfeatures.advancedtagsuse.uc205_popup;


import java.io.Serializable;
import java.util.List;

import com.bluage.documentation.business.advancedfeatures.advancedtagsuse.uc205_popup.bos.Player205BO;
import com.netfective.bluage.core.web.AbstractForm;
import com.netfective.bluage.core.web.IForm;

/**
* Class : popupForm
*/
public class popupForm extends AbstractForm implements Serializable, IForm {
	
	/** Constant : serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	//CONSTANT : allPlayers
	private static final String ALL_PLAYERS = "allPlayers";
	//CONSTANT : cp_traitementShowPopUp
	private static final String CP_TRAITEMENT_SHOW_POP_UP = "cp_traitementShowPopUp";
	/**
	 * 	Property: allPlayers 
	 */
	private List<Player205BO> allPlayers;
	/**
	 * 	Property: cp_traitementShowPopUp 
	 */
	private Boolean cp_traitementShowPopUp = Boolean.FALSE;
/**
	 * Default constructor : popupForm
	 */
	public popupForm() {
		super();
		// Initialize : allPlayers
		this.allPlayers = new java.util.ArrayList<com.bluage.documentation.business.advancedfeatures.advancedtagsuse.uc205_popup.bos.Player205BO>();
					this.setAlwaysCallPreControllers(true);
	}
	
	/**
	 * 	Getter : allPlayers 
	 * 	@return : Return the allPlayers instance.
	 */
	public List<Player205BO> getAllPlayers(){
		return allPlayers; // For popupForm
	}
	
	/**
	 * 	Setter : allPlayers 
	 *  @param allPlayersinstance : The instance to set.
	 */
	public void setAllPlayers(final List<Player205BO> allPlayersinstance){
		this.allPlayers = allPlayersinstance;// For popupForm
	}
	/**
	 * 	Getter : cp_traitementShowPopUp 
	 * 	@return: cp_traitementShowPopUpinstance
	 */
	public Boolean getCp_traitementShowPopUp(){
		return this.cp_traitementShowPopUp; 
	}
	
	/**
	 * 	Setter : cp_traitementShowPopUp 
	 * 	@param : cp_traitementShowPopUpinstance : the instance to set
	 */
	public void setCp_traitementShowPopUp(final Boolean cp_traitementShowPopUp){
		this.cp_traitementShowPopUp = cp_traitementShowPopUp; 
	}
/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "popupForm [ "+
ALL_PLAYERS +" = " + allPlayers + "]";
	}
	
	/**
	 * Method setInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public void setInstanceByInstanceName(final String instanceName, final Object instance) {
		// Set the instance AllPlayers.
		if(ALL_PLAYERS.equals(instanceName)){// For popupForm
			this.setAllPlayers((List<Player205BO>)instance); // For popupForm
		}
				if(CP_TRAITEMENT_SHOW_POP_UP.equals(instanceName)){ // For popupForm
			this.setCp_traitementShowPopUp((Boolean)instance); // For popupForm
		}
	}
	
	/**
	 * Method getInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public Object getInstanceByInstanceName(final String instanceName){
		// Temporary object for tmppopupForm.
		Object tmppopupForm = null;
		
			
		// Get the instance AllPlayers for popupForm.
		if(ALL_PLAYERS.equals(instanceName)){ // For popupForm
			tmppopupForm = this.getAllPlayers(); // For popupForm
		}
		if(CP_TRAITEMENT_SHOW_POP_UP.equals(instanceName)){// For popupForm
			tmppopupForm = this.getCp_traitementShowPopUp();// For popupForm
		}
		return tmppopupForm;// For popupForm
	}
	
			}
