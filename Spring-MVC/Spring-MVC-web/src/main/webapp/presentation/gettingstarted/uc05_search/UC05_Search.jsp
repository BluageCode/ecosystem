<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file="/include.jsp"  %>
<html >
<head >
<title  > 
<fmt:message key="cUC05_Search_BLU_AGE_DOCUMENTATION"/>
</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"  > 
</meta>
<link href="<c:url value= "/presentation/_style/_style.css"/>" rel="stylesheet" type="text/css"  /> 
<link href="<c:url value= "/presentation/_include/jquery/rd_lib.css"/>" rel="stylesheet" type="text/css"  /> 
<script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/jquery.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/rd_setup.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/rd_lib.js"/>" space="preserve"  ></script><link rel="shortcut icon" href="<c:url value= "/presentation/_style/favicon.ico"/>"  /> 

</head>

<body class=" " > 
<div id="rd_ghost"  > 
<small  > 
<fmt:message key="cUC05_Search___CSS_DEACTIVATED"/>
</small>
</div>
<div id="rd_jghost"  > 
<small  > 
<fmt:message key="cUC05_Search___JAVASCRIPT_DEACTIVATED"/>
</small>
</div>
<div id="shape"  > 
<div class="ui-layout-center"  > 
<div class="east-north"  > 
	<form:form id="uC05_Search" commandName="uC05_SearchForm"  enctype="application/x-www-form-urlencoded" method="post">
<h2 class="rd_first"  > 
<fmt:message key="cUC05_Search_GETTING_STARTED__UC05__SEARC"/>
</h2>
<table class="rd_tableform"  >
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC05_Search_FIRST_NAME"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
<form:input id="txt_firstname" path="playerCriteria.firstName"  
		/>
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC05_Search_LAST_NAME"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
<form:input id="txt_lastname" path="playerCriteria.lastName"  
		/>
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC05_Search_POSITION"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
<form:select id="select_position" path="playerCriteria.position.code"   
		>
		<form:options items="${uC05_SearchForm.allPositions}" itemValue="code" itemLabel="name" />
</form:select>
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<fmt:message key="cUC05_Search_"/>
</td> 
<td rowspan="1" colspan="1"  >
<a id="lnk_searchProperties"onclick="javascript:btn_click_cExecuteUC05_Search(this.href);return false;"				href="<c:url value="/presentation/gettingstarted/uc05_search/UC05_Search/lnk_searchProperties.html"/>"
							 shape="rect"  >									 
<fmt:message key="cUC05_Search_SEARCH_WITH_ABOVE_PROPERTIES"/>
</a>
</td> 
</tr>
<tr  > 
<td colspan="2" rowspan="1"  >
<fmt:message key="cUC05_Search_OR"/>
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC05_Search_LAST_NAME_BEGINS_WITH"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
<form:input id="txt_lastnamebeginswith" path="hqlCriteria.lastName"  
		/>
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC05_Search_HEIGHT_BETWEEN"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
<form:input id="txt_minheight" path="hqlCriteria.minHeight"  size="5" 
		/>
<fmt:message key="cUC05_Search____AND___"/>
<form:input id="txt_maxheight" path="hqlCriteria.maxHeight"  size="5" 
		/>
<fmt:message key="cUC05_Search____CM"/>
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<fmt:message key="cUC05_Search__64"/>
</td> 
<td rowspan="1" colspan="1"  >
<a id="lnk_searchCriteria"onclick="javascript:btn_click_cExecuteUC05_Search(this.href);return false;"				href="<c:url value="/presentation/gettingstarted/uc05_search/UC05_Search/lnk_searchCriteria.html"/>"
							 shape="rect"  >									 
<fmt:message key="cUC05_Search_SEARCH_WITH_AN_HQL_OPERATION"/>
</a>
</td> 
</tr>
</table> 
<display:table uid="tab_searchResult"  sort="list" requestURI="" name="sessionScope.uC05_SearchForm.searchResult" class="rd_tabledata"  >
	<c:set   var="rownum">
		<%=(tab_searchResult !=	null) ? new Integer(tab_searchResult_rowNum.intValue() - 1) : new Integer(0) %>
	</c:set>
	<c:set   var="idRow">
		<%=(tab_searchResult != null ) ? tab_searchResult_rowNum.intValue() - 1 : -1 %>
	</c:set>
	<c:set   var="idTab">
		<%=(tab_searchResult != null ) ? "tab_searchResult" : null %>
	</c:set>
	<c:set var="tableHashId">
		<%= new org.displaytag.util.ParamEncoder("tab_searchResult").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE) %>
	</c:set>
	<c:set var="tableHashId2">
		<%=request.getParameter(new org.displaytag.util.ParamEncoder("tab_searchResult").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE)) %>
	</c:set>	
	
<display:column format=""  titleKey="cUC05_Search_LAST_NAME_65">
		<span id="txt_lastname1">
	<c:out value="${tab_searchResult.lastName}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="cUC05_Search_FIRST_NAME_66">
		<span id="txt_firstname1">
	<c:out value="${tab_searchResult.firstName}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="cUC05_Search_POSITION_67">
		<span id="txt_position">
	<c:out value="${tab_searchResult.position.name}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="cUC05_Search_HEIGHT_CM">
		<span id="txt_height">
	<c:out value="${tab_searchResult.height}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="cUC05_Search__68">
<a id="lnk_view[${rownum}]"onclick="javascript:btn_click_cExecuteUC05_Search(this.href);return false;"			href="<c:url value="/presentation/gettingstarted/uc05_search/UC05_Search/lnk_view.html?id="/><c:out value="${idRow}" />&tab=<c:out value="tab_searchResult" />&<c:out value="${tableHashId}"/>=<c:out value="${tableHashId2}"/>"
	 shape="rect"  >									 
<fmt:message key="cUC05_Search_VIEW"/>
</a>
</display:column>
</display:table>
	</form:form>
	<script language="javascript">
		function btn_click_cExecuteUC05_Search(url) {
			UC05_Searchvar = document.getElementById('uC05_Search');
			UC05_Searchvar.setAttribute('action', url);
			UC05_Searchvar.submit();
		}
		function btn_click_cSetActionUC05_Search(url) {
			UC05_Searchvar = document.getElementById('uC05_Search');
			UC05_Searchvar.setAttribute('action', url);
		}
	</script>
</div>
<div class="east-center"  > 
<tiles:insertDefinition	name="presentation/gettingstarted/uc05_search/UC05_Searchcmp_tabs" />
</div>
</div>
<div class="ui-layout-north"  > 
<tiles:insertDefinition	name="presentation/gettingstarted/uc05_search/UC05_Searchcmp_header" />
</div>
<div class="ui-layout-south"  > 
<fmt:message key="cUC05_Search___"/>
<span id="rd_jversion"  > 
<fmt:message key="cUC05_Search_BLU_AGE_CORPORATION"/>
</span>
</div>
<div class="ui-layout-west"  > 
<tiles:insertDefinition	name="presentation/gettingstarted/uc05_search/UC05_Searchcmp_treeview" />
</div>
</div>
</body>
</html>
