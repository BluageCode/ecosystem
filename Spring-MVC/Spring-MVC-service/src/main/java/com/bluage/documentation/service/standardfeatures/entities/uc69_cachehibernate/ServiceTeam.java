/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.service.standardfeatures.entities.uc69_cachehibernate;


import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import com.bluage.documentation.business.standardfeatures.entities.uc69_cachehibernate.bos.Team69BO;
import com.bluage.documentation.business.standardfeatures.entities.uc69_cachehibernate.entities.daofinder.Team69DAOFinderImpl;
import com.bluage.documentation.service.AbstractService;
import com.bluage.documentation.service.standardfeatures.entities.uc69_cachehibernate.daofinder.ServiceTeamDAOMethodFinderImpl;
import com.netfective.bluage.core.exception.ApplicationException;

/**
 * Defines  application services for ServiceTeam
  */
@Named("com.bluage.documentation.service.standardfeatures.entities.uc69_cachehibernate.ServiceTeam")
public class ServiceTeam  extends AbstractService{


			@Inject
		@Named ("com.bluage.documentation.service.standardfeatures.entities.uc69_cachehibernate.daofinder.ServiceTeamDAOMethodFinder")
		private ServiceTeamDAOMethodFinderImpl serviceTeamDao;
		@Inject
		@Named ("com.bluage.documentation.business.standardfeatures.entities.uc69_cachehibernate.entities.daofinder.Team69DAOFinderImpl")
		private Team69DAOFinderImpl team69BOFinder;
	


	/**
	 *  Default constructor 
	 */
	public ServiceTeam() {
		super(); // Call super for default constructor : ServiceTeam
	}
	



				
	/**    
	 * Operation HQL.
	 * @return {@link List}
	 */
	public List searchTeams( )
 throws ApplicationException {
	
	return serviceTeamDao.searchTeams();
	}

					
				
	/**    
	 * Operation Crud
	 * @param bo {@link Team69BO}
		 * @return {@link Team69BO}
	 */
	public Team69BO team69FindByID(Team69BO bo )
 throws ApplicationException {

					return (Team69BO)team69BOFinder.findByPrimaryKey(getBoId(bo));
	}
	/**
	 *  Return the primary key from business object.
	 */
	private Object getBoId(Team69BO bo) {
				return bo.getId();
		}

		}
