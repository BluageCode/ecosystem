<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file="/include.jsp"  %>
<html >
<head >
<title  > 
<fmt:message key="cUC43_AddPlayer_BLU_AGE_DOCUMENTATION"/>
</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"  > 
</meta>
<link href="<c:url value= "/presentation/_style/_style.css"/>" rel="stylesheet" type="text/css"  /> 
<link href="<c:url value= "/presentation/_include/jquery/rd_lib.css"/>" rel="stylesheet" type="text/css"  /> 
<script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/jquery.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/rd_setup.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/rd_lib.js"/>" space="preserve"  ></script><link rel="shortcut icon" href="<c:url value= "/presentation/_style/favicon.ico"/>"  /> 

</head>

<body class=" " > 
<div id="rd_ghost"  > 
<small  > 
<fmt:message key="cUC43_AddPlayer___CSS_DEACTIVATED"/>
</small>
</div>
<div id="rd_jghost"  > 
<small  > 
<fmt:message key="cUC43_AddPlayer___JAVASCRIPT_DEACTIVATED"/>
</small>
</div>
<div id="shape"  > 
<div class="ui-layout-center"  > 
<div class="east-north"  > 
	<form:form id="uC43_AddPlayer" commandName="uC43_AddPlayerForm"  enctype="application/x-www-form-urlencoded" method="post">
<h2 class="rd_first"  > 
<fmt:message key="cUC43_AddPlayer_STANDARD_FEATURES__APPLICATIO"/>
</h2>
<h4 class="rd_first"  > 
<fmt:message key="cUC43_AddPlayer_ADD_PLAYER"/>
</h4>
<table class="rd_tableform"  >
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC43_AddPlayer_FIRST_NAME_"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
<form:input id="txt_firstname" path="playerToAdd.firstName"  size="30" 
		/>
</td> 
<td rowspan="1" colspan="1"  >
<form:errors path="playerToAdd.firstName"/>
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC43_AddPlayer_LAST_NAME"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
<form:input id="txt_lastname" path="playerToAdd.lastName"  size="30" 
		/>
</td> 
<td rowspan="1" colspan="1"  >
<form:errors path="playerToAdd.lastName"/>
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC43_AddPlayer_DATE_OF_BIRTH_MMDDYYYY"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
<form:input id="txt_dateofbirth" path="playerToAdd.dateOfBirth"  size="12" 
		/>
</td> 
<td rowspan="1" colspan="1"  >
<form:errors path="playerToAdd.dateOfBirth"/>
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC43_AddPlayer_POSITION"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
<form:select id="slt_position" path="playerToAdd.position.code"   
		>
		<form:options items="${uC43_AddPlayerForm.allPositions}" itemValue="code" itemLabel="name" />
</form:select>
</td> 
<td rowspan="1" colspan="1"  >
<fmt:message key="cUC43_AddPlayer_"/>
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC43_AddPlayer_ROOKIE"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
<form:checkbox id="chk_rookie" path="playerToAdd.rookie"   
		/>
</td> 
<td rowspan="1" colspan="1"  >
<fmt:message key="cUC43_AddPlayer__103"/>
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<fmt:message key="cUC43_AddPlayer__104"/>
</td> 
<td rowspan="1" colspan="1"  >

	<c:out value="<%=com.octo.captcha.CaptchaQuestionHelper.getQuestion(java.util.Locale.getDefault(), \"com.octo.captcha.image.gimpy.Gimpy\")%>"/>:<br/>
	<img id="cap_create"   src="<c:url value="/presentation/standardfeatures/application/form/uc43_captcha/UC43_AddPlayer/captcha.html"/>"/>
	<a href="#" title="Refresh button" onclick='var now = new Date().getTime();document.getElementById("cap_create").src="<%=request.getContextPath()%>/presentation/standardfeatures/application/form/uc43_captcha/UC43_AddPlayer/captcha.html#"+now; return false;'>
			<c:out value="[refresh image]" />
	</a><br/>
	<form:input id="txt_captcha" path="captcha" /><br />
<form:errors path="captcha"/>
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<fmt:message key="cUC43_AddPlayer__105"/>
</td> 
<td rowspan="1" colspan="1"  >
<a id="lnk_add"onclick="javascript:btn_click_cExecuteUC43_AddPlayer(this.href);return false;"				href="<c:url value="/presentation/standardfeatures/application/form/uc43_captcha/UC43_AddPlayer/lnk_add.html"/>"
							 shape="rect"  >									 
<fmt:message key="cUC43_AddPlayer_CREATE_PLAYER"/>
</a>
</td> 
<td rowspan="1" colspan="1"  >
<fmt:message key="cUC43_AddPlayer__106"/>
</td> 
</tr>
</table> 
<display:table uid="tab_players"  sort="list" requestURI="" name="sessionScope.uC43_AddPlayerForm.listPlayers" class="rd_tabledata"  >
	<c:set   var="rownum">
		<%=(tab_players !=	null) ? new Integer(tab_players_rowNum.intValue() - 1) : new Integer(0) %>
	</c:set>
	<c:set   var="idRow">
		<%=(tab_players != null ) ? tab_players_rowNum.intValue() - 1 : -1 %>
	</c:set>
	<c:set   var="idTab">
		<%=(tab_players != null ) ? "tab_players" : null %>
	</c:set>
	<c:set var="tableHashId">
		<%= new org.displaytag.util.ParamEncoder("tab_players").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE) %>
	</c:set>
	<c:set var="tableHashId2">
		<%=request.getParameter(new org.displaytag.util.ParamEncoder("tab_players").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE)) %>
	</c:set>	
	
<display:column format=""  titleKey="cUC43_AddPlayer_NAME">
		<span id="dyn_lastname">
	<c:out value="${tab_players.lastName}" />
		</span>
	<fmt:message key="cUC43_AddPlayer______________"/>
		<span id="dyn_firstname">
	<c:out value="${tab_players.firstName}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="cUC43_AddPlayer_DATE_OF_BIRTH">
<span id="dyn_dateofbirth">
	<format:formatDateValidator value="${tab_players.dateOfBirth}" pattern="MM/dd/yyyy" />
</span>
	</display:column>
	
<display:column format=""  titleKey="cUC43_AddPlayer_POSITION_107">
		<span id="dyn_position">
	<c:out value="${tab_players.position.code}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="cUC43_AddPlayer_ROOKIE_108">
<form:checkbox id="chk_rookie[${rownum}]" path="listPlayers[${rownum}].rookie"  name="isRookie" disabled="true"  
		/>
</display:column>
	
<display:column format=""  titleKey="cUC43_AddPlayer_DELETE">
<a id="lnk_delete[${rownum}]"onclick="javascript:btn_click_cExecuteUC43_AddPlayer(this.href);return false;"			href="<c:url value="/presentation/standardfeatures/application/form/uc43_captcha/UC43_AddPlayer/lnk_delete.html?id="/><c:out value="${idRow}" />&tab=<c:out value="tab_players" />&<c:out value="${tableHashId}"/>=<c:out value="${tableHashId2}"/>"
	 shape="rect"  >									 
<fmt:message key="cUC43_AddPlayer_DELETE_109"/>
</a>
</display:column>
</display:table>
<br clear="none"  /> 
	</form:form>
	<script language="javascript">
		function btn_click_cExecuteUC43_AddPlayer(url) {
			UC43_AddPlayervar = document.getElementById('uC43_AddPlayer');
			UC43_AddPlayervar.setAttribute('action', url);
			UC43_AddPlayervar.submit();
		}
		function btn_click_cSetActionUC43_AddPlayer(url) {
			UC43_AddPlayervar = document.getElementById('uC43_AddPlayer');
			UC43_AddPlayervar.setAttribute('action', url);
		}
	</script>
</div>
<div class="east-center"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/application/form/uc43_captcha/UC43_AddPlayercmp_tabs" />
</div>
</div>
<div class="ui-layout-north"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/application/form/uc43_captcha/UC43_AddPlayercmp_header" />
</div>
<div class="ui-layout-south"  > 
<fmt:message key="cUC43_AddPlayer________"/>
<span id="rd_jversion"  > 
<fmt:message key="cUC43_AddPlayer_BLU_AGE_DOCUMENTATION_110"/>
</span>
</div>
<div class="ui-layout-west"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/application/form/uc43_captcha/UC43_AddPlayercmp_treeview" />
</div>
</div>
</body>
</html>
