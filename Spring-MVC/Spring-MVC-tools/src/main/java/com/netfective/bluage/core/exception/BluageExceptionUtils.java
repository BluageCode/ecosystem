/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.netfective.bluage.core.exception;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * Class BluageExceptionUtils
 * @author NETFECTIVE TECHNOLOGY
 */
public final class BluageExceptionUtils {

	/**
	 * default constructor for BluageExceptionUtils
	 */
	private BluageExceptionUtils(){}  
	
    /**
	 * Prints the stack trace of the exception into string
	 * @param e
	 * @return
	 */
    public static String getStackTrace(final Exception e)
    {
        final StringWriter sw = new StringWriter();
        final PrintWriter pw = new PrintWriter(sw, true);
        e.printStackTrace(pw);
        pw.flush();
        sw.flush();
        return sw.toString();
    }
}

