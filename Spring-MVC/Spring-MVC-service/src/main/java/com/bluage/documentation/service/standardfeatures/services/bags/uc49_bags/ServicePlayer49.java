/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.service.standardfeatures.services.bags.uc49_bags;


import javax.inject.Named;

import com.bluage.documentation.business.standardfeatures.services.bags.uc49_bags.bos.Player49BO;
import com.bluage.documentation.business.standardfeatures.services.bags.uc49_bags.bos.Team49BO;
import com.bluage.documentation.service.AbstractService;
import com.netfective.bluage.core.exception.ApplicationException;

/**
 * Defines  application services for ServicePlayer49
  */
@Named("com.bluage.documentation.service.standardfeatures.services.bags.uc49_bags.ServicePlayer49")
public class ServicePlayer49  extends AbstractService{


				


	/**
	 *  Default constructor 
	 */
	public ServicePlayer49() {
		super(); // Call super for default constructor : ServicePlayer49
	}
	



			
	/**    
	 * Process operation.
	 * @param selectedPlayer {@link Player49BO}
		 * @return {@link Team49BO}
	 */
	public Team49BO returnTeam49(Player49BO selectedPlayer )
 throws ApplicationException {

				return privateReturnTeam49(selectedPlayer);
	}
	

	/**    
	 * Process operation.
	 * @param selectedPlayer {@link Player49BO}
		 * @return {@link Team49BO}
	 */
	private Team49BO privateReturnTeam49(Player49BO selectedPlayer )
 throws ApplicationException {
						// Return Type declaration.
				Team49BO teamToDisplay = new Team49BO();
					// Central buffer node declaration for operation returnTeam49
			/* Executing :  language: bags*/
teamToDisplay = selectedPlayer.getTeam();

						return teamToDisplay;
		}

		}
