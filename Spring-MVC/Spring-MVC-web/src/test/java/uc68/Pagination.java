package uc68;


import org.junit.Test;
import test.common.AbstractBADocTestCase;

public class Pagination extends AbstractBADocTestCase {
	
	@Override
	public  String getName(){
		return "UC-68";
	}


	@Test
	public void pagination() throws Exception {
		// On teste l'UC68 - Standard Features/Sentites - Inheritance subclass
		selenium.open("");	
		selenium.click("link=Expand All");
		selenium.waitForPageToLoad("30000");
		selenium.click("link=UC68 - Inheritance subclass");
		verifyTrue(selenium.isElementPresent("//h2[contains(text(),\"Standard Features > Entities > UC68 > Inheritance subclass\")]"));
		// On teste la pagination.
		// On teste la pagination pour le tableau Players.
		// On va sur la page 2.
		selenium.click("xpath=//a[5]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie qu'on est sur la page 2.
		assertTrue(selenium.isElementPresent("xpath=(//span/strong)[1][contains(text(),\"2\")]"));
		// On va tester le lien fast next.
//		selenium.click("//a[10]");
//		selenium.waitForPageToLoad("30000");
//		// Les sauts de pages sont de 6 pages.
//		assertTrue(selenium.isElementPresent("(//span/strong)[1][contains(text(),\"5\")]"));
//		// On teste le lien fast prev.
//		selenium.click("//a[2]");
//		selenium.waitForPageToLoad("30000");
//		// Les sauts de pages sont de 3 pages. 
//		assertTrue(selenium.isElementPresent("(//span/strong)[1][contains(text(),\"1\")]"));
		// On teste le lien Prev.
		selenium.click("xpath=//a[2]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie que l'on est sur la premi�re page.
		assertTrue(selenium.isElementPresent("xpath=(//span/strong)[1][contains(text(),\"1\")]"));
		// On teste le lien Last.
		selenium.click("xpath=//a[6]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("xpath=(//span/strong)[1][contains(text(),\"5\")]"));
		// On teste le lien First
		selenium.click("xpath=//a[1]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("xpath=(//span/strong)[1][contains(text(),\"1\")]"));
		// On teste la pagination pour le tableau Rookie.
		// On va sur la page 2.
		selenium.click("xpath=(//a)[9]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie qu'on est sur la page 2.
		assertTrue(selenium.isElementPresent("xpath=(//span/strong)[2][contains(text(),\"2\")]"));
		// On va tester le lien fast next.
//		selenium.click("xpath=(//a)[19]");
//		selenium.waitForPageToLoad("30000");
//		// Les sauts de pages sont de 6 pages.
//		assertTrue(selenium.isElementPresent("xpath=(//span/strong)[2][contains(text(),\"3\")]"));
//		// On teste le lien fast prev.
//		selenium.click("xpath=(//a)[13]");
//		selenium.waitForPageToLoad("30000");
//		// Les sauts de pages sont de 3 pages. 
//		assertTrue(selenium.isElementPresent("xpath=(//span/strong)[2][contains(text(),\"1\")]"));
		// On teste le lien Prev.
		selenium.click("xpath=(//a)[8]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie que l'on est sur la premi�re page.
		assertTrue(selenium.isElementPresent("xpath=(//span/strong)[2][contains(text(),\"1\")]"));
		// On teste le lien Last.
		selenium.click("xpath=(//a)[10]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("xpath=(//span/strong)[2][contains(text(),\"3\")]"));
		// On teste le lien First
		selenium.click("xpath=(//a)[7]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("xpath=(//span/strong)[3][contains(text(),\"1\")]"));
		// On teste la pagination pour le tableau Professionals.
		// On va sur la page 2.
		selenium.click("xpath=(//a)[13]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie qu'on est sur la page 2.
		assertTrue(selenium.isElementPresent("xpath=(//span/strong)[3][contains(text(),\"2\")]"));
		// On va tester le lien fast next.
//		selenium.click("xpath=(//a)[28]");
//		selenium.waitForPageToLoad("30000");
//		// Les sauts de pages sont de 6 pages.
//		assertTrue(selenium.isElementPresent("xpath=(//span/strong)[3][contains(text(),\"3\")]"));
//		// On teste le lien fast prev.
//		selenium.click("xpath=(//a)[22]");
//		selenium.waitForPageToLoad("30000");
//		// Les sauts de pages sont de 3 pages. 
//		assertTrue(selenium.isElementPresent("xpath=(//span/strong)[3][contains(text(),\"1\")]"));
		// On teste le lien Prev.
		selenium.click("xpath=(//a)[12]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie que l'on est sur la premi�re page.
		assertTrue(selenium.isElementPresent("xpath=(//span/strong)[3][contains(text(),\"1\")]"));
		// On teste le lien Last.
		selenium.click("xpath=(//a)[14]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("xpath=(//span/strong)[3][contains(text(),\"3\")]"));
		// On teste le lien First
		selenium.click("xpath=(//a)[11]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("xpath=(//span/strong)[3][contains(text(),\"1\")]"));
	}


}
