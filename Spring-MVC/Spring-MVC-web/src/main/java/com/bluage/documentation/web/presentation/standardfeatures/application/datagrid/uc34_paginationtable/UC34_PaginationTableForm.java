/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.application.datagrid.uc34_paginationtable;


import java.io.Serializable;
import java.util.List;

import com.bluage.documentation.business.standardfeatures.application.datagrid.uc34_paginationtable.bos.Team34BO;
import com.netfective.bluage.core.web.AbstractForm;
import com.netfective.bluage.core.web.IForm;

/**
* Class : UC34_PaginationTableForm
*/
public class UC34_PaginationTableForm extends AbstractForm implements Serializable, IForm {
	
	/** Constant : serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	//CONSTANT : allTeams
	private static final String ALL_TEAMS = "allTeams";
	//CONSTANT : selectedTeam
	private static final String SELECTED_TEAM = "selectedTeam";
	//CONSTANT : teamDetails
	private static final String TEAM_DETAILS = "teamDetails";
	/**
	 * 	Property: allTeams 
	 */
	private List<Team34BO> allTeams;
	/**
	 * 	Property: selectedTeam 
	 */
	private Team34BO selectedTeam;
	/**
	 * 	Property: teamDetails 
	 */
	private Team34BO teamDetails;
/**
	 * Default constructor : UC34_PaginationTableForm
	 */
	public UC34_PaginationTableForm() {
		super();
		// Initialize : allTeams
		this.allTeams = new java.util.ArrayList<com.bluage.documentation.business.standardfeatures.application.datagrid.uc34_paginationtable.bos.Team34BO>();
		// Initialize : selectedTeam
		this.selectedTeam = null;
		// Initialize : teamDetails
		this.teamDetails = null;
					this.setAlwaysCallPreControllers(true);
	}
	
	/**
	 * 	Getter : allTeams 
	 * 	@return : Return the allTeams instance.
	 */
	public List<Team34BO> getAllTeams(){
		return allTeams; // For UC34_PaginationTableForm
	}
	
	/**
	 * 	Setter : allTeams 
	 *  @param allTeamsinstance : The instance to set.
	 */
	public void setAllTeams(final List<Team34BO> allTeamsinstance){
		this.allTeams = allTeamsinstance;// For UC34_PaginationTableForm
	}
	/**
	 * 	Getter : selectedTeam 
	 * 	@return : Return the selectedTeam instance.
	 */
	public Team34BO getSelectedTeam(){
		return selectedTeam; // For UC34_PaginationTableForm
	}
	
	/**
	 * 	Setter : selectedTeam 
	 *  @param selectedTeaminstance : The instance to set.
	 */
	public void setSelectedTeam(final Team34BO selectedTeaminstance){
		this.selectedTeam = selectedTeaminstance;// For UC34_PaginationTableForm
	}
	/**
	 * 	Getter : teamDetails 
	 * 	@return : Return the teamDetails instance.
	 */
	public Team34BO getTeamDetails(){
		return teamDetails; // For UC34_PaginationTableForm
	}
	
	/**
	 * 	Setter : teamDetails 
	 *  @param teamDetailsinstance : The instance to set.
	 */
	public void setTeamDetails(final Team34BO teamDetailsinstance){
		this.teamDetails = teamDetailsinstance;// For UC34_PaginationTableForm
	}
/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UC34_PaginationTableForm [ "+
ALL_TEAMS +" = " + allTeams +SELECTED_TEAM +" = " + selectedTeam +TEAM_DETAILS +" = " + teamDetails + "]";
	}
	
	/**
	 * Method setInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public void setInstanceByInstanceName(final String instanceName, final Object instance) {
		// Set the instance AllTeams.
		if(ALL_TEAMS.equals(instanceName)){// For UC34_PaginationTableForm
			this.setAllTeams((List<Team34BO>)instance); // For UC34_PaginationTableForm
		}
				// Set the instance SelectedTeam.
		if(SELECTED_TEAM.equals(instanceName)){// For UC34_PaginationTableForm
			this.setSelectedTeam((Team34BO)instance); // For UC34_PaginationTableForm
		}
				// Set the instance TeamDetails.
		if(TEAM_DETAILS.equals(instanceName)){// For UC34_PaginationTableForm
			this.setTeamDetails((Team34BO)instance); // For UC34_PaginationTableForm
		}
			}
	
	/**
	 * Method getInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public Object getInstanceByInstanceName(final String instanceName){
		// Temporary object for tmpUC34_PaginationTableForm.
		Object tmpUC34_PaginationTableForm = null;
		
			
		// Get the instance AllTeams for UC34_PaginationTableForm.
		if(ALL_TEAMS.equals(instanceName)){ // For UC34_PaginationTableForm
			tmpUC34_PaginationTableForm = this.getAllTeams(); // For UC34_PaginationTableForm
		}
			
		// Get the instance SelectedTeam for UC34_PaginationTableForm.
		if(SELECTED_TEAM.equals(instanceName)){ // For UC34_PaginationTableForm
			tmpUC34_PaginationTableForm = this.getSelectedTeam(); // For UC34_PaginationTableForm
		}
			
		// Get the instance TeamDetails for UC34_PaginationTableForm.
		if(TEAM_DETAILS.equals(instanceName)){ // For UC34_PaginationTableForm
			tmpUC34_PaginationTableForm = this.getTeamDetails(); // For UC34_PaginationTableForm
		}
		return tmpUC34_PaginationTableForm;// For UC34_PaginationTableForm
	}
	
			}
