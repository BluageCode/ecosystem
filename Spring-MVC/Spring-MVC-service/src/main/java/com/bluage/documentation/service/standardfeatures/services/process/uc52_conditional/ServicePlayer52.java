/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.service.standardfeatures.services.process.uc52_conditional;


import javax.inject.Inject;
import javax.inject.Named;

import com.bluage.documentation.business.standardfeatures.services.process.uc52_conditional.bos.Player52BO;
import com.bluage.documentation.service.AbstractService;
import com.netfective.bluage.core.exception.ApplicationException;

/**
 * Defines  application services for ServicePlayer52
  */
@Named("com.bluage.documentation.service.standardfeatures.services.process.uc52_conditional.ServicePlayer52")
public class ServicePlayer52  extends AbstractService{


					@Inject
		@Named ("com.bluage.documentation.service.standardfeatures.services.process.uc52_conditional.ServicePlayer52Create")
		private ServicePlayer52Create  instanceServicePlayer52Create;
						


	/**
	 *  Default constructor 
	 */
	public ServicePlayer52() {
		super(); // Call super for default constructor : ServicePlayer52
	}
	



			
	/**    
	 * Process operation.
	 * @param newPlayer {@link Player52BO}
		 * @return {@link void}
	 */
	public void addNewPlayer(Player52BO newPlayer )
 throws ApplicationException {

			privateAddNewPlayer(newPlayer);
	}
	

	/**    
	 * Process operation.
	 * @param newPlayer {@link Player52BO}
		 * @return {@link void}
	 */
	private void privateAddNewPlayer(Player52BO newPlayer )
 throws ApplicationException {
		// Central buffer node declaration for operation addNewPlayer
			Float	 value =  Float.valueOf(0);
					boolean	 result =  false;
					Boolean	 rookie =  Boolean.FALSE;
						// Calling an operation : 
rookie = newPlayer.getRookie();
if (rookie.booleanValue()) {
// Calling an operation : 
value = newPlayer.getValue();
/* Executing : A rookie player has a diminished value*/
value = value.floatValue()* new Float(0.9);
// Calling an operation : 
newPlayer.setValue(value);
} 
// Calling an operation : 
result = instanceServicePlayer52Create.player52Create(newPlayer);

		}

		}
