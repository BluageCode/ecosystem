/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.application.navigation.uc28_packagescope.package_one;


import java.io.Serializable;
import java.util.List;

import com.bluage.documentation.business.standardfeatures.application.navigation.uc28_packagescope.bos.Player28BO;
import com.netfective.bluage.core.web.AbstractForm;
import com.netfective.bluage.core.web.IForm;

/**
* Class : UC28_PlayersForm
*/
public class UC28_PlayersForm extends AbstractForm implements Serializable, IForm {
	
	/** Constant : serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	//CONSTANT : allStates
	private static final String ALL_STATES = "allStates";
	//CONSTANT : allStadiums
	private static final String ALL_STADIUMS = "allStadiums";
	//CONSTANT : allPlayers
	private static final String ALL_PLAYERS = "allPlayers";
	/**
	 * 	Property: allStates 
	 */
	private List allStates;
	/**
	 * 	Property: allStadiums 
	 */
	private List allStadiums;
	/**
	 * 	Property: allPlayers 
	 */
	private List<Player28BO> allPlayers;
/**
	 * Default constructor : UC28_PlayersForm
	 */
	public UC28_PlayersForm() {
		super();
		// Initialize : allStates
		this.allStates = null;
		// Initialize : allStadiums
		this.allStadiums = null;
		// Initialize : allPlayers
		this.allPlayers = new java.util.ArrayList<com.bluage.documentation.business.standardfeatures.application.navigation.uc28_packagescope.bos.Player28BO>();
					this.setAlwaysCallPreControllers(true);
	}
	
	/**
	 * 	Getter : allStates 
	 * 	@return : Return the allStates instance.
	 */
	public List getAllStates(){
		return allStates; // For UC28_PlayersForm
	}
	
	/**
	 * 	Setter : allStates 
	 *  @param allStatesinstance : The instance to set.
	 */
	public void setAllStates(final List allStatesinstance){
		this.allStates = allStatesinstance;// For UC28_PlayersForm
	}
	/**
	 * 	Getter : allStadiums 
	 * 	@return : Return the allStadiums instance.
	 */
	public List getAllStadiums(){
		return allStadiums; // For UC28_PlayersForm
	}
	
	/**
	 * 	Setter : allStadiums 
	 *  @param allStadiumsinstance : The instance to set.
	 */
	public void setAllStadiums(final List allStadiumsinstance){
		this.allStadiums = allStadiumsinstance;// For UC28_PlayersForm
	}
	/**
	 * 	Getter : allPlayers 
	 * 	@return : Return the allPlayers instance.
	 */
	public List<Player28BO> getAllPlayers(){
		return allPlayers; // For UC28_PlayersForm
	}
	
	/**
	 * 	Setter : allPlayers 
	 *  @param allPlayersinstance : The instance to set.
	 */
	public void setAllPlayers(final List<Player28BO> allPlayersinstance){
		this.allPlayers = allPlayersinstance;// For UC28_PlayersForm
	}
/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UC28_PlayersForm [ "+
ALL_STATES +" = " + allStates +ALL_STADIUMS +" = " + allStadiums +ALL_PLAYERS +" = " + allPlayers + "]";
	}
	
	/**
	 * Method setInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public void setInstanceByInstanceName(final String instanceName, final Object instance) {
		// Set the instance AllStates.
		if(ALL_STATES.equals(instanceName)){// For UC28_PlayersForm
			this.setAllStates((List)instance); // For UC28_PlayersForm
		}
				// Set the instance AllStadiums.
		if(ALL_STADIUMS.equals(instanceName)){// For UC28_PlayersForm
			this.setAllStadiums((List)instance); // For UC28_PlayersForm
		}
				// Set the instance AllPlayers.
		if(ALL_PLAYERS.equals(instanceName)){// For UC28_PlayersForm
			this.setAllPlayers((List<Player28BO>)instance); // For UC28_PlayersForm
		}
			}
	
	/**
	 * Method getInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public Object getInstanceByInstanceName(final String instanceName){
		// Temporary object for tmpUC28_PlayersForm.
		Object tmpUC28_PlayersForm = null;
		
			
		// Get the instance AllStates for UC28_PlayersForm.
		if(ALL_STATES.equals(instanceName)){ // For UC28_PlayersForm
			tmpUC28_PlayersForm = this.getAllStates(); // For UC28_PlayersForm
		}
			
		// Get the instance AllStadiums for UC28_PlayersForm.
		if(ALL_STADIUMS.equals(instanceName)){ // For UC28_PlayersForm
			tmpUC28_PlayersForm = this.getAllStadiums(); // For UC28_PlayersForm
		}
			
		// Get the instance AllPlayers for UC28_PlayersForm.
		if(ALL_PLAYERS.equals(instanceName)){ // For UC28_PlayersForm
			tmpUC28_PlayersForm = this.getAllPlayers(); // For UC28_PlayersForm
		}
		return tmpUC28_PlayersForm;// For UC28_PlayersForm
	}
	
			}
