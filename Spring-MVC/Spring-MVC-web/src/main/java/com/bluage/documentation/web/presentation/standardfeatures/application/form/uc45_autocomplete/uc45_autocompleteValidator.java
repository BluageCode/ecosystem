/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.application.form.uc45_autocomplete ;


import org.apache.log4j.Logger;
import org.springframework.validation.Errors;

import com.netfective.bluage.core.validator.AbstractValidator;
/**
* Class :uc45_autocompleteValidator
*/
public class uc45_autocompleteValidator extends AbstractValidator{
	
	// LOGGER for the class uc45_autocompleteValidator
	private static final Logger LOGGER = Logger.getLogger( uc45_autocompleteValidator.class);
	
	
	/**
	* Operation validate for uc45_autocompleteForm
	* @param obj : the current form (uc45_autocompleteForm)
	* @param errors : The spring errors to return for the form uc45_autocompleteForm
	*/
	public void validate(final Object obj,final Errors errors){
		LOGGER.info("Beginning method : validate"); // uc45_autocompleteValidator
		uc45_autocompleteForm cuc45_autocompleteForm = (uc45_autocompleteForm)obj; // uc45_autocompleteValidator
		LOGGER.info("Ending method : validate the form "+ cuc45_autocompleteForm.getClass().getName()); // uc45_autocompleteValidator
	}

	/**
	* Method to implements to use spring validators (uc45_autocompleteForm)
	* @param aClass : Class for the form uc45_autocompleteForm
	* @return boolean
	*/
	public boolean supports(final Class aClass){
		return (new uc45_autocompleteForm()).getClass().equals(aClass); // uc45_autocompleteValidator
	}
}
