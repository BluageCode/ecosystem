/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.advancedfeatures.externaltools.uc210_sonar ;

// Import declaration.
// Java imports.
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;

/**
 * Class : UC210_HomePageController
 */
@Controller
@Scope("prototype")
@SessionAttributes("uC210_HomePageForm")
@RequestMapping(value= "/presentation/advancedfeatures/externaltools/uc210_sonar")
public class UC210_HomePageController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC210_HomePageController.class);
	
	//Current form name
	private static final String U_C210__HOME_PAGE_FORM = "uC210_HomePageForm";

	
	/**
	 * Property:customDateEditorsUC210_HomePageController
	 */
	final private Map<String,CustomDateEditor> customDateEditorsUC210_HomePageController = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:uC210_HomePageValidator
	 */
	final private UC210_HomePageValidator uC210_HomePageValidator = new UC210_HomePageValidator();
	
	
	// Initialise all the instances for UC210_HomePageController

	/**
	* This method initialise the form : UC210_HomePageForm 
	* @return UC210_HomePageForm
	*/
	@ModelAttribute("UC210_HomePageFormInit")
	public void initUC210_HomePageForm(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, U_C210__HOME_PAGE_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method UC210_HomePageForm."); //for lnk_clear 
		}
		UC210_HomePageForm uC210_HomePageForm;
	
		if(request.getSession().getAttribute(U_C210__HOME_PAGE_FORM) != null){
			uC210_HomePageForm = (UC210_HomePageForm)request.getSession().getAttribute(U_C210__HOME_PAGE_FORM);
		} else {
			uC210_HomePageForm = new UC210_HomePageForm();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form UC210_HomePageForm.");
		}
		uC210_HomePageForm = (UC210_HomePageForm)populateDestinationForm(request.getSession(), uC210_HomePageForm);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  UC210_HomePageForm.");
		}
		model.addAttribute(U_C210__HOME_PAGE_FORM, uC210_HomePageForm);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param UC210_HomePageForm : The sceen form.
	 */
	@RequestMapping(value = "/UC210_HomePage.html" ,method = RequestMethod.GET)
	public void prepareUC210_HomePage(final HttpServletRequest request,final  Model model,final  @ModelAttribute("UC210_HomePageFormInit") UC210_HomePageForm uC210_HomePageForm){
		
		UC210_HomePageForm currentUC210_HomePageForm = uC210_HomePageForm;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(U_C210__HOME_PAGE_FORM) == null){
			if(currentUC210_HomePageForm!=null){
				request.getSession().setAttribute(U_C210__HOME_PAGE_FORM, currentUC210_HomePageForm);
			}else {
				currentUC210_HomePageForm = new UC210_HomePageForm();
				request.getSession().setAttribute(U_C210__HOME_PAGE_FORM, currentUC210_HomePageForm);	
			}
		} else {
			currentUC210_HomePageForm = (UC210_HomePageForm) request.getSession().getAttribute(U_C210__HOME_PAGE_FORM);
		}

		currentUC210_HomePageForm = (UC210_HomePageForm)populateDestinationForm(request.getSession(), currentUC210_HomePageForm);
		// Call all the Precontroller.
	currentUC210_HomePageForm.setAlwaysCallPreControllers(true);
		model.addAttribute(U_C210__HOME_PAGE_FORM, currentUC210_HomePageForm);
		request.getSession().setAttribute(U_C210__HOME_PAGE_FORM, currentUC210_HomePageForm);
		
	}
	
	
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("UC210_HomePageController [ ");
		// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(uC210_HomePageValidator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsUC210_HomePageController!=null); 
		return strBToS.toString();
	}
}
