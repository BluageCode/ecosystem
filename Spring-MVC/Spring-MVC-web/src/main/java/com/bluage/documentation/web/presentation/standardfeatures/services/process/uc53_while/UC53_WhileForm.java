/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.services.process.uc53_while;


import java.io.Serializable;

import com.bluage.documentation.business.standardfeatures.services.process.uc53_while.bos.Player53BO;
import com.netfective.bluage.core.web.AbstractForm;
import com.netfective.bluage.core.web.IForm;

/**
* Class : UC53_WhileForm
*/
public class UC53_WhileForm extends AbstractForm implements Serializable, IForm {
	
	/** Constant : serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	//CONSTANT : player
	private static final String PLAYER = "player";
	/**
	 * 	Property: player 
	 */
	private Player53BO player;
/**
	 * Default constructor : UC53_WhileForm
	 */
	public UC53_WhileForm() {
		super();
		// Initialize : player
		this.player = new Player53BO();
			this.setAlwaysCallPreControllers(true);
	}
	
	/**
	 * 	Getter : player 
	 * 	@return : Return the player instance.
	 */
	public Player53BO getPlayer(){
		return player; // For UC53_WhileForm
	}
	
	/**
	 * 	Setter : player 
	 *  @param playerinstance : The instance to set.
	 */
	public void setPlayer(final Player53BO playerinstance){
		this.player = playerinstance;// For UC53_WhileForm
	}
/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UC53_WhileForm [ "+
PLAYER +" = " + player + "]";
	}
	
	/**
	 * Method setInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public void setInstanceByInstanceName(final String instanceName, final Object instance) {
		// Set the instance Player.
		if(PLAYER.equals(instanceName)){// For UC53_WhileForm
			this.setPlayer((Player53BO)instance); // For UC53_WhileForm
		}
			}
	
	/**
	 * Method getInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public Object getInstanceByInstanceName(final String instanceName){
		// Temporary object for tmpUC53_WhileForm.
		Object tmpUC53_WhileForm = null;
		
			
		// Get the instance Player for UC53_WhileForm.
		if(PLAYER.equals(instanceName)){ // For UC53_WhileForm
			tmpUC53_WhileForm = this.getPlayer(); // For UC53_WhileForm
		}
		return tmpUC53_WhileForm;// For UC53_WhileForm
	}
	
	}
