/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.netfective.bluage.tag.format.validator;


import java.io.IOException;
import java.text.SimpleDateFormat;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
/**
 * 
 * Format the component passed as parameter with the corresponding pattern.
 *
 */
public class FormatDateValidator extends TagSupport {
	
    /**
     * LOGGER.
     */
    private static final  Logger LOGGER = Logger.getLogger(FormatDateValidator.class);
	
	private static final String DEFAULT_PATTERN = "dd/MM/yyyy"; //$NON-NLS-1$
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1250000261924529145L;
	
	private String pattern;

	private Object value;
	

	@Override
	public int doEndTag() throws JspException {
		try {
			final JspWriter out = pageContext.getOut();
			out.print(formatDate());
		} catch (IOException ioe) {
			throw new JspException("IOException while writing data to page"
					+ ioe.getMessage(),ioe);
		}
		return SKIP_BODY;

	}
	
	/**
	 * Format Numeric
	 * @return
	 * @throws JspException 
	 */
	private char[] formatDate() throws JspException {
		if (pattern == null || "".equals(pattern)) {
			pattern = DEFAULT_PATTERN; 
		}
		final SimpleDateFormat sdf = new SimpleDateFormat(pattern);
		String currentDate = StringUtils.EMPTY;
		try{
			currentDate = sdf.format(value);
		}catch(IllegalArgumentException ex){
			if(value != null && StringUtils.isBlank(value.toString())){
				LOGGER.warn("Can not parse the date, the setted value is empty or null.");
			} else {
				LOGGER.warn("Can not parse the date.");
			}
			return StringUtils.EMPTY.toCharArray();
		}
		return currentDate.toCharArray();
	}

	@Override
	public int doStartTag() throws JspException {
		return EVAL_PAGE;
	}

	public String getPattern() {
		return pattern;
	}

	public void setPattern(final String pattern) {
		this.pattern = pattern;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(final Object value) {
		this.value = value;
	}
}

