package uc02;

import org.junit.Test;
import org.openqa.selenium.By;
import test.common.AbstractBADocTestCase;
import test.common.Tools;



public class ITuc02 extends AbstractBADocTestCase {
	
	@Override
	public  String getName(){
		return "UC-02";
	}
	
	public String tabTeams(String tab,String city, String name, String state, String code)
	{
		return "//table[contains(@id,'"+tab+"')]/tbody/tr[td/span[contains(text(),\""+city+"\")] and td/span[contains(text(),\""+name+"\")] and td/span[contains(text(),\""+state+"\")]  and td/span[contains(text(),\""+code+"\")]]";
	}
	
	public String option(String op )
	{
		return "//option[contains(text(),\""+op+"\")]";
	} 
	@Test
	public void testITuc02() throws Exception {
		// Test de l'UC02 - Getting started - Entities Creation
		selenium.open("");
		selenium.click("link=Expand All");
		selenium.waitForPageToLoad("30000");
		selenium.click("link=UC02 - Entities Creation");
		assertTrue(selenium.isElementPresent("//h2[contains(text(),\"Getting started > UC02 > Entities Creation\")]"));
		// On v�rifie les donn�es de base de l'UC.
		assertTrue(selenium.isElementPresent("xpath=//table[contains(@id,'table_teams')]/thead/tr[th[contains(text(),\"City\")] and th[contains(text(),\"Name\")] and th[contains(text(),\"State\")] and th[contains(text(),\"Delete\")]]"));
		// Pour les teams :
		assertEquals("6", selenium.getXpathCount("//table[contains(@id,'table_teams')]/tbody//tr"));
		assertTrue(selenium.isElementPresent(tabTeams("table_teams","Chicago","Bulls", "Illinois","IL")));
		assertTrue(selenium.isElementPresent(tabTeams("table_teams","Bordeaux","Girondins", "Gironde","GI")));
		assertTrue(selenium.isElementPresent(tabTeams("table_teams","Cleveland","Cavaliers", "Ohio","OH")));
		assertTrue(selenium.isElementPresent(tabTeams("table_teams","Boston","Celtics", "Massachusetts","MA")));
		assertTrue(selenium.isElementPresent(tabTeams("table_teams","Anchorage","Furious Penguins", "Alaska","AK")));
		assertTrue(selenium.isElementPresent(tabTeams("table_teams","Juneau","Wild Meese", "Alaska","AK")));
		// Pour les states :
		assertTrue(selenium.isElementPresent(option("Alabama")));
		assertTrue(selenium.isElementPresent(option("Alaska")));
		assertTrue(selenium.isElementPresent(option("Arizona")));
		assertTrue(selenium.isElementPresent(option("Arkansas")));
		assertTrue(selenium.isElementPresent(option("California")));
		assertTrue(selenium.isElementPresent(option("Colorado")));
		assertTrue(selenium.isElementPresent(option("Connecticut")));
		assertTrue(selenium.isElementPresent(option("Delaware")));
		assertTrue(selenium.isElementPresent(option("Florida")));
		assertTrue(selenium.isElementPresent(option("Gironde")));
		assertTrue(selenium.isElementPresent(option("Hawaii")));
		assertTrue(selenium.isElementPresent(option("Idaho")));
		assertTrue(selenium.isElementPresent(option("Illinois")));
		assertTrue(selenium.isElementPresent(option("Indiana")));
		assertTrue(selenium.isElementPresent(option("Iowa")));
		assertTrue(selenium.isElementPresent(option("Kansas")));
		assertTrue(selenium.isElementPresent(option("Kentucky")));
		assertTrue(selenium.isElementPresent(option("Louisiana")));
		assertTrue(selenium.isElementPresent(option("Maine")));
		assertTrue(selenium.isElementPresent(option("Maryland")));
		assertTrue(selenium.isElementPresent(option("Massachusetts")));
		assertTrue(selenium.isElementPresent(option("Michigan")));
		assertTrue(selenium.isElementPresent(option("Minnesota")));
		assertTrue(selenium.isElementPresent(option("Mississippi")));
		assertTrue(selenium.isElementPresent(option("Missouri")));
		assertTrue(selenium.isElementPresent(option("Montana")));
		assertTrue(selenium.isElementPresent(option("Nebraska")));
		assertTrue(selenium.isElementPresent(option("Nevada")));
		assertTrue(selenium.isElementPresent(option("New Hampshire")));
		assertTrue(selenium.isElementPresent(option("New Jersey")));
		assertTrue(selenium.isElementPresent(option("New Mexico")));
		assertTrue(selenium.isElementPresent(option("New York")));
		assertTrue(selenium.isElementPresent(option("North Carolina")));
		assertTrue(selenium.isElementPresent(option("North Dakota")));
		assertTrue(selenium.isElementPresent(option("Ohio")));
		assertTrue(selenium.isElementPresent(option("Oklahoma")));
		assertTrue(selenium.isElementPresent(option("Oregon")));
		assertTrue(selenium.isElementPresent(option("Pennsylvania")));
		assertTrue(selenium.isElementPresent(option("Rhode Island")));
		assertTrue(selenium.isElementPresent(option("South Carolina")));
		assertTrue(selenium.isElementPresent(option("South Dakota")));
		assertTrue(selenium.isElementPresent(option("Tennessee")));
		assertTrue(selenium.isElementPresent(option("Texas")));
		assertTrue(selenium.isElementPresent(option("Utah")));
		assertTrue(selenium.isElementPresent(option("Vermont")));
		assertTrue(selenium.isElementPresent(option("Virginia")));
		assertTrue(selenium.isElementPresent(option("Washington")));
		assertTrue(selenium.isElementPresent(option("West Virginia")));
		assertTrue(selenium.isElementPresent(option("Wisconsin")));
		assertTrue(selenium.isElementPresent(option("Wyoming")));
		// On cr�e une entit�.
		selenium.click("//*[contains(@id,'txt_city')]");
		selenium.type("//*[contains(@id,'txt_city')]", "Bordeaux");
		selenium.click("//*[contains(@id,'txt_name')]");
		selenium.type("//*[contains(@id,'txt_name')]", "UBB");
//		selenium.click("//*[contains(@id,'select_state')]");
		selenium.select("//*[contains(@id,'select_state')]", "label=Gironde");
		selenium.click("//*[contains(@id,'lnk_create')]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie que la nouvelle entit� ait �t� cr�e.
		verifyEquals("7", selenium.getXpathCount("//table[contains(@id,'table_teams')]/tbody//tr"));
		assertTrue(selenium.isElementPresent(tabTeams("table_teams","Bordeaux","UBB", "Gironde","GI")));
		// On cr�e un index.
		Number index = selenium.getElementIndex(tabTeams("table_teams","Bordeaux","UBB", "Gironde","GI"));
		System.out.println("variable index vaut : " + index);
		// On supprime l'entit� cr�e.
		selenium.click("xpath=//table[contains(@id,'table_teams')]/tbody/tr[" + index + " + 1]/td/a[contains(text(),\"[delete]\")]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie qu'elle ait bien �t� supprim�e.
		assertFalse(selenium.isElementPresent(tabTeams("table_teams","Bordeaux","UBB", "Gironde","GI")));
		// On supprime toutes les teams.
		// Pour les Bulls - Chicago.
		// On cr�e un index.
		Number index1 = selenium.getElementIndex(tabTeams("table_teams","Chicago","Bulls", "Illinois","IL"));
		System.out.println("variable index vaut : " + index1);
		// On supprime l'entit� cr�e.
		selenium.click("xpath=//table[contains(@id,'table_teams')]/tbody/tr[" + index1 + " + 1]/td/a[contains(text(),\"[delete]\")]");
		selenium.waitForPageToLoad("30000");
		// Pour les Girondins de Bordeaux :
		// On cr�e un index.
		Number index2 = selenium.getElementIndex(tabTeams("table_teams","Bordeaux","Girondins", "Gironde","GI"));
		System.out.println("variable index vaut : " + index2);
		// On supprime l'entit� cr�e.
		selenium.click("xpath=//table[contains(@id,'table_teams')]/tbody/tr[" + index2 + " + 1]/td/a[contains(text(),\"[delete]\")]");
		selenium.waitForPageToLoad("30000");
		// Pour les Cavaliers de Cleveland :
		// On cr�e un index.
		Number index3 = selenium.getElementIndex(tabTeams("table_teams","Cleveland","Cavaliers", "Ohio","OH"));
		System.out.println("variable index vaut : " + index3);
		// On supprime l'entit� cr�e.
		selenium.click("xpath=//table[contains(@id,'table_teams')]/tbody/tr[" + index3 + " + 1]/td/a[contains(text(),\"[delete]\")]");
		selenium.waitForPageToLoad("30000");
		// Pour les Celtics de Boston :
		// On cr�e un index.
		Number index4 = selenium.getElementIndex(tabTeams("table_teams","Boston","Celtics", "Massachusetts","MA"));
		System.out.println("variable index vaut : " + index4);
		// On supprime l'entit� cr�e.
		selenium.click("xpath=//table[contains(@id,'table_teams')]/tbody/tr[" + index4 + " + 1]/td/a[contains(text(),\"[delete]\")]");
		selenium.waitForPageToLoad("30000");
		// Pour les Furious Penguins d'Anchorage :
		// On cr�e un index.
		Number index5 = selenium.getElementIndex(tabTeams("table_teams","Anchorage","Furious Penguins", "Alaska","AK") );
		System.out.println("variable index vaut : " + index5);
		// On supprime l'entit� cr�e.
		selenium.click("xpath=//table[contains(@id,'table_teams')]/tbody/tr[" + index5 + " + 1]/td/a[contains(text(),\"[delete]\")]");
		selenium.waitForPageToLoad("30000");
		// Pour les Wild Meese de Juneau :
		// On cr�e un index.
		Number index6 = selenium.getElementIndex(tabTeams("table_teams","Juneau","Wild Meese", "Alaska","AK"));
		System.out.println("variable index vaut : " + index6);
		// On supprime l'entit� cr�e.
		selenium.click("xpath=//table[contains(@id,'table_teams')]/tbody/tr[" + index6 + " + 1]/td/a[contains(text(),\"[delete]\")]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie que le tableau est vide.
		assertEquals("0", selenium.getXpathCount("//table[contains(@id,'tab_searchResult')]//tbody//tr|//table[contains(@id,'table_teams_empty')]//tbody//tr"));
	}

	}
