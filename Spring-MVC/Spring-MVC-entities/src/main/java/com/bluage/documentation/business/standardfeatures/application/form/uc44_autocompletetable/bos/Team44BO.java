/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.business.standardfeatures.application.form.uc44_autocompletetable.bos;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.inject.Inject;
import javax.inject.Named;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.LazyInitializationException;

import com.bluage.documentation.business.common.bos.TeamBO;
import com.bluage.documentation.business.standardfeatures.application.form.uc44_autocompletetable.entities.daofinder.Team44DAOFinderImpl;
import com.netfective.bluage.core.exception.ApplicationException;

/**
 * Team44BO
 *
* @see com.netfective.bluage.core.business.IBusinessObject
* * @see TeamBO
 */
@XmlRootElement

public class Team44BO
 extends TeamBO {
	
	/**
	 * Default SUID for Team44BO.
	 */
	private static final long serialVersionUID = 1L;
	

	@Inject
	@Named ("com.bluage.documentation.business.standardfeatures.application.form.uc44_autocompletetable.entities.daofinder.Team44Finder") 
	private Team44DAOFinderImpl finder;

																			/**
	 * Entity property: player44s
	 *
	 */
private Set<Player44BO> player44s = new HashSet<Player44BO>();



   /**
	* public default constructor for class Team44BO
	*/
	public Team44BO(){
		//NOP Team44BO
	}


																			/**
	 * Getter for player44s of class Team44BO.
	 *
	 * @return {@link java.util.Set<com.bluage.documentation.business.standardfeatures.application.form.uc44_autocompletetable.bos.Player44BO>}
	 * @generated
	 */ 
public  Set<Player44BO> getPlayer44s() {
	return this.player44s;
			}


	/**
	 * Setter for player44s of class Team44BO.
	 * @param value {@link java.util.Set<com.bluage.documentation.business.standardfeatures.application.form.uc44_autocompletetable.bos.Player44BO>}
	 * @generated
	 */

public  void setPlayer44s(final Set<Player44BO> player44s) {
	this.player44s = player44s;
}

	/**
	 * Remove an entity from the com.bluage.documentation.business.standardfeatures.application.form.uc44_autocompletetable.bos.Player44BO collection.
	 *
	 * @param entity
	 *		: {@link Set<Player44BO>} the entity
	 * @throws ApplicationException
     */
public void removeFromPlayer44s( Player44BO entity ) throws ApplicationException {
	try {
		if( getPlayer44s() != null && getPlayer44s().contains(entity) ) {
			getPlayer44s().remove( entity );
		entity.setTeam44( null );
		}
		} catch ( LazyInitializationException e ) {
				Team44BO bo = ( Team44BO ) finder.findByPrimaryKey( getId() );
			finder.getHibernateTemplate().initialize( bo.getPlayer44s() );
			setPlayer44s( bo.getPlayer44s() );
			// evict object from session to avoid NonUniqueObjectException
			finder.getHibernateTemplate().evict( bo );
		
			if( getPlayer44s() != null && getPlayer44s().contains( entity ) ) {
				getPlayer44s().remove( entity );
				entity.setTeam44( null );
			}
				}
	}
	
	/**
	 * Add entity to the com.bluage.documentation.business.standardfeatures.application.form.uc44_autocompletetable.bos.Player44BO collection.
	 *
	 * @param entity
	 *		{@link Player44BO} the entity
	 * @throws ApplicationException
     */
public void addToPlayer44s( Player44BO entity ) throws ApplicationException
	{
		try {
			if( getPlayer44s() != null && !getPlayer44s().contains( entity ) ) {
				getPlayer44s().add( entity );
				entity.setTeam44( this );
			}
			} catch ( LazyInitializationException e ) {
			Team44BO bo = ( Team44BO ) finder.findByPrimaryKey( getId() );
		finder.getHibernateTemplate().initialize( bo.getPlayer44s() );
		setPlayer44s( bo.getPlayer44s() );
		// evict object from session to avoid NonUniqueObjectException
		finder.getHibernateTemplate().evict( bo );
		if( getPlayer44s() != null && !getPlayer44s().contains( entity ) ) {
			getPlayer44s().add( entity );
			entity.setTeam44( this );
		}
			}
	}
	
	/**
	 * Add entitys to the com.bluage.documentation.business.standardfeatures.application.form.uc44_autocompletetable.bos.Player44BO collection
	 *
	 * @param entities			
	 *		: {@link java.util.List} A list of entities
	 * @throws ApplicationException 
     */	
public void addAllToPlayer44s( Collection<Player44BO> entities ) throws ApplicationException {
	for( Player44BO entity : entities) {
		if( getPlayer44s()!=null ) {
			addToPlayer44s( entity );
		}
	}
}
	
	/**
	 * Remove entities form the com.bluage.documentation.business.standardfeatures.application.form.uc44_autocompletetable.bos.Player44BO collection
	 *
	 * @param entities				
	 *		: {@link java.util.List} a list of entities
	 * @throws ApplicationException
     */	
public void removeAllFromPlayer44s( Collection<Player44BO> entities ) throws ApplicationException {
	for( Player44BO entity : entities) {
		if( getPlayer44s()!=null && getPlayer44s().contains( entity ) )	{		
			removeFromPlayer44s( entity );
		}
	}
}
	
	/**
	 * Remove all items form the collection com.bluage.documentation.business.standardfeatures.application.form.uc44_autocompletetable.bos.Player44BO
	 *
	 * @throws ApplicationException
     */	
public void removeAllFromPlayer44s() throws ApplicationException {
	Set<Player44BO> entities = this.getPlayer44s();
	if ( entities == null ) {
		return;
	}
	Iterator<Player44BO> it = entities.iterator();
	while ( it.hasNext() ) {
    	Player44BO entity = it.next(); 
		entity.setTeam44( null ); 
	}
	entities.clear();
}

/**
	 * Returns <code>true</code> if the argument is an Team44 instance and all identifiers for this entity
     * equal the identifiers of the argument entity. The <code>equals</code> method of the parent entity
     * will also need to return <code>true</code>. Returns <code>false</code> otherwise.
     *
     * @see TeamBO#hashCode()
	 * @param object  object to compare with current instance      
     * @return boolean true if equals false if not
     */
    public boolean equals(final Object object )
    {
		if ( this == object )
        {
            return true;
        }
        if ( !( object instanceof Team44BO ) )
        {
            return false;
        }
        final Team44BO that = (Team44BO)object;
    
    	            if (        				        			this.getId() == null
        	        ) {
        	return false;
        }
		return new EqualsBuilder()
			.append(this.getId(), that.getId())
			 	.isEquals();
	}
	/**

	 * Returns a hash code based on this entity's identifiers and the hash code of the parent entity.
     *
     * @see TeamBO#hashCode()
     * @return int
     */    
    public int hashCode() {
        return new HashCodeBuilder()
             .append(getId())
				 	.appendSuper(super.hashCode())
            .toHashCode();
    }
}

