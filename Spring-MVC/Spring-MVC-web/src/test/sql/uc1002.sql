--<ScriptOptions statementTerminator=";"/>


DROP TABLE player1002;


CREATE TABLE player1002 (
		id INT8 NOT NULL,
		version INT4 NOT NULL,
		first_name VARCHAR(30) NOT NULL,
		last_name VARCHAR(30) NOT NULL,
		date_of_birth DATE,
		estimated_value FLOAT4,
		weight INT4,
		height INT4,
		salary DOUBLE PRECISION
	);
	





INSERT INTO player1002 VALUES (284, 0, 'Roger', 'Martin', '1984-10-02', 12, 100, 200, 3000.00);
INSERT INTO player1002 VALUES (298, 0, 'Roger', 'Martin', '1984-10-02', 12, 100, 200, 2000.00);
INSERT INTO player1002 VALUES (317, 0, 'aaaaaaaaa', 'e', NULL, NULL, NULL, NULL, 2800.00);
INSERT INTO player1002 VALUES (321, 0, 'Julian', 'Sabos', NULL, NULL, NULL, NULL, 3400.00);
INSERT INTO player1002 VALUES (326, 0, 'Alphonse', 'Brown', '2011-03-09', 12, NULL, NULL,2600.00);
INSERT INTO player1002 VALUES (329, 0, 'Toto', 'Tito', '2011-07-11', NULL, NULL, NULL, 3900.00);



	