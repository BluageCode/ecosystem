/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.service.standardfeatures.application.form.uc43_captcha;


import javax.inject.Inject;
import javax.inject.Named;

import com.bluage.documentation.business.standardfeatures.application.form.uc43_captcha.bos.Player43BO;
import com.bluage.documentation.business.standardfeatures.application.form.uc43_captcha.entities.dao.Player43DAOImpl;
import com.bluage.documentation.service.AbstractService;
import com.netfective.bluage.core.exception.ApplicationException;

/**
 * Defines  application services for ServicePlayer43
  */
@Named("com.bluage.documentation.service.standardfeatures.application.form.uc43_captcha.ServicePlayer43")
public class ServicePlayer43  extends AbstractService{


			@Inject
		@Named ("com.bluage.documentation.business.standardfeatures.application.form.uc43_captcha.entities.dao.Player43DAOImpl")
		private Player43DAOImpl player43BODao;
	


	/**
	 *  Default constructor 
	 */
	public ServicePlayer43() {
		super(); // Call super for default constructor : ServicePlayer43
	}
	


			
				
	/**    
	 * Operation Crud
	 * @param bo {@link Player43BO}
		 * @return {@link boolean}
	 */
	public boolean player43Create(Player43BO bo )
 throws ApplicationException {

					return player43BODao.create(bo);
	}

					
				
	/**    
	 * Operation Crud
	 * @param bo {@link Player43BO}
		 * @return {@link boolean}
	 */
	public boolean player43Delete(Player43BO bo )
 throws ApplicationException {

					return player43BODao.delete(bo);
	}

		}
