<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file="/include.jsp"  %>
<html >
<head >
<title  > 
<fmt:message key="cUC35_PaginatorBDD_BLU_AGE_DOCUMENTATION"/>
</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"  > 
</meta>
<link href="<c:url value= "/presentation/_style/_style.css"/>" rel="stylesheet" type="text/css"  /> 
<link href="<c:url value= "/presentation/_include/jquery/rd_lib.css"/>" rel="stylesheet" type="text/css"  /> 
<script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/jquery.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/rd_setup.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/rd_lib.js"/>" space="preserve"  ></script><link rel="shortcut icon" href="<c:url value= "/presentation/_style/favicon.ico"/>"  /> 

</head>

<body class=" " > 
<div id="rd_ghost"  > 
<small  > 
<fmt:message key="cUC35_PaginatorBDD___CSS_DEACTIVATED"/>
</small>
</div>
<div id="rd_jghost"  > 
<small  > 
<fmt:message key="cUC35_PaginatorBDD___JAVASCRIPT_DEACTIVATED"/>
</small>
</div>
<div id="shape"  > 
<div class="ui-layout-center"  > 
<div class="east-north"  > 
	<form:form id="uC35_PaginatorBDD" commandName="uC35_PaginatorBDDForm"  enctype="application/x-www-form-urlencoded" method="post">
<h2 class="rd_first"  > 
<fmt:message key="cUC35_PaginatorBDD_STANDARD_FEATURES__APPLICATIO"/>
</h2>
<display:table uid="id_table0" pagesize="4"  sort="external" requestURI="" name="sessionScope.uC35_PaginatorBDDForm.listPlayers" class="rd_tabledata"  size="sessionScope.uC35_PaginatorBDDForm.listPlayersSizeResult" partialList="true">
	<c:set   var="rownum">
		<%=(id_table0 !=	null) ? new Integer(id_table0_rowNum.intValue() - 1) : new Integer(0) %>
	</c:set>
	<c:set   var="idRow">
		<%=(id_table0 != null ) ? id_table0_rowNum.intValue() - 1 : -1 %>
	</c:set>
	<c:set   var="idTab">
		<%=(id_table0 != null ) ? "id_table0" : null %>
	</c:set>
	<c:set var="tableHashId">
		<%= new org.displaytag.util.ParamEncoder("id_table0").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE) %>
	</c:set>
	<c:set var="tableHashId2">
		<%=request.getParameter(new org.displaytag.util.ParamEncoder("id_table0").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE)) %>
	</c:set>	
	
<display:column format=""  titleKey="cUC35_PaginatorBDD_NAME">
		<span id="dyn_lastname">
	<c:out value="${id_table0.lastName}" />
		</span>
	<fmt:message key="cUC35_PaginatorBDD______"/>
		<span id="dyn_firstname">
	<c:out value="${id_table0.firstName}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="cUC35_PaginatorBDD_DATE_OF_BIRTH">
<span id="dyn_dateofbirth">
	<format:formatDateValidator value="${id_table0.dateOfBirth}" pattern="MM/dd/yyyy" />
</span>
	</display:column>
	
<display:column format=""  titleKey="cUC35_PaginatorBDD_WEIGHT_KG">
		<span id="dyn_weight">
	<c:out value="${id_table0.weight}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="cUC35_PaginatorBDD_HEIGHT_CM">
		<span id="dyn_height">
	<c:out value="${id_table0.height}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="cUC35_PaginatorBDD_ESTIMATED_VALUE_M">
		<span id="dyn_value">
	<c:out value="${id_table0.estimatedValue}" />
		</span>
	</display:column>
</display:table>
	</form:form>
	<script language="javascript">
		function btn_click_cExecuteUC35_PaginatorBDD(url) {
			UC35_PaginatorBDDvar = document.getElementById('uC35_PaginatorBDD');
			UC35_PaginatorBDDvar.setAttribute('action', url);
			UC35_PaginatorBDDvar.submit();
		}
		function btn_click_cSetActionUC35_PaginatorBDD(url) {
			UC35_PaginatorBDDvar = document.getElementById('uC35_PaginatorBDD');
			UC35_PaginatorBDDvar.setAttribute('action', url);
		}
	</script>
</div>
<div class="east-center"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/application/datagrid/uc35_paginatorbdd/UC35_PaginatorBDDcmp_tabs" />
</div>
</div>
<div class="ui-layout-north"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/application/datagrid/uc35_paginatorbdd/UC35_PaginatorBDDcmp_header" />
</div>
<div class="ui-layout-south"  > 
<fmt:message key="cUC35_PaginatorBDD___"/>
<span id="rd_jversion"  > 
<fmt:message key="cUC35_PaginatorBDD_BLU_AGE_CORPORATION"/>
</span>
</div>
<div class="ui-layout-west"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/application/datagrid/uc35_paginatorbdd/UC35_PaginatorBDDcmp_treeview" />
</div>
</div>
</body>
</html>
