/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.application.datagrid.uc33_editabletable ;

// Import declaration.
// Java imports.
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.bluage.documentation.business.standardfeatures.application.datagrid.uc33_editabletable.bos.Team33BO;
import com.bluage.documentation.business.standardfeatures.application.datagrid.uc33_editabletable.entities.daofinder.Team33DAOFinderImpl;
import com.bluage.documentation.service.standardfeatures.application.datagrid.uc33_editabletable.ServiceEdit;
import com.netfective.bluage.core.exception.ApplicationException;
import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;
import com.netfective.bluage.core.web.IForm;

/**
 * Class : UC33_editableTableController
 */
@Controller
@Scope("prototype")
@SessionAttributes("uC33_editableTableForm")
@RequestMapping(value= "/presentation/standardfeatures/application/datagrid/uc33_editabletable")
public class UC33_editableTableController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC33_editableTableController.class);
	
	//Current form name
	private static final String U_C33_EDITABLE_TABLE_FORM = "uC33_editableTableForm";

	//CONSTANT: allTeams table or repeater.
	private static final String ALL_TEAMS = "allTeams";
				//CONSTANT: selectedTeam
	private static final String SELECTED_TEAM = "selectedTeam";
	
	/**
	 * Property:customDateEditorsUC33_editableTableController
	 */
	final private Map<String,CustomDateEditor> customDateEditorsUC33_editableTableController = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:uC33_editableTableValidator
	 */
	final private UC33_editableTableValidator uC33_editableTableValidator = new UC33_editableTableValidator();
	
	/**
	 * Service declaration : serviceEdit.
	 */
	@Autowired
	private ServiceEdit serviceEdit;
	
	/**
	 * Generic Finder : team33DAOFinderImpl.
	 */
	@Autowired
	private Team33DAOFinderImpl team33DAOFinderImpl;
	
	
	// Initialise all the instances for UC33_editableTableController
		// Initialize the instance allTeams for the state lnk_edit
		private List allTeams; // Initialize the instance allTeams for UC33_editableTableController
						// Declare the instance selectedTeam
		private Team33BO selectedTeam;
			/**
	 * Operation : lnk_update
 	 * @param model : 
 	 * @param uC33_editableTable : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC33_editableTable/lnk_update.html",method = RequestMethod.POST)
	public String lnk_update(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC33_editableTableForm") UC33_editableTableForm  uC33_editableTableForm, final BindingResult bindingResult ,final @RequestParam(ID_REQUEST_PARAM) Integer index){
		
		
		
		LOGGER.info("Begin method : lnk_update"); 
		infoLogger(uC33_editableTableForm); 
		uC33_editableTableForm.setAlwaysCallPreControllers(false); 
		// initialization for lnk_update
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state lnk_update if the validation fail, you will be forward to the screen : "/presentation/standardfeatures/application/datagrid/uc33_editabletable/UC33_editableTable"
		if(errorsMessages(request,response,uC33_editableTableForm,bindingResult,"selectedTeam", uC33_editableTableValidator, customDateEditorsUC33_editableTableController)){ 
			return "/presentation/standardfeatures/application/datagrid/uc33_editabletable/UC33_editableTable"; 
		}
		// Updating selected row for the state lnk_update 
		uC33_editableTableForm.setSelectedRow(-1); //Set selected row for lnk_update 
		// Updating selected table for the state lnk_update 
		uC33_editableTableForm.setSelectedTab(null); //reset selected row for lnk_update 

										 callTeamupdate(request,uC33_editableTableForm , index);
						// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.application.datagrid.uc33_editabletable.UC33_editableTableForm or not from lnk_update
			// Populate the destination form with all the instances returned from lnk_update.
			final IForm uC33_editableTableForm2 = populateDestinationForm(request.getSession(), uC33_editableTableForm); 
					
			// Add the form to the model.
			model.addAttribute("uC33_editableTableForm", uC33_editableTableForm2); 
			
			request.getSession().setAttribute("uC33_editableTableForm", uC33_editableTableForm2);
			
			// "OK" CASE => destination screen path from lnk_update
			LOGGER.info("Go to the screen 'UC33_editableTable'.");
			// Redirect (PRG) from lnk_update
			destinationPath =  "redirect:/presentation/standardfeatures/application/datagrid/uc33_editabletable/UC33_editableTable.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_update 
	}
	
		/**
	 * Operation : lnk_edit
 	 * @param model : The model
 	 * @param uC33_editableTable : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC33_editableTable/lnk_edit.html")
	public String lnk_edit(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC33_editableTableForm") UC33_editableTableForm  uC33_editableTableForm, BindingResult bindingResult, final @RequestParam(ID_REQUEST_PARAM) Integer index){
		uC33_editableTableForm.setSelectedRow(index);	
		String tableId = request.getParameter(TAB_REQUEST_PARAM);
		uC33_editableTableForm.setSelectedTab(tableId);
		
		Team33BO selectedTeam = (Team33BO) uC33_editableTableForm.getAllTeams().get(index);
		uC33_editableTableForm.setSelectedTeam(selectedTeam);
		return "/presentation/standardfeatures/application/datagrid/uc33_editabletable/UC33_editableTable";
	}
	/**
	 * Operation : btn_cancel
 	 * @param model : 
 	 * @param uC33_editableTable : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC33_editableTable/btn_cancel.html")
	public String btn_cancel(final HttpServletRequest request,final HttpServletResponse response, final Model model,final @ModelAttribute("uC33_editableTableForm") UC33_editableTableForm  uC33_editableTableForm, BindingResult bindingResult){
		uC33_editableTableForm.setSelectedRow(-1);
		uC33_editableTableForm.setSelectedTab(null);
		uC33_editableTableForm.setSelectedTeam(null);
		return "redirect:/presentation/standardfeatures/application/datagrid/uc33_editabletable/UC33_editableTable.html";
	}

	/**
	* This method initialise the form : UC33_editableTableForm 
	* @return UC33_editableTableForm
	*/
	@ModelAttribute("UC33_editableTableFormInit")
	public void initUC33_editableTableForm(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, U_C33_EDITABLE_TABLE_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method UC33_editableTableForm."); //for btn_cancel 
		}
		UC33_editableTableForm uC33_editableTableForm;
	
		if(request.getSession().getAttribute(U_C33_EDITABLE_TABLE_FORM) != null){
			uC33_editableTableForm = (UC33_editableTableForm)request.getSession().getAttribute(U_C33_EDITABLE_TABLE_FORM);
		} else {
			uC33_editableTableForm = new UC33_editableTableForm();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form UC33_editableTableForm.");
		}
		uC33_editableTableForm = (UC33_editableTableForm)populateDestinationForm(request.getSession(), uC33_editableTableForm);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  UC33_editableTableForm.");
		}
		model.addAttribute(U_C33_EDITABLE_TABLE_FORM, uC33_editableTableForm);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param UC33_editableTableForm : The sceen form.
	 */
	@RequestMapping(value = "/UC33_editableTable.html" ,method = RequestMethod.GET)
	public void prepareUC33_editableTable(final HttpServletRequest request,final  Model model,final  @ModelAttribute("UC33_editableTableFormInit") UC33_editableTableForm uC33_editableTableForm){
		
		UC33_editableTableForm currentUC33_editableTableForm = uC33_editableTableForm;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(U_C33_EDITABLE_TABLE_FORM) == null){
			if(currentUC33_editableTableForm!=null){
				request.getSession().setAttribute(U_C33_EDITABLE_TABLE_FORM, currentUC33_editableTableForm);
			}else {
				currentUC33_editableTableForm = new UC33_editableTableForm();
				request.getSession().setAttribute(U_C33_EDITABLE_TABLE_FORM, currentUC33_editableTableForm);	
			}
		} else {
			currentUC33_editableTableForm = (UC33_editableTableForm) request.getSession().getAttribute(U_C33_EDITABLE_TABLE_FORM);
		}

		try {
			List allTeams = team33DAOFinderImpl.findAll();
			put(request.getSession(), ALL_TEAMS, allTeams);
		} catch (ApplicationException e) {
			errorLogger("An error while calling the generic finder for the instance : allTeams.",e);
		}
				currentUC33_editableTableForm = (UC33_editableTableForm)populateDestinationForm(request.getSession(), currentUC33_editableTableForm);
		// Call all the Precontroller.
	currentUC33_editableTableForm.setAlwaysCallPreControllers(true);
		model.addAttribute(U_C33_EDITABLE_TABLE_FORM, currentUC33_editableTableForm);
		request.getSession().setAttribute(U_C33_EDITABLE_TABLE_FORM, currentUC33_editableTableForm);
		
	}
	
										/**
	 * method callTeamupdate
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 * @param index Integer the current index 
	 */
	private void callTeamupdate(HttpServletRequest request,IForm form,Integer index) {
					if(index!=null){  
			 selectedTeam = (Team33BO)((List)get(request.getSession(),form, "allTeams")).get(index);  
		} else {
			 selectedTeam= null; 
		}
				 
					try {
				// executing Teamupdate in btn_cancel
	serviceEdit.team33Update(
			selectedTeam
			);  

													// processing variables Teamupdate in btn_cancel
				put(request.getSession(), SELECTED_TEAM,selectedTeam); 
			
			} catch (ApplicationException e) { 
				// error handling for operation team33Update called Teamupdate
				errorLogger("An error occured during the execution of the operation : team33Update",e); 
		
			}
	}
		
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("UC33_editableTableController [ ");
			strBToS.append(ALL_TEAMS);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(allTeams);
			strBToS.append(END_TO_STRING_DELIMITER);
						strBToS.append(SELECTED_TEAM);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(selectedTeam);
			strBToS.append(END_TO_STRING_DELIMITER);
		// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(uC33_editableTableValidator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsUC33_editableTableController!=null); 
		return strBToS.toString();
	}
}
