<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file="/include.jsp"  %>
<html >
<head >
<title  > 
<fmt:message key="cUC54_For_BLU_AGE_DOCUMENTATION"/>
</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"  > 
</meta>
<link href="<c:url value= "/presentation/_style/_style.css"/>" rel="stylesheet" type="text/css"  /> 
<link href="<c:url value= "/presentation/_include/jquery/rd_lib.css"/>" rel="stylesheet" type="text/css"  /> 
<script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/jquery.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/rd_setup.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/rd_lib.js"/>" space="preserve"  ></script><link rel="shortcut icon" href="<c:url value= "/presentation/_style/favicon.ico"/>"  /> 

</head>

<body class=" " > 
<div id="rd_ghost"  > 
<small  > 
<fmt:message key="cUC54_For___CSS_DEACTIVATED"/>
</small>
</div>
<div id="rd_jghost"  > 
<small  > 
<fmt:message key="cUC54_For___JAVASCRIPT_DEACTIVATED"/>
</small>
</div>
<div id="shape"  > 
<div class="ui-layout-center"  > 
<div class="east-north"  > 
	<form:form id="uC54_For" commandName="uC54_ForForm"  enctype="application/x-www-form-urlencoded" method="post">
<h2 class="rd_first"  > 
<fmt:message key="cUC54_For_STANDARD_FEATURES__SERVICES_"/>
</h2>
<a id="lnk_showPlayers"onclick="javascript:btn_click_cExecuteUC54_For(this.href);return false;"				href="<c:url value="/presentation/standardfeatures/services/process/uc54_for/UC54_For/lnk_showPlayers.html"/>"
							 shape="rect"  >									 
<fmt:message key="cUC54_For_SHOW_ALL_PLAYERS"/>
</a>
<h4  > 
<fmt:message key="cUC54_For_LIST_OF_PLAYERS"/>
</h4>
<display:table uid="tab_PlayersList"  sort="list" requestURI="" name="sessionScope.uC54_ForForm.players" class="rd_tabledata"  >
	<c:set   var="rownum">
		<%=(tab_PlayersList !=	null) ? new Integer(tab_PlayersList_rowNum.intValue() - 1) : new Integer(0) %>
	</c:set>
	<c:set   var="idRow">
		<%=(tab_PlayersList != null ) ? tab_PlayersList_rowNum.intValue() - 1 : -1 %>
	</c:set>
	<c:set   var="idTab">
		<%=(tab_PlayersList != null ) ? "tab_PlayersList" : null %>
	</c:set>
	<c:set var="tableHashId">
		<%= new org.displaytag.util.ParamEncoder("tab_PlayersList").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE) %>
	</c:set>
	<c:set var="tableHashId2">
		<%=request.getParameter(new org.displaytag.util.ParamEncoder("tab_PlayersList").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE)) %>
	</c:set>	
	
<display:column format=""  titleKey="cUC54_For_LAST_NAME">
		<span id="txt_lastname1">
	<c:out value="${tab_PlayersList.lastName}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="cUC54_For_FIRST_NAME">
		<span id="txt_firstname1">
	<c:out value="${tab_PlayersList.firstName}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="cUC54_For_ESTIMATED_VALUE_M">
		<span id="txt_value1">
	<c:out value="${tab_PlayersList.value}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="cUC54_For_ROOKIE">
<form:checkbox id="chk_rookie[${rownum}]" path="players[${rownum}].rookie"  name="isRookie" disabled="true"  
		/>
</display:column>
	
<display:column format=""  titleKey="cUC54_For_ADD_TO_TEAM">
<form:checkbox id="chx_team[${rownum}]" path="players[${rownum}].isInTeam"   
		/>
</display:column>
</display:table>
<a id="lnk_selectPlayers"onclick="javascript:btn_click_cExecuteUC54_For(this.href);return false;"				href="<c:url value="/presentation/standardfeatures/services/process/uc54_for/UC54_For/lnk_selectPlayers.html"/>"
							 shape="rect"  >									 
<fmt:message key="cUC54_For_VALIDATE_THE_SELECTION"/>
</a>
<br clear="none"  /> 
<br clear="none"  /> 
<h4  > 
<fmt:message key="cUC54_For_PLAYERS_SELECTED_FOR_THE_TEAM"/>
</h4>
<display:table uid="tab_TeamPlayersList"  sort="list" requestURI="" name="sessionScope.uC54_ForForm.teamPlayers" class="rd_tabledata"  >
	<c:set   var="rownum">
		<%=(tab_TeamPlayersList !=	null) ? new Integer(tab_TeamPlayersList_rowNum.intValue() - 1) : new Integer(0) %>
	</c:set>
	<c:set   var="idRow">
		<%=(tab_TeamPlayersList != null ) ? tab_TeamPlayersList_rowNum.intValue() - 1 : -1 %>
	</c:set>
	<c:set   var="idTab">
		<%=(tab_TeamPlayersList != null ) ? "tab_TeamPlayersList" : null %>
	</c:set>
	<c:set var="tableHashId">
		<%= new org.displaytag.util.ParamEncoder("tab_TeamPlayersList").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE) %>
	</c:set>
	<c:set var="tableHashId2">
		<%=request.getParameter(new org.displaytag.util.ParamEncoder("tab_TeamPlayersList").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE)) %>
	</c:set>	
	
<display:column format=""  titleKey="cUC54_For_LAST_NAME_215">
		<span id="txt_lastname1">
	<c:out value="${tab_TeamPlayersList.lastName}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="cUC54_For_FIRST_NAME_216">
		<span id="txt_firstname1">
	<c:out value="${tab_TeamPlayersList.firstName}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="cUC54_For_ESTIMATED_VALUE_M_217">
		<span id="txt_value1">
	<c:out value="${tab_TeamPlayersList.value}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="cUC54_For_ROOKIE_218">
<form:checkbox id="chk_rookie1[${rownum}]" path="teamPlayers[${rownum}].rookie"  name="isRookie" disabled="true"  
		/>
</display:column>
</display:table>
	</form:form>
	<script language="javascript">
		function btn_click_cExecuteUC54_For(url) {
			UC54_Forvar = document.getElementById('uC54_For');
			UC54_Forvar.setAttribute('action', url);
			UC54_Forvar.submit();
		}
		function btn_click_cSetActionUC54_For(url) {
			UC54_Forvar = document.getElementById('uC54_For');
			UC54_Forvar.setAttribute('action', url);
		}
	</script>
</div>
<div class="east-center"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/services/process/uc54_for/UC54_Forcmp_tabs" />
</div>
</div>
<div class="ui-layout-north"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/services/process/uc54_for/UC54_Forcmp_header" />
</div>
<div class="ui-layout-south"  > 
<fmt:message key="cUC54_For________"/>
<span id="rd_jversion"  > 
<fmt:message key="cUC54_For_BLU_AGE_DOCUMENTATION_219"/>
</span>
</div>
<div class="ui-layout-west"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/services/process/uc54_for/UC54_Forcmp_treeview" />
</div>
</div>
</body>
</html>
