/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.service.standardfeatures.services.sql.uc56_sqloperation.daofinder;


import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateTemplate;

import com.bluage.documentation.business.standardfeatures.services.sql.uc56_sqloperation.bos.Team56BO;
import com.netfective.bluage.core.exception.ApplicationException;


/**
 * This class represents query invocation methods implementation
 * for ServiceSearch56 service.
 *
 * @see com.bluage.documentation.service.standardfeatures.services.sql.uc56_sqloperation.ServiceSearch56
 */
 
@Named ("com.bluage.documentation.service.standardfeatures.services.sql.uc56_sqloperation.daofinder.ServiceSearch56DAOMethodFinderImpl")
public class ServiceSearch56DAOMethodFinderImpl {

	// LOGGER for ServiceSearch56DAOMethodFinderImpl
	private static final Logger LOGGER = Logger.getLogger(ServiceSearch56DAOMethodFinderImpl.class);
	
	/**
	* Hibernate Template (injected)
	*/
	@Inject
	private HibernateTemplate hibernateTemplate;

	
	/**
	 * setter for HibernateTemplate
	 * @param value : {@link HibernateTemplate}
	 */
	public void setHibernateTemplate( HibernateTemplate value )
	{
		hibernateTemplate = value;
	}
	

	
				
								
	/**    
	 * SQL operation.
	 * @param name {@link String}
		 * @return {@link List}
	 */
	public List searchBySqlOperation(String name )
 throws ApplicationException {
Session session = hibernateTemplate.getSessionFactory().getCurrentSession();
			List ret = privateSearchBySqlOperation(name, session);
	
		return ret;
	}
 

	
	/**    
	 * private delegation for SQL operation.
	 * @param name {@link String}
		 * @return {@link List}
	 */
	private List privateSearchBySqlOperation(String name , Session session)
 throws ApplicationException {
		StringBuffer buffer = new StringBuffer();
		buffer.append(" SELECT * FROM team56 WHERE name LIKE :name ");

	Query query = session.createSQLQuery( buffer.toString() ).addEntity(Team56BO.class );

		query.setParameter( "name", name );
						List<Team56BO> returnValue;
try {
		returnValue = query.list();
		} catch ( HibernateException e ) {
		StringBuilder strLog = new StringBuilder("Error while executing the SQL [");
			strLog.append(" SELECT * FROM team56 WHERE name LIKE :name ");
			strLog.append("] ");
			LOGGER.error( strLog.toString(), e );
			throw e;
		}
		return returnValue;
			}
				
								
	/**    
	 * SQL operation.
	 * @param name {@link String}
		 * @return {@link List}
	 */
	public List searchBySqlOperationPostProcess(String name )
 throws ApplicationException {
Session session = hibernateTemplate.getSessionFactory().getCurrentSession();
			List ret = privateSearchBySqlOperationPostProcess(name, session);
	
		return ret;
	}
 

	
	/**    
	 * private delegation for SQL operation.
	 * @param name {@link String}
		 * @return {@link List}
	 */
	private List privateSearchBySqlOperationPostProcess(String name , Session session)
 throws ApplicationException {
		StringBuffer buffer = new StringBuffer();
		buffer.append(" SELECT id, name FROM team56 WHERE name LIKE :name ");

	Query query = session.createSQLQuery( buffer.toString() );

		query.setParameter( "name", name );
						List<Team56BO> returnValue;
try {
		returnValue =  ((SQLQuery) query).addScalar("name").addScalar("id", org.hibernate.Hibernate.LONG).setResultTransformer(org.hibernate.transform.Transformers.aliasToBean(com.bluage.documentation.business.standardfeatures.services.sql.uc56_sqloperation.bos.Team56ForPostProcessBO.class)).list();
		} catch ( HibernateException e ) {
		StringBuilder strLog = new StringBuilder("Error while executing the SQL [");
			strLog.append(" SELECT id, name FROM team56 WHERE name LIKE :name ");
			strLog.append("] ");
			LOGGER.error( strLog.toString(), e );
			throw e;
		}
		return returnValue;
			}




}
