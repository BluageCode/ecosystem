<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file="/include.jsp"  %>
<html >
<head >
<title  > 
<fmt:message key="cUC64_HomePage_BLU_AGE_DOCUMENTATION"/>
</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"  > 
</meta>
<link href="<c:url value= "/presentation/_style/_style.css"/>" rel="stylesheet" type="text/css"  /> 
<link href="<c:url value= "/presentation/_include/jquery/rd_lib.css"/>" rel="stylesheet" type="text/css"  /> 
<script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/jquery.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/rd_setup.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/rd_lib.js"/>" space="preserve"  ></script><link rel="shortcut icon" href="<c:url value= "/presentation/_style/favicon.ico"/>"  /> 

</head>

<body class=" " > 
<div id="rd_ghost"  > 
<small  > 
<fmt:message key="cUC64_HomePage___CSS_DEACTIVATED"/>
</small>
</div>
<div id="rd_jghost"  > 
<small  > 
<fmt:message key="cUC64_HomePage___JAVASCRIPT_DEACTIVATED"/>
</small>
</div>
<div id="shape"  > 
<div class="ui-layout-center"  > 
<div class="east-north"  > 
	<form:form id="uC64_HomePage" commandName="uC64_HomePageForm"  enctype="application/x-www-form-urlencoded" method="post">
<h2 class="rd_first"  > 
<fmt:message key="cUC64_HomePage_STANDARD_FEATURES__ENTITIES_"/>
</h2>
<h4  > 
<fmt:message key="cUC64_HomePage_TEAMS"/>
</h4>
<display:table uid="tab_teams" pagesize="4"  sort="list" requestURI="" name="sessionScope.uC64_HomePageForm.listTeams" class="rd_tabledata"  >
	<c:set   var="rownum">
		<%=(tab_teams !=	null) ? new Integer(tab_teams_rowNum.intValue() - 1) : new Integer(0) %>
	</c:set>
	<c:set   var="idRow">
		<%=(tab_teams != null ) ? tab_teams_rowNum.intValue() - 1 : -1 %>
	</c:set>
	<c:set   var="idTab">
		<%=(tab_teams != null ) ? "tab_teams" : null %>
	</c:set>
	<c:set var="tableHashId">
		<%= new org.displaytag.util.ParamEncoder("tab_teams").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE) %>
	</c:set>
	<c:set var="tableHashId2">
		<%=request.getParameter(new org.displaytag.util.ParamEncoder("tab_teams").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE)) %>
	</c:set>	
	
<display:column format=""  titleKey="cUC64_HomePage_NAME">
<c:if test="${uC64_HomePageForm.selectedRow!=idRow || uC64_HomePageForm.selectedTab!=idTab}">
		<span id="dyn_name">
	<c:out value="${tab_teams.name}" />
		</span>
	</c:if>
<c:if test="${uC64_HomePageForm.selectedRow==idRow && uC64_HomePageForm.selectedTab==idTab}">
<form:input id="txt_name[${rownum}]" path="listTeams[${rownum}].name"  
		/>
</c:if>
</display:column>
	
<display:column format=""  titleKey="cUC64_HomePage_CITY">
		<span id="txt_city">
	<c:out value="${tab_teams.city}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="cUC64_HomePage_STATE">
		<span id="txt_state">
	<c:out value="${tab_teams.state.name}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="cUC64_HomePage_DELETE">
<a id="lnk_deleteTeam[${rownum}]"onclick="javascript:btn_click_cExecuteUC64_HomePage(this.href);return false;"			href="<c:url value="/presentation/standardfeatures/entities/uc64_cascade/UC64_HomePage/lnk_deleteTeam.html?id="/><c:out value="${idRow}" />&tab=<c:out value="tab_teams" />&<c:out value="${tableHashId}"/>=<c:out value="${tableHashId2}"/>"
	 shape="rect"  >									 
<fmt:message key="cUC64_HomePage__DELETE_"/>
</a>
</display:column>
	
<display:column format=""  titleKey="cUC64_HomePage_DETAILS">
<a id="lnk_details[${rownum}]"onclick="javascript:btn_click_cExecuteUC64_HomePage(this.href);return false;"			href="<c:url value="/presentation/standardfeatures/entities/uc64_cascade/UC64_HomePage/lnk_details.html?id="/><c:out value="${idRow}" />&tab=<c:out value="tab_teams" />&<c:out value="${tableHashId}"/>=<c:out value="${tableHashId2}"/>"
	 shape="rect"  >									 
<fmt:message key="cUC64_HomePage__DETAILS_"/>
</a>
</display:column>
</display:table>
<h4  > 
<fmt:message key="cUC64_HomePage_PLAYERS_"/>
<i  > 
<fmt:message key="cUC64_HomePage_CHILDS_CASCADE_DELETE"/>
</i>
</h4>
<display:table uid="tab_players" pagesize="4"  sort="list" requestURI="" name="sessionScope.uC64_HomePageForm.allPlayers" class="rd_tabledata"  >
	<c:set   var="rownum">
		<%=(tab_players !=	null) ? new Integer(tab_players_rowNum.intValue() - 1) : new Integer(0) %>
	</c:set>
	<c:set   var="idRow">
		<%=(tab_players != null ) ? tab_players_rowNum.intValue() - 1 : -1 %>
	</c:set>
	<c:set   var="idTab">
		<%=(tab_players != null ) ? "tab_players" : null %>
	</c:set>
	<c:set var="tableHashId">
		<%= new org.displaytag.util.ParamEncoder("tab_players").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE) %>
	</c:set>
	<c:set var="tableHashId2">
		<%=request.getParameter(new org.displaytag.util.ParamEncoder("tab_players").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE)) %>
	</c:set>	
	
<display:column format=""  titleKey="cUC64_HomePage_NAME_153">
		<span id="dyn_lastname">
	<c:out value="${tab_players.lastName}" />
		</span>
	<fmt:message key="cUC64_HomePage__"/>
		<span id="dyn_firstname">
	<c:out value="${tab_players.firstName}" />
		</span>
	</display:column>
</display:table>
<h4  > 
<fmt:message key="cUC64_HomePage_COACHS_"/>
<i  > 
<fmt:message key="cUC64_HomePage_CHILDS_CASCADE_NONE"/>
</i>
</h4>
<display:table uid="tab_coachs" pagesize="4"  sort="list" requestURI="" name="sessionScope.uC64_HomePageForm.allcoachs" class="rd_tabledata"  >
	<c:set   var="rownum">
		<%=(tab_coachs !=	null) ? new Integer(tab_coachs_rowNum.intValue() - 1) : new Integer(0) %>
	</c:set>
	<c:set   var="idRow">
		<%=(tab_coachs != null ) ? tab_coachs_rowNum.intValue() - 1 : -1 %>
	</c:set>
	<c:set   var="idTab">
		<%=(tab_coachs != null ) ? "tab_coachs" : null %>
	</c:set>
	<c:set var="tableHashId">
		<%= new org.displaytag.util.ParamEncoder("tab_coachs").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE) %>
	</c:set>
	<c:set var="tableHashId2">
		<%=request.getParameter(new org.displaytag.util.ParamEncoder("tab_coachs").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE)) %>
	</c:set>	
	
<display:column format=""  titleKey="cUC64_HomePage_NAME_154">
		<span id="dyn_name">
	<c:out value="${tab_coachs.name}" />
		</span>
	</display:column>
</display:table>
<h4  > 
<fmt:message key="cUC64_HomePage_STATES_"/>
<i  > 
<fmt:message key="cUC64_HomePage_CHILDS_CASCADE_SAVEUPDATE"/>
</i>
</h4>
<display:table uid="tab_states" pagesize="4"  sort="list" requestURI="" name="sessionScope.uC64_HomePageForm.allstates" class="rd_tabledata"  >
	<c:set   var="rownum">
		<%=(tab_states !=	null) ? new Integer(tab_states_rowNum.intValue() - 1) : new Integer(0) %>
	</c:set>
	<c:set   var="idRow">
		<%=(tab_states != null ) ? tab_states_rowNum.intValue() - 1 : -1 %>
	</c:set>
	<c:set   var="idTab">
		<%=(tab_states != null ) ? "tab_states" : null %>
	</c:set>
	<c:set var="tableHashId">
		<%= new org.displaytag.util.ParamEncoder("tab_states").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE) %>
	</c:set>
	<c:set var="tableHashId2">
		<%=request.getParameter(new org.displaytag.util.ParamEncoder("tab_states").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE)) %>
	</c:set>	
	
<display:column format=""  titleKey="cUC64_HomePage_NAME_155">
		<span id="dyn_name">
	<c:out value="${tab_states.name}" />
		</span>
	</display:column>
	
<display:column format=""  titleKey="cUC64_HomePage_DELETE_156">
<a id="lnk_deleteState[${rownum}]"onclick="javascript:btn_click_cExecuteUC64_HomePage(this.href);return false;"			href="<c:url value="/presentation/standardfeatures/entities/uc64_cascade/UC64_HomePage/lnk_deleteState.html?id="/><c:out value="${idRow}" />&tab=<c:out value="tab_states" />&<c:out value="${tableHashId}"/>=<c:out value="${tableHashId2}"/>"
	 shape="rect"  >									 
<fmt:message key="cUC64_HomePage__DELETE__157"/>
</a>
</display:column>
</display:table>
<h4  > 
<fmt:message key="cUC64_HomePage_AUDIENCES_"/>
<i  > 
<fmt:message key="cUC64_HomePage_CHILDS_CASCADE_ALL"/>
</i>
</h4>
<display:table uid="tab_audiences" pagesize="4"  sort="list" requestURI="" name="sessionScope.uC64_HomePageForm.allaudiences" class="rd_tabledata"  >
	<c:set   var="rownum">
		<%=(tab_audiences !=	null) ? new Integer(tab_audiences_rowNum.intValue() - 1) : new Integer(0) %>
	</c:set>
	<c:set   var="idRow">
		<%=(tab_audiences != null ) ? tab_audiences_rowNum.intValue() - 1 : -1 %>
	</c:set>
	<c:set   var="idTab">
		<%=(tab_audiences != null ) ? "tab_audiences" : null %>
	</c:set>
	<c:set var="tableHashId">
		<%= new org.displaytag.util.ParamEncoder("tab_audiences").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE) %>
	</c:set>
	<c:set var="tableHashId2">
		<%=request.getParameter(new org.displaytag.util.ParamEncoder("tab_audiences").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE)) %>
	</c:set>	
	
<display:column format=""  titleKey="cUC64_HomePage_NAME_158">
		<span id="dyn_name">
	<c:out value="${tab_audiences.name}" />
		</span>
	</display:column>
</display:table>
	</form:form>
	<script language="javascript">
		function btn_click_cExecuteUC64_HomePage(url) {
			UC64_HomePagevar = document.getElementById('uC64_HomePage');
			UC64_HomePagevar.setAttribute('action', url);
			UC64_HomePagevar.submit();
		}
		function btn_click_cSetActionUC64_HomePage(url) {
			UC64_HomePagevar = document.getElementById('uC64_HomePage');
			UC64_HomePagevar.setAttribute('action', url);
		}
	</script>
</div>
<div class="east-center"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/entities/uc64_cascade/UC64_HomePagecmp_tabs" />
</div>
</div>
<div class="ui-layout-north"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/entities/uc64_cascade/UC64_HomePagecmp_header" />
</div>
<div class="ui-layout-south"  > 
<fmt:message key="cUC64_HomePage___"/>
<span id="rd_jversion"  > 
<fmt:message key="cUC64_HomePage_BLU_AGE_DOCUMENTATION_159"/>
</span>
</div>
<div class="ui-layout-west"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/entities/uc64_cascade/UC64_HomePagecmp_treeview" />
</div>
</div>
</body>
</html>
