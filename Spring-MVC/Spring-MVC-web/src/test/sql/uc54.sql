--<ScriptOptions statementTerminator=";"/>

ALTER TABLE player54 DROP CONSTRAINT player53_pkey;

DROP TABLE player54;

CREATE TABLE player54 (
		id INT8 NOT NULL,
		version INT4 NOT NULL,
		first_name VARCHAR(1024),
		last_name VARCHAR(1024),
		rookie BOOL,
		value FLOAT4
	);

INSERT INTO player54 (id, version, first_name, last_name, rookie, value) VALUES (1, 0, 'Jack', 'White', false, 25);
INSERT INTO player54 (id, version, first_name, last_name, rookie, value) VALUES (2, 0, 'Robert', 'Plant', false, 43);
INSERT INTO player54 (id, version, first_name, last_name, rookie, value) VALUES (3, 0, 'Jim', 'Morrisson', false, 38);
INSERT INTO player54 (id, version, first_name, last_name, rookie, value) VALUES (4, 0, 'Roger', 'Waters', true, 32);
INSERT INTO player54 (id, version, first_name, last_name, rookie, value) VALUES (5, 0, 'Trent', 'Reznor', true, 29);
INSERT INTO player54 (id, version, first_name, last_name, rookie, value) VALUES (6, 0, 'Howard', 'Lovecraft', false, 51);
INSERT INTO player54 (id, version, first_name, last_name, rookie, value) VALUES (7, 0, 'Dan', 'Simmons', true, 27);
INSERT INTO player54 (id, version, first_name, last_name, rookie, value) VALUES (8, 0, 'Stanley', 'Kubrick', true, 16);
INSERT INTO player54 (id, version, first_name, last_name, rookie, value) VALUES (9, 0, 'Tim', 'Burton', false, 14);
INSERT INTO player54 (id, version, first_name, last_name, rookie, value) VALUES (10, 0, 'Isaac', 'Asimov', true, 20);


ALTER TABLE player54 ADD CONSTRAINT player53_pkey PRIMARY KEY (id);

