<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file="/include.jsp"  %>
<html >
<head >
<title  > 
<fmt:message key="cUC03_FormValidationMessage_BLU_AGE_DOCUMENTATION"/>
</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"  > 
</meta>
<link href="<c:url value= "/presentation/_style/_style.css"/>" rel="stylesheet" type="text/css"  /> 
<link href="<c:url value= "/presentation/_include/jquery/rd_lib.css"/>" rel="stylesheet" type="text/css"  /> 
<script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/jquery.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/rd_setup.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/rd_lib.js"/>" space="preserve"  ></script><link rel="shortcut icon" href="<c:url value= "/presentation/_style/favicon.ico"/>"  /> 

</head>

<body class=" " > 
<div id="rd_ghost"  > 
<small  > 
<fmt:message key="cUC03_FormValidationMessage___CSS_DEACTIVATED"/>
</small>
</div>
<div id="rd_jghost"  > 
<small  > 
<fmt:message key="cUC03_FormValidationMessage___JAVASCRIPT_DEACTIVATED"/>
</small>
</div>
<div id="shape"  > 
<div class="ui-layout-center"  > 
<div class="east-north"  > 
	<form:form id="uC03_FormValidationMessage" commandName="uC03_FormValidationMessageForm"  enctype="application/x-www-form-urlencoded" method="post">
<h2 class="rd_first"  > 
<fmt:message key="cUC03_FormValidationMessage_GETTING_STARTED__UC03__FORM_"/>
</h2>
<p  > 
<fmt:message key="cUC03_FormValidationMessage_THE_PLAYER_HAS_BEEN_CREATED"/>
</p>
<p align="center"  > 
<a id="lnk_back"				href="<c:url value="/presentation/gettingstarted/uc03_formvalidation/UC03_FormValidationMessage/lnk_back.html"/>"
													 shape="rect"  >									 
<fmt:message key="cUC03_FormValidationMessage_CREATE_ANOTHER_PLAYER"/>
</a>
</p>
	</form:form>
	<script language="javascript">
		function btn_click_cExecuteUC03_FormValidationMessage(url) {
			UC03_FormValidationMessagevar = document.getElementById('uC03_FormValidationMessage');
			UC03_FormValidationMessagevar.setAttribute('action', url);
			UC03_FormValidationMessagevar.submit();
		}
		function btn_click_cSetActionUC03_FormValidationMessage(url) {
			UC03_FormValidationMessagevar = document.getElementById('uC03_FormValidationMessage');
			UC03_FormValidationMessagevar.setAttribute('action', url);
		}
	</script>
</div>
<div class="east-center"  > 
<tiles:insertDefinition	name="presentation/gettingstarted/uc03_formvalidation/UC03_FormValidationMessagecmp_tabs" />
</div>
</div>
<div class="ui-layout-north"  > 
<tiles:insertDefinition	name="presentation/gettingstarted/uc03_formvalidation/UC03_FormValidationMessagecmp_header" />
</div>
<div class="ui-layout-south"  > 
<fmt:message key="cUC03_FormValidationMessage___"/>
<span id="rd_jversion"  > 
<fmt:message key="cUC03_FormValidationMessage_BLU_AGE_CORPORATION"/>
</span>
</div>
<div class="ui-layout-west"  > 
<tiles:insertDefinition	name="presentation/gettingstarted/uc03_formvalidation/UC03_FormValidationMessagecmp_treeview" />
</div>
</div>
</body>
</html>
