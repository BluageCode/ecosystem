/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.entities.uc59_umltosqltypemapping ;

// Import declaration.
// Java imports.
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.bluage.documentation.business.standardfeatures.entities.uc59_umltosqltypemapping.entities.daofinder.Player59DAOFinderImpl;
import com.netfective.bluage.core.exception.ApplicationException;
import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;

/**
 * Class : UC59_UMLtoSQLmappingController
 */
@Controller
@Scope("prototype")
@SessionAttributes("uC59_UMLtoSQLmappingForm")
@RequestMapping(value= "/presentation/standardfeatures/entities/uc59_umltosqltypemapping")
public class UC59_UMLtoSQLmappingController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC59_UMLtoSQLmappingController.class);
	
	//Current form name
	private static final String U_C59__U_M_LTO_S_Q_LMAPPING_FORM = "uC59_UMLtoSQLmappingForm";

	//CONSTANT: players table or repeater.
	private static final String PLAYERS = "players";
				
	/**
	 * Property:customDateEditorsUC59_UMLtoSQLmappingController
	 */
	final private Map<String,CustomDateEditor> customDateEditorsUC59_UMLtoSQLmappingController = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:uC59_UMLtoSQLmappingValidator
	 */
	final private UC59_UMLtoSQLmappingValidator uC59_UMLtoSQLmappingValidator = new UC59_UMLtoSQLmappingValidator();
	
	/**
	 * Generic Finder : player59DAOFinderImpl.
	 */
	@Autowired
	private Player59DAOFinderImpl player59DAOFinderImpl;
	
	
	// Initialise all the instances for UC59_UMLtoSQLmappingController
		// Initialize the instance players for the state lnk_sort
		private List players; // Initialize the instance players for UC59_UMLtoSQLmappingController
				
	/**
	* This method initialise the form : UC59_UMLtoSQLmappingForm 
	* @return UC59_UMLtoSQLmappingForm
	*/
	@ModelAttribute("UC59_UMLtoSQLmappingFormInit")
	public void initUC59_UMLtoSQLmappingForm(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, U_C59__U_M_LTO_S_Q_LMAPPING_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method UC59_UMLtoSQLmappingForm."); //for lnk_sort 
		}
		UC59_UMLtoSQLmappingForm uC59_UMLtoSQLmappingForm;
	
		if(request.getSession().getAttribute(U_C59__U_M_LTO_S_Q_LMAPPING_FORM) != null){
			uC59_UMLtoSQLmappingForm = (UC59_UMLtoSQLmappingForm)request.getSession().getAttribute(U_C59__U_M_LTO_S_Q_LMAPPING_FORM);
		} else {
			uC59_UMLtoSQLmappingForm = new UC59_UMLtoSQLmappingForm();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form UC59_UMLtoSQLmappingForm.");
		}
		uC59_UMLtoSQLmappingForm = (UC59_UMLtoSQLmappingForm)populateDestinationForm(request.getSession(), uC59_UMLtoSQLmappingForm);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  UC59_UMLtoSQLmappingForm.");
		}
		model.addAttribute(U_C59__U_M_LTO_S_Q_LMAPPING_FORM, uC59_UMLtoSQLmappingForm);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param UC59_UMLtoSQLmappingForm : The sceen form.
	 */
	@RequestMapping(value = "/UC59_UMLtoSQLmapping.html" ,method = RequestMethod.GET)
	public void prepareUC59_UMLtoSQLmapping(final HttpServletRequest request,final  Model model,final  @ModelAttribute("UC59_UMLtoSQLmappingFormInit") UC59_UMLtoSQLmappingForm uC59_UMLtoSQLmappingForm){
		
		UC59_UMLtoSQLmappingForm currentUC59_UMLtoSQLmappingForm = uC59_UMLtoSQLmappingForm;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(U_C59__U_M_LTO_S_Q_LMAPPING_FORM) == null){
			if(currentUC59_UMLtoSQLmappingForm!=null){
				request.getSession().setAttribute(U_C59__U_M_LTO_S_Q_LMAPPING_FORM, currentUC59_UMLtoSQLmappingForm);
			}else {
				currentUC59_UMLtoSQLmappingForm = new UC59_UMLtoSQLmappingForm();
				request.getSession().setAttribute(U_C59__U_M_LTO_S_Q_LMAPPING_FORM, currentUC59_UMLtoSQLmappingForm);	
			}
		} else {
			currentUC59_UMLtoSQLmappingForm = (UC59_UMLtoSQLmappingForm) request.getSession().getAttribute(U_C59__U_M_LTO_S_Q_LMAPPING_FORM);
		}

		try {
			List players = player59DAOFinderImpl.findAll();
			put(request.getSession(), PLAYERS, players);
		} catch (ApplicationException e) {
			errorLogger("An error while calling the generic finder for the instance : players.",e);
		}
				currentUC59_UMLtoSQLmappingForm = (UC59_UMLtoSQLmappingForm)populateDestinationForm(request.getSession(), currentUC59_UMLtoSQLmappingForm);
		// Call all the Precontroller.
	currentUC59_UMLtoSQLmappingForm.setAlwaysCallPreControllers(true);
		model.addAttribute(U_C59__U_M_LTO_S_Q_LMAPPING_FORM, currentUC59_UMLtoSQLmappingForm);
		request.getSession().setAttribute(U_C59__U_M_LTO_S_Q_LMAPPING_FORM, currentUC59_UMLtoSQLmappingForm);
		
	}
	
			
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("UC59_UMLtoSQLmappingController [ ");
			strBToS.append(PLAYERS);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(players);
			strBToS.append(END_TO_STRING_DELIMITER);
					// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(uC59_UMLtoSQLmappingValidator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsUC59_UMLtoSQLmappingController!=null); 
		return strBToS.toString();
	}
}
