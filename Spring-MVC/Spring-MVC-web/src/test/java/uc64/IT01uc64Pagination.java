package uc64;


import org.junit.Test;
import test.common.AbstractBADocTestCase;

public class IT01uc64Pagination extends AbstractBADocTestCase {

	@Override
	public  String getName(){
		return "UC-64";
	}

	@Test
	public void testIT01uc64Pagination() throws Exception {
		// On teste l'UC64 - Standard Features/Entities - Cascade
		selenium.open("");
		selenium.click("link=Expand All");
		selenium.waitForPageToLoad("30000");
		selenium.click("link=UC64 - Cascade");
		verifyTrue(selenium.isElementPresent("//h2[contains(text(),\"Standard Features > Entities > UC64 > Cascade\")]"));
		// On teste la pagination.
		// Pour le tableau 'Teams'.
		// On va sur la page 2.
		selenium.click("xpath=(//span/a)[3]");
		selenium.waitForPageToLoad("30000");
		// On vérifie qu'on est sur la page 2.
		assertTrue(selenium.isElementPresent("xpath= (//span/strong)[1][contains(text(),\"2\")]| (//a[not(text()) or (text() != '[delete]' and text() != '[details]')])[5]/b[contains(text(),\"2\")] "));
		// On teste le lien Prev.
		selenium.click("xpath=(//span/a)[2]");
		selenium.waitForPageToLoad("30000");
		// On vérifie que l'on est sur la première page.
		assertTrue(selenium.isElementPresent("xpath= (//span/strong)[1][contains(text(),\"1\")]|(//a[not(text()) or (text() != '[delete]' and text() != '[details]')])[4]/b[contains(text(),\"1\")]"));
		// On teste le lien Last.
		selenium.click("xpath=(//span/a)[4]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("xpath= (//span/strong)[1][contains(text(),\"3\")]|(//a[not(text()) or (text() != '[delete]' and text() != '[details]')])[6]/b[contains(text(),\"3\")]"));
		// On teste le lien First
		selenium.click("xpath=(//span/a)[1]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("xpath= (//span/strong)[1][contains(text(),\"1\")]|(//a[not(text()) or (text() != '[delete]' and text() != '[details]')])[4]/b[contains(text(),\"1\")]"));
		// On teste la pagination pour le tableau Players.
		// On va sur la page 2.
		selenium.click("xpath=(//span/a)[5]");
		selenium.waitForPageToLoad("30000");
		// On vérifie qu'on est sur la page 2.
		assertTrue(selenium.isElementPresent("xpath= (//span/strong)[2][contains(text(),\"2\")]|(//a[not(text()) or (text() != '[delete]' and text() != '[details]')])[14]/b[contains(text(),\"2\")]"));
		// On teste le lien Prev.
		selenium.click("xpath=(//span/a)[6]");
		selenium.waitForPageToLoad("30000");
		// On vérifie que l'on est sur la première page.
		assertTrue(selenium.isElementPresent("xpath= (//span/strong)[2][contains(text(),\"1\")]|(//a[not(text()) or (text() != '[delete]' and text() != '[details]')])[13]/b[contains(text(),\"1\")]"));
		// On teste le lien Last.
		selenium.click("xpath=(//span/a)[13]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("xpath= (//span/strong)[2][contains(text(),\"9\")]|(//a[not(text()) or (text() != '[delete]' and text() != '[details]')])[21]/b[contains(text(),\"9\")]"));
		// On teste le lien First
		selenium.click("xpath=(//span/a)[5]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("xpath= (//span/strong)[2][contains(text(),\"1\")]|(//a[not(text()) or (text() != '[delete]' and text() != '[details]')])[13]/b[contains(text(),\"1\")]"));
		// On teste la pagination pour le tableau Coach.
		// On va sur la page 2.
		selenium.click("xpath=(//span/a)[14]");
		selenium.waitForPageToLoad("30000");
		// On vérifie qu'on est sur la page 2.
		assertTrue(selenium.isElementPresent("xpath= (//span/strong)[3][contains(text(),\"2\")]|(//a[not(text()) or (text() != '[delete]' and text() != '[details]')])[29]/b[contains(text(),\"2\")]"));
		// On teste le lien Prev.
		selenium.click("xpath=(//span/a)[15]");
		selenium.waitForPageToLoad("30000");
		// On vérifie que l'on est sur la première page.
		assertTrue(selenium.isElementPresent("xpath= (//span/strong)[3][contains(text(),\"1\")]|(//a[not(text()) or (text() != '[delete]' and text() != '[details]')])[28]/b[contains(text(),\"1\")]"));
		// On teste le lien Last.
		selenium.click("xpath=(//span/a)[18]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("xpath= (//span/strong)[3][contains(text(),\"4\")]|(//a[not(text()) or (text() != '[delete]' and text() != '[details]')])[31]/b[contains(text(),\"4\")]"));
		// On teste le lien First
		selenium.click("xpath=(//span/a)[14]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("xpath= (//span/strong)[3][contains(text(),\"1\")]|(//a[not(text()) or (text() != '[delete]' and text() != '[details]')])[28]/b[contains(text(),\"1\")]"));
		// On teste la pagination pour le tableau States.
		// On va sur la page 2.
		selenium.click("xpath=(//span/a)[19]");
		selenium.waitForPageToLoad("30000");
		// On vérifie qu'on est sur la page 2.
		assertTrue(selenium.isElementPresent("xpath= (//span/strong)[4][contains(text(),\"2\")]|(//a[not(text()) or (text() != '[delete]' and text() != '[details]')])[39]/b[contains(text(),\"2\")]"));
		// On teste le lien Prev.
		selenium.click("xpath=(//span/a)[20]");
		selenium.waitForPageToLoad("30000");
		// On vérifie que l'on est sur la première page.
		assertTrue(selenium.isElementPresent("xpath= (//span/strong)[4][contains(text(),\"1\")]|(//a[not(text()) or (text() != '[delete]' and text() != '[details]')])[38]/b[contains(text(),\"1\")]"));
		// On teste le lien Last.
		selenium.click("xpath=(//span/a)[22]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("xpath= (//span/strong)[4][contains(text(),\"3\")]|(//a[not(text()) or (text() != '[delete]' and text() != '[details]')])[40]/b[contains(text(),\"3\")]"));
		// On teste le lien First
		selenium.click("xpath=(//span/a)[19]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("xpath= (//span/strong)[4][contains(text(),\"1\")]|(//a[not(text()) or (text() != '[delete]' and text() != '[details]')])[38]/b[contains(text(),\"1\")]"));
	}


}
