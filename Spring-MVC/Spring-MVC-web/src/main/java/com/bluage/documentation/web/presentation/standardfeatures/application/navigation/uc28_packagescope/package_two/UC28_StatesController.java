/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.application.navigation.uc28_packagescope.package_two ;

// Import declaration.
// Java imports.
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.bluage.documentation.service.standardfeatures.application.navigation.uc28_packagescope.ServicePlayer28;
import com.bluage.documentation.service.standardfeatures.application.navigation.uc28_packagescope.ServiceTeam28;
import com.bluage.documentation.web.presentation.standardfeatures.application.navigation.uc28_packagescope.package_one.UC28_PlayersForm;
import com.bluage.documentation.web.presentation.standardfeatures.application.navigation.uc28_packagescope.package_one.UC28_TeamsForm;
import com.netfective.bluage.core.exception.ApplicationException;
import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;
import com.netfective.bluage.core.web.IForm;

/**
 * Class : UC28_StatesController
 */
@Controller
@Scope("prototype")
@SessionAttributes("uC28_StatesForm")
@RequestMapping(value= "/presentation/standardfeatures/application/navigation/uc28_packagescope/package_two")
public class UC28_StatesController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC28_StatesController.class);
	
	//Current form name
	private static final String U_C28__STATES_FORM = "uC28_StatesForm";

	//CONSTANT: allStates table or repeater.
	private static final String ALL_STATES = "allStates";
				//CONSTANT: allTeams
	private static final String ALL_TEAMS = "allTeams";
	//CONSTANT: allPlayers
	private static final String ALL_PLAYERS = "allPlayers";
	
	/**
	 * Property:customDateEditorsUC28_StatesController
	 */
	final private Map<String,CustomDateEditor> customDateEditorsUC28_StatesController = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:uC28_StatesValidator
	 */
	final private UC28_StatesValidator uC28_StatesValidator = new UC28_StatesValidator();
	
	/**
	 * Service declaration : servicePlayer28.
	 */
	@Autowired
	private ServicePlayer28 servicePlayer28;
	
	/**
	 * Service declaration : serviceTeam28.
	 */
	@Autowired
	private ServiceTeam28 serviceTeam28;
	
	
	// Initialise all the instances for UC28_StatesController
		// Initialize the instance allStates for the state findTeams
		private List allStates; // Initialize the instance allStates for UC28_StatesController
						// Declare the instance allTeams
		private List allTeams;
		// Declare the instance allPlayers
		private List allPlayers;
			/**
	 * Operation : lnk_teams
 	 * @param model : 
 	 * @param uC28_States : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC28_States/lnk_teams.html",method = RequestMethod.POST)
	public String lnk_teams(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC28_StatesForm") UC28_StatesForm  uC28_StatesForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : lnk_teams"); 
		infoLogger(uC28_StatesForm); 
		uC28_StatesForm.setAlwaysCallPreControllers(false); 
		// initialization for lnk_teams
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state lnk_teams if the validation fail, you will be forward to the screen : "/presentation/standardfeatures/application/navigation/uc28_packagescope/package_two/UC28_States"
		if(errorsMessages(request,response,uC28_StatesForm,bindingResult,"", uC28_StatesValidator, customDateEditorsUC28_StatesController)){ 
			return "/presentation/standardfeatures/application/navigation/uc28_packagescope/package_two/UC28_States"; 
		}

	 callFindallteams(request,uC28_StatesForm );
											// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.application.navigation.uc28_packagescope.package_one.UC28_TeamsForm or not from lnk_teams
			final UC28_TeamsForm uC28_TeamsForm =  new UC28_TeamsForm();
			// Populate the destination form with all the instances returned from lnk_teams.
			final IForm uC28_TeamsForm2 = populateDestinationForm(request.getSession(), uC28_TeamsForm); 
					
			// Add the form to the model.
			model.addAttribute("uC28_TeamsForm", uC28_TeamsForm2); 
			
			request.getSession().setAttribute("uC28_TeamsForm", uC28_TeamsForm2);
			
			// "OK" CASE => destination screen path from lnk_teams
			LOGGER.info("Go to the screen 'UC28_Teams'.");
			// Redirect (PRG) from lnk_teams
			destinationPath =  "redirect:/presentation/standardfeatures/application/navigation/uc28_packagescope/package_one/UC28_Teams.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_teams 
	}
	
				/**
	 * Operation : lnk_players
 	 * @param model : 
 	 * @param uC28_States : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC28_States/lnk_players.html",method = RequestMethod.POST)
	public String lnk_players(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC28_StatesForm") UC28_StatesForm  uC28_StatesForm, final BindingResult bindingResult ){
		
		
		
		LOGGER.info("Begin method : lnk_players"); 
		infoLogger(uC28_StatesForm); 
		uC28_StatesForm.setAlwaysCallPreControllers(false); 
		// initialization for lnk_players
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state lnk_players if the validation fail, you will be forward to the screen : "/presentation/standardfeatures/application/navigation/uc28_packagescope/package_two/UC28_States"
		if(errorsMessages(request,response,uC28_StatesForm,bindingResult,"", uC28_StatesValidator, customDateEditorsUC28_StatesController)){ 
			return "/presentation/standardfeatures/application/navigation/uc28_packagescope/package_two/UC28_States"; 
		}

	 callFindallplayers(request,uC28_StatesForm );
											// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.application.navigation.uc28_packagescope.package_one.UC28_PlayersForm or not from lnk_players
			final UC28_PlayersForm uC28_PlayersForm =  new UC28_PlayersForm();
			// Populate the destination form with all the instances returned from lnk_players.
			final IForm uC28_PlayersForm2 = populateDestinationForm(request.getSession(), uC28_PlayersForm); 
					
			// Add the form to the model.
			model.addAttribute("uC28_PlayersForm", uC28_PlayersForm2); 
			
			request.getSession().setAttribute("uC28_PlayersForm", uC28_PlayersForm2);
			
			// "OK" CASE => destination screen path from lnk_players
			LOGGER.info("Go to the screen 'UC28_Players'.");
			// Redirect (PRG) from lnk_players
			destinationPath =  "redirect:/presentation/standardfeatures/application/navigation/uc28_packagescope/package_one/UC28_Players.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_players 
	}
	
	
	/**
	* This method initialise the form : UC28_StatesForm 
	* @return UC28_StatesForm
	*/
	@ModelAttribute("UC28_StatesFormInit")
	public void initUC28_StatesForm(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, U_C28__STATES_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method UC28_StatesForm."); //for lnk_players 
		}
		UC28_StatesForm uC28_StatesForm;
	
		if(request.getSession().getAttribute(U_C28__STATES_FORM) != null){
			uC28_StatesForm = (UC28_StatesForm)request.getSession().getAttribute(U_C28__STATES_FORM);
		} else {
			uC28_StatesForm = new UC28_StatesForm();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form UC28_StatesForm.");
		}
		uC28_StatesForm = (UC28_StatesForm)populateDestinationForm(request.getSession(), uC28_StatesForm);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  UC28_StatesForm.");
		}
		model.addAttribute(U_C28__STATES_FORM, uC28_StatesForm);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param UC28_StatesForm : The sceen form.
	 */
	@RequestMapping(value = "/UC28_States.html" ,method = RequestMethod.GET)
	public void prepareUC28_States(final HttpServletRequest request,final  Model model,final  @ModelAttribute("UC28_StatesFormInit") UC28_StatesForm uC28_StatesForm){
		
		UC28_StatesForm currentUC28_StatesForm = uC28_StatesForm;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(U_C28__STATES_FORM) == null){
			if(currentUC28_StatesForm!=null){
				request.getSession().setAttribute(U_C28__STATES_FORM, currentUC28_StatesForm);
			}else {
				currentUC28_StatesForm = new UC28_StatesForm();
				request.getSession().setAttribute(U_C28__STATES_FORM, currentUC28_StatesForm);	
			}
		} else {
			currentUC28_StatesForm = (UC28_StatesForm) request.getSession().getAttribute(U_C28__STATES_FORM);
		}

					currentUC28_StatesForm = (UC28_StatesForm)populateDestinationForm(request.getSession(), currentUC28_StatesForm);
		// Call all the Precontroller.
	currentUC28_StatesForm.setAlwaysCallPreControllers(true);
		model.addAttribute(U_C28__STATES_FORM, currentUC28_StatesForm);
		request.getSession().setAttribute(U_C28__STATES_FORM, currentUC28_StatesForm);
		
	}
	
				/**
	 * method callFindallteams
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callFindallteams(HttpServletRequest request,IForm form) {
		allTeams = (List)get(request.getSession(),form, "allTeams");  
										 
					try {
				// executing Findallteams in lnk_players
				allTeams = 	serviceTeam28.team28FindAll(
	);  
 
				put(request.getSession(), ALL_TEAMS,allTeams);
								// processing variables Findallteams in lnk_players

			} catch (ApplicationException e) { 
				// error handling for operation team28FindAll called Findallteams
				errorLogger("An error occured during the execution of the operation : team28FindAll",e); 
		
			}
	}
		/**
	 * method callFindallplayers
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 */
	private void callFindallplayers(HttpServletRequest request,IForm form) {
		allPlayers = (List)get(request.getSession(),form, "allPlayers");  
										 
					try {
				// executing Findallplayers in lnk_players
				allPlayers = 	servicePlayer28.player28FindAll(
	);  
 
				put(request.getSession(), ALL_PLAYERS,allPlayers);
								// processing variables Findallplayers in lnk_players

			} catch (ApplicationException e) { 
				// error handling for operation player28FindAll called Findallplayers
				errorLogger("An error occured during the execution of the operation : player28FindAll",e); 
		
			}
	}
		
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("UC28_StatesController [ ");
			strBToS.append(ALL_STATES);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(allStates);
			strBToS.append(END_TO_STRING_DELIMITER);
						strBToS.append(ALL_TEAMS);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(allTeams);
			strBToS.append(END_TO_STRING_DELIMITER);
			strBToS.append(ALL_PLAYERS);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(allPlayers);
			strBToS.append(END_TO_STRING_DELIMITER);
		// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(uC28_StatesValidator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsUC28_StatesController!=null); 
		return strBToS.toString();
	}
}
