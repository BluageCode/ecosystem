/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.application.form.uc45_autocomplete ;


import org.apache.log4j.Logger;
import org.springframework.validation.Errors;

import com.netfective.bluage.core.validator.AbstractValidator;
/**
* Class :UC45_HomePageValidator
*/
public class UC45_HomePageValidator extends AbstractValidator{
	
	// LOGGER for the class UC45_HomePageValidator
	private static final Logger LOGGER = Logger.getLogger( UC45_HomePageValidator.class);
	
	
	/**
	* Operation validate for UC45_HomePageForm
	* @param obj : the current form (UC45_HomePageForm)
	* @param errors : The spring errors to return for the form UC45_HomePageForm
	*/
	public void validate(final Object obj,final Errors errors){
		LOGGER.info("Beginning method : validate"); // UC45_HomePageValidator
		UC45_HomePageForm cUC45_HomePageForm = (UC45_HomePageForm)obj; // UC45_HomePageValidator
		LOGGER.info("Ending method : validate the form "+ cUC45_HomePageForm.getClass().getName()); // UC45_HomePageValidator
	}

	/**
	* Method to implements to use spring validators (UC45_HomePageForm)
	* @param aClass : Class for the form UC45_HomePageForm
	* @return boolean
	*/
	public boolean supports(final Class aClass){
		return (new UC45_HomePageForm()).getClass().equals(aClass); // UC45_HomePageValidator
	}
}
