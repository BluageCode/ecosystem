/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.netfective.bluage.core.exception;

import java.io.Serializable;

import org.apache.commons.lang.builder.EqualsBuilder;

/**
 * Cette classe represente les erreurs de donnees dans l application. elle
 * contient tous les champs qui permettent de fabriquer la cle du message
 * d erreur qui doit suivre la norme suivante:
 * sourceScreenId.error.controlPattern.boName.propertyName
 * 
 * <DL>
 * <DT><B>Nom du Projet: </B>
 * <DD>Bluage</DD>
 * <DT><B>Service: </B>
 * 
 */
public class BusinessError implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// Le nom de l objet metier controle
	private final String boName;
	// Le nom de la propriete pour laquelle le controle a echoue
	private final String propertyName;
	// Le type du controle effectue
	private final String controlPattern;
	// Le contexte utilise pour executer les controles
	private final String controlContext;
	// L'ecran source de l'erreur
	private final String sourceScreenId;

	/**
	 * Construit une BusinessError
	 * 
	 * @param boName
	 *            Le nom de l?objet metier controle
	 * @param propertyName
	 *            Le nom de la propriete pour laquelle le controle a echoue
	 * @param controlPattern
	 *            Le type du controle effectue
	 * @param controlContext
	 *            Le contexte utilise pour executer les controles
	 * @param sourceScreenId
	 *            L'ecran source de l'erreur
	 */
	public BusinessError(final String boName, final String propertyName,
			final String controlPattern, final String controlContext, final String sourceScreenId) {
		this.boName = boName;
		this.controlContext = controlContext;
		this.controlPattern = controlPattern;
		this.propertyName = propertyName;
		this.sourceScreenId = sourceScreenId;
	}

	/**
	 * default constructor for BusinessError
	 */
	public BusinessError() {
		sourceScreenId = "";
		propertyName = "";
		controlPattern = "";
		controlContext = "";
		boName = "";
	}
	/**
	 * @return boName Le nom de l?objet metier controle
	 */
	public String getBoName() {
		return boName;
	}

	/**
	 * @return controlContext Le contexte utilise pour executer les controles
	 */
	public String getControlContext() {
		return controlContext;
	}

	/**
	 * @return controlPattern Le type du controle effectue
	 */
	public String getControlPattern() {
		return controlPattern;
	}

	/**
	 * @return propertyName Le nom de la propriete pour laquelle le controle a
	 *         echoue
	 */
	public String getPropertyName() {
		return propertyName;
	}

	/**
	 * @return sourceScreenId L id de l ecran source de l erreur
	 */
	public String getSourceScreenId() {
		return sourceScreenId;
	}

	@Override
	public int hashCode() {
		return System.identityHashCode(this);
	}

	/** equals method for BusinessError
	 * 
	 * @param object Object
	 * @return boolean
	 */
	public boolean equals(final Object object) {
		if (this == object) {
			return true;
		}
		if (!(object instanceof BusinessError)) {
			return false;
		}
		final BusinessError other = (BusinessError) object;

		return new EqualsBuilder()
		.append(this.getBoName(), other.getBoName())
		.append(this.getControlContext(), other.getControlContext())
		.append(this.getControlPattern(), other.getControlPattern())
		.append(this.getPropertyName(), other.getPropertyName())
		.append(this.getSourceScreenId(), other.getSourceScreenId()).isEquals();
	}
	
}

