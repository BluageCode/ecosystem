/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.application.datagrid.uc37_collectionorder ;


import org.apache.log4j.Logger;
import org.springframework.validation.Errors;

import com.netfective.bluage.core.validator.AbstractValidator;
/**
* Class :UC37_TeamsValidator
*/
public class UC37_TeamsValidator extends AbstractValidator{
	
	// LOGGER for the class UC37_TeamsValidator
	private static final Logger LOGGER = Logger.getLogger( UC37_TeamsValidator.class);
	
	
	/**
	* Operation validate for UC37_TeamsForm
	* @param obj : the current form (UC37_TeamsForm)
	* @param errors : The spring errors to return for the form UC37_TeamsForm
	*/
	public void validate(final Object obj,final Errors errors){
		LOGGER.info("Beginning method : validate"); // UC37_TeamsValidator
		UC37_TeamsForm cUC37_TeamsForm = (UC37_TeamsForm)obj; // UC37_TeamsValidator
		LOGGER.info("Ending method : validate the form "+ cUC37_TeamsForm.getClass().getName()); // UC37_TeamsValidator
	}

	/**
	* Method to implements to use spring validators (UC37_TeamsForm)
	* @param aClass : Class for the form UC37_TeamsForm
	* @return boolean
	*/
	public boolean supports(final Class aClass){
		return (new UC37_TeamsForm()).getClass().equals(aClass); // UC37_TeamsValidator
	}
}
