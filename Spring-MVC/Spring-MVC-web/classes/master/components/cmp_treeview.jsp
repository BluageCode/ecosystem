<%@ include file="/include.jsp"  %>
	<form:form id="cmp_treeview" commandName="cmp_treeviewForm"  enctype="application/x-www-form-urlencoded" method="post">

    <div id="rd_jtree_key">
     <p align="center"><b>Search:</b>&nbsp;&nbsp;<input type="text" id="search" style="width:200;"/></p>
        <p id="collapse" ><a href="#">Collapse All</a> | <a href="#">Expand All</a></p>
    </div>
    <ul id="rd_jtree">
     <li><a href="#" onclick="clickOn('lnk_home');"><strong>Home</strong></a></li>
        <li><a href="#"><strong>Getting started</strong></a>
            <ul>
                <li><a href="#" onclick="clickOn('howto_helloworld');">UC01 - Hello World</a></li>
                <li><a href="#" onclick="clickOn('howto_entitiescreation');">UC02 - Entities Creation</a></li>
                <li><a href="#" onclick="clickOn('howto_validationform');">UC03 - Form Validation</a></li>
                <li><a href="#" onclick="clickOn('howto_entitiesmodification');">UC04 - Entities Modification</a></li>
                <li><a href="#" onclick="clickOn('howto_search');">UC05 - Search</a></li>
            </ul>
        </li>
        <li><a href="#"><strong>Standard Features</strong></a>
            <ul>
                <li><a href="#"><strong>Application</strong></a>
                    <ul>
                        <li><a href="#"><strong>Navigation</strong></a>
                            <ul>
                                <li><a href="#"  onclick="clickOn('howto_package');">UC28 - Package Scope</a></li>
                                <li><a href="#"  onclick="clickOn('howto_global');">UC29 - Global Scope</a></li>
                                <li><a href="#"  onclick="clickOn('howto_navigation');">UC30 - Navigation</a></li>
                                <li><a href="#"  onclick="clickOn('howto_preaction');">UC31 - PreAction</a></li>
                            </ul>
                        </li>
                    </ul>
                    <ul>
                        <li><a href="#"><strong>Form</strong></a>
                            <ul>
                                <li><a href="#" onclick="clickOn('howto_captcha');" >UC43 - Captcha</a></li>
                            </ul>
                        </li>
                    </ul>
                    <ul>
                        <li><a href="#"><strong>Datagrid</strong></a>
                            <ul>
                             <li><a href="#" onclick="clickOn('howto_calculabletable');" >UC32 - Calculable Table</a></li>
                                <li><a href="#" onclick="clickOn('howto_edittable');" >UC33 - Editable Table</a></li>                    
                                <li><a href="#" onclick="clickOn('howto_paginationTable');" >UC34 - Pagination Table</a></li>
                                <li><a href="#" onclick="clickOn('howto_paginatorBDD');" >UC35 - Paginator BDD</a></li>
                                <li><a href="#" onclick="clickOn('howto_collectionorder');" >UC37 - Collection Order</a></li>
                             <li><a href="#" onclick="clickOn('howto_selectedList');" >UC38 - Selected List</a></li>
                             <li><a href="#" onclick="clickOn('howto_multiselectiontable');" >UC42 - MultiSelection Table</a></li>
                            </ul>
                        </li>
                    </ul>
                    <ul>
                        <li><a href="#"><strong>Repeater</strong></a>
                            <ul>
                          <li><a href="#" onclick="clickOn('howto_repeater');" >UC39 - Repeater</a></li>
                         </ul>
                        </li>
                    </ul>
                    <ul>
                        <li><a href="#"><strong>Form</strong></a>
                            <ul>
                                <li><a href="#" onclick="clickOn('howto_dropdown');" >UC41 - Action on drop down selection</a></li>
                                <!-- li><a href="#" onclick="clickOn('howto_captcha');" >UC43 - Captcha</a></li -->
                                <li><a href="#" onclick="clickOn('howto_autotable');" >UC44 - Auto Complete Table</a></li>
                                <li><a href="#" onclick="clickOn('howto_autocomplete');" >UC45 - Auto Complete</a></li>
                            </ul>                       
                        </li>
                    </ul>
                    <ul>
                        <li><a href="#"><strong>Component</strong></a>
                            <ul>
                                <li><a href="#" onclick="clickOn('howto_menu');">UC46 - Menu</a></li>
                                <li><a href="#" onclick="clickOn('howto_contents');">UC47 - Contents</a></li>
                            </ul>
                        </li>
                    </ul>
                   
                </li>
            </ul>
            <ul>
                <li><a href="#"><strong>Services</strong></a>
                    <ul>
                     <li><a href="#"><strong>BAGS</strong></a>
                            <ul>
                             <li><a href="#" onclick="clickOn('howto_bags');">UC49 - BAGS</a></li>
                            </ul>
                        </li>
                     <li><a href="#"><strong>Ajax</strong></a>
                            <ul>
                             <li><a href="#" onclick="clickOn('howto_ajaxcall');">UC50 - Ajax Call</a></li>
                            </ul>
                        </li>
                        <li><a href="#"><strong>Process</strong></a>
                            <ul>
                                <li><a href="#" onclick="clickOn('howto_conditional');">UC52 - Conditional</a></li>
                                <li><a href="#" onclick="clickOn('howto_while');">UC53 - While</a></li>
                                <li><a href="#" onclick="clickOn('howto_for');">UC54 - For</a></li>
                            </ul>
                        </li>
                        
                        <li><a href="#"><strong>SQL</strong></a>
                            <ul>
                                <li><a href="#" onclick="clickOn('howto_sqloperation');">UC56 - SQL Operation</a></li>
                            </ul>
                        </li>
                        <li><a href="#"><strong>HQL</strong></a>
                            <ul>
                                <li><a href="#" onclick="clickOn('howto_hqloperation');">UC57 - HQL Operation</a></li>
                            </ul>
                        </li>
                        <li><a href="#"><strong>Standard</strong></a>
                            <ul>
                                <li><a href="#" onclick="clickOn('howto_copy');">UC58 - Service Copy</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
            </ul>
            <ul>
                <li><a href="#"><strong>Entities</strong></a>
                    <ul>
                     <li><a href="#" onclick="clickOn('howto_defaultsort');">UC40 - Default Sort</a></li>
                        <li><a href="#" onclick="clickOn('howto_umltosqlmapping');">UC59 - UML to SQL Mapping</a></li>
                        <li><a href="#" onclick="clickOn('howto_multidomaine');">UC60 - MultiDomain</a></li>
                        <li><a href="#" onclick="clickOn('howto_transientobject');">UC61 - Transient Object</a></li>
                        <li><a href="#" onclick="clickOn('howto_association');">UC63 - Association</a></li>
                        <li><a href="#" onclick="clickOn('howto_cascade');">UC64 - Cascade</a></li>
                        <li><a href="#" onclick="clickOn('howto_lazy');">UC65 - Lazy</a></li>
                        <li><a href="#" onclick="clickOn('howto_heritagesubclass');">UC68 - Inheritance subclass</a></li>
                     <li><a href="#" onclick="clickOn('howto_cachehibernate');">UC69 - Hibernate Cache</a></li>
                     <li><a href="#" onclick="clickOn('howto_generator');">UC72 - Generator Class</a></li> 
                    </ul>
                </li>
            </ul>
            <ul>
                <li><a href="#"><strong>Exception</strong></a>
                    <ul>
                        <li><a href="#" onclick="clickOn('howto_exception');">UC73 - Exception</a></li>
                    </ul>
                </li>
            </ul>
           
        </li>
        <li><a href="#"><strong>Advanced Features</strong></a>
            <ul>
                <li><a href="#"><strong>Performance</strong></a>
                    <ul>
                        <li><a href="#" onclick="clickOn('howto_datagridloading');">UC200 - Data loading (N+1 select)</a></li>
                    </ul>
                </li>
            </ul>
            <ul>
                <li><a href="#"><strong>Advanced Tags Use</strong></a>
                    <ul>
                        <li><a href="#" onclick="clickOn('howto_popup');">UC205 - Pop up</a></li>
                        
                    </ul>
                </li>
            </ul>
            
             <ul>
                <li><a href="#"><strong>Value Object</strong></a>
                    <ul>
                        <li><a href="#" onclick="clickOn('howto_valueobject');">UC206 - Value Object</a></li>
                    </ul>
                </li>
            </ul>
            <ul>
                <li><a href="#"><strong>External Tools</strong></a>
                    <ul>
                     <li><a href="#" onclick="clickOn('howto_javamelody');">UC209 - JavaMelody</a></li>
                        <li><a href="#" onclick="clickOn('howto_sonar');">UC210 - Sonar</a></li>
                    </ul>
                </li>
            </ul>
            <ul>
                <li><a href="#"><strong>Filters</strong></a>
                    <ul>
                     <li><a href="#" onclick="clickOn('howto_timeout');">UC220 - Time Out</a></li>
                    </ul>
                </li>
            </ul>
         
        </li>
         <li><a href="#"><strong>QA UC</strong></a>
            <ul>
                <li><a href="#" onclick="clickOn('howto_tables');">UC1001 - Tables</a></li>
                 <li><a href="#" onclick="clickOn('howto_validators');">UC1002 - Validators</a></li>
            </ul>
        </li>
    </ul>
    
<div style="visibility: hidden"  > 
<a id="lnk_home"onclick="javascript:btn_click_cExecutecmp_treeview(this.href);return false;"				href="<c:url value="/master/components/cmp_treeview/lnk_home.html"/>"
							 shape="rect"  >									 
</a>
<a id="howto_helloworld"onclick="javascript:btn_click_cExecutecmp_treeview(this.href);return false;"				href="<c:url value="/master/components/cmp_treeview/howto_helloworld.html"/>"
							 shape="rect"  >									 
</a>
<a id="howto_entitiescreation"onclick="javascript:btn_click_cExecutecmp_treeview(this.href);return false;"				href="<c:url value="/master/components/cmp_treeview/howto_entitiescreation.html"/>"
							 shape="rect"  >									 
</a>
<a id="howto_validationform"onclick="javascript:btn_click_cExecutecmp_treeview(this.href);return false;"				href="<c:url value="/master/components/cmp_treeview/howto_validationform.html"/>"
							 shape="rect"  >									 
</a>
<a id="howto_entitiesmodification"onclick="javascript:btn_click_cExecutecmp_treeview(this.href);return false;"				href="<c:url value="/master/components/cmp_treeview/howto_entitiesmodification.html"/>"
							 shape="rect"  >									 
</a>
<a id="howto_search"onclick="javascript:btn_click_cExecutecmp_treeview(this.href);return false;"				href="<c:url value="/master/components/cmp_treeview/howto_search.html"/>"
							 shape="rect"  >									 
</a>
<a id="howto_navigation"onclick="javascript:btn_click_cExecutecmp_treeview(this.href);return false;"				href="<c:url value="/master/components/cmp_treeview/howto_navigation.html"/>"
							 shape="rect"  >									 
</a>
<a id="howto_edittable"onclick="javascript:btn_click_cExecutecmp_treeview(this.href);return false;"				href="<c:url value="/master/components/cmp_treeview/howto_edittable.html"/>"
							 shape="rect"  >									 
</a>
<a id="howto_paginationTable"onclick="javascript:btn_click_cExecutecmp_treeview(this.href);return false;"				href="<c:url value="/master/components/cmp_treeview/howto_paginationTable.html"/>"
							 shape="rect"  >									 
</a>
<a id="howto_paginatorBDD"onclick="javascript:btn_click_cExecutecmp_treeview(this.href);return false;"				href="<c:url value="/master/components/cmp_treeview/howto_paginatorBDD.html"/>"
							 shape="rect"  >									 
</a>
<a id="howto_paginationFast"onclick="javascript:btn_click_cExecutecmp_treeview(this.href);return false;"				href="<c:url value="/master/components/cmp_treeview/howto_paginationFast.html"/>"
							 shape="rect"  >									 
</a>
<a id="howto_collectionorder"onclick="javascript:btn_click_cExecutecmp_treeview(this.href);return false;"				href="<c:url value="/master/components/cmp_treeview/howto_collectionorder.html"/>"
							 shape="rect"  >									 
</a>
<a id="howto_repeater"onclick="javascript:btn_click_cExecutecmp_treeview(this.href);return false;"				href="<c:url value="/master/components/cmp_treeview/howto_repeater.html"/>"
							 shape="rect"  >									 
</a>
<a id="howto_dropdown"onclick="javascript:btn_click_cExecutecmp_treeview(this.href);return false;"				href="<c:url value="/master/components/cmp_treeview/howto_dropdown.html"/>"
							 shape="rect"  >									 
</a>
<a id="howto_autocomplete"onclick="javascript:btn_click_cExecutecmp_treeview(this.href);return false;"				href="<c:url value="/master/components/cmp_treeview/howto_autocomplete.html"/>"
							 shape="rect"  >									 
</a>
<a id="howto_menu"onclick="javascript:btn_click_cExecutecmp_treeview(this.href);return false;"				href="<c:url value="/master/components/cmp_treeview/howto_menu.html"/>"
							 shape="rect"  >									 
</a>
<a id="howto_defaultsort"onclick="javascript:btn_click_cExecutecmp_treeview(this.href);return false;"				href="<c:url value="/master/components/cmp_treeview/howto_defaultsort.html"/>"
							 shape="rect"  >									 
</a>
<a id="howto_conditional"onclick="javascript:btn_click_cExecutecmp_treeview(this.href);return false;"				href="<c:url value="/master/components/cmp_treeview/howto_conditional.html"/>"
							 shape="rect"  >									 
</a>
<a id="howto_while"onclick="javascript:btn_click_cExecutecmp_treeview(this.href);return false;"				href="<c:url value="/master/components/cmp_treeview/howto_while.html"/>"
							 shape="rect"  >									 
</a>
<a id="howto_for"onclick="javascript:btn_click_cExecutecmp_treeview(this.href);return false;"				href="<c:url value="/master/components/cmp_treeview/howto_for.html"/>"
							 shape="rect"  >									 
</a>
<a id="howto_heritagesubclass"onclick="javascript:btn_click_cExecutecmp_treeview(this.href);return false;"				href="<c:url value="/master/components/cmp_treeview/howto_heritagesubclass.html"/>"
							 shape="rect"  >									 
</a>
<a id="howto_cachehibernate"onclick="javascript:btn_click_cExecutecmp_treeview(this.href);return false;"				href="<c:url value="/master/components/cmp_treeview/howto_cachehibernate.html"/>"
							 shape="rect"  >									 
</a>
<a id="howto_umltosqlmapping"onclick="javascript:btn_click_cExecutecmp_treeview(this.href);return false;"				href="<c:url value="/master/components/cmp_treeview/howto_umltosqlmapping.html"/>"
							 shape="rect"  >									 
</a>
<a id="howto_crudfromscratch"onclick="javascript:btn_click_cExecutecmp_treeview(this.href);return false;"				href="<c:url value="/master/components/cmp_treeview/howto_crudfromscratch.html"/>"
							 shape="rect"  >									 
</a>
<a id="howto_exception"onclick="javascript:btn_click_cExecutecmp_treeview(this.href);return false;"				href="<c:url value="/master/components/cmp_treeview/howto_exception.html"/>"
							 shape="rect"  >									 
</a>
<a id="howto_datagridloading"onclick="javascript:btn_click_cExecutecmp_treeview(this.href);return false;"				href="<c:url value="/master/components/cmp_treeview/howto_datagridloading.html"/>"
							 shape="rect"  >									 
</a>
<a id="howto_preaction"onclick="javascript:btn_click_cExecutecmp_treeview(this.href);return false;"				href="<c:url value="/master/components/cmp_treeview/howto_preaction.html"/>"
							 shape="rect"  >									 
</a>
<a id="howto_sqloperation"onclick="javascript:btn_click_cExecutecmp_treeview(this.href);return false;"				href="<c:url value="/master/components/cmp_treeview/howto_sqloperation.html"/>"
							 shape="rect"  >									 
</a>
<a id="howto_hqloperation"onclick="javascript:btn_click_cExecutecmp_treeview(this.href);return false;"				href="<c:url value="/master/components/cmp_treeview/howto_hqloperation.html"/>"
							 shape="rect"  >									 
</a>
<a id="howto_association"onclick="javascript:btn_click_cExecutecmp_treeview(this.href);return false;"				href="<c:url value="/master/components/cmp_treeview/howto_association.html"/>"
							 shape="rect"  >									 
</a>
<a id="howto_cascade"onclick="javascript:btn_click_cExecutecmp_treeview(this.href);return false;"				href="<c:url value="/master/components/cmp_treeview/howto_cascade.html"/>"
							 shape="rect"  >									 
</a>
<a id="howto_lazy"onclick="javascript:btn_click_cExecutecmp_treeview(this.href);return false;"				href="<c:url value="/master/components/cmp_treeview/howto_lazy.html"/>"
							 shape="rect"  >									 
</a>
<a id="howto_transientobject"onclick="javascript:btn_click_cExecutecmp_treeview(this.href);return false;"				href="<c:url value="/master/components/cmp_treeview/howto_transientobject.html"/>"
							 shape="rect"  >									 
</a>
<a id="howto_multidomaine"onclick="javascript:btn_click_cExecutecmp_treeview(this.href);return false;"				href="<c:url value="/master/components/cmp_treeview/howto_multidomaine.html"/>"
							 shape="rect"  >									 
</a>
<a id="howto_copy"onclick="javascript:btn_click_cExecutecmp_treeview(this.href);return false;"				href="<c:url value="/master/components/cmp_treeview/howto_copy.html"/>"
							 shape="rect"  >									 
</a>
<a id="howto_javamelody"onclick="javascript:btn_click_cExecutecmp_treeview(this.href);return false;"				href="<c:url value="/master/components/cmp_treeview/howto_javamelody.html"/>"
							 shape="rect"  >									 
</a>
<a id="howto_sonar"onclick="javascript:btn_click_cExecutecmp_treeview(this.href);return false;"				href="<c:url value="/master/components/cmp_treeview/howto_sonar.html"/>"
							 shape="rect"  >									 
</a>
<a id="howto_calculabletable"onclick="javascript:btn_click_cExecutecmp_treeview(this.href);return false;"				href="<c:url value="/master/components/cmp_treeview/howto_calculabletable.html"/>"
							 shape="rect"  >									 
</a>
<a id="howto_selectedList"onclick="javascript:btn_click_cExecutecmp_treeview(this.href);return false;"				href="<c:url value="/master/components/cmp_treeview/howto_selectedList.html"/>"
							 shape="rect"  >									 
</a>
<a id="howto_contents"onclick="javascript:btn_click_cExecutecmp_treeview(this.href);return false;"				href="<c:url value="/master/components/cmp_treeview/howto_contents.html"/>"
							 shape="rect"  >									 
</a>
<a id="howto_ajaxcall"onclick="javascript:btn_click_cExecutecmp_treeview(this.href);return false;"				href="<c:url value="/master/components/cmp_treeview/howto_ajaxcall.html"/>"
							 shape="rect"  >									 
</a>
<a id="howto_autotable"onclick="javascript:btn_click_cExecutecmp_treeview(this.href);return false;"				href="<c:url value="/master/components/cmp_treeview/howto_autotable.html"/>"
							 shape="rect"  >									 
</a>
<a id="howto_package"onclick="javascript:btn_click_cExecutecmp_treeview(this.href);return false;"				href="<c:url value="/master/components/cmp_treeview/howto_package.html"/>"
							 shape="rect"  >									 
</a>
<a id="howto_global"onclick="javascript:btn_click_cExecutecmp_treeview(this.href);return false;"				href="<c:url value="/master/components/cmp_treeview/howto_global.html"/>"
							 shape="rect"  >									 
</a>
<a id="howto_popup"onclick="javascript:btn_click_cExecutecmp_treeview(this.href);return false;"				href="<c:url value="/master/components/cmp_treeview/howto_popup.html"/>"
							 shape="rect"  >									 
</a>
<a id="howto_generator"onclick="javascript:btn_click_cExecutecmp_treeview(this.href);return false;"				href="<c:url value="/master/components/cmp_treeview/howto_generator.html"/>"
							 shape="rect"  >									 
</a>
<a id="howto_valueobject"onclick="javascript:btn_click_cExecutecmp_treeview(this.href);return false;"				href="<c:url value="/master/components/cmp_treeview/howto_valueobject.html"/>"
							 shape="rect"  >									 
</a>
<a id="howto_bags"onclick="javascript:btn_click_cExecutecmp_treeview(this.href);return false;"				href="<c:url value="/master/components/cmp_treeview/howto_bags.html"/>"
							 shape="rect"  >									 
</a>
<a id="howto_timeout"onclick="javascript:btn_click_cExecutecmp_treeview(this.href);return false;"				href="<c:url value="/master/components/cmp_treeview/howto_timeout.html"/>"
							 shape="rect"  >									 
</a>
<a id="howto_tables"onclick="javascript:btn_click_cExecutecmp_treeview(this.href);return false;"				href="<c:url value="/master/components/cmp_treeview/howto_tables.html"/>"
							 shape="rect"  >									 
</a>
<a id="howto_validators"onclick="javascript:btn_click_cExecutecmp_treeview(this.href);return false;"				href="<c:url value="/master/components/cmp_treeview/howto_validators.html"/>"
							 shape="rect"  >									 
</a>
<a id="howto_captcha"onclick="javascript:btn_click_cExecutecmp_treeview(this.href);return false;"				href="<c:url value="/master/components/cmp_treeview/howto_captcha.html"/>"
							 shape="rect"  >									 
</a>
</div>
	</form:form>
	<script language="javascript">
		function btn_click_cExecutecmp_treeview(url) {
			cmp_treeviewvar = document.getElementById('cmp_treeview');
			cmp_treeviewvar.setAttribute('action', url);
			cmp_treeviewvar.submit();
		}
		function btn_click_cSetActioncmp_treeview(url) {
			cmp_treeviewvar = document.getElementById('cmp_treeview');
			cmp_treeviewvar.setAttribute('action', url);
		}
	</script>
