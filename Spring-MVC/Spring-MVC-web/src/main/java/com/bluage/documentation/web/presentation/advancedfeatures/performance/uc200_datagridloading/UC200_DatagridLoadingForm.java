/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.advancedfeatures.performance.uc200_datagridloading;


import java.io.Serializable;
import java.util.List;

import com.bluage.documentation.business.advancedfeatures.performance.uc200_datagridloading.bos.Player200BO;
import com.netfective.bluage.core.web.AbstractForm;
import com.netfective.bluage.core.web.IForm;

/**
* Class : UC200_DatagridLoadingForm
*/
public class UC200_DatagridLoadingForm extends AbstractForm implements Serializable, IForm {
	
	/** Constant : serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	//CONSTANT : players
	private static final String PLAYERS = "players";
	/**
	 * 	Property: players 
	 */
	private List<Player200BO> players;
/**
	 * Default constructor : UC200_DatagridLoadingForm
	 */
	public UC200_DatagridLoadingForm() {
		super();
		// Initialize : players
		this.players = new java.util.ArrayList<com.bluage.documentation.business.advancedfeatures.performance.uc200_datagridloading.bos.Player200BO>();
					this.setAlwaysCallPreControllers(true);
	}
	
	/**
	 * 	Getter : players 
	 * 	@return : Return the players instance.
	 */
	public List<Player200BO> getPlayers(){
		return players; // For UC200_DatagridLoadingForm
	}
	
	/**
	 * 	Setter : players 
	 *  @param playersinstance : The instance to set.
	 */
	public void setPlayers(final List<Player200BO> playersinstance){
		this.players = playersinstance;// For UC200_DatagridLoadingForm
	}
/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UC200_DatagridLoadingForm [ "+
PLAYERS +" = " + players + "]";
	}
	
	/**
	 * Method setInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public void setInstanceByInstanceName(final String instanceName, final Object instance) {
		// Set the instance Players.
		if(PLAYERS.equals(instanceName)){// For UC200_DatagridLoadingForm
			this.setPlayers((List<Player200BO>)instance); // For UC200_DatagridLoadingForm
		}
			}
	
	/**
	 * Method getInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public Object getInstanceByInstanceName(final String instanceName){
		// Temporary object for tmpUC200_DatagridLoadingForm.
		Object tmpUC200_DatagridLoadingForm = null;
		
			
		// Get the instance Players for UC200_DatagridLoadingForm.
		if(PLAYERS.equals(instanceName)){ // For UC200_DatagridLoadingForm
			tmpUC200_DatagridLoadingForm = this.getPlayers(); // For UC200_DatagridLoadingForm
		}
		return tmpUC200_DatagridLoadingForm;// For UC200_DatagridLoadingForm
	}
	
			}
