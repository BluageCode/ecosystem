/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.entities.uc65_lazy;


import java.io.Serializable;
import java.util.List;

import com.bluage.documentation.business.standardfeatures.entities.uc65_lazy.bos.Rookie65BO;
import com.bluage.documentation.business.standardfeatures.entities.uc65_lazy.bos.Team65BO;
import com.netfective.bluage.core.web.AbstractForm;
import com.netfective.bluage.core.web.IForm;

/**
* Class : UC65_DisplaywithoutFetchForm
*/
public class UC65_DisplaywithoutFetchForm extends AbstractForm implements Serializable, IForm {
	
	/** Constant : serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	//CONSTANT : teamLazyFalse
	private static final String TEAM_LAZY_FALSE = "teamLazyFalse";
	//CONSTANT : players
	private static final String PLAYERS = "players";
	/**
	 * 	Property: teamLazyFalse 
	 */
	private Team65BO teamLazyFalse;
	/**
	 * 	Property: players 
	 */
	private List<Rookie65BO> players;
/**
	 * Default constructor : UC65_DisplaywithoutFetchForm
	 */
	public UC65_DisplaywithoutFetchForm() {
		super();
		// Initialize : teamLazyFalse
		this.teamLazyFalse = new Team65BO();
		// Initialize : players
		this.players = new java.util.ArrayList<com.bluage.documentation.business.standardfeatures.entities.uc65_lazy.bos.Rookie65BO>();
					this.setAlwaysCallPreControllers(true);
	}
	
	/**
	 * 	Getter : teamLazyFalse 
	 * 	@return : Return the teamLazyFalse instance.
	 */
	public Team65BO getTeamLazyFalse(){
		return teamLazyFalse; // For UC65_DisplaywithoutFetchForm
	}
	
	/**
	 * 	Setter : teamLazyFalse 
	 *  @param teamLazyFalseinstance : The instance to set.
	 */
	public void setTeamLazyFalse(final Team65BO teamLazyFalseinstance){
		this.teamLazyFalse = teamLazyFalseinstance;// For UC65_DisplaywithoutFetchForm
	}
	/**
	 * 	Getter : players 
	 * 	@return : Return the players instance.
	 */
	public List<Rookie65BO> getPlayers(){
		return players; // For UC65_DisplaywithoutFetchForm
	}
	
	/**
	 * 	Setter : players 
	 *  @param playersinstance : The instance to set.
	 */
	public void setPlayers(final List<Rookie65BO> playersinstance){
		this.players = playersinstance;// For UC65_DisplaywithoutFetchForm
	}
/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UC65_DisplaywithoutFetchForm [ "+
TEAM_LAZY_FALSE +" = " + teamLazyFalse +PLAYERS +" = " + players + "]";
	}
	
	/**
	 * Method setInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public void setInstanceByInstanceName(final String instanceName, final Object instance) {
		// Set the instance TeamLazyFalse.
		if(TEAM_LAZY_FALSE.equals(instanceName)){// For UC65_DisplaywithoutFetchForm
			this.setTeamLazyFalse((Team65BO)instance); // For UC65_DisplaywithoutFetchForm
		}
				// Set the instance Players.
		if(PLAYERS.equals(instanceName)){// For UC65_DisplaywithoutFetchForm
			this.setPlayers((List<Rookie65BO>)instance); // For UC65_DisplaywithoutFetchForm
		}
				// Parent instance name for UC65_DisplaywithoutFetchForm
		if(TEAM_LAZY_FALSE.equals(instanceName) && teamLazyFalse != null){ // For UC65_DisplaywithoutFetchForm
			players.clear(); // For UC65_DisplaywithoutFetchForm
			players.addAll(teamLazyFalse.getRookies());// For UC65_DisplaywithoutFetchForm
		}
		
	}
	
	/**
	 * Method getInstanceByInstanceName
	 * @param instanceName : the instance name.
	 * @param instance : the instance
	 * @see com.bluage.documentation.web.IForm#getInstanceByInstanceName(java.lang.String)
	 */
	public Object getInstanceByInstanceName(final String instanceName){
		// Temporary object for tmpUC65_DisplaywithoutFetchForm.
		Object tmpUC65_DisplaywithoutFetchForm = null;
		
			
		// Get the instance TeamLazyFalse for UC65_DisplaywithoutFetchForm.
		if(TEAM_LAZY_FALSE.equals(instanceName)){ // For UC65_DisplaywithoutFetchForm
			tmpUC65_DisplaywithoutFetchForm = this.getTeamLazyFalse(); // For UC65_DisplaywithoutFetchForm
		}
			
		// Get the instance Players for UC65_DisplaywithoutFetchForm.
		if(PLAYERS.equals(instanceName)){ // For UC65_DisplaywithoutFetchForm
			tmpUC65_DisplaywithoutFetchForm = this.getPlayers(); // For UC65_DisplaywithoutFetchForm
		}
		return tmpUC65_DisplaywithoutFetchForm;// For UC65_DisplaywithoutFetchForm
	}
	
			}
