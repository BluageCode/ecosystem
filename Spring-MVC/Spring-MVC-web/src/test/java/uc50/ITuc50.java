package uc50;

import org.junit.Test;
import test.common.AbstractBADocTestCase;

public class ITuc50 extends AbstractBADocTestCase {
	
	@Override
	public  String getName(){
		return "UC-50";
	}

	@Test
	public void testITuc50() throws Exception {
		// On teste l'UC50 - Ajax Call
		selenium.open("");
		selenium.click("link=Expand All");
		selenium.waitForPageToLoad("30000");
		selenium.click("link=UC50 - Ajax Call");
		verifyTrue(selenium.isElementPresent("//h2[contains(text(),\"Standard Features > Services > Ajax > UC50 > Ajax Call\")]"));
		assertTrue(selenium.isElementPresent("//table//thead//tr[th/div[contains(text(),\"First Name\")]  and th/div[contains(text(),\"Last Name\")] and th/div[contains(text(),\"Date Of Birth\")] and th/div[contains(text(),\"Height (cm)\")] and th/div[contains(text(),\"Weight (kg)\")] and th/div[contains(text(),\"Estimated Value (M$)\")] and th/div[contains(text(),\"Current Team\")] and th/div[contains(text(),\"Rookie\")]]|//table//thead//tr[th[contains(text(),\"First Name\")]  and th[contains(text(),\"Last Name\")] and th[contains(text(),\"Date Of Birth\")] and th[contains(text(),\"Height (cm)\")] and th[contains(text(),\"Weight (kg)\")] and th[contains(text(),\"Estimated Value (M$)\")] and th[contains(text(),\"Current Team\")] and th[contains(text(),\"Rookie\")]]"));
		// On teste les Tigers non rookies.
		selenium.select("//*[contains(@id,'sel_team')]", "label=Tigers");
		selenium.type("//*[contains(@id,'txt_minimum_value')]", "");
		selenium.click("//*[contains(@id,'lnk_value')]");
		selenium.waitForPageToLoad("30000");
		verifyTrue(selenium.isElementPresent("Error : Invalid argument(s) to tag FormatValidator"));
		selenium.select("//*[contains(@id,'sel_team')]", "label=Tigers");
		selenium.type("//*[contains(@id,'txt_minimum_value')]", "0");
		selenium.click("//*[contains(@id,'lnk_value')]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("2", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Oliver\")]  and td//span[contains(text(),\"Frank\")] and td//span[contains(text(),\"10/31/1968\")] and td//span[contains(text(),\"116\")] and td//span[contains(text(),\"228\")] and td//span[contains(text(),\"0.11\")] and td//span[contains(text(),\"Tigers\")] and td//input[not(@checked)]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Earl\")] and td//span[contains(text(),\"Russel\")] and td//span[contains(text(),\"12/15/1981\")] and td//span[contains(text(),\"228\")] and td//span[contains(text(),\"116\")] and td//span[contains(text(),\"29.3\")]and td//span[contains(text(),\"Tigers\")] and td//input[not(@checked)]]"));
		// Tigers rookies.
		selenium.click("//*[contains(@id,'chk_rookie')]");
		selenium.click("//*[contains(@id,'lnk_value')]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("1", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Nick\")] and td//span[contains(text(),\"Garner\")] and td//span[contains(text(),\"07/27/1992\")] and td//span[contains(text(),\"228\")] and td//span[contains(text(),\"116\")] and td//span[contains(text(),\"44.73\")]and td//span[contains(text(),\"Tigers\")] and td//input[@checked]]"));
		// On teste les Bears non rookies.
		selenium.select("//*[contains(@id,'sel_team')]", "label=Bears");
		selenium.click("//*[contains(@id,'chk_rookie')]");
		selenium.click("//*[contains(@id,'lnk_value')]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("1", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Elbert\")] and td//span[contains(text(),\"Cole\")] and td//span[contains(text(),\"04/07/1965\")] and td//span[contains(text(),\"171\")] and td//span[contains(text(),\"100\")] and td//span[contains(text(),\"3.95\")] and td//span[contains(text(),\"Bears\")] and td//input[not(@checked)]]"));
		// Bears rookies.
		selenium.click("//*[contains(@id,'chk_rookie')]");
		selenium.click("//*[contains(@id,'lnk_value')]");
		selenium.waitForPageToLoad("30000");
//		String url = selenium.getLocation();
//		String count = selenium.getEval("(storedVars['url'].indexOf('Struts') != -1) ? 1 : 0");
		verifyEquals(0, selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		// On teste les Chargers non rookies.
		selenium.select("//*[contains(@id,'sel_team')]", "label=Chargers");
		selenium.click("//*[contains(@id,'chk_rookie')]");
		selenium.click("//*[contains(@id,'lnk_value')]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("1", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Abel\")] and td//span[contains(text(),\"Dennis\")] and td//span[contains(text(),\"04/16/1980\")] and td//span[contains(text(),\"208\")] and td//span[contains(text(),\"105\")] and td//span[contains(text(),\"30.51\")] and td//span[contains(text(),\"Chargers\")] and td//input[not(@checked)]]"));
		// Chargers rookies.
		selenium.click("//*[contains(@id,'chk_rookie')]");
		selenium.click("//*[contains(@id,'lnk_value')]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("1", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Tommy\")] and td//span[contains(text(),\"Wood\")] and td//span[contains(text(),\"01/20/1992\")] and td//span[contains(text(),\"173\")] and td//span[contains(text(),\"77\")] and td//span[contains(text(),\"13.93\")] and td//span[contains(text(),\"Chargers\")] and td//input[@checked]]"));
		// On teste les Hawks non rookies.
		selenium.select("//*[contains(@id,'sel_team')]", "label=Hawks");
		selenium.click("//*[contains(@id,'chk_rookie')]");
		selenium.click("//*[contains(@id,'lnk_value')]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("2", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Howard\")] and td//span[contains(text(),\"Ramos\")] and td//span[contains(text(),\"08/25/1970\")] and td//span[contains(text(),\"228\")] and td//span[contains(text(),\"116\")] and td//span[contains(text(),\"47.84\")] and td//span[contains(text(),\"Hawks\")] and td//input[not(@checked)]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Sylvester\")] and td//span[contains(text(),\"Nash\")] and td//span[contains(text(),\"07/10/1965\")] and td//span[contains(text(),\"184\")] and td//span[contains(text(),\"68\")] and td//span[contains(text(),\"37.73\")] and td//span[contains(text(),\"Hawks\")] and td//input[not(@checked)]]"));
		// Hawks rookies.
		selenium.click("//*[contains(@id,'chk_rookie')]");
		selenium.click("//*[contains(@id,'lnk_value')]");
		selenium.waitForPageToLoad("30000");
//		url = selenium.getLocation();
//		 count = selenium.getEval("(storedVars['url'].indexOf('Struts') != -1) ? 1 : 0");
		verifyEquals(0, selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		// On teste les Braves non rookies.
		selenium.select("//*[contains(@id,'sel_team')]", "label=Braves");
		selenium.click("//*[contains(@id,'chk_rookie')]");
		selenium.click("//*[contains(@id,'lnk_value')]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("2", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Lester\")] and td//span[contains(text(),\"Cross\")] and td//span[contains(text(),\"02/21/1976\")] and td//span[contains(text(),\"214\")] and td//span[contains(text(),\"69\")] and td//span[contains(text(),\"30.98\")] and td//span[contains(text(),\"Braves\")] and td//input[not(@checked)]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Michael\")] and td//span[contains(text(),\"Hodges\")] and td//span[contains(text(),\"04/01/1988\")] and td//span[contains(text(),\"158\")] and td//span[contains(text(),\"204\")] and td//span[contains(text(),\"25.35\")] and td//span[contains(text(),\"Braves\")] and td//input[not(@checked)]]"));
		// Braves rookies.
		selenium.click("//*[contains(@id,'chk_rookie')]");
		selenium.click("//*[contains(@id,'lnk_value')]");
		selenium.waitForPageToLoad("30000");
//		 url = selenium.getLocation();
//		 count = selenium.getEval("(storedVars['url'].indexOf('Struts') != -1) ? 1 : 0");
		verifyEquals(0, selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		// On teste les Eagles non rookies.
		selenium.select("//*[contains(@id,'sel_team')]", "label=Eagles");
		selenium.click("//*[contains(@id,'chk_rookie')]");
		selenium.click("//*[contains(@id,'lnk_value')]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("2", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Keith\")] and td//span[contains(text(),\"Barnett\")] and td//span[contains(text(),\"11/10/1976\")] and td//span[contains(text(),\"190\")] and td//span[contains(text(),\"66\")] and td//span[contains(text(),\"5.81\")] and td//span[contains(text(),\"Eagles\")] and td//input[not(@checked)]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Jerome\")] and td//span[contains(text(),\"Grant\")] and td//span[contains(text(),\"06/28/1982\")] and td//span[contains(text(),\"205\")] and td//span[contains(text(),\"105\")] and td//span[contains(text(),\"28.94\")] and td//span[contains(text(),\"Eagles\")] and td//input[not(@checked)]]"));
		// Eagles rookies.
		selenium.click("//*[contains(@id,'chk_rookie')]");
		selenium.click("//*[contains(@id,'lnk_value')]");
		selenium.waitForPageToLoad("30000");
//		url = selenium.getLocation();
//		 count = selenium.getEval("(storedVars['url'].indexOf('Struts') != -1) ? 1 : 0");
		verifyEquals(0, selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		// Pour les Cardinals non rookies.
		selenium.select("//*[contains(@id,'sel_team')]", "label=Cardinals");
		selenium.click("//*[contains(@id,'chk_rookie')]");
		selenium.click("//*[contains(@id,'lnk_value')]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("3", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Ruben\")] and td//span[contains(text(),\"Pope\")] and td//span[contains(text(),\"05/01/1985\")] and td//span[contains(text(),\"201\")] and td//span[contains(text(),\"86\")] and td//span[contains(text(),\"44.66\")] and td//span[contains(text(),\"Cardinals\")] and td//input[not(@checked)]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Ivan\")] and td//span[contains(text(),\"Tate\")] and td//span[contains(text(),\"02/08/1972\")] and td//span[contains(text(),\"175\")] and td//span[contains(text(),\"143\")] and td//span[contains(text(),\"48.49\")] and td//span[contains(text(),\"Cardinals\")] and td//input[not(@checked)]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Woodrow\")] and td//span[contains(text(),\"Carpenter\")] and td//span[contains(text(),\"08/08/1987\")] and td//span[contains(text(),\"224\")] and td//span[contains(text(),\"69\")] and td//span[contains(text(),\"41.75\")] and td//span[contains(text(),\"Cardinals\")] and td//input[not(@checked)]]"));
		// Cardinals rookies.
		selenium.click("//*[contains(@id,'chk_rookie')]");
		selenium.click("//*[contains(@id,'lnk_value')]");
		selenium.waitForPageToLoad("30000");
//		 url = selenium.getLocation();
//		 count = selenium.getEval("(storedVars['url'].indexOf('Struts') != -1) ? 1 : 0");
		verifyEquals(0, selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		// Pour les Red Devils non rookies.
		selenium.select("//*[contains(@id,'sel_team')]", "label=Red Devils");
		selenium.click("//*[contains(@id,'chk_rookie')]");
		selenium.click("//*[contains(@id,'lnk_value')]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("2", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Allan\")] and td//span[contains(text(),\"Meyer\")] and td//span[contains(text(),\"02/14/1969\")] and td//span[contains(text(),\"229\")] and td//span[contains(text(),\"114\")] and td//span[contains(text(),\"33.67\")] and td//span[contains(text(),\"Red Devils\")] and td//input[not(@checked)]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Roderick\")] and td//span[contains(text(),\"Robbins\")] and td//span[contains(text(),\"07/29/1975\")] and td//span[contains(text(),\"164\")] and td//span[contains(text(),\"115\")] and td//span[contains(text(),\"12.043\")] and td//span[contains(text(),\"Red Devils\")] and td//input[not(@checked)]]"));
		// Red Devils rookies.
		selenium.click("//*[contains(@id,'chk_rookie')]");
		selenium.click("//*[contains(@id,'lnk_value')]");
		selenium.waitForPageToLoad("30000");
//		 url = selenium.getLocation();
//		 count = selenium.getEval("(storedVars['url'].indexOf('Struts') != -1) ? 1 : 0");
		verifyEquals(0, selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		// Pour les Mustangs non rookies.
		selenium.select("//*[contains(@id,'sel_team')]", "label=Mustangs");
		selenium.click("//*[contains(@id,'chk_rookie')]");
		selenium.click("//*[contains(@id,'lnk_value')]");
		selenium.waitForPageToLoad("30000");
//		 url = selenium.getLocation();
//		 count = selenium.getEval("(storedVars['url'].indexOf('Struts') != -1) ? 1 : 0");
		verifyEquals(0, selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		// Mustangs rookies.
		selenium.click("//*[contains(@id,'chk_rookie')]");
		selenium.click("//*[contains(@id,'lnk_value')]");
		selenium.waitForPageToLoad("30000");
//		 url = selenium.getLocation();
//		 count = selenium.getEval("(storedVars['url'].indexOf('Struts') != -1) ? 1 : 0");
		verifyEquals(0, selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		// On teste les values.
		selenium.select("//*[contains(@id,'sel_team')]", "label=Cardinals");
		selenium.type("//*[contains(@id,'txt_minimum_value')]", "40");
		selenium.click("//*[contains(@id,'chk_rookie')]");
		selenium.click("//*[contains(@id,'lnk_value')]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("3", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Ruben\")] and td//span[contains(text(),\"Pope\")] and td//span[contains(text(),\"05/01/1985\")] and td//span[contains(text(),\"201\")] and td//span[contains(text(),\"86\")] and td//span[contains(text(),\"44.66\")] and td//span[contains(text(),\"Cardinals\")] and td//input[not(@checked)]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Ivan\")] and td//span[contains(text(),\"Tate\")] and td//span[contains(text(),\"02/08/1972\")] and td//span[contains(text(),\"175\")] and td//span[contains(text(),\"143\")] and td//span[contains(text(),\"48.49\")] and td//span[contains(text(),\"Cardinals\")] and td//input[not(@checked)]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Woodrow\")] and td//span[contains(text(),\"Carpenter\")] and td//span[contains(text(),\"08/08/1987\")] and td//span[contains(text(),\"224\")] and td//span[contains(text(),\"69\")] and td//span[contains(text(),\"41.75\")] and td//span[contains(text(),\"Cardinals\")] and td//input[not(@checked)]]"));
		selenium.type("//*[contains(@id,'txt_minimum_value')]", "42");
		selenium.click("//*[contains(@id,'lnk_value')]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("2", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Ruben\")] and td//span[contains(text(),\"Pope\")] and td//span[contains(text(),\"05/01/1985\")] and td//span[contains(text(),\"201\")] and td//span[contains(text(),\"86\")] and td//span[contains(text(),\"44.66\")] and td//span[contains(text(),\"Cardinals\")] and td//input[not(@checked)]]"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Ivan\")] and td//span[contains(text(),\"Tate\")] and td//span[contains(text(),\"02/08/1972\")] and td//span[contains(text(),\"175\")] and td//span[contains(text(),\"143\")] and td//span[contains(text(),\"48.49\")] and td//span[contains(text(),\"Cardinals\")] and td//input[not(@checked)]]"));
		selenium.type("//*[contains(@id,'txt_minimum_value')]", "45");
		selenium.click("//*[contains(@id,'lnk_value')]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("1", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Ivan\")] and td//span[contains(text(),\"Tate\")] and td//span[contains(text(),\"02/08/1972\")] and td//span[contains(text(),\"175\")] and td//span[contains(text(),\"143\")] and td//span[contains(text(),\"48.49\")] and td//span[contains(text(),\"Cardinals\")] and td//input[not(@checked)]]"));
		selenium.type("//*[contains(@id,'txt_minimum_value')]", "48.49");
		selenium.click("//*[contains(@id,'lnk_value')]");
		selenium.waitForPageToLoad("30000");
		verifyEquals("1", selenium.getXpathCount("//table[contains(@id,'tab_players')]//tbody//tr"));
		assertTrue(selenium.isElementPresent("//table/tbody//tr[td//span[contains(text(),\"Ivan\")] and td//span[contains(text(),\"Tate\")] and td//span[contains(text(),\"02/08/1972\")] and td//span[contains(text(),\"175\")] and td//span[contains(text(),\"143\")] and td//span[contains(text(),\"48.49\")] and td//span[contains(text(),\"Cardinals\")] and td//input[not(@checked)]]"));
		selenium.type("//*[contains(@id,'txt_minimum_value')]", "49");
		selenium.click("//*[contains(@id,'lnk_value')]");
		selenium.waitForPageToLoad("30000");
//		 url = selenium.getLocation();
//		 count = selenium.getEval("(storedVars['url'].indexOf('Struts') != -1) ? 1 : 0");
		verifyEquals(0, selenium.getXpathCount("//table[contains(@id,\"tab_players\")]//tbody//tr"));
	}

	
}
