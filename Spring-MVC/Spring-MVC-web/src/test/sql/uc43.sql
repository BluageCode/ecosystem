ALTER TABLE player43 DROP CONSTRAINT player43_position_fkc;

ALTER TABLE position43 DROP CONSTRAINT position43_pkey;

ALTER TABLE player43 DROP CONSTRAINT player43_pkey;

DROP TABLE position43;

DROP TABLE player43;

CREATE TABLE position43 (
		code VARCHAR(2) NOT NULL,
		version INT4 NOT NULL,
		name VARCHAR(20) NOT NULL
	);

CREATE TABLE player43 (
		id INT8 NOT NULL,
		version INT4 NOT NULL,
		first_name VARCHAR(30) NOT NULL,
		last_name VARCHAR(30) NOT NULL,
		date_of_birth DATE,
		rookie BOOL,
		position_fk VARCHAR(2) NOT NULL
	);
	
INSERT INTO position43 VALUES ('PG', 0, 'Point Guard');
INSERT INTO position43 VALUES ('SG', 0, 'Shooting Guard');
INSERT INTO position43 VALUES ('SF', 0, 'Small Forward');
INSERT INTO position43 VALUES ('PF', 0, 'Power Forward');
INSERT INTO position43 VALUES ('C', 0, 'Center');

INSERT INTO player43 VALUES (1, 0, 'Oliver', 'Frank', '1968-10-31', false,'PG');
INSERT INTO player43 VALUES (2, 0, 'Elbert', 'Cole', '1965-04-07', false,'SG');
INSERT INTO player43 VALUES (3, 0, 'Ricky', 'Dawson', '1990-03-03', true,'SF');	

ALTER TABLE position43 ADD CONSTRAINT position43_pkey PRIMARY KEY (code);

ALTER TABLE player43 ADD CONSTRAINT player43_pkey PRIMARY KEY (id);

ALTER TABLE player43 ADD CONSTRAINT player43_position_fkc FOREIGN KEY (position_fk)
	REFERENCES position43 (code);

