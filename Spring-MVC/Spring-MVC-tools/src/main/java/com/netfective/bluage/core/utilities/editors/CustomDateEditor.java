/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.netfective.bluage.core.utilities.editors;

import java.beans.PropertyEditorSupport;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.util.StringUtils;
/**
 * Custom Date Editor with format error handling
 * @author t.masson
 *
 */
public class CustomDateEditor extends PropertyEditorSupport {
	
	/**
     * LOGGER.
     */
    private static final  Logger LOGGER = Logger.getLogger(CustomDateEditor.class);
	

	private final DateFormat dateFormat;

	private final boolean allowEmpty;

	private final int exactDateLength;
	
	private final String errorMessage;
	
	/**
	 * Error handling
	 */
	private String validationError;



	/**
	 * Create a new CustomDateEditor instance, using the given DateFormat
	 * for parsing and rendering.
	 * <p>The "allowEmpty" parameter states if an empty String should
	 * be allowed for parsing, i.e. get interpreted as null value.
	 * Otherwise, an IllegalArgumentException gets thrown in that case.
	 * @param dateFormat DateFormat to use for parsing and rendering
	 * @param allowEmpty if empty strings should be allowed
	 */
	public CustomDateEditor(final DateFormat dateFormat, final boolean allowEmpty, final String errorMessage) {
		this.dateFormat = dateFormat;
		this.allowEmpty = allowEmpty;
		this.exactDateLength = -1;
		this.errorMessage = errorMessage;
	}

	/**
	 * Create a new CustomDateEditor instance, using the given DateFormat
	 * for parsing and rendering.
	 * <p>The "allowEmpty" parameter states if an empty String should
	 * be allowed for parsing, i.e. get interpreted as null value.
	 * Otherwise, an IllegalArgumentException gets thrown in that case.
	 * <p>The "exactDateLength" parameter states that IllegalArgumentException gets
	 * thrown if the String does not exactly match the length specified. This is useful
	 * because SimpleDateFormat does not enforce strict parsing of the year part,
	 * not even with <code>setLenient(false)</code>. Without an "exactDateLength"
	 * specified, the "01/01/05" would get parsed to "01/01/0005".
	 * @param dateFormat DateFormat to use for parsing and rendering
	 * @param allowEmpty if empty strings should be allowed
	 * @param exactDateLength the exact expected length of the date String
	 */
	public CustomDateEditor(final DateFormat dateFormat, final boolean allowEmpty, final int exactDateLength, final String errorMessage) {
		this.dateFormat = dateFormat;
		this.allowEmpty = allowEmpty;
		this.exactDateLength = exactDateLength;
		this.errorMessage = errorMessage;
	}

	public String getValidationError() {
		return validationError;
	}
	
	/**
	 * Parse the Date from the given text, using the specified DateFormat.
	 */
	@Override
	public void setAsText(final String text) throws IllegalArgumentException {
		//reset validation error marker
		this.validationError = null;
		if (this.allowEmpty && !StringUtils.hasText(text)) {
			// Treat empty String as null value.
			setValue(null);
		}
		else if (text != null && this.exactDateLength >= 0 && text.length() != this.exactDateLength) {
			this.validationError = this.errorMessage;
			throw new IllegalArgumentException(this.errorMessage);
		}
		else {
			try {
				this.dateFormat.setLenient(false);
				final Date d = this.dateFormat.parse(text);
				final String dtext = this.dateFormat.format(d);
				//roundtrip ensures validity on month and day values
				if(text.equals(dtext)){
					setValue(this.dateFormat.parse(text));
				} else {
					throw new IllegalArgumentException(this.errorMessage);
				}
			}
			catch (ParseException ex) {
				throw new IllegalArgumentException(this.errorMessage,ex);
			}
		}
	}
	
	/**
	 * Format the Date as String, using the specified DateFormat.
	 */
	@Override
	public String getAsText() {
		final Date value = (Date) getValue();
		if(value != null){
			return this.dateFormat.format(value);
		}else{
			return "";
		}
	}

}
