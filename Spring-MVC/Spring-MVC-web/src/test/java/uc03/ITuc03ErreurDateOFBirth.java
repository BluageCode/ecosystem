package uc03;

import org.junit.Test;
import test.common.AbstractBADocTestCase;

public class ITuc03ErreurDateOFBirth extends AbstractBADocTestCase {
	
	@Override
	public  String getName(){
		return "UC-03";
	}

	@Test
	public void testITuc03ErreurDateOFBirth() throws Exception {
		// On teste l'UC03 - Getting started - Form Validation
		// On teste l'UC afin de vérifier la présence du message d'erreur pour la date de naissance. 
		selenium.open("");
		selenium.waitForPageToLoad("30000");
		selenium.click("link=Expand All");
		selenium.click("link=UC03 - Form Validation");
		verifyTrue(selenium.isElementPresent("//h2[contains(text(),\"Getting started > UC03 > Form Validation\")]"));
		// On rempli les champs avec des suites de caractères valides - sauf pour "dateofbirth"
		selenium.type("//*[contains(@id,'txt_firstname')]", "Jean");
		selenium.type("//*[contains(@id,'txt_lastname')]", "Pierre");
		// On entre une valeur erronée dans le champ "Date of birth" : jj/mm/aa
		selenium.type("//*[contains(@id,'txt_dateofbirth')]", "14/12/1980");
		selenium.type("//*[contains(@id,'txt_weight')]", "100");
		selenium.type("//*[contains(@id,'txt_height')]", "181");
		selenium.click("//*[contains(@id,'lnk_create')]");
		selenium.waitForPageToLoad("30000");
		// On vérifie que l'ID du message d'erreur est présente.
		assertTrue(selenium.isElementPresent("//*[contains(@id,'playerToCreate.dateOfBirth.errors')]"));
		// On vérifie que l'ID du message d'erreur existe.
		assertEquals("Failed to convert property value of type java.lang.String to required type java.util.Date for property playerToCreate.dateOfBirth; nested exception is java.lang.IllegalArgumentException: Invalid format.", selenium.getText("//*[contains(@id,'playerToCreate.dateOfBirth.errors')]"));
	}

	
}
