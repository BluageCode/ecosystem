package uc32;

import org.junit.Test;
import test.common.AbstractBADocTestCase;

public class ITuc32 extends AbstractBADocTestCase {
	

	@Override
	public  String getName(){
		return "UC-32";
	}
	
	@Test
	public void testITuc32() throws Exception {
		// On teste l'UC32 - Standard Features/Application/Datagrid - Calculable Table.
		selenium.open("");
		selenium.click("link=Expand All");
		selenium.waitForPageToLoad("30000");
		selenium.click("link=UC32 - Calculable Table");
		verifyTrue(selenium.isElementPresent("//h2[contains(text(),\"Standard Features > Application > Datagrid > UC32 > Calculable Table\")]"));
		// On v�rifie ici les donn�es pr�sentes dans l'UC.
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/thead/tr[th[contains(text(),\"Name\")] and th[contains(text(),\"Date of Birth\")] and th[contains(text(),\"Estimated Value (M$)\")] and th[contains(text(),\"Rookie\")] and th[contains(text(),\"Position\")] and th[contains(text(),\"Shots\")] and th[contains(text(),\"Scored\")] and th[contains(text(),\"Accuracy (%)\")]]"));
		verifyEquals("8", selenium.getXpathCount("//table[contains(@id,'tab_players')]/tbody/tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Stokes\")] and td/span[contains(text(),\"Loren\")] and td/span[contains(text(),\"07/17/1976\")] and td/span[contains(text(),\"30.97\")] and td//input[not(@checked)] and td/span[contains(text(),\"C\")] and td/span[contains(text(),\"18\")] and td/span[contains(text(),\"17\")] and td/span[contains(text(),\"94.44\")] and td/a[contains(text(),\"[edit]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Colon\")] and td/span[contains(text(),\"Loren\")] and td/span[contains(text(),\"12/14/1971\")] and td/span[contains(text(),\"30.14\")] and td//input[not(@checked)] and td/span[contains(text(),\"SF\")] and td/span[contains(text(),\"17\")] and td/span[contains(text(),\"15\")] and td/span[contains(text(),\"88.24\")] and td/a[contains(text(),\"[edit]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Sandoval\")] and td/span[contains(text(),\"Shane\")] and td/span[contains(text(),\"09/24/1975\")] and td/span[contains(text(),\"24.14\")] and td//input[not(@checked)] and td/span[contains(text(),\"PF\")] and td/span[contains(text(),\"13\")] and td/span[contains(text(),\"9\")] and td/span[contains(text(),\"69.23\")] and td/a[contains(text(),\"[edit]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Clark\")] and td/span[contains(text(),\"Bob\")] and td/span[contains(text(),\"09/04/1972\")] and td//input[not(@checked)] and td/span[contains(text(),\"SG\")] and td/span[contains(text(),\"15\")] and td/span[contains(text(),\"10\")] and td/span[contains(text(),\"66.67\")] and td/a[contains(text(),\"[edit]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Davis\")] and td/span[contains(text(),\"Alton\")]and td/span[contains(text(),\"02/13/1970\")] and td/span[contains(text(),\"20.34\")] and td//input[not(@checked)] and td/span[contains(text(),\"SG\")] and td/span[contains(text(),\"16\")] and td/span[contains(text(),\"9\")] and td/span[contains(text(),\"56.25\")] and td/a[contains(text(),\"[edit]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Favis\")] and td/span[contains(text(),\"Marion\")] and td/span[contains(text(),\"02/13/1970\")]and td/span[contains(text(),\"20.34\")] and td//input[not(@checked)] and td/span[contains(text(),\"PF\")] and td/span[contains(text(),\"15\")] and td/span[contains(text(),\"8\")] and td/span[contains(text(),\"53.33\")] and td/a[contains(text(),\"[edit]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Davis\")] and td/span[contains(text(),\"Harry\")] and td/span[contains(text(),\"04/14/1988\")] and td/span[contains(text(),\"9.69\")] and td//input[@checked] and td/span[contains(text(),\"PF\")] and td/span[contains(text(),\"10\")] and td/span[contains(text(),\"2\")] and td/span[contains(text(),\"20\")] and td/a[contains(text(),\"[edit]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Davis\")] and td/span[contains(text(),\"Roosevelt\")] and td/span[contains(text(),\"11/10/1983\")] and td/span[contains(text(),\"8.55\")] and td//input[@checked] and td/span[contains(text(),\"PF\")] and td/span[contains(text(),\"12\")] and td/span[contains(text(),\"1\")] and td/span[contains(text(),\"8.33\")] and td/a[contains(text(),\"[edit]\")]]"));
		// On v�rifie les fonctionalit�s de l'UC.
		// Pour ce faire, on indexe un joueur qu'on va �diter.
		// Pour Sandoval Shane.
		Number indexSandovalS = selenium.getElementIndex("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Sandoval\")] and td/span[contains(text(),\"Shane\")] and td/span[contains(text(),\"09/24/1975\")] and td/span[contains(text(),\"24.14\")] and td//input[not(@checked)] and td/span[contains(text(),\"PF\")] and td/span[contains(text(),\"13\")] and td/span[contains(text(),\"9\")] and td/span[contains(text(),\"69.23\")] and td/a[contains(text(),\"[edit]\")]]");
		System.out.println("variable index vaut : " + indexSandovalS);
		selenium.click("//table[contains(@id,'tab_players')]/tbody/tr[" + indexSandovalS + " + 1]/td[9]/a[contains(text(),\"[edit]\")]");
		selenium.waitForPageToLoad("30000");
		// On modifie les valeurs.
		selenium.type("//table[contains(@id,'tab_players')]/tbody/tr[" + indexSandovalS + " + 1]/td/input[contains(@id,'txt_shots')]", "${space}");
		selenium.type("//table[contains(@id,'tab_players')]/tbody/tr[" + indexSandovalS + " + 1]/td/input[contains(@id,'txt_shots')]", "10");
		selenium.type("//table[contains(@id,'tab_players')]/tbody/tr[" + indexSandovalS + " + 1]/td/input[contains(@id,'txt_scored')]", "${space}");
		selenium.type("//table[contains(@id,'tab_players')]/tbody/tr[" + indexSandovalS + " + 1]/td/input[contains(@id,'txt_scored')]", "50");
		// On v�rifie l'efficacit� de la ligne.
		selenium.click("//table[contains(@id,'tab_players')]/tbody/tr[" + indexSandovalS + " + 1]/td[9]/a[contains(text(),\"[update]\")]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie que la ligne a bien �t� modifi�e.
		verifyEquals("8", selenium.getXpathCount("//table[contains(@id,'tab_players')]/tbody/tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Sandoval\")] and td/span[contains(text(),\"Shane\")] and td/span[contains(text(),\"09/24/1975\")] and td/span[contains(text(),\"24.14\")] and td//input[not(@checked)] and td/span[contains(text(),\"PF\")] and td/span[contains(text(),\"10\")] and td/span[contains(text(),\"50\")] and td/span[contains(text(),\"500\")] and td/a[contains(text(),\"[edit]\")]]"));
		// Pour Stokes Loren.
		Number indexStokesL = selenium.getElementIndex("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Stokes\")] and td/span[contains(text(),\"Loren\")] and td/span[contains(text(),\"07/17/1976\")] and td/span[contains(text(),\"30.97\")] and td//input[not(@checked)] and td/span[contains(text(),\"C\")] and td/span[contains(text(),\"18\")] and td/span[contains(text(),\"17\")] and td/span[contains(text(),\"94.44\")] and td/a[contains(text(),\"[edit]\")]]");
		System.out.println("variable index vaut : " + indexStokesL);
		selenium.click("//table[contains(@id,'tab_players')]/tbody/tr[" + indexStokesL + " + 1]/td[9]/a[contains(text(),\"[edit]\")]");
		selenium.waitForPageToLoad("30000");
		// On modifie les valeurs.
		selenium.type("//table[contains(@id,'tab_players')]/tbody/tr[" + indexStokesL + " + 1]/td/input[contains(@id,'txt_shots')]", "0");
		selenium.type("//table[contains(@id,'tab_players')]/tbody/tr[" + indexStokesL + " + 1]/td/input[contains(@id,'txt_scored')]", "0");
		// On v�rifie l'efficacit� de la ligne.
		selenium.click("//table[contains(@id,'tab_players')]/tbody/tr[" + indexStokesL + " + 1]/td[9]/a[contains(text(),\"[update]\")]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie que la ligne a bien �t� modifi�e.
		verifyEquals("8", selenium.getXpathCount("//table[contains(@id,'tab_players')]/tbody/tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Stokes\")] and td/span[contains(text(),\"Loren\")] and td/span[contains(text(),\"07/17/1976\")] and td/span[contains(text(),\"30.97\")] and td//input[not(@checked)] and td/span[contains(text(),\"C\")] and td/span[contains(text(),\"0\")] and td/span[contains(text(),\"0\")] and td/span[contains(text(),\"\")] and td/a[contains(text(),\"[edit]\")]]"));
	}


}
