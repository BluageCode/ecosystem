--<ScriptOptions statementTerminator=";"/>

ALTER TABLE player53 DROP CONSTRAINT player53_pkey1;

DROP TABLE player53;

CREATE TABLE player53 (
		id INT8 NOT NULL,
		version INT4 NOT NULL,
		first_name VARCHAR(1024),
		last_name VARCHAR(1024),
		rookie BOOL,
		matches INT4,
		value FLOAT4
	);
	
INSERT INTO player53 (id, version, first_name, last_name, rookie, matches, value) VALUES (1, 0, 'John', 'Smith', true, 23, NULL);



ALTER TABLE player53 ADD CONSTRAINT player53_pkey1 PRIMARY KEY (id);

