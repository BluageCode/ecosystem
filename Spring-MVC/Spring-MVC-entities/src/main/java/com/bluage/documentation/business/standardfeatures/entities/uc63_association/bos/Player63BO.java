/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.business.standardfeatures.entities.uc63_association.bos;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.inject.Inject;
import javax.inject.Named;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.LazyInitializationException;

import com.bluage.documentation.business.common.bos.PlayerBO;
import com.bluage.documentation.business.standardfeatures.entities.uc63_association.entities.daofinder.Player63DAOFinderImpl;
import com.netfective.bluage.core.exception.ApplicationException;

/**
 * Player63BO
 *
* @see com.netfective.bluage.core.business.IBusinessObject
* * @see PlayerBO
 */
@XmlRootElement

public class Player63BO
 extends PlayerBO {
	
	/**
	 * Default SUID for Player63BO.
	 */
	private static final long serialVersionUID = 1L;
	

	@Inject
	@Named ("com.bluage.documentation.business.standardfeatures.entities.uc63_association.entities.daofinder.Player63Finder") 
	private Player63DAOFinderImpl finder;

																			/**
	 * Entity property: championship63s
	 *
	 */
private Set<Championship63BO> championship63s = new HashSet<Championship63BO>();



   /**
	* public default constructor for class Player63BO
	*/
	public Player63BO(){
		//NOP Player63BO
	}


																			/**
	 * Getter for championship63s of class Player63BO.
	 *
	 * @return {@link java.util.Set<com.bluage.documentation.business.standardfeatures.entities.uc63_association.bos.Championship63BO>}
	 * @generated
	 */ 
public  Set<Championship63BO> getChampionship63s() {
	return this.championship63s;
			}


	/**
	 * Setter for championship63s of class Player63BO.
	 * @param value {@link java.util.Set<com.bluage.documentation.business.standardfeatures.entities.uc63_association.bos.Championship63BO>}
	 * @generated
	 */

public  void setChampionship63s(final Set<Championship63BO> championship63s) {
	this.championship63s = championship63s;
}

	/**
	 * Remove an entity from the com.bluage.documentation.business.standardfeatures.entities.uc63_association.bos.Championship63BO collection.
	 *
	 * @param entity
	 *		: {@link Set<Championship63BO>} the entity
	 * @throws ApplicationException
     */
public void removeFromChampionship63s( Championship63BO entity ) throws ApplicationException {
	try {
		if( getChampionship63s() != null && getChampionship63s().contains(entity) ) {
			getChampionship63s().remove( entity );
		entity.getPlayer63s().remove(this);
		}
		} catch ( LazyInitializationException e ) {
				Player63BO bo = ( Player63BO ) finder.findByPrimaryKey( getId() );
			finder.getHibernateTemplate().initialize( bo.getChampionship63s() );
			setChampionship63s( bo.getChampionship63s() );
			// evict object from session to avoid NonUniqueObjectException
			finder.getHibernateTemplate().evict( bo );
		
			if( getChampionship63s() != null && getChampionship63s().contains( entity ) ) {
				getChampionship63s().remove( entity );
				entity.getPlayer63s().remove( this );
			}
				}
	}
	
	/**
	 * Add entity to the com.bluage.documentation.business.standardfeatures.entities.uc63_association.bos.Championship63BO collection.
	 *
	 * @param entity
	 *		{@link Championship63BO} the entity
	 * @throws ApplicationException
     */
public void addToChampionship63s( Championship63BO entity ) throws ApplicationException
	{
		try {
			if( getChampionship63s() != null && !getChampionship63s().contains( entity ) ) {
				getChampionship63s().add( entity );
				entity.getPlayer63s().add( this );
			}
			} catch ( LazyInitializationException e ) {
			Player63BO bo = ( Player63BO ) finder.findByPrimaryKey( getId() );
		finder.getHibernateTemplate().initialize( bo.getChampionship63s() );
		setChampionship63s( bo.getChampionship63s() );
		// evict object from session to avoid NonUniqueObjectException
		finder.getHibernateTemplate().evict( bo );
		if( getChampionship63s() != null && !getChampionship63s().contains( entity ) ) {
			getChampionship63s().add( entity );
			entity.getPlayer63s().add( this );
		}
			}
	}
	
	/**
	 * Add entitys to the com.bluage.documentation.business.standardfeatures.entities.uc63_association.bos.Championship63BO collection
	 *
	 * @param entities			
	 *		: {@link java.util.List} A list of entities
	 * @throws ApplicationException 
     */	
public void addAllToChampionship63s( Collection<Championship63BO> entities ) throws ApplicationException {
	for( Championship63BO entity : entities) {
		if( getChampionship63s()!=null ) {
			addToChampionship63s( entity );
		}
	}
}
	
	/**
	 * Remove entities form the com.bluage.documentation.business.standardfeatures.entities.uc63_association.bos.Championship63BO collection
	 *
	 * @param entities				
	 *		: {@link java.util.List} a list of entities
	 * @throws ApplicationException
     */	
public void removeAllFromChampionship63s( Collection<Championship63BO> entities ) throws ApplicationException {
	for( Championship63BO entity : entities) {
		if( getChampionship63s()!=null && getChampionship63s().contains( entity ) )	{		
			removeFromChampionship63s( entity );
		}
	}
}
	
	/**
	 * Remove all items form the collection com.bluage.documentation.business.standardfeatures.entities.uc63_association.bos.Championship63BO
	 *
	 * @throws ApplicationException
     */	
public void removeAllFromChampionship63s() throws ApplicationException {
	Set<Championship63BO> entities = this.getChampionship63s();
	if ( entities == null ) {
		return;
	}
	Iterator<Championship63BO> it = entities.iterator();
	while ( it.hasNext() ) {
    	Championship63BO entity = it.next(); 
		entity.setPlayer63s( null ); 
	}
	entities.clear();
}

/**
	 * Returns <code>true</code> if the argument is an Player63 instance and all identifiers for this entity
     * equal the identifiers of the argument entity. The <code>equals</code> method of the parent entity
     * will also need to return <code>true</code>. Returns <code>false</code> otherwise.
     *
     * @see PlayerBO#hashCode()
	 * @param object  object to compare with current instance      
     * @return boolean true if equals false if not
     */
    public boolean equals(final Object object )
    {
		if ( this == object )
        {
            return true;
        }
        if ( !( object instanceof Player63BO ) )
        {
            return false;
        }
        final Player63BO that = (Player63BO)object;
    
    	            if (        				        			this.getId() == null
        	        ) {
        	return false;
        }
		return new EqualsBuilder()
			.append(this.getId(), that.getId())
			 	.isEquals();
	}
	/**

	 * Returns a hash code based on this entity's identifiers and the hash code of the parent entity.
     *
     * @see PlayerBO#hashCode()
     * @return int
     */    
    public int hashCode() {
        return new HashCodeBuilder()
             .append(getId())
				 	.appendSuper(super.hashCode())
            .toHashCode();
    }
}

