/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.service.gettingstarted.uc01_helloworld;


import javax.inject.Named;

import com.bluage.documentation.business.gettingstarted.uc01_helloworld.bos.HelloWorld01BO;
import com.bluage.documentation.service.AbstractService;
import com.netfective.bluage.core.exception.ApplicationException;

/**
 * Defines  application services for ServiceHelloWorld
  */
@Named("com.bluage.documentation.service.gettingstarted.uc01_helloworld.ServiceHelloWorld")
public class ServiceHelloWorld  extends AbstractService{


							


	/**
	 *  Default constructor 
	 */
	public ServiceHelloWorld() {
		super(); // Call super for default constructor : ServiceHelloWorld
	}
	



			
	/**    
	 * Process operation.
	 * @return {@link HelloWorld01BO}
	 */
	public HelloWorld01BO displayHelloWorld( )
 throws ApplicationException {

				return privateDisplayHelloWorld();
	}
	

	/**    
	 * Process operation.
	 * @return {@link HelloWorld01BO}
	 */
	private HelloWorld01BO privateDisplayHelloWorld( )
 throws ApplicationException {
				// Return Type declaration.
				HelloWorld01BO helloWorld = new HelloWorld01BO();
					// Central buffer node declaration for operation displayHelloWorld
			// Calling an operation : 
helloWorld.setText("Hello World !");

						return helloWorld;
		}

		
			
	/**    
	 * Process operation.
	 * @return {@link HelloWorld01BO}
	 */
	public HelloWorld01BO resetHelloWorld( )
 throws ApplicationException {

				return privateResetHelloWorld();
	}
	

	/**    
	 * Process operation.
	 * @return {@link HelloWorld01BO}
	 */
	private HelloWorld01BO privateResetHelloWorld( )
 throws ApplicationException {
				// Return Type declaration.
				HelloWorld01BO helloWorld = new HelloWorld01BO();
					// Central buffer node declaration for operation resetHelloWorld
			// Calling an operation : 
helloWorld.setText("");

						return helloWorld;
		}

		}
