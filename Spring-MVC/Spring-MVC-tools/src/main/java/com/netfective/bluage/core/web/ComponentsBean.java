/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.netfective.bluage.core.web ;

import java.util.Map;

/**
 * 
 * Component Bean manager
 * 
 */
public class ComponentsBean {

	// Bean description.
	// Properties
	private String parentPathScreenName;
	private String parentHelperName;
	private Map<String, String> attributs;

	// Getter/Setters
	public String getParentPathScreenName() {
		return parentPathScreenName;
	}

	public void setParentPathScreenName(final String parentPathScreenName) {
		this.parentPathScreenName = parentPathScreenName;
	}

	public String getParentHelperName() {
		return parentHelperName;
	}

	public void setParentHelperName(final String parentHelperName) {
		this.parentHelperName = parentHelperName;
	}

	public Map<String, String> getAttributs() {
		return attributs;
	}

	public void setAttributs(final Map<String, String> attributs) {
		this.attributs = attributs;
	}

	@Override
	public String toString() {
		return "ComponentsBean [parentPathScreenName=" + parentPathScreenName
				+ ", parentHelperName=" + parentHelperName + ", attributs="
				+ attributs + "]";
	}

}
