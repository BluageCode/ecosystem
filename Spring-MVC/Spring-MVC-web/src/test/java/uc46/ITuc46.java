package uc46;

import org.junit.Test;
import test.common.AbstractBADocTestCase;

public class ITuc46 extends AbstractBADocTestCase{

	@Override
	public  String getName(){
		return "UC-46";
	}

	@Test
	public void testITuc46() throws Exception {
		// On teste l'UC46 - Standard Features/Application/Component - Menu
		selenium.open("");
		selenium.click("link=Expand All");
		selenium.waitForPageToLoad("30000");
		selenium.click("link=UC46 - Menu");
		verifyTrue(selenium.isElementPresent("//h2[contains(text(),\"Standard Features > Application > Component > UC46 > Menu\")]"));
		verifyTrue(selenium.isElementPresent("//h4[contains(text(),\"Page One\")]"));
		assertTrue(selenium.isElementPresent("xpath=//p[contains(text(),\"\nYou are here : page one.\n\")]"));
		// On va sur la page 1
		selenium.click("//*[contains(@id,'lnk_goToPage1')]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("xpath=//p[contains(text(),\"\nYou are here : page one.\n\")]"));
		// On vérifie que l'on est sur la page 1
		assertTrue(selenium.isElementPresent("//h4[contains(text(),'Page One')]"));
		// On va sur la page 2
		selenium.click("//*[contains(@id,'lnk_goToPage2')]");
		selenium.waitForPageToLoad("30000");
		// On vérifie que l'on est bien sur la page 2
		verifyTrue(selenium.isElementPresent("//h4[contains(text(),\"Page Two\")]"));
		assertTrue(selenium.isElementPresent("xpath=//p[contains(text(),\"\nYou are here : page two.\n\")]"));
		assertTrue(selenium.isElementPresent("//h4[contains(text(),'Page Two')]"));
	}

	
}
