package uc54;

import org.junit.Test;
import test.common.AbstractBADocTestCase;

public class AddAllPlayers extends AbstractBADocTestCase {
	
	@Override
	public  String getName(){
		return "UC-54";
	}	

	
	@Test
	public void taddAllPlayers() throws Exception {
		// On teste l'UC54 - Standard Features/Services/Process - For
		selenium.open("");	
		selenium.click("link=Expand All");
		selenium.waitForPageToLoad("30000");
		selenium.click("link=UC54 - For");
		verifyTrue(selenium.isElementPresent("//h2[contains(text(),\"Standard Features > Services > Process > UC54 > For\")]"));
		selenium.click("//*[contains(@id,'lnk_showPlayers')]");
		selenium.waitForPageToLoad("30000");
		// On sélectionne tous les joueurs.
		// On crée une boucle.
		Number nbLignes = selenium.getXpathCount("//table[contains(@id,'tab_PlayersList')]/tbody/tr");
		System.out.println(nbLignes);
		for (int iterateur=0; iterateur<nbLignes.intValue(); iterateur++){
			selenium.click("//*[contains(@id,'" + iterateur + ":chx_team') or contains(@id,'chx_team[" + iterateur + "]')]");
		}
		// On valide la sélection.
		selenium.click("//*[contains(@id,'lnk_selectPlayers')]");
		selenium.waitForPageToLoad("30000");
		// On vérifie que les joueurs sélectionnés s'affichent.
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_TeamPlayersList')]/tbody/tr[td/span[contains(text(),\"Jack\")] and td/span[contains(text(),\"White\")] and td/span[contains(text(),\"25.0\")] and td//input[not(@checked)]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_TeamPlayersList')]/tbody/tr[td//span[contains(text(),\"Robert\")] and td//span[contains(text(),\"Plant\")] and td//span[contains(text(),\"43.0\")] and td//input[not(@checked)]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_TeamPlayersList')]/tbody//tr[td//span[contains(text(),\"Jim\")] and td//span[contains(text(),\"Morrisson\")] and td//span[contains(text(),\"38.0\")] and td//input[not(@checked)]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_TeamPlayersList')]/tbody//tr[td//span[contains(text(),\"Roger\")] and td//span[contains(text(),\"Waters\")] and td//span[contains(text(),\"32.0\")] and td//input[@checked]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_TeamPlayersList')]/tbody//tr[td//span[contains(text(),\"Trent\")] and td//span[contains(text(),\"Reznor\")] and td//span[contains(text(),\"29.0\")] and td//input[@checked]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_TeamPlayersList')]/tbody//tr[td//span[contains(text(),\"Howard\")] and td//span[contains(text(),\"Lovecraft\")] and td//span[contains(text(),\"51.0\")] and td//input[not(@checked)]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_TeamPlayersList')]/tbody//tr[td//span[contains(text(),\"Dan\")] and td//span[contains(text(),\"Simmons\")] and td//span[contains(text(),\"27.0\")] and td//input[@checked]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_TeamPlayersList')]/tbody//tr[td//span[contains(text(),\"Stanley\")] and td//span[contains(text(),\"Kubrick\")] and td//span[contains(text(),\"16.0\")] and td//input[@checked]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_TeamPlayersList')]/tbody//tr[td//span[contains(text(),\"Tim\")] and td//span[contains(text(),\"Burton\")] and td//span[contains(text(),\"14.0\")] and td//input[not(@checked)]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_TeamPlayersList')]/tbody//tr[td//span[contains(text(),\"Isaac\")] and td//span[contains(text(),\"Asimov\")] and td//span[contains(text(),\"20.0\")] and td//input[@checked]]"));
	}


}
