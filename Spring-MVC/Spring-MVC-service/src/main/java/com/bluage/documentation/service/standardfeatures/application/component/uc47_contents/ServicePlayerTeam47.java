/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.service.standardfeatures.application.component.uc47_contents;


import javax.inject.Inject;
import javax.inject.Named;

import com.bluage.documentation.business.standardfeatures.application.component.uc47_contents.bos.Player47BO;
import com.bluage.documentation.business.standardfeatures.application.component.uc47_contents.bos.PlayerTeam47BO;
import com.bluage.documentation.business.standardfeatures.application.component.uc47_contents.bos.Team47BO;
import com.bluage.documentation.business.standardfeatures.application.component.uc47_contents.entities.dao.PlayerTeam47DAOImpl;
import com.bluage.documentation.service.AbstractService;
import com.bluage.documentation.service.standardfeatures.application.component.uc47_contents.daofinder.ServicePlayerTeam47DAOMethodFinderImpl;
import com.netfective.bluage.core.exception.ApplicationException;

/**
 * Defines  application services for ServicePlayerTeam47
  */
@Named("com.bluage.documentation.service.standardfeatures.application.component.uc47_contents.ServicePlayerTeam47")
public class ServicePlayerTeam47  extends AbstractService{


			@Inject
		@Named ("com.bluage.documentation.service.standardfeatures.application.component.uc47_contents.daofinder.ServicePlayerTeam47DAOMethodFinder")
		private ServicePlayerTeam47DAOMethodFinderImpl servicePlayerTeam47Dao;
		@Inject
		@Named ("com.bluage.documentation.business.standardfeatures.application.component.uc47_contents.entities.dao.PlayerTeam47DAOImpl")
		private PlayerTeam47DAOImpl playerTeam47BODao;
					@Inject
		@Named ("com.bluage.documentation.service.standardfeatures.application.component.uc47_contents.ServiceTeam47")
		private ServiceTeam47  instanceServiceTeam47;
							@Inject
		@Named ("com.bluage.documentation.service.standardfeatures.application.component.uc47_contents.ServicePlayer47")
		private ServicePlayer47  instanceServicePlayer47;
						


	/**
	 *  Default constructor 
	 */
	public ServicePlayerTeam47() {
		super(); // Call super for default constructor : ServicePlayerTeam47
	}
	



			
	/**    
	 * Process operation.
	 * @param teamToAdd {@link Team47BO}
		 * @param playerToUpdate {@link Player47BO}
		 * @return {@link void}
	 */
	public void createPlayerTeam47(Team47BO teamToAdd,Player47BO playerToUpdate )
 throws ApplicationException {

			privateCreatePlayerTeam47(teamToAdd,playerToUpdate);
	}
	

	/**    
	 * Process operation.
	 * @param teamToAdd {@link Team47BO}
		 * @param playerToUpdate {@link Player47BO}
		 * @return {@link void}
	 */
	private void privateCreatePlayerTeam47(Team47BO teamToAdd,Player47BO playerToUpdate )
 throws ApplicationException {
				// Central buffer node declaration for operation createPlayerTeam47
			PlayerTeam47BO	 newplayerteam =  new PlayerTeam47BO();
					Player47BO	 player =  new Player47BO();
					Team47BO	 team =  new Team47BO();
					PlayerTeam47BO	 playerteamfound =  new PlayerTeam47BO();
							// Calling an operation : 
player = instanceServicePlayer47.player47FindByID(playerToUpdate);
// Calling an operation : 
team = instanceServiceTeam47.team47FindByID(teamToAdd);
// Calling an operation : 
newplayerteam.setPlayer(player);
// Calling an operation : 
newplayerteam.setTeam(team);
// Calling an operation : 
playerteamfound = searchPlayerTeam47(team, player);
// Test on exception : 
if (playerteamfound!=null
) {
ApplicationException app = new ApplicationException("");
throw app;
}
// Calling an operation : 
playerTeam47Create(newplayerteam);

		}

					
				
	/**    
	 * Operation Crud
	 * @param bo {@link PlayerTeam47BO}
		 * @return {@link boolean}
	 */
	public boolean playerTeam47Create(PlayerTeam47BO bo )
 throws ApplicationException {

					return playerTeam47BODao.create(bo);
	}

		
			
	/**    
	 * Operation HQL.
	 * @param team {@link Team47BO}
		 * @param player {@link Player47BO}
		 * @return {@link PlayerTeam47BO}
	 */
	public PlayerTeam47BO searchPlayerTeam47(Team47BO team,Player47BO player )
 throws ApplicationException {
	
	return servicePlayerTeam47Dao.searchPlayerTeam47(team,player);
	}

		}
