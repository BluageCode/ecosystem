package test.common;

import com.thoughtworks.selenium.SeleneseTestBase;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverBackedSelenium;
import org.openqa.selenium.firefox.FirefoxDriver;

/**
 * User: b.maggi
 * Date: 12/02/13
 */
public abstract class AbstractBADocTestCase extends SeleneseTestBase {


   public  String getName(){
       return "TODO";
   }

    @Before
    public void setUp() throws Exception {
        String baseUrl = System.getProperty("webdriver.base.url");
        if (baseUrl == null){
            System.out.println("test selenium local (localhost)");
            baseUrl= "http://dtv-120902:8081/BA-Doc-Struts-web";
        }
         WebDriver driver = new FirefoxDriver();

         String applicationName= System.getProperty("uc");
         if (applicationName == null) {
        	 applicationName = getName();
         }
         
         System.out.println("Url : "+baseUrl+applicationName+"-web");
         selenium = new WebDriverBackedSelenium(driver, baseUrl+applicationName+"-web");
    }

    @After
    public void tearDown() throws Exception {
        selenium.stop();
    }

}
