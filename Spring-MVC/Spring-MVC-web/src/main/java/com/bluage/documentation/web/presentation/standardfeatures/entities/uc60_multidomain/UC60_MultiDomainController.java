/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.entities.uc60_multidomain ;

// Import declaration.
// Java imports.
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.bluage.documentation.business.standardfeatures.entities.uc60_multidomain.teams.bos.Team60BO;
import com.bluage.documentation.business.standardfeatures.entities.uc60_multidomain.teams.entities.daofinder.Team60DAOFinderImpl;
import com.bluage.documentation.service.standardfeatures.entities.uc60_multidomain.ServiceMultidomaine;
import com.netfective.bluage.core.exception.ApplicationException;
import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;
import com.netfective.bluage.core.web.IForm;

/**
 * Class : UC60_MultiDomainController
 */
@Controller
@Scope("prototype")
@SessionAttributes("uC60_MultiDomainForm")
@RequestMapping(value= "/presentation/standardfeatures/entities/uc60_multidomain")
public class UC60_MultiDomainController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC60_MultiDomainController.class);
	
	//Current form name
	private static final String U_C60__MULTI_DOMAIN_FORM = "uC60_MultiDomainForm";

	//CONSTANT: allTeams table or repeater.
	private static final String ALL_TEAMS = "allTeams";
				//CONSTANT: viewPlayers
	private static final String VIEW_PLAYERS = "viewPlayers";
	//CONSTANT: selectedTeam
	private static final String SELECTED_TEAM = "selectedTeam";
	
	/**
	 * Property:customDateEditorsUC60_MultiDomainController
	 */
	final private Map<String,CustomDateEditor> customDateEditorsUC60_MultiDomainController = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:uC60_MultiDomainValidator
	 */
	final private UC60_MultiDomainValidator uC60_MultiDomainValidator = new UC60_MultiDomainValidator();
	
	/**
	 * Service declaration : serviceMultidomaine.
	 */
	@Autowired
	private ServiceMultidomaine serviceMultidomaine;
	
	/**
	 * Generic Finder : team60DAOFinderImpl.
	 */
	@Autowired
	private Team60DAOFinderImpl team60DAOFinderImpl;
	
	
	// Initialise all the instances for UC60_MultiDomainController
		// Initialize the instance allTeams for the state lnk_back
		private List allTeams; // Initialize the instance allTeams for UC60_MultiDomainController
						// Declare the instance viewPlayers
		private Team60BO viewPlayers;
		// Declare the instance selectedTeam
		private Team60BO selectedTeam;
			/**
	 * Operation : lnk_view
 	 * @param model : 
 	 * @param uC60_MultiDomain : The form
 	 * @return
	 */
	@RequestMapping(value = "/UC60_MultiDomain/lnk_view.html",method = RequestMethod.POST)
	public String lnk_view(final HttpServletRequest request,final HttpServletResponse response,final  Model model,final @ModelAttribute("uC60_MultiDomainForm") UC60_MultiDomainForm  uC60_MultiDomainForm, final BindingResult bindingResult ,final @RequestParam(ID_REQUEST_PARAM) Integer index){
		
		
		
		LOGGER.info("Begin method : lnk_view"); 
		infoLogger(uC60_MultiDomainForm); 
		uC60_MultiDomainForm.setAlwaysCallPreControllers(false); 
		// initialization for lnk_view
		init();
		
		String destinationPath = null; 
		
										// Calling the validators for the state lnk_view if the validation fail, you will be forward to the screen : "/presentation/standardfeatures/entities/uc60_multidomain/UC60_MultiDomain"
		if(errorsMessages(request,response,uC60_MultiDomainForm,bindingResult,"selectedTeam", uC60_MultiDomainValidator, customDateEditorsUC60_MultiDomainController)){ 
			return "/presentation/standardfeatures/entities/uc60_multidomain/UC60_MultiDomain"; 
		}

										 callTeamFindByID(request,uC60_MultiDomainForm , index);
											// Init the destination form com.bluage.documentation.web.presentation.standardfeatures.entities.uc60_multidomain.UC60_ViewForm or not from lnk_view
			final UC60_ViewForm uC60_ViewForm =  new UC60_ViewForm();
			// Populate the destination form with all the instances returned from lnk_view.
			final IForm uC60_ViewForm2 = populateDestinationForm(request.getSession(), uC60_ViewForm); 
					
			// Add the form to the model.
			model.addAttribute("uC60_ViewForm", uC60_ViewForm2); 
			
			request.getSession().setAttribute("uC60_ViewForm", uC60_ViewForm2);
			
			// "OK" CASE => destination screen path from lnk_view
			LOGGER.info("Go to the screen 'UC60_View'.");
			// Redirect (PRG) from lnk_view
			destinationPath =  "redirect:/presentation/standardfeatures/entities/uc60_multidomain/UC60_View.html";
						
	
	
		return destinationPath; // Returns the destination path for the state : lnk_view 
	}
	
	
	/**
	* This method initialise the form : UC60_MultiDomainForm 
	* @return UC60_MultiDomainForm
	*/
	@ModelAttribute("UC60_MultiDomainFormInit")
	public void initUC60_MultiDomainForm(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, U_C60__MULTI_DOMAIN_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method UC60_MultiDomainForm."); //for lnk_view 
		}
		UC60_MultiDomainForm uC60_MultiDomainForm;
	
		if(request.getSession().getAttribute(U_C60__MULTI_DOMAIN_FORM) != null){
			uC60_MultiDomainForm = (UC60_MultiDomainForm)request.getSession().getAttribute(U_C60__MULTI_DOMAIN_FORM);
		} else {
			uC60_MultiDomainForm = new UC60_MultiDomainForm();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form UC60_MultiDomainForm.");
		}
		uC60_MultiDomainForm = (UC60_MultiDomainForm)populateDestinationForm(request.getSession(), uC60_MultiDomainForm);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  UC60_MultiDomainForm.");
		}
		model.addAttribute(U_C60__MULTI_DOMAIN_FORM, uC60_MultiDomainForm);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param UC60_MultiDomainForm : The sceen form.
	 */
	@RequestMapping(value = "/UC60_MultiDomain.html" ,method = RequestMethod.GET)
	public void prepareUC60_MultiDomain(final HttpServletRequest request,final  Model model,final  @ModelAttribute("UC60_MultiDomainFormInit") UC60_MultiDomainForm uC60_MultiDomainForm){
		
		UC60_MultiDomainForm currentUC60_MultiDomainForm = uC60_MultiDomainForm;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(U_C60__MULTI_DOMAIN_FORM) == null){
			if(currentUC60_MultiDomainForm!=null){
				request.getSession().setAttribute(U_C60__MULTI_DOMAIN_FORM, currentUC60_MultiDomainForm);
			}else {
				currentUC60_MultiDomainForm = new UC60_MultiDomainForm();
				request.getSession().setAttribute(U_C60__MULTI_DOMAIN_FORM, currentUC60_MultiDomainForm);	
			}
		} else {
			currentUC60_MultiDomainForm = (UC60_MultiDomainForm) request.getSession().getAttribute(U_C60__MULTI_DOMAIN_FORM);
		}

		try {
			List allTeams = team60DAOFinderImpl.findAll();
			put(request.getSession(), ALL_TEAMS, allTeams);
		} catch (ApplicationException e) {
			errorLogger("An error while calling the generic finder for the instance : allTeams.",e);
		}
				currentUC60_MultiDomainForm = (UC60_MultiDomainForm)populateDestinationForm(request.getSession(), currentUC60_MultiDomainForm);
		// Call all the Precontroller.
	currentUC60_MultiDomainForm.setAlwaysCallPreControllers(true);
		model.addAttribute(U_C60__MULTI_DOMAIN_FORM, currentUC60_MultiDomainForm);
		request.getSession().setAttribute(U_C60__MULTI_DOMAIN_FORM, currentUC60_MultiDomainForm);
		
	}
	
										/**
	 * method callTeamFindByID
	 * 
	 * @param request HttpServletRequest the request
	 * @param form IForm the form
	 * @param index Integer the current index 
	 */
	private void callTeamFindByID(HttpServletRequest request,IForm form,Integer index) {
		viewPlayers = (Team60BO)get(request.getSession(),form, "viewPlayers");  
										 
					if(index!=null){  
			 selectedTeam = (Team60BO)((List)get(request.getSession(),form, "allTeams")).get(index);  
		} else {
			 selectedTeam= null; 
		}
				 
					try {
				// executing TeamFindByID in lnk_view
				viewPlayers = 	serviceMultidomaine.teamFindByID(
			selectedTeam
			);  
 
				put(request.getSession(), VIEW_PLAYERS,viewPlayers);
								// processing variables TeamFindByID in lnk_view
				put(request.getSession(), SELECTED_TEAM,selectedTeam); 
			
			} catch (ApplicationException e) { 
				// error handling for operation teamFindByID called TeamFindByID
				errorLogger("An error occured during the execution of the operation : teamFindByID",e); 
		
			}
	}
		
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("UC60_MultiDomainController [ ");
			strBToS.append(ALL_TEAMS);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(allTeams);
			strBToS.append(END_TO_STRING_DELIMITER);
						strBToS.append(VIEW_PLAYERS);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(viewPlayers);
			strBToS.append(END_TO_STRING_DELIMITER);
			strBToS.append(SELECTED_TEAM);
			strBToS.append(START_TO_STRING_DELIMITER);
			strBToS.append(selectedTeam);
			strBToS.append(END_TO_STRING_DELIMITER);
		// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(uC60_MultiDomainValidator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorsUC60_MultiDomainController!=null); 
		return strBToS.toString();
	}
}
