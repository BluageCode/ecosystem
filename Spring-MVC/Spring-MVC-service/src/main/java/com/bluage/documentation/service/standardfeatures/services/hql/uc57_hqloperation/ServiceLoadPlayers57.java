/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.service.standardfeatures.services.hql.uc57_hqloperation;


import javax.inject.Inject;
import javax.inject.Named;

import com.bluage.documentation.business.standardfeatures.services.hql.uc57_hqloperation.bos.Team57BO;
import com.bluage.documentation.service.AbstractService;
import com.bluage.documentation.service.standardfeatures.services.hql.uc57_hqloperation.daofinder.ServiceLoadPlayers57DAOMethodFinderImpl;
import com.netfective.bluage.core.exception.ApplicationException;

/**
 * Defines  application services for ServiceLoadPlayers57
  */
@Named("com.bluage.documentation.service.standardfeatures.services.hql.uc57_hqloperation.ServiceLoadPlayers57")
public class ServiceLoadPlayers57  extends AbstractService{


			@Inject
		@Named ("com.bluage.documentation.service.standardfeatures.services.hql.uc57_hqloperation.daofinder.ServiceLoadPlayers57DAOMethodFinder")
		private ServiceLoadPlayers57DAOMethodFinderImpl serviceLoadPlayers57Dao;



	/**
	 *  Default constructor 
	 */
	public ServiceLoadPlayers57() {
		super(); // Call super for default constructor : ServiceLoadPlayers57
	}
	



			
	/**    
	 * Operation HQL.
	 * @param team {@link Team57BO}
		 * @return {@link Team57BO}
	 */
	public Team57BO loadPlayers(Team57BO team )
 throws ApplicationException {
	
	return serviceLoadPlayers57Dao.loadPlayers(team);
	}

		}
