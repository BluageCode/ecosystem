/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.entities.uc72_generatorclass ;


import org.apache.log4j.Logger;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;

import com.netfective.bluage.core.validator.AbstractValidator;
import com.netfective.bluage.core.validator.ValidatorsUtil;
/**
* Class :UC72_AddPlayerValidator
*/
public class UC72_AddPlayerValidator extends AbstractValidator{
	
	// LOGGER for the class UC72_AddPlayerValidator
	private static final Logger LOGGER = Logger.getLogger( UC72_AddPlayerValidator.class);
	
	
	/**
	* Operation validate for UC72_AddPlayerForm
	* @param obj : the current form (UC72_AddPlayerForm)
	* @param errors : The spring errors to return for the form UC72_AddPlayerForm
	*/
	public void validate(final Object obj,final Errors errors){
		LOGGER.info("Beginning method : validate"); // UC72_AddPlayerValidator
		UC72_AddPlayerForm cUC72_AddPlayerForm = (UC72_AddPlayerForm)obj; // UC72_AddPlayerValidator
				ValidationUtils.rejectIfEmpty(errors, "playerToCreate.firstName", "", "First name is required.");

		if(errors.getFieldError("playerToCreate.firstName")== null){
						if(LOGGER.isDebugEnabled()){
				LOGGER.debug("validation of the 'playerToCreate.firstName' field with the LengthValidator validator");
			}
			
			if(ValidatorsUtil.lengthValidator(cUC72_AddPlayerForm.getPlayerToCreate().getFirstName(), 1, 30) == 0){
				errors.rejectValue("playerToCreate.firstName", "", "First name should be less than 30 characters long.");
			} 
				
		}
				ValidationUtils.rejectIfEmpty(errors, "playerToCreate.lastName", "", "Last name is required.");

		if(errors.getFieldError("playerToCreate.lastName")== null){
						if(LOGGER.isDebugEnabled()){
				LOGGER.debug("validation of the 'playerToCreate.lastName' field with the LengthValidator validator");
			}
			
			if(ValidatorsUtil.lengthValidator(cUC72_AddPlayerForm.getPlayerToCreate().getLastName(), 1, 30) == 0){
				errors.rejectValue("playerToCreate.lastName", "", "Last name should be less than 30 characters long.");
			} 
				
		}
				ValidationUtils.rejectIfEmpty(errors, "playerToCreate.id", "", "Id is required.");

			LOGGER.info("Ending method : validate the form "+ cUC72_AddPlayerForm.getClass().getName()); // UC72_AddPlayerValidator
	}

	/**
	* Method to implements to use spring validators (UC72_AddPlayerForm)
	* @param aClass : Class for the form UC72_AddPlayerForm
	* @return boolean
	*/
	public boolean supports(final Class aClass){
		return (new UC72_AddPlayerForm()).getClass().equals(aClass); // UC72_AddPlayerValidator
	}
}
