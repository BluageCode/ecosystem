<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file="/include.jsp"  %>
<html >
<head >
<title  > 
<fmt:message key="cUC206_DetailsPlayer_BLU_AGE_DOCUMENTATION"/>
</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"  > 
</meta>
<link href="<c:url value= "/presentation/_style/_style.css"/>" rel="stylesheet" type="text/css"  /> 
<link href="<c:url value= "/presentation/_include/jquery/rd_lib.css"/>" rel="stylesheet" type="text/css"  /> 
<script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/jquery.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/rd_setup.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/rd_lib.js"/>" space="preserve"  ></script><link rel="shortcut icon" href="<c:url value= "/presentation/_style/favicon.ico"/>"  /> 

</head>

<body class=" " > 
<div id="rd_ghost"  > 
<small  > 
<fmt:message key="cUC206_DetailsPlayer___CSS_DEACTIVATED"/>
</small>
</div>
<div id="rd_jghost"  > 
<small  > 
<fmt:message key="cUC206_DetailsPlayer___JAVASCRIPT_DEACTIVATED"/>
</small>
</div>
<div id="shape"  > 
<div class="ui-layout-center"  > 
<div class="east-north"  > 
	<form:form id="uC206_DetailsPlayer" commandName="uC206_DetailsPlayerForm"  enctype="application/x-www-form-urlencoded" method="post">
<h2 class="rd_first"  > 
<fmt:message key="cUC206_DetailsPlayer_ADVANCED_FEATURES__VALUE_OBJE"/>
</h2>
<table class="rd_tableform"  >
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC206_DetailsPlayer_FIRST_NAME"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
		<span id="txt_firstname">
	<c:out value="${uC206_DetailsPlayerForm.selectedPlayer.firstName}" />
		</span>
	</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC206_DetailsPlayer_LAST_NAME"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
		<span id="txt_lastname">
	<c:out value="${uC206_DetailsPlayerForm.selectedPlayer.lastName}" />
		</span>
	</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC206_DetailsPlayer_DATE_OF_BIRTH"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
<span id="txt_dateofbirth">
	<format:formatDateValidator value="${uC206_DetailsPlayerForm.selectedPlayer.dateOfBirth}" pattern="MM/dd/yyyy" />
</span>
	</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC206_DetailsPlayer_ESTIMATED_VALUE"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
		<span id="txt_estimatedvalue">
	<c:out value="${uC206_DetailsPlayerForm.selectedPlayer.estimatedValue}" />
		</span>
	<fmt:message key="cUC206_DetailsPlayer_M"/>
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC206_DetailsPlayer_WEIGHT"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
		<span id="txt_weight">
	<c:out value="${uC206_DetailsPlayerForm.selectedPlayer.weight}" />
		</span>
	<fmt:message key="cUC206_DetailsPlayer_KG"/>
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC206_DetailsPlayer_HEIGHT"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
		<span id="txt_height">
	<c:out value="${uC206_DetailsPlayerForm.selectedPlayer.height}" />
		</span>
	<fmt:message key="cUC206_DetailsPlayer_CM"/>
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC206_DetailsPlayer_POSITION"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
		<span id="txt_position">
	<c:out value="${uC206_DetailsPlayerForm.selectedPlayer.positionVO.name}" />
		</span>
	</td> 
<td rowspan="1" colspan="1"  >
<fmt:message key="cUC206_DetailsPlayer_"/>
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<label  > 
<fmt:message key="cUC206_DetailsPlayer_ROOKIE"/>
</label>
</td> 
<td rowspan="1" colspan="1"  >
<form:checkbox id="chk_rookie" path="selectedPlayer.rookie"  disabled="true"  
		/>
</td> 
<td rowspan="1" colspan="1"  >
<fmt:message key="cUC206_DetailsPlayer__12"/>
</td> 
</tr>
<tr  > 
<td rowspan="1" colspan="1"  >
<fmt:message key="cUC206_DetailsPlayer__13"/>
</td> 
<td rowspan="1" colspan="1"  >
<a id="lnk_back"				href="<c:url value="/presentation/advancedfeatures/valueobject/uc206_valueobject/UC206_DetailsPlayer/lnk_back.html"/>"
													 shape="rect"  >									 
<fmt:message key="cUC206_DetailsPlayer_BACK"/>
</a>
</td> 
<td rowspan="1" colspan="1"  >
<fmt:message key="cUC206_DetailsPlayer__14"/>
</td> 
</tr>
</table> 
	</form:form>
	<script language="javascript">
		function btn_click_cExecuteUC206_DetailsPlayer(url) {
			UC206_DetailsPlayervar = document.getElementById('uC206_DetailsPlayer');
			UC206_DetailsPlayervar.setAttribute('action', url);
			UC206_DetailsPlayervar.submit();
		}
		function btn_click_cSetActionUC206_DetailsPlayer(url) {
			UC206_DetailsPlayervar = document.getElementById('uC206_DetailsPlayer');
			UC206_DetailsPlayervar.setAttribute('action', url);
		}
	</script>
</div>
<div class="east-center"  > 
<tiles:insertDefinition	name="presentation/advancedfeatures/valueobject/uc206_valueobject/UC206_DetailsPlayercmp_tabs" />
</div>
</div>
<div class="ui-layout-north"  > 
<tiles:insertDefinition	name="presentation/advancedfeatures/valueobject/uc206_valueobject/UC206_DetailsPlayercmp_header" />
</div>
<div class="ui-layout-south"  > 
<fmt:message key="cUC206_DetailsPlayer___"/>
<span id="rd_jversion"  > 
<fmt:message key="cUC206_DetailsPlayer_BLU_AGE_CORPORATION"/>
</span>
</div>
<div class="ui-layout-west"  > 
<tiles:insertDefinition	name="presentation/advancedfeatures/valueobject/uc206_valueobject/UC206_DetailsPlayercmp_treeview" />
</div>
</div>
</body>
</html>
