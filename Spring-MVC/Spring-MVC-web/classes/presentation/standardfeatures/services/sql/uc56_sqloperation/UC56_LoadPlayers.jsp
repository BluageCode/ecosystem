<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file="/include.jsp"  %>
<html >
<head >
<title  > 
<fmt:message key="cUC56_LoadPlayers_BLU_AGE_DOCUMENTATION"/>
</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"  > 
</meta>
<link href="<c:url value= "/presentation/_style/_style.css"/>" rel="stylesheet" type="text/css"  /> 
<link href="<c:url value= "/presentation/_include/jquery/rd_lib.css"/>" rel="stylesheet" type="text/css"  /> 
<script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/jquery.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/rd_setup.js"/>" space="preserve"  ></script><script type="text/javascript" src="<c:url value= "/presentation/_include/jquery/rd_lib.js"/>" space="preserve"  ></script><link rel="shortcut icon" href="<c:url value= "/presentation/_style/favicon.ico"/>"  /> 

</head>

<body class=" " > 
<div id="rd_ghost"  > 
<small  > 
<fmt:message key="cUC56_LoadPlayers___CSS_DEACTIVATED"/>
</small>
</div>
<div id="rd_jghost"  > 
<small  > 
<fmt:message key="cUC56_LoadPlayers___JAVASCRIPT_DEACTIVATED"/>
</small>
</div>
<div id="shape"  > 
<div class="ui-layout-center"  > 
<div class="east-north"  > 
	<form:form id="uC56_LoadPlayers" commandName="uC56_LoadPlayersForm"  enctype="application/x-www-form-urlencoded" method="post">
<h2 class="rd_first"  > 
<fmt:message key="cUC56_LoadPlayers_STANDARD_FEATURES__SERVICES_"/>
</h2>
<b  > 
<fmt:message key="cUC56_LoadPlayers__TEAM__"/>
</b>
		<span id="txt_TeamName">
	<c:out value="${uC56_LoadPlayersForm.team.name}" />
		</span>
	<b  > 
<fmt:message key="cUC56_LoadPlayers___CITY_"/>
</b>
		<span id="txt_Teamcity">
	<c:out value="${uC56_LoadPlayersForm.team.city}" />
		</span>
	<fmt:message key="cUC56_LoadPlayers__"/>
<a id="lnk_back"				href="<c:url value="/presentation/standardfeatures/services/sql/uc56_sqloperation/UC56_Search.html"/>"
													 shape="rect"  >									 
<fmt:message key="cUC56_LoadPlayers_BACK"/>
</a>
<h4  > 
<fmt:message key="cUC56_LoadPlayers_PLAYERS"/>
</h4>
<display:table uid="tab_players" pagesize="10"  sort="list" requestURI="" name="sessionScope.uC56_LoadPlayersForm.players" class="rd_tabledata"  >
	<c:set   var="rownum">
		<%=(tab_players !=	null) ? new Integer(tab_players_rowNum.intValue() - 1) : new Integer(0) %>
	</c:set>
	<c:set   var="idRow">
		<%=(tab_players != null ) ? tab_players_rowNum.intValue() - 1 : -1 %>
	</c:set>
	<c:set   var="idTab">
		<%=(tab_players != null ) ? "tab_players" : null %>
	</c:set>
	<c:set var="tableHashId">
		<%= new org.displaytag.util.ParamEncoder("tab_players").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE) %>
	</c:set>
	<c:set var="tableHashId2">
		<%=request.getParameter(new org.displaytag.util.ParamEncoder("tab_players").encodeParameterName(org.displaytag.tags.TableTagParameters.PARAMETER_PAGE)) %>
	</c:set>	
	
<display:column format=""  titleKey="cUC56_LoadPlayers_NAME">
		<span id="txt_firstName">
	<c:out value="${tab_players.firstName}" />
		</span>
	<fmt:message key="cUC56_LoadPlayers___"/>
		<span id="txt_lastName">
	<c:out value="${tab_players.lastName}" />
		</span>
	</display:column>
</display:table>
	</form:form>
	<script language="javascript">
		function btn_click_cExecuteUC56_LoadPlayers(url) {
			UC56_LoadPlayersvar = document.getElementById('uC56_LoadPlayers');
			UC56_LoadPlayersvar.setAttribute('action', url);
			UC56_LoadPlayersvar.submit();
		}
		function btn_click_cSetActionUC56_LoadPlayers(url) {
			UC56_LoadPlayersvar = document.getElementById('uC56_LoadPlayers');
			UC56_LoadPlayersvar.setAttribute('action', url);
		}
	</script>
</div>
<div class="east-center"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/services/sql/uc56_sqloperation/UC56_LoadPlayerscmp_tabs" />
</div>
</div>
<div class="ui-layout-north"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/services/sql/uc56_sqloperation/UC56_LoadPlayerscmp_header" />
</div>
<div class="ui-layout-south"  > 
<fmt:message key="cUC56_LoadPlayers____220"/>
<span id="rd_jversion"  > 
<fmt:message key="cUC56_LoadPlayers_BLU_AGE_DOCUMENTATION_221"/>
</span>
</div>
<div class="ui-layout-west"  > 
<tiles:insertDefinition	name="presentation/standardfeatures/services/sql/uc56_sqloperation/UC56_LoadPlayerscmp_treeview" />
</div>
</div>
</body>
</html>
