/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.business.gettingstarted.uc05_search.bos;

import javax.inject.Inject;
import javax.inject.Named;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import com.bluage.documentation.business.AbstractBusinessObject;
import com.bluage.documentation.business.gettingstarted.uc05_search.entities.daofinder.Position05DAOFinderImpl;

/**
 * Position05BO
 *
* @see com.netfective.bluage.core.business.IBusinessObject
 */
@XmlRootElement

public class Position05BO
 extends AbstractBusinessObject {
	
	/**
	 * Default SUID for Position05BO.
	 */
	private static final long serialVersionUID = 1L;
	

	@Inject
	@Named ("com.bluage.documentation.business.gettingstarted.uc05_search.entities.daofinder.Position05Finder") 
	private Position05DAOFinderImpl finder;
	/**
	 * Hibernate optimistic lock for class Position05BO.
	 */
	private int version;

						/**
	 * Entity property: code
	 *
	 */
private String code;

					/**
	 * Entity property: name
	 *
	 */
private String name;



   /**
	* public default constructor for class Position05BO
	*/
	public Position05BO(){
		//NOP Position05BO
	}

	
	/**
	 * Getter for version for class Position05BO
	 * @return int
	 */
	public int getVersion() {
		return this.version;
	}
	
	/**
	 * Setter for version for class Position05BO
	 * @param int version
	 */
	public void setVersion(final int version) {
		this.version = version;
	}

						/**
	 * Getter for code of class Position05BO.
	 *
	 * @return {@link java.lang.String}
	 * @generated
	 */ 
public  String getCode() {
	return this.code;
	}


	/**
	 * Setter for code of class Position05BO.
	 * @param value {@link java.lang.String}
	 * @generated
	 */

public  void setCode(final String code) {
	this.isEmpty = (code == null);
	this.code = code;
}

					/**
	 * Getter for name of class Position05BO.
	 *
	 * @return {@link java.lang.String}
	 * @generated
	 */ 
public  String getName() {
	return this.name;
	}


	/**
	 * Setter for name of class Position05BO.
	 * @param value {@link java.lang.String}
	 * @generated
	 */

public  void setName(final String name) {
	this.name = name;
}


/**
	 * Returns <code>true</code> if the argument is an Position05 instance and all identifiers for this entity
     * equal the identifiers of the argument entity. Returns <code>false</code> otherwise.
	 * @param object  object to compare with current instance      
     * @return boolean true if equals false if not
     */
    public boolean equals(final Object object )
    {
		if ( this == object )
        {
            return true;
        }
        if ( !( object instanceof Position05BO ) )
        {
            return false;
        }
        final Position05BO that = (Position05BO)object;
    
    	            if (        				        			this.getCode() == null
        	        ) {
        	return false;
        }
		return new EqualsBuilder()
			.append(this.getCode(), that.getCode())
			 	.appendSuper(super.equals(object))
		 	.isEquals();
	}
	/**
     * Returns a hash code based on this entity's identifiers.     * @return int
     */    
    public int hashCode() {
        return new HashCodeBuilder()
             .append(getCode())
		            .toHashCode();
    }
}

