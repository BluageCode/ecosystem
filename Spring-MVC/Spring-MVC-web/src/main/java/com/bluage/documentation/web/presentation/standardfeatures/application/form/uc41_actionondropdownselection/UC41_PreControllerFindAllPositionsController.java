/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.web.presentation.standardfeatures.application.form.uc41_actionondropdownselection;

// Import declaration.
// Java imports.
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import com.bluage.documentation.business.standardfeatures.application.form.uc41_actionondropdownselection.bos.Position41ForSelectBO;
import com.bluage.documentation.service.standardfeatures.application.form.uc41_actionondropdownselection.ServicePosition41;
import com.netfective.bluage.core.exception.ApplicationException;
import com.netfective.bluage.core.web.AbstractController;
import com.netfective.bluage.core.web.IForm;

/**
 * Class : UC41_PreControllerFindAllPositionsController
 */
 @Service("com.bluage.documentation.web.presentation.standardfeatures.application.form.uc41_actionondropdownselection.UC41_PreControllerFindAllPositionsController")
public class UC41_PreControllerFindAllPositionsController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(UC41_PreControllerFindAllPositionsController.class);
	
	//CONSTANT PINS: positions
	private static final String POSITIONS = "positions";
	
	// Declaring all the instances.
		Position41ForSelectBO positions;
	/**
	 * Service declaration : servicePosition41.
	 */
	@Autowired
	private ServicePosition41 servicePosition41;
	
	/**
	 * Operation : UC41_PreControllerFindAllPositionsControllerInit
	 * @param request : The current HttpRequest
 	 * @param model :  The current Model
 	 * @param currentIForm : The current IForm
 	 * @return
	 */
	public IForm uC41_PreControllerFindAllPositionsControllerInit(final HttpServletRequest request,final  Model model,final  IForm  currentIForm){

		LOGGER.info("Begin the precontroller method : findPositions");
		LOGGER.info("Form diagnostic : " + currentIForm);
		

	callFindAllPositions(request,currentIForm);
		
			LOGGER.info("Populate the destination screen.");
			// Populate the destination form with all the instances returned from an executed service.
			return populateDestinationForm(request.getSession(), currentIForm); // Populate the destination screen

	}
	/**
	 * method callFindAllPositions
	 * 
	 * @param request HttpServletRequest
	 * @param currentIForm IForm
	 */
	private void callFindAllPositions(HttpServletRequest request, IForm currentIForm) {
		// Initialize all the instances.
		positions = (Position41ForSelectBO)get(request.getSession(),currentIForm,POSITIONS);
							try {

				positions = 	servicePosition41.loadPositions41(
	);  

				put(request.getSession(), POSITIONS,positions);
							
			} catch (ApplicationException e) {
				LOGGER.error("An error occured during the execution of the operation : loadPositions41",e);
			}
	}
}
