/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.qauc.uc1001_tables ;


import org.apache.log4j.Logger;
import org.springframework.validation.Errors;

import com.netfective.bluage.core.validator.AbstractValidator;
/**
* Class :UC1001_ViewTeamValidator
*/
public class UC1001_ViewTeamValidator extends AbstractValidator{
	
	// LOGGER for the class UC1001_ViewTeamValidator
	private static final Logger LOGGER = Logger.getLogger( UC1001_ViewTeamValidator.class);
	
	
	/**
	* Operation validate for UC1001_ViewTeamForm
	* @param obj : the current form (UC1001_ViewTeamForm)
	* @param errors : The spring errors to return for the form UC1001_ViewTeamForm
	*/
	public void validate(final Object obj,final Errors errors){
		LOGGER.info("Beginning method : validate"); // UC1001_ViewTeamValidator
		UC1001_ViewTeamForm cUC1001_ViewTeamForm = (UC1001_ViewTeamForm)obj; // UC1001_ViewTeamValidator
		LOGGER.info("Ending method : validate the form "+ cUC1001_ViewTeamForm.getClass().getName()); // UC1001_ViewTeamValidator
	}

	/**
	* Method to implements to use spring validators (UC1001_ViewTeamForm)
	* @param aClass : Class for the form UC1001_ViewTeamForm
	* @return boolean
	*/
	public boolean supports(final Class aClass){
		return (new UC1001_ViewTeamForm()).getClass().equals(aClass); // UC1001_ViewTeamValidator
	}
}
