package uc43;

import org.junit.Test;

import com.thoughtworks.selenium.Selenium;

import test.common.AbstractBADocTestCase;

public class ITuc43 extends AbstractBADocTestCase {
	

	@Override
	public  String getName(){
		return "UC-43";
	}
	@Test
	public void testITuc43() throws Exception {
		// On teste l'UC43 - Standard Features > Application > Form > UC43 > Captcha
		selenium.open("");
		selenium.click("link=Expand All");
		selenium.waitForPageToLoad("30000");
		selenium.click("link=UC43 - Captcha");
		verifyTrue(selenium.isElementPresent("//h2[contains(text(),'Standard Features > Application > Form > UC43 > Captcha')]"));
		verifyTrue(selenium.isElementPresent("//*[contains(text(),'Please type the word appearing in the picture:')]"));
		assertTrue(selenium.isElementPresent("//img[@id=\"cap_create\"]"));
		// On ne rempli aucun champs, cliquer sur "Create Player"
		selenium.click("//*[contains(@id,'lnk_add')]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie que l'ID du message d'erreur est pr�sente.
		assertTrue(selenium.isElementPresent("//*[contains(@id,'error_txt_firstname')]|//*[contains(@id,'playerToAdd.firstName.errors')]"));
		// On v�rifie que le message d'erreur est pr�sent.
		assertEquals("First name is required.", selenium.getText("//*[contains(@id,'error_txt_firstname')]|//*[contains(@id,'playerToAdd.firstName.errors')]"));
		// On v�rifie que l'ID du message d'erreur est pr�sente.
		assertTrue(selenium.isElementPresent("//*[contains(@id,'error_txt_lastname')]|//*[contains(@id,'playerToAdd.lastName.errors')]"));
		// On v�rifie que le message d'erreur est pr�sent.
		assertEquals("Last name is required.", selenium.getText("//*[contains(@id,'error_txt_lastname')]|//*[contains(@id,'playerToAdd.lastName.errors')]"));
		// On v�rifie que le message d'erreur du captcha est pr�sent.
		assertTrue(selenium.isTextPresent("Captcha validation failed."));
		verifyEquals("3", selenium.getXpathCount("//table[contains(@id,'tab_players')]/tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/thead/tr[th[contains(text(),\"Name\")] and th[contains(text(),\"Date of Birth\")] and th[contains(text(),\"Position\")] and th[contains(text(),\"Rookie\")] and th[contains(text(),\"Delete\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Frank\")] and td/span[contains(text(),\"Oliver\")] and td/span[contains(text(),\"10/31/1968\")] and td/span[contains(text(),\"PG\")] and td//input[not(@checked)] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Cole\")] and td/span[contains(text(),\"Elbert\")] and td/span[contains(text(),\"04/07/1965\")] and td/span[contains(text(),\"SG\")] and td//input[not(@checked)] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Dawson\")] and td/span[contains(text(),\"Ricky\")] and td/span[contains(text(),\"03/03/1990\")] and td/span[contains(text(),\"SF\")] and td//input[@checked] and td/a[contains(text(),\"[delete]\")]]"));
		selenium.click("link=UC43 - Captcha");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//h2[contains(text(),'Standard Features > Application > Form > UC43 > Captcha')]"));
		// On entre des suites de caract�res valides pour tous les champs.
		selenium.type("//*[contains(@id,'txt_firstname')]", "Jean");
		selenium.type("//*[contains(@id,'txt_lastname')]", "Pierre");
		selenium.type("//*[contains(@id,'txt_dateofbirth')]", "12/14/1980");
		selenium.select("//*[contains(@id,'slt_position')]", "label=Small Forward");
		selenium.click("//*[contains(@id,'chk_rookie')]");
		assertTrue(selenium.isElementPresent("//*[contains(text(),'[Create player]')]"));
		selenium.click("//*[contains(@id,'lnk_add')]");
		selenium.waitForPageToLoad("30000");
		// On v�rifie que le message d'erreur est pr�sent.
		assertTrue(selenium.isTextPresent("Captcha validation failed."));
		verifyEquals("3", selenium.getXpathCount("//table[contains(@id,'tab_players')]/tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/thead/tr[th[contains(text(),\"Name\")] and th[contains(text(),\"Date of Birth\")] and th[contains(text(),\"Position\")] and th[contains(text(),\"Rookie\")] and th[contains(text(),\"Delete\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Frank\")] and td/span[contains(text(),\"Oliver\")] and td/span[contains(text(),\"10/31/1968\")] and td/span[contains(text(),\"PG\")] and td//input[not(@checked)] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Cole\")] and td/span[contains(text(),\"Elbert\")] and td/span[contains(text(),\"04/07/1965\")] and td/span[contains(text(),\"SG\")] and td//input[not(@checked)] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Dawson\")] and td/span[contains(text(),\"Ricky\")] and td/span[contains(text(),\"03/03/1990\")] and td/span[contains(text(),\"SF\")] and td//input[@checked] and td/a[contains(text(),\"[delete]\")]]"));
		selenium.click("link=UC43 - Captcha");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isElementPresent("//h2[contains(text(),'Standard Features > Application > Form > UC43 > Captcha')]"));
		// On entre des suites de caract�res valides pour tous les champs.
		selenium.type("//*[contains(@id,'txt_firstname')]", "Jean");
		selenium.type("//*[contains(@id,'txt_lastname')]", "Pierre");
		selenium.type("//*[contains(@id,'txt_dateofbirth')]", "12/14/1980");
		selenium.select("//*[contains(@id,'slt_position')]", "label=Small Forward");
		selenium.click("//*[contains(@id,'chk_rookie')]");
		selenium.type("//input[contains(@name,'jcaptcha_response')]|//input[contains(@name,'captcha')]", "nogood");
		assertTrue(selenium.isElementPresent("//*[contains(text(),'[Create player]')]"));
		selenium.click("//*[contains(@id,'lnk_add')]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isTextPresent("Captcha validation failed."));
		verifyEquals("3", selenium.getXpathCount("//table[contains(@id,'tab_players')]/tbody//tr"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/thead/tr[th[contains(text(),\"Name\")] and th[contains(text(),\"Date of Birth\")] and th[contains(text(),\"Position\")] and th[contains(text(),\"Rookie\")] and th[contains(text(),\"Delete\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Frank\")] and td/span[contains(text(),\"Oliver\")] and td/span[contains(text(),\"10/31/1968\")] and td/span[contains(text(),\"PG\")] and td//input[not(@checked)] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Cole\")] and td/span[contains(text(),\"Elbert\")] and td/span[contains(text(),\"04/07/1965\")] and td/span[contains(text(),\"SG\")] and td//input[not(@checked)] and td/a[contains(text(),\"[delete]\")]]"));
		assertTrue(selenium.isElementPresent("//table[contains(@id,'tab_players')]/tbody/tr[td/span[contains(text(),\"Dawson\")] and td/span[contains(text(),\"Ricky\")] and td/span[contains(text(),\"03/03/1990\")] and td/span[contains(text(),\"SF\")] and td//input[@checked] and td/a[contains(text(),\"[delete]\")]]"));
	}

	
}
