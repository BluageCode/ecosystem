--<ScriptOptions statementTerminator=";"/>

ALTER TABLE team02 DROP CONSTRAINT team02_state_fkc;

ALTER TABLE state02 DROP CONSTRAINT state02_pkey;

ALTER TABLE team02 DROP CONSTRAINT team02_pkey;

DROP TABLE team02;

DROP TABLE state02;

CREATE TABLE team02 (
		id INT8 NOT NULL,
		version INT4 NOT NULL,
		city VARCHAR(1024),
		name VARCHAR(1024),
		state_fk VARCHAR(2) NOT NULL
	);

CREATE TABLE state02 (
		code VARCHAR(2) NOT NULL,
		version INT4 NOT NULL,
		name VARCHAR(20) NOT NULL
	);
	
INSERT INTO team02 VALUES (296, 0, 'Chicago', 'Bulls', 'IL');
INSERT INTO team02 VALUES (297, 0, 'Bordeaux', 'Girondins', 'GI');
INSERT INTO team02 VALUES (301, 0, 'Cleveland', 'Cavaliers', 'OH');
INSERT INTO team02 VALUES (303, 0, 'Boston', 'Celtics', 'MA');
INSERT INTO team02 VALUES (305, 0, 'Anchorage', 'Furious Penguins', 'AK');
INSERT INTO team02 VALUES (306, 0, 'Juneau', 'Wild Meese', 'AK');
	
	
INSERT INTO state02 (code, version, name) VALUES ('AL', 0, 'Alabama');
INSERT INTO state02 (code, version, name) VALUES ('AK', 0, 'Alaska');
INSERT INTO state02 (code, version, name) VALUES ('AZ', 0, 'Arizona');
INSERT INTO state02 (code, version, name) VALUES ('AR', 0, 'Arkansas');
INSERT INTO state02 (code, version, name) VALUES ('CA', 0, 'California');
INSERT INTO state02 (code, version, name) VALUES ('CO', 0, 'Colorado');
INSERT INTO state02 (code, version, name) VALUES ('CT', 0, 'Connecticut');
INSERT INTO state02 (code, version, name) VALUES ('DE', 0, 'Delaware');
INSERT INTO state02 (code, version, name) VALUES ('FL', 0, 'Florida');
INSERT INTO state02 (code, version, name) VALUES ('GI', 0, 'Gironde');
INSERT INTO state02 (code, version, name) VALUES ('HI', 0, 'Hawaii');
INSERT INTO state02 (code, version, name) VALUES ('ID', 0, 'Idaho');
INSERT INTO state02 (code, version, name) VALUES ('IL', 0, 'Illinois');
INSERT INTO state02 (code, version, name) VALUES ('IN', 0, 'Indiana');
INSERT INTO state02 (code, version, name) VALUES ('IA', 0, 'Iowa');
INSERT INTO state02 (code, version, name) VALUES ('KS', 0, 'Kansas');
INSERT INTO state02 (code, version, name) VALUES ('KY', 0, 'Kentucky');
INSERT INTO state02 (code, version, name) VALUES ('LA', 0, 'Louisiana');
INSERT INTO state02 (code, version, name) VALUES ('ME', 0, 'Maine');
INSERT INTO state02 (code, version, name) VALUES ('MD', 0, 'Maryland');
INSERT INTO state02 (code, version, name) VALUES ('MA', 0, 'Massachusetts');
INSERT INTO state02 (code, version, name) VALUES ('MI', 0, 'Michigan');
INSERT INTO state02 (code, version, name) VALUES ('MN', 0, 'Minnesota');
INSERT INTO state02 (code, version, name) VALUES ('MS', 0, 'Mississippi');
INSERT INTO state02 (code, version, name) VALUES ('MO', 0, 'Missouri');
INSERT INTO state02 (code, version, name) VALUES ('MT', 0, 'Montana');
INSERT INTO state02 (code, version, name) VALUES ('NE', 0, 'Nebraska');
INSERT INTO state02 (code, version, name) VALUES ('NV', 0, 'Nevada');
INSERT INTO state02 (code, version, name) VALUES ('NH', 0, 'New Hampshire');
INSERT INTO state02 (code, version, name) VALUES ('NJ', 0, 'New Jersey');
INSERT INTO state02 (code, version, name) VALUES ('NM', 0, 'New Mexico');
INSERT INTO state02 (code, version, name) VALUES ('NY', 0, 'New York');
INSERT INTO state02 (code, version, name) VALUES ('NC', 0, 'North Carolina');
INSERT INTO state02 (code, version, name) VALUES ('ND', 0, 'North Dakota');
INSERT INTO state02 (code, version, name) VALUES ('OH', 0, 'Ohio');
INSERT INTO state02 (code, version, name) VALUES ('OK', 0, 'Oklahoma');
INSERT INTO state02 (code, version, name) VALUES ('OR', 0, 'Oregon');
INSERT INTO state02 (code, version, name) VALUES ('PA', 0, 'Pennsylvania');
INSERT INTO state02 (code, version, name) VALUES ('RI', 0, 'Rhode Island');
INSERT INTO state02 (code, version, name) VALUES ('SC', 0, 'South Carolina');
INSERT INTO state02 (code, version, name) VALUES ('SD', 0, 'South Dakota');
INSERT INTO state02 (code, version, name) VALUES ('TN', 0, 'Tennessee');
INSERT INTO state02 (code, version, name) VALUES ('TX', 0, 'Texas');
INSERT INTO state02 (code, version, name) VALUES ('UT', 0, 'Utah');
INSERT INTO state02 (code, version, name) VALUES ('VT', 0, 'Vermont');
INSERT INTO state02 (code, version, name) VALUES ('VA', 0, 'Virginia');
INSERT INTO state02 (code, version, name) VALUES ('WA', 0, 'Washington');
INSERT INTO state02 (code, version, name) VALUES ('WV', 0, 'West Virginia');
INSERT INTO state02 (code, version, name) VALUES ('WI', 0, 'Wisconsin');
INSERT INTO state02 (code, version, name) VALUES ('WY', 0, 'Wyoming');

	

ALTER TABLE state02 ADD CONSTRAINT state02_pkey PRIMARY KEY (code);

ALTER TABLE team02 ADD CONSTRAINT team02_pkey PRIMARY KEY (id);

ALTER TABLE team02 ADD CONSTRAINT team02_state_fkc FOREIGN KEY (state_fk)
	REFERENCES state02 (code);

