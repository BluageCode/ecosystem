/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.presentation.standardfeatures.entities.uc63_association ;


import org.apache.log4j.Logger;
import org.springframework.validation.Errors;

import com.netfective.bluage.core.validator.AbstractValidator;
/**
* Class :UC63_HomeValidator
*/
public class UC63_HomeValidator extends AbstractValidator{
	
	// LOGGER for the class UC63_HomeValidator
	private static final Logger LOGGER = Logger.getLogger( UC63_HomeValidator.class);
	
	
	/**
	* Operation validate for UC63_HomeForm
	* @param obj : the current form (UC63_HomeForm)
	* @param errors : The spring errors to return for the form UC63_HomeForm
	*/
	public void validate(final Object obj,final Errors errors){
		LOGGER.info("Beginning method : validate"); // UC63_HomeValidator
		UC63_HomeForm cUC63_HomeForm = (UC63_HomeForm)obj; // UC63_HomeValidator
		LOGGER.info("Ending method : validate the form "+ cUC63_HomeForm.getClass().getName()); // UC63_HomeValidator
	}

	/**
	* Method to implements to use spring validators (UC63_HomeForm)
	* @param aClass : Class for the form UC63_HomeForm
	* @return boolean
	*/
	public boolean supports(final Class aClass){
		return (new UC63_HomeForm()).getClass().equals(aClass); // UC63_HomeValidator
	}
}
