/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package com.bluage.documentation.service.standardfeatures.entities.uc72_generatorclass;


import javax.inject.Inject;
import javax.inject.Named;

import com.bluage.documentation.business.standardfeatures.entities.uc72_generatorclass.bos.State72BO;
import com.bluage.documentation.business.standardfeatures.entities.uc72_generatorclass.entities.daofinder.State72DAOFinderImpl;
import com.bluage.documentation.service.AbstractService;
import com.netfective.bluage.core.exception.ApplicationException;

/**
 * Defines  application services for ServiceState72
  */
@Named("com.bluage.documentation.service.standardfeatures.entities.uc72_generatorclass.ServiceState72")
public class ServiceState72  extends AbstractService{


			@Inject
		@Named ("com.bluage.documentation.business.standardfeatures.entities.uc72_generatorclass.entities.daofinder.State72DAOFinderImpl")
		private State72DAOFinderImpl state72BOFinder;
	


	/**
	 *  Default constructor 
	 */
	public ServiceState72() {
		super(); // Call super for default constructor : ServiceState72
	}
	


			
				
	/**    
	 * Operation Crud
	 * @param bo {@link State72BO}
		 * @return {@link State72BO}
	 */
	public State72BO state72FindByID(State72BO bo )
 throws ApplicationException {

					return (State72BO)state72BOFinder.findByPrimaryKey(getBoId(bo));
	}
	/**
	 *  Return the primary key from business object.
	 */
	private Object getBoId(State72BO bo) {
				return bo.getCode();
		}

		}
