/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.business ;
 
//Import declaration
import javax.xml.bind.annotation.XmlTransient;

import org.apache.log4j.Logger;

import com.netfective.bluage.core.business.IBusinessObject;
/**
* Class :AbstractBusinessObject
*/
public abstract class AbstractBusinessObject  implements IBusinessObject{


	/**
	 * HFactor
	 */
	protected static final int H_FACTOR=29;

	/**
	* Property:LOGGER
	*/
	protected static final Logger LOGGER = Logger.getLogger(AbstractBusinessObject.class);

	/**
	* Property:L_END_OF_METHOD
	*/
	protected static final String L_END_OF_METHOD = "End of Method whith Error ";
	
	/**
	* Property:L_RETURNS
	*/
	protected static final String L_RETURNS = "return ";
	
	/**
	* serialVersionUID
	*/
	private static final long serialVersionUID =  - (8542674304551719677L);

	/**
	* Check if object has an identifier set.
	*/
	@XmlTransient
	public boolean isEmpty;

	/**
	* Mode, initializing to DAO
	*/
	@XmlTransient
	public String objMode = IBusinessObject.DAO_MODE;

	/**
	*Set the object mode
	*/
	@XmlTransient
	public void setObjMode(final String strValue){
		this.objMode = strValue;
	}


	/**
	*Get the object mode
	*/
	public String getObjMode(){
		return this.objMode;
	}
}
