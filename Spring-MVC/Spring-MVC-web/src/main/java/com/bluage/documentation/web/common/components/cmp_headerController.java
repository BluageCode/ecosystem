/**
 *  Copyright (C) <2013>  <Blu Age Corporation>

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

// Generated with Blu Age version 4.2.0GA20130506
package  com.bluage.documentation.web.common.components ;

// Import declaration.
// Java imports.
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.netfective.bluage.core.utilities.editors.CustomDateEditor;
import com.netfective.bluage.core.web.AbstractController;

/**
 * Class : cmp_headerController
 */
@Controller
@Scope("prototype")
@SessionAttributes("cmp_headerForm")
@RequestMapping(value= "/common/components")
public class cmp_headerController extends AbstractController {
	
	/**
	 * Property:LOGGER
	 */
	private static final  Logger LOGGER = Logger.getLogger(cmp_headerController.class);
	
	//Current form name
	private static final String CMP_HEADER_FORM = "cmp_headerForm";

	
	/**
	 * Property:customDateEditorscmp_headerController
	 */
	final private Map<String,CustomDateEditor> customDateEditorscmp_headerController = new HashMap<String,CustomDateEditor>();
	
	/**
	 * Property:cmp_headerValidator
	 */
	final private cmp_headerValidator cmp_headerValidator = new cmp_headerValidator();
	
	
	// Initialise all the instances for cmp_headerController

	/**
	* This method initialise the form : cmp_headerForm 
	* @return cmp_headerForm
	*/
	@ModelAttribute("cmp_headerFormInit")
	public void initcmp_headerForm(final HttpServletRequest request,final Model model,final SessionStatus sessionStatus){
		// Clean HTTP Session.
		cleanHttpSession(request, CMP_HEADER_FORM);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Start method cmp_headerForm."); //for lnk_home 
		}
		cmp_headerForm cmp_headerForm;
	
		if(request.getSession().getAttribute(CMP_HEADER_FORM) != null){
			cmp_headerForm = (cmp_headerForm)request.getSession().getAttribute(CMP_HEADER_FORM);
		} else {
			cmp_headerForm = new cmp_headerForm();
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Populate the form cmp_headerForm.");
		}
		cmp_headerForm = (cmp_headerForm)populateDestinationForm(request.getSession(), cmp_headerForm);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Set in the model the form :  cmp_headerForm.");
		}
		model.addAttribute(CMP_HEADER_FORM, cmp_headerForm);

	}
/**
	 * Initialize the form if nécessary. 
	 * @param model : The Spring MVC instance store.
	 * @param cmp_headerForm : The sceen form.
	 */
	@RequestMapping(value = "/cmp_header.html" ,method = RequestMethod.GET)
	public void preparecmp_header(final HttpServletRequest request,final  Model model,final  @ModelAttribute("cmp_headerFormInit") cmp_headerForm cmp_headerForm){
		
		cmp_headerForm currentcmp_headerForm = cmp_headerForm;

		// Verify the controller is initialized.
		if(request.getSession().getAttribute(CMP_HEADER_FORM) == null){
			if(currentcmp_headerForm!=null){
				request.getSession().setAttribute(CMP_HEADER_FORM, currentcmp_headerForm);
			}else {
				currentcmp_headerForm = new cmp_headerForm();
				request.getSession().setAttribute(CMP_HEADER_FORM, currentcmp_headerForm);	
			}
		} else {
			currentcmp_headerForm = (cmp_headerForm) request.getSession().getAttribute(CMP_HEADER_FORM);
		}

		currentcmp_headerForm = (cmp_headerForm)populateDestinationForm(request.getSession(), currentcmp_headerForm);
		// Call all the Precontroller.
	currentcmp_headerForm.setAlwaysCallPreControllers(true);
		model.addAttribute(CMP_HEADER_FORM, currentcmp_headerForm);
		request.getSession().setAttribute(CMP_HEADER_FORM, currentcmp_headerForm);
		
	}
	
	
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder strBToS = new StringBuilder("cmp_headerController [ ");
		// Validator is initialised.
		strBToS.append(LOG_VALIDATOR_INITIALISED);
		strBToS.append(cmp_headerValidator!=null);
		// CustomEditor is initialised
		strBToS.append(LOG_CUSTOM_DATE_INITIALISED);
		strBToS.append(customDateEditorscmp_headerController!=null); 
		return strBToS.toString();
	}
}
